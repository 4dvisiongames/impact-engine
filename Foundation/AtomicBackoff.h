/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__AtomicBackoff_h__
#define __Foundation__AtomicBackoff_h__

#include <Foundation/Interlocked.h>
#include <Foundation/System.h>
#include <Foundation/AtomicBackoffStrategy.h>

namespace Threading
{
	/// <summary>
	/// Class that implements exponential backoff with selected strategies.
	/// </summary>
	template< typename SpinStrategy, typename FailStrategy, SizeT BackoffCount = 16, SizeT BackoffThreshold = 2 >
	class AtomicBackoff 
	{
		__IME_DISABLE_ASSIGNMENT( AtomicBackoff );

	public:
		/// <summary cref="AtomicBackoff::AtomicBackoff">
		/// </summary>
		AtomicBackoff();

		/// <summary cref="AtomicBackoff::Pause">
		/// Pause for a while.
		/// </summary>
		void Pause();

		/// <summary cref="AtomicBackoff::BoundedPause">
		/// Pause for a few times and then return false immediately.
		/// </summary>
		bool BoundedPause();

		/// <summary cref="AtomicBackoff::Reset">
		/// Reset backoff counter.
		/// </summary>
		void Reset();

	private:
		/// My backoff counter
		u32 myBackoffCounter;

		/// Backoff strategy to do something before fail condition happened
		SpinStrategy mySpinStrategy;

		/// Backoff strategy to do something after fail condition happened
		FailStrategy myFailStrategy;
	};

	template< typename SpinStrategy, typename FailStrategy, SizeT BackoffCount, SizeT BackoffThreshold >
	__IME_INLINED AtomicBackoff< SpinStrategy, FailStrategy, BackoffCount, BackoffThreshold >::AtomicBackoff()
		: myBackoffCounter(1)
	{ 
		/* Nothing */ 
	}

	template< typename SpinStrategy, typename FailStrategy, SizeT BackoffCount, SizeT BackoffThreshold >
	__IME_INLINED void AtomicBackoff< SpinStrategy, FailStrategy, BackoffCount, BackoffThreshold >::Pause()
	{
		if (myBackoffCounter <= BackoffCount)
		{
			mySpinStrategy();
			// Pause twice as long the next time.
			myBackoffCounter *= BackoffThreshold;
		} 
		else
		{
			// Pause is so long that we might as well yield CPU to scheduler.
			myFailStrategy();
		}
	}

	// pause for a few times and then return false immediately.
	template< typename SpinStrategy, typename FailStrategy, SizeT BackoffCount, SizeT BackoffThreshold >
	__IME_INLINED bool AtomicBackoff< SpinStrategy, FailStrategy, BackoffCount, BackoffThreshold >::BoundedPause()
	{
		if (myBackoffCounter <= BackoffCount)
		{
			mySpinStrategy();
			// Pause twice as long the next time.
			myBackoffCounter *= BackoffThreshold;
			return true;
		} 
		else 
		{
			return false;
		}
	}

	template< typename SpinStrategy, typename FailStrategy, SizeT BackoffCount, SizeT BackoffThreshold >
	__IME_INLINED void AtomicBackoff< SpinStrategy, FailStrategy, BackoffCount, BackoffThreshold >::Reset()
	{ 
		myBackoffCounter = 1;
	}
}

#endif /// __Foundation__AtomicBackoff_h__

/*
	change log:

	19-Apr-15	codepoet	class AtomicBackoff		rewritten AtomicBackoff code: added new mechanisms to manipulate backoff spinning.
*/