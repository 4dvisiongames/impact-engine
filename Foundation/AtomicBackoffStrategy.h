/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__AtomicBackoffStrategy_h__
#define __Foundation__AtomicBackoffStrategy_h__

#include <Foundation/System.h>

namespace Threading
{
	struct BackoffStrategyNothing
	{
		void operator()() const;
	};

	__IME_INLINED void BackoffStrategyNothing::operator()() const
	{
		/* Nothing */
	}


	struct BackoffStrategyYield
	{
		void operator()() const;
	};

	__IME_INLINED void BackoffStrategyYield::operator()() const
	{
		__IME_SLEEPING_SWITCH_TO_THREAD;
	}



	struct BackoffStrategyPause
	{
		void operator()() const;
	};

	__IME_INLINED void BackoffStrategyPause::operator()() const
	{
		__IME_SLEEPING_PAUSE;
	}



	struct BackoffStrategyNoOperation
	{
		void operator()() const;
	};

	__IME_INLINED void BackoffStrategyNoOperation::operator()() const
	{
	}
}

#endif /// __Foundation__AtomicBackoffStrategy_h__