/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__AtomicDoOnce_h__
#define __Foundation__AtomicDoOnce_h__

#include <Foundation/AtomicBackoff.h>

namespace Threading
{
	//! One-time initialization states
	struct DoOnceState 
	{
		enum List 
		{
			//! No execution attempts have been undertaken yet
			Uninitialized = 0, 
			//! A thread is executing associated do-once routine
			Pending, 
			//! Do-once routine has been executed
			Executed,
			//! Convenience alias
			InitializationComplete = Executed 
		};
	};

	namespace Internal
	{
		//! Initialization from function that do not returns anything, e.g. initializaes on demand
		void RunInitializer( void (*f)(void), volatile DoOnceState::List& state );
		//! Initialization from function that returns result and may need re-initialization on false result
		void RunInitializer( bool (*f)(void), volatile DoOnceState::List& state );
	}

	template< typename F >
	__IME_INLINED void AtomicDoOnce( const F& func, volatile DoOnceState::List& state )
	{
		typedef AtomicBackoff< BackoffStrategyPause, BackoffStrategyYield > DoOnceBackoff;

		/// The loop in the implementation is necessary to avoid race when thread T2
		/// that arrived in the middle of initialization attempt by another thread T1
		/// has just made initialization possible.
		/// In such a case T2 has to rely on T1 to initialize, but T1 may already be past
		/// the point where it can recognize the changed conditions.
		while( Interlocked::Fetch< MemoryOrderEnum::Acquire >( state ) != DoOnceState::Executed ) 
		{
			if( Interlocked::Fetch< MemoryOrderEnum::Acquire >( state ) == DoOnceState::Uninitialized ) 
			{
				if( Interlocked::CompareAndSwap( state, DoOnceState::Pending, DoOnceState::Uninitialized ) == DoOnceState::Uninitialized ) 
				{
					Internal::RunInitializer( func, state );
					break;
				}
			}

			DoOnceBackoff backoff;
			do
			{
				backoff.Pause();
			} while( Interlocked::Fetch< MemoryOrderEnum::Acquire >( state ) == DoOnceState::Pending );
		}
	};
}

#endif /// __Foundation__AtomicDoOnce_h__