/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef _BaseTime_h_
#define _BaseTime_h_

/// Engine shared code
#include <Foundation/Shared.h>
/// STL string
#include <Foundation/STLString.h>

/// Timing namespace
namespace Timing
{
	/// Forward declaration
	class Calendar;

	/// <summary>
	/// <c>CalendarBase</c> is a basic definition of calendar types: year, month, week, day,
	/// etc. generally used by FileSystem, but can used for other.
	class CalendarBase
	{
	public:
		typedef unsigned int Year; //!< Year definition
		typedef unsigned int Day; //!< Day type definition
		typedef unsigned int Hour; //!< Hour type definition
		typedef unsigned int Minute; //!< Minute type definition
		typedef unsigned int Second; //!< Second type definition
		typedef unsigned int Millisecond; //!< Millisecond type definition

		/// <summary>
		/// <c>Month</c> is a month type defintion.
		/// </summary>
		struct Month
		{
			enum List
			{
				January = 1,
				Febrary,
				March,
				April,
				May,
				June,
				July,
				August,
				September,
				October,
				November,
				December
			};
		};

		/// <summary>
		/// <c>WeekDay</c> is a day of week type definition.
		/// </summary>
		struct WeekDay
		{
			enum List
			{
				Sunday = 0,
				Monday,
				Tuesday,
				Wednesday,
				Thursday,
				Friday,
				Saturday
			};
		};

	protected:
		Year mYear;
		Month::List mMonth;
		WeekDay::List mWeekDay;
		Day mDay;
		Hour mHour;
		Minute mMinute;
		Second mSecond;
		Millisecond mMillisecond;

	public:

		/// <summary cref="CalendarBase::CalendayBase">
		/// Constructor.
		/// </summary>
		CalendarBase( void );

		/// <summary cref="CalendarBase::SetYear">
		/// Set year.
		/// </summary>
		/// <param name="value">Year value.</param>
		void SetYear( Year value );

		/// <summary cref="CalendarBase::GetYear">
		/// Get current year.
		/// </summary>
		/// <returns>Current year value.</returns>
		Year GetYear( void ) const;

		/// <summary cref="CalendarBase::SetMonth">
		/// Set month.
		/// </summary>
		/// <param name="value">Month value.</param>
		void SetMonth( Month::List value );

		/// <summary cref="CalendarBase::GetMonth">
		/// Get current month.
		/// </summary>
		Month::List GetMonth( void ) const;

		/// <summary cref="CalendarBase::SetWeekDay">
		/// Set week day.
		/// </summary>
		/// <param name="value">Day of a week value.</param>
		void SetWeekDay( WeekDay::List value );

		/// <summary cref="CalendarBase::GetWeekDay">
		/// Get current day of a week.
		/// </summary>
		/// <returns>Current day of a week.</returns>
		WeekDay::List GetWeekDay( void ) const;

		/// <summary cref="CalendarBase::SetDay">
		/// Set day of a month.
		/// </summary>
		/// <param name="value">Day of a month.</param>
		void SetDay( Day value );

		/// <summary cref="CalendarBase::GetDay">
		/// Get current day of a month.
		/// </summary>
		/// <returns>Current day of a month.</returns>
		Day GetDay( void ) const;

		/// <summary cref="CalendarBase::SetHour">
		/// Set hour of a day.
		/// </summary>
		/// <param name="value">Current hour of a day.</param>
		void SetHour( Hour value );

		/// <summary cref="CalendarBase::GetHour">
		/// Get current hour of a day
		/// </summary>
		/// <returns>Current hour of a day.</returns>
		Hour GetHour( void ) const;

		/// <summary cref="CalendarBase::SetMinute">
		/// Set minute of an hour.
		/// </summary>
		/// <param name="value">Minute of an hour value.</param>
		void SetMinute( Minute value );

		/// <summary cref="CalendarBase::GetMinute">
		/// Get minute of and hour.
		/// </summary>
		/// <returns>Current value of minute of an hour.</returns>
		Minute GetMinute( void ) const;

		/// <summary cref="CalendarBase::SetSecond">
		/// Set second of a minute.
		/// </summary>
		/// <param name="value">Second of a minute value.</param>
		void SetSecond( Second value );

		/// <summary cref="CalendarBase::GetSecond">
		/// Get current second of a minute.
		/// </summary>
		/// <returns>Current value of second of a minute.</returns>
		Second GetSecond( void ) const;

		/// <summary cref="CalendarBase::SetMillisecond">
		/// Set millisecond(-s).
		/// </summary>
		/// <param name="value">Millisecond value.</param>
		void SetMillisecond( Millisecond value );

		/// <summary cref="CalendarBase::GetMillisecond">
		/// Get current millisecond(-s).
		/// </summary>
		/// <returns>Current value of milliseconds.</returns>
		Millisecond GetMilliseconds( void ) const;

		/// <summary cref="CalendarBase::FormatDate">
		/// Convert current year to string type.
		/// </summary>
		/// <returns>Formatted string.</returns>
		static STL::String< Char16 > FormatDate( const Timing::Calendar& rhs );

		/// <summary cref="CalendarBase::ConvertToString">
		/// Convert month to string.
		/// </summary>
		/// <param name="value">Month value.</param>
		/// <returns>Pre-built string with selected month.</returns>
		static STL::String< Char16 > ConvertToString( Month::List value );

		/// <summary cref="CalendarBase::ConvertToString">
		/// Convert week day to string.
		/// </summary>
		/// <param name="value">Day of a week value.</param>
		/// <returns>Pre-built string with selected day.</returns>
		static STL::String< Char16 > ConvertToString( WeekDay::List value );
	};
}

#endif /// _NanoTimeBase_h_