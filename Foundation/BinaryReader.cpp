/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Binary reader
#include <Foundation/BinaryReader.h>

/// FileSystem sub-system namespace
namespace FileSystem
{
	BinaryReader::BinaryReader( void ) 
		: bEnableMapping( false )
		, bIsMapped( false )
		, mByteOrder( Core::ByteOrder::LittleEndian )
	{
		/// Empty
	}

	BinaryReader::~BinaryReader( void )
	{
		if( this->IsOpen() )
			this->Close();
	}

	bool BinaryReader::Open( void )
	{
		if( StreamReader::Open() ) 
		{
			if( this->GetMemoryMapping() && this->pInputStream->CanBeMapped() ) 
			{
				this->bIsMapped = true;
				this->pMapCursor = static_cast<u8*>(this->pInputStream->Map());
				this->pMapEnd = this->pMapCursor + this->pInputStream->GetSize();
			} 
			else 
			{
				this->bIsMapped = false;
			}
			return true;
		}
		return false;
	}

	void BinaryReader::Close( void )
	{
		/// Close basic stream reader
		StreamReader::Close();
		/// Make nullfied everything
		this->bIsMapped = false;
		this->pMapCursor = nullptr;
		this->pMapEnd = nullptr;
	}

	s8 BinaryReader::ReadByte( void )
	{
		s8 data;

		if( this->bIsMapped ) 
		{
			/// Assertion
			NDKAssert2( this->pMapCursor < this->pMapEnd );
			// Get result 
			data = *this->pMapCursor++;
		} 
		else 
		{
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return data;
	}

	u8 BinaryReader::ReadUByte( void )
	{
		u8 data;

		if( this->bIsMapped ) 
		{
			/// Assertion
			NDKAssert2( this->pMapCursor < this->pMapEnd );
			data = *this->pMapCursor++;
		} 
		else 
		{
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return data;
	}

	s16 BinaryReader::ReadShort( void )
	{
		s16 data;

		if( this->bIsMapped ) 
		{
			/// Setup mapped data
			NDKAssert2( (this->pMapCursor + sizeof( s16 )) <= this->pMapEnd );
			/// Copy all data into memory block
			Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, &data, sizeof( data ) );
			/// Resetup mapper cursor
			this->pMapCursor += sizeof( data );
		} 
		else
		{
			/// Get data as always
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return Core::ByteOrder::Convert< s16 >( this->mByteOrder, Core::ByteOrder::HostMachine, data );
	}

	u16 BinaryReader::ReadUShort( void )
	{
		u16 data;

		if( this->bIsMapped ) 
		{
			/// Setup mapped data
			NDKAssert2( (this->pMapCursor + sizeof( u16 )) <= this->pMapEnd );
			/// Copy all data into memory block
			Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, &data, sizeof( data ) );
			/// Resetup mapper cursor
			this->pMapCursor += sizeof( data );
		} 
		else 
		{
			/// Get data as always
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return Core::ByteOrder::Convert< u16 >( this->mByteOrder, Core::ByteOrder::HostMachine, data ); 
	}

	s32 BinaryReader::ReadInteger( void )
	{
		s32 data;

		if( this->bIsMapped ) 
		{
			/// Setup mapped data
			NDKAssert2( (this->pMapCursor + sizeof( s32 )) <= this->pMapEnd );
			/// Copy all data into memory block
			Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, &data, sizeof( data ) );
			/// Resetup mapper cursor
			this->pMapCursor += sizeof( data );
		} 
		else 
		{
			/// Get data as always
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return Core::ByteOrder::Convert< s32 >( this->mByteOrder, Core::ByteOrder::HostMachine, data );
	}

	u32 BinaryReader::ReadUInteger( void )
	{
		u32 data;

		if( this->bIsMapped ) 
		{
			/// Setup mapped data
			NDKAssert2( (this->pMapCursor + sizeof( u32 )) <= this->pMapEnd );
			/// Copy all data into memory block
			Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, &data, sizeof( data ) );
			/// Resetup mapper cursor
			this->pMapCursor += sizeof( data );
		} 
		else 
		{
			/// Get data as always
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return Core::ByteOrder::Convert< u32 >( this->mByteOrder, Core::ByteOrder::HostMachine, data );
	}

	float BinaryReader::ReadFloat( void )
	{
		float data;

		if( this->bIsMapped ) 
		{
			/// Setup mapped data
			NDKAssert2( (this->pMapCursor + sizeof( float )) <= this->pMapEnd );
			/// Copy all data into memory block
			Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, &data, sizeof( data ) );
			/// Resetup mapper cursor
			this->pMapCursor += sizeof( data );
		} 
		else
		{
			/// Get data as always
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return Core::ByteOrder::Convert< float >( this->mByteOrder, Core::ByteOrder::HostMachine, data );
	}

	double BinaryReader::ReadDouble( void )
	{
		double data;

		if( this->bIsMapped ) 
		{
			/// Setup mapped data
			NDKAssert2( (this->pMapCursor + sizeof( double )) <= this->pMapEnd );
			/// Copy all data into memory block
			Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, &data, sizeof( data ) );
			/// Resetup mapper cursor
			this->pMapCursor += sizeof( data );
		} 
		else 
		{
			/// Get data as always
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return Core::ByteOrder::Convert< double >( this->mByteOrder, Core::ByteOrder::HostMachine, data );
	}

	bool BinaryReader::ReadBoolean( void )
	{
		bool data;

		if( this->bIsMapped ) 
		{
			/// Setup mapped data
			NDKAssert2( (this->pMapCursor + sizeof( bool )) <= this->pMapEnd );
			/// Copy all data into memory block
			Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, &data, sizeof( data ) );
			/// Resetup mapper cursor
			this->pMapCursor += sizeof( data );
		} 
		else 
		{
			/// Get data as always
			this->pInputStream->Read( &data, sizeof( data ) );
		}

		return data;
	}

	STL::String< Char8 > BinaryReader::ReadCharString( void )
	{
		STL::String< Char8 > data;
		u16 strLength = this->ReadUShort();

		if( this->bIsMapped ) 
		{
			/// Assertion
			NDKAssert2( (this->pMapCursor + strLength) <= this->pMapEnd );
			/// Do any string data copying if size higher than nothing, e.g. zero
			if( strLength > NULL ) 
			{
				data.Reserve( strLength + 1 );
				u8* buffer = (u8*)data.AsCharPtr();
				Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, buffer, strLength );
				this->pMapCursor += strLength;
				buffer[strLength] = NULL;
			}
		} 
		else 
		{
			if( strLength > NULL ) 
			{
				data.Reserve( strLength + 1 );
				u8* buffer = (u8*)data.AsCharPtr();
				this->pInputStream->Read( buffer, strLength );
				buffer[strLength] = NULL;
			}
		}

		return data;
	}

	void BinaryReader::ReadRawData( void* ptr, FileSystem::Size sz )
	{
		/// Error checking
		NDKAssert2( (ptr != 0x0) && (sz > NULL) );

		if( this->bIsMapped ) 
		{
			/// Setup mapped data
			NDKAssert2( (this->pMapCursor + sz) <= this->pMapEnd );
			/// Copy all data into memory block
			Memory::LowLevelMemory::CopyMemoryBlock( this->pMapCursor, ptr, sz );
			/// Resetup mapper cursor
			this->pMapCursor += sz;
		} 
		else 
		{
			this->pInputStream->Read( ptr, sz );
		} 
	}
}