/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#ifndef _NanoBinaryReader_h_
#define _NanoBinaryReader_h_

/// Stream
#include <Foundation/StreamReader.h>
/// Byte ordering
#include <Foundation/ByteOrder.h>
/// STL string
#include <Foundation/STLString.h>

/// FileSystem sub-system namespace
namespace FileSystem
{
	/// <summary>
	/// <c>BinaryReader</c> is an interface to read binary data from a stream. Optionally the
    /// reader can use memory mapping for optimal read performance. Performs
    /// automatic byte order conversion if necessary.
	/// </summary>
	class BinaryReader : public StreamReader
	{
		bool bEnableMapping; //!< Mapping enabled indicator
		bool bIsMapped; //!< Mapped indicator
		Enumeration< SizeT, Core::ByteOrder::Ordering > mByteOrder; //!< Byte ordering enumeration
		u8* pMapCursor; //!< Where do we stay at
		u8* pMapEnd; //!< End of mapped data

	public:
		/// <summary cref="BinaryReader::BinaryReader">
		/// Constructor.
		/// </summary>
		BinaryReader( void );

		/// <summary cref="BinaryReader::~BinaryReader">
		/// Destructor.
		/// </summary>
		virtual ~BinaryReader( void );

		void SetStreamOrdering( Core::ByteOrder::Ordering order );
		Core::ByteOrder::Ordering GetStreamOrdering( void ) const;

		void SetMemoryMapping( bool b );
		bool GetMemoryMapping( void ) const;

		virtual bool Open( void );
		virtual void Close( void );

		s8 ReadByte( void );
		u8 ReadUByte( void );
		s16 ReadShort( void );
		u16 ReadUShort( void );
		s32 ReadInteger( void );
		u32 ReadUInteger( void );
		float ReadFloat( void );
		double ReadDouble( void );
		bool ReadBoolean( void );
		STL::String< Char8 > ReadCharString( void );
		STL::String< Char16 > ReadWCharString( void );
		void ReadRawData( void* ptr, FileSystem::Size numBytes );
	};

	__IME_INLINED void BinaryReader::SetStreamOrdering( Core::ByteOrder::Ordering order )
	{
		this->mByteOrder = order;
	}

	__IME_INLINED Core::ByteOrder::Ordering BinaryReader::GetStreamOrdering( void ) const
	{
		Core::ByteOrder::Ordering result = this->mByteOrder;
		return result;
	}

	__IME_INLINED void BinaryReader::SetMemoryMapping( bool b )
	{
		this->bEnableMapping = b;
	}

	__IME_INLINED bool BinaryReader::GetMemoryMapping( void ) const
	{
		bool result = this->bEnableMapping;
		return result;
	}
}

#endif /// _NanoBinaryReader_h_