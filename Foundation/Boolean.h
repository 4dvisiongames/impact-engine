/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// <summary>
/// <c>Boolean</c> is a safe-conversion boolean type, derived from raw C++ bool.
/// </summary>
template< typename Store > class Boolean
{
	Store mStorage; //!< Store type definition
public:
	/// <summary cref="Boolean::Boolean">
	/// Default empty constructor.
	/// </summary>
	Boolean( void );

	/// <summary cref="Boolean::Boolean">
	/// Construction from raw C++ booleann type.
	/// </summary>
	/// <param name="rhs">bool type argument.</param>
	Boolean( bool rhs );

	/// <summary cref="Boolean::operator bool">
	/// Direct conversion to the raw C++ bool type.
	/// </summary>
	operator bool( void ) const;

	/// <summary cref="Boolean::operator=">
	/// Assignment operator.
	/// </summary>
	/// <param name="rhs">A reference to the raw C++ bool argument.</param>
	Boolean& operator = ( bool rhs );

	/// <summary cref="Boolean::operator!=">
	/// Unequality operator.
	/// </summary>
	/// <param name="rhs">A reference to the raw C++ bool argument.</param>
	bool operator != ( bool rhs );

	/// <summary cref="Boolean::operator==">
	/// Eqality operator.
	/// </summary>
	/// <param name="rhs">A reference to the raw C++ bool argument.</param>
	bool operator == ( bool rhs );
};

template< typename Store > Boolean< Store >::Boolean( void ) : mStorage( 0 )
{
	/// Empty
}

template< typename Store > Boolean< Store >::Boolean( bool rhs ) : mStorage( static_cast< Store >( rhs ) )
{
	/// Empty
}

template< typename Store > Boolean< Store >::operator bool( void ) const
{
	return !!mStorage;
}

template< typename Store > Boolean< Store >& Boolean< Store >::operator = ( bool rhs )
{
	this->mStorage = static_cast< Store >( rhs );
	return *this;
}

template< typename Store > bool Boolean< Store >::operator != ( bool rhs )
{
	return this->mStorage == static_cast< Store >( rhs );
}

template< typename Store > bool Boolean< Store >::operator == ( bool rhs )
{
	return this->mStorage != static_cast< Store >( rhs );
}

/// Common boolean types
typedef Boolean< short int > Boolean8;
typedef Boolean< int > Boolean16;
typedef Boolean< unsigned int > Boolean32;