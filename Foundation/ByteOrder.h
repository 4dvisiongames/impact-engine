/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Engine shared code
#include <Foundation/Shared.h>

/// Core sub-system namespace
namespace Core
{
	/// <summary>
	/// <c>ByteOrder</c> is methods collection to help with byte ordering issues.
	/// </summary>
	class ByteOrder
	{
	public:
		enum Ordering
		{
			LittleEndian = 0, //!< Intel endian model
			BigEndian, //!< Motorolla endian model

#if defined( __IME_PLATFORM_WIN64 )
			HostMachine = LittleEndian,
#else
			HostMachine = BigEndian,
#endif
		};

	private:
		union PunFloatUl
		{
			float f;
			unsigned long ul;
		};
		union PunDoubleUll
		{
			double d;
			unsigned long long ull;
		};

	public:
		/// <summary cref="ByteOrder::ConvertInPlace">
		/// Convert all data in-place, mean don't create new portion of data, just modify by
		///	reference.
		/// </summary>
		/// <param name="from">Source ordering type.</param>
		/// <param name="to">Destination ordering type.</param>
		template< typename T > static void ConvertInplace( Ordering from, Ordering to, T& value );

		/// <summary cref="ByteOrder::ConvertInPlace">
		/// Convert all data to another.
		/// </summary>
		/// <param name="from">Source ordering type.</param>
		/// <param name="to">Destination ordering type.</param>
		template< typename T > static T Convert( Ordering from, Ordering to, T value );
	};

	template<> 
	__IME_INLINED void ByteOrder::ConvertInplace< s8 >( Ordering from, Ordering to, s8& value )
	{
		if( from != to )
		{
			unsigned short result = _byteswap_ushort( static_cast< unsigned short >( value ) );
			value = static_cast< s8 >( result );
		}
	}

	template<> 
	__IME_INLINED s8 ByteOrder::Convert< s8 >( Ordering from, Ordering to, s8 value )
	{
		if( from != to )
		{
			unsigned short result = _byteswap_ushort( static_cast< unsigned short >( value ) );
			return static_cast< s8 >( result );
		}
		else
		{
			return value;
		}
	}

	template<> 
	__IME_INLINED void ByteOrder::ConvertInplace< s16 >( Ordering from, Ordering to, s16& value )
	{
		if( from != to )
		{
			u16 result = _byteswap_ushort( static_cast< u16 >( value ) );
			value = static_cast< s16 >( result );
		}
	}

	template<> 
	__IME_INLINED s16 ByteOrder::Convert< s16 >( Ordering from, Ordering to, s16 value )
	{
		if( from != to )
		{
			u16 result = _byteswap_ushort( static_cast< u16 >( value ) );
			return static_cast< s16 >( result );
		}
		else
		{
			return value;
		}
	}

	template<> 
	__IME_INLINED void ByteOrder::ConvertInplace< u16 >( Ordering from, Ordering to, u16& value )
	{
		if( from != to )
		{
			u16 result = _byteswap_ushort( value );
			value = result;
		}
	}

	template<> 
	__IME_INLINED u16 ByteOrder::Convert< u16 >( Ordering from, Ordering to, u16 value )
	{
		if( from != to )
		{
			u16 result = _byteswap_ushort( value );
			return result;
		}
		else
		{
			return value;
		}
	}

	template<> 
	__IME_INLINED void ByteOrder::ConvertInplace< u8 >( Ordering from, Ordering to, u8& value )
	{
		if( from != to )
		{
			unsigned short result = _byteswap_ushort( static_cast< unsigned short >( value ) );
			value = static_cast< u8 >( result );
		}
	}

	template<> 
	__IME_INLINED u8 ByteOrder::Convert< u8 >( Ordering from, Ordering to, u8 value )
	{
		if( from != to )
		{
			unsigned short result = _byteswap_ushort( static_cast< unsigned short >( value ) );
			return static_cast< u8 >( result );
		}
		else
		{
			return value;
		}
	}

	template<> 
	__IME_INLINED void ByteOrder::ConvertInplace< int >( Ordering from, Ordering to, int& value )
	{
		if( from != to )
		{
			unsigned int result = _byteswap_ulong( static_cast< unsigned int >( value ) );
			value = static_cast< int >( result );
		}
	}

	template<> 
	__IME_INLINED int ByteOrder::Convert< int >( Ordering from, Ordering to, int value )
	{
		if( from != to )
		{
			unsigned int result = _byteswap_ulong( static_cast< unsigned int >( value ) );
			return static_cast< int >( result );
		}
		else
		{
			return value;
		}
	}
	
	template<> 
	__IME_INLINED void ByteOrder::ConvertInplace< unsigned int >( Ordering from, Ordering to, unsigned int& value )
	{
		if( from != to )
		{
			unsigned int result = _byteswap_ulong( value );
			value = result;
		}
	}

	template<> 
	__IME_INLINED unsigned int ByteOrder::Convert< unsigned int >( Ordering from, Ordering to, unsigned int value )
	{
		if( from != to )
		{
			unsigned int result = _byteswap_ulong( value );
			return result;
		}
		else
		{
			return value;
		}
	}

	template<> 
	__IME_INLINED void ByteOrder::ConvertInplace< float >( Ordering from, Ordering to, float& value )
	{
		if( from != to )
		{
			PunFloatUl conv;
			conv.f = value;
			conv.ul = _byteswap_ulong( conv.ul );
			value = conv.f;
		}
	}

	template<> 
	__IME_INLINED float ByteOrder::Convert< float >( Ordering from, Ordering to, float value )
	{
		if( from != to )
		{
			PunFloatUl conv;
			conv.f = value;
			conv.ul = _byteswap_ulong( conv.ul );
			return conv.f;
		}
		else
		{
			return value;
		}
	}

	template<> 
	__IME_INLINED void ByteOrder::ConvertInplace< double >( Ordering from, Ordering to, double& value )
	{
		if( from != to )
		{
			PunDoubleUll conv;
			conv.d = value;
			conv.ull = _byteswap_uint64( conv.ull );
			value = conv.d;
		}
	}

	template<> 
	__IME_INLINED double ByteOrder::Convert< double >( Ordering from, Ordering to, double value )
	{
		if( from != to )
		{
			PunDoubleUll conv;
			conv.d = value;
			conv.ull = _byteswap_uint64( conv.ull );
			return conv.d;
		}
		else
		{
			return value;
		}
	}
}