/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

//! Memory stream object
#include <Foundation/MemoryStream.h>
//! Memory stream writer
#include <Foundation/StreamWriter.h>

//! Kernel namespace
namespace Kernel
{
	//! Command compiler object system, which compiles list of commands into memory block.
	//! That list can be used further then in
	class CommandCompiler
	{
		//! Memory block that used for command compilation
		IntrusivePtr< FileSystem::StreamWriter > myStreamWriter;

	public:
		CommandCompiler( void );
		~CommandCompiler( void );

		//! Start commands compilation
		bool Begin( IntrusivePtr< FileSystem::MemoryStream >& writer, 
					bool restartOnAttach = true );
		//! Signal end of commands compilation
		bool End( void );
		//! Validation hint
		bool IsValid( void ) const;
		//! Restart position of command stream
		void RestartCommandStream( void );

		//! Writes pointer into compilation memory block
		template< typename T >
		bool Write( T value ) const;

		//! Writes pointer into compilation memory block
		template< typename T >
		bool Write( T value );
	};

	__IME_INLINED bool CommandCompiler::IsValid( void ) const
	{
		return this->myStreamWriter.IsValid();
	}

	__IME_INLINED void CommandCompiler::RestartCommandStream( void )
	{
		if( this->myStreamWriter.IsValid() ) 
		{
			if( this->myStreamWriter->IsOpen() )
				this->myStreamWriter->GetInput()->Seek( 0, FileSystem::SeekOrigin::Begin );
		}
	}

	template< typename T >
	__IME_INLINED bool CommandCompiler::Write( T value ) const
	{
		if( !this->myStreamWriter->HasStream() )
			return false;

		if( !this->myStreamWriter->IsOpen() )
			return false;

		IntrusivePtr< FileSystem::MemoryStream > myStream = this->myStreamWriter->GetInput().Cast< FileSystem::MemoryStream >();
		myStream->Write( &value, sizeof( T ) );

		return true;
	}

	template< typename T >
	__IME_INLINED bool CommandCompiler::Write( T value )
	{
		if( !this->myStreamWriter->HasStream() )
			return false;

		if( !this->myStreamWriter->IsOpen() )
			return false;

		FileSystem::Size size = sizeof( T );
		IntrusivePtr< FileSystem::MemoryStream > myStream = this->myStreamWriter->GetInput().Cast< FileSystem::MemoryStream >();
		myStream->Write( &value, size );

		return true;
	}
}