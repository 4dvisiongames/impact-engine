/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/** This header is supposed to contain macro definitions and C style comments only.
    The macros defined here are intended to control such aspects of Impact Engine build as
    - presence of compiler features
    - compilation modes
    - feature sets
    - known compiler/platform issues
**/

#define __IME_GCC_VERSION (__GNUC__ * 10000 + __GNUC_MINOR__ * 100 + __GNUC_PATCHLEVEL__)

#if __clang__
    /**according to clang documentation version can be vendor specific **/
#	define __IME_CLANG_VERSION (__clang_major__ * 10000 + __clang_minor__ * 100 + __clang_patchlevel__)
#endif

/** Presence of compiler features **/

#if __INTEL_COMPILER == 9999 && __INTEL_COMPILER_BUILD_DATE == 20110811
	/* Intel(R) Composer XE 2011 Update 6 incorrectly sets __INTEL_COMPILER. Fix it. */
#	undef __INTEL_COMPILER
#	define __INTEL_COMPILER 1210
#endif

#if (__IME_GCC_VERSION >= 40400) && !defined(__INTEL_COMPILER)
	/** warning suppression pragmas available in GCC since 4.4 **/
#	define __IME_GCC_WARNING_SUPPRESSION_PRESENT 1
#endif

/* Select particular features of C++11 based on compiler version.
   ICC 12.1 (Linux), GCC 4.3 and higher, clang 2.9 and higher
   set __GXX_EXPERIMENTAL_CXX0X__ in c++11 mode.

   Compilers that mimics other compilers (ICC, clang) must be processed before
   compilers they mimic (GCC, MSVC).

   TODO: The following conditions should be extended when new compilers/runtimes
   support added.
 */

#if __INTEL_COMPILER
#	define __IME_INLINED							 __forceinline
#	define __IME_NO_INLINE							 __declspec(noinline)
#	define __IME_ALIGNED_MSVC( a )					 __declspec( align( a ) )
#	define __IME_ALIGNED_GCC( a )
#	define __IME_ALIGN_OF( a )						 __alignof( a )

    /** On Windows environment when using Intel C++ compiler with Visual Studio 2010*,
        the C++0x features supported by Visual C++ 2010 are enabled by default
        TODO: find a way to get know if c++0x mode is specified in command line on windows **/
#	define __IME_CPP11_VARIADIC_TEMPLATES_PRESENT    ( __VARIADIC_TEMPLATES && (__GXX_EXPERIMENTAL_CXX0X__ || _MSC_VER) )
#	define __IME_CPP11_RVALUE_REF_PRESENT            ( (__GXX_EXPERIMENTAL_CXX0X__ || _MSC_VER >= 1600) && (__INTEL_COMPILER >= 1200) )

#	if _MSC_VER >= 1600
#		define __IME_EXCEPTION_PTR_PRESENT           ( __INTEL_COMPILER > 1300                                                \
														/*ICC 12.1 Upd 10 and 13 beta Upd 2 fixed exception_ptr linking  issue*/ \
														|| (__INTEL_COMPILER == 1300 && __INTEL_COMPILER_BUILD_DATE >= 20120530) \
														|| (__INTEL_COMPILER == 1210 && __INTEL_COMPILER_BUILD_DATE >= 20120410) )
    /** libstc++ that comes with GCC 4.6 use C++11 features not supported by ICC 12.1.
     * Because of that ICC 12.1 does not support C++11 mode with with gcc 4.6. (or higher)
     * , and therefore does not  define __GXX_EXPERIMENTAL_CXX0X__ macro**/
#	elif (__IME_GCC_VERSION >= 40404) && (__TBB_GCC_VERSION < 40600)
#		define __IME_EXCEPTION_PTR_PRESENT           ( __GXX_EXPERIMENTAL_CXX0X__ && __INTEL_COMPILER >= 1200 )
#	elif (__IME_GCC_VERSION >= 40600)
#		define __IME_EXCEPTION_PTR_PRESENT           ( __GXX_EXPERIMENTAL_CXX0X__ && __INTEL_COMPILER >= 1300 )
#	else
#		define __IME_EXCEPTION_PTR_PRESENT           0
#	endif

#	define __IME_MAKE_EXCEPTION_PTR_PRESENT          (_MSC_VER >= 1700 || (__GXX_EXPERIMENTAL_CXX0X__ && __TBB_GCC_VERSION >= 40600))
#	define __IME_STATIC_ASSERT_PRESENT               ( __GXX_EXPERIMENTAL_CXX0X__ || (_MSC_VER >= 1600) )
#	define __IME_CPP11_TUPLE_PRESENT                 ( (_MSC_VER >= 1600) || ((__GXX_EXPERIMENTAL_CXX0X__) && (__TBB_GCC_VERSION >= 40300)) )
    /** TODO: re-check for compiler version greater than 12.1 if it supports initializer lists**/
#	define __IME_INITIALIZER_LISTS_PRESENT           0
#	define __IME_CONSTEXPR_PRESENT                   0
#	define __IME_DEFAULTED_AND_DELETED_FUNC_PRESENT  0

#elif __clang__
#	define __IME_INLINED							 inline __attribute((always_inline))
#	define __IME_NO_INLINE							 __attribute__((noinline))

	//TODO: these options need to be rechecked
	/** on OS X* the only way to get C++11 is to use clang. For library features (e.g. exception_ptr) libc++ is also
	 *  required. So there is no need to check GCC version for clang**/
#	define __IME_CPP11_VARIADIC_TEMPLATES_PRESENT     __has_feature(__cxx_variadic_templates__)
#	define __IME_CPP11_RVALUE_REF_PRESENT             __has_feature(__cxx_rvalue_references__)
#	define __IME_EXCEPTION_PTR_PRESENT               (__GXX_EXPERIMENTAL_CXX0X__ && (__cplusplus >= 201103L))
#	define __IME_MAKE_EXCEPTION_PTR_PRESENT          (__GXX_EXPERIMENTAL_CXX0X__ && (__cplusplus >= 201103L))
#	define __IME_STATIC_ASSERT_PRESENT               __has_feature(__cxx_static_assert__)

    /**Clang (preprocessor) has problems with dealing with expression having __has_include in #if's
     * used inside C++ code. (At least version that comes with OS X 10.8) **/
#	if (__GXX_EXPERIMENTAL_CXX0X__ && __has_include(<tuple>))
#		define __IME_CPP11_TUPLE_PRESENT             1
#	endif
#	if (__has_feature(__cxx_generalized_initializers__) && __has_include(<initializer_list>))
#		define __IME_INITIALIZER_LISTS_PRESENT       1
#	endif

#	define __IME_CONSTEXPR_PRESENT                   __has_feature(__cxx_constexpr__)
#	define __IME_DEFAULTED_AND_DELETED_FUNC_PRESENT  (__has_feature(__cxx_defaulted_functions__) && __has_feature(__cxx_deleted_functions__))

#elif __GNUC__
#	define __IME_INLINED							 inline __attribute((always_inline))
#	define __IME_NO_INLINE							 __attribute__((noinline))
#	define __IME_ALIGNED_MSVC( a )
#	define __IME_ALIGNED_GCC( a )					 __attribute__((aligned(a)))
#	define __IME_ALIGN_OF( a )						 __alignof__( a )

#	define __IME_CPP11_VARIADIC_TEMPLATES_PRESENT    __GXX_EXPERIMENTAL_CXX0X__
#	define __IME_CPP11_RVALUE_REF_PRESENT            __GXX_EXPERIMENTAL_CXX0X__
    /** __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4 here is a substitution for _GLIBCXX_ATOMIC_BUILTINS_4, which is a prerequisite 
        for exception_ptr but cannot be used in this file because it is defined in a header, not by the compiler. 
        If the compiler has no atomic intrinsics, the C++ library should not expect those as well. **/
#	define __IME_EXCEPTION_PTR_PRESENT               ((__GXX_EXPERIMENTAL_CXX0X__) && (__TBB_GCC_VERSION >= 40404) && __GCC_HAVE_SYNC_COMPARE_AND_SWAP_4)
#	define __IME_MAKE_EXCEPTION_PTR_PRESENT          ((__GXX_EXPERIMENTAL_CXX0X__) && (__TBB_GCC_VERSION >= 40600))
#	define __IME_STATIC_ASSERT_PRESENT               ((__GXX_EXPERIMENTAL_CXX0X__) && (__TBB_GCC_VERSION >= 40300))
#	define __IME_CPP11_TUPLE_PRESENT                 ((__GXX_EXPERIMENTAL_CXX0X__) && (__TBB_GCC_VERSION >= 40300))
#	define __IME_INITIALIZER_LISTS_PRESENT           ((__GXX_EXPERIMENTAL_CXX0X__) && (__TBB_GCC_VERSION >= 40400))
    /** gcc seems have to support constexpr from 4.4 but tests in (test_atomic) seeming reasonable fail to compile prior 4.6**/
#	define __IME_CONSTEXPR_PRESENT                   ((__GXX_EXPERIMENTAL_CXX0X__) && (__TBB_GCC_VERSION >= 40400))
#	define __IME_DEFAULTED_AND_DELETED_FUNC_PRESENT  ((__GXX_EXPERIMENTAL_CXX0X__) && (__TBB_GCC_VERSION >= 40400))

#elif _MSC_VER
#	define __IME_INLINED							 __forceinline
#	define __IME_NO_INLINE							 __declspec(noinline)
#	define __IME_ALIGNED_GCC( a )
#	define __IME_ALIGNED_MSVC( a )				     __declspec( align( a ) )
#	define __IME_ALIGN_OF( a )						 __alignof( a )
#	define __IME_CONDITION_LIKELY( a )				 (a)
#	define __IME_READ_AS_ALIGNED( ptr, align )		 ptr

#	define __IME_CPP11_VARIADIC_TEMPLATES_PRESENT    0
#	define __IME_CPP11_RVALUE_REF_PRESENT            0
#	define __IME_CPP11_NEW_PLACEMENT				 (_MSC_VER >= 1900)
#	define __IME_EXCEPTION_PTR_PRESENT               (_MSC_VER >= 1600)
#	define __IME_STATIC_ASSERT_PRESENT               (_MSC_VER >= 1600)
#	define __IME_MAKE_EXCEPTION_PTR_PRESENT          (_MSC_VER >= 1700)
#	define __IME_CPP11_TUPLE_PRESENT                 (_MSC_VER >= 1600)
#	define __IME_CPP11_MOVE_SEMANTICS_PRESENT		(_MSC_VER >= 1600)
#	define __IME_INITIALIZER_LISTS_PRESENT           0
#	define __IME_CONSTEXPR_PRESENT                   0
#	define __IME_DEFAULTED_AND_DELETED_FUNC_PRESENT  0

#else
#	define __IME_INLINED
#	define __IME_NO_INLINE
#	define __IME_ALIGN_OF( a )
#	define __IME_CONDITION_LIKELY( a )				 (a)

#	define __IME_CPP11_VARIADIC_TEMPLATES_PRESENT    0
#	define __IME_CPP11_RVALUE_REF_PRESENT            0
#	define __IME_EXCEPTION_PTR_PRESENT               0
#	define __IME_STATIC_ASSERT_PRESENT               0
#	define __IME_MAKE_EXCEPTION_PTR_PRESENT          0
#	define __IME_CPP11_TUPLE_PRESENT                 0
#	define __IME_CPP11_MOVE_SEMANTICS_PRESENT		0
#	define __IME_INITIALIZER_LISTS_PRESENT           0
#	define __IME_CONSTEXPR_PRESENT                   0
#	define __IME_DEFAULTED_AND_DELETED_FUNC_PRESENT  0

#endif

#	define __IME_READ_AS_ALIGNED_CTF( ptr ) __IME_READ_AS_ALIGNED( ptr, __IME_ALIGN_OF(ptr) )

#if __GNUC__ || __SUNPRO_CC || __IBMCPP__
    /* ICC defines __GNUC__ and so is covered */
#	define __IME_ATTRIBUTE_ALIGNED_PRESENT 1
#elif _MSC_VER && (_MSC_VER >= 1300 || __INTEL_COMPILER)
#	define __IME_DECLSPEC_ALIGN_PRESENT 1
#endif

/* Actually ICC supports gcc __sync_* intrinsics starting 11.1,
 * but 64 bit support for 32 bit target comes in later ones*/
/* TODO: change the version back to 4.1.2 once macro __TBB_WORD_SIZE become optional */
#if (__IME_GCC_VERSION >= 40306) || (__INTEL_COMPILER >= 1200)
    /** built-in atomics available in GCC since 4.1.2 **/
#	define __IME_GCC_BUILTIN_ATOMICS_PRESENT 1
#endif

#if (__INTEL_COMPILER >= 1210)
    /** built-in C++11 style atomics available in compiler since 12.1 **/
#	define __IME_ICC_BUILTIN_ATOMICS_PRESENT 1
#endif

#ifndef __IME_USE_DEBUG
#	ifdef _DEBUG
#	define __IME_DO_ASSERT _DEBUG
#		ifdef __IME_DO_ASSERT
#		define __IME_USE_DEBUG __IME_DO_ASSERT
#		endif
#	else
#	define __IME_DO_ASSERT 0
#	endif
#endif

#ifndef __IME_USE_ASSERT
#	define __IME_USE_ASSERT __IME_DO_ASSERT
#endif

#ifndef __IME_USE_THREADING_TOOLS
#	ifdef __IME_DO_THREADING_TOOLS
#	define __IME_USE_THREADING_TOOLS __IME_DO_THREADING_TOOLS
#else
#	define __IME_USE_THREADING_TOOLS __IME_USE_DEBUG
#	endif
#endif

#ifndef __IME_USE_PERFORMANCE_WARNINGS
#	ifdef __IME_PERFORMANCE_WARNINGS
#	define __IME_USE_PERFORMANCE_WARNINGS __IME_PERFORMANCE_WARNINGS
#else
#	define __IME_USE_PERFORMANCE_WARNINGS __IME_USE_DEBUG
#	endif
#endif

/** Check whether the request to use GCC atomics can be satisfied **/
#if (__IME_USE_GCC_BUILTINS && !__IME_GCC_BUILTIN_ATOMICS_PRESENT)
#	error "GCC atomic built-ins are not supported."
#endif

// Define preprocessor symbols used to determine architecture
#if _WIN32 || _WIN64
#   if defined(_M_X64)||defined(__x86_64__) || defined(_M_AMD64)  // the latter for MinGW support
#       define __IME_x86_64 1
#   elif defined(_M_IA64)
#       define __IME_ipf 1
#   elif defined(_M_IX86)||defined(__i386__) // the latter for MinGW support
#       define __IME_x86_32 1
#   endif

#else /* Assume generic Unix */
#   if !__linux__ && !__APPLE__
#       define __IME_generic_os 1
#   endif
#   if __x86_64__
#       define __IME_x86_64 1
#   elif __ia64__
#       define __IME_ipf 1
#   elif __i386__||__i386  // __i386 is for Sun OS
#       define __IME_x86_32 1
#   else
#       define __IME_generic_arch 1
#   endif

#endif
/** Macros of the form __TBB_XXX_BROKEN denote known issues that are caused by
    the bugs in compilers, standard or OS specific libraries. They should be
    removed as soon as the corresponding bugs are fixed or the buggy OS/compiler
    versions go out of the support list.
**/

#if __ANDROID__ && __IME_GCC_VERSION <= 40403 && !__GCC_HAVE_SYNC_COMPARE_AND_SWAP_8
    /** Necessary because on Android 8-byte CAS and F&A are not available for some processor architectures,
        but no mandatory warning message appears from GCC 4.4.3. Instead, only a linkage error occurs when
        these atomic operations are used (such as in unit test test_atomic.exe). **/
#	define __IME_GCC_64BIT_ATOMIC_BUILTINS_BROKEN 1
#endif

#if __GNUC__ && __IME_x86_64 && __INTEL_COMPILER == 1200
#	define __IME_ICC_12_0_INL_ASM_FSTCW_BROKEN 1
#endif

#if _MSC_VER && __INTEL_COMPILER && (__INTEL_COMPILER<1110 || __INTEL_COMPILER==1110 && __INTEL_COMPILER_BUILD_DATE < 20091012)
    /** Necessary to avoid ICL error (or warning in non-strict mode):
        "exception specification for implicitly declared virtual destructor is
        incompatible with that of overridden one". **/
#	define __IME_DEFAULT_DTOR_THROW_SPEC_BROKEN 1
#endif

//TODO: recheck for different clang versions 
#if __GLIBC__==2 && __GLIBC_MINOR__==3 || __MINGW32__ || (__APPLE__ && (__clang__ || __INTEL_COMPILER==1200 && !__IME_USE_DEBUG))
    // Macro controlling EH usages in tests.
	//Some older versions of glibc crash when exception handling happens concurrently.
#	define __IME_THROW_ACROSS_MODULE_BOUNDARY_BROKEN 1
#else
#	define __IME_THROW_ACROSS_MODULE_BOUNDARY_BROKEN 0
#endif

#if (_WIN32 || _WIN64) && __INTEL_COMPILER == 1110
    // That's a bug in Intel compiler 11.1.044/IA-32/Windows, that leads to a worker thread crash on the thread's startup.
#	define __IME_INTELCOMPILER_11_1_WORKERTHREAD_WORKAROUND_BROKEN 1
#endif

#if __MINGW32__ && (__GNUC__<4 || __GNUC__==4 && __GNUC_MINOR__<2)
    // MinGW has a bug with stack alignment for routines invoked from MS RTLs.
	// Since GCC 4.2, the bug can be worked around via a special attribute.
#	define __IME_SSE_STACK_ALIGNMENT_BROKEN 1
#else
#	define __IME_SSE_STACK_ALIGNMENT_BROKEN 0
#endif

#if __GNUC__==4 && __GNUC_MINOR__==3 && __GNUC_PATCHLEVEL__==0
    /* GCC of this version may rashly ignore control dependencies */
#	define __TBB_GCC_OPTIMIZER_ORDERING_BROKEN 1
#endif

#if __FreeBSD__
    /** A bug in FreeBSD 8.0 results in kernel panic when there is contention
        on a mutex created with this attribute. **/
#	define __IME_PRIO_INHERIT_BROKEN 1

    /** A bug in FreeBSD 8.0 results in test hanging when an exception occurs
        during (concurrent?) object construction by means of placement new operator. **/
#	define __IME_PLACEMENT_NEW_EXCEPTION_SAFETY_BROKEN 1
#endif /* __FreeBSD__ */

#if (__linux__ || __APPLE__) && __i386__ && defined(__INTEL_COMPILER)
    /** The Intel compiler for IA-32 (Linux|OS X) crashes or generates
        incorrect code when __asm__ arguments have a cast to volatile. **/
#	define __IME_ICC_ASM_VOLATILE_BROKEN 1
#endif

#if !__INTEL_COMPILER && (_MSC_VER || __GNUC__==3 && __GNUC_MINOR__<=2)
    // Bug in GCC 3.2 and MSVC compilers that sometimes return 0 for __alignof(Type)
	// when <c>Type</c> has not yet been instantiated.
#	define __IME_ALIGNOF_NOT_INSTANTIATED_TYPES_BROKEN 1
#endif

// __IME_WIN8UI_SUPPORT enables support of New Windows*8 Store Apps and limit a possibility to load
// shared libraries at run time only from application container
#if defined(WINAPI_FAMILY) && WINAPI_FAMILY == WINAPI_FAMILY_APP
#	define __IME_WIN8UI_SUPPORT 1
#else
#	define __IME_WIN8UI_SUPPORT 0
#endif

#if __IME_x86_32 && (__linux__ || __APPLE__ || _WIN32 || __sun) &&  ((defined(__INTEL_COMPILER) && (__INTEL_COMPILER <= 1300)) || (__GNUC__==3 && __GNUC_MINOR__==3 ) || defined(__SUNPRO_CC))
    // Some compilers for IA-32 fail to provide 8-byte alignment of objects on the stack,
    // even if the object specifies 8-byte alignment.  On such platforms, the IA-32 implementation
    // of 64 bit atomics (e.g. atomic<long long>) use different tactics depending upon
    // whether the object is properly aligned or not.
#	define __IME_FORCE_64BIT_ALIGNMENT_BROKEN 1
#else
#	define __IME_FORCE_64BIT_ALIGNMENT_BROKEN 0
#endif

#if (__IME_DEFAULTED_AND_DELETED_FUNC_PRESENT && (__IME_GCC_VERSION < 40700) && (!defined(__INTEL_COMPILER) && !defined (__clang__)))
#	define __IME_ZERO_INIT_WITH_DEFAULTED_CTOR_BROKEN 1
#endif

#define __TBB_ATOMIC_CTORS     (__IME_CONSTEXPR_PRESENT && __IME_DEFAULTED_AND_DELETED_FUNC_PRESENT && (!__IME_ZERO_INIT_WITH_DEFAULTED_CTOR_BROKEN))
#define __IME_DEFAULT_VEC_ALIGNMENT 16