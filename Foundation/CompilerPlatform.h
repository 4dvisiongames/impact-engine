/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

// _IME_PLATFORM_PTR_SIZE
// Platform pointer size; same as sizeof(void*).
// This is not the same as sizeof(int), as int is usually 32 bits on 
// even 64 bit platforms. 
//
// _WIN64 is defined by Win64 compilers, such as VC++.
// _M_IA64 is defined by VC++ and Intel compilers for IA64 processors.
// __LP64__ is defined by HP compilers for the LP64 standard.
// _LP64 is defined by the GCC and Sun compilers for the LP64 standard.
// __ia64__ is defined by the GCC compiler for IA64 processors.
// __arch64__ is defined by the Sparc compiler for 64 bit processors.
// __mips64__ is defined by the GCC compiler for MIPS processors.
// __powerpc64__ is defined by the GCC compiler for PowerPC processors.
// __64BIT__ is defined by the AIX compiler for 64 bit processors.
// __sizeof_ptr is defined by the ARM compiler (armcc, armcpp).
//
#ifndef __IME_PLATFORM_PTR_SIZE
    #if defined(__WORDSIZE) // Defined by some variations of GCC.
        #define __IME_PLATFORM_PTR_SIZE ((__WORDSIZE) / 8)
    #elif defined(_WIN64) || defined(__LP64__) || defined(_LP64) || defined(_M_IA64) || defined(__ia64__) || defined(__arch64__) || defined(__mips64__) || defined(__64BIT__) 
        #define __IME_PLATFORM_PTR_SIZE 8
    #elif defined(__CC_ARM) && (__sizeof_ptr == 8)
        #define __IME_PLATFORM_PTR_SIZE 8
    #else
        #define __IME_PLATFORM_PTR_SIZE 4
    #endif
#endif



// __IME_PLATFORM_WORD_SIZE
// This defines the size of a machine word. This will be the same as 
// the size of registers on the machine but not necessarily the same
// as the size of pointers on the machine. A number of 64 bit platforms
// have 64 bit registers but 32 bit pointers.
//
#ifndef __IME_PLATFORM_WORD_SIZE
   #if defined(__IME_PLATFORM_XBOX_ONE) || defined(NDKPlatformPS3) 
      #define __IME_PLATFORM_WORD_SIZE 8
   #else
      #define __IME_PLATFORM_WORD_SIZE __IME_PLATFORM_PTR_SIZE
   #endif
#endif



// Disabled until and unless deemed useful:
//
// Platform integer types
// These definitions allow us to define other things properly, such as 
// sized integer types. In order to bring some order to this chaos, 
// we follow a variation of the standard LP64 conventions defined at:
//    http://www.opengroup.org/public/tech/aspen/lp64_wp.htm 
//
#if defined(__IME_PLATFORM_LINUX64) || defined(__IME_PLATFORM_OSX_INTEL) || defined(__IME_PLATFORM_XBOX_ONE)
#	define __IME_PLATFORM_ILP32_LL64			// int, long, ptr = 32 bits; long long = 64 bits. 
#elif defined(NDKPlatformSun) || defined(NDKPlatformSGI)
#	define __IME_PLATFORM_I32_LLLP64		// int = 32 bits; long, long long, ptr = 64 bits.
#elif defined( __IME_PLATFORM_WIN64 )
#	if defined(__MWERKS__) || defined(__GNUC__)
#		define __IME_PLATFORM_I32_LLLP64	// int = 32 bits; long, long long, ptr = 64 bits.
#	else // MSVC
#		define __IME_PLATFORM_IL32_LLP64	// int, long = 32 bits; long long, ptr = 64 bits.
#	endif
#endif