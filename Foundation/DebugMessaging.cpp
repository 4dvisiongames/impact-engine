/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Stream writer
#include <Foundation/StreamWriter.h>
/// File stream
#include <Foundation/FileStream.h>
/// Spin wait
#include <Foundation/SpinWait.h>
/// Default scoped locker
#include <Foundation/ScopedLock.h>
/// System
#include <Foundation/System.h>
/// Debug messaging
#include <Foundation/DebugMessaging.h>

namespace Debug
{
	/// <summary>
	/// <c>SDebugLog</c> is a debugging object
	/// </summary>
	struct SDebugLog
	{
		Threading::SpinWait mCS; //!< Locks by the sending thread to pass data safety
		STL::String< Char8 > strLastLogged;

		/// Our file, where we write all logging information.
		IntrusivePtr< FileSystem::FileStream > pLogFileStream;
		IntrusivePtr< FileSystem::StreamWriter > pStreamWriter;

		/// Exception acceleration table
		STL::String< Char8 > exceptionCodes[EExceptionCodes::NumMaxExceptions];

		/// Last epoch used for logging
		uintptr_t myLastLogEpoch;
	};

	static SDebugLog* pDebugLog; //!< Debugger logging system

	void Messaging::Initialize( void )
	{
		__IME_ASSERT( !pDebugLog );

		/// Initialize debugger log
		pDebugLog = new SDebugLog;
		pDebugLog->myLastLogEpoch = 0;

		pDebugLog->exceptionCodes[0] = "FileSystem API Exception - Failed write to file";
		pDebugLog->exceptionCodes[1] = "Common Exception - Invalid engine state";
		pDebugLog->exceptionCodes[2] = "Common Exception - Invalid parameter passed";
		pDebugLog->exceptionCodes[3] = "Rendering API Exception - Renderer system error";
		pDebugLog->exceptionCodes[4] = "Physics API Exception - Physics system error";
		pDebugLog->exceptionCodes[5] = "Audio API Exception - Audio system error";
		pDebugLog->exceptionCodes[6] = "Common Exception - Duplicated item";
		pDebugLog->exceptionCodes[7] = "Common Exception - Item not found";
		pDebugLog->exceptionCodes[8] = "FileSystem API Exception - File not found";
		pDebugLog->exceptionCodes[9] = "Internal Error";
		pDebugLog->exceptionCodes[10] = "Not implemented";
		pDebugLog->exceptionCodes[11] = "Extension Framework Exception";

		/// Create logging
		pDebugLog->pLogFileStream = new FileSystem::FileStream;
		pDebugLog->pStreamWriter = new FileSystem::StreamWriter;
		/// Setup logger file stream name
		pDebugLog->pLogFileStream->SetPath( NDKOSPath( "engine.log" ) );
		/// Attach stream object
		pDebugLog->pStreamWriter->SetInput( pDebugLog->pLogFileStream.Downcast< FileSystem::Stream >() );
		pDebugLog->pStreamWriter->Open();
	}

	void Messaging::Shutdown( void )
	{
		__IME_ASSERT( pDebugLog );

		/// Close file, if all is OK.
		pDebugLog->pStreamWriter->Close();

		/// Release all used pointers
		pDebugLog->pStreamWriter = nullptr;
		pDebugLog->pLogFileStream = nullptr;

		delete pDebugLog;
	}

	const char* Messaging::ExceptionToString( const EExceptionCodes::List code )
	{
		return pDebugLog->exceptionCodes[code].AsCharPtr();
	}

	void Messaging::Send( EMessageType::List type, 
						  const char* message )
	{
		if( !pDebugLog )
			return;

		uintptr_t currentLogEpoch = Core::System::GetMilliseconds();

		Threading::ScopedLock< Threading::SpinWait > lock( pDebugLog->mCS );
		/// Reformat new message
		pDebugLog->strLastLogged.Assign( message );
		pDebugLog->strLastLogged.Append( "\r\n" );
		/// Add time of writing
		pDebugLog->pLogFileStream->Write( pDebugLog->strLastLogged.AsCharPtr(), pDebugLog->strLastLogged.Length() );
		
		if( (currentLogEpoch - pDebugLog->myLastLogEpoch) > 1500 )
			pDebugLog->pLogFileStream->Flush();

		pDebugLog->myLastLogEpoch = currentLogEpoch;

		/// Output to console
		if( Core::System::GetConsoleMode() )
			printf( "%s", pDebugLog->strLastLogged.AsCharPtr() );

		/// Throw error on fatals only
		if( type == EMessageType::FatalError )
			Core::System::Error(pDebugLog->strLastLogged.AsCharPtr());
	}

	FileSystem::FileStream* Messaging::GetLogStream() 
	{
		return pDebugLog->pLogFileStream;
	}
}