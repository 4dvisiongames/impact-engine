/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

namespace FileSystem
{
	class FileStream;
}

namespace Debug
{
	/// <summary>
	/// <c>EExceptionCodes</c> is an exception codes definition of enumeration list.
	/// </summary>
	struct EExceptionCodes
	{
		enum List
		{
			CannotWriteToFile,
			InvalidState,
			InvalidParams,
			RendererError,
			PhysicsError,
			AudioError,
			DuplicateItem,
			ItemNotFound,
			FileNotFonnd,
			InternalError,
			NotImplemented,
			ExtensionError,
			
			NumMaxExceptions
		};
	};

	/// <summary>
	/// <c>EMessageType</c> is a debugging message type enumeration list.
	/// </summary>
	struct EMessageType 
	{
		enum List 
		{
			Normal, //!< Normal message just to say what engine's done
			Warning, //!< Warning message to point on small problem, but engine will not stop working
			Error, //!< Error message to point on problems with content
			FatalError, //!< Fatal error message shut down engine processing and shows error to the user
		};
	};

	class Messaging
	{
	public:
		static void Initialize( void );
		static void Shutdown( void );

		/// <summary>
		/// <c>ExceptionToString</c> is a converted string.
		/// </summary>
		static const char* ExceptionToString( const EExceptionCodes::List code );

		/// <summary>
		/// <c>Send</c> function uses for debug message sending.
		/// </summary>
		static void Send( EMessageType::List type, 
						  const char* message );

		/// <summary cref="Messaging::GetLogStream">
		/// </summary>
		FileSystem::FileStream* GetLogStream();
	};
}

/// Core assertion, this assertion works always, because some of them
/// needed to be shown to the user
#define NDKAssert2(expr) ((expr) \
	? ((void)0) \
	: Debug::Messaging::Send( Debug::EMessageType::FatalError, STL::String< Char8 >::VA( "NanoEngine has been stopped because of assertion failure!\nExpression: %s\nFunction: %s\nSource code: %s \nLine: %u", #expr, __FUNCTION__, __FILE__, __LINE__ ) ))

/// Static assertion defintion
#define NDKStaticAssert( expr ) static_assert( expr, #expr )

/// Undef definition if exists
#ifdef NDKThrowException
#undef NDKThrowException
#endif
/// Exception definition.
#define NDKThrowException( num, desc, src ) \
		Debug::Messaging::Send( Debug::EMessageType::FatalError, STL::String< Char8 >::VA( "NanoEngine has been stopped because of exception!\nException code: %s\nDescription: %s\nFunction: %s\nSource code %s \nLine: %u", Debug::Messaging::ExceptionToString(num), desc, src, __FILE__, __LINE__ ) )