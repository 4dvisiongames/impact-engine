/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// <summary>
/// <c>Enumeration</c> is a specifical enumeration type definition. One main feature 
/// is type safetness so you can easily convert and compare different enumeations using this 
/// definitions.
/// </summary>
template< typename Store, typename Enum > class Enumeration
{
public:
	typedef Enumeration< Store, Enum > ThisType;
	typedef Store ValueType;
	typedef Enum EnumerationType;

	/// <summary cref="Enumeration::Enumeration">
	/// Default empty constructor.
	/// </summary>
	Enumeration( void );

	/// <summary cref="Enumeration::Enumeration">
	/// Copy constructor.
	/// </summary>
	Enumeration( const ThisType& rhs );

	/// <summary cref="Enumeration::Enumeration">
	/// Default constructor.
	/// </summary>
	/// <param name="rhs">A reference to the raw C++ enumeration.</param>
	Enumeration( EnumerationType e );

	/// <summary cref="Enumeration::operator bool">
	/// Conversion operator to the raw C++ from NanoEngine's enumeration.
	/// </summary>
	operator EnumerationType( void ) const;

	/// <summary cref="Enumeration::operator bool">
	/// Conversion operator to the raw C++ from NanoEngine's enumeration.
	/// </summary>
	operator ValueType( void ) const;

	/// <summary cref="Enumeration::operator=">
	/// Assignment operator.
	/// </summary>
	/// <param name="rhs">A reference to the enumeration object.</param>
	ThisType& operator = ( const ThisType& rhs );

	/// <summary cref="Enumeration::operator=">
	/// Assignment operator.
	/// </summary>
	/// <param name="e">A value of raw C++ enumeration.</param>
	ThisType& operator = ( EnumerationType e );

	ThisType& operator += ( const EnumerationType e );
	ThisType& operator -= ( const EnumerationType e );

	/// <summary cref="Enumeration::operator==">
	/// Equality operator.
	/// </summary>
	/// <param name="rhs">A reference to the raw C++ enumeration.</param>
	bool operator == ( EnumerationType e ) const;

	/// <summary cref="Enumeration::operator!=">
	/// Unequality operator.
	/// </summary>
	/// <param name="rhs">A reference to the C++ raw enumeration.</param>
	bool operator != ( EnumerationType e ) const;

	/// <summary cref="Enumeration::operator<=">
	/// Lower or equal comparison operator.
	/// </summary>
	/// <param name="rhs">A reference to the C++ raw enumeration.</param>
	bool operator <= ( EnumerationType e ) const;

	/// <summary cref="Enumerartion::operator>="
	/// Higher or equal comparison operator.
	/// </summary>
	/// <param name="rhs">A reference to the C++ raw enumration.</param>
	bool operator >= ( EnumerationType e ) const;

	/// <summary cref="Enumeration::operator<">
	/// Lower comparison operator.
	/// </summary>
	/// <param name="rhs">A reference to the C++ raw enumeration.</param>
	bool operator < ( EnumerationType e ) const;

	/// <summary cref="Enumeration::operator>">
	/// Higher comparison operator.
	/// </summary>
	/// <param name="rhs">A reference to the C++ raw enumeration.</param>
	bool operator > ( EnumerationType e ) const;

	bool Test( EnumerationType e ) const;

private:
	ValueType mStorage; //!< Store of enumerations
 };

template< typename Store, typename Enum > 
__IME_INLINED Enumeration< Store, Enum >::Enumeration( void )
{
	/// Empty
}

template< typename Store, typename Enum > 
__IME_INLINED Enumeration< Store, Enum >::Enumeration( Enum e ) : mStorage( static_cast< Store >( e ) )
{
	/// Empty
}

template< typename Store, typename Enum >
__IME_INLINED typename Enumeration< Store, Enum >::ThisType& Enumeration< Store, Enum >::operator = ( const ThisType& rhs )
{
	this->mStorage = static_cast< ValueType >( rhs.mStorage );
	return *this;
}

template< typename Store, typename Enum > 
__IME_INLINED typename Enumeration< Store, Enum >::ThisType& Enumeration< Store, Enum >::operator = ( Enum e )
{
	this->mStorage = static_cast< ValueType >( e );
	return *this;
}

template< typename Store, typename Enum > 
__IME_INLINED Enumeration< Store, Enum >::operator Enum( void ) const
{
	return static_cast< Enum >( this->mStorage ); 
}

template< typename Store, typename Enum > 
__IME_INLINED Enumeration< Store, Enum >::operator Store( void ) const
{
	return this->mStorage; 
}

template< typename Store, typename Enum >
__IME_INLINED typename Enumeration< Store, Enum >::ThisType& Enumeration< Store, Enum >::operator += ( const EnumerationType e )
{
	this->mStorage |= static_cast< ValueType >( e );
	return *this;
}

template< typename Store, typename Enum >
__IME_INLINED typename Enumeration< Store, Enum >::ThisType& Enumeration< Store, Enum >::operator -= ( const EnumerationType e )
{
	this->mStorage &= ~static_cast< ValueType >( e );
	return *this;
}

template< typename Store, typename Enum > 
__IME_INLINED bool Enumeration< Store, Enum >::operator == ( Enum e ) const
{
	return this->mStorage == static_cast< Store >( e );
}

template< typename Store, typename Enum > 
__IME_INLINED bool Enumeration< Store, Enum >::operator != ( Enum e ) const
{
	return this->mStorage != static_cast< Store >( e );
}

template< typename Store, typename Enum > 
__IME_INLINED bool Enumeration< Store, Enum >::operator <= ( Enum e ) const
{
	return this->mStorage <= static_cast< Store >( e );
}

template< typename Store, typename Enum > 
__IME_INLINED bool Enumeration< Store, Enum >::operator >= ( Enum e ) const
{
	return this->mStorage >= static_cast< Store >( e );
}

template< typename Store, typename Enum > 
__IME_INLINED bool Enumeration< Store, Enum >::operator < ( Enum e ) const
{
	return this->mStorage < static_cast< Store >( e );
}

template< typename Store, typename Enum > 
__IME_INLINED bool Enumeration< Store, Enum >::operator > ( Enum e ) const
{
	return this->mStorage > static_cast< Store >( e );
}

template< typename Store, typename Enum >
__IME_INLINED bool Enumeration< Store, Enum >::Test( EnumerationType e ) const
{
	return (this->mStorage & static_cast< ValueType >( e )) != 0;
}