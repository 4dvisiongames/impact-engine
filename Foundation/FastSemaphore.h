/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// System
#include <Foundation/System.h>
/// Interlocked
#include <Foundation/AtomicBackoff.h>

namespace Threading
{
	/// <summary>
	/// Fast semaphore object that only tracks count of threads that could access some critical section, but it's vital
	/// that programmer knows what he does: semaphore only tracks count of threads and "blocks" when thread count is
	/// higher than <c>MaxThreads</c>, and unblocks other case.
	/// </summary>
	template< SizeT MaxThreads, typename BackoffType = AtomicBackoff< BackoffStrategyPause, BackoffStrategyYield > >
	class FastSemaphore 
	{
	public:
		/// <summary cref="FastSemaphore::FastSemaphore">
		/// </summary>
		FastSemaphore();

		/// <summary cref="FastSemaphore::Wait">
		/// Wait for semaphore object monitor.
		/// </summary>
		bool Wait();

		/// <summary cref="FastSemaphore::Signal">
		/// Signal semaphore object to stop fencing.
		/// </summary>
		void Signal();

	private:
		enum { MaxSimulataneusAccessingThread = MaxThreads };
		SSizeT active;
	};

	template< SizeT MaxThreads, typename BackoffType >
	__IME_INLINED FastSemaphore<MaxThreads, BackoffType>::FastSemaphore()
		: active(0) 
	{
		/* Nothing */
	}

	template< SizeT MaxThreads, typename BackoffType >
	__IME_INLINED bool FastSemaphore<MaxThreads, BackoffType>::Wait()
	{
		bool rescanBins = false;

		__forever 
		{
			intptr_t prev = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >(this->active);
			if (prev < MaxSimulataneusAccessingThread) 
			{
				intptr_t num = Threading::Interlocked::CompareAndSwap< SSizeT, SSizeT, SSizeT >(this->active, prev + 1, prev);
				if( num == prev ) 
					break;
			} 
			else 
			{
				BackoffType backoff;
				do 
				{
					backoff.Pause();
				} while( active == prev );

				rescanBins = true;
				break;
			}
		}

		return rescanBins;
	}

	template< SizeT MaxThreads, typename BackoffType >
	__IME_INLINED void FastSemaphore<MaxThreads, BackoffType>::Signal()
	{
		__IME_ASSERT(this->active >= 0);
		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SSizeT, SSizeT >(this->active, -1);
	}
}

/*
	Change log:

	21-Mar-15		codepoet		class FastSemaphore			added BackoffCount to template
	21-Mar-15		codepoet		method FastSemaphore::Wait	manual exponential backoff replaced with already prebuilt in-engine AtomicBackoff
	19-Apr-15		codepoet		class FastSemaphore			rewritten class to accomodate changes to new atomic backcoff
	04-Jul-15		codepoet		this file					removed ifndef-define Guard. now uses pragma once.
	05-Jul-15		codepoet		Wait and Signal methods		strong typification of interlocked functions to workaround 64-bit issues 
*/