/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// File stream
#include <Foundation/FileStream.h>

/// FileSystem sub-system namespace
namespace FileSystem
{
	FileStream::FileStream( void ) : fileHandler( nullptr ), pBuffer( nullptr )
	{
		/// Empty
	}

	FileStream::~FileStream( void )
	{
		if( this->IsOpen() )
			this->Close();
	}

	bool FileStream::CanRead( void ) const
	{
		return true;
	}

	bool FileStream::CanWrite( void ) const
	{
		return true;
	}

	bool FileStream::CanSeek( void ) const
	{
		return true;
	}

	bool FileStream::CanBeMapped( void ) const
	{
		return true;
	}

	Size FileStream::GetSize( void ) const
	{
		/// Assertion
		NDKAssert2( this->fileHandler != 0x0 );
		/// Get size of file
		Size result = OSWrapper::GetFileSize( this->fileHandler );
		return result;
	}

	Position FileStream::GetPosition( void ) const
	{
		/// Assertion
		NDKAssert2( this->fileHandler != 0x0 );
		/// Get position inside file
		Position result = OSWrapper::Tell( this->fileHandler );
		return result;
	}

	bool FileStream::Open( void )
	{
		/// Assertion
		__IME_ASSERT( !this->IsOpen() );
		__IME_ASSERT( this->fileHandler == 0x0 );
		__IME_ASSERT( this->myPath.IsValid() );

		/// Open basic stream and open file
		if( Stream::Open() ) 
		{
			this->fileHandler = OSWrapper::OpenFile( this->myPath, this->mAccessMode, this->mAccessPattern );
			if( this->fileHandler )
				return true;
		}

		/// Fallthrough: failure
		Stream::Close();
		return false;
	}

	void FileStream::Close( void )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( this->fileHandler != 0x0 );

		if( this->IsMapped() )
			this->Unmap();

		OSWrapper::CloseFile( this->fileHandler );
		this->fileHandler = 0x0;
		Stream::Close();
	}

	void FileStream::Write( const void* ptr, Size numBytes )
	{
		__IME_ASSERT( !this->IsMapped() );

		if( numBytes > NULL ) 
		{
			__IME_ASSERT( this->IsOpen() );
			__IME_ASSERT( this->fileHandler != 0x0 );
			__IME_ASSERT( ptr != 0x0 );

			OSWrapper::Write( this->fileHandler, ptr, numBytes );
		}
	}

	Size FileStream::Read( void* ptr, Size numBytes )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( this->fileHandler != 0x0 );
		__IME_ASSERT( ptr != 0x0 );
		__IME_ASSERT( numBytes > NULL );

		return OSWrapper::Read( this->fileHandler, ptr, numBytes );
	}

	void FileStream::Seek( Offset offset, SeekOrigin origin )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( this->fileHandler != 0x0 );

		OSWrapper::Seek( this->fileHandler, offset, origin );
	}

	void FileStream::Flush( void )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( this->fileHandler != 0x0 );

		OSWrapper::Flush( this->fileHandler );
	}

	bool FileStream::Eof( void ) const
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( this->fileHandler != 0x0 );

		return OSWrapper::Eof( this->fileHandler );
	}

	void* FileStream::Map( void )
	{
		__IME_ASSERT( this->pBuffer == 0x0 );

		Size sz = this->GetSize();
		this->pBuffer = static_cast< u8* >( Memory::Allocator::Malloc( sz ) );
		this->Seek( 0, SeekOrigin::Begin );
		Size readSz = this->Read( this->pBuffer, sz );

		__IME_ASSERT( readSz == sz );
		Stream::Map();
		return this->pBuffer;
	}

	void FileStream::Unmap( void )
	{
		__IME_ASSERT( this->pBuffer != 0x0 );

		Stream::Unmap();
		Memory::Allocator::Free( this->pBuffer );
		this->pBuffer = 0x0;
	}
}