/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Stream
#include <Foundation/Stream.h>
/// STL string
#include <Foundation/STLString.h>
/// File system wrapper
#include <Foundation/FileSystemWrapper.h>
/// File time
#include <Foundation/FileTime.h>

/// Filesystem namespace
namespace FileSystem
{
	/// <summary>
	/// <c>FileStream</c> is a stream class to which offers read/write access to filesystem files.
	/// </summary>
	class FileStream : public Stream
	{
	protected:
		OSWrapper::Handle fileHandler; //!< Handler of file data
		u8* pBuffer; //!< Buffer of written data
		Path myPath; //!< Path to the file

	public:
		/// <summary cref="FileStream::FileStream">
		/// Constructor.
		/// </summary>
		FileStream( void );

		/// <summary cref="FileStream::~FileStream">
		/// Destructor.
		/// </summary>
		virtual ~FileStream( void );

		/// <summary cref="FileStream::SetPath">
		/// Set stream location as URI.
		/// </summary>
		/// <param name="uri">Unique resource identifier.</param>
		void SetPath( const Path& uri );

		/// <summary cref="FileStream::GetPath">
		/// Get stream URI.
		/// </summary>
		const Path& GetPath( void ) const;

		/// <summary cref="FileStream::CanRead">
		/// Check if we can read from stream.
		/// </summary>
		/// <returns>True if the stream supports reading.</returns>
		virtual bool CanRead( void ) const;

		/// <summary cref="FileStream::CanWrite">
		/// Check if we can write to the stream.
		/// </summary>
		/// <returns>True if the stream supports writing.</returns>
		virtual bool CanWrite( void ) const;

		/// <summary cref="MemoryStream::CanSeek">
		/// Check if current stream supports seeking.
		/// </summary>
		/// <returns>True if the stream supports seeking.</returns>
		virtual bool CanSeek( void ) const;

		/// <summary cref="FileStream::CanBeMapped">
		/// Check if we can map current stream to read/write on CPU.
		/// </summary>
		/// <returns>True if the stream provides direct memory access.</returns>
		virtual bool CanBeMapped( void ) const;

		/// <summary cref="FileStream::GetSize">
		/// Get the size of the stream in bytes.
		/// </summary>
		/// <returns>Size of stream.</returns>
		virtual Size GetSize( void ) const;

		/// <summary cref="FileStream::GetPosition">
		/// Get the current position of the read/write cursor.
		/// </summary>
		/// <returns>Position of stream in memory.</param>
		virtual Position GetPosition( void ) const;

		/// <summary cref="FileStream::Open">
		/// Open the stream.
		/// </summary>
		virtual bool Open( void );

		/// <summary cref="FileStream::Close">
		/// Close the stream.
		/// </summary>
		virtual void Close( void );

		/// <summary cref="FileStream::Write">
		/// Directly write to the stream.
		/// </summary>
		/// <param name="ptr">A pointer to the place where to place data.</param>
		/// <param name="numBytes">Size of data.</param>
		virtual void Write( const void* ptr, Size numBytes );

		/// <summary cref="FileStream::Read">
		/// Directly read from the stream.
		/// </summary>
		/// <param name="ptr">A pointer to the data where we read from.</param>
		/// <param name="numBytes">Size of data.</param>
		/// <returns>Read data in bytes.</returns>
		virtual Size Read( void* ptr, Size numBytes );

		/// <summary cref="FileStream::Seek">
		/// Seek in stream.
		/// </summary>
		/// <param name="offset">Offset in bytes.</param>
		/// <param name="origin">Origin in data.</param>
		virtual void Seek( Offset offset, SeekOrigin origin );

		/// <summary cref="FileStream::Flush">
		/// Flush unsaved data.
		/// </summary>
		virtual void Flush( void );

		/// <summary cref="FileStream::Eof">
		/// End of reading.
		/// </summary>
		/// <returns>True if end-of-stream reached, otherwise false.</returns>
		virtual bool Eof( void ) const;

		/// <summary cref="FileStream::Map">
		/// Map stream to memory.
		/// </summary>
		/// <returns>A pointer to the data in stream.</returns>
		virtual void* Map( void );

		/// <summary cref="FileStream::Unmap">
		/// Unmap stream.
		/// </summary>
		virtual void Unmap( void );
	};

	__IME_INLINED void FileStream::SetPath( const Path& path )
	{
		__IME_ASSERT( path.IsValid( ) );
		__IME_ASSERT( !this->IsOpen( ) );
		this->myPath = path;
	}

	__IME_INLINED const Path& FileStream::GetPath( void ) const
	{
		return this->myPath;
	}
}