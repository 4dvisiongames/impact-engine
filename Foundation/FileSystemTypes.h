/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// STL string
#include <Foundation/STLString.h>

/// OS-platform dependent directory definitions to put into Path
#if !defined( __IME_PLATFORM_XBOX_ONE ) || !defined( __IME_PLATFORM_PLAYSTATION_4 )
#define NDKOSPath( path ) __IME_CHAR16( path )
#else
#define NDKOSPath( path ) path
#endif

/// Filesystem namespace
namespace FileSystem
{
#if defined( __IME_PLATFORM_XBOX_ONE ) || defined( __IME_PLATFORM_PLAYSTATION_4 )
		typedef STL::String< Char8 > Path;
#else
		typedef STL::String< Char16 > Path;
#endif

#if defined(__IME_PLATFORM_WIN64)
		typedef s64 Position; //!< File position definition
		typedef s64 Offset; //!< File offset from other file
		typedef s64 Size; //!< File size in bytes
#else
		typedef s32 Position; //!< File position definition
		typedef s32 Offset; //!< File offset from other file
		typedef s32 Size; //!< File size in bytes
#endif
}