/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Global string atomic table
#include <Foundation/GlobalStringAtomTable.h>
/// Default scoped locker
#include <Foundation/ScopedLock.h>

/// Standard template library 
namespace STL
{
	/// Implement interface singleton
	__IME_IMPLEMENT_SINGLETON( GlobalStringAtomTable );

	GlobalStringAtomTable::GlobalStringAtomTable()
	{
		/// Construct singleton object
		__IME_CONSTRUCT_SINGLETON;
    
		// setup the global string buffer
		this->stringBuffer.Setup( _GLOBAL_STRINGBUFFER_CHUNKSIZE );
	}

	GlobalStringAtomTable::~GlobalStringAtomTable()
	{
		/// Lock string table and destruct singleton object
		Threading::ScopedLock< Threading::SpinWait > lock( this->mySW );
		this->stringBuffer.Discard();
		__IME_DESTRUCT_SINGLETON;
	}

	void GlobalStringAtomTable::Lock()
	{
		/// Lock critical section
		this->mySW.Lock();
	}

	void GlobalStringAtomTable::Unlock()
	{
		// Unlock critical section
		this->mySW.Unlock();
	}

	const s8* GlobalStringAtomTable::Add(const s8* str)
	{
		StaticString sstr;
		/// Get string to the thread-safe static string buffer
		sstr.ptr = (s8*)this->stringBuffer.AddString(str);
		/// Insert value to the table and sort
		this->table.InsertSorted(sstr);
		/// Get it, if you need it.
		return sstr.ptr;
	}

	GlobalStringAtomTable::DebugInfo GlobalStringAtomTable::GetDebugInfo()
	{
		/// Lock current string table and write definitions to information
		/// table.
		this->mySW.Lock();
		DebugInfo debugInfo;
		debugInfo.strings.Reserve( STL::CountOf(this->table) );
		debugInfo.chunkSize = _GLOBAL_STRINGBUFFER_CHUNKSIZE; //!< Depends on current platform, always 32*1024, except Nintondo Wii U, where 16*1024
		debugInfo.numChunks = this->stringBuffer.GetNumChunks();
		debugInfo.allocSize = debugInfo.chunkSize * debugInfo.numChunks;
		debugInfo.usedSize  = 0;
		debugInfo.growthEnabled = _ENABLE_GLOBAL_STRINGBUFFER_GROWTH; //!< Depends on current platform, always 1, except Nintendo Wii U, where 0

		/// Write strings content and used size in kbytes
		for ( SizeT i = 0; i < STL::CountOf(table); i++ )
		{        
			const s8* str = this->table[i].ptr;
			debugInfo.strings.Append( this->table[i].ptr );
			debugInfo.usedSize += strlen( str ) + 1;
		}
		/// Leave critical section
		this->mySW.Unlock();
		/// Get debugger information
		return debugInfo;
	}
} /// namespace STL