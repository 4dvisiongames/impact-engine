/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _GlobalStringAtomTable_h_
#define _GlobalStringAtomTable_h_

/// String atom basic definitions
#include <Foundation/StringAtomBase.h>
/// String buffer
#include <Foundation/Stringbuffer.h>
/// Singleton
#include <Foundation/Singleton.h>
/// Spin wait
#include <Foundation/SpinWait.h>

/// Text-worker module namespace
namespace STL
{
	/// <summary>
	/// <c>STL::GlobalStringAtomTable</c> is the definitive string atom table which contains 
	/// the string of all string atoms of all threads. Locking is necessary to lookup or add 
	/// a string (that's why thread-local string atom tables exist as local cache to prevent 
	/// too much locking of the global table).
	/// </summary>
	class GlobalStringAtomTable : public StringAtomTableBase
	{
		/// Declare singleton object
		__IME_DECLARE_SINGLETON( GlobalStringAtomTable );

	public:
		/// <summary cref="GlobalStringAtomTable::GlobalStringAtomTable">
		/// Constructor
		/// </summary>
		GlobalStringAtomTable( void );

		/// <summary cref="GlobalStringAtomTable::~GlobalStringAtomTable">
		/// Destructor
		~GlobalStringAtomTable( void );

		/// <summary cref="GlobalStringAtomTable::GetGlobalStringBuffer">
		/// Get pointer to global string buffer.
		/// </summary>
		StringBuffer* GetGlobalStringBuffer( void ) const;

		/// <summary>
		/// <s>DebugInfo</s> has debug functionality, just for memory tracking
		/// </summary>
		struct DebugInfo
		{
			STL::Array<const s8*> strings; //!< All strings from buffer
			SizeT chunkSize; //!< One chunk size
			SizeT numChunks; //!< Number of chunks
			SizeT allocSize; //!< Allocated size of chunks
			SizeT usedSize; //!< Used size 
			bool growthEnabled; //!< Growth?
		};

		/// <summary cref="GlobalStringAtomTable::GetDebugInfo">
		/// Debug functionality: get copy of the string atom table.
		/// </summary>
		/// <returns>Collected debug information.</returns>
		DebugInfo GetDebugInfo();

	private:
		friend class StringAtom;

		/// <summary cref="GlobalStringAtomTable::Lock">
		/// Lock string table to one thread for reading or writing.
		/// </summary>
		void Lock();

		/// <summary cref="GlobalStringAtomTable::Add">
		/// This adds a new string to the atom table and the global string buffer.
		/// </summary>
		/// <returns>The pointer to the string in the string buffer.</returns>
		/// <notes>
		/// You MUST call this method within Lock()/Unlock() to be thread-safe!.
		/// </notes>
		const s8* Add(const s8* str);

		/// <summary cref="GlobalStringAtomTable::Unlock">
		/// Release the critical section.
		/// </summary>
		void Unlock();

		Threading::SpinWait mySW; //!< Critical section
		StringBuffer stringBuffer; //!< Thread-safe static string buffer
	};
} // namespace STL

#endif /// _STLGlobalStringAtomTable_h_