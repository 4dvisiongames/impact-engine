/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef __Foundation__Interlocked_h__
#define __Foundation__Interlocked_h__

/// Shared engine code
#include <Foundation/Shared.h>
/// Memory barriers
#include <Foundation/MemoryBarriers.h>

#if defined( __IME_PLATFORM_WIN64 )
/// Windows implementation of interlocked functions
#include <Foundation/Win32/Win32Interlocked.h>

#elif defined( __IME_PLATFORM_LINUX64 ) || defined( __IME_PLATFORM_OSX_INTEL )
/// POSIX implementation of interlocked functions
#include <Foundation/Posix/PosixInterlocked.h>

#else
#error "Interlocked functions are not implemented on this platform!"
#endif

/// Task namespace 
namespace Threading
{
	/// <summary>
	/// <c>MemoryOrder</c> is a memory ordering definitions.
	/// </summary>
	struct MemoryOrderEnum
	{
		enum List
		{
			Relaxed, //!< No order constraint, same as plain load/store. Unsafe but best performance
			Acquire, //!< Must be a load op. Synchronize with a prior release in another thread.
			Release, //!< Must be a store op. Synchronize with a later acquire in another thread.
			AcquireRelease, //!< Must be a load-modify-store op. Performs both acquire and release.
			Sequental //!< Sequential consistency, safe total order but least performance
		};
	};
	typedef MemoryOrderEnum::List MemoryOrder;


	template< size_t size > struct InterlockedType;
	template<> struct InterlockedType< 1 > { typedef s8 type; };
	template<> struct InterlockedType< 2 > { typedef s16 type; };
#if defined( __M_X64 )
	template<> struct InterlockedType< 4 > { typedef s32 type; };
#else
	template<> struct InterlockedType< 4 > { typedef ptrdiff_t type; };
#endif
	template<> struct InterlockedType< 8 > { typedef s64 type; };


	template< typename T, size_t size > struct InterlockedAlign;
	template< typename T > struct InterlockedAlign< T, 1 > { __declspec( align( 1 ) ) T data; };
	template< typename T > struct InterlockedAlign< T, 2 > { __declspec( align( 2 ) ) T data; };
	template< typename T > struct InterlockedAlign< T, 4 > { __declspec( align( 4 ) ) T data; };
	template< typename T > struct InterlockedAlign< T, 8 > { __declspec( align( 8 ) ) T data; };

	template< typename T > struct InterlockedPODType : InterlockedAlign< T, sizeof( T ) > { };


	namespace Internal
	{
		struct FunctionTypeEnum
		{
			enum List
			{
				Load,
				Store,
				LoadAndStore,
				Add,
				CompareAndSwap
			};
		};
		typedef FunctionTypeEnum::List FunctionType;


		struct LogicalOpEnum
		{
			enum List
			{
				Or,
				And,
				Xor
			};
		};
		typedef LogicalOpEnum::List LogicalOp;

		//! Abstractive interlocked logical operations
		template< size_t size, LogicalOp op > struct InterlockedLogicalOp;
		template<> struct InterlockedLogicalOp< 1, LogicalOpEnum::Or >
		{
			typedef InterlockedType< 1 >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< 1, LogicalOpEnum::Or >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_OR_1( operand, addend );
		}

		template<> struct InterlockedLogicalOp< 1, LogicalOpEnum::And >
		{
			typedef InterlockedType< 1 >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< 1, LogicalOpEnum::And >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_AND_1( operand, addend );
		}

		template<> struct InterlockedLogicalOp< 1, LogicalOpEnum::Xor >
		{
			typedef InterlockedType< 1 >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< 1, LogicalOpEnum::Xor >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_XOR_1( operand, addend );
		}



		template<> struct InterlockedLogicalOp< 2, LogicalOpEnum::Or >
		{
			typedef InterlockedType< 2 >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< 2, LogicalOpEnum::Or >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_OR_2( operand, addend );
		}

		template<> struct InterlockedLogicalOp< 2, LogicalOpEnum::And >
		{
			typedef InterlockedType< 2 >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< 2, LogicalOpEnum::And >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_AND_2( operand, addend );
		}

		template<> struct InterlockedLogicalOp< 2, LogicalOpEnum::Xor >
		{
			typedef InterlockedType< 2 >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< 2, LogicalOpEnum::Xor >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_XOR_2( operand, addend );
		}


		template<> struct InterlockedLogicalOp< sizeof( size_t ), LogicalOpEnum::Or >
		{
			typedef InterlockedType< sizeof( size_t ) >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< sizeof( size_t ), LogicalOpEnum::Or >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_OR( operand, addend );
		}

		template<> struct InterlockedLogicalOp< sizeof( size_t ), LogicalOpEnum::And >
		{
			typedef InterlockedType< sizeof( size_t ) >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< sizeof( size_t ), LogicalOpEnum::And >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_AND( operand, addend );
		}

		template<> struct InterlockedLogicalOp< sizeof( size_t ), LogicalOpEnum::Xor >
		{
			typedef InterlockedType< sizeof( size_t ) >::type type;
			static void Process( volatile type* operand, type addend );
		};

		__IME_INLINED void InterlockedLogicalOp< sizeof( size_t ), LogicalOpEnum::Xor >::Process( volatile type* operand, type addend )
		{
			ATOMIC_LOGICAL_XOR( operand, addend );
		}


		//! Abstractive interlocked functions definition with order
		template< size_t size, FunctionType func, MemoryOrder order > struct InterlockedFuncWithOrder;

		//! Implementation of 1-byte interlocked relaxed ordered loading
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr )
		{
			type result = *ptr;
			return result;
		}

		//! Implementation of 2-byte interlocked relaxed ordered loading
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr )
		{
			type result = *ptr;
			return result;
		}

		//! Implementation of 4-byte interlocked relaxed ordered loading
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr )
		{
			type result = *ptr;
			return result;
		}

		//! Implementation of 8-byte interlocked relaxed ordered loading
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr )
		{
			type result = *ptr;
			return result;
		}



		//! Implementation of 1-byte interlocked acquire ordered loading
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >::Process( volatile type* ptr )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;
			return result;
		}

		//! Implementation of 2-byte interlocked acquire ordered loading
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >::Process( volatile type* ptr )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;
			return result;
		}

		//! Implementation of 4-byte interlocked acquire ordered loading
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >::Process( volatile type* ptr )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;
			return result;
		}

		//! Implementation of 8-byte interlocked acquire ordered loading
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Acquire >::Process( volatile type* ptr )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;
			return result;
		}



		//! Implementation of 1-byte interlocked sequental consistensy ordered loading
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >::Process( volatile type* ptr )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;
			return result;
		}

		//! Implementation of 2-byte interlocked sequental consistensy ordered loading
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >::Process( volatile type* ptr )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;
			return result;
		}

		//! Implementation of 4-byte interlocked sequental consistensy ordered loading
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >::Process( volatile type* ptr )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;
			return result;
		}

		//! Implementation of 2-byte interlocked sequental consistensy ordered loading
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::Load, MemoryOrderEnum::Sequental >::Process( volatile type* ptr )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;
			return result;
		}



		//! Implementation of 1-byte interlocked relaxed ordered storing
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Store, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 1 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 1, FunctionTypeEnum::Store, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			*ptr = value;
		}

		//! Implementation of 2-byte interlocked relaxed ordered storing
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Store, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 2 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 2, FunctionTypeEnum::Store, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			*ptr = value;
		}

		//! Implementation of 4-byte interlocked relaxed ordered storing
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Store, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 4 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 4, FunctionTypeEnum::Store, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			*ptr = value;
		}

		//! Implementation of 4-byte interlocked relaxed ordered storing
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Store, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 8 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 8, FunctionTypeEnum::Store, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			*ptr = value;
		}



		//! Implementation of 1-byte interlocked storing with release
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Store, MemoryOrderEnum::Release >
		{
			typedef InterlockedType< 1 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 1, FunctionTypeEnum::Store, MemoryOrderEnum::Release >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			*ptr = value;
		}

		//! Implementation of 2-byte interlocked storing with release
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Store, MemoryOrderEnum::Release >
		{
			typedef InterlockedType< 2 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 2, FunctionTypeEnum::Store, MemoryOrderEnum::Release >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			*ptr = value;
		}

		//! Implementation of 4-byte interlocked storing with release
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Store, MemoryOrderEnum::Release >
		{
			typedef InterlockedType< 4 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 4, FunctionTypeEnum::Store, MemoryOrderEnum::Release >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			*ptr = value;
		}

		//! Implementation of 8-byte interlocked storing with release
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Store, MemoryOrderEnum::Release >
		{
			typedef InterlockedType< 8 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 8, FunctionTypeEnum::Store, MemoryOrderEnum::Release >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			*ptr = value;
		}



		//! Implementation of 1-byte interlocked sequental consistency storing
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Store, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 1 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 1, FunctionTypeEnum::Store, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			ATOMIC_EXCHANGE_1( ptr, value );
		}

		//! Implementation of 2-byte interlocked sequental consistency storing
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Store, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 2 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 2, FunctionTypeEnum::Store, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			ATOMIC_EXCHANGE_2( ptr, value );
		}

		//! Implementation of 4-byte interlocked sequental consistency storing
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Store, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 4 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 4, FunctionTypeEnum::Store, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			ATOMIC_EXCHANGE_4( ptr, value );
		}

		//! Implementation of 8-byte interlocked sequental consistency storing
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Store, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 8 >::type	type;
			static void Process( volatile type* ptr, type value );
		};

		__IME_INLINED void InterlockedFuncWithOrder< 8, FunctionTypeEnum::Store, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			ATOMIC_EXCHANGE_8( ptr, value );
		}



		//! Implementation of 1-byte interlocked relaxed load-modify-store
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			type prev = *ptr;
			*ptr = value;
			return prev;
		}

		//! Implementation of 2-byte interlocked relaxed load-modify-store
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			type prev = *ptr;
			*ptr = value;
			return prev;
		}

		//! Implementation of 4-byte interlocked relaxed load-modify-store
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			type prev = *ptr;
			*ptr = value;
			return prev;
		}

		//! Implementation of 8-byte interlocked relaxed load-modify-store
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			type prev = *ptr;
			*ptr = value;
			return prev;
		}



		//! Implementation of 1-byte interlocked acquire-modify load-modify-store
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type prev = *ptr;

			__IME_MEMORY_READWRITE_BARRIER;
			*ptr = value;

			return prev;
		}

		//! Implementation of 2-byte interlocked acquire-modify load-modify-store
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type prev = *ptr;

			__IME_MEMORY_READWRITE_BARRIER;
			*ptr = value;

			return prev;
		}

		//! Implementation of 4-byte interlocked acquire-modify load-modify-store
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type prev = *ptr;

			__IME_MEMORY_READWRITE_BARRIER;
			*ptr = value;

			return prev;
		}

		//! Implementation of 8-byte interlocked acquire-modify load-modify-store
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::AcquireRelease >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type prev = *ptr;

			__IME_MEMORY_READWRITE_BARRIER;
			*ptr = value;

			return prev;
		}



		//! Implementation of 1-byte interlocked sequental consistency load-modify-store
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			return (type)ATOMIC_EXCHANGE_1( ptr, value );
		}

		//! Implementation of 2-byte interlocked sequental consistency load-modify-store
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			return (type)ATOMIC_EXCHANGE_2( ptr, value );
		}

		//! Implementation of 4-byte interlocked sequental consistency load-modify-store
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			return (type)ATOMIC_EXCHANGE_4( (volatile long*)ptr, value );
		}

		//! Implementation of 8-byte interlocked sequental consistency load-modify-store
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::LoadAndStore, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			return (type)ATOMIC_EXCHANGE_8( ptr, value );
		}



		//! Implementation of 1-byte interlocked relaxed addition
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			type result = *ptr;
			*ptr += value;
			return result;
		}

		//! Implementation of 2-byte interlocked relaxed addition
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			type result = *ptr;
			*ptr += value;
			return result;
		}

		//! Implementation of 4-byte interlocked relaxed addition
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			type result = *ptr;
			*ptr += value;
			return result;
		}

		//! Implementation of 8-byte interlocked relaxed addition
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::Relaxed >::Process( volatile type* ptr, type value )
		{
			type result = *ptr;
			*ptr += value;
			return result;
		}



		//! Implementation of 1-byte interlocked acquire-release addition
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;

			__IME_MEMORY_READWRITE_BARRIER;
			*ptr += value;

			return result;
		}

		//! Implementation of 2-byte interlocked acquire-release addition
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;

			__IME_MEMORY_READWRITE_BARRIER;
			*ptr += value;

			return result;
		}

		//! Implementation of 4-byte interlocked acquire-release addition
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;

			__IME_MEMORY_READWRITE_BARRIER;
			*ptr += value;

			return result;
		}

		//! Implementation of 8-byte interlocked acquire-release addition
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::AcquireRelease >::Process( volatile type* ptr, type value )
		{
			__IME_MEMORY_READWRITE_BARRIER;
			type result = *ptr;

			__IME_MEMORY_READWRITE_BARRIER;
			*ptr += value;

			return result;
		}



		//! Implementation of 1-byte interlocked acquire-release addition
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			return (type)ATOMIC_EXCHANGE_ADD_1( ptr, value );
		}

		//! Implementation of 2-byte interlocked acquire-release addition
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			return (type)ATOMIC_EXCHANGE_ADD_2( ptr, value );
		}

		//! Implementation of 4-byte interlocked acquire-release addition
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			return (type)ATOMIC_EXCHANGE_ADD_4( ptr, value );
		}

		//! Implementation of 8-byte interlocked acquire-release addition
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr, type value );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::Add, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value )
		{
			return (type)ATOMIC_EXCHANGE_ADD_8( ptr, value );
		}


		//! Implementation of 1-byte interlocked compare-and-swap
		template<> struct InterlockedFuncWithOrder< 1, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 1 >::type	type;
			static type Process( volatile type* ptr, type value, type comparand );
		};

		__IME_INLINED InterlockedFuncWithOrder< 1, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 1, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value, type comparand )
		{
			return (type)ATOMIC_COMPARE_EXCHANGE_1( ptr, value, comparand );
		}

		//! Implementation of 2-byte interlocked compare-and-swap
		template<> struct InterlockedFuncWithOrder< 2, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 2 >::type	type;
			static type Process( volatile type* ptr, type value, type comparand );
		};

		__IME_INLINED InterlockedFuncWithOrder< 2, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 2, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value, type comparand )
		{
			return (type)ATOMIC_COMPARE_EXCHANGE_2( ptr, value, comparand );
		}

		//! Implementation of 4-byte interlocked compare-and-swap
		template<> struct InterlockedFuncWithOrder< 4, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 4 >::type	type;
			static type Process( volatile type* ptr, type value, type comparand );
		};

		__IME_INLINED InterlockedFuncWithOrder< 4, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 4, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value, type comparand )
		{
			return (type)ATOMIC_COMPARE_EXCHANGE_4( ptr, value, comparand );
		}

		//! Implementation of 8-byte interlocked compare-and-swap
		template<> struct InterlockedFuncWithOrder< 8, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >
		{
			typedef InterlockedType< 8 >::type	type;
			static type Process( volatile type* ptr, type value, type comparand );
		};

		__IME_INLINED InterlockedFuncWithOrder< 8, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >::type InterlockedFuncWithOrder< 8, FunctionTypeEnum::CompareAndSwap, MemoryOrderEnum::Sequental >::Process( volatile type* ptr, type value, type comparand )
		{
			return (type)ATOMIC_COMPARE_EXCHANGE_8( ptr, value, comparand );
		}
	}

	/// <summary>
	/// <c>Interlocked</c> is Windows-platform interlocked functions.
	/// </summary>
    class Interlocked
    {
		//! TODO: rechecks on recent versions of GCC and MSVC if union is still the ONLY(!!!!) way to do a conversion without warnings
		//! Union type used to convert type T to underlying integral type.
		template< typename T >
		union IntegralConverter
		{
			typedef T derivativeType;
			typedef typename InterlockedType< sizeof( T ) >::type integralType;
			IntegralConverter() : myValue( (T)0 ) { };
			IntegralConverter( derivativeType value ) : myValue( value ) { };

			IntegralConverter( const IntegralConverter& ) = delete;
			IntegralConverter& operator = (const IntegralConverter&) = delete;

			derivativeType myValue;
			integralType myBits;
		};

		template< typename T >
		static typename IntegralConverter< T >::integralType ToBits( T value );

		template< typename T >
		static T ToValue( typename IntegralConverter< T >::integralType bits );

    public:
		/// <summary cref="Interlocked::Load">
		/// Load value from atomic variable.
		/// </summary>
		template< MemoryOrder order, typename T > 
		static T Fetch( volatile T& address );

		/// <summary cref="Interlocked::Store">
		/// Store some value atomic variable.
		/// </summary>
		template< MemoryOrder order, typename T, typename Y > 
		static void Store( volatile T& address, Y value );

		/// <summary cref="Interlocked::Store">
		/// Store some value atomic variable.
		/// </summary>
		template< MemoryOrder order, typename T, typename Y > 
		static T FetchStore( volatile T& address, Y value );

		/// <summary cref="Interlocked::Add">
		/// Add some data to the atomic variable.
		/// </summary>
		template< MemoryOrder order, typename T, typename Y > 
		static T FetchAdd( volatile T& address, Y value );

		/// <summary cref="Interlocked::CompareAndSwap">
		/// Compare values and store comparand in atomic variable.
		/// </summary>
		template< typename T, typename Y, typename U > 
		static T CompareAndSwap( volatile T& address, Y value, U comparand );

		/// <summary cref="Interlocked::Or">
		/// Interlocked "OR" Boolean operation.
		/// </summary>
		template< typename T, typename Y >
		static void Or( volatile T& operand, Y addend );

		/// <summary cref="Interlocked::And">
		/// Interlocked "AND" Boolean operation.
		/// </summary> 
		template< typename T, typename Y >
		static void And( volatile T& operand, Y addend );

		/// <summary cref="Interlocked::Xor">
		/// Interlocked "XOR" Boolean operation.
		/// </summary> 
		template< typename T, typename Y >
		static void Xor( volatile T& operand, Y addend );
    };

	template< typename T >
	__IME_INLINED static typename Interlocked::IntegralConverter< T >::integralType Interlocked::ToBits( T value )
	{
		return Interlocked::IntegralConverter< T >( value ).myBits;
	}

	template< typename T >
	__IME_INLINED static T Interlocked::ToValue( typename Interlocked::IntegralConverter< T >::integralType bits )
	{
		Interlocked::IntegralConverter< T > my;
		my.myBits = bits;
		return my.myValue;
	}

	template< MemoryOrder order, typename T >
	__IME_INLINED T Interlocked::Fetch( volatile T& address ) 
	{
		static_assert( sizeof( T ) <= 8, "Atomic operation can be done only on 1-/2-/4-/8-byte sized objects" );
		static_assert( (order != Threading::MemoryOrderEnum::Release) || (order != Threading::MemoryOrderEnum::AcquireRelease), 
					   "Atomic fetching can be: Relaxed, Acquire and Sequental" );

		typedef Internal::InterlockedFuncWithOrder< sizeof( T ), Internal::FunctionTypeEnum::Load, order >::type interlockedType;

		return Interlocked::ToValue< T >( Internal::InterlockedFuncWithOrder< sizeof( T ), 
										  Internal::FunctionTypeEnum::Load, order >::Process( (volatile interlockedType*)&address ) );
	}

	template< MemoryOrder order, typename T, typename Y >
	__IME_INLINED void Interlocked::Store( volatile T& address, Y value ) 
	{
		static_assert( sizeof( T ) <= 8 || sizeof( Y ) <= 8, "Atomic operation can be done only on 1-/2-/4-/8-byte sized objects" );
		static_assert( (order != Threading::MemoryOrderEnum::Acquire) || (order != Threading::MemoryOrderEnum::AcquireRelease), 
					   "Atomic storing can be: Relaxed, Release and Sequental" );

		typedef InterlockedType< sizeof( T ) >::type interlockedType;

		Internal::InterlockedFuncWithOrder< sizeof( T ), 
											Internal::FunctionTypeEnum::Store, order >::Process( (volatile interlockedType*)&address, 
																								 Interlocked::ToBits( value ) );
	}

	template< MemoryOrder order, typename T, typename Y >
	__IME_INLINED T Interlocked::FetchStore( volatile T& address, Y value )
	{
		static_assert( sizeof( T ) <= 8 || sizeof( Y ) <= 8, "Atomic operation can be done only on 1-/2-/4-/8-byte sized objects" );
		static_assert( (order != Threading::MemoryOrderEnum::Acquire) || (order != Threading::MemoryOrderEnum::Release), 
					   "Atomic fetch-storing can be: Relaxed, Acquire-Release and Sequental" );

		typedef InterlockedType< sizeof( T ) >::type interlockedType;

		return Interlocked::ToValue< T >( Internal::InterlockedFuncWithOrder< sizeof( T ), 
										  Internal::FunctionTypeEnum::LoadAndStore, order >::Process( (volatile interlockedType*)&address, 
																									  Interlocked::ToBits( value ) ) );
	}

	template< MemoryOrder order, typename T, typename Y >
	__IME_INLINED T Interlocked::FetchAdd( volatile T& address, Y value )
	{
		static_assert( sizeof( T ) <= 8 || sizeof( Y ) <= 8, "Atomic operation can be done only on 1-/2-/4-/8-byte sized objects" );
		static_assert( (order != Threading::MemoryOrderEnum::Acquire) || (order != Threading::MemoryOrderEnum::Release), 
					   "Atomic fetch-addition can be: Relaxed, Acquire-Release and Sequental" );

		typedef InterlockedType< sizeof( T ) >::type interlockedType;

		return Interlocked::ToValue< T >( Internal::InterlockedFuncWithOrder< sizeof( T ), 
										  Internal::FunctionTypeEnum::Add, order >::Process( (volatile interlockedType*)&address, 
																							 Interlocked::ToBits( value ) ) );
	}

	template< typename T, typename Y, typename U >
	__IME_INLINED T Interlocked::CompareAndSwap( volatile T& address, Y value, U comparand ) {
		static_assert( sizeof( T ) <= 8 || sizeof( Y ) <= 8 || sizeof( U ) <= 8, 
					   "Atomic operation can be done only on 1-/2-/4-/8-byte sized objects" );

		typedef InterlockedType< sizeof( T ) >::type interlockedType;

		return Interlocked::ToValue< T >( Internal::InterlockedFuncWithOrder< sizeof( T ), 
										  Internal::FunctionTypeEnum::CompareAndSwap, 
										  MemoryOrderEnum::Sequental >::Process( (volatile interlockedType*)&address, 
																				 Interlocked::ToBits( value ), 
																				 Interlocked::ToBits( comparand ) ) );
	}

	template< typename T, typename Y >
	__IME_INLINED void Interlocked::Or( volatile T& operand, Y addend )
	{
		static_assert( sizeof( T ) <= 8 || sizeof( Y ) <= 8, 
					   "Atomic operation can be done only on 1-/2-/4-/8-byte sized objects" );

		typedef InterlockedType< sizeof( T ) >::type interlockedType;

		Internal::InterlockedLogicalOp< sizeof( T ), 
										Internal::LogicalOpEnum::Or >::Process( (volatile interlockedType*)&operand, 
																				Interlocked::ToBits( addend ) );
	}

	template< typename T, typename Y >
	__IME_INLINED void Interlocked::And( volatile T& operand, Y addend )
	{
		static_assert( sizeof( T ) <= 8 || sizeof( Y ) <= 8, 
					   "Atomic operation can be done only on 1-/2-/4-/8-byte sized objects" );

		typedef InterlockedType< sizeof( T ) >::type interlockedType;

		Internal::InterlockedLogicalOp< sizeof( T ), 
										Internal::LogicalOpEnum::And >::Process( (volatile interlockedType*)&operand, 
																				 Interlocked::ToBits( addend ) );
	}

	template< typename T, typename Y >
	__IME_INLINED void Interlocked::Xor( volatile T& operand, Y addend )
	{
		static_assert( sizeof( T ) <= 8 || sizeof( Y ) <= 8, 
					   "Atomic operation can be done only on 1-/2-/4-/8-byte sized objects" );

		typedef InterlockedType< sizeof( T ) >::type interlockedType;

		Internal::InterlockedLogicalOp< sizeof( T ), 
										Internal::LogicalOpEnum::Xor >::Process( (volatile interlockedType*)&operand, 
																				 Interlocked::ToBits( addend ) );
	}
}

#endif /// __Foundation__Interlocked_h__
