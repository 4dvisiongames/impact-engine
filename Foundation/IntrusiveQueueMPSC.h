/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Foundation__IntrusiveQueueMPSC_h__
#define __Foundation__IntrusiveQueueMPSC_h__

/// Interlocked functions
#include <Foundation/Interlocked.h>
/// STL intrusive slist
#include <Foundation/STLIntrusiveSList.h>

namespace Threading
{
	/// <summary>
	/// <c>InstrusiveQueueMPSC</c> is an implementation of multiple producer, single consumer wait-free queue.
	/// See http://www.1024cores.net/home/lock-free-algorithms/queues/intrusive-mpsc-node-based-queue
	/// for more details.
	/// </summary>
	template< typename T, typename TypeAdapter = Traits::TypeSelect< Traits::IsSmartPointer< T >::value,
																	 Traits::SmartPtrIntrusiveAdapter< T >,
																	 Traits::DefaultIntrusiveAdapter< T > >::ValueType >
	class IntrusiveQueueMPSC
	{
	public:
		typedef IntrusiveQueueMPSC< T, TypeAdapter > ThisType;
		typedef T ReferencedValueType;
		typedef T& ReferencedRef;
		typedef const T& ReferencedConstRef;
		typedef T* ReferencedPointer;
		typedef typename TypeAdapter::ValueType ValueType;
		typedef ValueType* Pointer;
		typedef ValueType& Reference;
		typedef const ValueType* ConstPointer;
		typedef const ValueType& ConstReference;

		/// Hack
		typedef typename STL::IntrusiveSList< T, TypeAdapter >::NodeType NodeType;

		IntrusiveQueueMPSC( void );
		~IntrusiveQueueMPSC( void );

		void Enqueue( ReferencedConstRef rhs );
		bool Dequeue( ReferencedPointer& output );
		
	private:
		NodeType* myHead;
		NodeType* myTail;
		NodeType stubbed;
	};

	template< typename T, typename TypeAdapter >
	IntrusiveQueueMPSC< T, TypeAdapter >::IntrusiveQueueMPSC(void) : myHead(&stubbed), myTail(&stubbed)
	{
		this->stubbed.myNext = &this->stubbed;
	}

	template< typename T, typename TypeAdapter >
	IntrusiveQueueMPSC< T, TypeAdapter >::~IntrusiveQueueMPSC(void)
	{
		__IME_ASSERT( this->stubbed.myNext == &this->stubbed );
	}

	template< typename T, typename TypeAdapter >
	void IntrusiveQueueMPSC< T, TypeAdapter >::Enqueue( ReferencedConstRef rhs )
	{
		NodeType* input = TypeAdapter::getPointerFrom( const_cast<ReferencedRef>(rhs) );
		input->myNext = nullptr;

		NodeType* prev = Threading::Interlocked::FetchStore< Threading::MemoryOrderEnum::Sequental >( this->myHead, input );
		prev->myNext = input;

		/// In release builds compiler will optimize out this function call if DefaultIntrusiveAdapter used 
		TypeAdapter::appendedTo(const_cast<ValueType*>(&rhs));
	}

	template< typename T, typename TypeAdapter >
	bool IntrusiveQueueMPSC< T, TypeAdapter >::Dequeue( ReferencedPointer& output )
	{
		/// They are relaxed, because every thread has it's own copy of this, so don't need to sync
		NodeType* tail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myTail );
		NodeType* next = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( tail->myNext );

		if( tail == &this->stubbed ) {
			if( next == nullptr ) 
				return false;

			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myTail, next );
			tail = next;
			next = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( next->myNext );
		}

		if( next ) {
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myTail, next );

			Pointer node = static_cast<Pointer>(tail);
			/// In release builds compiler will optimize out this function call if DefaultIntrusiveAdapter used 
			TypeAdapter::removedFrom( node );
			TypeAdapter::setPointerTo( node, output );
			return true;
		}

		NodeType* head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myHead );
		if( tail != head ) {
			return false;
		}

		return false;
	}
}

#endif /// __Foundation__IntrusiveQueueMPSC_h__