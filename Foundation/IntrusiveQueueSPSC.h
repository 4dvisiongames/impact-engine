/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Foundation__IntrusiveQueueSPSC_h__
#define __Foundation__IntrusiveQueueSPSC_h__

/// Interlocked functions
#include <Foundation/Interlocked.h>

namespace Threading
{
	template< typename T, typename TypeAdapter = Traits::TypeSelect< Traits::IsSmartPointer< T >::value,
																	 Traits::SmartPtrIntrusiveAdapter< T >,
																	 Traits::DefaultIntrusiveAdapter< T > >::ValueType >
	class InstrusiveQueueSPSC
	{
	private:
		struct Node
		{
			Node* myNext;
		};

	public:
		typedef InstrusiveQueueSPSC< T, TypeAdapter > ThisType;
		typedef T ReferencedValueType;
		typedef T& ReferencedRef;
		typedef const T& ReferencedConstRef;
		typedef T* ReferencedPointer;
		typedef typename TypeAdapter::ValueType ValueType;
		typedef ValueType* Pointer;
		typedef ValueType& Reference;
		typedef const ValueType* ConstPointer;
		typedef const ValueType& ConstReference;
		typedef Node NodeType;

		InstrusiveQueueSPSC( void );
		~InstrusiveQueueSPSC( void );

		void Enqueue( ReferencedRef input );
		bool Dequeue( ReferencedPointer& output );

	private:
		NodeType* pHead; //!< Head of queue
		s8 padding[gCacheLine - sizeof( NodeType* ) % gCacheLine]; //!< Padding magic

		NodeType* pTail; //!< Tail of queue
	};

	template< typename T, typename TypeAdapter > 
	void InstrusiveQueueSPSC< T, TypeAdapter >::Enqueue( ReferencedRef rhs )
	{
		NodeType* n = TypeAdapter::getPointerFrom( rhs );

		n->next = nullptr;
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( &this->pTail->next, n );
		this->pTail = n;

		/// In release builds compiler will optimize out this function call if DefaultIntrusiveAdapter used 
		TypeAdapter::appendedTo( const_cast<ValueType*>(&rhs) );
	}

	template< typename T, typename TypeAdapter >
	bool InstrusiveQueueSPSC< T, TypeAdapter >::Dequeue( ReferencedPointer& output )
	{
		if( nullptr != Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( &this->pHead->next ) ) {
			NodeType* node = this->pHead->next;

			/// In release builds compiler will optimize out this function call if DefaultIntrusiveAdapter used 
			TypeAdapter::removedFrom( node );
			TypeAdapter::setPointerTo( node, output );

			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( &this->pHead, this->pHead->next );
			return true;
		} else {
			return false;
		}
	}
}

#endif /// __Foundation__IntrusiveQueueMPSC_h__