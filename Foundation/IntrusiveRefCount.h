/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Interlocked
#include <Foundation/Interlocked.h>

namespace Core
{
	template< typename ReferencesType = SizeT >
	class IntrusiveRefCount
	{
	protected:
		ReferencesType myReferences; //!< Reference counter for engine objects

	public:
		typedef ReferencesType ValueType;

		/// <summary cref="ReferenceCount::ReferenceCount">
		/// Constructor.
		/// </summary>
		IntrusiveRefCount( void );
		virtual ~IntrusiveRefCount( void ) { }

		/// <summary cref="IntrusiveRefCount::SetRefCount">
		/// </summary>
		template< Threading::MemoryOrder Order >
		void SetRefCount( ReferencesType refs );

		/// <summary cref="ReferenceCount::GetRefCount">
		/// Get all references.
		/// </summary>
		/// <returns>References count.</returns>
		template< Threading::MemoryOrder Order >
		ReferencesType GetRefCount( void ) const;

		/// <summary cref="ReferenceCount::AddRef">
		/// Increment reference count.
		/// </summary>
		template< Threading::MemoryOrder Order >
		void AddRef( ReferencesType references = 1 );

		/// <summary cref="ReferenceCount::Release">
		/// Release reference.
		/// </summary>
		template< Threading::MemoryOrder Order >
		ReferencesType Release( ReferencesType references = 1 );
	};

	template< typename ReferencesType >
	__IME_INLINED IntrusiveRefCount<ReferencesType>::IntrusiveRefCount( void )
	{
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release, ReferencesType, ReferencesType >( this->myReferences, 0 );
	}

	template< typename ReferencesType >
	template< Threading::MemoryOrder Order >
	__IME_INLINED void IntrusiveRefCount<ReferencesType>::SetRefCount( ReferencesType refs )
	{
		Threading::Interlocked::Store< Order, ReferencesType, ReferencesType >( this->myReferences, refs );
	}

	template< typename ReferencesType >
	template< Threading::MemoryOrder Order >
	__IME_INLINED ReferencesType IntrusiveRefCount<ReferencesType>::GetRefCount( void ) const
	{
		return Threading::Interlocked::Fetch< Order >( this->myReferences );
	}

	template< typename ReferencesType >
	template< Threading::MemoryOrder Order >
	__IME_INLINED void IntrusiveRefCount<ReferencesType>::AddRef( ReferencesType references )
	{
		/// Increment reference value by blocking it
		Threading::Interlocked::FetchAdd< Order, ReferencesType, ReferencesType >( this->myReferences, references );
	}

	template< typename ReferencesType >
	template< Threading::MemoryOrder Order >
	__IME_INLINED ReferencesType IntrusiveRefCount<ReferencesType>::Release( ReferencesType references )
	{
		typedef typename Traits::SignedUnsignedInversion<ReferencesType>::Type SignedType;
		return Threading::Interlocked::FetchAdd< Order, ReferencesType, typename Traits::SignedUnsignedInversion<ReferencesType>::Type >( this->myReferences, -static_cast<SignedType>(references) ) - references;
	}
}