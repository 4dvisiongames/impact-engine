/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#ifndef __Foundation__IntrusiveSListMPMC_h__
#define __Foundation__IntrusiveSListMPMC_h__

/// Interlocked functions
#include <Foundation/Interlocked.h>

namespace Threading
{
	class IntrusiveSListMPMCNode
	{
		template< typename T, typename TypeAdapter >
		friend class IntrusiveSListMPMC;

	private:
		void MarkForDeath( IntrusiveSListMPMCNode** head );
		bool IsNeedToBeMarked();

		static void LinkNodeToList( IntrusiveSListMPMCNode** head, IntrusiveSListMPMCNode* from );
		static void UnlinkNodeFromList( IntrusiveSListMPMCNode* head, IntrusiveSListMPMCNode** to );

		IntrusiveSListMPMCNode* myNext; //!< My next linked value
		SizeT myABAEpoch; //!< ABA-prevention marker
		SizeT myMarkedDeath; //!< 
	};

	__IME_INLINED bool IntrusiveSListMPMCNode::IsNeedToBeMarked() {
		return Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::AcquireRelease, SizeT, SSizeT >( this->myABAEpoch, -1 ) == (SizeT)1;
	}

	__IME_INLINED void IntrusiveSListMPMCNode::MarkForDeath( IntrusiveSListMPMCNode** h ) {
		IntrusiveSListMPMCNode* head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( *h );
		while ( true ) {
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( this->myMarkedDeath, 0 );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myNext, head );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release, SizeT, SizeT >( this->myABAEpoch, 1 );
			if ( Threading::Interlocked::CompareAndSwap( *h, this, head ) != *h ) {
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( this->myMarkedDeath, 1 );
				if ( this->IsNeedToBeMarked() )
					continue;
			}

			return;
		}
	}

	__IME_INLINED void IntrusiveSListMPMCNode::LinkNodeToList( IntrusiveSListMPMCNode** head, 
															   IntrusiveSListMPMCNode* node ) {
		/// TODO: make this a bit weaker with consume semantics
		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::AcquireRelease, SizeT, SizeT >( node->myABAEpoch, 1 );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release, SizeT, SizeT >( node->myMarkedDeath, 1 );
		if ( node->IsNeedToBeMarked() )
			node->MarkForDeath( head );
	}

	__IME_INLINED void IntrusiveSListMPMCNode::UnlinkNodeFromList( IntrusiveSListMPMCNode* head, 
																   IntrusiveSListMPMCNode** to ) {
		IntrusiveSListMPMCNode* myHead = 
			Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( head );

		while ( myHead != nullptr ) {
			IntrusiveSListMPMCNode* prev = myHead;
			SizeT abaEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( myHead->myABAEpoch );
			if ( abaEpoch == 0 || Threading::Interlocked::CompareAndSwap< SizeT, SizeT, SizeT >( myHead->myABAEpoch, abaEpoch + 1, abaEpoch ) != abaEpoch ) {
				myHead = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( head );
			}

			IntrusiveSListMPMCNode* next = 
				Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( myHead->myNext );

			if ( Threading::Interlocked::CompareAndSwap( head, next, myHead ) == myHead ) {
				if (myHead != nullptr)
					Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Relaxed, SizeT, SSizeT >( myHead->myABAEpoch, -2 );
				*to = myHead;
			}

			if ( prev->IsNeedToBeMarked() && Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( prev->myMarkedDeath ) == (SizeT)1 )
				prev->MarkForDeath( &head );
		}

		/// Default value outputs on failure or other 
		*to = nullptr;
	}

	/// <summary>
	/// <c>IntrusiveSListMPMC</c> is a thread-safe multiple producers, multiple consumers 
	/// intrusive singly-linked list.
	/// </summary>
	template< typename T, typename TypeAdapter = Traits::TypeSelect< Traits::IsSmartPointer< T >::value,
																	 Traits::SmartPtrIntrusiveAdapter< T >,
																	 Traits::DefaultIntrusiveAdapter< T > >::ValueType >
	class IntrusiveSListMPMC
	{
		__IME_DISABLE_ASSIGNMENT( IntrusiveSListMPMC );

	public:
		typedef T ReferencedValueType;
		typedef T& ReferencedRef;
		typedef const T& ReferencedConstRef;
		typedef T* ReferencedPointer;
		typedef typename TypeAdapter::ValueType ValueType;
		typedef ValueType* Pointer;
		typedef ValueType& Reference;
		typedef const ValueType* ConstPointer;
		typedef const ValueType& ConstReference;
		typedef SizeT SizeType;
		typedef IntrusiveSListMPMCNode NodeType;

		IntrusiveSListMPMC( void );
		~IntrusiveSListMPMC( void );

		void Prepend( ReferencedRef rhs );
		bool GetBack( ReferencedPointer& output );
		bool IsEmpty( void ) const;
		SizeT Size( void ) const;

	private:
		NodeType* myHead;
		SizeT myNodes;
	};

	template< typename T, typename TypeAdapter >
	IntrusiveSListMPMC< T, TypeAdapter >::IntrusiveSListMPMC( void ) : myHead( nullptr ), myNodes( 0 )
	{
	}

	template< typename T, typename TypeAdapter >
	IntrusiveSListMPMC< T, TypeAdapter >::~IntrusiveSListMPMC( void )
	{
		__IME_ASSERT( myHead == nullptr );
	}

	template< typename T, typename TypeAdapter >
	void IntrusiveSListMPMC< T, TypeAdapter >::Prepend( ReferencedRef rhs )
	{
		NodeType* node = static_cast< NodeType* >( &rhs );
		NodeType::LinkNodeToList( &this->myHead, node );
		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SizeT >( this->myNodes, 1 );

		/// In release builds compiler will optimize out this function call if DefaultIntrusiveAdapter used 
		TypeAdapter::appendedTo( &rhs );
	}

	template< typename T, typename TypeAdapter >
	bool IntrusiveSListMPMC< T, TypeAdapter >::GetBack( ReferencedPointer& output )
	{
		NodeType* result = nullptr;
		NodeType::UnlinkNodeFromList( this->myHead, &result );
		if ( !result )
			return false;

		Pointer node = static_cast< Pointer >(result);
		/// In release builds compiler will optimize out this function call if DefaultIntrusiveAdapter used 
		TypeAdapter::removedFrom( node );
		TypeAdapter::setPointerTo( node, output );

		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SSizeT >( this->myNodes, -1 );
		return true;
	}

	template< typename T, typename TypeAdapter >
	bool IntrusiveSListMPMC< T, TypeAdapter >::IsEmpty( void ) const
	{
		NodeType* res = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myHead );
		return res == nullptr;
	}

	template< typename T, typename TypeAdapter >
	SizeT IntrusiveSListMPMC< T, TypeAdapter >::Size( void ) const
	{
		return Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myNodes );
	}
}

#endif /// __Foundation__IntrusiveListMPMC_h__

/*
	change log:

	15-Mar-15	codepoet	method IntrusiveSListMPMCNode::UnlinkNodeFromList	fixed possible dereferencing of null pointer
*/