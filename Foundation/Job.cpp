/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Foundation/Job.h>
#include <Foundation/JobServer.h>
#include <Foundation/JobDispatcher.h>
#include <Foundation/MemoryLocklessFreeList.h>

namespace Jobs
{
	/// [Codepoet]: seems like strange bug of lexical analyzer, because we cannot just pass templated object type to
	/// __IME_IMPLEMENT_CUSTOM_ALLOCATOR preprocessor definition - so, used such workaround via typedef...

	typedef Memory::LocklessFreeList< Job, 256 > JobsAllocator;
	__IME_IMPLEMENT_CUSTOM_ALLOCATOR( Jobs::Job, JobsAllocator );

	Job::Job( void ) 
		: myState(JobStateEnum::Allocated)
		, myExtraState(0)
		, myParent(nullptr)
		, myAffinityID(0)
		, myValidation(false) 
	{
		this->myReferences = 0;
	}

	Job::~Job( void ) 
	{
		if (this->IsValid())
			this->Discard();
	}

	void Job::Setup() 
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT_MSG( this->GetState() == JobStateEnum::Allocated, "Attempt to setup job that is not in 'JobStateEnumList::Allocated' state" );
		this->myState = JobStateEnum::Ready;
		if (Job* parent = this->GetParent()) {
			SizeT references = parent->GetRefCount< Threading::MemoryOrderEnum::Acquire >();
			__IME_ASSERT_MSG( references >= 0, "Attempt to spawn job whose parent has a reference count < 0" );
			__IME_ASSERT_MSG( references != 0, "Attempt to spawn job whore reference count is zero.(forgot set SetRefCount?)" );
			parent->myExtraState |= JobExtraStateEnum::StillAlive;
		}

		u32 affinity = this->myAffinityID;
		if (affinity != 0) {
			this->myExtraState |= JobExtraStateEnum::Mailboxed;
		}

		this->myValidation = true;
	}

	void Job::Discard() 
	{
		__IME_ASSERT( this->IsValid() );

		/// This is for sharing manual and automatic memory reclamation, because of reference counter
		if ((this->myExtraState & JobExtraStateEnum::IsDone) == 0) 
		{
			Job* parent = this->GetParent();
			if (parent) 
			{
				__IME_ASSERT_MSG( (parent->GetState() != JobStateEnum::Free) && (parent->GetState() != JobStateEnum::Ready), 
								  "Attempt to destroy child of running or corrupted parent job?" );
				/// 'Reexecute' and 'Running' are also signs of a race condition, since most tasks
				/// set their myReferences upon entry but "StillAlive" should detect this.
				parent->Release< Threading::MemoryOrderEnum::Sequental >();
				/// Even if the last reference to *parent is removed, it should not be spawned (documented behavior).
			}
		}

		this->myValidation = false;
	}
}