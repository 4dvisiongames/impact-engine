/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Reference counter
#include <Foundation/IntrusiveRefCount.h>
/// Job state defintion
#include <Foundation/JobState.h>
/// Weak pointer
#include <Foundation/WeakPtr.h>
/// Enumeration
#include <Foundation/Enumeration.h>
/// Intrusive SList
#include <Foundation/STLIntrusiveSList.h>
/// Tuple
//#include <Foundation/STLTuple.h>

namespace Jobs
{
	/// <summary>
	/// <c>Job</c> is a basic entry point for all job tasks to be scheduled by dispatchers. One of the most
	/// important part is fully automatic job "life" management. So you don't need to use them with <c>IntrusivePtr<></c>
	/// or <c>IntrusiveWeakPtr<></c> to do any RAII-style reference up-/downcounting. Just allocate as usual object via <c>new</c> 
	/// and pass it to <c>Dispatcher</c>. 
	/// </summary>
	/// <remarks>
	/// <c>Job</c> is also inherits from <c>STL::IntrusiveSListQueue<></c> to support batch posting onto <c>Dispatcher</c>.
	/// </remarks>
	class Job : public NoAssign, public Core::IntrusiveRefCount<>, public STL::IntrusiveSList< Job >::NodeType
	{
		__IME_DECLARE_CUSTOM_ALLOCATOR;

		friend class Server;
		friend class Dispatcher;

		//! My parent job
		Job* myParent;
		//! My extra state
		u8 myExtraState;
		//! My primary state
		u8 myState;
		//! My affinity ID
		u32 myAffinityID;
		//! My validation
		bool myValidation;

		void ResetExtraState();

	protected:
		//! Main executable function of job
		virtual Job* Execute();

	public:
		Job();
		virtual ~Job();

		/// <summary cref="Job::Setup">
		/// </summary>
		void Setup();

		/// <summary cref="Job::Discard">
		/// </summary>
		void Discard();

		/// <summary cref="Job::IsValid">
		/// </summary>
		bool IsValid() const;

		/// <summary cref="Job::IsDone">
		/// Non-blocking method to check if current job is done.
		/// </summary>
		/// <remarks>
		/// Do not block execution via this method like <c>while(!IsDone) { ... };</c>, because it will block current thread.
		/// Only when it necessary, but anyway it's better to wait using <c>Dispatcher::Wait</c> method.
		/// </remarks>
		bool IsDone() const;

		/// <summary cref="Job::RescheduleAsContinuation">
		/// Reschedules selected job as continuation of other one. This works in one context.
		/// </summary>
		void RescheduleAsContinuation();

		/// <summary cref="Job::RescheduleAsChildOf">
		/// </summary>
		void RescheduleAsChildOf( Job* t );

		/// <summary cref="Job::RescheduleAsReExecute">
		/// </summary>
		void RescheduleAsReExecute();

		/// <summary cref="Job::SetAffinity">
		/// </summary>
		void SetAffinity( u32 affinity );

		/// <summary cref="Job::SetParent">
		/// </summary>
		void SetParent( Job* parent );

		/// <summary cref="Job::GetParent">
		/// </summary>
		Job* GetParent();

		/// <summary cref="Job::GetAffinity">
		/// </summary>
		u32 GetAffinity() const;

		/// <summary cref="Job::GetState">
		/// </summary>
		JobState GetState() const;
	};

	typedef STL::IntrusiveSList< Job > JobList;

	__IME_INLINED void Job::RescheduleAsContinuation( void ) {
		__IME_ASSERT_MSG( this->myState == JobStateEnum::Running, "Execute not running?" ); 
		this->myState = JobStateEnum::Recycle; 
	}

	__IME_INLINED void Job::RescheduleAsChildOf( Job* t ) {
		__IME_ASSERT_MSG( t, "Parent job must be valid!" );
		__IME_ASSERT_MSG( this->myState == JobStateEnum::Running|| this->myState == JobStateEnum::Allocated, "Execute not running, or already recycled" );
		__IME_ASSERT_MSG( this->myReferences == 0, "No child tasks allowed when recycled as a child." );
		__IME_ASSERT_MSG( this->myParent == nullptr, "Parent must be nullptr." );
		__IME_ASSERT_MSG( t->myState <= JobStateEnum::Recycle, "Corrupt parent's state." );
		__IME_ASSERT_MSG( t->myState != JobStateEnum::Free, "Parent already freed." );
		this->myState = JobStateEnum::Allocated;
		this->myParent = t;
		if( this->myParent )
			Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SizeT >( this->myParent->myReferences, 1 );
	}

	__IME_INLINED void Job::RescheduleAsReExecute( void ) {
		__IME_ASSERT_MSG( this->myState == JobStateEnum::Running, "Execute not running, or already recycled." );
		__IME_ASSERT_MSG( this->myReferences == 0, "No child tasks allowed when re-scheduled for reexecution." );
		this->myState = JobStateEnum::Reexecute;
	}

	__IME_INLINED Job* Job::Execute( void ) {
		return nullptr;
	}

	__IME_INLINED bool Job::IsValid( void ) const {
		bool result = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myValidation );
		return result;
	}

	__IME_INLINED bool Job::IsDone( void ) const {
		u8 extraState = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myExtraState );
		bool result = (Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myReferences ) == 0);
		result &= ((extraState & JobExtraStateEnum::IsDone) != 0);
		return result;
	}

	__IME_INLINED void Job::SetAffinity( u32 affinity ) {
		this->myAffinityID = affinity;
	}

	__IME_INLINED void Job::SetParent( Job* parent ) {
		this->myParent = parent;
	}

	__IME_INLINED Job* Job::GetParent( void ) {
		return this->myParent;
	}

	__IME_INLINED u32 Job::GetAffinity( void ) const {
		u32 result = this->myAffinityID;
		return result;
	}

	__IME_INLINED JobState Job::GetState( void ) const {
		JobState result = JobState( this->myState );
		return result;
	}

	__IME_INLINED void Job::ResetExtraState( void ) {
		this->myExtraState &= ~(JobExtraStateEnum::IsStolen | JobExtraStateEnum::Enqueued);
	}


	/// <summary>
	/// <c>LambdaJob</c> is an implementation of <c>Jobs::Job</c> with C++11 lambda function support.
	/// </summary>
	template< typename Function >
	class LambdaJob : public Job
	{
	public:
		/// <summary cref="LambdaJob::LambdaJob">
		/// </summary>
		/// <param name="function">Lambda function or just function pointer.</param>
		/// <param name="arguments">All needed arguments for lambda function.</param>
		LambdaJob( Function&& function );

	private:
		Function myFunction;
		//STL::Tuple< Args... > myArguments;

		virtual Job* Execute( void );
	};

	template< typename Function >
	__IME_INLINED LambdaJob< Function >::LambdaJob( Function&& function )
		: myFunction( function )
		/*, myArguments( STL::Forward<Args>( arguments )... ) */ {

	}

	template< typename Function >
	__IME_INLINED Job* LambdaJob< Function >::Execute( void ) {
		return this->myFunction(/* this->myArguments */);
	}
}