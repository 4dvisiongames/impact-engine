/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Jobs area
#include <Foundation/JobArea.h>
/// Jobs dispatching
#include <Foundation/JobDispatcher.h>
/// Jobs threading server
#include <Foundation/JobServer.h>
/// Threads
#include <Foundation/Thread.h>
/// Default scoped lockers
#include <Foundation/ScopedLock.h>
/// Core mathematics
#include <Foundation/Math.h>
/// STL fast reverse vector
#include <Foundation/STLFastReverseVector.h>

namespace Jobs
{
	IntrusivePtr< Area > Area::Create(SizeT maxNumWorkers)
	{
		__IME_ASSERT_MSG( maxNumWorkers >= 0, "Please specify workers count as unsigned value!" );
		if( !maxNumWorkers )
			maxNumWorkers = Threading::Thread::GetAvailableConcurrency() - 1;

		SizeT mSize = sizeof( Pool );
		mSize *= maxNumWorkers; // Primary thread job pool already has it's space in 'size'
		mSize += sizeof( Area );

		Area* ptr = static_cast< Area* >( Memory::Allocator::Malloc( mSize ) );
		__IME_ASSERT(ptr);

		Memory::LowLevelMemory::EraseMemoryBlock( ptr, mSize );
		new( ptr ) Area( maxNumWorkers );

		return ptr;
	}

	Area::Area(SizeT maxNumWorkers) 
		: myValidation(false)
		, myServer(nullptr)
	{ 
		this->myMaxNumWorkers = maxNumWorkers;
		this->myNumJobPools = Math::TMax< SizeT >( 2, maxNumWorkers + 1 );
		this->myNumWorkersRequested = 0;
		this->myNumWorkersAllotted = 0;
		this->myLimit.data = 1;
		this->myNumWorkersReferences.SetRefCount< Threading::MemoryOrderEnum::Relaxed >( 1 );
	}

	Area::~Area() 
	{
		if( this->IsValid() )
			this->Discard();
	}

	void Area::Setup()
	{
		__IME_ASSERT( !this->myValidation );
		__IME_ASSERT( this->myMaxNumWorkers < this->myNumJobPools );

		for( SizeT idx = 0; idx < this->myNumJobPools; ++idx )
		{
			Pool* pool = &this->myJobPools[idx];
			__IME_ASSERT( !pool->myDispatcher.IsValid() && !pool->myJobPool );
			__IME_ASSERT( !pool->myJobPoolPtr );
			__IME_ASSERT( !pool->myJobPoolSize );
			new( pool ) Pool();
		}

		this->myValidation = true;
	}

	void Area::Discard()
	{
		__IME_ASSERT( this->myValidation );
		__IME_ASSERT_MSG( !this->myNumWorkersRequested && !this->myNumWorkersAllotted, "Destroying area has requesting workers!" );

#if defined( DEBUG )
		PoolState state = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myState );
		__IME_ASSERT_MSG( state == Area::theSnapshotEmptyOrLock || !this->myMaxNumWorkers, "Inconsistend state of current area" );
#endif

		for( SizeT idx = 0; idx < this->myNumJobPools; ++idx ) 
		{
			Pool* ptr = &this->myJobPools[idx];
			ptr->myDispatcher = nullptr;
			__IME_ASSERT_MSG( !ptr->myDispatcher.IsValid(), "Dispatcher must be released from pool before destroying" );
			__IME_ASSERT_MSG( ptr->IsQuiescentEmptyLocalPool(), "Head and tail must be the same, meaning job pool is empty" );
			ptr->Discard();
		}

		this->myValidation = false;
	}

	void Area::AdjustJobDemand( SSizeT demand ) 
	{
		__IME_ASSERT( this->myServer.IsValid() );
		if( !demand )
			return;

		this->myAreaAccessGuard.Lock();
		SSizeT previous = this->myNumWorkersRequested;
		this->myNumWorkersRequested += demand;
		if( this->myNumWorkersRequested <= 0 ) 
		{
			this->myNumWorkersAllotted = 0;
			if( previous <= 0 ) 
			{
				this->myAreaAccessGuard.Unlock();
				return;
			}
			demand = -previous;
		}
		this->myAreaAccessGuard.Unlock();

		__IME_ASSERT_MSG( previous >= 0, "Partial request from Jobs::Server?" );
		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental >( myServer->myTotalDemand, demand );

		myServer->UpdateWorkerAllotment();
		///! Signal jobs server we have some work to execute or just to release
		///! some jobs from thread exector to prevent exit-from-application problems.
		myServer->SignalAvailableWork( demand );
	}

	bool Area::IsSnapshotFull() 
	{
		/// Use unique id for "busy" in order to avoid ABA problems.
		const PoolState busy = PoolState( &busy );
		/// Request permission to take snapshot
		if( Threading::Interlocked::CompareAndSwap( this->myState, busy, Area::theSnapshotFull ) == Area::theSnapshotFull ) {
			/// Got permission. Take the snapshot.
            /// NOTE: This is not a lock, as the state can be set to FULL at 
            ///       any moment by a thread that spawns/enqueues new job.
			SizeT n = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myLimit.data );

			/// Make local copies of volatile parameters. Their change during
            /// snapshot taking procedure invalidates the attempt, and returns
            /// this thread into the dispatch loop.
			SizeT k;
			for( k = 0; k < n; ++k ) 
			{
				Pool* p = &this->myJobPools[k];
				__IME_ASSERT( p );
				SizeT head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( p->myHead );
				SizeT tail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( p->myTail );
				Job** localJobPool = Threading::Interlocked::Fetch< Threading::MemoryOrder::Acquire >( p->myJobPool );
				if( localJobPool != Pool::theEmptyPool && head < tail )
					break;
			}
			__IME_ASSERT( k <= n );
			bool workAbsent = k == n;
			if( Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myState ) == busy ) {
				if( workAbsent ) 
				{
					/// save current demand value before setting SNAPSHOT_EMPTY,
                    /// to avoid race with advertise_new_work.
					SizeT currentDemand = this->myMaxNumWorkers;
					if( Threading::Interlocked::CompareAndSwap( this->myState, Area::theSnapshotEmptyOrLock, busy ) == busy ) 
					{
						this->AdjustJobDemand( -currentDemand );
						return true;
					}
					return false;
				}
				/// Undo previous transition SNAPSHOT_FULL-->busy, unless another thread undid it.
				Threading::Interlocked::CompareAndSwap( this->myState, Area::theSnapshotFull, busy );
			}
		}
		return false;
	}

	bool Area::IsOutOfJobs() 
	{
		__forever 
		{
			PoolState localState = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myState );
			switch( localState ) 
			{
			case Area::theSnapshotEmptyOrLock:
				return true;

			case Area::theSnapshotFull:
				return this->IsSnapshotFull();

			default: // Another thread is taking a snapshot.
				return false;
			};
		}
	}

	SizeT Area::GetWorkersNodeCount() const
	{
		SizeT result = 0;
		for( SizeT idx = 0; idx < this->myNumJobPools; ++idx )
			if( IntrusivePtr< Dispatcher > dispatcher = this->myJobPools[idx].myDispatcher )
				result += dispatcher->GetJobNodeCount();

		return result;
	}

	void Area::NotifyOnJobs( void ) 
	{
		/// Double-check idiom that, in case of spawning, is deliberately sloppy about memory fences.Technically, to 
		/// avoid missed wakeups, there should be a full memory fence between the point we released the job pool 
		/// (i.e. spawned Job) and read the arena's state.  However, adding such a fence might hurt overall performance
		/// more than it helps, because the fence would be executed on every job pool release, even when stealing does
		/// not occur.
		PoolState snapshot = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myState );
		if( snapshot < Area::theSnapshotFull ) 
		{
			if( Threading::Interlocked::CompareAndSwap( this->myState, theSnapshotFull, snapshot ) == theSnapshotEmptyOrLock ) {
				if( snapshot != theSnapshotEmptyOrLock ) 
				{
					/// This thread read busy state into snapshot, and then another thread transitioned myPoolState to 
					/// empty state in the meantime, which caused the CompareAndSwap above to fail. Attempt to transition
					/// myPoolState from empty to full.
					if( Threading::Interlocked::CompareAndSwap( this->myState, theSnapshotFull, theSnapshotEmptyOrLock ) != theSnapshotEmptyOrLock ) 
					{
						/// Some other thread transitioned myPoolState from empty state, and hence became
						/// responsible for waking up workers.
						return;
					}
				}
				this->AdjustJobDemand( this->myMaxNumWorkers );
			}
		}
	}

	void Area::SpawnOnPool( Jobs::Job* job, SizeT dispatcherIndex ) {
		__IME_ASSERT_MSG( dispatcherIndex <= this->myMaxNumWorkers, "Dispatcher index must be lower than maximal number of workers to process within this Area!" );
		Jobs::Pool* PoolPtr = &this->myJobPools[dispatcherIndex];

		SizeT tail = PoolPtr->Setup( 1 );
		PoolPtr->myJobPoolPtr[tail] = job;
		tail += 1;
		__IME_ASSERT( tail < PoolPtr->myJobPoolSize );

		/// Release fence is necessary to make sure that previously stored <c>Job</c> pointers are visible to thieves.
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( PoolPtr->myTail, tail );

		this->ValidateCurrentPool( PoolPtr );
	}

	void Area::SpawnOnPool( Jobs::JobList& list, SizeT dispatcherIndex ) {
		__IME_ASSERT_MSG( dispatcherIndex <= this->myMaxNumWorkers, "Dispatcher index must be lower than maximal number of workers to process within this Area!" );
		Jobs::Pool* PoolPtr = &this->myJobPools[dispatcherIndex];

		/// Job list is being spawned, so need to allocate enough space for them
		Job* array[Pool::minJobPoolSize];
		STL::FastReverseVector< Job* > jobs( array, Pool::minJobPoolSize );

		JobList::Iterator iter = list.Begin();
		while ( iter != list.End() ) {
			JobList::Iterator current = iter++;
			Jobs::Job& j = *current;

			jobs.push_back( &j );
		}
		list.Clear();

		SizeT numJobs = jobs.size();
		SizeT tail = PoolPtr->Setup( numJobs );
		jobs.copy_memory( PoolPtr->myJobPoolPtr + tail );
		tail += numJobs;
		__IME_ASSERT( tail < PoolPtr->myJobPoolSize );

		/// Release fence is necessary to make sure that previously stored job pointers are visible to thieves.
		__IME_ASSERT_MSG( tail <= PoolPtr->myJobPoolSize, "Job queue end was overwritten! Possible programmer's fault." );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( PoolPtr->myTail, tail );

		this->ValidateCurrentPool( PoolPtr );
	}
}