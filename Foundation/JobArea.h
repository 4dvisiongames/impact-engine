/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Object reference counter
#include <Foundation/RefCount.h>
/// Intrusive reference counter
#include <Foundation/IntrusiveRefCount.h>
/// Pool
#include <Foundation/JobPool.h>
/// Job
#include <Foundation/Job.h>
/// STL array
#include <Foundation/STLArray.h>
/// STL intrusive list
#include <Foundation/STLIntrusiveList.h>

namespace Jobs
{
	class AreaCacheLine : public STL::IntrusiveListNode, public Core::ReferenceCount
	{
		friend class Server;
		friend class Dispatcher;

	public:
		typedef uintptr_t PoolState;

	protected:
		static const PoolState theSnapshotEmptyOrLock = 0;
		static const PoolState theSnapshotFull = (uintptr_t)(-1);

		typedef Threading::SpinWait AreaLocker;

		//! Current pool state
		PoolState myState;
		//! My maximal currently busy job pool index
		Threading::InterlockedPODType< SizeT > myLimit;
		//! My max num job pools
		SizeT myNumJobPools;
		//! Number of workers requested by the master thread owning the idArena
		SizeT myMaxNumWorkers;
		//! Number of workers that are executes on current area
		Core::IntrusiveRefCount<> myNumWorkersReferences;
		//! Number of workers that are currently requested from the resource manager
		SSizeT myNumWorkersRequested;
		//! Number of workers that have been marked out by the resource manager to service the area
		SSizeT myNumWorkersAllotted;
		//! My access guard grant
		AreaLocker myAreaAccessGuard;
	};

	class Area : public Padded< AreaCacheLine >
	{
	private:
		friend class Server;
		friend class Dispatcher;

		//! My validation
		bool myValidation;
		//! My server that holds me
		IntrusiveWeakPtr< Server, SmartPointerPolicy< Server, SmartPointerThreadSafeOp > > myServer;
		//! Array of attached job pools to the area(must be always the last one!)
		Pool myJobPools[1];

		void AdjustJobDemand( SSizeT demand );

		bool IsSnapshotFull( void );

		void ValidateCurrentPool(Jobs::Pool* pool);

		/// <summary cref="Area::Area">
		/// Forbidden operation, create through <c>Create</c> function.
		/// </summary>
		Area( SizeT maxNumWorkers );

	public:
		/// <summary cref="Area::Create">
		/// </summary>
		static IntrusivePtr< Area > Create(SizeT maxNumWorkers = 0);

		/// <summary cref="Area::~Area">
		/// </summary>
		virtual ~Area();

		/// <summary cref="Area::Setup">
		/// </summary>
		void Setup();

		/// <summary cref="Area::Discard">
		/// </summary>
		void Discard();

		/// <summary cref="Area::IsValid">
		/// </summary>
		bool IsValid() const;

		/// <summary cref="Area::SpawnOnPool">
		/// Appends one job into job pool queue.
		/// </summary>
		/// <param name="job">Pointer to a single instance of job.</param>
		/// <param name="dispatcherIndex">Index of current dispatcher that locates index of thread.</param>
		void SpawnOnPool( Jobs::Job* job, SizeT dispatcherIndex );

		/// <summary cref="Area::SpawnOnPool">
		/// Appends list of jobs into job pool queue.
		/// </summary>
		/// <param name="list">Reference to a list of jobs.</param>
		/// <param name="dispatcherIndex">Index of current dispatcher that locates index of thread.</param>
		void SpawnOnPool( Jobs::JobList& list, SizeT dispatcherIndex );

		/// <summary cref="Area::NotifyOnJobs">
		/// Notifies area observer on some added jobs to be executed from job pool. Always call this
		/// method yourself after calling <c>SpawnOnPool</c> method.
		/// </summary>
		void NotifyOnJobs();

		/// <summary cref="Area::GetWorkersNodeCount">
		/// Selects workers count.
		/// </summary>
		/// <returns>Count of workers got from traversing job pools.</returns>
		/// <remarks>
		/// This algorithm traverses all job pools and looks if current pool has attached dispatcher.
		/// If job pool has dispatcher, then workersCount += 1.
		/// </remarks>
		SizeT GetWorkersNodeCount() const;

		/// <summary cref="Area::GetActiveNumWorkers">
		/// Selects workers count that may access simultaneously this area of job pools.
		/// </summary>
		/// <returns>Number of workers that has access to this area.</returns>
		SizeT GetActiveNumWorkers() const;

		/// <summary cref="Area::IsOutOfJobs">
		/// Checks if current area doesn't have any jobs on job pools to be processed.
		/// </summary>
		/// <returns>True if no jobs, othrwise false.</returns>
		bool IsOutOfJobs();
	};

	__IME_INLINED bool Area::IsValid() const {
		bool result = this->myValidation;
		return result;
	}

	__IME_INLINED SizeT Area::GetActiveNumWorkers() const {
		return this->myNumWorkersReferences.GetRefCount< Threading::MemoryOrderEnum::Acquire >() >> 1;
	}

	__IME_INLINED void Area::ValidateCurrentPool( Jobs::Pool* pool ) {
		/// Check if pooled index of current attached area is not empty, e.g. we need to lock it 
		if ( pool->IsEmpty() ) {
			__IME_ASSERT_MSG( pool->IsValid(), "No area here: initialization not completed?" );
			__IME_ASSERT_MSG( pool->IsEmpty(), "Someone else grabbed this pool from area" );

			SizeT head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( pool->myHead );
			SizeT tail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( pool->myTail );
			__IME_ASSERT_MSG( head < tail, "Entering area's job pool, while it doesn't have any jobs to be shared!" );
			// Release signal on behalf of previously spawned tasks (when this thread was not in idArena yet)
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( pool->myJobPool, pool->myJobPoolPtr );
		}
	}
}