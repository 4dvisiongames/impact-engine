/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Job dispather
#include <Foundation/JobDispatcher.h>
/// Job pool
#include <Foundation/JobPool.h>
/// Job area of pools
#include <Foundation/JobArea.h>
/// Job server
#include <Foundation/JobServer.h>
/// Thread implementation
#include <Foundation/Thread.h>
/// Fast reverse array
#include <Foundation/STLFastReverseVector.h>
/// CPU caching instructions
#include <Foundation/CPUCaching.h>

namespace Jobs
{
	struct SchedulerTraits 
	{
		static const bool ittPossible = false;
#if __IME_x86_32 || __IME_x86_64
		static const bool hasSlowAtomic = true;
#else
		static const bool hasSlowAtomic = false;
#endif /* __IME_x86_32 || __IME_x86_64 */
	};

	__IME_IMPLEMENT_THREAD_LOCAL_SINGLETON( Dispatcher );

	Dispatcher::Dispatcher( void ) 
		: myWorker( false )
		, myCurrentPoolIdx( 0 )
		, myCurrentPool( nullptr )
		, myInnermostRunningJob( nullptr )
		, myCurrentJob( nullptr )
		, myDummyJob( nullptr )
		, myJobNodeCount( 0 )
		, mySmallJobCount( 0 )
		, myStealingThreshold( 0 )
		, myThreadId( 0 )
		, myValidation( false )
		, myRandomizer( this ) 
	{
		__IME_CONSTRUCT_SINGLETON;
	}

	Dispatcher::~Dispatcher( void ) 
	{
		__IME_DESTRUCT_SINGLETON;
	}

	void Dispatcher::InitializeThreadInformation( const SizeT stackSize ) 
	{
		// Stacks are growing top-down. Highest address is called "stack base",
		// and the lowest is "stack limit".
		__IME_ASSERT_MSG( !myStealingThreshold, "Stealing threshold has already been calculated" );

		NT_TIB* pteb = reinterpret_cast< NT_TIB* >(NtCurrentTeb());

		__IME_ASSERT_MSG( &pteb < pteb->StackBase && &pteb > pteb->StackLimit, "invalid stack info in TEB" );
		__IME_ASSERT_MSG( stackSize > 0, "stack_size not initialized?" );

		/// When a worker thread is created with the attribute STACK_SIZE_PARAM_IS_A_RESERVATION, stack limit
		/// in the thread information block points to the committed part of the stack only. This renders the expression
		/// "(uintptr_t)pteb->StackBase / 2 + (uintptr_t)pteb->StackLimit / 2" virtually useless.
		/// Thus for worker threads we use the explicit stack size we used while creating them.
		/// And for master threads we rely on the following fact and assumption:
		/// - the default stack size of a master thread on Windows is 1M;
		/// - if it was explicitly set by the application it is at least as large as the size of a worker stack.
		if ( IsWorker() || stackSize < 1024 * 1024 )
			myStealingThreshold = reinterpret_cast< uintptr_t >( pteb->StackBase ) - stackSize / 2;
		else
			myStealingThreshold = reinterpret_cast< uintptr_t >( pteb->StackBase ) - 1024 * 1024 / 2;
	}

	bool Dispatcher::PredessorReadyForContinuation( _In_ Job* job, Job*& bypassJobSlot ) {
		// Check for additional childs of current checking task, so if any childs are present then we just can't allow
		// any scheduler to pick it up for further execution
		if ( SchedulerTraits::hasSlowAtomic && job->myReferences == 1 )
			job->myReferences = 0;
		else if( Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SSizeT >( job->myReferences, -1 ) > 1 )
			return false;

		__IME_MEMORY_READWRITE_BARRIER;
		__IME_ASSERT_MSG( job->myReferences == 0, 
						  "Current job must be forever alone: e.g. no additional childs attached" );
		job->myExtraState &= ~JobExtraStateEnum::StillAlive;

		if ( bypassJobSlot == nullptr )
			bypassJobSlot = job;
		else
			this->LocalSpawn( job );

		return true;
	}

	template< typename T >
	__IME_INLINED void Dispatcher::LocalSpawn( T jobOrList ) {
		__IME_ASSERT( this->myCurrentArea != nullptr );
		__IME_ASSERT( this->myCurrentPool == &this->myCurrentArea->myJobPools[this->myCurrentPoolIdx] );
		this->myCurrentArea->SpawnOnPool( jobOrList, this->myCurrentPoolIdx );

		/// Because current pool could be selected by another dispatcher
		if ( this->myCurrentPool->IsEmpty() ) {
			__IME_ASSERT_MSG( this->myCurrentPoolIdx < this->myCurrentArea->myNumJobPools, "Area's slot is out of bounds!" );
			__IME_ASSERT( this->myCurrentPool == &this->myCurrentArea->myJobPools[this->myCurrentPoolIdx] );
		}

		/// Ask dispatcher's area to share new jobs with server's store
		this->myCurrentArea->NotifyOnJobs();
	}

	void Dispatcher::Spawn( _In_ Jobs::Job* job ) {
		__IME_ASSERT( this->IsValid() );
		__IME_ASSERT( job );
		this->LocalSpawn< Jobs::Job* >( job );
	}

	void Dispatcher::Spawn( Jobs::JobList& list ) {
		__IME_ASSERT( this->IsValid() );
		__IME_ASSERT( !list.IsEmpty() );
		this->LocalSpawn< Jobs::JobList& >( list );
	}

	void Dispatcher::LocalWait( _In_ Job* job, Job* next ) {
		__IME_ASSERT_MSG( job->myReferences >= (next && next->GetParent() == job ? 2 : 1),
						  "Reference count is too small for parent job" );

		Job* t = next;
		static const SizeT parentsWorkDone = 1;
		static const SizeT allLocalWorkDone = (SizeT)3 << (sizeof( SizeT ) * 8 - 2);
		SizeT quitPoint;

		Job* oldDispatchingJob = this->myCurrentJob;
		this->myCurrentJob = this->myInnermostRunningJob;

		if ( this->MasterDispatcherLevel() )
		{
			// We are in the outermost task dispatch loop of a master thread or a worker which mimics master
			__IME_ASSERT( !this->IsWorker() || this->myCurrentJob != oldDispatchingJob );
			quitPoint = job == this->myDummyJob ? allLocalWorkDone : parentsWorkDone;
		}
		else
		{
			quitPoint = parentsWorkDone;
		}

		/// Outer loop of safe exception handling of corrupted tasks
		__forever
		{
			try
			{
				/// Outer loop receives tasks from global environment (via stealing from other threads' task pools).
				/// All exit points from the dispatch loop are located in its immediate scope.
				__forever
				{
					/// Middle loop retrieves tasks from the local task pool.
					__forever
					{
						/// Inner loop evaluates tasks coming from nesting loops and those returned
						/// by just executed tasks (bypassing spawn or enqueue calls).
						while ( t )
						this->DispatchLocal( t );

						if ( job->myReferences == quitPoint )
						{
							__IME_ASSERT( quitPoint != allLocalWorkDone );
							__IME_MEMORY_READWRITE_BARRIER;
							goto done;
						}

						if ( !this->myCurrentPool->IsEmpty() )
						{
							this->GetFromLocalQueue( t );
						}
						else
						{
							__IME_ASSERT( this->myCurrentPool->IsQuiescentReset() );
							break;
						}

						__IME_ASSERT( !t || t->myExtraState != JobExtraStateEnum::Mailboxed );

						if ( !t )
							break;
					}

						//! Get current state of dispatcher
						if ( quitPoint == allLocalWorkDone )
						{
							__IME_ASSERT( this->myCurrentPool->IsEmpty() && this->myCurrentPool->IsQuiescentReset() );
							this->myInnermostRunningJob = this->myCurrentJob;
							this->myCurrentJob = oldDispatchingJob;
							return;
						}

					// Dispatching task pointer is NULL *iff* this is a worker thread in its outermost
					// dispatch loop (i.e. its execution stack is empty). In this case it should exit it
					// either when there is no more work in the current arena, or when revoked by the market.
					t = this->ReceiveOrSteal( job->myReferences, !this->myCurrentJob );
					if ( !t )
						goto done;
				}
			}
			catch ( ... )
			{
				// TODO: add exception handling
			}

			if ( t->GetState() == JobStateEnum::Recycle )
			{
				t->myState = JobStateEnum::Allocated;
				if ( Threading::Interlocked::FetchAdd<Threading::MemoryOrderEnum::Sequental, SizeT, SSizeT>( t->myReferences, -1 ) == 1 )
				{
					// Successfully decremented reference counter
				}
				else
				{
					t = nullptr;
				}
			}
		}

done:
		this->myInnermostRunningJob = this->myCurrentJob;
		this->myCurrentJob = oldDispatchingJob;
		//! TODO: add concurrent waiting for job execution
		if ( job->myReferences != parentsWorkDone )
		{
			// This is a worker that was revoked by the market.
			__IME_ASSERT_MSG( this->IsWorker() && (!this->myCurrentJob), "Worker thread exits nested dispatch loop prematurely" );
			return;
		}

		job->myReferences = 0;
		job->myExtraState &= ~JobExtraStateEnum::StillAlive;
	}

	void Dispatcher::DispatchLocal( Job*& job ) {
		__IME_ASSERT( job->IsValid() );
		__IME_ASSERT( 1L << job->GetState() & ( 1L << JobStateEnum::Allocated | 1L << JobStateEnum::Ready | 1L << JobStateEnum::Reexecute ) );
		Job* jobNext = nullptr;
		this->myInnermostRunningJob = job;

		job->myState = JobStateEnum::Running;
		job->myAffinityID = this->myThreadId;
		jobNext = job->Execute();
		if( jobNext ) {
			__IME_ASSERT_MSG( jobNext->GetState() == JobStateEnum::Ready, "Job::Execute(): continuation-passed job must be marked as allocated only" );
			jobNext->ResetExtraState();
		}

		/// Codepoet: why do we have state checking here? It's because you can explcitly specify state of current job via
		/// RescheduleAs* functions. See them for more details. 

		switch( job->GetState() ) {
		case JobStateEnum::Running:
			__IME_ASSERT( this->myInnermostRunningJob == job );
			__IME_ASSERT_MSG( job->myReferences == 0, "Job still has child after it has been executed" );
			job->myExtraState |= JobExtraStateEnum::IsDone;
			if( job->GetParent() )
				this->PredessorReadyForContinuation( const_cast< Job* >( job->GetParent() ), jobNext );
			delete job;
			break;

		case JobStateEnum::Recycle:
			job->myState = JobStateEnum::Allocated;
			__IME_ASSERT_MSG( jobNext != job, "Only the same job can be rescheduled for further execution, and only that way" );
			job->ResetExtraState();
			this->PredessorReadyForContinuation( job, jobNext );
			break;

		case JobStateEnum::Reexecute:
			__IME_ASSERT_MSG( jobNext, "Reexecution requires that method Execute() return another job" );
			__IME_ASSERT_MSG( jobNext != job, "A job returned from method Execute() can't be rescheduled in another way" );
			job->myState = JobStateEnum::Allocated;
			job->ResetExtraState();
			this->LocalSpawn( job );
			break;

		case JobStateEnum::Allocated:
			job->ResetExtraState();
			break;

		case JobStateEnum::Ready:
			__IME_ASSERT_MSG( false, "Job is in READY state upon return from method Execute()" );
			break;

		default:
			/// This should never happen
			__IME_ASSERT_MSG( false, "Invalid job state!" );
			break;
		}

		// Make it as continuation-passed job
		job = jobNext;
	}

	bool Dispatcher::GetFromLocalQueue( Job*& res ) {
		__IME_ASSERT( !this->myCurrentPool->IsEmpty() );
		Job* result = nullptr;
		SizeT tail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myCurrentPool->myTail );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myCurrentPool->myTail, --tail );
		__IME_MEMORY_FULLCONSISTENCY_BARRIER;

		if( Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myCurrentPool->myHead ) > tail ) {
			// Just acquire(e.g. lock) job pool without need to resize the whole pool
			this->myCurrentPool->Acquire();
			SizeT head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myCurrentPool->myHead );
			if( head <= tail ) {
				// The thief backed off - grab the job instance
				result = this->myCurrentPool->myJobPoolPtr[tail];
				__IME_ASSERT( result );
				this->myCurrentPool->myJobPoolPtr[tail] = nullptr;
			}
#if defined( DEBUG )
			else {
				SizeT localHead = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCurrentPool->myHead );
				SizeT localTail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCurrentPool->myTail );
				__IME_ASSERT_MSG( head == localHead && tail == localTail && head == tail + 1, "Job pool acquisition failed, because of victim arbitration algorithm failure!" );
			}
#endif

			if( head < tail ) {
				this->myCurrentPool->Lose();
			} else {
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( this->myCurrentPool->myTail, 0 );
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( this->myCurrentPool->myHead, 0 );

				__IME_ASSERT_MSG( !this->myCurrentPool->IsEmpty(), "Not in area" );
				/// Do not reset myAreaIndex. It will be used to (attempt to) re-acquire the slot next time
				__IME_ASSERT_MSG( &this->myCurrentArea->myJobPools[this->myCurrentPoolIdx] == this->myCurrentPool, "Area of job pools slot and job pool index mismatch" );
				__IME_ASSERT_MSG( this->myCurrentPool->myJobPool == Pool::theLockedPool, "Job pool must be locked when leaving area of job pools" );
				__IME_ASSERT_MSG( this->myCurrentPool->IsQuiescentEmpty(), "Cannot leave area when the job pool is not empty" );

				/// No release fence is necessary here as this assignment precludes external
				/// accesses to the local task pool when becomes visible. Thus it is harmless
				/// if it gets hoisted above preceding local bookkeeping manipulations.
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->myCurrentPool->myJobPool, Pool::theEmptyPool );
			}
		} else {
			__IME_MEMORY_READWRITE_BARRIER;
			result = this->myCurrentPool->myJobPoolPtr[tail];
			__IME_ASSERT( result );
			this->myCurrentPool->myJobPoolPtr[tail] = nullptr;
		}

		// Assign on exit only
		res = result;
		return result || this->myCurrentPool->IsQuiescentReset();
	}

	_Ret_maybenull_ Job* Dispatcher::GetFromJobPool(_In_ Pool* stealPool) {
		__IME_ASSERT( stealPool );
		Pool* localPool = stealPool;
		Job** victim = localPool->Lock();
		if( !victim )
			return nullptr;

		Job* result = nullptr;
		SizeT head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( localPool->myHead );
		const SizeT head0 = head;
		s32 skipAndBump = 0; // +1 for skipped task and +1 for bumped head&tail

		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( localPool->myHead, ++head );
		__IME_MEMORY_FULLCONSISTENCY_BARRIER;
		if( head > Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( localPool->myTail ) ) {
			/// Stealing attempt failed, deque contents has not been changed by us
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( localPool->myHead, head0 );
			skipAndBump++; /// trigger that we bumped head and tail
			__IME_ASSERT( !result );
		} else {
			__IME_MEMORY_READWRITE_BARRIER;
			result = victim[head-1];
			__IME_ASSERT( result );
			// emit "job was consumed" signal
			const SizeT head1 = head0 + 1;
			if( head1 < head ) {
				/// Some proxies in the job pool have been bypassed. Need to close the hole left by the stolen job. The 
				/// following variant: victim[head - 1] = victim[head0]; is of constant time, but creates a potential 
				/// for degrading stealing mechanism efficiency and growing owner's stack size too much becauseof moving
				/// earlier split off (and thus larger) chunks closer to owner's end of the deque (tail). So we use 
				/// linear time variant that is likely to be amortized to be near-constant time, though, and preserves 
				/// stealing efficiency premises. These changes in the deque must be released to the owner.
				Memory::LowLevelMemory::CopyMemoryBlock16( victim + head0, victim + head1, (head - head1) * Traits::SizeOfPtr::value );
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( localPool->myHead, head1 );
				if( head > Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( localPool->myTail ) )
					skipAndBump++; // trigger that we bumped head and tail
			}
			// Poison pointer
			victim[head0] = nullptr;
		}

		/// Write back all results to the job pool, that has been acquired earlier
		localPool->Unlock( victim );
		__IME_ASSERT( skipAndBump <= 2 );
		if( --skipAndBump > 0 ) {
			/// if job skipped and head&tail is bumped then synchronize with snapshot as we bumped head&tail which can 
			/// falsely trigger empty state.
			__IME_MEMORY_FULLCONSISTENCY_BARRIER;
			this->myCurrentArea->NotifyOnJobs();
		}

		return result;
	}

	_Ret_maybenull_ Job* Dispatcher::ReceiveOrSteal(SizeT& childCount, bool returnOnFail) {
		Job* result = nullptr;
		SizeT yieldCount = 0;
		
		for( SizeT failureCount = 0; /* Nothing */; ++failureCount ) {
			if( childCount == 1 ) {
				__IME_ASSERT( !result );
				__IME_MEMORY_READWRITE_BARRIER;
				break;
			}

			__IME_ASSERT( this->myCurrentArea->myLimit.data > 0 );
			SizeT n = this->myCurrentArea->myLimit.data;
			__IME_ASSERT( this->myCurrentPoolIdx < n );

			if( returnOnFail && this->myCurrentArea->myNumWorkersAllotted < this->myCurrentArea->GetActiveNumWorkers() ) {
				__IME_ASSERT( this->IsWorker() );
				return nullptr;
			}

			if( this->MightStealJob() && n > 1 ) {
				SizeT random = this->myRandomizer.Get() % (n-1);
				Pool* victimSelector = &this->myCurrentArea->myJobPools[random]; 

				/// The following condition excludes the master that might have already taken our previous place in the
				/// area from the list of potential victims. But since such a situation can take place only in case of
				/// significant oversubscription, keeping the checks simple seems to be preferable to complicating the code.
				if( random >= this->myCurrentPoolIdx )
					++victimSelector;

				Pool* pool = victimSelector;
				result = this->GetFromJobPool( pool );
				if( !result ) {
					goto failed;
				}
			} else {
				goto failed;
			}

			__IME_ASSERT( result );
			break;

failed:
			yieldCount = this->ProcessFailedSteal( n, failureCount, yieldCount, returnOnFail );
			if ( yieldCount == InvalidIndex )
				return nullptr;
		}

		return result;
	}

	SizeT Dispatcher::ProcessFailedSteal( SizeT n, SizeT& failCount, SizeT prevYieldCount, bool returnOnFail ) {
		static const u32 yieldTime = 80U;
		SizeT yieldCount = prevYieldCount;
		// Pause, even if we are going to yield, because the yield might return immediately.
		Core::System::Sleep< Core::SleepingDoSpinWait >( yieldTime );
		const SizeT failureThreshold = 2*n;
		if( failCount >= failureThreshold ) {
			failCount = failureThreshold;
			Core::System::Sleep< Core::SleepingDoThreadSwitch >();
			const SizeT yieldThreshold = 100;
			if( yieldCount++ >= yieldThreshold )
				if( returnOnFail && this->myCurrentArea->IsOutOfJobs() )
					return -1; //! This indicator helps to ensure if we need to exit
		}

		return yieldCount;
	}

	void Dispatcher::Setup( const SizeT stackSize, IntrusivePtr< Area > area, SizeT index ) {
		__IME_ASSERT( !this->IsValid() );

		this->myWorker = (index != 0);
		this->myCurrentPoolIdx = index;
		this->myThreadId = Threading::Thread::GetLocalThreadId();

		this->InitializeThreadInformation( stackSize );

		/// Select area
		this->myCurrentArea = area;
		//! Create dummy job that used for waiting for completion
		this->myDummyJob = new Job();

		if( !this->IsWorker() ) {
			__IME_ASSERT_MSG( this->myCurrentPoolIdx == 0, "Master thread must occupy the first slot in its market" );

			this->myDummyJob->SetRefCount< Threading::MemoryOrderEnum::Relaxed >( 1 );

			this->myInnermostRunningJob = this->myDummyJob;
			this->myCurrentJob = this->myDummyJob;

			/// Primary dispatcher always lives in the first job pool of area
			this->myCurrentPool = &this->myCurrentArea->myJobPools[0];
			this->myCurrentPool->myDispatcher = this;
		} else {
			this->myDummyJob->SetRefCount< Threading::MemoryOrderEnum::Relaxed >( 2 );
		}

		this->myValidation = true;
	}

	void Dispatcher::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		//! Cleanup all primary dispatcher resources
		if ( !this->IsWorker() ) {
			if ( this->myCurrentPool ) {
				Job** localJobPool = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCurrentPool->myJobPool );
				if ( localJobPool != Pool::theEmptyPool ) {
#if defined( DEBUG )
					Pool* p = &this->myCurrentArea->myJobPools[this->myCurrentPoolIdx];
					__IME_ASSERT_MSG( this->myCurrentPool == p, "Invalid area index" );
#endif

					this->myCurrentPool->Acquire();
					SizeT localHead = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCurrentPool->myHead );
					SizeT localTail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCurrentPool->myTail );
					localJobPool = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCurrentPool->myJobPool );

					if ( localJobPool == Pool::theEmptyPool || localHead >= localTail ) {
						// Local task pool is empty
						__IME_ASSERT_MSG( localJobPool != Pool::theEmptyPool, "Not in area" );
						// Do not reset my_arena_index. It will be used to (attempt to) re-acquire the slot next time
						__IME_ASSERT_MSG( &this->myCurrentArea->myJobPools[this->myCurrentPoolIdx] == this->myCurrentPool, "Area slot and slot index mismatch" );
						__IME_ASSERT_MSG( localJobPool == Pool::theLockedPool, "Job pool must be locked when leaving area" );
						__IME_ASSERT_MSG( this->myCurrentPool->IsQuiescentEmpty(), "Can't leave area when the job pool isn't empty" );

						/// No release fence is necessary here as this assignment precludes external
						/// accesses to the local task pool when becomes visible. Thus it is harmless
						/// if it gets hoisted above preceding local bookkeeping manipulations.
						Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myCurrentPool->myJobPool, Pool::theEmptyPool );
					} else {
						/// Since master's pool can contain undone jobs, so wait for them.
						/// Proposing: it's better to signal area/job pool/what_ever_else that we are exiting. There is 
						/// nothing to wait, because we're in exit state: just cancel all undone jobs.
						this->myCurrentPool->Lose();
						this->LocalWait( this->myDummyJob, nullptr );
						__IME_ASSERT( localJobPool == Pool::theEmptyPool );
					}
				}

				__IME_ASSERT( this->myCurrentPool == &this->myCurrentArea->myJobPools[0] );
				this->myCurrentPool = nullptr;
			}
		}

		//! Cleanup all thread-local dispatcher resources
		if (this->myDummyJob)
			delete this->myDummyJob;

		/// Detach my area of jobs as soon as this is possible(since this decrements
		/// reference count of area and this helps in faster destruction of instance)
		if( this->myCurrentArea ) {
			this->myCurrentArea = nullptr;
		}

		this->myValidation = false;
	}
}