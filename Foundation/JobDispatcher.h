/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/
#pragma once

/// Reference counter
#include <Foundation/RefCount.h>
/// Job list
#include <Foundation/Job.h>
/// Fast random generator
#include <Foundation/Random.h>
/// Singleton object
#include <Foundation/Singleton.h>
/// Array
#include <Foundation/STLArray.h>
/// Thread ID
#include <Foundation/ThreadID.h>

namespace Jobs
{
	/// Forward declaration
	class Pool;
	class Area;

	//! Thread-Local scheduler object implementation
	class Dispatcher : public Core::ReferenceCount
	{
		__IME_DECLARE_THREAD_LOCAL_SINGLETON( Dispatcher );

		friend class Server;
		friend class Area;
		friend class PreemptingJob;
		friend class AllocateRootNode;
		friend class AllocateContinuationNode;
		friend class AllocateChildNode;
		friend class AllocateAdditionChildOfNode;

		//! My worker state
		bool myWorker;
		//! My random generator
		Utils::FastRandom myRandomizer;
		//! My current job pool index taken from market
		SizeT myCurrentPoolIdx;
		//! My current area that holds job pool
		IntrusivePtr< Area, SmartPointerPolicy< Area, SmartPointerThreadSafeOp > > myCurrentArea;
		//! My current job pool pointer
		Pool* myCurrentPool;
		//! My current innermost running job
		Job* myInnermostRunningJob;
		//! My current dispatching job
		Job* myCurrentJob;
		//! My dummy job, used for waiting for job completion
		Job* myDummyJob;

		//! My local job node counts
		SizeT myJobNodeCount;
		//! My local small job count
		SizeT mySmallJobCount;

		//! My stealing threshold
		SSizeT myStealingThreshold;
		//! My thread ID holder
		Threading::ThreadID myThreadId;
		//! My validation 
		bool myValidation;

		//! Initialize thread information
		void InitializeThreadInformation( const SizeT stackSize );

		//! Predecessor state of continuation
		bool PredessorReadyForContinuation( _In_ Job* job, Job*& bypassJobSlot );

		template< typename T >
		void LocalSpawn( T jobOrList );

		void LocalWait( _In_ Job* job, Job* next );

		void DispatchLocal( Job*& job );

		bool MightStealJob() const;

		/// <summary cref="Disatcher::GetFromLocalQueue">
		/// This function used when you need to get job from dispatcher's local queue.
		/// </summary>
		/// <returns>Job that dispatcher might got from it's local queue.</returns>
		/// <remarks>
		/// Using this function is quite efficition because of lower cache misses, and continuation
		/// job might be selected immmediatelly which qualifies early proposed hint.
		/// </remarks>
		bool GetFromLocalQueue( Job*& result );

		/// <summary cref="Dispatcher::GetFromJobPool">
		/// This function used when you need to steal job from different queue.
		/// </summary>
		/// <returns>Job that dispatcher might got from selected victim job pool.</returns>
		_Ret_maybenull_ Job* GetFromJobPool( _In_ Pool* stealPool );

		/// <summary cref="Dispatcher::ReceiveOrSteal">
		/// Try getting a job from other places via stealing from other threads, local thread queue, orphans adoption.
		/// </summary>
		/// <param name="childCount">Child count of current passed job.</param>
		/// <param name="returnOnFail">Do you need to return immediatelly on job getting failure?</param>
		/// <returns>Returns obtained job or NULL if all attempts fail.</returns>
		_Ret_maybenull_ Job* ReceiveOrSteal( SizeT& childCount, bool returnOnFail );

		//! Helper method for ReceiveOrSteal() function.
		SizeT ProcessFailedSteal( SizeT n, SizeT& failCount, SizeT prevYieldCount, bool returnOnFail );

		/// <summary cref="Dispatcher::MasterDispatcherLevel">
		/// Check current level of this dispatcher, just checks if current dispatching job equals
		/// dummy job organized for internal waiting purposes.
		/// </summary>
		bool MasterDispatcherLevel() const;

	public:
		Dispatcher();
		virtual ~Dispatcher();

		void Setup( const SizeT stackSize, IntrusivePtr< Area > area, SizeT index );
		void Discard();
		bool IsValid() const;
		bool IsWorker() const;

		/// Spawn root job on current dispatcher's job pool
		void Spawn( _In_ Job* rootJob );
		void Spawn( Jobs::JobList& list );
		/// Wait job for completeness
		void Wait( _In_ Job* rootJob );

		/// Efficient version of Wait(), but has manual setup of first child
		/// that needs to be executed by the scheduler without traversing all
		/// job pool.
		void Wait( _In_ Job* rootJob, _In_opt_ Job* nextChild );

		SizeT GetJobNodeCount() const;
	};

	__IME_INLINED bool Dispatcher::MasterDispatcherLevel() const
	{
		bool result = this->myCurrentJob == this->myDummyJob;
		return result;
	}

	__IME_INLINED bool Dispatcher::MightStealJob() const
	{
		int currentStackPosPtr;
		return myStealingThreshold < reinterpret_cast<intptr_t>(&currentStackPosPtr);
	}

	__IME_INLINED bool Dispatcher::IsValid() const
	{
		bool result = this->myValidation;
		return result;
	}

	__IME_INLINED bool Dispatcher::IsWorker() const {
		bool result = this->myWorker;
		return result;
	}

	__IME_INLINED void Dispatcher::Wait( _In_ Job* rootJob )
	{
		__IME_ASSERT( this->IsValid() );
		__IME_ASSERT( rootJob );
		this->LocalWait( rootJob, nullptr );
	}

	__IME_INLINED void Dispatcher::Wait( _In_ Job* rootJob, _In_opt_ Job* nextChild )
	{
		__IME_ASSERT( this->IsValid() );
		__IME_ASSERT( rootJob );
		this->LocalWait( rootJob, nextChild );
	}

	__IME_INLINED SizeT Dispatcher::GetJobNodeCount() const {
		SizeT result = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myJobNodeCount );
		return result;
	}
}