/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Foundation/JobPool.h>
#include <Foundation/JobDispatcher.h>
#include <Foundation/Job.h>

namespace Jobs
{
	const Job** Pool::theEmptyPool = (static_cast<const Job**>(0));
	const Job** Pool::theLockedPool = (reinterpret_cast<const Job**>(~static_cast< intptr_t >(0)));

	Pool::Pool( void ) : myValidation( false ) {
		this->myDispatcher = nullptr;
		this->myJobPool = nullptr;
		this->myHead = 0;
		this->myTail = 0;
		this->myJobPoolSize = 0;
		this->myJobPoolPtr = nullptr;
	}

	Pool::~Pool( void ) {
		if (this->IsValid())
			this->Discard();
	}

	void Pool::AllocatePool( SizeT num ) {
		__IME_ASSERT( num > 0 );
		SizeT byteSize = ((num * sizeof(Job*) + gCacheLine - 1) / gCacheLine) * gCacheLine;
		this->myJobPoolSize = byteSize / sizeof(Job*);
		this->myJobPoolPtr = static_cast< Job** >( Memory::Allocator::Malloc( byteSize ) );
		__IME_ASSERT( this->myJobPoolPtr );

		for (SizeT idx = 0; idx < this->myJobPoolSize; idx++) {
			this->myJobPoolPtr[idx] = nullptr;
		}
	}

	SizeT Pool::Setup( SizeT numJobs ) {
		SizeT tail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myTail );
		const SizeT tail0 = tail;
		if( numJobs + tail < this->myJobPoolSize )
			return tail;

		/// Lock job pool to modify it inside
		this->Acquire();
		SizeT head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myHead );
		tail -= head;
		SizeT newSize = tail + numJobs;
		__IME_ASSERT( !this->myJobPoolSize || this->myJobPoolSize >= minJobPoolSize );

		if( !this->myJobPoolSize ) {
			__IME_ASSERT( this->IsEmpty() && !this->myJobPoolPtr );
			if( newSize < minJobPoolSize ) newSize = minJobPoolSize;
			this->AllocatePool( newSize );
		} else if( newSize <= this->myJobPoolSize - minJobPoolSize * 0.25 ) {
			__IME_ASSERT_MSG( this->IsQuiescent(), "Job pool must be locked when entering this section of code!" );

			/// If the free space at the beginning of the job pool is too short, we are likely facing a pathological
			/// single-producer-multiple-consumers scenario, and thus it's better to expand the job pool. Relocate 
			/// the busy part to the beginning of the deque.
			Memory::LowLevelMemory::CopyMemoryBlock16( this->myJobPoolPtr + head, 
													   this->myJobPoolPtr, 
													   tail * Traits::SizeOfPtr::value );

			// No need to clear the fresh deque since valid items are designated by the head and tail members. But
			/// fill it with a canary pattern in the high vigilance debug mode.
			for( SizeT idx = tail; idx < tail0; idx++ )
				this->myJobPoolPtr[idx] = nullptr;

			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release, SizeT, SizeT >( this->myHead, 0 );
			/// Tail is updated last to minimize probability of a thread making area snapshot being misguided into
			/// thinking that this job pool is empty.
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( this->myTail, tail );
			this->Lose();
		} else {
			__IME_ASSERT_MSG( this->IsQuiescent(), "Job pool must be locked when entering this section of code!" );

			/// Grow job pool. As this operation is rare, and its cost is asymptotically amortizable, we can tolerate
			/// new job pool allocation done under the lock.
			if( newSize < this->myJobPoolSize * 2 )
				newSize = this->myJobPoolSize * 2;
			/// Reallocate job pool, since it update myJobPoolSize, then you need to reload size using interlocked
			/// function to retrieve it.
			this->AllocatePool( newSize );
			__IME_ASSERT_MSG( tail <= this->myJobPoolSize, "New job pool is too short!" );
			Memory::LowLevelMemory::CopyMemoryBlock16( this->myJobPoolPtr + head, 
													   this->myJobPoolPtr, 
													   tail * Traits::SizeOfPtr::value );

			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( this->myHead, 0 );
			/// Tail is updated last to minimize probability of a thread making area snapshot being misguided into
			/// thinking that this job pool is empty.
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( this->myTail, tail );
			this->Lose();

			__IME_ASSERT_MSG( this->myJobPoolPtr, "Attempting to free nullfied/empty job pool!" );
			Memory::Allocator::Free( this->myJobPoolPtr );
		}

		if( !this->IsValid() )
			this->myValidation = true;

		return tail;
	}

	void Pool::Discard( void ) {
		if( this->myJobPoolPtr ) {
			__IME_ASSERT( this->myJobPoolSize );
			Memory::Allocator::Free( this->myJobPoolPtr );
			this->myJobPoolPtr = nullptr;
			this->myJobPoolSize = 0;
		}
		this->myValidation = false;
	}

	void Pool::Acquire( void ) const {
		/// We are not in Area - nothing to lock
		if( this->IsEmpty() )
			return;

		PoolBackoff backoff;
		bool syncPrepareDone = false;
		__forever {
			/// Local copy of the area slot task pool pointer is necessary for the next assertion to work correctly to
			/// exclude asynchronous state transition effect.
			Job** pool = this->myJobPool;
			__IME_ASSERT_MSG( pool == Pool::theLockedPool || pool == this->myJobPoolPtr, 
							  "Job pool ownership corrupt?" );

			if (this->myJobPool != Pool::theLockedPool &&
				Threading::Interlocked::CompareAndSwap(this->myJobPool,
													   Pool::theLockedPool,
													   this->myJobPoolPtr) == this->myJobPoolPtr) {
				break;
			} else if (!syncPrepareDone) {
				syncPrepareDone = true;
			}

			/// Someone else acquired a lock, so pause and do exponential backoff.
			backoff.Pause();
		}

		__IME_MEMORY_READWRITE_BARRIER;
		__IME_ASSERT_MSG( this->myJobPool == Pool::theLockedPool, "Not really acquired task pool" );
	}

	void Pool::Lose( void ) {
		/// We are not in Area - nothing to unlock
		if( this->IsEmpty() )
			return;
		__IME_ASSERT_MSG( Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myJobPool ) == 
			Pool::theLockedPool, "Job pool is not really locked!" );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release, Job**, Job** >( this->myJobPool, 
																							this->myJobPoolPtr );
	}

	Job** Pool::Lock( void ) {
		Job** thisVictim = nullptr;
		PoolBackoff backoff;
		bool syncPrepareDone = false;
		__forever {
			thisVictim = this->myJobPool;
			/// NOTE: Do not use comparison of head and tail indices to check for the presence of work in the victim's 
			/// task pool, as they may give incorrect indication because of task pool relocations and resizes.
			if( thisVictim == Pool::theEmptyPool ) {
				/// The victim thread emptied its job pool - nothing to lock
				if( syncPrepareDone ) { }
				break;
			}

			if( thisVictim != Pool::theLockedPool && Threading::Interlocked::CompareAndSwap( this->myJobPool, 
																							 Pool::theLockedPool, 
																							 thisVictim ) == thisVictim )
				break;
			else if( !syncPrepareDone )
				syncPrepareDone = true;

			/// This code is for future prepration for era of mega-cores, where consumer CPUs will have more than 8 cores
			if( !backoff.BoundedPause() )
				Core::System::Sleep< Core::SleepingDoThreadSwitch >();
		}

		__IME_ASSERT( thisVictim == Pool::theEmptyPool || 
					  (this->myJobPool == Pool::theLockedPool && thisVictim != Pool::theLockedPool ) );
		return thisVictim;
	}

	void Pool::Unlock( Job** writeBackPtr ) {
		__IME_ASSERT_MSG( this->IsValid(), "Uninitialized job pool!" );
		__IME_ASSERT_MSG( this->myJobPool == Pool::theLockedPool, "This job pool is not locked! It must be" );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->myJobPool, writeBackPtr );
	}
}

/*
	change log:

	21-Mar-15		codepoet		method Pool::Acquire	changed AtomicBackoff declaration to AtomicBackoff<> to accomodate changes
	21-Mar-15		codepoet		method Pool::Lock		changed Atomicbackoff declaration to AtomicBackoff<> to accomodate changes
*/