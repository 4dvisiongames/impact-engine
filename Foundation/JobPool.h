/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// TODO: merge locking/unlocking mechanigms with acquisition/releasing.

#pragma once

// Intrusive reference counter
#include <Foundation/RefCount.h>
// Weak smart pointer
#include <Foundation/WeakPtr.h>

namespace Jobs
{
	/// Forward declaration
	class Dispatcher;
	class Job;

	struct PoolCacheLine1
	{
		/// Scheduler of the thread attached to the slot
		/// Marks the slot as busy, and is used to iterate through the schedulers belonging to this area
		IntrusiveWeakPtr< Dispatcher, SmartPointerPolicy< Dispatcher, SmartPointerThreadSafeOp > > myDispatcher;

		/// Synchronization of access to job pool
		/// Also is used to specify if the slot is empty or locked
		Job** myJobPool;

		/// Index of the first ready job in the deque.
		/// Modified by thieves, and by the owner during compaction/reallocation
		SizeT myHead;
	};

	struct PoolCacheLine2
	{
		/// Index of the element following the last ready job in the deque.
		/// Modified by the owner thread.
		SizeT myTail;

		/// Capacity of the primary job pool(number of elements - pointers to job).
		SizeT myJobPoolSize;

		/// Task pool of the scheduler that owns this slot
		Job** myJobPoolPtr;
	};

	class Pool : public Padded< PoolCacheLine1 >, public Padded< PoolCacheLine2 >
	{
		friend class Server;
		friend class Area;
		friend class Dispatcher;

		static const SizeT minJobPoolSize = 64;
		static const Job** theEmptyPool;
		static const Job** theLockedPool;

		typedef Threading::AtomicBackoff< Threading::BackoffStrategyPause, Threading::BackoffStrategyYield > PoolBackoff;

	public:
		Pool( void );
		~Pool( void );

		SizeT Setup( SizeT num );
		void Discard( void );
		bool IsValid( void ) const;
		bool IsEmpty( void ) const;

		bool IsQuiescent() const;
		bool IsQuiescentEmpty() const;
		bool IsQuiescentReset() const;
		bool IsQuiescentEmptyLocalPool() const;

		/// <summary cref="Pool::Acquire">
		/// </summary>
		/// <remarks>
		/// ATTENTION:
		/// This method is mostly the same as <c>Pool::Lock()</c>, with a little different logic of job pool 
		/// state checks (job pool is either locked or points to our local job pool). Thus if either of them
		/// is changed, consider changing the counterpart as well.
		/// </remarks>
		void Acquire( void ) const;

		/// <summary cref="Pool::Lose">
		/// Unlocks the local job pool.
		/// </summary>
		/// <remarks>
		/// Restores this->myArenaSlot->myTaskPool mangled by <c>Pool::Acquire</c>. Requires correctly set 
		/// this->myArenaSlot->myTaskPoolPtr.
		/// </remarks>
		void Lose( void );

		Job** Lock( void );
		void Unlock( Job** writeBackPtr );

	private:
		void AllocatePool(SizeT size);

		//! My validation
		bool myValidation;
	};

	__IME_INLINED bool Pool::IsValid( void ) const
	{
		bool result = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myValidation );
		return result;
	}

	__IME_INLINED bool Pool::IsEmpty( void ) const
	{
		return this->myJobPool == Pool::theEmptyPool;
	}

	__IME_INLINED bool Pool::IsQuiescent( void ) const
	{
		Job** jobPool = this->myJobPool;
		return jobPool == theEmptyPool || jobPool == theLockedPool;
	}

	__IME_INLINED bool Pool::IsQuiescentEmpty( void ) const
	{
		return this->IsQuiescent() && this->IsQuiescentEmptyLocalPool(); 
	}

	__IME_INLINED bool Pool::IsQuiescentReset( void ) const
	{
		SizeT head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myHead );
		SizeT tail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myTail );
		return this->IsQuiescent() && ( head == 0 && tail == 0 ); 
	}

	__IME_INLINED bool Pool::IsQuiescentEmptyLocalPool() const {
		SizeT head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >(this->myHead);
		SizeT tail = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >(this->myTail);
		return head == tail;
	}
}