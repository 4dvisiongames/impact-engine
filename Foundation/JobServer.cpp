/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Intrusive reference
#include <Foundation/SmartRef.h>
/// Job server
#include <Foundation/JobServer.h>
/// Jobs dispatcher
#include <Foundation/JobDispatcher.h>
/// Jobs area of pools
#include <Foundation/JobArea.h>
/// Pool implementation
#include <Foundation/JobPool.h>
/// Thread workers
#include <Foundation/JobWorker.h>
/// Default scoped locker
#include <Foundation/ScopedLock.h>
/// STL front inserter iterator
#include <Foundation/STLFrontInsertIterator.h>

namespace Jobs
{
	Server::Server( void ) 
		: myMaxNumWorkers( 0 )
		, myValidation( false )
		, myNextAreaIter( nullptr )
		, myStackSize( 0 )
		, myTotalDemand( 0 ) 
	{
		this->myTrackedJobs.data = 0;
	}

	Server::~Server( void )
	{
		if ( this->IsValid() )
			this->Discard();
	}

	void Server::ProcessAreaOnDispatcher( AreaThreadSafePtr& area, IntrusivePtr< Dispatcher >& dispatcher )
	{
		__IME_ASSERT( !dispatcher->myCurrentJob );
		__IME_ASSERT( !dispatcher->myInnermostRunningJob );
		__IME_ASSERT( area->myNumJobPools != 1 );

		// Start search for an empty slot from the one we occupied the last time
		SizeT index = dispatcher->myCurrentPoolIdx < area->myNumJobPools ? dispatcher->myCurrentPoolIdx : dispatcher->myRandomizer.Get() % (area->myNumJobPools - 1) + 1;
		SizeT end = index;

		__IME_ASSERT_MSG( index != 0, "Thread worker can't occupy primary pool from area!" );
		__IME_ASSERT( index < area->myNumJobPools );

		/// Find first vacant pool from selected area
		__forever {
			IntrusivePtr< Dispatcher, SmartPointerPolicy< Dispatcher, SmartPointerThreadSafeOp > > localDispatcher = area->myJobPools[index].myDispatcher.GetUnsafe();
			if( !localDispatcher.IsValid() && localDispatcher.SwapCompared( dispatcher, nullptr )  )
				break;

			if( ++index == area->myNumJobPools )
				index = 1;

			if( index == end ) {
				//! Release this thread from worker list of area
				this->OnDispatchAreaLeave( dispatcher, area );
				return;
			}
		}

		dispatcher->myCurrentArea = area;
		dispatcher->myCurrentPoolIdx = index;
		dispatcher->myCurrentPool = area->myJobPools + index;

		/// Atomic comparison sub-function
		{
			std::less< SizeT > comparison;
			SizeT newValue = index + 1;
			SizeT oldValue = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( area->myLimit.data );
			while( comparison( oldValue, newValue ) ) {
				if( Threading::Interlocked::CompareAndSwap( area->myLimit.data, newValue, oldValue ) == oldValue )
					break;

				oldValue = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( area->myLimit.data );
			}
		}

		__forever {
			// Try to steal a job.
			// Passing reference count is technically unnecessary in this context,
			// but omitting it here would add checks inside the function.
			Job* localDummyJob = dispatcher->myDummyJob;
			Job* j = dispatcher->ReceiveOrSteal( localDummyJob->myReferences, true );
			if( j ) {
				// A side effect of receive_or_steal_task is that my_innermost_running_task can be set.
				// But for the outermost dispatch loop of a worker it has to be NULL.
				dispatcher->myInnermostRunningJob = nullptr;
				__IME_ASSERT( !dispatcher->myCurrentJob );
				dispatcher->LocalWait( dispatcher->myDummyJob, j );
			}

#if defined( DEBUG )
			__IME_ASSERT_MSG( dispatcher->myCurrentPool->IsQuiescentEmptyLocalPool(), "Thread worker can't leave area while it's job pool is not empty!");
			__IME_ASSERT_MSG( dispatcher->myCurrentPool->IsEmpty(), "Empty job pool isn't marked appropriately!" );
#endif

			// This check prevents relinquishing more than necessary workers because
			// of the non-atomicity of the decision making procedure
			SSizeT allotted = area->myNumWorkersAllotted;
			if( area->GetActiveNumWorkers() > allotted ) 
				break;
		}

		area->myJobPools[index].myDispatcher = nullptr;
		dispatcher->myCurrentPool = nullptr;
		__IME_ASSERT( !dispatcher->myInnermostRunningJob );
		__IME_ASSERT( !dispatcher->myCurrentJob );

		//! Release this thread from worker list of area
		this->OnDispatchAreaLeave( dispatcher, area );
	}

	void Server::SignalAvailableWork( SSizeT additionalWork, bool propagateContinuation )
	{
		if( propagateContinuation ) {
			if( additionalWork < 0 ) {
				Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental >( this->myTrackedJobs.data, additionalWork );
				return;
			}
		}

		IntrusivePtr< Worker > wakee[2]; //! At least wakeup two threads at once
		IntrusivePtr< Worker >* wakees = wakee;

		{
			Threading::ScopedLock< Threading::SpinWait > lock( this->mySleepingWorkersGuard );
			while( this->mySleepingWorkersRoot && wakee < wakees + 2 ) {
				bool done = false;
				if( additionalWork > 0 ) {
					SizeT localTrackedJobs = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myTrackedJobs.data );
					if( additionalWork + localTrackedJobs <= 0 )
						// additional demand does not exceed surplus supply
						done = true;

					if( !done )
						--additionalWork;
				} else {
					// Chain reaction; Try to claim unit of additional work to be tracked
					SizeT oldAdditionalWork;
					do {
						oldAdditionalWork = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myTrackedJobs.data );
						if( oldAdditionalWork <= 0 ) { done = true; break; }
					} while( Threading::Interlocked::CompareAndSwap( this->myTrackedJobs.data, oldAdditionalWork - 1, oldAdditionalWork ) != oldAdditionalWork );
				}

				if( done )
					break;

				//! Pop into waiting root
				this->mySleepingWorkersRoot = (*wakees++ = this->mySleepingWorkersRoot)->myNextSleepingFriend;

				if( additionalWork )
					Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental >( this->myTrackedJobs.data, additionalWork );
			}
		}

		//! Wakeup all threads got from sleeping list
		while( wakees > wakee ) {
			IntrusivePtr< Worker >& worker = *--wakees;
			/// BUGBUG: This is not really bug, but this checking might be gone, because all threads
			/// go here from asleep list, so they exist and are valid by default.
			if( worker.IsValid() )
				worker->EmitWakeupSignal();
		}
	}

	void Server::UpdateWorkerAllotment( void )
	{
		if( this->myTotalDemand ) {
			SSizeT maxNumWorkers = Math::TMin< SSizeT, SizeT >( this->myTotalDemand, this->myMaxNumWorkers );
			SSizeT carry = 0;
			SSizeT assigned = 0;

			this->myAreasGuard.Lock();

			STL::ForEach( this->myAreasRegistry.Begin(), 
				this->myAreasRegistry.End(), 
				[&maxNumWorkers, &carry, &assigned, this]( IntrusivePtr< Area > rhs ) {
				Threading::ScopedLock<Threading::SpinWait> lock( rhs->myAreaAccessGuard );
				if ( rhs->myNumWorkersRequested <= 0 ) {
					__IME_ASSERT( !rhs->myNumWorkersAllotted );
					return;
				}

				SizeT temporary = rhs->myNumWorkersRequested * maxNumWorkers + carry;
				SizeT allotted = temporary / this->myTotalDemand;
				carry = temporary % this->myTotalDemand;
				rhs->myNumWorkersAllotted = Math::TMin( allotted, rhs->myMaxNumWorkers );
				assigned += rhs->myNumWorkersAllotted;
			} );

			NDKAssert2( assigned <= this->myTotalDemand );
			this->myAreasGuard.Unlock();
		}
	}

	bool Server::PutIntoAsleepList( const IntrusivePtr< Worker >& worker )
	{
		Threading::ScopedLock< Threading::SpinWait > lock( this->mySleepingWorkersGuard );

		// Contribute to slack under lock so that if another takes that unit of slack,
		// it sees us sleeping on the list and wakes us up.
		SizeT k = Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SizeT >( this->myTrackedJobs.data, 1 ) + 1;
		if( k < 0 ) {
			worker->myNextSleepingFriend = this->mySleepingWorkersRoot;
			this->mySleepingWorkersRoot = worker;
			return true;
		} else {
			Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SSizeT >( this->myTrackedJobs.data, -1 );
			return false;
		}
	}

	void Server::Setup( void )
	{
		__IME_ASSERT( !Dispatcher::HasInstance() );
		SizeT stackSize = 1024 * 1024;
		this->myStackSize = stackSize;

		SizeT concurrency = Threading::Thread::GetAvailableConcurrency();
		this->myMaxNumWorkers = concurrency - 1;

		/// This area will be erased from store by it's server
		IntrusivePtr< Server > thisServer = this;
		IntrusivePtr< Area > primaryArea = Area::Create(this->myMaxNumWorkers);
		this->InsertAreaIntoList( primaryArea );
		primaryArea->Setup();
		__IME_ASSERT( primaryArea->IsValid() );

		this->myPrimaryDispatcher = new Dispatcher;
		this->myPrimaryDispatcher->Setup( this->myStackSize, primaryArea, 0 );
		__IME_ASSERT( this->myPrimaryDispatcher->IsValid() );

		STL::FrontInsertIterator< WorkerList > workerInserterIterator( this->myWorkersList );
		for( SizeT idx = 0; idx < this->myMaxNumWorkers; ++idx ) {
			IntrusivePtr< Worker > worker = new Worker(thisServer);
			worker->Setup( this->myStackSize, 
						   false, 
						   Threading::ThreadPrioritiesEnum::High, 
						   STL::String< Char8 >::VA( "JobWorker[#%i]", idx ) );
			workerInserterIterator += worker;
		}

		this->myValidation = true;
	}

	void Server::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		/// Remove all workers from list
		this->ReleaseAllWorkers();

		/// Remove all areas of jobs
		this->ReleaseAllAreas();

		this->myPrimaryDispatcher->Discard();
		this->myPrimaryDispatcher = nullptr;

		this->myValidation = false;
	}

	void Server::InsertAreaIntoList( IntrusivePtr< Area >& area )
	{
		Threading::ScopedLock< Threading::SpinWait > lock( this->myAreasGuard );

		AreaList& areaList = this->myAreasRegistry;
		AreaList::Iterator& next = this->myNextAreaIter;
		areaList.Prepend( area );
		if( STL::CountOf(areaList) == 1 )
			next = areaList.Begin();

		area->myServer = this;
	}

	void Server::RemoveAreaFromList( IntrusivePtr< Area >& area )
	{
		Threading::ScopedLock< Threading::SpinWait > lock( this->myAreasGuard );

		AreaList& areaList = this->myAreasRegistry;
		AreaList::Iterator& next = this->myNextAreaIter;
		__IME_ASSERT( next != areaList.End() );
		if( *next == area )
			if( ++next == areaList.End() && STL::CountOf(areaList) > 1 )
				next = areaList.Begin();

		area->myServer = nullptr;
		areaList.Remove( area );
	}

	Server::AreaThreadSafePtr Server::SelectNeededArea(void)
	{
		Threading::ScopedLock< Threading::SpinWait > lock( this->myAreasGuard );
		AreaList& areaList = this->myAreasRegistry;
		AreaList::Iterator& next = this->myNextAreaIter;

		if( areaList.Empty() )
			return nullptr;

		__IME_ASSERT( next != areaList.End() );
		AreaList::Iterator iter = next;
		do {
			IntrusivePtr< Area > area = *iter;
			if( ++iter == areaList.End() )
				iter = areaList.Begin();
			if( area->GetActiveNumWorkers() < area->myNumWorkersAllotted ) {
				area->myNumWorkersReferences.AddRef< Threading::MemoryOrderEnum::Sequental >( 2 );
				next = iter;
				return area;
			}
		} while( iter != next );

		return nullptr;
	}

	void Server::OnDispatchAreaLeave( IntrusivePtr< Dispatcher >& dispatcher, IntrusivePtr< Area, SmartPointerPolicy< Area, SmartPointerThreadSafeOp > >& area )
	{
		SizeT releaseReferences = dispatcher->IsWorker() ? 2 : 1;
		area->myNumWorkersReferences.Release< Threading::MemoryOrderEnum::Sequental >( releaseReferences );
	}

	void Server::ReleaseAllWorkers()
	{
		WorkerList::Iterator iter = this->myWorkersList.Begin();
		while (iter != this->myWorkersList.End()) {
			WorkerList::Iterator current = iter++;
			IntrusivePtr< Worker > worker = *current;
			worker->Join();
			this->myWorkersList.Remove(worker);
			worker = nullptr;
		}
	}

	void Server::ReleaseAllAreas()
	{
		AreaList::Iterator iter = this->myAreasRegistry.Begin();
		while (iter != this->myAreasRegistry.End()) {
			AreaList::Iterator current = iter++;
			IntrusivePtr< Area > area = *current;
			this->myAreasRegistry.Remove(area);
			area = nullptr;
		}
	}
}