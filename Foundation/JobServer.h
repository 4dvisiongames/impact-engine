/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Reference counter
#include <Foundation/RefCount.h>
/// Singleton
#include <Foundation/Singleton.h>
/// STL doubly-linked list
//#include <Foundation/STLList.h>
#include <Foundation/STLIntrusiveList.h>
/// Thread monitor
#include <Foundation/ThreadEventMonitor.h>

namespace Jobs
{
	/// Forward declaration
	class Dispatcher;
	class Area;

	class Server : public Core::ReferenceCount
	{
		friend class Worker;
		friend class Area;
		friend class Dispatcher;

		typedef STL::IntrusiveList< IntrusivePtr< Area > > AreaList;
		typedef STL::IntrusiveList< IntrusivePtr< Worker > > WorkerList;

		typedef IntrusivePtr< Area, SmartPointerPolicy< Area, SmartPointerThreadSafeOp > > AreaThreadSafePtr;

		//! All created areas must go there
		AreaList myAreasRegistry;
		//! Next area to be accessed when market will be acquires by some thread
		AreaList::Iterator myNextAreaIter;
		//! My area's synchronization primitive
		Threading::SpinWait myAreasGuard;
		//! My primary dispatcher
		IntrusivePtr< Dispatcher > myPrimaryDispatcher;
		//! My tracked jobs to be executed on threads
		Threading::InterlockedPODType< SizeT > myTrackedJobs;
		//! My maximal number of workers
		SizeT myMaxNumWorkers;
		//! My workers list
		WorkerList myWorkersList;
		//! My stack size of workers
		SizeT myStackSize;
		//! Number of workers that were requested by all arenas
		SSizeT myTotalDemand;
		//! Net number of nodes that have been allocated from heap.
		/** Updated each time a scheduler or area is destroyed. */
		Threading::InterlockedPODType< SizeT > myJobNodeCount;
		//! My sleeping worker list root
		IntrusivePtr< Worker, SmartPointerPolicy< Worker, SmartPointerThreadSafeOp > > mySleepingWorkersRoot;
		//! My sleeping worker list blocking guard
		Threading::SpinWait mySleepingWorkersGuard;
		//! My validation hint
		bool myValidation;

		/// <summary cref="Server::ProcessAreaOnDispatcher">
		/// A function used by thread for processing jobs from area by local dispatcher.
		/// </summary>
		/// <param name="area">Current area to be processed by dispatcher.</param>
		/// <param name="dispatcher">Current dispatcher that will be used for processing.</param>
		void ProcessAreaOnDispatcher(  AreaThreadSafePtr& area, IntrusivePtr< Dispatcher >& dispatcher );

		/// <summary cref="Server::PropagateContinuation">
		/// Propagates command that wakesup all threads that were put into sleep list long before.
		/// </summary>
		void PropagateContinuation( void );

		/// <summary cref="Server::SignalAvailableWork">
		/// Signales additional work to thread workers by manual managing size of work. Also it wakes up
		/// all needed threads like <c>PropagateContinuation</c> function.
		/// </summary>
		/// <param name="additionalWork">Additional work to be executed on thread workers.</param>
		/// <remarks>
		/// This function is partially the same as <c>PropagateContinuation</c> function, but with adjusting
		/// size of additional work size which will be used by threads to indicate existing work.
		/// </remarks>
		void SignalAvailableWork( SSizeT additionalWork, bool propagateContinuation = false );

		/// <summary cref="Server::PutIntoAsleepList">
		/// </summary>
		bool PutIntoAsleepList( const IntrusivePtr< Worker >& worker );

		/// <summary cref="Server::UpdateAllotment">
		/// </summary>
		void UpdateWorkerAllotment( void );

		void UpdateJobNodeCount( SSizeT count );

		void OnDispatchAreaLeave( IntrusivePtr< Dispatcher >& dispatcher, IntrusivePtr< Area, SmartPointerPolicy< Area, SmartPointerThreadSafeOp > >& area );

		void ReleaseAllWorkers();

		void ReleaseAllAreas();

	public:
		Server( void );
		virtual ~Server( void );

		void Setup( void );
		void Discard( void );
		bool IsValid( void ) const;

		bool IsNeedToWakeWorkers( void ) const;
		//! Get stack size
		SizeT GetStackSize( void ) const;

		void InsertAreaIntoList( IntrusivePtr< Area >& area );
		void RemoveAreaFromList( IntrusivePtr< Area >& area );

		//! Returns next area that needs more workers or NULL pointer.
		AreaThreadSafePtr SelectNeededArea(void);
		//! Thread exiting(for all workers)
		void OnThreadExit( void );
	};

	__IME_INLINED bool Server::IsValid() const
	{
		return this->myValidation;
	}

	__IME_INLINED bool Server::IsNeedToWakeWorkers( void ) const
	{
		SSizeT slack = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myTrackedJobs.data );
		return slack >= 0;
	}

	__IME_INLINED void Server::PropagateContinuation( void )
	{
		if( this->mySleepingWorkersRoot )
			this->SignalAvailableWork( 0 );
	}

	__IME_INLINED void Server::UpdateJobNodeCount( SSizeT count )
	{
		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental >( this->myJobNodeCount.data, count );
	}

	__IME_INLINED SizeT Server::GetStackSize( void ) const
	{
		SizeT result = this->myStackSize;
		return result;
	}

	__IME_INLINED void Server::OnThreadExit( void )
	{
		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SSizeT >( this->myJobNodeCount.data, 1 );
	}
}