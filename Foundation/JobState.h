/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

namespace Jobs
{
	//! Job state enumeration object implementation
	struct JobStateEnum
	{
		enum List
		{
			//! job is running, and will be destroyed after method Execute() completes.
			Running,

			//! job to be rescheduled.
			Reexecute,

			//! job is in ready pool, or is going to be put there, or was just taken off.
			Ready,

			//! job object is freshly allocated or recycled.
			Allocated,

			//! job object is on free list, or is going to be put there, or was just taken off.
			Free,

			//! job to be recycled as continuation
			Recycle
		};
	};

	typedef JobStateEnum::List JobState;


	//! Extra job state enumeration implementation
	struct JobExtraStateEnum
	{
		enum List
		{
			///! This state shows that job has additional childs
			StillAlive = 1 << 0,

			///! This state shows that job is selected for thread with affinity id
			Mailboxed = 1 << 1,

			///! Enqueued job
			Enqueued = 1 << 2,

			///! Checker for steal state
			IsStolen = 1 << 3,

			IsDone = 1 << 4,
		};
	};

	typedef JobExtraStateEnum::List JobExtraState;
}