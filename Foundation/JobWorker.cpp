/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Foundation/JobWorker.h>
#include <Foundation/JobServer.h>
#include <Foundation/JobDispatcher.h>
#include <Foundation/JobArea.h>
#include <Foundation/JobPool.h>

namespace Jobs
{
	Worker::Worker(IntrusivePtr< Server >& server)
	{
		this->myHandleState = false;
		this->myState.data = WorkerStateEnum::Initialized;
		this->myServer = server.Cast< Server, SmartPointerPolicy< Server, SmartPointerThreadSafeOp > >();
	}

	Worker::~Worker( void )
	{
		this->myServer = nullptr;
	}

	void Worker::Executor( void )
	{
		// Firstly wakeup all needed threads
		myServer->PropagateContinuation();
		WorkerState currentState = Threading::Interlocked::CompareAndSwap( this->myState.data, WorkerStateEnum::Normal, WorkerStateEnum::Starting );

		if( currentState == WorkerStateEnum::Starting )
		{
			IntrusivePtr< Dispatcher > localDispatcher = new Dispatcher;
			localDispatcher->Setup( myServer->GetStackSize(), nullptr, this->GetCurrentThreadIndex() );

			currentState = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myState.data );
			while( currentState == WorkerStateEnum::Normal ) 
			{
				currentState = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myState.data );
				// Immediate checking for quitting state
				if( currentState == WorkerStateEnum::Quit || currentState == WorkerStateEnum::Plugged )
					break;

				if( myServer->IsNeedToWakeWorkers() ) 
				{
					while (IntrusivePtr< Area, SmartPointerPolicy< Area, SmartPointerThreadSafeOp > > selectedArea = myServer->SelectNeededArea())
						myServer->ProcessAreaOnDispatcher( selectedArea, localDispatcher );
				} 
				else 
				{
					Threading::EventMonitor::Ticket context;
					this->myThreadMonitor.PrepareWait( context );
					currentState = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myState.data );

					if( currentState == WorkerStateEnum::Normal && myServer->PutIntoAsleepList( this ) ) 
					{
						this->myThreadMonitor.CommitWait( context );
						myServer->PropagateContinuation(); //! Say other threads to wake up, if any
					} 
					else 
					{
						this->myThreadMonitor.CancelWait(); //! Cancel wait on broken condition of waiting
					}
				}
			}

			localDispatcher->Discard();
			localDispatcher = nullptr;
		}

		//! Let server know, this thread is about to be destroyed
		myServer->OnThreadExit();
	}

	//! Construct a thread object with a new thread of execution.
	void Worker::Setup( size_t stackSize, bool manualResume, Threading::ThreadPriorities prio, const s8* name )
	{
		__IME_ASSERT_MSG(!this->IsValid(), "Worker has been initialized! Possible programmer's fault.");

		Threading::Interlocked::CompareAndSwap(this->myState.data, WorkerStateEnum::Starting, WorkerStateEnum::Initialized);
		Threading::Thread::Setup( stackSize, manualResume, prio, name );
		this->myHandleState = true;
	}

	bool Worker::IsValid() const
	{
		WorkerState s = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >(this->myState.data);
		return s != WorkerStateEnum::Initialized && this->myHandleState;
	}

	void Worker::Join( void )
	{
		WorkerState s;

		// Transition from Starting or Normal to Plugged or Quit
		do 
		{
			s = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myState.data );
			__IME_ASSERT( s == WorkerStateEnum::Initialized || s == WorkerStateEnum::Starting || s == WorkerStateEnum::Normal );
		} while( Threading::Interlocked::CompareAndSwap( this->myState.data, s == WorkerStateEnum::Starting ? WorkerStateEnum::Plugged : WorkerStateEnum::Quit, s ) != s );

		if( s == WorkerStateEnum::Starting )
			// May have invalidated invariant for sleeping, so wake up the thread.
			// Note that the Notify() here occurs without maintaining invariants for mySlack.
			// It does not matter, because myState == WorkerStateEnum::Quit overrides checking of mySlack.
			this->myThreadMonitor.Notify();

		if( s != WorkerStateEnum::Initialized ) 
		{
			while( !this->myHandleState )
				Core::System::Sleep< Core::SleepingDoThreadSwitch >();

			//! Call main function
			Threading::Thread::Join();
		}
	}

	void Worker::EmitWakeupSignal( void )
	{
		//! Always check is thread is paused, because at some point it can be
		//! paused during processing.
		//if( this->IsPaused() )
			//this->Resume();

		//! Notify thread that it can be signalled to continue execution
		this->myThreadMonitor.Notify();
	}
}