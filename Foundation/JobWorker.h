/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Thread implementation
#include <Foundation/Thread.h>
/// Event monitor
#include <Foundation/ThreadEventMonitor.h>
/// Worker state
#include <Foundation/JobWorkerState.h>
/// 
#include <Foundation/STLIntrusiveList.h>
/// 
#include <Foundation/WeakPtr.h>

namespace Jobs
{
	class Worker;
	class Server;

	class WorkerCacheLine : public STL::IntrusiveListNode, public Threading::Thread
	{
		friend class Server;

	protected:
		/// State in finite-state machine that controls the worker.
		/// State diagram:
		///
		///	Initialized ---------------\ 
		///	    |                      | 
		///	    V                      V
		///	Starting --> Normal --> Quit
		///		|
		///		V
		///	 Plugged
		///
		/// See JobWorkerStateEnum for more information about state FSM.
		/// This is quite same FSM like that used in Intel's TBB task library.
		Threading::InterlockedPODType< WorkerState > myState;

		/// Current thread state, used only for proper shutting down.
		bool myHandleState;

		/// Event monitor for sleeping when it's "nothing to do here".
		/// The invariant that holds for sleeping workers is:
		///	"mySlack <= 0 && myStayte == Normal && I am on server's list of asleep workers" */
		Threading::EventMonitor myThreadMonitor;

		/// Next sleeping worker in free-list
		IntrusivePtr< Worker, SmartPointerPolicy< Worker, SmartPointerThreadSafeOp > > myNextSleepingFriend;

		/// My holding server
		IntrusiveWeakPtr< Server, SmartPointerPolicy< Server, SmartPointerThreadSafeOp > > myServer;
	};

	//! Job processor implementation
	class Worker : public Padded< WorkerCacheLine >
	{
	public:
		/// <summary cref="Worker::Worker">
		/// </summary>
		Worker( IntrusivePtr< Server >& server );

		/// <summary cref="Worker::~Worker">
		/// </summary>
		virtual ~Worker( void );

		/// <summary cref="Worker::~Worker">
		/// Construct a thread object with a new thread of execution.
		/// </summary>
		void Setup( size_t stackSize = 0, 
			bool manualResume = false, 
			Threading::ThreadPriorities prio = Threading::ThreadPrioritiesEnum::Medium, 
			const s8* name = NULL );

		/// <summary cref="Worker::IsValid">
		/// Checks if current worker is valid.
		/// </summary>
		bool IsValid() const;

		/// <summary cref="Worker::Join">
		/// Waits for thread's shutdown and releases all associated resources with that thread.
		/// </summary>
		void Join();

		/// <summary cref="Worker::EmitWakeupSignal">
		/// Signals to the worker to wake up.
		/// </summary>
		virtual void EmitWakeupSignal();

	private:
		virtual void Executor();
	};
}