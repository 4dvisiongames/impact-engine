/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Shared code
#include <Foundation/Shared.h>
/// Local string atom table
#include <Foundation/LocalStringAtomTable.h>

/// STL namespace
namespace STL
{
	__IME_IMPLEMENT_SINGLETON( LocalStringAtomTable );

	LocalStringAtomTable::LocalStringAtomTable()
	{
		__IME_CONSTRUCT_SINGLETON;
	}

	LocalStringAtomTable::~LocalStringAtomTable()
	{
		__IME_DESTRUCT_SINGLETON;
	}

	/**
		NOTE: the string added here must already be located in the global string
		buffer (see GlobalStringAtomTable for details).
	*/
	void LocalStringAtomTable::Add(const s8* str)
	{
		StaticString sstr;
		sstr.ptr = (s8*)str;
		this->table.InsertSorted(sstr);
	}
} // namespace STL