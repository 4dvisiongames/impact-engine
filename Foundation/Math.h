/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#include <math.h>

namespace Math
{
	const unsigned int MaxUInt32 = 0xFFFFFFFF;
	const int MinInt32 = 0x80000000;
	const int MaxInt32 = 0x7FFFFFFF;
	const float MinFloat = 1.e-8f;
	const float MaxFloat = 3.402823466e+38F;
	const float MinPosFloat = 1.175494351e-38F;

	const float Pi = 3.141592654f;
	const float TwoPi = 6.283185307f;
	const float PiRecip = 0.318309886f;
	const float TwoPiRecip = 0.159154943f;
	const float PiDiv2 = 1.570796327f;
	const float PiDiv4 = 0.785398163f;

	const float Epsilon = 0.000001f;
	const float ZeroEpsilon = 32.0f * MinPosFloat;  // Very small epsilon for checking against 0.0f
#ifdef __GNUC__
	const float NaN = __builtin_nanf("");
#else
	const float NaN = *(float *)&MaxUInt32;
#endif

	enum NoInitHint
	{
		NO_INIT
	};


	__IME_INLINED float DegreesToRadians( float f )
	{
		return f * 0.017453293f;
	}

	__IME_INLINED float RadiansToDegrees( float f )
	{
		return f * 57.29577951f;
	}

	template< typename T >
	__IME_INLINED T TClamp( T f, T min, T max )
	{
		if ( f < min )
			f = min;
		else if ( f > max )
			f = max;

		return f;
	}

	template< typename T >
	__IME_INLINED T TMin( T a, T b )
	{
		return a < b ? a : b;
	}

	template< typename T, typename V >
	__IME_INLINED T TMin( T a, V b )
	{
		return a < b ? a : b;
	}

	template< typename T >
	__IME_INLINED T TMax( T a, T b )
	{
		return a > b ? a : b;
	}

	__IME_INLINED float FAbs( float a )
	{
		Int32Float Converter( a );
		Converter.Int32 &= 0x7FFFFFFF;
		return Converter.Float;
	}

	__IME_INLINED float FSqrt( float a )
	{
		long i;
		float x, y;
		const float f = 1.5F;

		x = a * 0.5F;
		y = a;
		i = *(long *)&y;
		i = 0x5f3759df - (i >> 1);
		y = *(float *)&i;
		y = y * (f - (x * y * y));
		y = y * (f - (x * y * y));
		return a * y;
	}

	__IME_INLINED float FInvSqrt( float a )
	{
		float xhalf = 0.5f * a;
		s32 i = *(s32*)&a; // get bits for floating value
		i = 0x5f375a86 - (i >> 1); // gives initial guess y0
		a = *(float*)&i; // convert bits back to float
		a = a * (1.5f - xhalf * a * a); // Newton step, repeating increases accuracy
		return a;
	}

	__IME_INLINED float fsel( float test, float a, float b )
	{
		// Branchless selection
		return test >= 0 ? a : b;
	}

	// -------------------------------------------------------------------------------------------------
	// Conversion
	// -------------------------------------------------------------------------------------------------

	__IME_INLINED int FToIGeneric( double val )
	{
		// Float to int conversion using truncation

		return (int)val;
	}

	__IME_INLINED int FToIFastRound( double val )
	{
		// Fast round (banker's round) using Sree Kotay's method
		// This function is much faster than a naive cast from float to int

		union
		{
			double dval;
			int ival[2];
		} u;

		u.dval = val + 6755399441055744.0;  // Magic number: 2^52 * 1.5;
		return u.ival[0];         // Needs to be [1] for big-endian
	}

	// aligning methods
	template< typename T >
	__IME_INLINED T AlignUp( T arg, uintptr_t alignment )
	{
		return T( (static_cast<uintptr_t>(arg)+(alignment - 1)) & ~(alignment - 1) );
	}

	template< typename T > // works for not power-of-2 alignments
	__IME_INLINED T AlignUpGeneric( T arg, uintptr_t alignment )
	{
		if ( size_t rem = arg % alignment )
		{
			arg += alignment - rem;
		}
		return arg;
	}

	template< typename T >
	__IME_INLINED T AlignDown( T arg, uintptr_t alignment )
	{
		return T( static_cast<uintptr_t>(arg)& ~(alignment - 1) );
	}

	template< typename T >
	__IME_INLINED bool IsAligned( _In_opt_ T* pointer, uintptr_t alignment )
	{
		__IME_ASSERT( pointer );
		return 0 == (reinterpret_cast<uintptr_t>(pointer)& (alignment - 1));
	}

	template< typename T >
	__IME_INLINED bool IsPowerOfTwo( T arg )
	{
		return arg && (0 == (arg & (arg - 1)));
	}

	template< typename T, typename U >
	__IME_INLINED bool IsPowerOfTwoFactor( T arg, U divisor )
	{
		// Divisor is assumed to be a power of two (which is valid for current uses).
		__IME_ASSERT( IsPowerOfTwo( divisor ) );;
		return 0 == (arg & (arg - divisor));
	}

	template< typename T, typename U >
	__IME_INLINED bool IsPowerOfTwoMultiple( T arg, U multiply )
	{
		// Divisor is assumed to be a power of two (which is valid for current uses).
		__IME_ASSERT_MSG( IsPowerOfTwo( divisor ), "Divisor should be a power of two" );
		return arg && (arg & (divisor - 1));
	}

	template< typename T >
	__IME_INLINED T UpperPowerOfTwo( T inputValue )
	{
		inputValue--;
		inputValue |= inputValue >> 1;
		inputValue |= inputValue >> 2;
		inputValue |= inputValue >> 4;
		inputValue |= inputValue >> 8;
		inputValue |= inputValue >> 16;
		inputValue++;
		return inputValue;
	}
}