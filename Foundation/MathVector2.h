/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#include <Foundation/Math.h>

namespace Foundation 
{
namespace Math 
{
	__declspec( align( 16 ) )
	class Vector2
	{
	public:
		Vector2( );
		Vector2( float inX, float inY );

		Vector2 operator + ( const Vector2& rhs ) const;
		Vector2 operator - ( const Vector2& rhs ) const;
		Vector2 operator * ( const Vector2& rhs ) const;
		Vector2 operator / ( const Vector2& rhs ) const;

		Vector2 operator * ( const float scale ) const;
		Vector2 operator / ( const float scale ) const;

		Vector2 operator + ( const float scalar ) const;
		Vector2 operator - ( const float scalar ) const;

		Vector2 operator - ( ) const;

		Vector2& operator += ( const Vector2& rhs );
		Vector2& operator -= ( const Vector2& rhs );
		Vector2& operator *= ( const Vector2& rhs );
		Vector2& operator /= ( const Vector2& rhs );

		Vector2& operator *= ( const float scale );
		Vector2& operator /= ( const float scale );

		bool operator == ( const Vector2& rhs ) const;
		bool operator != ( const Vector2& rhs ) const;
		bool operator < ( const Vector2& rhs ) const;
		bool operator > ( const Vector2& rhs ) const;
		bool operator <= ( const Vector2& rhs ) const;
		bool operator >= ( const Vector2& rhs ) const;

		float GetX( ) const;
		float GetY( ) const;

		float GetAbsMax( ) const;

		float GetMax( ) const;
		float GetMin( ) const;
		float Length( ) const;
		float LengthSquared( ) const;

		Vector2 Normalized( float error = Math::MinFloat );

		float DotProduct( const Vector2& rhs );
		float CrossProduct( const Vector2& rhs );

		bool Equals( const Vector2& rhs, const float error ) const;

	private:
		float X;
		float Y;
	};	
} 
} // namespace Foundation::Math