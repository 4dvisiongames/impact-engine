/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Shared code
#include <Foundation/Shared.h>

namespace Math
{
	__IME_ALIGNED_MSVC( __IME_DEFAULT_VEC_ALIGNMENT )
	class VectorX
	{
	private:
		VectorX( void );

	public:
		VectorX( u32 Size );
		VectorX( u32 Size, float* Values );
		VectorX( const VectorX& Rhs );

		VectorX& operator = ( const VectorX& Rhs );

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
		VectorX( VectorX&& Move ) noexcept;
		VectorX& operator = ( VectorX&& Move );
#endif

		~VectorX( void );

		float operator[]( const u32 Index ) const;
		float& operator[]( const u32 Index );

		VectorX operator- ( void ) const;
		VectorX operator* ( const float Scalar ) const;
		VectorX operator/ ( const float Scalar ) const;
		VectorX operator- ( const VectorX& Rhs ) const;
		VectorX operator+ ( const VectorX& Rhs ) const;
		VectorX& operator*= ( const float Scalar );
		VectorX& operator/= ( const float Scalar );
		VectorX& operator+= ( const VectorX& Rhs );
		VectorX& operator-= ( const VectorX& Rhs );

		bool Compare( const VectorX&, const float Epsilon );
		bool operator != ( const VectorX& Rhs );
		bool operator == ( const VectorX& Rhs );

		float Length( void ) const;
		float LengthSqr( void ) const;

		VectorX Normalize( void ) const;
		u32 Dimension( void ) const;

	private:
		void Preallocate( u32 Size, float* Values );
		void Release( void );

		u32 MyElementsCount;
		u32 MyRowSize;
		float* MyRowData;
	} __IME_ALIGNED_GCC( __IME_DEFAULT_VEC_ALIGNMENT );

	__IME_INLINED VectorX::VectorX( void )
		: MyRowSize( 0 )
		, MyRowData( nullptr )
		, MyElementsCount( 0 )
	{
		/* Nothing */
	}

	__IME_INLINED VectorX::VectorX( u32 Size ) 
		: MyRowSize( 0 )
		, MyRowData( nullptr )
		, MyElementsCount( 0 ) 
	{
		this->Preallocate( Size, nullptr );
	}

	__IME_INLINED VectorX::VectorX( u32 Size, float* Value )
		: MyRowSize( 0 )
		, MyRowData( nullptr )
		, MyElementsCount( 0 )
	{
		this->Preallocate( Size, Value );
	}

	__IME_INLINED VectorX::VectorX( const VectorX& Rhs )
		: MyRowSize( Rhs.MyRowSize )
		, MyRowData( Rhs.MyRowData )
		, MyElementsCount( Rhs.MyElementsCount ) 
	{
		/* Nothing */
	}

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
	__IME_INLINED VectorX::VectorX( VectorX&& Move ) noexcept
		: MyRowSize( 0 )
		, MyRowData( nullptr ) 
	{
		this->MyElementsCount = Move.MyElementsCount;
		this->MyRowSize = Move.MyRowSize;
		this->MyRowData = Move.MyRowData;
		Move.MyRowSize = 0;
		Move.MyElementsCount = 0;
		Move.MyRowData = nullptr;
	}

	__IME_INLINED VectorX& VectorX::operator = ( VectorX&& Move ) 
	{
		if (this != &Move) 
		{
			this->Release();
			this->MyElementsCount = Move.MyElementsCount;
			this->MyRowSize = Move.MyRowSize;
			this->MyRowData = Move.MyRowData;
			Move.MyRowData = nullptr;
			Move.MyRowSize = 0;
		}

		return *this;
	}
#endif

	__IME_INLINED VectorX::~VectorX( void )
	{
		this->Release();
	}

	__IME_INLINED float VectorX::operator[]( const u32 Index ) const 
	{
		__IME_ASSERT_MSG( Index <= this->MyRowSize, "Index must be lower than maximum size of vector" );
		return this->MyRowData[Index];
	}

	__IME_INLINED float& VectorX::operator[]( const u32 Index ) 
	{
		__IME_ASSERT( Index <= this->MyRowSize );
		return this->MyRowData[Index];
	}

	__IME_INLINED VectorX VectorX::operator - ( void ) const 
	{
		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index ) {
			/// Fast negation based on XORing this value and lowest 32 bit signed integer
			Uint32Float Converter( this->MyRowData[Index] );
			Converter.Uint32 ^= 0x80000000;
			this->MyRowData[Index] = Converter.Float;
		}

		return *this;
	}

	__IME_INLINED VectorX VectorX::operator* (const float Scalar) const 
	{
		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index )
			this->MyRowData[Index] *= Scalar;

		return *this;
	}

	__IME_INLINED VectorX VectorX::operator / (const float Scalar) const 
	{
		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index )
			this->MyRowData[Index] /= Scalar;

		return *this;
	}

	__IME_INLINED VectorX VectorX::operator - (const VectorX& Rhs) const 
	{
		u32 ElementsCount = Math::TMin( this->MyElementsCount, Rhs.MyElementsCount );
		for ( u32 Index = 0; Index < ElementsCount; ++Index )
			this->MyRowData[Index] -= Rhs.MyRowData[Index];

		return *this;
	}

	__IME_INLINED VectorX VectorX::operator + (const VectorX& Rhs) const 
	{
		u32 ElementsCount = Math::TMin( this->MyElementsCount, Rhs.MyElementsCount );
		for ( u32 Index = 0; Index < ElementsCount; ++Index )
			this->MyRowData[Index] += Rhs.MyRowData[Index];

		return *this;
	}

	__IME_INLINED VectorX& VectorX::operator *= (const float Scalar) 
	{
		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index )
			this->MyRowData[Index] *= Scalar;

		return *this;
	}

	__IME_INLINED VectorX& VectorX::operator /= (const float Scalar) 
	{
		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index )
			this->MyRowData[Index] /= Scalar;

		return *this;
	}

	__IME_INLINED VectorX& VectorX::operator += (const VectorX& Rhs) 
	{
		u32 ElementsCount = Math::TMin( this->MyElementsCount, Rhs.MyElementsCount );
		for ( u32 Index = 0; Index < ElementsCount; ++Index )
			this->MyRowData[Index] += Rhs.MyRowData[Index];

		return *this;
	}

	__IME_INLINED VectorX& VectorX::operator -= (const VectorX& Rhs) 
	{
		u32 ElementsCount = Math::TMin( this->MyElementsCount, Rhs.MyElementsCount );
		for ( u32 Index = 0; Index < ElementsCount; ++Index )
			this->MyRowData[Index] += Rhs.MyRowData[Index];

		return *this;
	}

	__IME_INLINED bool VectorX::Compare( const VectorX& Rhs, const float Epsilon ) 
	{
		__IME_ASSERT_MSG( this->MyElementsCount == Rhs.MyElementsCount, "Both vectors must be the same size!" );
		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index ) {
			if ( Math::FAbs( this->MyRowData[Index] - Rhs.MyRowData[Index] ) > Epsilon )
				return false;
		}

		return true;
	}

	__IME_INLINED bool VectorX::operator != (const VectorX& Rhs) 
	{
		return this->Compare( Rhs, 0.0001 );
	}

	__IME_INLINED bool VectorX::operator == (const VectorX& Rhs) 
	{
		return !this->Compare( Rhs, 0.0001 );
	}

	__IME_INLINED float VectorX::Length( void ) const 
	{
		float Sum = 0.0f;

		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index )
			Sum += this->MyRowData[Index] * this->MyRowData[Index];

		return Math::FSqrt( Sum );
	}

	__IME_INLINED float VectorX::LengthSqr( void ) const 
	{
		float Sum = 0.0f;

		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index )
			Sum += this->MyRowData[Index] * this->MyRowData[Index];

		return Sum;
	}

	__IME_INLINED VectorX VectorX::Normalize( void ) const 
	{
		float Temp = 0.0f;
		VectorX Result( this->MyElementsCount );

		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index )
			Temp += this->MyRowData[Index] * this->MyRowData[Index];

		Temp = Math::FInvSqrt( Temp );

		for ( u32 Index = 0; Index < this->MyElementsCount; ++Index )
			Result.MyRowData[Index] = this->MyRowData[Index] * Temp;

		return Result;
	}

	__IME_INLINED u32 VectorX::Dimension( void ) const 
	{
		return this->MyElementsCount;
	}

	__IME_INLINED void VectorX::Preallocate( u32 Size, float* Value )
	{
		if (this->MyRowData) 
		{
			if (this->MyRowSize >= Size) 
			{
				Memory::LowLevelMemory::EraseMemoryBlock( this->MyRowData, sizeof( float ) * this->MyRowSize );

				if (Value)
					Memory::LowLevelMemory::CopyMemoryBlock( Value, this->MyRowData, sizeof( float ) * Size );

				this->MyElementsCount = Size;
			} 
			else if (this->MyRowSize < Size) 
			{
				delete[] this->MyRowData;
				this->MyRowData = new float[Size];
				this->MyRowSize = this->MyElementsCount = Size;

				if (Value)
					Memory::LowLevelMemory::CopyMemoryBlock( Value, this->MyRowData, sizeof( float ) * Size );
			}
		} 
		else 
		{
			this->MyRowData = new float[Size];
			this->MyRowSize = this->MyElementsCount = Size;
			if (Value)
				Memory::LowLevelMemory::CopyMemoryBlock( Value, this->MyRowData, sizeof( float ) * Size );
		}
	}

	__IME_INLINED void VectorX::Release( void ) 
	{
		if (this->MyRowData) 
		{
			delete[] this->MyRowData;
			this->MyRowData = nullptr;
		}

		this->MyRowSize = 0;
		this->MyElementsCount = 0;
	}
}