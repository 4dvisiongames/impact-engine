/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Memory utilities
#include <Foundation/MemoryUtils.h>
/// Memory pool
#include <Foundation/MemoryPool.h>
/// For new placement
#include <new> 

/// Codepoet: this is because evil Microsoft cannot handle std::bad_alloc like all normal people...

#if !__IME_CPP11_NEW_PLACEMENT
#if _MSC_VER
#define __IME_ALLOCATOR_THROW_0
#else
#define __IME_ALLOCATOR_THROW_0 throw(std::bad_alloc)
#endif /// _MSC_VER
#endif /// !__IME_CPP11_NEW_PLACEMENT

#if __IME_CPP11_NEW_PLACEMENT
#define __IME_DECLARE_CUSTOM_ALLOCATOR \
	public: \
		static void* operator new( std::size_t sz ); \
		static void* operator new( std::size_t sz, const std::nothrow_t& nothrow_value ) noexcept; \
		static void operator delete( void* ptr ) noexcept; \
		static void operator delete( void* ptr, const std::nothrow& ) noexcept; \
	private:

#define __IME_IMPLEMENT_CUSTOM_ALLOCATOR(Class, Allocator) \
	static Allocator theAllocator; \
	void* Class::operator new(std::size_t sz) { \
	return reinterpret_cast< void* >( theAllocator.Allocate() ); \
	} \
	void* Class::operator new( std::size_t size, const std::nothrow_t& nothrow_value ) noexcept { \
	return reinterpret_cast< void* >( theAllocator.Allocate() ); \
	} \
	void Class::operator delete(void* ptr) noexcept { \
	theAllocator.Free( static_cast< Allocator::Pointer >(ptr) ); \
	} \
	void Class::operator delete(void* ptr, const std::nothrow_t&) noexcept { \
	theAllocator.Free( static_cast< Allocator::Pointer >( ptr ); \
	}

#else
#define __IME_DECLARE_CUSTOM_ALLOCATOR \
	public: \
		static void* operator new( std::size_t sz ) __IME_ALLOCATOR_THROW_0; \
		static void* operator new( std::size_t sz, const std::nothrow_t& nothrow_value ) throw(); \
		static void operator delete( void* ptr ) throw(); \
		static void operator delete( void* ptr, const std::nothrow_t& ) throw(); \
	private:

#define __IME_IMPLEMENT_CUSTOM_ALLOCATOR(Class, Allocator) \
	static Allocator theAllocator; \
	void* Class::operator new( std::size_t sz ) __IME_ALLOCATOR_THROW_0 { \
		UNREFERENCED_PARAMETER( sz ); \
		return reinterpret_cast< void* >( theAllocator.Allocate() ); \
	} \
	void* Class::operator new( std::size_t size, const std::nothrow_t& nothrow_value ) throw() { \
		UNREFERENCED_PARAMETER( size ); \
		UNREFERENCED_PARAMETER( nothrow_value ); \
		return reinterpret_cast< void* >( theAllocator.Allocate() ); \
	} \
	void Class::operator delete( void* ptr ) throw() { \
		theAllocator.Free( static_cast< Allocator::Pointer >( ptr ) ); \
	} \
	void Class::operator delete( void* ptr, const std::nothrow_t& ) throw() { \
		theAllocator.Free( static_cast< Allocator::Pointer >( ptr ) ); \
	}

#endif /// __IME_CPP11_NEW_PLACEMENT

#define __alloca_align(s, align) _alloca( Math::AlignUp< size_t >( s, align ) )
#define __alloca16(s) __alloca_align(s, 16)

namespace Memory
{
	/// Aligned allocator policy
	class Allocator
	{
	public:
		/// <summary cref="Allocator::Malloc">
		/// Allocate new chunk of memory of heap. Returns 16-byte aligned memory block.
		/// </summary>
		/// <param name="sz">Size of memory to ask from OS.</param>
		/// <param name="heap">Pointer to the heap used for memory allocation.</param>
		/// <returns>Pointer to the begining of allocated memory block.</returns>
		/// <remarks>
		/// Default <c>heap</c> parameter is NULL, means that default heap will be used in memory allocation
		/// process. Otherwise you can set your custom heap.
		/// </remarks>
		static _Ret_maybenull_ _Post_writable_byte_size_(size) void* Malloc( const SizeT size, 
																			 const SizeT alignment = 16 );

		static _Ret_maybenull_ _Post_writable_byte_size_(size) void* Realloc( _In_opt_ void *ptr,
																			  const SizeT size, 
																			  const SizeT alignment = 16 );

		/// <summary cref="Allocator::Free">
		/// Free allocated memory chunk. 
		/// </summary>
		/// <param name="address">Old memory address to use data from.</param>
		/// <param name="heap">Pointer to the heap used for memory allocation.</param>
		/// <returns>True if deletion has been successfull.</returns>
		/// <remarks>
		/// Default <c>heap</c> parameter is NULL, means that default heap will be used in memory freeing
		/// process. Otherwise you can set your custom heap.
		/// </remarks>
		static bool Free( _In_opt_ void* pMemoryBlock );

		/// <summary cref="Allocator::Size">
		/// Get size of one memory block allocated with the engine's allocator.
		/// </summary>
		/// <param name="address">Memory address to use data from.</param>
		/// <returns>Size of memory block.</returns>
		static size_t Size( _In_opt_ void* pMemoryBlock );
	};
}

#ifdef new
#undef new
#endif

#ifdef delete
#undef delete
#endif

/// This is default memory allocator used by all objects that are not inherited from Core::ReferenceCount

#if __IME_CPP11_NEW_PLACEMENT
__IME_INLINED void* __cdecl operator new(std::size_t size) throw(std::bad_alloc) {
	return Memory::Allocator::Malloc( size );
}

__IME_INLINED void* __cdecl operator new(std::size_t size, const std::nothrow_t& nothrow_value) noexcept {
	return Memory::Allocator::Malloc( size );
}

__IME_INLINED void* __cdecl operator new[]( std::size_t size ) throw(std::bad_alloc) {
	return Memory::Allocator::Malloc( size );
}

__IME_INLINED void* __cdecl operator new[]( std::size_t size, const std::nothrow_t& nothrow_value ) noexcept {
	return Memory::Allocator::Malloc( size );
}

__IME_INLINED void __cdecl operator delete( void* ptr ) noexcept {
	Memory::Allocator::Free( ptr );
}

__IME_INLINED void __cdecl operator delete( void* ptr, const std::nothrow_t& ) noexcept {
	Memory::Allocator::Free( ptr );
}

__IME_INLINED void __cdecl operator delete[]( void* ptr ) noexcept {
	Memory::Allocator::Free( ptr );
}

__IME_INLINED void __cdecl operator delete[]( void* ptr, const std::nothrow_t& ) noexcept {
	Memory::Allocator::Free( ptr );
}

#else 
__IME_INLINED 
_Ret_notnull_ _Post_writable_byte_size_(size)  
void* __cdecl operator new(std::size_t size) __IME_ALLOCATOR_THROW_0 {
	void* p = Memory::Allocator::Malloc( size );
	__IME_ASSERT_MSG(p, "Allocation result is null, your host might be out of memory!");
	return p;
}

__IME_INLINED
_Ret_maybenull_ _Post_writable_byte_size_(size)
void* __cdecl operator new(std::size_t size, const std::nothrow_t& nothrow_value) throw() {
	UNREFERENCED_PARAMETER( nothrow_value );
	return Memory::Allocator::Malloc( size );
}

__IME_INLINED
_Ret_notnull_ _Post_writable_byte_size_(size)
void* __cdecl operator new[](std::size_t size) __IME_ALLOCATOR_THROW_0 {
	void* p = Memory::Allocator::Malloc(size);
	__IME_ASSERT_MSG(p, "Allocation result is null, your host might be out of memory!");
	return p;
}

__IME_INLINED
_Ret_maybenull_ _Post_writable_byte_size_(size)
void* __cdecl operator new[]( std::size_t size, const std::nothrow_t& nothrow_value ) throw() {
	UNREFERENCED_PARAMETER( nothrow_value );
	return Memory::Allocator::Malloc( size );
}

__IME_INLINED void __cdecl operator delete( void* ptr ) throw() {
	Memory::Allocator::Free( ptr );
}

__IME_INLINED void __cdecl operator delete( void* ptr, const std::nothrow_t& ) throw() {
	Memory::Allocator::Free( ptr );
}

__IME_INLINED void __cdecl operator delete[]( void* ptr ) throw() {
	Memory::Allocator::Free( ptr );
}

__IME_INLINED void __cdecl operator delete[]( void* ptr, const std::nothrow_t& ) throw() {
	Memory::Allocator::Free( ptr );
}

#endif /// __IME_CPP11_NEW_PLACEMENT