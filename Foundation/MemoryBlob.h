/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _NanoMemoryBlob_h_
#define _NanoMemoryBlob_h_

/// Engine shared code
#include <Foundation/Shared.h>
/// Low-level memory routines
#include <Foundation/LowLevelMemory.h>

/// Memory
namespace Memory
{
	/// Memory blob
	class Blob
	{
	public:
		/// default constructor
		Blob( void );
		/// constructor
		Blob( const void* ptr, SizeT size );
		/// reserve N bytes
		Blob( SizeT size );
		/// copy constructor
		Blob( const Blob& rhs );
		/// destructor
		~Blob( void );
		/// assignment operator
		void operator = ( const Blob& rhs );
    
		/// equality operator
		bool operator==(const Blob& rhs) const;
		/// inequality operator
		bool operator!=(const Blob& rhs) const;
		/// greater operator
		bool operator>(const Blob& rhs) const;
		/// less operator
		bool operator<(const Blob& rhs) const;
		/// greater-equal operator
		bool operator>=(const Blob& rhs) const;
		/// less-eqial operator
		bool operator<=(const Blob& rhs) const;
    
		/// return true if the blob contains data
		bool IsValid( void ) const;
		/// reserve N bytes
		void Reserve( SizeT size );
		/// trim the size member (without re-allocating!)
		void Trim( SizeT size );
		/// set blob contents
		void Set( const void* ptr, SizeT size );
		/// get blob ptr
		void* GetPtr( void ) const;
		/// get blob size
		SizeT Size( void ) const;
		/// get a hash code (compatible with Util::HashTable)
		SizeT HashCode( void ) const;

	private:
		/// delete content
		void Delete( void );
		/// allocate internal buffer
		void Allocate( SizeT size );
		/// copy content
		void Copy( const void* ptr, SizeT size );
		/// do a binary comparison between this and other blob
		int BinaryCompare( const Blob& rhs ) const;

		void* ptr;
		SizeT size;
		SizeT allocSize;
	};


	__IME_INLINED Blob::Blob() : ptr(0), size(0), allocSize(0)
	{
		/// Empty
	}

	__IME_INLINED bool Blob::IsValid() const
	{
		return ( this->ptr != nullptr );
	}

	__IME_INLINED void Blob::Delete()
	{
		if ( this->IsValid() ) {
			__IME_ASSERT( this->ptr != nullptr );
			Memory::Allocator::Free( (void*)this->ptr );
			this->ptr = nullptr;
			this->size = 0;
			this->allocSize = 0;
		}
	}

	__IME_INLINED Blob::~Blob()
	{
		this->Delete();
	}

	__IME_INLINED void Blob::Allocate(SizeT s)
	{
		__IME_ASSERT( !this->IsValid() );
		this->ptr = Memory::Allocator::Malloc( s );
		this->allocSize = s;
		this->size = s;
	}

	__IME_INLINED void Blob::Copy( const void* fromPtr, SizeT fromSize )
	{
		__IME_ASSERT(( fromPtr != nullptr ) && ( fromSize > 0 ));

		// only re-allocate if not enough space
		if (( this->ptr == nullptr ) || ( this->allocSize < fromSize ))
		{
			this->Delete();
			this->Allocate(fromSize);
		}
		this->size = fromSize;
		Memory::LowLevelMemory::CopyMemoryBlock( fromPtr, this->ptr, fromSize );
	}

	__IME_INLINED Blob::Blob( const void* fromPtr, SizeT fromSize ) : ptr(0), size(0), allocSize(0)
	{    
		this->Copy( fromPtr, fromSize );
	}

	__IME_INLINED Blob::Blob( const Blob& rhs ) : ptr(0), size(0), allocSize(0)
	{
		if (rhs.IsValid())
		{
			this->Copy(rhs.ptr, rhs.size);
		}
	}

	__IME_INLINED Blob::Blob(SizeT s) : ptr(0), size(0), allocSize(0)
	{
		this->Allocate(s);
	}

	__IME_INLINED void Blob::operator = ( const Blob& rhs )
	{
		if (rhs.IsValid())
		{
			this->Copy( rhs.ptr, rhs.size );
		}
	}

	__IME_INLINED bool Blob::operator == ( const Blob& rhs ) const
	{
		return (this->BinaryCompare(rhs) == 0);
	}

	__IME_INLINED bool Blob::operator != ( const Blob& rhs ) const
	{
		return ( this->BinaryCompare(rhs) != 0 );
	}

	__IME_INLINED bool Blob::operator > ( const Blob& rhs ) const
	{
		return (this->BinaryCompare(rhs) > 0);
	}
            
	__IME_INLINED bool Blob::operator < ( const Blob& rhs ) const
	{
		return (this->BinaryCompare(rhs) < 0);
	}

	__IME_INLINED bool Blob::operator >= ( const Blob& rhs ) const
	{
		return (this->BinaryCompare(rhs) >= 0);
	}

	__IME_INLINED bool Blob::operator <= ( const Blob& rhs ) const
	{
		return (this->BinaryCompare(rhs) <= 0);
	}

	__IME_INLINED void Blob::Reserve( SizeT s )
	{
		if (this->allocSize < s)
		{
			this->Delete();
			this->Allocate(s);
		}
		this->size = s;
	}

	__IME_INLINED void Blob::Trim( SizeT trimSize )
	{
		__IME_ASSERT( trimSize <= this->size );
		this->size = trimSize;
	}

	__IME_INLINED void Blob::Set( const void* fromPtr, SizeT fromSize )
	{
		this->Copy(fromPtr, fromSize);
	}

	__IME_INLINED void* Blob::GetPtr() const
	{
		__IME_ASSERT(this->IsValid());
		return this->ptr;
	}

	__IME_INLINED SizeT Blob::Size() const
	{
		__IME_ASSERT(this->IsValid());
		return this->size;
	}

	__IME_INLINED SizeT Blob::HashCode() const
	{
		SizeT hash = 0;
		const s8* charPtr = (const s8*) this->ptr;
		SizeT i;
		for (i = 0; i < this->size; i++)
		{
			hash += charPtr[i];
			hash += hash << 10;
			hash ^= hash >>  6;
		}
		hash += hash << 3;
		hash ^= hash >> 11;
		hash += hash << 15;
		hash &= ~(1<<31);       // don't return a negative number (in case SizeT is defined signed)
		return hash;
	}

} // namespace NanoMemory

#endif /// _NanoMemoryBlob