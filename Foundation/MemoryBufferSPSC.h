/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Engine shared code
#include <Foundation/Shared.h>
/// Low-Level memory routines
#include <Foundation/LowLevelMemory.h>
/// Memory barriers
#include <Foundation/MemoryBarriers.h>
/// Core mathematics
#include <Foundation/Math.h>

/// Threading namespace
namespace Threading
{
	/// <summary>
	/// <c>MemoryBufferSPSC</c> is a pipe class that is designed for use by at most 
	/// two threads: one reader, one writer. Access by more than two threads isn't 
	/// guaranteed to be safe.
	/// </summary>
	/// <remarks>
	/// In order to provide efficient access the size of the buffer is passed
	/// as a template parameter and restricted to powers of two less than 31.
	/// </remarks>
	template< u8 bufferSize > 
	class MemoryBufferSPSC
	{
	private:
		/// Static members
		static const u8 ucBufferSizeLog2 = minimum( bufferSize, 31 );
		static const USizeT ulBufferSize = ( 1 << ucBufferSizeLog2 );
		static const USizeT ulBitMask = ulBufferSize - 1;

		/// Disable assignment access
		__IME_DISABLE_ASSIGNMENT( MemoryBufferSPSC );

		u8 ucBuffer[ulBufferSize]; //!< This buffer size
		/// Note that these offsets are not clamped to the buffer size.
		/// Instead the calculations rely on wrapping at ULONG_MAX+1.
		/// See the comments in Read() for details.
		volatile USizeT __IME_ALIGNED_MSVC( 4 ) myReadOffset __IME_ALIGNED_GCC( 4 );
		volatile USizeT __IME_ALIGNED_MSVC( 4 ) ulWriteOffset __IME_ALIGNED_GCC( 4 );

	public:
		/// <summary cref="MemoryBufferSPSC::MemoryBufferSPSC">
		/// Constructor.
		/// </summary>
		MemoryBufferSPSC( void );

		/// <summary cref="MemoryBufferSPSC::GetSize">
		/// Get buffer size.
		/// </summary>
		/// <returns>Buffer size in bytes.</returns>
		u8 GetSize( void ) const;

		/// <summary cref="MemoryBufferSPSC::BytesAvailable">
		/// Bytes available after reading and writing.
		/// </summary>
		/// <returns>Available bytes.</returns>
		USizeT BytesAvailable( void ) const;

		/// <summary cref="MemoryBufferSPSC::Read">
		/// Reading queue from thread.
		/// </summary>
		/// <param name="ptrDest">Destination block of data.</param>
		/// <param name="uBytes">Bytes to read.</param>
		/// <returns>True if reading queue is successfull, otherwise false.</returns>
		bool Read( void* ptrDest, USizeT uBytes );

		/// <summary cref="MemoryBufferSPSC::Write">
		/// Writing queue to thread.
		/// </summary>
		/// <param name="ptrSrc">Source block of data.</param>
		/// <param name="uBytes">Bytes to write.</param>
		/// <returns>True if writing queue is successfull, otherwise false.</returns>
		bool Write( void* ptrSrc, USizeT uBytes );
	};

	template< u8 bufferSize > 
	__IME_INLINED MemoryBufferSPSC< bufferSize >::MemoryBufferSPSC( void )
		: myReadOffset( 0 )
		, ulWriteOffset( 0 )
	{
		/// Empty
	}

	template< u8 bufferSize > 
	__IME_INLINED u8 MemoryBufferSPSC< bufferSize >::GetSize( void ) const
	{
		/// Get buffer size
		return this->ucBufferSize;
	}

	template< u8 bufferSize > 
	__IME_INLINED USizeT MemoryBufferSPSC< bufferSize >::BytesAvailable( void ) const
	{
		/// Calc diferrence between read bytes and write bytes(atomic)
		USizeT result = this->myWriteOffset - this->ulReadOffset;
		/// Get atomic result
		return result;
	}

	template< u8 bufferSize > 
	__IME_INLINED bool MemoryBufferSPSC< bufferSize >::Read( void* ptrDst, USizeT uBytes )
	{
		/// Store the read and write offsets into local variables--this is
        /// essentially a snapshot of their values so that they stay constant
        /// for the duration of the function (and so we don't end up with cache 
        /// misses due to false sharing).
		USizeT readOffset = __IME_READ_AS_ALIGNED_CTF( this->myReadOffset );
		USizeT writeOffset = __IME_READ_AS_ALIGNED_CTF( this->ulWriteOffset );

		/// Compare the two offsets to see if we have anything to read.
        /// Note that we don't do anything to synchronize the offsets here.
        /// Really there's not much we *can* do unless we're willing to completely
        /// synchronize access to the entire object. We have to assume that as we 
        /// read, someone else may be writing, and the write offset we have now
        /// may be out of date by the time we read it. Fortunately that's not a
        /// very big deal. We might miss reading some data that was just written.
        /// But the assumption is that we'll be back before long to grab more data
        /// anyway.
        ///
        /// Note that this comparison works because we're careful to constrain
        /// the total buffer size to be a power of 2, which means it will divide
        /// evenly into ULONG_MAX+1. That, and the fact that the offsets are 
        /// unsigned, means that the calculation returns correct results even
        /// when the values wrap around.
		USizeT cbAvailable = writeOffset - readOffset;
        if( uBytes > cbAvailable )
            return false;

		/// The data has been made available, but we need to make sure
        /// that our view on the data is up to date -- at least as up to
        /// date as the control values we just read. We need to prevent
        /// the compiler or CPU from moving any of the data reads before
        /// the control value reads.

		/// Reading a control value and then having a barrier is known
        /// as a "read-acquire."
		__IME_MEMORY_READWRITE_BARRIER();

		/// Load data to local variable
		u8* lpDest = static_cast< u8* >( ptrDst );

		/// Find actual read offset
		USizeT actualReadOffset = __IME_READ_AS_ALIGNED_CTF( this->myReadOffset ) & this->ulBitMask;
		/// Bytes
        USizeT bytesLeft = uBytes;

		/// Copy from the tail, then the head. Note that there's no explicit
        /// check to see if the write offset comes between the read offset
        /// and the end of the buffer--that particular condition is implicitly
        /// checked by the comparison with AvailableToRead(), above. If copying
        /// cbDest bytes off the tail would cause us to cross the write offset,
        /// then the previous comparison would have failed since that would imply
        /// that there were less than cbDest bytes available to read.
		USizeT cbTailBytes = Math::TMin( bytesLeft, this->ulBufferSize - actualReadOffset );
		/// Copy source memory block to destination block with selected mem size
		Memory::LowLevelMemory::CopyMemoryBlock( this->ucBuffer + actualReadOffset, lpDest, cbTailBytes );
		/// Substract already read bytes
        bytesLeft -= cbTailBytes;

		if( bytesLeft )
			Memory::LowLevelMemory::CopyMemoryBlock( this->ucBuffer, lpDest + cbTailBytes, bytesLeft );

		/// When we update the read offset we are, effectively, 'freeing' buffer
        /// memory so that the writing thread can use it. We need to make sure that
        /// we don't free the memory before we have finished reading it. That is,
        /// we need to make sure that the write to m_readOffset can't get reordered
        /// above the reads of the buffer data. The only way to guarantee this is to
        /// have an export barrier to prevent both compiler and CPU rearrangements
		__IME_MEMORY_READWRITE_BARRIER();

		/// Advance the read offset. From the CPUs point of view this is several
        /// operations--read, modify, store--and we'd normally want to make sure that
        /// all of the operations happened atomically. But in the case of a single
        /// reader, only one thread updates this value and so the only operation that
        /// must be atomic is the store. That's lucky, because 32-bit aligned stores are
        /// atomic on all modern processors.
		readOffset += uBytes;
        this->myReadOffset = readOffset;

		return true;
	}

	template< u8 bufferSize > 
	__IME_INLINED bool MemoryBufferSPSC< bufferSize >::Write( void* lpSrc, USizeT uBytes )
	{
		/// Reading the read offset here has the same caveats as reading
        /// the write offset had in the Read() function above. 
		USizeT readOffset = __IME_READ_AS_ALIGNED_CTF( this->myReadOffset );
		USizeT writeOffset = __IME_READ_AS_ALIGNED_CTF( this->ulWriteOffset );

		/// Compute the available write size. This comparison relies on
        /// the fact that the buffer size is always a power of 2, and the
        /// offsets are unsigned integers, so that when the write pointer
        /// wraps around the subtraction still yields a value (assuming
        /// we haven't messed up somewhere else) between 0 and c_cbBufferSize - 1.
		USizeT cbAvailable = this->ulBufferSize - ( writeOffset - readOffset );
		/// Fail is written bytes size is bigger than blob's size
		if( uBytes > cbAvailable )
            return false;

		/// It is theoretically possible for writes of the data to be reordered
        /// above the reads to see if the data is available. Improbable perhaps,
        /// but possible. This barrier guarantees that the reordering will not
        /// happen.
		__IME_MEMORY_READWRITE_BARRIER();

		/// Write the data
        const u8* pbSrc = static_cast< const u8* >( lpSrc );
		/// Actual write offset of current buffer
		USizeT actualWriteOffset = __IME_READ_AS_ALIGNED_CTF( this->ulWriteOffset ) & this->ulBitMask;
		/// How much bytes do we left, when we copy to temporary buffer
        USizeT bytesLeft = uBytes;

		/// See the explanation in the Read() function as to why we don't 
        /// explicitly check against the read offset here.
		USizeT cbTailBytes = Math::TMin( bytesLeft, this->ulBufferSize - actualWriteOffset );
		/// Copy memory block of source buffer to temporary read-write buffer
		Memory::LowLevelMemory::CopyMemoryBlock< u8, u8 >( pbSrc, this->ucBuffer + actualWriteOffset, cbTailBytes );
		/// We've already made a copy, erase wasted bytes of memory
        bytesLeft -= cbTailBytes;

		if( bytesLeft )
			Memory::LowLevelMemory::CopyMemoryBlock( pbSrc + cbTailBytes, this->ucBuffer, bytesLeft );

		/// Now it's time to update the write offset, but since the updated position
        /// of the write offset will imply that there's data to be read, we need to 
        /// make sure that the data all actually gets written before the update to
        /// the write offset. The writes could be reordered by the compiler (on any
        /// platform) or by the CPU (on Xbox 360). We need a barrier which prevents
        /// the writes from being reordered past each other.
		__IME_MEMORY_READWRITE_BARRIER();

		/// See comments in Read() as to why this operation isn't interlocked.
        writeOffset += uBytes;
        this->ulWriteOffset = writeOffset;

		return true;
	}
}