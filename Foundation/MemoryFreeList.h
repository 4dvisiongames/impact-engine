/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#ifndef __MemoryFreeList_h__
#define __MemoryFreeList_h__

#include <Foundation/Shared.h>

namespace Memory 
{
	template< typename T, SizeT SizeOf = sizeof( T ), SizeT Alignment = 16 > 
	class FreeList
	{
	public:
		FreeList( void );
		~FreeList( void );

		//! Shutdown free list object
		void Shutdown( void );

		//! Allocate memory block on free list
		T* Alloc( void );
		//! Free element allocated on free list
		void Free( T* element );

		bool IsStarving( void ) const;
		bool IsValid( void ) const;

		size_t GetTotalCount( void ) const;
		size_t GetAllocCount( void ) const;
		size_t GetFreeCount( void ) const;

	private:
		enum { MyAlignment = Alignment };
		enum { MySize = SizeOf };

		struct Element 
		{
			Element* myNext;
		};

		Element* myFreeList;
		SizeT myTotal;
		SizeT myActive;
	};

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED FreeList< T, SizeOf, Alignment >::FreeList( void ) 
		: blocks( nullptr )
		, free( nullptr )
		, total( 0 )
		, active( 0 ) {
		/// Empty
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED FreeList< T, SizeOf, Alignment >::~FreeList( void ) {
		if( this->IsValid() ) {
			this->Shutdown();
		}
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED T* FreeList< T, SizeOf, Alignment >::Alloc( void ) {
		Element* result;

		if( (result = this->myFreeList) != nullptr ) {
			this->myFreeList = result->myNext;
		} else {
			result = Memory::Allocator::Malloc( sizeof( Element ) + MySize, MyAlignment );
			this->myTotal++;
		}

		this->myActive++;
		return reinterpret_cast< T* >( (reinterpret_cast< u8 >( result ) + sizeof( Element ) ) );
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED void FreeList< T, SizeOf, Alignment >::Free( T* ptr ) {
		Element* element = reinterpret_cast< Element* >( ( reinterpret_cast< u8* >( ptr ) - sizeof( Element ) ) );
		element->myNext = this->myFreeList;
		this->myFreeList = element;
		this->myActive--;
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED void FreeList< T, SizeOf, Alignment >::Shutdown( void ) {
		__IME_ASSERT( !this->myActive ); 

		if( !this->IsValid() )
			return;

#if defined( _DEBUG )
		size_t deleted = 0;
		size_t guardValue = this->myTotal;
#endif

		while( Element* element = this->myFreeList ) {
			this->myFreeList = element->myNext;
			Memory::Allocator::Free( element );
			deleted++;
		}

		__IME_ASSERT( deleted == guardValue );

		this->myFree = nullptr;
		this->myTotal = 0;
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED bool FreeList< T, SizeOf, Alignment >::IsStarving() const {
		return !this->myFreeList;
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED bool FreeList< T, SizeOf, Alignment >::IsValid() const {
		return this->myTotal != 0 || !this->IsStarving();
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED size_t FreeList< T, SizeOf, Alignment >::GetTotalCount() const { 
		return this->myTotal; 
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED size_t FreeList< T, SizeOf, Alignment >::GetAllocCount() const { 
		return this->myActive; 
	}

	template< typename T, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED size_t FreeList< T, SizeOf, Alignment >::GetFreeCount() const { 
		return this->myTotal - this->myActive; 
	}
}

#endif /// __MemoryFreeList_h__