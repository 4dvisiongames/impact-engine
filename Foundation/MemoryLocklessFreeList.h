/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#ifndef __MemoryLocklessFreeList_h__
#define __MemoryLocklessFreeList_h__

/// Memory pools
#include <Foundation/MemoryPool.h>
/// Interlocked
#include <Foundation/Interlocked.h>

namespace Memory 
{
	template< typename T, size_t SMemoryBlock = sizeof( T ), SizeT SAlignment = 16 >
	class LocklessFreeList
	{
	public:
		typedef LocklessFreeList< T, SMemoryBlock, SAlignment > ThisType;
		typedef T ValueType;
		typedef T* Pointer;
		typedef T& Reference;
		typedef const T* ConstPointer;
		typedef const T& ConstReference;

		LocklessFreeList();
		~LocklessFreeList();

		Pointer Allocate();
		void Free( Pointer ptr );
		bool IsStarving() const;

		SizeT GetTotalCount() const;
		SizeT GetAllocCount() const;
		SizeT GetFreeCount() const;

	private:
		enum { MyAlignment = SAlignment };
		enum { MyBlockSize = SMemoryBlock };

		struct Element {
			Element* myNext;
			SizeT myABAEpoch;
			SizeT myMarkedState;
		};

		Element* myFreeList;
		SizeT myTotal;
		SizeT myActive;

		void LinkToFreeList( Element* toLink );
		Element* UnlinkFromFreeList();
		void Mark( Element* toMark );
		bool NeedsToBeMarked( Element* toCheck );
		void Clean() { }
	};

	template< typename T, size_t SBlockSize, SizeT SAlignment >
	__IME_INLINED LocklessFreeList< T, SBlockSize, SAlignment >::LocklessFreeList() 
		: myFreeList( nullptr )
		, myTotal( 0 )
		, myActive( 0 ) {
		/// Empty
	}

	template< typename T, size_t SBlockSize, SizeT SAlignment >
	__IME_INLINED LocklessFreeList< T, SBlockSize, SAlignment >::~LocklessFreeList() {
		this->Clean();
	}

	template< typename T, size_t SBlockSize, SizeT SAlignment >
	__IME_INLINED typename LocklessFreeList< T, SBlockSize, SAlignment >::Pointer LocklessFreeList< T, SBlockSize, SAlignment >::Allocate() {
		Element* element;

		if( (element = this->UnlinkFromFreeList()) != nullptr ) {
			/// Successfully got element
		} else {
			SizeT memoryDataSize = Math::AlignUp< SizeT >( MyBlockSize, MyAlignment );
			element = static_cast< Element* >( Memory::Allocator::Malloc( sizeof( Element ) + memoryDataSize ) );
			Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SizeT >( this->myTotal, 1 );
		}

		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SizeT >( this->myActive, 1 );
		return reinterpret_cast< Pointer >( reinterpret_cast< uintptr_t >( element ) + sizeof( Element ) );
	}

	template< typename T, size_t SBlockSize, SizeT SAlignment >
	__IME_INLINED void LocklessFreeList< T, SBlockSize, SAlignment >::Free( Pointer ptr ) {
		//memset( ptr, 0, sizeof( ValueType ) );
		Memory::LowLevelMemory::EraseMemoryBlock( ptr, sizeof(ValueType) );
		Element* element = reinterpret_cast< Element* >( ( reinterpret_cast< uintptr_t >( ptr ) - sizeof( Element ) ) );
		this->LinkToFreeList( element );

		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental, SizeT, SSizeT >( this->myActive, -1 );
	}

	template< typename T, size_t SBlockSize, SizeT Alignment >
	__IME_INLINED void LocklessFreeList< T, SBlockSize, Alignment >::LinkToFreeList( Element* linked ) {
		/// TODO: make this a bit weaker with consume semantics
		Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::AcquireRelease, SizeT, SizeT >( linked->myABAEpoch, 1 );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release, SizeT, SizeT >( linked->myMarkedState, 1 );
		if( this->NeedsToBeMarked( linked ) )
			this->Mark( linked );
	}

	template< typename T, size_t SBlockSize, SizeT Alignment >
	__IME_INLINED typename LocklessFreeList< T, SBlockSize, Alignment >::Element* LocklessFreeList< T, SBlockSize, Alignment >::UnlinkFromFreeList( void ) {
		Element* myHead = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myFreeList );
		while( myHead != nullptr ) {
			Element* prev = myHead;
			SizeT abaEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( myHead->myABAEpoch );
			if( abaEpoch == 0 || Threading::Interlocked::CompareAndSwap< SizeT, SizeT, SizeT >( myHead->myABAEpoch, abaEpoch + 1, abaEpoch ) != abaEpoch ) {
				myHead = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myFreeList );
			}

			Element* next = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( myHead->myNext );
			if( Threading::Interlocked::CompareAndSwap( this->myFreeList, next, myHead ) == myHead ) {
				if ( myHead != nullptr )
					Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Relaxed, SizeT, SSizeT >( myHead->myABAEpoch, -2 );
				return myHead;
			}

			if( this->NeedsToBeMarked( prev ) && Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( prev->myMarkedState ) == (SizeT)1 )
				this->Mark( prev );
		}

		return nullptr;
	}

	template< typename T, size_t SBlockSize, SizeT Alignment >
	__IME_INLINED void LocklessFreeList< T, SBlockSize, Alignment >::Mark( Element* linkAt ) {
		Element* head = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myFreeList );
		__forever 
		{
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( linkAt->myMarkedState, 0 );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( linkAt->myNext, head );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release, SizeT, SizeT >( linkAt->myABAEpoch, 1 );
			if( Threading::Interlocked::CompareAndSwap( this->myFreeList, linkAt, head ) != head ) 
			{
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, SizeT, SizeT >( linkAt->myMarkedState, 1 );
				if( this->NeedsToBeMarked( linkAt ) )
					continue;
			}

			return;
		}
	}

	template< typename T, size_t SBlockSize, SizeT Alignment >
	__IME_INLINED bool LocklessFreeList< T, SBlockSize, Alignment >::NeedsToBeMarked( Element* toCheck ) {
		return Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::AcquireRelease, SizeT, SSizeT >( toCheck->myABAEpoch, -1 ) == (SizeT)1;
	}
}

#endif /// __MemoryLocklessFreeList_h__