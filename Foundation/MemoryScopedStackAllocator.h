/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#include <Foundation/STLArray.h>
#include <Foundation/MemoryStackFrame.h>
#include <Foundation/STLGenericIterator.h>

namespace Memory
{
	template< SizeT StackSize, 
			  SizeT Alignment = Traits::Workarounds::AlignmentBug<sizeof(uintptr_t),uintptr_t>::alignment >
	class ScopedStack
	{
	public:
		typedef ScopedStack ThisType;
		typedef SizeT SizeType;

		ScopedStack() : myStackPointer(myData) { }

		template< typename Type >
		Type& Construct();

		template< typename Type, typename... Args >
		Type& Construct(Args... args);

		template< typename Type >
		Type* ConstructArray(SizeType count);

		template<typename Function>
		ThisType& operator << (Function scopedFunction);

	private:
		enum { StorageSize = StackSize };
		enum { StorageAlignment = Alignment };
		enum { StorageSizeWithAlignment = (StorageSize + StorageAlignment) & ~(StorageAlignment - 1) };

		SizeType EnterStackFrame();
		void LeaveStackFrame();

		void* Allocate(const SizeT size, const SizeT alignment);

		STL::Array<StackFrame*> myStackFrames;
		u8 myData[StorageSizeWithAlignment];
		u8* myStackPointer; 
	};

	template< SizeT StackSize, SizeT Alignment >
	template< typename Type >
	Type& ScopedStack<StackSize, Alignment>::Construct() {
		Type* ptr = new(this->Allocate(sizeof(Type), __IME_ALIGN_OF(Type))) Type();
		__IME_ASSERT_MSG(ptr, "Failed to allocate more memory! Possibly, you are out of memory.");
		return *ptr;
	}

	template< SizeT StackSize, SizeT Alignment >
	template< typename Type >
	Type* ScopedStack<StackSize, Alignment>::ConstructArray(SizeType count) {
		SizeType size = sizeof(Type) * count;
		Type* ptr = static_cast<Type*>(this->Allocate(size, __IME_ALIGN_OF(Type)));
		__IME_ASSERT_MSG(ptr, "Failed to allocate more memory! Possibly, you are out of memory.");
		
		STL::GenericIterator< Type > beginIter(ptr);
		STL::GenericIterator< Type > endIter(ptr + size);

		STL::ForEach(beginIter, endIter, [&](const Type& element) {
			new((void*)&element) Type();
		});

		return ptr;
	}

	template< SizeT StackSize, SizeT Alignment >
	template< typename Type, typename... Args >
	Type& ScopedStack<StackSize, Alignment>::Construct(Args... args) {
		Type* ptr = new(this->Allocate(sizeof(Type), __IME_ALIGN_OF(Type))) Type(args...);
		__IME_ASSERT_MSG(ptr, "Failed to allocate more memory! Possibly, you are out of memory.");
		return *ptr;
	}

	template< SizeT StackSize, SizeT Alignment >
	template< typename Function >
	typename ScopedStack<StackSize, Alignment>::ThisType& ScopedStack<StackSize, Alignment>::operator<<(Function scopedFunction) 
	{
		scopedFunction();
		return *this;
	}

	template< SizeT StackSize, SizeT Alignment >
	void* ScopedStack<StackSize, Alignment>::Allocate(const SizeT size, const SizeT alignment) 
	{
		u8* ptr = this->myStackPointer;
		const uintptr_t basePtr = reinterpret_cast<uintptr_t>(ptr);
		ptr += size + alignment;
		return reinterpret_cast<void*>(basePtr + (alignment - basePtr % alignment));
	}
}