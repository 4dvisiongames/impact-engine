/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Memory stream
#include <Foundation/MemoryStream.h>
// Memory
#include <Foundation/MemoryAllocator.h>
// Mathematic core
#include <Foundation/Math.h>

namespace FileSystem
{
	MemoryStream::MemoryStream() 
		: szCapacity( NULL )
		, szSize( NULL )
		, pBuffer( 0x0 )
	{
		/// Empty
	}

	MemoryStream::~MemoryStream()
	{
		if( this->IsOpen() )
			this->Close();

		if( this->pBuffer != 0x0 ) 
		{
			Memory::Allocator::Free( this->pBuffer );
			this->pBuffer = 0x0;
		}
	}

	bool MemoryStream::CanRead( void ) const
	{
		return true;
	}

	bool MemoryStream::CanWrite( void ) const
	{
		return true;
	}

	bool MemoryStream::CanSeek( void ) const
	{
		return true;
	}

	bool MemoryStream::CanBeMapped( void ) const
	{
		return true;
	}

	void MemoryStream::SetSize( Size s )
	{
		if( s > this->szCapacity )
			this->Realloc( s );

		this->szSize = s;
	}

	Size MemoryStream::GetSize( void ) const
	{
		Size result = this->szSize;
		return result;
	}

	Position MemoryStream::GetPosition( void ) const
	{
		Size result = this->offPosition;
		return result;
	}

	bool MemoryStream::Open( void )
	{
		__IME_ASSERT( !this->IsOpen() );

		/// Nothing to do here, allocation happens in the first Write() call
		/// if necessary, all we do is reset the read/write position to the
		/// beginning of the stream
		if( Stream::Open() ) 
		{
			if( this->mAccessMode == AccessModeEnum::WriteOnly ) 
			{
				this->offPosition = NULL;
				this->szSize = NULL;
			} 
			else if( this->mAccessMode == AccessModeEnum::Append )
			{
				this->offPosition = this->szSize;
			}
			else 
			{
				this->offPosition = NULL;
			}

			return true;
		}

		return false;
	}

	void MemoryStream::Close( void )
	{
		__IME_ASSERT( this->IsOpen() );

		if( this->IsMapped() )
			this->Unmap();

		Stream::Close();
	}

	void MemoryStream::Write( const void* ptr, FileSystem::Size numBytes )
	{
		__IME_ASSERT( numBytes > 0 );
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( (this->mAccessMode == AccessModeEnum::Append) || (this->mAccessMode == AccessModeEnum::WriteOnly) || (this->mAccessMode == AccessModeEnum::ReadWrite) );
		__IME_ASSERT( (this->offPosition >= NULL) && (this->offPosition <= this->szSize) ); //!< Was 2

		/// If it's not enough capacity of stream, then reallocate
		if( !this->HasRoom( numBytes ) )
			this->MakeRoom( numBytes );

		__IME_ASSERT( (this->offPosition + numBytes) <= this->szCapacity ); //!< Was 2
		Memory::LowLevelMemory::CopyMemoryBlock( ptr, this->pBuffer + this->offPosition, numBytes );
		this->offPosition += numBytes;

		if( this->offPosition > this->szSize )
			this->szSize = this->offPosition;
	}

	Size MemoryStream::Read( void* ptr, Size numBytes )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( (this->mAccessMode == AccessModeEnum::ReadWrite) || (this->mAccessMode == AccessModeEnum::ReadOnly) );
		__IME_ASSERT( (this->offPosition >= NULL) && (this->offPosition <= this->szSize) ); //!< Was 2

		Size readBytes = numBytes <= this->szSize - this->offPosition ? numBytes : this->szSize - this->offPosition;
		NDKAssert2( (this->offPosition + readBytes) <= this->szSize );
		if( readBytes > NULL ) 
		{
			Memory::LowLevelMemory::CopyMemoryBlock( this->pBuffer + this->offPosition, ptr, readBytes );
			this->offPosition += readBytes;
		}

		return readBytes;
	}

	void MemoryStream::Seek( Offset offset, SeekOrigin origin )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( (this->offPosition >= NULL) && (this->offPosition <= this->szSize) ); // was 2

		switch( origin ) 
		{
		case SeekOrigin::Begin:
			this->offPosition = offset;
			break;

		case SeekOrigin::Current:
			this->offPosition += offset;
			break;

		case SeekOrigin::End:
			this->offPosition = this->szSize + offset;
			break;
		}

		this->offPosition = Math::TClamp< Position >( this->offPosition, NULL, this->szSize );
	}

	bool MemoryStream::Eof( void ) const
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( (this->offPosition >= NULL) && (this->offPosition <= this->szSize) ); // was 2

		return (this->offPosition == this->szSize);
	}

	void* MemoryStream::Map( void )
	{
		__IME_ASSERT( this->IsOpen() );

		Stream::Map();
		NDKAssert2( this->szSize > NULL );
		return this->pBuffer;
	}

	void MemoryStream::Unmap( void )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( this->IsMapped() );

		Stream::Unmap();
	}

	u8* MemoryStream::GetPointerData( void )
	{
		__IME_ASSERT( this->pBuffer != 0x0 );
		return this->pBuffer;
	}

	void MemoryStream::Realloc( Size sz )
	{
		uintptr_t newBuffer = reinterpret_cast< uintptr_t >( Memory::Allocator::Malloc( sz ) );

#if _DEBUG
		uintptr_t endOfNewBuffer = newBuffer + sz;
#endif
		__IME_ASSERT( newBuffer != 0 );
		Size newSize = sz < this->szSize ? sz : this->szSize;
		if( this->pBuffer != 0x0 ) 
		{
#if _DEBUG
			__IME_ASSERT( (newBuffer + newSize) < endOfNewBuffer );
#endif
			Memory::LowLevelMemory::CopyMemoryBlock( this->pBuffer, reinterpret_cast< void* >( newBuffer ), newSize );
			Memory::Allocator::Free( this->pBuffer );
		}

		this->pBuffer = reinterpret_cast< u8* >( newBuffer );
		this->szSize = newSize;
		this->szCapacity = sz;

		if( this->offPosition > this->szSize )
			this->offPosition = this->szSize;
	}

	bool MemoryStream::HasRoom( Size sz ) const
	{
		bool result = ( (this->offPosition + sz) <= this->szCapacity );
		return result;
	}

	void MemoryStream::MakeRoom( Size sz )
	{
		__IME_ASSERT( sz > NULL );
		__IME_ASSERT( (this->szSize + sz) > this->szCapacity ); 

		Size oneDotFiveCurrentSize = this->szCapacity + (this->szCapacity >> 1);
		Size newCapacity = this->szSize + sz;

		if( oneDotFiveCurrentSize > newCapacity )
			newCapacity = oneDotFiveCurrentSize;

		__IME_ASSERT( newCapacity > this->szCapacity );
		this->Realloc( sz );
	}
}