/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _NanoMemoryStream_h_
#define _NanoMemoryStream_h_

/// Stream
#include <Foundation/Stream.h>

/// Filesystem namespace
namespace FileSystem
{
	/// <summary>
	/// <c>MemoryStream</c> is a stream class which writes to and reads from system RAM. 
    /// Memory streams provide memory mapping for fast direct read/write access.
	/// </summary>
	class MemoryStream : public Stream
	{
		static const Size szInitialSize = 256; //!< Every reallocation adds this or required size of buffer
		Size szCapacity; //!< Capacity of memory stream
		Size szSize; //!< Size of written data
		Position offPosition; //!< Position
		u8* pBuffer; //!< Buffer of written data

		/// <summary cref="MemoryStream::Realloc">
		/// Re-allocate stream buffer.
		/// </summary>
		void Realloc( Size sz );

		/// <summary cref="MemoryStream::HasRoom">
		/// Do we have room for data writing?
		/// </summary>
		/// <returns>True if capacity is good, otherwise false.</returns>
		bool HasRoom( Size sz ) const;

		/// <summary cref="MemoryStream::MakeRoom">
		/// Make free room for data to write.
		/// </summary>
		/// <param name="sz">Memory size to add/re-allocate.</param>
		void MakeRoom( Size sz );

	public:
		/// <summary cref="MemoryStream::MemoryStream">
		/// Constructor.
		/// </summary>
		MemoryStream( void );

		/// <summary cref="MemoryStream::~MemoryStream">
		/// Destructor.
		/// </summary>
		virtual ~MemoryStream( void );

		/// <summary cref="MemoryStream::CanRead">
		/// Check if we can read from stream.
		/// </summary>
		/// <returns>True if the stream supports reading.</returns>
		virtual bool CanRead( void ) const;

		/// <summary cref="MemoryStream::CanWrite">
		/// Check if we can write to the stream.
		/// </summary>
		/// <returns>True if the stream supports writing.</returns>
		virtual bool CanWrite( void ) const;

		/// <summary cref="MemoryStream::CanSeek">
		/// Check if current stream supports seeking.
		/// </summary>
		/// <returns>True if the stream supports seeking.</returns>
		virtual bool CanSeek( void ) const;

		/// <summary cref="MemoryStream::CanBeMapped">
		/// Check if we can map current stream to read/write on CPU.
		/// </summary>
		/// <returns>True if the stream provides direct memory access.</returns>
		virtual bool CanBeMapped( void ) const;

		/// <summary cref="MemoryStream::SetSize">
		/// Set a new size for the stream.
		/// </summary>
		/// <param name="s">Size in bytes.</param>
		virtual void SetSize( Size s );

		/// <summary cref="MemoryStream::GetSize">
		/// Get the size of the stream in bytes.
		/// </summary>
		/// <returns>Size of stream.</returns>
		virtual Size GetSize( void ) const;

		/// <summary cref="MemoryStream::GetPosition">
		/// Get the current position of the read/write cursor.
		/// </summary>
		/// <returns>Position of stream in memory.</param>
		virtual Position GetPosition( void ) const;

		/// <summary cref="MemoryStream::Open">
		/// Open the stream.
		/// </summary>
		virtual bool Open( void );

		/// <summary cref="MemoryStream::Close">
		/// Close the stream.
		/// </summary>
		virtual void Close( void );

		/// <summary cref="MemoryStream::Write">
		/// Directly write to the stream.
		/// </summary>
		/// <param name="ptr">A pointer to the stream.</param>
		/// <param name="numBytes">Size of data.</param>
		virtual void Write( const void* ptr, Size numBytes );

		/// <summary cref="MemoryStream::Read">
		/// Directly read from the stream.
		/// </summary>
		/// <param name="ptr">A pointer to the data where we read from.</param>
		/// <param name="numBytes">Size of data.</param>
		/// <returns>Read data in bytes.</returns>
		virtual Size Read( void* ptr, Size numBytes );

		/// <summary cref="MemoryStream::Seek">
		/// Seek in stream.
		/// </summary>
		/// <param name="offset">Offset in bytes.</param>
		/// <param name="origin">Origin in data.</param>
		virtual void Seek( Offset offset, SeekOrigin origin );

		/// <summary cref="MemoryStream::Eof">
		/// End of reading.
		/// </summary>
		/// <returns>True if end-of-stream reached, otherwise false.</returns>
		virtual bool Eof( void ) const;

		/// <summary cref="MemoryStream::Map">
		/// Map stream to memory.
		/// </summary>
		/// <returns>A pointer to the data in stream.</returns>
		virtual void* Map( void );

		/// <summary cref="MemoryStream::Unmap">
		/// Unmap stream.
		/// </summary>
		virtual void Unmap( void );

		/// <summary cref="MemoryStream::GetPointerData">
		/// Get pointer to the data of memory stream.
		/// </summary>
		/// <returns>Pointer to the data.</returns>
	 	u8* GetPointerData( void );
	};
}

#endif /// _NanoMemoryStream_h_