/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#include <Foundation/Shared.h>

namespace Parallel
{
	class DataBufferDesc
	{
	public:
		static const SizeT MaxNumBuffers = 4;

		/// <summary cref="DataBufferDesc::DataBufferDesc">
		/// </summary>
		DataBufferDesc();

		/// <summary cref="DataBufferDesc::DataBufferDesc">
		/// </summary>
		DataBufferDesc( void* ptr, SizeT bufferSize, SizeT sliceSize );

		/// <summary cref="DataBufferDesc::DataBufferDesc">
		/// </summary>
		DataBufferDesc( void* ptr0, SizeT bufferSize0, SizeT sliceSize0,
						void* ptr1, SizeT bufferSize1, SizeT sliceSize1 );

		/// <summary cref="DataBufferDesc::DataBufferDesc">
		/// </summary>
		DataBufferDesc( void* ptr0, SizeT bufferSize0, SizeT sliceSize0,
						void* ptr1, SizeT bufferSize1, SizeT sliceSize1,
						void* ptr2, SizeT bufferSize2, SizeT sliceSize2 );

		/// <summary cref="DataBufferDesc::DataBufferDesc">
		/// </summary>
		DataBufferDesc( void* ptr0, SizeT bufferSize0, SizeT sliceSize0,
						void* ptr1, SizeT bufferSize1, SizeT sliceSize1,
						void* ptr2, SizeT bufferSize2, SizeT sliceSize2,
						void* ptr3, SizeT bufferSize3, SizeT sliceSize3 );

		/// <summary cref="DataBufferDesc::Update">
		/// </summary>
		void UpdateSet( SizeT index, void* ptr, SizeT bufferSize, SizeT sliceSize );

		/// <summary cref="DataBufferDesc::GetNumBuffers">
		/// Gets actual number of buffers passed into this local descriptor.
		/// </summary>
		/// <returns>Number of occupied regions for data.</returns>
		SizeT GetNumBuffers() const;

		/// <summary cref="DataBufferDesc::GetPointerFrom">
		/// </summary>
		/// <param name="index">Index of buffer from descriptor.</param>
		/// <returns>Pointer to actual data in buffer.</returns>
		void* GetPointerFrom( SizeT index ) const;

		/// <summary cref="DataBufferDesc::GetBufferSizeFrom">
		/// </summary>
		/// <param name="index">Index of buffer from descriptor.</param>
		/// <returns>Size of actual data in buffer.</returns>
		SizeT GetBufferSizeFrom( SizeT index ) const;

		/// <summary cref="DataBufferDesc::GetSliceSizeFrom">
		/// </summary>
		/// <param name="index">Index of buffer from descriptor.</param>
		/// <returns>Unit of work of actual data in buffer.</returns>
		SizeT GetSliceSizeFrom( SizeT index ) const;

	private:
		SizeT myNumBuffers;
		void* myBuffersPointers[MaxNumBuffers];
		SizeT myBuffersSizes[MaxNumBuffers];
		SizeT myBuffersSlices[MaxNumBuffers];
	};

	__IME_INLINED DataBufferDesc::DataBufferDesc()
		: myNumBuffers(0)
	{
		/// [Codepoet]: Compiled will unroll this itself, but some compiler might not
		/// do that, so make unroller with templates
		for ( SizeT index = 0; index < MaxNumBuffers; index++ )
		{
			this->myBuffersPointers[index] = nullptr;
			this->myBuffersSizes[index] = 0;
			this->myBuffersSlices[index] = 0;
		}
	}

	__IME_INLINED DataBufferDesc::DataBufferDesc( void* ptr, SizeT bufferSize, SizeT sliceSize )
		: myNumBuffers(1)
	{
		__IME_ASSERT( ptr != nullptr );
		__IME_ASSERT( bufferSize > 0 );
		__IME_ASSERT( sliceSize > 0 );

		this->myBuffersPointers[0] = ptr;
		this->myBuffersSizes[0] = bufferSize;
		this->myBuffersSlices[0] = sliceSize;

		/// [Codepoet]: Compiled will unroll this itself, but some compiler might not
		/// do that, so make unroller with templates
		for ( SizeT index = 1; index < MaxNumBuffers; index++ )
		{
			this->myBuffersPointers[index] = nullptr;
			this->myBuffersSizes[index] = 0;
			this->myBuffersSlices[index] = 0;
		}
	}

	__IME_INLINED DataBufferDesc::DataBufferDesc( 
		void* ptr0, SizeT bufferSize0, SizeT sliceSize0,
		void* ptr1, SizeT bufferSize1, SizeT sliceSize1 )
		: myNumBuffers(2)
	{
		__IME_ASSERT( ptr0 != nullptr );
		__IME_ASSERT( bufferSize0 > 0 );
		__IME_ASSERT( sliceSize0 > 0 );

		this->myBuffersPointers[0] = ptr0;
		this->myBuffersSizes[0] = bufferSize0;
		this->myBuffersSlices[0] = sliceSize0;

		__IME_ASSERT( ptr1 != nullptr );
		__IME_ASSERT( bufferSize1 > 0 );
		__IME_ASSERT( sliceSize1 > 0 );

		this->myBuffersPointers[1] = ptr1;
		this->myBuffersSizes[1] = bufferSize1;
		this->myBuffersSlices[1] = sliceSize1;

		/// [Codepoet]: Compiled will unroll this itself, but some compiler might not
		/// do that, so make unroller with templates
		for ( SizeT index = 2; index < MaxNumBuffers; index++ )
		{
			this->myBuffersPointers[index] = nullptr;
			this->myBuffersSizes[index] = 0;
			this->myBuffersSlices[index] = 0;
		}
	}

	__IME_INLINED DataBufferDesc::DataBufferDesc( 
		void* ptr0, SizeT bufferSize0, SizeT sliceSize0,
		void* ptr1, SizeT bufferSize1, SizeT sliceSize1,
		void* ptr2, SizeT bufferSize2, SizeT sliceSize2 )
		: myNumBuffers(3)
	{
		__IME_ASSERT( ptr0 != nullptr );
		__IME_ASSERT( bufferSize0 > 0 );
		__IME_ASSERT( sliceSize0 > 0 );

		this->myBuffersPointers[0] = ptr0;
		this->myBuffersSizes[0] = bufferSize0;
		this->myBuffersSlices[0] = sliceSize0;

		__IME_ASSERT( ptr1 != nullptr );
		__IME_ASSERT( bufferSize1 > 0 );
		__IME_ASSERT( sliceSize1 > 0 );

		this->myBuffersPointers[1] = ptr1;
		this->myBuffersSizes[1] = bufferSize1;
		this->myBuffersSlices[1] = sliceSize1;

		__IME_ASSERT( ptr2 != nullptr );
		__IME_ASSERT( bufferSize2 > 0 );
		__IME_ASSERT( sliceSize2 > 0 );

		this->myBuffersPointers[2] = ptr2;
		this->myBuffersSizes[2] = bufferSize2;
		this->myBuffersSlices[2] = sliceSize2;

		this->myBuffersPointers[3] = nullptr;
		this->myBuffersSizes[3] = 0;
		this->myBuffersSlices[3] = 0;
	}

	__IME_INLINED  DataBufferDesc::DataBufferDesc( 
		void* ptr0, SizeT bufferSize0, SizeT sliceSize0,
		void* ptr1, SizeT bufferSize1, SizeT sliceSize1,
		void* ptr2, SizeT bufferSize2, SizeT sliceSize2,
		void* ptr3, SizeT bufferSize3, SizeT sliceSize3 )
		: myNumBuffers(4)
	{
		__IME_ASSERT( ptr0 != nullptr );
		__IME_ASSERT( bufferSize0 > 0 );
		__IME_ASSERT( sliceSize0 > 0 );

		this->myBuffersPointers[0] = ptr0;
		this->myBuffersSizes[0] = bufferSize0;
		this->myBuffersSlices[0] = sliceSize0;

		__IME_ASSERT( ptr1 != nullptr );
		__IME_ASSERT( bufferSize1 > 0 );
		__IME_ASSERT( sliceSize1 > 0 );

		this->myBuffersPointers[1] = ptr1;
		this->myBuffersSizes[1] = bufferSize1;
		this->myBuffersSlices[1] = sliceSize1;

		__IME_ASSERT( ptr2 != nullptr );
		__IME_ASSERT( bufferSize2 > 0 );
		__IME_ASSERT( sliceSize2 > 0 );

		this->myBuffersPointers[2] = ptr2;
		this->myBuffersSizes[2] = bufferSize2;
		this->myBuffersSlices[2] = sliceSize2;

		__IME_ASSERT( ptr3 != nullptr );
		__IME_ASSERT( bufferSize3 > 0 );
		__IME_ASSERT( sliceSize3 > 0 );

		this->myBuffersPointers[3] = ptr3;
		this->myBuffersSizes[3] = bufferSize3;
		this->myBuffersSlices[3] = sliceSize3;
	}

	__IME_INLINED void DataBufferDesc::UpdateSet( SizeT index, void* ptr, SizeT bufferSize, SizeT sliceSize )
	{
		__IME_ASSERT( index < this->myNumBuffers );
		this->myBuffersPointers[index] = ptr;
		this->myBuffersSizes[index] = bufferSize;
		this->myBuffersSlices[index] = sliceSize;
	}

	__IME_INLINED SizeT DataBufferDesc::GetNumBuffers() const
	{
		return this->myNumBuffers;
	}

	__IME_INLINED void* DataBufferDesc::GetPointerFrom( SizeT index ) const
	{
		__IME_ASSERT( index < this->myNumBuffers );
		return this->myBuffersPointers[index];
	}

	__IME_INLINED SizeT DataBufferDesc::GetBufferSizeFrom( SizeT index ) const
	{
		__IME_ASSERT( index < this->myNumBuffers );
		return this->myBuffersSizes[index];
	}

	__IME_INLINED SizeT DataBufferDesc::GetSliceSizeFrom( SizeT index ) const
	{
		__IME_ASSERT( index < this->myNumBuffers );
		return this->myBuffersSlices[index];
	}
}