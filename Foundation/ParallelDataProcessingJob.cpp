/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Foundation/ParallelDataProcessingJob.h>
#include <Foundation/JobDispatcher.h>
#include <Foundation/STLAlgorithm.h>

namespace Parallel
{
	class SliceJob : public Jobs::Job
	{
	public:
		SliceJob( const JobExecutionContext& ctx,
				  const STL::DelegatedFunction< void, JobExecutionContext >& func );

		JobExecutionContext& GetExecutionContext();

	private:
		Jobs::Job* Execute();

		JobExecutionContext myContext;
		STL::DelegatedFunction< void, JobExecutionContext > myFunc;
	};

	__IME_INLINED SliceJob::SliceJob( const JobExecutionContext& ctx,
									  const STL::DelegatedFunction< void, JobExecutionContext >& func )
	{
		this->myFunc = func;
		Memory::LowLevelMemory::CopyMemoryBlock( &ctx, 
												 &this->myContext, 
												 sizeof( JobExecutionContext ) );
	}

	Jobs::Job* SliceJob::Execute()
	{
		// Just execute 
		this->myFunc( STL::Forward<JobExecutionContext>( this->myContext ) );
		return nullptr;
	}


	class PerThreadPartitionerJob : public Jobs::Job
	{
	public:
		PerThreadPartitionerJob( SizeT threadIndex, 
								 SizeT threadOffset, 
								 SizeT numSlices, 
								 DataContext* context, 
								 const STL::DelegatedFunction< void, JobExecutionContext >& func );

		Jobs::Job* Execute();

	private:
		SizeT myThreadIndex;
		SizeT myThreadOffset;
		SizeT myNumSlices;
		DataContext* mySharedContext;
		const STL::DelegatedFunction< void, JobExecutionContext > myFunction;
	};

	PerThreadPartitionerJob::PerThreadPartitionerJob( 
		SizeT threadIndex, SizeT threadOffset, SizeT numSlices, DataContext* context, 
		const STL::DelegatedFunction< void, JobExecutionContext >& func )
		: myThreadIndex( threadIndex )
		, myThreadOffset( threadOffset )
		, myNumSlices( numSlices )
		, mySharedContext( context )
		, myFunction(func)
	{
		/// Empty
	}

	Jobs::Job* PerThreadPartitionerJob::Execute()
	{
		for ( SizeT indexOfSlice = 0; indexOfSlice < this->myNumSlices; indexOfSlice++ )
		{
			JobExecutionContext ctx = { 0 };

			DataUniformDesc& uniforms = this->mySharedContext->myUniforms;
			DataBufferDesc& inputs = this->mySharedContext->myInputs;
			DataBufferDesc& outputs = this->mySharedContext->myOutputs;

			SizeT bufferIndex;
			ctx.myNumUniforms = uniforms.GetNumBuffers();
			for ( bufferIndex = 0; bufferIndex < ctx.myNumUniforms; ++bufferIndex )
			{
				ctx.myUniforms[bufferIndex] = reinterpret_cast<uintptr_t>(uniforms.GetPointerFrom( bufferIndex ));
				ctx.myUniformSizes[bufferIndex] = uniforms.GetBufferSizeFrom( bufferIndex );
			}

			ctx.myNumInputs = inputs.GetNumBuffers();
			for ( bufferIndex = 0; bufferIndex < ctx.myNumInputs; ++bufferIndex )
			{
				void* buffer = inputs.GetPointerFrom( bufferIndex );
				__IME_ASSERT( buffer != nullptr );
				SizeT offset = this->myThreadIndex + inputs.GetSliceSizeFrom( bufferIndex );
				ctx.myInputs[bufferIndex] = reinterpret_cast<uintptr_t>(buffer) + offset;
				ctx.myInputSizes[bufferIndex] = Math::TMin(inputs.GetSliceSizeFrom(bufferIndex), inputs.GetBufferSizeFrom( bufferIndex ) - offset );
			}

			ctx.myNumOutputs = outputs.GetNumBuffers();
			for ( bufferIndex = 0; bufferIndex < ctx.myNumOutputs; ++bufferIndex )
			{
				void* buffer = outputs.GetPointerFrom( bufferIndex );
				__IME_ASSERT( buffer != nullptr );
				SizeT offset = this->myThreadIndex + outputs.GetSliceSizeFrom( bufferIndex );
				ctx.myInputs[bufferIndex] = reinterpret_cast<uintptr_t>(buffer)+offset;
				ctx.myInputSizes[bufferIndex] = Math::TMin( outputs.GetSliceSizeFrom( bufferIndex ), outputs.GetBufferSizeFrom( bufferIndex ) - offset );
			}

			this->AddRef< Threading::MemoryOrderEnum::Sequental >();

			SliceJob* j = new SliceJob( ctx, this->myFunction );
			j->SetParent( this );
			j->Setup();

			Jobs::Dispatcher::Instance()->Spawn( j );
		}

		return nullptr;
	}

	void DataProcessing::MakeParallel( Jobs::Job* parentJob,
									   DataContext* context,
									   const STL::DelegatedFunction< void, JobExecutionContext >& function,
									   SizeT maxParallelism )
	{
		SizeT uowInputSlices = (context->myInputs.GetBufferSizeFrom( 0 ) +
								 (context->myInputs.GetSliceSizeFrom( 0 ) - 1)) /
								 context->myInputs.GetSliceSizeFrom( 0 );

#if defined( DEBUG )
		SizeT uowOutputSlices = (context->myOutputs.GetBufferSizeFrom( 0 ) +
								  (context->myOutputs.GetSliceSizeFrom( 0 ) - 1)) /
								  context->myOutputs.GetSliceSizeFrom( 0 );

		__IME_ASSERT_MSG( uowInputSlices == uowOutputSlices,
						  "Unit Of Work size for input and output slices must be the same!" );
#endif

		SizeT* numWorkUnitSlices = static_cast< SizeT* >(__alloca16( sizeof( SizeT ) * maxParallelism ));
		SizeT remainder = uowInputSlices % maxParallelism;

		// If no parent job, then just create pure sync job and use it
		Jobs::Job* parent = parentJob != nullptr ? parentJob : new Jobs::Job();
		if ( !parent->IsValid() )
			parent->Setup();

		if ( parentJob->GetRefCount< Threading::MemoryOrderEnum::Acquire>() == 0 )
			parentJob->SetRefCount< Threading::MemoryOrderEnum::Release >( 1 );

		for ( SizeT index = 0; index < maxParallelism; ++index )
		{
			numWorkUnitSlices[index] = uowInputSlices / maxParallelism;
			if ( remainder > index )
				numWorkUnitSlices[index] += 1;

			if ( numWorkUnitSlices[index] > 0 )
			{
				PerThreadPartitionerJob* j = new PerThreadPartitionerJob( 
					index, maxParallelism, numWorkUnitSlices[index], context, function );

				parent->AddRef< Threading::MemoryOrderEnum::Sequental >();
				j->SetParent( parent );
				j->Setup();

				Jobs::Dispatcher::Instance()->Spawn( j );
			}
		}

		Jobs::Dispatcher::Instance()->Wait( parent );

		if ( !parentJob )
			delete parentJob;
	}
}