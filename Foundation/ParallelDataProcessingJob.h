/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Thread
#include <Foundation/Thread.h>
/// Delegate
#include <Foundation/STLDelegate.h>
/// Jobs
#include <Foundation/Job.h>
#include <Foundation/ParallelDataContext.h>

namespace Parallel
{
	// Context layout is sequental sorted by sizeof to prevent aliasing
	struct JobExecutionContext
	{
		static const u32 MaxInputs = 4;
		static const u32 MaxUniforms = 2;
		static const u32 MaxOutputs = 4;

		SizeT myNumInputs;
		SizeT myNumUniforms;
		SizeT myNumOutputs;

		// This is set inside data processing job for mimic shared chunk of 
		// memory for group of jobs.
		void* myScratchBuffer;

		SizeT myInputSizes[MaxOutputs];
		SizeT myUniformSizes[MaxOutputs];
		SizeT myOutputSizes[MaxOutputs];

		uintptr_t myInputs[MaxInputs];
		uintptr_t myUniforms[MaxUniforms];
		uintptr_t myOutputs[MaxOutputs];
	};

	class DataProcessing
	{
	public:
		/// <summary cref="DataProcessing::MakeParallel">
		/// </summary>
		static void MakeParallel( Jobs::Job* parentJob,
								  DataContext* context,
								  const STL::DelegatedFunction< void, JobExecutionContext >& function,
								  SizeT maxParallelism = Threading::Thread::GetAvailableConcurrency() );
	};
}