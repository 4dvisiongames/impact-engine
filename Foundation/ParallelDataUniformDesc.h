/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#include <Foundation/Shared.h>

namespace Parallel
{
	class DataUniformDesc
	{
	public:
		static const SizeT MaxNumBuffers = 2;

		/// <summary cref="DataUniformDesc::DataUniformDesc">
		/// </summary>
		DataUniformDesc();

		/// <summary cref="DataUniformDesc::DataUniformDesc">
		/// </summary>
		DataUniformDesc( void* ptr, SizeT bufferSize, SizeT scratchSize );

		/// <summary cref="DataUniformDesc::DataUniformDesc">
		/// </summary>
		DataUniformDesc( void* ptr0, SizeT bufferSize0, void* ptr1, SizeT bufferSize1, SizeT scratchSize );

		/// <summary cref="DataUniformDesc::UpdateSet">
		/// </summary>
		void UpdateSet( SizeT index, void* ptr, SizeT bufferSize );

		/// <summary cref="DataUniformDesc::UpdateScratchSize">
		/// </summary>
		void UpdateScratchSize( SizeT size );

		/// <summary cref="DataUniformDesc::GetNumBuffers">
		/// Gets actual number of buffers passed into this local descriptor.
		/// </summary>
		/// <returns>Number of occupied regions for data.</returns>
		SizeT GetNumBuffers() const;

		/// <summary cref="DataUniformDesc::GetPointerFrom">
		/// </summary>
		/// <param name="index">Index of buffer from descriptor.</param>
		/// <returns>Pointer to actual data in buffer.</returns>
		void* GetPointerFrom( SizeT index ) const;

		/// <summary cref="DataUniformDesc::GetBufferSizeFrom">
		/// </summary>
		/// <param name="index">Index of buffer from descriptor.</param>
		/// <returns>Size of actual data in buffer.</returns>
		SizeT GetBufferSizeFrom( SizeT index ) const;

		/// <summary cref="DataUniformDesc::GetScratchSize">
		/// </summary>
		/// <returns>Size of scratch buffer.</returns>
		SizeT GetScratchSize() const;

	private:
		SizeT myNumBuffers;
		SizeT myScratchSize;
		void* myBuffersPointers[MaxNumBuffers];
		SizeT myBuffersSizes[MaxNumBuffers];
	};

	__IME_INLINED DataUniformDesc::DataUniformDesc()
		: myScratchSize( 0 )
		, myNumBuffers( 0 )
	{
		/// [Codepoet]: Compiled will unroll this itself, but some compiler might not
		/// do that, so make unroller with templates
		for ( SizeT index = 0; index < MaxNumBuffers; index++ )
		{
			this->myBuffersPointers[index] = nullptr;
			this->myBuffersSizes[index] = 0;
		}
	}

	__IME_INLINED DataUniformDesc::DataUniformDesc( void* ptr, SizeT bufferSize, SizeT scratchSize )
		: myNumBuffers( 1 )
		, myScratchSize( scratchSize )
	{
		__IME_ASSERT( ptr != nullptr );
		__IME_ASSERT( bufferSize > 0 );

		this->myBuffersPointers[0] = ptr;
		this->myBuffersSizes[0] = bufferSize;

		this->myBuffersPointers[1] = nullptr;
		this->myBuffersSizes[1] = 0;
	}

	__IME_INLINED DataUniformDesc::DataUniformDesc( 
		void* ptr0, SizeT bufferSize0,
		void* ptr1, SizeT bufferSize1, SizeT scratchSize )
		: myNumBuffers(2)
		, myScratchSize( scratchSize )
	{
		__IME_ASSERT( ptr0 != nullptr );
		__IME_ASSERT( bufferSize0 > 0 );

		this->myBuffersPointers[0] = ptr0;
		this->myBuffersSizes[0] = bufferSize0;

		__IME_ASSERT( ptr1 != nullptr );
		__IME_ASSERT( bufferSize1 > 0 );

		this->myBuffersPointers[1] = ptr1;
		this->myBuffersSizes[1] = bufferSize1;
	}

	__IME_INLINED void DataUniformDesc::UpdateSet( SizeT index, void* ptr, SizeT bufferSize )
	{
		__IME_ASSERT( index < this->myNumBuffers );
		this->myBuffersPointers[index] = ptr;
		this->myBuffersSizes[index] = bufferSize;
	}

	__IME_INLINED void DataUniformDesc::UpdateScratchSize( SizeT bufferSize )
	{
		this->myScratchSize = bufferSize;
	}

	__IME_INLINED SizeT DataUniformDesc::GetNumBuffers() const
	{
		return this->myNumBuffers;
	}

	__IME_INLINED void* DataUniformDesc::GetPointerFrom( SizeT index ) const
	{
		__IME_ASSERT( index < this->myNumBuffers );
		return this->myBuffersPointers[index];
	}

	__IME_INLINED SizeT DataUniformDesc::GetBufferSizeFrom( SizeT index ) const
	{
		__IME_ASSERT( index < this->myNumBuffers );
		return this->myBuffersSizes[index];
	}

	__IME_INLINED SizeT DataUniformDesc::GetScratchSize() const
	{
		return this->myScratchSize;
	}
}