//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: GNU-compatible critical section definition.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _GNUCriticalSection_h_
#define _GNUCriticalSection_h_

/// Engine shared code
#include <NanoShared.h>

/// POSIX threads
namespace PosixThreading
{
    /// POSIX critical section defintion
    class PosixCriticalSection
    {
    private:
        /// POSIX threads critical section definition
        pthread_mutex_t criticalSection;
        /// POSIX mutex attributes
        pthread_mutexattr_t _attr;

        /// Disable any assignment
        NDKDisableAssignment( PosixCriticalSection );

    public:
        /// Constructor
        PosixCriticalSection( void );
        /// Destructor
        ~PosixCriticalSection( void );
        /// Lock critical section
        void Enter( void );
        /// Leave critical section
        void Leave( void );
    }

    NDKInlined PosixCriticalSection::PosixCriticalSection( void )
    {
        /// Initialize mutex attribute seetings
        pthread_mutexattr_init( &this->_attr );
        /// Setup it as resursive
        pthread_mutexattr_settype( &this->_attr, PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP );
        /// Initialize our recursive mutex
        pthread_mutex_init( &this->_criticalSection, &this->_attr );
    }

    NDKInlined PosixCriticalSection::~PosixCriticalSection( void )
    {
        /// Release out recursive mutex
        pthread_mutex_destroy( &this->_criticalSection );
    }

    NDKInlined void PosixCriticalSection::Enter( void )
    {
        /// Enter into out recursive mutex
        pthread_mutex_lock( &this->_criticalSection );
    }

    NDKInlined void PosixCriticalSection::Leave( void )
    {
        /// Leave from out recursive mutex
        pthread_mutex_unlock( &this->_criticalSection );
    }
} /// namespace GNUThread

#endif // _GNUCriticalSection_h_
