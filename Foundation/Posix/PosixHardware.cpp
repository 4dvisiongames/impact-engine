//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: POSIX Linux/MacOSX API defintions for hardware state knowing.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// GNU POSIX hardware
#include <GNU/GNUHardware.h>

namespace GNUCore
{
    GNUHardware::GNUHardware( void )
    {
        /// Write contents to variables
		this->numCpuCores = sysconf( _SC_NPROCESSORS_ONLN );
		this->pageSize = -1;
    }

    int GNUHardware::GetHardwareConcurrency( void )
    {
        return this->numCpuCores;
    }

    int GNUHardware::GetPageSize( void )
    {
        return this->pageSize;
    }
}
