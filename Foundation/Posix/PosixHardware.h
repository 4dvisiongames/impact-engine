//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: POSIX Linux/MacOSX API defintions for hardware state knowing.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _GNUHardware_h_
#define _GNUHardware_h_

/// Engine shared code
#include <NanoShared.h>

namespace PosixCore
{
    class PosixHardware
    {
    public:
        SizeT numCpuCores; //!< Number of CPU cores(including logical from Intel's HT)
		SizeT pageSize; //!< Size of page

    private:
        /// Constructor
        PosixHardware( void );

        /// Get hardware concurrency
		int GetHardwareConcurrency( void ) const;
		/// Get disk page size
		int GetPageSize( void ) const;
		/// Get CPU feature support
		unsigned int GetCPUFeature( void );
		/// If CPU supports SSE2
		bool SupportsSSE2( void );
    };
}

#endif // _GNUHardware_h_
