//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: GNU-compatible interlocked functions definition for NanoEngine.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _GNUInterlocked_h_
#define _GNUInterlocked_h_

/// Shared code
#include <NanoShared.h>

namespace PosixThreading
{
    /// <summary>
	/// <c>PosixInterlocked</c> is Windows-platform interlocked functions.
	/// </summary>
    class PosixInterlocked
    {
    public:
		/// <summary cref="PosixInterlocked::Increment">
        /// Interlocked increment.
		/// </summary>
		/// <returns>
		template< typename T > static T Increment( volatile T& var );

		/// <summary cref="PosixInterlocked::Decrement">
		/// Interlocked decrement.
		/// </summary>
		template< typename T > static T Decrement( volatile T& var );

		/// <summary cref="PosixInterlocked::Add">
		/// Interlocked addition.
		/// </sumamry>
		template< typename T > static T Add( volatile T& var, T add );

		/// <summary cref="PosixInterlocked::Exchange">
		/// Interlocked exchange.
		/// </summary>
		template< typename T > static T Exchange( volatile T* dest, T value );

		/// <summary cref="PosixInterlocked::CompareExchange">
		/// Interlocked compare-exchange.
		/// </summary>
		template< typename T > static T CompareExchange( volatile T* dest, T exchange, T comparand );

		/// <summary cref="PosixInterlocked::Or">
		/// Interlocked "OR" Boolean operation.
		/// </summary>
		static void Or( volatile void* operand, i32 addend );

		/// <summary cref="PosixInterlocked::And">
		/// Interlocked "AND" Boolean operation.
		/// </summary>
		static void And( volatile void* operand, i32 addend );
    };

    /// 8-bit Interlocked functions
    template<> NDKInlined i8 PosixInterlocked::Increment( volatile i8& var )
    {
		i8 result;
		volatile i8 *p = static_cast< volatile i8* >(&var);

		__asm__ __volatile__("lock\nxadd" " %0,%1"
                             : "=q" (result), "=m"(*(volatile i8 *)p)
                             : "0"(1), "m"(*p)
                             : "memory");

		return result;
    }

    template<> NDKInlined i8 PosixInterlocked::Decrement( volatile i8& var )
    {
		i8 result;
		volatile i8 *p = static_cast< volatile i8* >(&var);

		__asm__ __volatile__("lock\nxadd" " %0,%1"
                             : "=q" (result), "=m"(*(volatile i8 *)p)
                             : "0"(-1), "m"(*p)
                             : "memory");

		return result;
    }

    template<> NDKInlined i8 PosixInterlocked::Add( volatile i8& var, i8 value )
    {
		i8 result;
		volatile i8 *p = static_cast< volatile i8* >(&var);

		__asm__ __volatile__("lock\nxadd" " %0,%1"
                             : "=q" (result), "=m"(*(volatile i8 *)p)
                             : "0"(value), "m"(*p)
                             : "memory");

		return result;
    }

    template<> NDKInlined i8 PosixInterlocked::Exchange( volatile i8* var, i8 value )
    {
		i8 result;
		volatile i8 *p = var;

		__asm__ __volatile__("lock\ncmpxchg" " %2,%1"
                             : "=a"(result), "=m"(*(volatile i8 *)ptr)
                             : "0"(value), "m"(*(volatile i8 *)ptr)
                             : "memory");

		return result;
    }

    template<> NDKInlined i8 PosixInterlocked::CompareExchange( volatile i8* var, i8 exchange, i8 comparand)
    {
		i8 result;
		volatile i8 *p = var;

		__asm mov edx, p
		__asm mov cl, exchange
		__asm mov al, comparand
		__asm lock cmpxchg [edx], cl
		__asm mov result, al

		return result;
    }


	/// 16-bit Interlocked functions
    template<> NDKInlined i16 PosixInterlocked::Increment( volatile i16& var )
    {
		i16 result;
		volatile i16 *p = static_cast< volatile i16* >(&var);

		__asm mov edx, p
		__asm mov ax, 1
		__asm lock xadd [edx], ax
		__asm mov result, ax

		return result;
    }

    template<> NDKInlined i16 PosixInterlocked::Decrement( volatile i16& var )
    {
		i16 result;
		volatile i16 *p = static_cast< volatile i16* >(&var);

		__asm mov edx, p
		__asm mov ax, -1
		__asm lock xadd [edx], ax
		__asm mov result, ax

		return result;
    }

    template<> NDKInlined i16 PosixInterlocked::Add( volatile i16& var, i16 value )
    {
		i16 result;
		volatile i16 *p = static_cast< volatile i16* >(&var);

		__asm mov edx, p
		__asm mov ax, value
		__asm lock xadd [edx], ax
		__asm mov result, ax

		return result;
    }

    template<> NDKInlined i16 PosixInterlocked::Exchange( volatile i16* var, i16 value )
    {
		i16 result;
		volatile i16 *p = var;

		__asm mov edx, p
		__asm mov ax, value
		__asm lock xchg [edx], ax
		__asm mov result, ax

		return result;
    }

    template<> NDKInlined i16 PosixInterlocked::CompareExchange( volatile i16* var, i16 exchange, i16 comparand)
    {
		i16 result;
		volatile i16 *p = var;

		__asm mov edx, p
		__asm mov cx, exchange
		__asm mov ax, comparand
		__asm lock cmpxchg [edx], cx
		__asm mov result, ax

		return result;
    }


	/// 32-bit Interlocked functions
    template<> NDKInlined i32 PosixInterlocked::Increment( volatile i32& var )
    {
		i32 result;
		volatile ptrdiff_t *p = static_cast< volatile i32* >(&var);

		__asm mov edx, p
		__asm mov eax, 1
		__asm lock xadd [edx], eax
		__asm mov result, eax

		return result;
    }

    template<> NDKInlined i32 PosixInterlocked::Decrement( volatile i32& var )
    {
		i32 result;
		volatile ptrdiff_t *p = static_cast< volatile i32* >(&var);

		__asm mov edx, p
		__asm mov eax, -1
		__asm lock xadd [edx], eax
		__asm mov result, eax

		return result;
    }

    template<> NDKInlined i32 PosixInterlocked::Add( volatile i32& var, i32 value )
    {
		i32 result;
		volatile i32 *p = static_cast< volatile i32* >(&var);

		__asm mov edx, p
		__asm mov eax, value
		__asm lock xadd [edx], eax
		__asm mov result, eax

		return result;
    }

    template<> NDKInlined i32 PosixInterlocked::Exchange( volatile i32* var, i32 value )
    {
		i32 result;
		volatile i32 *p = var;

		__asm mov edx, p
		__asm mov eax, value
		__asm lock xchg [edx], eax
		__asm mov result, eax

		return result;
    }

    template<> NDKInlined i32 PosixInterlocked::CompareExchange( volatile i32* var, i32 exchange, i32 comparand)
    {
		i32 result;
		volatile i32 *p = var;

		__asm mov edx, p
		__asm mov ecx, exchange
		__asm mov eax, comparand
		__asm lock cmpxchg [edx], ecx
		__asm mov result, eax

		return result;
    }

	NDKInlined void PosixInterlocked::And( volatile void* operand, i32 addend )
	{
		__asm mov eax, addend
		__asm mov edx, [operand]
		__asm lock and [edx], eax
	}

	NDKInlined void PosixInterlocked::Or( volatile void* operand, i32 addend )
	{
		__asm mov eax, addend
		__asm mov edx, [operand]
		__asm lock or [edx], eax
	}
}

#endif /// _GNUInterlocked_h_
