//================= (C) Copyright 2011, NanoEngine Team. All rights reserved. ====================
/// Desc: Low-Level memory routines of POSIX OS.
///
/// Author: Pavel Umnikov
//================================================================================================

#ifndef _GNULowLevelMemory_h_
#define _GNULowLevelMemory_h_

/// Engine shared code
#include <NanoShared.h>

namespace GNUMemory
{
    class GNULowLevelMemory
    {
    public:
        /// Memory copy routines
        template< typename T, typename Y > static void CopyMemoryBlock( const T* src, Y* dest, size_t memSize );
        /// Memory nullfieding rountine
        template< typename T > static void EraseMemoryBlock( T* src );
    };

    template< typename T, typename Y >
    NDKInlined void CopyMemoryBlock( const T* src, Y* dest, size_t memSize )
    {
        /// Copy memory block to next memory block
        NDKAssert( src != 0x0 );
        memcpy( dest, src, memSize );
    }

    template< typename T >
    NDKInlined void EraseMemoryBlock( T* src )
    {
        /// Get size of current type
        size_t size_of_type = sizeof( T );
        /// Reset memory
        memset( src, NULL, size_of_type );
    }
}

#endif // _GNULowLevelMemory_h_
