# The Impact Engine Foundation

Disclaimer: the foundation is work-in-progress and stuff might change.

## Overview

The foundation provides basic functionality which every Impact Engine app and it moduls depends on:

* lifetime management for heap-allocated objects
* custom memory management for better resource tracking
* a central logging facility
* thread-safe primitives library
* a centralized task library for better integration
* container alternatives for parts of the C++ std library

### The Impact Engine Application Model

Foundation is just foundation, it cannot dictate how to do-the-thing. You can implement *application-state-model* class your self. There are several rules for that:

```cpp
class MyApp
{
public:
    /// ...Constructors-destructors-methods...

    void Initialize();
    void Run(); //!< You call this, when you want to start main loop running
    void Shutdown();
    
    /// ...Methods-fields-properies-etc...
};

void MyApp::Initialize()
{
    /* 
        This must be called first, to setup engine. In this example we pass 
        'true' as argument, this mean that we are using console output, so
        the engine will also output into console, not only into log file. 
        You can omit using this parameter, by default it is 'false', so you
        dont need to pass it.
    */
    Core::System::Setup(true);
    
    ///...your initialization conde...
}

void MyApp::Shutdown()
{
    ///...yout deinitialization code...
    
    /*
        Here we deallocate engine's core resources, also we pass exitCode that
        we got from OS when exiting. exitCode is just int, there is no magic by
        using this variable.
    */
    Core::System::Exit(exitCode);
}
```