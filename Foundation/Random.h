//============= (C) Copyright 2013, 4DVision Technologic Team. All rights reserved. =============
/// Desc: Pseudo-random generators.
///
/// Author: Pavel Umnikov
//===============================================================================================

#pragma once

#ifndef __NanoRandom_h__
#define __NanoRandom_h__

/// Shared
#include <Foundation/Shared.h>

namespace Utils
{
	/// A fast random number generator.
	/** Uses linear congruential method. */
	class FastRandom
	{
		static const unsigned int a = 0x9e3779b1; // a big prime number
		unsigned int x;
		unsigned int c;

	public:
		FastRandom( void* ptr ) { this->Initialize( (uintptr_t)ptr ); }
		FastRandom( u32 i ) { this->Initialize( i ); }
		FastRandom( u64 i ) { this->Initialize( i ); }

		template< typename T > void Initialize( T i )
		{
			this->Initialize( i, Traits::IntToType< sizeof( i ) >() );
		}

		void Initialize( u32 i, Traits::IntToType< 4 > )
		{
			// threads use different seeds for unique sequences
			c = (i|1)*0xba5703f5; // c must be odd, shuffle by a prime number
			x = c^(i>>1); // also shuffle x for the first get() invocation
		}

		void Initialize( u64 i, Traits::IntToType< 8 > )
		{
			this->Initialize( u32( (i >> 32) + i ), Traits::IntToType< 4 >() );
		}

		unsigned short Get( unsigned int& seed ) 
		{
			unsigned short r = (unsigned short)( seed >> 16 );
			__IME_ASSERT_MSG( c & 1, "c must be odd for big RNG period" );
			seed = seed * a + c;
			return r;
		}

		unsigned short Get( void ) { return this->Get( this->x ); }
	};
}

#endif /// __NanoRandom_h__