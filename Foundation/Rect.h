/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// <summary>
/// <c>TRectCorner</c> is a rectangle cornet definition.
/// </summary>
template< typename T > class TRectCorner
{
	T tLeft, tRight; //!< Corner definition;

public:
	TRectCorner( void );
	TRectCorner( T left, T right );
	TRectCorner( const TRectCorner< T >& rhs );
};

template< typename T > TRectCorner< T >::TRectCorner( void ) : tLeft( 0 ), tRight( 0 )
{
	/// Empty
}

template< typename T > TRectCorner< T >::TRectCorner( T left, T right ) : tLeft( left ), tRight( right )
{
	/// Empty
}

template< typename T > TRectCorner< T >::TRectCorner( const TRectCorner< T >& rhs ) : tLeft( rhs.tLeft ), tRight( rhs.tRight )
{
	/// Empty
}

/// <summary>
/// <c>TRectangle</c> is an common rectangle definiton
/// </summary>
template< typename T > class TRectangle
{
	TRectCorner< T > tUpperCorner; //!< Upper corner
	TRectCorner< T > tLowerCorner; //!< Lower corner

public:
	/// <summary cref="TRectangle::TRectangle">
	/// Constructor.
	/// </summary>
	TRectangle( void );

	/// <summary cref="TRectangle::TRectangle">
	/// Constructor.
	/// </summary>
	/// <param name="upperLeft">Upper left corner.</param>
	/// <param name="upperRight">Upper right corner.</param>
	/// <param name="lowerLeft">Lower left corner.</param>
	/// <param name="lowerRight">Lower right corner.</param>
	TRectangle( T upperLeft, T upperRight, T lowerLeft, T lowerRigth );

	/// <summary cref="TRectangle::TRectangle">
	/// Constructor.
	/// </summary>
	/// <param name="upper">Upper corners definition.</param>
	/// <param name="lower">Lower corenrs definition.</param>
	TRectangle( TRectCorner< T > upper, TRectCorner< T > lower );

	/// <summary cref="TRectangle::UpperLeft">
	/// Get upper left corner value.
	/// </summary>
	const T& UpperLeft( void ) const;

	/// <summary cref="TRectangle::UpperRight">
	/// Get upper right corner value.
	/// </summary>
	const T& UpperRight( void ) const;

	/// <summary cref="TRectangle::LowerLeft">
	/// Get lower left corner value.
	/// </summary>
	const T& LowerLeft( void ) const;

	/// <summary cref="TRectangle::LowerRight">
	/// Get lower right corner value.
	/// </summary>
	const T& LowerRight( void ) const;

	/// <summary cref="TRectangle::GetWidth">
	/// Get width.
	/// </summary>
	const T GetWidth( void ) const;

	/// <summary cref="Trectangle::GetHeight">
	/// Get heigth.
	/// </summary>
	const T GetHeight( void ) const;
};

template< typename T > TRectangle< T >::TRectangle( void ) : tUpperCorner( 0, 0 ), tLowerCorner( 0, 0 )
{
	/// Empty
}

template< typename T > TRectangle< T >::TRectangle( T upperLeft, T upperRight, T lowerLeft, T lowerRigth ) : tUpperCorner( upperLeft, upperRight ), tLowerCorner( lowerLeft, lowerRight )
{
	/// Empty
}

template< typename T > TRectangle< T >::TRectangle( TRectCorner< T > upper, TRectCorner< T > lower ) : tUpperCorner( upper ), tLowerCorner( lower )
{
	/// Empty
}

template< typename T > const T& TRectangle< T >::UpperLeft( void ) const
{
	TRectCorner< T >& result = this->tUpperCorner;
	return result.tLeft;
}

template< typename T > const T& TRectangle< T >::UpperRight( void ) const
{
	TRectCorner< T >& result = this->tUpperCorner;
	return result.tRight;
}

template< typename T > const T& TRectangle< T >::LowerLeft( void ) const
{
	TRectCorner< T >& result = this->tLowerCorner;
	return result.tLeft;
}

template< typename T > const T& TRectangle< T >::LowerRight( void ) const
{
	TRectCorner< T >& result = this->tLowerCorner;
	return result.tRight;
}

template< typename T > const T TRectangle< T >::GetHeight( void ) const
{
	T& upperLeft = this->tUpperCorner.tLeft;
	T& lowerLeft = this->tLowerCorner.tLeft;
	return lowerLeft - upperLeft;
}

template< typename T > const T TRectangle< T >::GetWidth( void ) const
{
	T& upperLeft = this->tUpperCorner.tLeft;
	T& upperRigth = this->tUpperCorner.tRight;
	return upperRigth - upperLeft;
}

/// Type definitions
typedef TRectangle< unsigned int > TRectangleU;
typedef TRectangle< float > TRectangleF;