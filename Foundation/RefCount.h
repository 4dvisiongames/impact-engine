/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Smart pointer
#include <Foundation/SmartPtr.h>
/// String
#include <Foundation/STLString.h>
/// Interlocked
#include <Foundation/Interlocked.h>
/// Spin wait
#include <Foundation/SpinWait.h>

/// NanoCore namespace
namespace Core
{
	/// <summary>
	/// <c>ReferenceCount</c> is reference counter definition for C++ objects.
	/// </summary>
	class ReferenceCount
	{
		SizeT myReferences; //!< Reference counter for engine object

	protected:
		/// <summary cref="ReferenceCount::~ReferenceCount">
		/// Destructor.
		/// </summary>
		/// <remarks>Every class inherited from this class must have it's 
		virtual ~ReferenceCount( void );

	public:
		/// <summary cref="ReferenceCount::ReferenceCount">
		/// Constructor.
		/// </summary>
		ReferenceCount( void );

		/// <summary cref="ReferenceCount::AddRef">
		/// Increment reference count.
		/// </summary>
		template< Threading::MemoryOrder Policy >
		void AddRef( void );

		/// <summary cref="ReferenceCount::Release">
		/// Release reference.
		/// </summary>
		template< Threading::MemoryOrder Policy >
		SizeT Release( void );

		/// <summary cref="ReferenceCount::GetRefCount">
		/// Get all references.
		/// </summary>
		/// <returns>References count.</returns>
		template< Threading::MemoryOrder Policy >
		SizeT GetRefCount( void ) const;
	};

	__IME_INLINED ReferenceCount::ReferenceCount( void )
	{
		this->myReferences = 0;
	}

	template< Threading::MemoryOrder Policy >
	__IME_INLINED SizeT ReferenceCount::GetRefCount( void ) const
	{
		return Threading::Interlocked::Fetch< Policy >( this->myReferences );
	}

	template< Threading::MemoryOrder Policy >
	__IME_INLINED void ReferenceCount::AddRef( void )
	{
		/// Increment reference value by blocking it
		Threading::Interlocked::FetchAdd< Policy, SizeT, SizeT >( this->myReferences, 1 );
	}

	template< Threading::MemoryOrder Policy >
	__IME_INLINED SizeT ReferenceCount::Release( void )
	{
		/// Decrement reference value and delete from memory if reference equals zero
		/// Since this FetchAdd returns old value, we must check for 1, and not 0
		SizeT result = Threading::Interlocked::FetchAdd< Policy, SizeT, SSizeT >( this->myReferences, -1 );
		if( result == 1 )
			delete this;

		return result;
	}
};