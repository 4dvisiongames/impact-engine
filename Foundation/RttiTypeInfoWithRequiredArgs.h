/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__Rtti_h__
#define __Foundation__Rtti_h__

#include <Foundation/RttiTypeInterface.h>
#include <Foundation/STLStaticString.h>
#include <Foundation/STLAlgorithm.h>

namespace Rtti
{
	/// <summary>
	/// <c>Rtti</c> is a template-based definition of Run-Time Type Information
	/// </summary>
	template< typename BaseClass, typename... RequiredArgs >
	class TypeInfoWithRequiredArgs : public TypeInterface<BaseClass>
	{
		typedef BaseClass* (*CreatorFunction)(RequiredArgs...);
		typedef void( *DestroyFunction )(BaseClass*);
		typedef STL::StaticString< Char8, 30 > ClassString;

		SizeT myClassSize;
		ClassString myClassName;
		CreatorFunction myCreatorFunction;
		DestroyFunction myDestructionFunction;

	public:
		TypeInfoImplementation( SizeT classSize,
			  ClassString::ConstPointer className,
			  CreatorFunction creatorFunction,
			  DestroyFunction destroyFunction );

		/// TODO: add normal destructor for RAII
		~Rtti() { }

		template< typename... Args >
		BaseClass* Invoke( Args... args );

		void Destroy( BaseClass* cls );

		virtual SizeT GetInstanceSize() const;

		virtual Char8* GetInstanceName() const;
	};

	template< typename BaseClass, typename... RequiredArgs >
	__IME_INLINED Rtti< BaseClass, RequiredArgs... >::Rtti(
		SizeT classSize,
		ClassString::ConstPointer className,
		CreatorFunction creatorFunction,
		DestroyFunction destroyFunction )
		: myClassSize( classSize )
		, myClassName( className )
		, myCreatorFunction( creatorFunction )
		, myDestructionFunction( destroyFunction )
	{
		/// Empty
	}

	template< typename BaseClass, typename... RequiredArgs >
	template< typename... Args >
	__IME_INLINED BaseClass* Rtti< BaseClass, RequiredArgs... >::Invoke( Args... args )
	{
		__IME_ASSERT(myCreatorFunction != nullptr);
		return this->myCreatorFunction(args...);
	}

	template< typename BaseClass, typename... RequiredArgs >
	__IME_INLINED void Rtti< BaseClass, RequiredArgs... >::Destroy( BaseClass* cls )
	{
		__IME_ASSERT( this->myDestructionFunction != nullptr );
		this->myDestructionFunction( cls );
	}

	template< typename BaseClass, typename... RequiredArgs >
	__IME_INLINED Rtti<BaseClass, RequiredArgs...>::ClassString::ConstPointer Rtti< BaseClass, RequiredArgs... >::GetClassDefinition() const
	{
		return this->myClassName.AsCharPtr();
	}

	template< typename BaseClass, typename... RequiredArgs >
	__IME_INLINED SizeT Rtti<BaseClass, RequiredArgs...>::GetInstanceSize() const
	{
		return this->myClassSize;
	}
}

#endif /// __Foundation__Rtti_h__