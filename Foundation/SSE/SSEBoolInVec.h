/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once
#include <Foundation/SSE/SSEMathIntrinsics.h>

namespace Math 
{
	__declspec( align( 16 ) )
	class BoolInVec
	{
	public:
		BoolInVec( void );
		BoolInVec( __m128 v );
		BoolInVec( const BoolInVec& rhs );
		explicit BoolInVec( bool val );

		operator bool( void ) const;

	private:
		union intfloat
		{
			int i;
			float f;
		};

		union boolvec
		{
			bool b[4];
			__m128 v;
		};

		__m128 myVec;
	};

	__IME_INLINED BoolInVec::BoolInVec( void )
	{
		static const intfloat i = { 0 };
		this->myVec = _mm_set_ps1( i.f );
	}

	__IME_INLINED BoolInVec::BoolInVec( __m128 v ) : myVec( v )
	{
	}

	__IME_INLINED BoolInVec::BoolInVec( const BoolInVec& rhs ) : myVec( rhs.myVec )
	{
	}

	__IME_INLINED BoolInVec::BoolInVec( bool val )
	{
		intfloat i = { -(int)val };
		this->myVec = _mm_set_ps1( i.f );
	}

	__IME_INLINED BoolInVec::operator bool( void ) const
	{
		boolvec b;
		b.v = this->myVec;
		return b.b[0];
	}
}