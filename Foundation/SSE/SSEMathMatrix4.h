/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once
#include <Foundation/SSE/SSEMathVector.h>
#include <math.h>

namespace Math
{
	__declspec( align( 16 ) )
	class Matrix4
	{
	public:
		Matrix4( void );
		Matrix4( __m128 row1, __m128 row2, __m128 row3, __m128 row4 );
		Matrix4( Vector& row1, Vector& row2, Vector& row3, Vector& row4 );

		float Determinant( void ) const;

		static Matrix4 LookAt( const Vector& eye, const Vector& at, const Vector& up );

	private:
		Intrinsics::fmatrix mx;
	};

	float Matrix4::Determinant( void ) const
	{
		
	}

	Matrix4 Matrix4::LookAt( const Vector& eye, const Vector& at, const Vector& up )
	{
		Vector v1 = up.Normalize();
		Vector v2 = (eye - at).Normalize();
		Vector v3 = v1.CrossProduct( v2 );
		v2 = v2.CrossProduct( v3 );

		return Matrix4( v3, v2, v1, const_cast< Vector& >( eye ) );
	}
}