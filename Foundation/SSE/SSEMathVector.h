/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#include <Foundation/Shared.h>
#include <Foundation/SSE/SSEFloatInVec.h>

namespace Math
{
	__IME_ALIGNED_MSVC( __IME_DEFAULT_VEC_ALIGNMENT )
	class Vector
	{
	public:
		Vector();
		Vector( __m128 rhs );
		Vector( const Vector& rhs );
		Vector(_In_count_c_(4) const float* pFloats);
		Vector( float x, float y, float z, float w = 1.0f );
		Vector( const FloatInVec& x, const FloatInVec& y, const FloatInVec& z, const FloatInVec& w = FloatInVec( 1.0f ) );

		Vector& operator = (const Vector& rhs);
		__declspec(deprecated) Vector& operator = (__m128 rhs);
		// Flip sign
		const Vector operator - (void) const;
		Vector& operator += (const Vector& rhs);
		Vector& operator -= (const Vector& rhs);
		Vector& operator *= (float f);
		Vector& operator *= (const Vector& rhs);
		Vector& operator /= (const Vector& rhs);

		const Vector operator + (const Vector& b) const;
		const Vector operator - (const Vector& b) const;
		const Vector operator * (const Vector& b) const;
		const Vector operator * (float f) const;
		const Vector operator / (const Vector& b) const;

		void SetX( const FloatInVec& scalar );
		void SetX( float scalar );
		void SetY( const FloatInVec& scalar );
		void SetY( float scalar );
		void SetZ( const FloatInVec& scalar );
		void SetZ( float scalar );
		void SetW( const FloatInVec& scalar );
		void SetW( float scalar );

		FloatInVec GetX() const;
		FloatInVec GetY() const;
		FloatInVec GetZ() const;
		FloatInVec GetW() const;

		FloatInVec Length() const;
		FloatInVec LengthSquare() const;
		FloatInVec DotProduct( const Vector& second ) const;

		Vector CrossProduct( const Vector& second ) const;
		//Vector MultByMatrix( const Matrix4& second ) const;

		Vector Absolute() const;

		// Approximated normalization using Newton-Raphson method
		Vector Normalize() const;

		/// Clamps this vector value between other ones
		Vector Clamp( const Vector& min, const Vector& max );

		/// Mixes this value with others
		Vector Mix( const Vector& v1, const Vector& v2 );

		static const Vector LinearInterpolation( const FloatInVec& dt, const Vector& v1, const Vector& v2 );
		static const Vector LinearInterpolation( float dt, const Vector& v1, const Vector& v2 );

		static const Vector SphericalInterpolation( const FloatInVec& dt, const Vector& v1, const Vector& v2 );
		static const Vector SphericalInterpolation( float dt, const Vector& v1, const Vector& v2 );

		static const Vector HermiteInterpolation( const FloatInVec& dt, const Vector& v1, const Vector& t1, const Vector& v2, const Vector& t2 );
		static const Vector HermiteInterpolation( float dt, const Vector& v1, const Vector& t1, const Vector& v2, const Vector& t2 );

		static const Vector CatmullRomInterpolation( const FloatInVec& dt, const Vector& v1, const Vector& v2, const Vector& v3, const Vector& v4 );
		static const Vector CatmullRomInterpolation( float dt, const Vector& v1, const Vector& v2, const Vector& v3, const Vector& v4 );

		static const Vector Forward( const Vector& Normal, const Vector& Incident, const Vector& Reference );
		static const Vector Reflect( const Vector& Normal, const Vector& Incident );

		// Comparison of xyzw components

		static bool EqualsAll( const Vector& a, const Vector& b );
		static bool EqualsAny( const Vector& a, const Vector& b );
		static bool NearEqual( const Vector& a, const Vector& b, const float epsilon );
		static bool NearEqual( const Vector& a, const Vector& b, const FloatInVec& epsilon );
		static bool LessAll( const Vector& a, const Vector& b );
		static bool LessAny( const Vector& a, const Vector& b );

		// Comparison of xyz components

		static bool EqualsAllV3( const Vector& a, const Vector& b );
		static bool EqualsAnyV3( const Vector& a, const Vector& b );
		static bool NearEqualV3( const Vector& a, const Vector& b, const float epsilon );
		static bool NearEqualV3( const Vector& a, const Vector& b, const FloatInVec& epsilon );
		static bool LessAllV3( const Vector& a, const Vector& b );
		static bool LessAnyV3( const Vector& a, const Vector& b );

	private:
		friend class Matrix4;
		__m128 v;
	} __IME_ALIGNED_GCC( __IME_DEFAULT_VEC_ALIGNMENT );

	__IME_INLINED Vector::Vector() 
		: v( _mm_set_ps1( 0.0f ) ) 
	{
		/// Empty
	}

	__IME_INLINED Vector::Vector( __m128 rhs ) 
		: v( rhs ) 
	{
		/// Empty
	}

	__IME_INLINED Vector::Vector( const Vector& rhs ) 
		: v( rhs.v ) 
	{
		/// Empty
	}

	__IME_INLINED Vector::Vector( _In_count_c_(4) const float* pFloats ) 
	{
		__IME_ASSERT_MSG( Math::IsAligned(pFloats, 16), "Input data block must be aligned to 16 bytes!" );
		this->v = _mm_load_ps( pFloats );
	}

	__IME_INLINED Vector::Vector( float x, float y, float z, float w ) 
	{
		this->v = _mm_setr_ps( x, y, z, w );
	}

	__IME_INLINED Vector::Vector( const FloatInVec& x, const FloatInVec& y, const FloatInVec& z, const FloatInVec& w ) 
	{
		__m128 vTemp1 = _mm_unpacklo_ps( y, w );
		__m128 vTemp2 = _mm_unpacklo_ps( x, z );
		__m128 vResult = _mm_unpacklo_ps( vTemp2, vTemp1 );
		this->v = vResult;
	}

	__IME_INLINED Vector& Vector::operator = (const Vector& rhs) 
	{
		__m128 vResult = rhs.v;
		this->v = vResult;
		return *this;
	}

	__IME_INLINED Vector& Vector::operator = (__m128 rhs) 
	{
		this->v = rhs;
		return *this;
	}

	__IME_INLINED const Vector Vector::operator - () const
	{
		// [Codepoet]: this is faster than subtraction method
		__m128 vResult = _mm_xor_ps( this->v, Intrinsics::SSE::g_fNegativeZero );
		return vResult;
	}

	__IME_INLINED Vector& Vector::operator += (const Vector& rhs) 
	{
		__m128 vResult = _mm_add_ps( this->v, rhs.v );
		this->v = vResult;
		return *this;
	}

	__IME_INLINED Vector& Vector::operator -= (const Vector& rhs) 
	{
		__m128 vResult = _mm_sub_ps( this->v, rhs.v );
		this->v = vResult;
		return *this;
	}

	__IME_INLINED Vector& Vector::operator *= (float f) 
	{
		__m128 vSecond = _mm_set_ps1( f );
		__m128 vResult = _mm_mul_ps( this->v, vSecond );
		this->v = vResult;
		return *this;
	}

	__IME_INLINED Vector& Vector::operator *= (const Vector& rhs) 
	{
		__m128 vResult = _mm_mul_ps( this->v, rhs.v );
		this->v = vResult;
		return *this;
	}

	__IME_INLINED Vector& Vector::operator /= (const Vector& rhs) 
	{
		__m128 vResult = _mm_div_ps( this->v, rhs.v );
		this->v = vResult;
		return *this;
	}

	__IME_INLINED void Vector::SetX( const FloatInVec& scalar )
	{
		__m128 vTemp = FloatInVec::Select< GET_FVEC_FROM_X >(scalar);
		this->v = _mm_move_ss( this->v, vTemp );
	}

	__IME_INLINED void Vector::SetX( float scalar )
	{
		__m128 vTemp = _mm_load_ss( &scalar );
		this->v = _mm_move_ss( this->v, vTemp );
	}

	__IME_INLINED void Vector::SetY( const FloatInVec& scalar )
	{
		__m128 vTemp = FloatInVec::Select< GET_FVEC_FROM_Y >( scalar );
		this->v = _mm_blend_ps( this->v, vTemp, 2 );
	}

	__IME_INLINED void Vector::SetY( float scalar )
	{
		__m128 vTemp = _mm_load_ps1( &scalar );
		this->v = _mm_blend_ps( this->v, vTemp, 2 );
	}

	__IME_INLINED void Vector::SetZ( const FloatInVec& scalar )
	{
		__m128 vTemp = FloatInVec::Select< GET_FVEC_FROM_Z >( scalar );
		this->v = _mm_blend_ps( this->v, vTemp, 4 );
	}

	__IME_INLINED void Vector::SetZ( float scalar )
	{
		__m128 vTemp = _mm_load_ps1( &scalar );
		this->v = _mm_blend_ps( this->v, vTemp, 4 );
	}

	__IME_INLINED void Vector::SetW( const FloatInVec& scalar )
	{
		__m128 vTemp = FloatInVec::Select< GET_FVEC_FROM_W >( scalar );
		this->v = _mm_blend_ps( this->v, vTemp, 8 );
	}

	__IME_INLINED void Vector::SetW( float scalar )
	{
		__m128 vTemp = _mm_load_ps1( &scalar );
		this->v = _mm_blend_ps( this->v, vTemp, 8 );
	}

	__IME_INLINED FloatInVec Vector::GetX() const
	{
		return FloatInVec::Select< GET_FVEC_FROM_X >( this->v );
	}

	__IME_INLINED FloatInVec Vector::GetY() const
	{
		return FloatInVec::Select< GET_FVEC_FROM_Y >( this->v );
	}

	__IME_INLINED FloatInVec Vector::GetZ() const
	{
		return FloatInVec::Select< GET_FVEC_FROM_Z >( this->v );
	}

	__IME_INLINED FloatInVec Vector::GetW() const
	{
		return FloatInVec::Select< GET_FVEC_FROM_W >( this->v );
	}

	__IME_INLINED FloatInVec Vector::Length() const 
	{
		// [Codepoet]: Sadly, built-in Dot-Product intrinsic is slow as hell :(
		//__m128 vResult = _mm_dp_ps( this->v, this->v, 0x1f );
		//vResult = _mm_sqrt_ss( vResult );
		//return FloatInVec::Select< GET_FVEC_FROM_X >( vResult );

		// Perform the dot product on x,y and z
		__m128 vLengthSq = _mm_mul_ps( this->v, this->v );
		// vTemp has z and y
		__m128 vTemp = _mm_shuffle_ps( vLengthSq, vLengthSq, _MM_SHUFFLE( 1, 2, 1, 2 ) );
		// x+z, y
		vLengthSq = _mm_add_ss( vLengthSq, vTemp );
		// y,y,y,y
		vTemp = _mm_shuffle_ps( vTemp, vTemp, _MM_SHUFFLE( 1, 1, 1, 1 ) );
		// x+z+y,??,??,??
		vLengthSq = _mm_add_ss( vLengthSq, vTemp );
		// Splat the length squared
		vLengthSq = _mm_shuffle_ps( vLengthSq, vLengthSq, _MM_SHUFFLE( 0, 0, 0, 0 ) );
		// Get the length
		vLengthSq = _mm_sqrt_ps( vLengthSq );
		return vLengthSq;
	}

	__IME_INLINED FloatInVec Vector::LengthSquare() const 
	{
		// Perform the dot product
		__m128 vDot = _mm_mul_ps( this->v, this->v );
		// x=Dot.vector4_f32[1], y=Dot.vector4_f32[2]
		__m128 vTemp = _mm_shuffle_ps( vDot, vDot, _MM_SHUFFLE( 2, 1, 2, 1 ) );
		// Result.vector4_f32[0] = x+y
		vDot = _mm_add_ss( vDot, vTemp );
		// x=Dot.vector4_f32[2]
		vTemp = _mm_shuffle_ps( vTemp, vTemp, _MM_SHUFFLE( 1, 1, 1, 1 ) );
		// Result.vector4_f32[0] = (x+y)+z
		vDot = _mm_add_ss( vDot, vTemp );
		// Splat x
		vDot = _mm_shuffle_ps( vDot, vDot, _MM_SHUFFLE( 0, 0, 0, 0 ) );
		return vDot;
	}

	__IME_INLINED FloatInVec Vector::DotProduct( const Vector& second ) const 
	{
		// [Codepoet]: Sadly, built-in Dot-Product intrinsic is slow as hell :(
		//return _mm_dp_ps( this->v, second.v, 0xFF );

		__m128 vCross = _mm_mul_ps( this->v, second.v );
		__m128 vTemp1 = _mm_shuffle_ps( vCross, vCross, _MM_SHUFFLE( 2, 2, 2, 2 ) );
		__m128 vTemp2 = _mm_shuffle_ps( vCross, vCross, _MM_SHUFFLE( 1, 1, 1, 1 ) );
		vTemp1 = _mm_add_ps( vTemp2, vTemp1 );
		vTemp2 = _mm_shuffle_ps( vCross, vCross, _MM_SHUFFLE( 0, 0, 0, 0 ) );
		vCross = _mm_add_ps( vTemp2, vTemp1 );
		return vCross;
	}

	__IME_INLINED Vector Vector::CrossProduct( const Vector& second ) const 
	{
		__m128 vTemp1 = _mm_shuffle_ps( this->v, this->v, _MM_SHUFFLE( 3, 0, 2, 1 ) );
		__m128 vTemp2 = _mm_shuffle_ps( second.v, second.v, _MM_SHUFFLE( 3, 1, 0, 2 ) );
		__m128 result = _mm_mul_ps( vTemp1, vTemp2 );
		vTemp1 = _mm_shuffle_ps( this->v, this->v, _MM_SHUFFLE( 3, 1, 0, 2 ) );
		vTemp2 = _mm_shuffle_ps( second.v, second.v, _MM_SHUFFLE( 3, 0, 2, 1 ) );
		vTemp1 = _mm_mul_ps( vTemp1, vTemp2 );
		result = _mm_sub_ps( result, vTemp1 );
	}

	__IME_INLINED Vector Vector::Absolute() const 
	{
		__m128 vResult = _mm_setzero_ps();
		vResult = _mm_sub_ps( vResult, this->v );
		vResult = _mm_max_ps( vResult, this->v );
		return vResult;
	}

	__IME_INLINED Vector Vector::Normalize() const 
	{
		__m128 vResult = _mm_mul_ps( this->v, this->v );
		__m128 vTemp1 = _mm_shuffle_ps( vResult, vResult, _MM_SHUFFLE( 2, 2, 2, 2 ) );
		__m128 vTemp2 = _mm_shuffle_ps( vResult, vResult, _MM_SHUFFLE( 1, 1, 1, 1 ) );
		vTemp1 = _mm_add_ps( vTemp2, vTemp1 );
		vTemp2 = _mm_shuffle_ps( vResult, vResult, _MM_SHUFFLE( 0, 0, 0, 0 ) );
		vResult = _mm_add_ps( vTemp2, vTemp1 );
		vResult = _mm_rsqrt_ps( vResult );
		vResult = _mm_mul_ps( this->v, vResult );
		return vResult;
	}

	__IME_INLINED Vector Vector::Clamp( const Vector& min, const Vector& max ) 
	{
		__m128 vTemp = _mm_min_ps( this->v, max.v );
		vTemp = _mm_max_ps( vTemp, min.v );
		return vTemp;
	}

	__IME_INLINED Vector Vector::Mix( const Vector& v1, const Vector& v2 ) 
	{
		__m128 vTemp1 = _mm_sub_ps( Intrinsics::SSE::g_fOne, this->v );
		vTemp1 = _mm_mul_ps( v1.v, vTemp1 );
		__m128 vTemp2 = _mm_mul_ps( v2.v, this->v );
		vTemp2 = _mm_add_ps( vTemp1, vTemp2 );
		return vTemp2;
	}

	__IME_INLINED const Vector Vector::LinearInterpolation( const FloatInVec& dt, const Vector& v1, const Vector& v2 ) 
	{
		return (v1 + ((v2 - v1) * dt));
	}

	__IME_INLINED const Vector Vector::LinearInterpolation( float dt, const Vector& v1, const Vector& v2 )
	{
		return LinearInterpolation( FloatInVec( dt ), v1, v2 );
	}

	/*const Vector Vector::SLerp( const FloatInVec& dt, const Vector& v1, const Vector& v2 )
	{
		__m128 cosAngle = v1.DotProduct( v2 );
		__m128 acos = Intrinsics::ACosf( cosAngle );
	}*/

	/*const Vector Vector::SLerp( float dt, const Vector& v1, const Vector& v2 )
	{
		return SLerp( FloatInVec( dt ), v1, v2 );
	}*/

	__IME_INLINED const Vector Vector::Forward( const Vector& normal, const Vector& incident, const Vector& reference ) 
	{
		Vector vtDotProduct = reference.DotProduct( incident );
		__m128 vTemp1 = _mm_cmplt_ps( vtDotProduct.v, Intrinsics::SSE::g_fZero );
		__m128 vTemp2 = _mm_cmpgt_ps( vtDotProduct.v, Intrinsics::SSE::g_fZero );
		vTemp1 = _mm_and_ps( vTemp1, Intrinsics::SSE::g_fNegativeOne );
		vTemp2 = _mm_and_ps( vTemp2, Intrinsics::SSE::g_fOne );
		vTemp1 = _mm_or_ps( vTemp1, vTemp2 ); //!< Get sign
		vTemp1 = _mm_mul_ps( vTemp1, Intrinsics::SSE::g_fNegativeOne );
		vTemp1 = _mm_mul_ps( normal.v, vTemp1 );
		return vTemp1;
	}

	__IME_INLINED const Vector Vector::Reflect( const Vector& Normal, const Vector& Incident )
	{
		Vector vtDotProduct = Normal.DotProduct( Incident );
		__m128 vTemp1 = _mm_mul_ps( Normal.v, vtDotProduct.v );
		vTemp1 = _mm_mul_ps( vTemp1, Intrinsics::SSE::g_fTwo );
		vTemp1 = _mm_sub_ps( Incident.v, vTemp1 );
		return vTemp1;
	}

	__IME_INLINED const Vector Vector::operator + (const Vector& b) const 
	{
		__m128 vResult;
		vResult = _mm_add_ps( this->v, b.v );
		return vResult;
	}

	__IME_INLINED const Vector Vector::operator - (const Vector& b) const 
	{
		__m128 vResult;
		vResult = _mm_sub_ps( this->v, b.v );
		return vResult;
	}

	__IME_INLINED const Vector Vector::operator * (const Vector& b) const
	{
		__m128 vResult;
		vResult = _mm_mul_ps( this->v, b.v );
		return vResult;
	}

	__IME_INLINED const Vector Vector::operator * (float f) const 
	{
		__m128 vResult;
		__m128 vSecond = _mm_set_ps1( f );
		vResult = _mm_mul_ps( this->v, vSecond );
		return vResult;
	}

	__IME_INLINED const Vector Vector::operator / (const Vector& b) const 
	{
		__m128 vResult;
		vResult = _mm_div_ps( this->v, b.v );
		return vResult;
	}

	__IME_INLINED bool Vector::EqualsAll( const Vector& a, const Vector& b ) 
	{
		__m128 vTemp = _mm_cmpeq_ps( a.v, b.v );
		return _mm_movemask_ps( vTemp ) == 0xF;
	}

	__IME_INLINED bool Vector::EqualsAny( const Vector& a, const Vector& b )
	{
		__m128 vTemp = _mm_cmpeq_ps( a.v, b.v );
		return _mm_movemask_ps( vTemp ) != 0x0;
	}

	__IME_INLINED bool Vector::NearEqual( const Vector& a, const Vector& b, const float epsilon )
	{
		return NearEqual( a, b, FloatInVec( epsilon ) );
	}

	__IME_INLINED bool Vector::NearEqual( const Vector& a, const Vector& b, const FloatInVec& epsilon )
	{
		__m128 vDelta = _mm_sub_ps( a.v, b.v );
		__m128 vTemp = _mm_setzero_ps();
		vTemp = _mm_sub_ps( vTemp, vDelta );
		vTemp = _mm_max_ps( vTemp, vDelta );
		vTemp = _mm_cmple_ps( vTemp, epsilon );
		return _mm_movemask_ps( vTemp ) == 0xF;
	}

	__IME_INLINED bool Vector::LessAll( const Vector& a, const Vector& b )
	{
		__m128 vTemp = _mm_cmpge_ps( a.v, b.v );
		return _mm_movemask_ps( vTemp ) == 0x0;
	}

	__IME_INLINED bool Vector::LessAny( const Vector& a, const Vector& b )
	{
		__m128 vTemp = _mm_cmpge_ps( a.v, b.v );
		return _mm_movemask_ps( vTemp ) != 0xF;
	}

	__IME_INLINED bool Vector::EqualsAllV3( const Vector& a, const Vector& b )
	{
		__m128 vTemp = _mm_cmpeq_ps( a.v, b.v );
		return (_mm_movemask_ps( vTemp ) & 0x7) == 0x7;
	}

	__IME_INLINED bool Vector::EqualsAnyV3( const Vector& a, const Vector& b )
	{
		__m128 vTemp = _mm_cmpeq_ps( a.v, b.v );
		return (_mm_movemask_ps( vTemp ) & 0x7) != 0x0;
	}

	__IME_INLINED bool Vector::NearEqualV3( const Vector& a, const Vector& b, const float epsilon )
	{
		return NearEqualV3( a, b, FloatInVec( epsilon ) );
	}

	__IME_INLINED bool Vector::NearEqualV3( const Vector& a, const Vector& b, const FloatInVec& epsilon )
	{
		__m128 vDelta = _mm_sub_ps( a.v, b.v );
		__m128 vTemp = _mm_setzero_ps();
		vTemp = _mm_sub_ps( vTemp, vDelta );
		vTemp = _mm_max_ps( vTemp, vDelta );
		vTemp = _mm_cmple_ps( vTemp, epsilon );
		return _mm_movemask_ps( vTemp ) == 0x7;
	}

	__IME_INLINED bool Vector::LessAllV3( const Vector& a, const Vector& b )
	{
		__m128 vTemp = _mm_cmpge_ps( a.v, b.v );
		return (_mm_movemask_ps( vTemp ) & 0x7) == 0;
	}

	__IME_INLINED bool Vector::LessAnyV3( const Vector& a, const Vector& b )
	{
		__m128 vTemp = _mm_cmpge_ps( a.v, b.v );
		return (_mm_movemask_ps( vTemp ) & 0x7) != 7;
	}
}