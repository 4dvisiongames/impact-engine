/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _STLAlgorithm_h_
#define _STLAlgorithm_h_

/// TODO: make all functions documented

/// Standard template library
namespace STL
{
	static const SizeT theHashMagic = SelectSizeTConstant< 2654435769U, 11400714819323198485ULL >::value;

	/// Forward declaration
	template< typename HashType, typename InputType >
	__IME_INLINED HashType HashGenerate(const InputType& first, const InputType& second) {
	}

	template< typename Type >
	typename Traits::RemoveReference< Type >::ValueType&& Move( Type&& Argument ) {
		return static_cast<typename Traits::RemoveReference< Type >::ValueType&&>(Argument);
	}

	template< typename IteratorType, typename FunctionType >
	__IME_INLINED FunctionType ForEach( IteratorType beginIter, IteratorType endIter, FunctionType function ) {
		while ( beginIter != endIter ) {
			function( *beginIter );
			++beginIter;
		}

		return Move( function );
	}

	/// <summary cref="STL::FindIf">
	/// Traverses through container using selected iterator. Check for condition proposed in plain function
	/// or lambda function.
	/// </summary>
	template< typename ReturnType, typename IteratorType, typename FunctionType >
	__IME_INLINED ReturnType FindIf( IteratorType beginIter, IteratorType endIter, FunctionType function ) {
		while ( beginIter != endIter ) {
			if ( function( *beginIter ) )
				return *beginIter;
		}
	}

	template< typename Containter, SizeT N, typename SizeType = SSizeT >
	__IME_INLINED SizeType CountOf( const Containter (&)[N] ) {
		return N;
	}

	template< typename Container >
	__IME_INLINED typename Container::SizeType CountOf( const Container& rhs ) {
		return rhs.Size();
	}

	/// <summary cref="STL::Forward">
	/// </summary>
	template< typename Args >
	__IME_INLINED Args&& Forward( typename Traits::RemoveReference< Args >::ValueType& arguments ) {
		return static_cast<Args&&>(arguments);
	}

	/// <summary cref="STL::Forward">
	/// </summary>
	template< typename Args >
	__IME_INLINED Args&& Forward( typename Traits::RemoveReference< Args >::ValueType&& arguments ) throw() {
		static_assert(!Traits::IsLValueReference<Args>::value, 
					   "Bad STL::Forward<> call! arguments must be proper left-value type.");
		return static_cast<Args&&>(arguments);
	}

	/// <summary cref="STL::CompareLess">
	/// Comparator for templates. 
	/// </summary>
	/// <returns>True when first operand less than second operand.</returns>
	/// <remarks>
	/// Use this only if your type has "less" operator definition, otherwise define this type of comparator
	/// yourself.
	/// </remarks>
	template< typename T > __IME_INLINED bool CompareLess(const T& first, const T& second)
	{
		return first < second;
	}

	/// <summary cref="STL::CompareGreater">
	/// Comparator for templates. 
	/// </summary>
	/// <returns>True when first operand greater than second operand.</returns>
	/// <remarks>
	/// Use this only if your type has "greater" operator definition, otherwise define this type of comparator
	/// yourself.
	/// </remarks>
	template< typename T > __IME_INLINED bool CompareGreater(const T& first, const T& second)
	{
		return first > second;
	}

	/// <summary cref="STL::SwapAlgorithm">
	/// Swap constructed objects.
	/// </summary>
	template< typename T > __IME_INLINED void SwapAlgorithm(T& first, T& second)
	{
		T third(first);
		first = second;
		second = third;
	}

	template< typename T >
	struct HashAlgorithm
	{
		__IME_INLINED SizeT operator()( const T rhs ) {
			return static_cast<SizeT>(rhs)* theHashMagic;
		}
	};

	template< typename Ptr >
	struct HashAlgorithm<Ptr*>
	{
		__IME_INLINED SizeT operator()( const Ptr* ptr ) {
			SizeT const uptr = reinterpret_cast<const SizeT>(ptr);
			return (uptr >> 3) ^ uptr;
		}
	};

	/// <summary cref="STL::HeapSortingAlgorithm">
	/// Generic heap sorting algorithm implementation.
	/// </summary>
	/// <remarks>
	/// TODO: it would be better to move to lambda function support for ComparatorFunc instead of function pointer.
	/// </remarks>
	template< typename T > 
	__IME_INLINED void HeapSortingAlgorithm(bool(*ComparatorFunc)(const T& first, const T& second), T* first_iter, T* second_iter)
	{
		// Nested heap sink algorithm
		template< typename Y >
		class HeapSink 
		{
		public:
			__IME_INLINED void operator()(bool(*ComparatorFunc)(const Y& first, const T& second), Y* data, size_t k, size_t n) 
			{
				Y temporary = data[k-1];
				while(k<=n/2) {
					size_t child = 2 * k;
					if(child < n && ComparatorFunc(data[child-1], data[child])) {
						++child;
					}

					if(ComparatorFunc(temporary, data[child-1])) {
						data[k-1] = data[child-1];
						k = child;
					} else {
						break;
					}
				}
				data[k-1] = temporary;
			}
		};

		// Size of array to sort
		size_t n = second_iter - first_iter;

		// Build heap
		for(size_t k = n/2; k != 0; --k) {
			//Internal::HeapSink(ComparatorFunc, first_iter, k, n);
			HeapSink()( ComparatorFunc, first_iter, k, n );
		}

		// Sort heap
		while(n>=1) {
			T temporary = first_iter[0];
			first_iter[0] = first_iter[n-1];
			first_iter[n-1] = temporary;
			n = n-1;
			//Internal::HeapSink(ComparatorFunc, first_iter, 1, n);
			HeapSink()( ComparatorFunc, first_iter, 1, n );
		}
	}
}

#endif /// _STLAlgorithm_h_