/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Shared code
#include <Foundation/Shared.h>

namespace STL
{
	template< typename Type, SizeT Alignment = 16 >
	class DefaultAllocator
	{
	public:
		typedef Type ValueType;
		typedef Type* Pointer;
		typedef Type& Reference;
		typedef SizeT SizeType;

		DefaultAllocator();

		Pointer Allocate( SizeType num );
		void Free( Pointer ptr );

		template< typename... Args >
		Pointer Construct( Pointer allocated, Args... args );

		void Destruct( Pointer ptr );

	private:
		enum { InstanceSize = sizeof(Type) };
		enum { InstanceAlignment = Alignment };
	};

	template< typename Type, SizeT Alignment >
	__IME_INLINED DefaultAllocator<Type, Alignment>::DefaultAllocator() {
		/// Empty
	}

	template< typename Type, SizeT Alignment >
	__IME_INLINED typename DefaultAllocator<Type, Alignment>::Pointer DefaultAllocator<Type, Alignment>::Allocate(SizeType num) {
		return static_cast<Pointer>(Memory::Allocator::Malloc(InstanceSize * num, InstanceAlignment));
	}

	template< typename Type, SizeT Alignment >
	__IME_INLINED void DefaultAllocator<Type, Alignment>::Free(Pointer ptr) {
		Memory::Allocator::Free(reinterpret_cast<void*>(ptr));
	}

	template< typename Type, SizeT Alignment >
	template< typename... Args >
	__IME_INLINED typename DefaultAllocator<Type, Alignment>::Pointer DefaultAllocator<Type, Alignment>::Construct( Pointer allocated, Args... args ) {
		__IME_ASSERT(ptr);
		return new(allocated) ValueType(args...);
	}

	template< typename Type, SizeT Alignment >
	__IME_INLINED void DefaultAllocator<Type, Alignment>::Destruct( Pointer ptr ) {
		__IME_ASSERT(ptr);
		ptr->~ValueType();
	}


	template< typename Type, SizeT Size, SizeT Alignment = 16 >
	class StackAllocator
	{
	public:
		typedef Type ValueType;
		typedef Type* Pointer;
		typedef Type& Reference;
		typedef SizeT SizeType;

		StackAllocator();

		_Ret_maybenull_ Pointer Allocate(SizeType num);
		void Free(_In_ Pointer ptr);

		template< typename... Args >
		Pointer Construct(_In_ Pointer allocated, Args... args);

		void Destruct(_In_ Pointer ptr);

	private:
		enum { StorageSize = Size };
		enum { InstanceAlignment = Alignment };

		u8 myStorage[StorageSize + InstanceAlignment + 1];
	};

	template< typename Type, SizeT Size, SizeT Alignment >
	__IME_INLINED StackAllocator<Type, Size, Alignment>::StackAllocator() {
	}

	template< typename Type, SizeT Size, SizeT Alignment >
	_Ret_maybenull_
	__IME_INLINED typename StackAllocator<Type, Size, Alignment>::Pointer StackAllocator<Type, Size, Alignment>::Allocate(SizeType num) {
	}

	template< typename Type, SizeT Size, SizeT Alignment >
	__IME_INLINED void StackAllocator<Type, Size, Alignment>::Free(_In_ Pointer ptr) {
	}

	template< typename Type, SizeT Size, SizeT Alignment >
	template< typename... Args >
	__IME_INLINED typename StackAllocator<Type, Size, Alignment>::Pointer StackAllocator<Type, Size, Alignment>::Construct(_In_ Pointer allocated, Args... args) {
		__IME_ASSERT(ptr);
		return new(allocated)ValueType(args...);
	}

	template< typename Type, SizeT Size, SizeT Alignment >
	__IME_INLINED void StackAllocator<Type, Size, Alignment>::Destruct(_In_ Pointer ptr) {
		__IME_ASSERT(ptr);
		ptr->~ValueType();
	}


	template< typename Type, SizeT Count, SizeT SizeOf = sizeof(Type), SizeT Alignment = 16 >
	class StackFreeListAllocator
	{
	public:
		typedef Type ValueType;
		typedef Type* Pointer;
		typedef Type& Reference;
		typedef SizeT SizeType;

		StackFreeListAllocator();

		_Ret_maybenull_ Pointer Allocate(SizeType num);
		void Free(_In_ Pointer ptr);

		template< typename... Args >
		Pointer Construct(_In_ Pointer allocated, Args... args);

		void Destruct(_In_ Pointer ptr);

	private:
		struct Node
		{
			Node* myNext;
		};

		enum { StorageSize = Count * SizeOf };
		enum { InstanceSize = SizeOf };
		enum { InstanceAlignment = Alignment };

		void Initialize();
		void Shutdown();
		bool CheckStackSize(SizeType acquisition);

		Node* myFreeList;
		SizeType myNodesCount;

		u8 myStorage[(StorageSize * sizeof(Node)) + InstanceAlignment + 1];
	};

	template< typename Type, SizeT Size, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED StackFreeListAllocator<Type, Size, SizeOf, Alignment>::StackFreeListAllocator()
		: myHead(nullptr)
		, myNodesCount(0)
	{
		this->Initialize();
	}

	template< typename Type, SizeT Size, SizeT SizeOf, SizeT Alignment >
	_Ret_maybenull_
	__IME_INLINED typename StackFreeListAllocator<Type, Size, SizeOf, Alignment>::Pointer StackFreeListAllocator<Type, Size, SizeOf, Alignment>::Allocate(SizeType num) 
	{
		__IME_ASSERT_MSG(num == 1, "You can allocate only one instance of object using free-list allocator!");

		Node* result;

		if ((result = this->myFreeList) != nullptr)
			this->myFreeList = result->myNext;
		else
			return nullptr;

		this->myNodesCount--;
		return reinterpret_cast<Pointer>((reinterpret_cast< u8 >(result)+sizeof(Node)));
	}

	template< typename Type, SizeT Size, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED void StackFreeListAllocator<Type, Size, SizeOf, Alignment>::Free(_In_ Pointer ptr) 
	{
		Node* result = reinterpret_cast< Node* >((reinterpret_cast< u8* >(ptr)-sizeof(Node)));
		result->myNext = this->myFreeList;
		this->myFreeList = result;
		this->myNodesCount++;
	}

	template< typename Type, SizeT Size, SizeT SizeOf, SizeT Alignment >
	template< typename... Args >
	__IME_INLINED typename StackFreeListAllocator<Type, Size, SizeOf, Alignment>::Pointer StackFreeListAllocator<Type, Size, SizeOf, Alignment>::Construct(_In_ Pointer allocated, Args... args) 
	{
		__IME_ASSERT(ptr);
		return new(allocated) ValueType(args...);
	}

	template< typename Type, SizeT Size, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED void StackFreeListAllocator<Type, Size, SizeOf, Alignment>::Destruct(_In_ Pointer ptr) 
	{
		__IME_ASSERT(ptr);
		ptr->~ValueType();
	}

	template< typename Type, SizeT Size, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED void StackFreeListAllocator<Type, Size, SizeOf, Alignment>::Initialize() 
	{
		for (SizeType stackWalk = 0; stackWalk != StorageSize; stackWalk += InstanceSize) 
		{
			Node* result = reinterpret_cast<Node*>(&this->myStorage[stackWalk]);
			result->myNext = this->myFreeList;
			this->myFreeList = result;
			this->myNodesCount++;
		}
	}

	template< typename Type, SizeT Size, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED void StackFreeListAllocator<Type, Size, SizeOf, Alignment>::Shutdown() 
	{
		for (Node* node = this->myFreeList; node != nullptr; node = node->myNext) {
			/// TODO: free all items
		}
	}

	template< typename Type, SizeT Size, SizeT SizeOf, SizeT Alignment >
	__IME_INLINED bool StackFreeListAllocator<Type, Size, SizeOf, Alignment>::CheckStackSize( SizeType acquisition ) 
	{
		return true;
	}
}