/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef __Foundation__STLArray_h_
#define __Foundation__STLArray_h_

/// Engine shared code
#include <Foundation/Shared.h>
/// STL algorithms
#include <Foundation/STLAlgorithm.h>

/// Standard template library
namespace STL
{
	/// Desc: Impact Engine dynamic array class. This class is also used by most other
	///       collection classes.
	///
	/// Details: The default constructor will not pre-allocate elements, so no space
	///			 is wasted as long as no elements are added. As soon as the first element
	///			 is added to the array, an initial buffer of 16 elements is created.
	///			 Whenever the element buffer would overflow, a new buffer of twice
	///			 the size of the previous buffer is created and the existing elements
	///			 are then copied over to the new buffer. The element buffer will
	///			 never shrink, the only way to reclaim unused memory is to
	///			 copy the Array to a new Array object. This is usually not a problem
	///			 since most arrays will oscillate around some specific size, so once
	///			 the array has reached this specific size, no costly memory free or allocs
	///			 will be performed.
	///
	/// Warning: One should generally be careful with costly copy operators, the Array
	///		 	 class (and the other container classes using Array) may do some heavy
	///			 element shuffling in some situations (especially when sorting and erasing
	///			 elements).
	///
	/// Notes: Generaly used as direct replacement for std::vector functionality.
	template<typename Type> 
	class Array
	{
	public:
		typedef Array<Type> ThisType;
		typedef Type ValueType;
		typedef Type* Pointer;
		typedef Type& Reference;
		typedef const Type& ConstReference;
		typedef const Type* ConstPointer;
		typedef Type* Iterator;
		typedef const Type* ConstIterator;
		typedef SizeT SizeType;
		typedef bool (*ComparatorFunc)(const Type& first, const Type& last);
		typedef void (*SortingFunc)(ComparatorFunc comp_fun, Type* first_iter, Type* second_iter);

		/// <summary cref="Array::Array">
		/// Constructor with default parameters.
		/// </summary>
		Array();

		/// <summary cref="Array::Array">
		/// Constuctor with initial size and grow size.
		/// </summary>
		/// <param name="initialCapacity">Initial caapcity of current array.</param>
		/// <param name="initialGrow">Initial grow of current array.</param>
		Array(SizeType initialCapacity, SizeType initialGrow);

		/// <summary cref="Array::Array">
		/// Constructor with initial size, grow size and initial values.
		/// </summary>
		/// <param name="initialSize">Initial capacity of current array.</param>
		/// <param name="initialGrow">Initial grow of current array.</param>
		/// <param name="initialValue">Initial(first) value.</param>
		Array(SizeType initialSize, SizeType initialGrow, ConstReference initialValue);

		/// <summary cref="Array::Array">
		/// Copy constructor.
		/// </summary>
		/// <param name="rhs">A reference to the Array.</param>
		Array(const ThisType& rhs);

		/// <summary cref="Array::~Array">
		/// Destructor.
		/// </summary>
		~Array();

		/// <summary cref="Array::operator=">
		/// Assignment operator.
		/// </summary>
		/// <param name="rhs">A reference to the Array.</param>
		ThisType& operator=(const ThisType& rhs);

		/// <summary cref="Array::operator[]">
		/// Read-only access an element from current Array.
		/// </summary>
		/// <param name="index">Index of element in current Array.</param>
		/// <remarks>
		/// This method will NOT grow the array, and instead do a range check, which may throw an assertion.
		/// </remarks>
		/// <returns>Contents from selected index, exception if index is bigger than elements count.</returns>
		Reference operator[](SizeType index) const;

		/// <summary cref="Array::operator==">
		/// The equality operator returns true if all elements are identical. The
		/// Type class must support the equality operator.
		/// </summary>
		/// <param name="rhs">A reference to the Array.</param>
		/// <remarks>
		/// You must provide 'operator==' definition in class that puts into container
		/// otherwise compiler error.
		/// </remarks>
		/// <returns>True if equals, otherwise false.</returns>
		bool operator==(const ThisType& rhs) const;

		/// <summary cref="Array::operator!=">
		/// The inequality operator returns true if at least one element in the
		/// array is different, or the array sizes are different.
		/// </summary>
		bool operator!=(const ThisType& rhs) const;

		/// <summary cref="Array::As">
		/// Convert to "anything".
		/// </summary>
		/// <returns>Reinterpreted data accordingly to <c>T</c></returns>
		/// <remarks>
		/// Because collection's data might be like as <c>void*</c>, and sometime we need mechanizm to
		/// reinterpretate array's data as something other. The simplest example is <c>void*</c> as source
		/// type of data and <c>Char8*</c> as destination type, using <c>As()</c> we can do such casting
		/// easily.
		template<typename T> 
		T As() const;

		/// <summary cref="Array::Append">
		/// append element to end of array
		/// </summary>
		/// <param name="elm">Element to append into array.</param>
		/// <returns>Current position of the element in array.</returns>
		SizeType Append(ConstReference elm);

		/// <summary cref="Array::AppendArray">
		/// Append the contents of an array to this array.
		/// </summary>
		/// <param name="rhs">An array to be appended.</param>
		void AppendArray(const ThisType& rhs);

		/// <summary cref="Array::Reserve">
		/// This increases the capacity to make room for N elements. If the
		/// number of elements is known before appending the elements, this
		/// method can be used to prevent reallocation. If there is already
		/// enough room for N more elements, nothing will happen.
		/// </summary>
		/// <param name="num">Size of to be reserved for an array.</param>
		/// <remarks>
		/// NOTE: the functionality of this method has been changed as of 26-Apr-10
		/// (see change log in the bottom of code), it will now only change 
		/// the capacity of the array, not its size.
		/// </remarks>
		void Reserve(SizeType num);

		/// <summary cref="Array::Size">
		/// Get number of elements in array.
		/// </summary>
		/// <returns>Size of an array.</returns>
		SizeType Size() const;

		/// <summary cref="Array::Capacity">
		/// Get overall allocated size of array in number of elements.
		/// </summary>
		/// <returns>Capacity size of an array.</returns>
		SizeType Capacity() const;

		/// <summary cref="Array::Front">
		/// Gets the first element of an array.
		/// </summary>
		/// <returns>A reference to first element.</returns>
		Reference Front() const;

		/// <summary cref="Array::Back">
		/// Gets the last element of an array.
		/// </summary>
		/// <returns>A reference to last element.</returns>
		Reference Back() const;

		/// <summary cref="Array::IsEmpty">
		/// Checks if array is empty.
		/// </summary>
		/// <returns>True if array empty, otherwise false.</returns>
		bool IsEmpty() const;

		/// <summary cref="Array::EraseIndex">
		/// Erase element at index, keep sorting intact.
		/// </summary>
		/// <param name="index">Position in array.</param>
		void EraseIndex(SizeType index);

		/// <summary cref="Array::Erase">
		/// Erase element pointed to by iterator, keep sorting intact.
		/// </summary>
		/// <param name="iter">Iterator of position in array.</param>
		/// <returns>An <c>iter</c> which was used as a point in erasing.</returns>
		Iterator Erase(Iterator iter);

		/// <summary cref="Array::EraseIndexSwap">
		/// Erase element at index, fill gap by swapping in last element, destroys sorting!
		/// </summary>
		/// <param name="index">Position in array.</param>
		/// <remarks>
		/// NOTE: this method is fast but destroys the sorting order!
		/// </remarks>
		void EraseIndexSwap(SizeType index);

		/// <summary cref="Array::EraseSwap">
		/// Erase element at iterator, fill gap by swapping in last element, destroys sorting!
		/// </summary>
		/// <param name="iter">Iterator of position in array.</param>
		/// <returns>An <c>iter</c> which was used as a point in erasing.</returns>
		/// <remarks>
		/// NOTE: this method is fast but destroys the sorting order!
		/// </remarks>
		Iterator EraseSwap(Iterator iter);

		/// <summary cref="Array::Insert">
		/// Insert element before element at index
		/// </summary>
		/// <param name="index">Position in array.</param>
		/// <param name="elm">Element contents to be inserted.</param>
		void Insert(SizeType index, ConstReference elm);

		/// <summary cref="Array::InsertSorted">
		/// This inserts the element into a sorted array. 
		/// </summary>
		/// <param name="elm">Element contents to be inserted.</param>
		/// <returns>
		/// The indexat which the element was inserted.
		/// </returns>
		SizeType InsertSorted(ConstReference elm);

		/// <summary cref="Array::InsertAtEndOfIdenticalRange">
		/// This inserts an element at the end of a range of identical elements
		/// starting at a given index.
		/// </summary>
		/// <param name="startIndex">Starting index of an array.</param>
		/// <param name="elm">Element to be inserted.</param>
		/// <returns>
		/// The index at which first unequality condition happened, or in case of equality
		/// returns position of the last element in array.
		/// </returns>
		/// <remarks>
		/// Performance is O(n).
		/// Algorithm is such: iterate through array from <c>startIndex</c>, check if current
		/// iterating element it the same as <c>elm</c>. If not the same, then add into an array,
		/// save index and exit. If all elements in array are the same as <c>elm</c> then just append
		/// into array and return this last index.
		/// </remarks>
		SizeType InsertAtEndOfIdenticalRange(SizeType startIndex, ConstReference elm);

		/// <summary cref="Array::IsSorted">
		/// This tests, whether the array is sorted.
		/// </summary>
		/// <returns>True if sorted, otherwise false.</returns>
		/// <remarks>
		/// This is a slow operation, function complexity is O(n).
		/// </remarks>
		bool IsSorted() const;

		/// <summary cref="Array:Clear">
		/// Clear array (calls destructors).
		/// </summary>
		/// <remarks>
		/// NOTE: The current implementation of this method does not shrink the preallocated space. 
		/// It simply sets the array size to 0.
		/// </remarks>
		void Clear();

		/// <summary cref="Array:Clear">
		/// Reset array (does NOT call destructors).
		/// </summary>
		/// <remarks>
		/// NOTE: This is identical with Clear(), but does NOT call destructors (it just resets the 
		/// <c>size</c> member). USE WITH CARE!
		/// </remarks>
		void Reset();

		/// <summary cref="Array::Begin">
		/// Gets iterator of beginning of an array.
		/// </summary>
		/// <reutrns>
		/// An iterator with pointer to begining of current array.
		/// </returns>
		Iterator Begin() const;

		/// <summary cref="Array::Begin">
		/// Gets iterator of ending of an array.
		/// </summary>
		/// <reutrns>
		/// An iterator with pointer to ending of current array.
		/// </returns>
		Iterator End() const;

		/// <summary cref="Array::Find">
		/// Find element in array.
		/// </summary>
		/// <param name="elm">Element to be a pattern of matching.</param>
		/// <returns>
		/// An iterator, or <c>nullptr</c> if element not found.
		/// </returns>
		Iterator Find(ConstReference elm) const;

		/// <summary cref="Array::FindIndex">
		/// Find element in array. 
		/// </summary>
		/// <param name="elm">Element to be a pattern of matching.</param>
		/// <returns>
		/// An element index, or InvalidIndex if element not found.
		/// </returns>
		SizeType FindIndex(ConstReference elm) const;

		/// <summary cref="Array::Fill">
		/// Fills an array range with the given element value. 
		/// </summary>
		/// <param name="first">Starting index for an array.</param>
		/// <param name="num">Count of indexes to be filled.</param>
		/// <param name="elm">Element to be inserted(possibly multiple times).</param>
		/// <remarks>
		/// Will grow the array if necessary.
		/// </remarks>
		void Fill(SizeType first, SizeType num, ConstReference elm);

		/// <summary cref="Array::Realloc">
		/// Clear contents and preallocate with new attributes.
		/// </summary>
		/// <param name="capacity">New capacity to be applied to an array.</param>
		/// <param name="grow">Count of new "free" elements indexes to be added on exhausting.</param>
		void Realloc(SizeType capacity, SizeType grow);

		/// <summary cref="Array::Realloc">
		/// Returns a new array with all element which are in rhs, but not in this.
		/// </summary>
		/// <param name="rhs">Array to be intersected with current array.</param>
		/// <returns>Number of difference elements.</returns>
		/// <remarks>
		/// Carefull, this method may be very slow with large arrays!
		/// </remarks>
		ThisType Difference(const ThisType& rhs);

		/// <summary cref="Array::Sort">
		/// Sorts the array. 
		/// </summary>
		/// <param name="sorting">Sorting function.</param>
		/// <param name="comparison">Comparison function.</param>
		void Sort(SortingFunc sorting = HeapSortingAlgorithm, ComparatorFunc comparison = CompareLess);

		/// <summary cref="Array::Sort">
		/// Do a binary search, requires a sorted array.
		/// </summary>
		/// <param name="elm">Element to be a pattern of matching.</param>
		/// <returns>Index of found element, or InvalidIndex.</returns>
		SizeType BinarySearchIndex(ConstReference elm) const;

	private:
		void Destroy(Pointer elm);
		void Copy(const ThisType& src);
		void Delete();
		void Grow();
		void GrowTo(SizeType newCapacity);
		void Move(SizeType fromIndex, SizeType toIndex);

		static const SizeType MinGrowSize = 16;
		static const SizeType MaxGrowSize = 65536;

		SizeType grow;	//!< Grow by this number of elements if array exhausted
		SizeType capacity;	//!< Number of elements allocated
		SizeType size;	//!< Number of elements in array
		Pointer elements;	//!< Pointer to element array
	};

	template<typename Type> 
	__IME_INLINED Array<Type>::Array() 
		: grow(8)
		, capacity(0)
		, size(0)
		, elements(nullptr) {
		/// Empty
	}

	template<typename Type> 
	__IME_INLINED Array<Type>::Array(SizeType _capacity, SizeType _grow)
		: grow(_grow)
		, capacity(_capacity)
		, size(0) {
		if (0 == this->grow) {
			this->grow = 16;
		}

		if (this->capacity > 0) {
			this->elements = new Type[ this->capacity ];
		} else {
			this->elements = 0;
		}
	}

	template<typename Type> 
	__IME_INLINED Array<Type>::Array(SizeType initialSize, SizeType _grow, ConstReference initialValue)
		: grow(_grow)
		, capacity(initialSize)
		, size(initialSize) {
		if (0 == this->grow) {
			this->grow = 16;
		}

		if (initialSize > 0) {
			this->elements = new Type[ this->capacity ];

			for (SizeType i = 0; i < initialSize; i++) {
				this->elements[i] = initialValue;
			}
		} else {
			this->elements = nullptr;
		}
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Copy(const ThisType& src) {
		__IME_ASSERT( nullptr == this->elements );

		this->grow = src.grow;
		this->capacity = src.capacity;
		this->size = src.size;
		if (this->capacity > 0) {
			this->elements = new Type[ this->capacity ];
			for (SizeType i = 0; i < this->size; i++) {
				this->elements[i] = src.elements[i];
			}
		}
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Delete(void) {
		this->grow = 0;
		this->capacity = 0;
		this->size = 0;

		if (this->elements) {
			delete[] this->elements;
			this->elements = nullptr;
		}
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Destroy(Pointer elm) {
		elm->~Type();
	}

	template<typename Type> 
	__IME_INLINED Array<Type>::Array(const ThisType& rhs)
		: grow( 0 )
		, capacity( 0 )
		, size( 0 )
		, elements( nullptr ) {
		/// Just do copy of contents. See Array::Copy function for details.
		this->Copy( rhs );
	}

	template<typename Type> 
	__IME_INLINED Array<Type>::~Array() {
		/// Delete everything that array has inside. See Array::Delete for details;
		this->Delete();
	}

	template<typename Type>
	__IME_INLINED void Array<Type>::Realloc(SizeType _capacity, SizeType _grow) {
		/// Codepoet: We are not using C-style reallocation with functions like
		/// Realloc, aligned_realloc, etc, with internal Array Delete fucntion and
		/// new that we already defined here. 
		this->Delete();
		this->grow = _grow;
		this->capacity = _capacity;
		this->size = 0;

		if (this->capacity > 0) {
			this->elements = new Type[this->capacity];
		} else {
			this->elements = nullptr;
		}
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::ThisType& Array<Type>::operator = (const ThisType& rhs) {
		if (this != &rhs) {
			if ( ( this->capacity > 0 ) && ( rhs.size <= this->capacity ) ) {
				/// Source array fits into our capacity, copy in place
				__IME_ASSERT( nullptr != this->elements );

				for (SizeType i = 0; i < rhs.size; i++) {
					this->elements[i] = rhs.elements[i];
				}

				/// Define all array corresponding variables
				this->grow = rhs.grow;
				this->size = rhs.size;
			} else {
				// Source array doesn't fit into our capacity, need to reallocate
				this->Delete();
				this->Copy(rhs);
			}
		}

		return *this;
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::GrowTo( SizeType newCapacity ) {
		__IME_ASSERT(newCapacity > this->capacity);

		Pointer newArray = new Type[ newCapacity ];

		if ( this->elements ) {
			for (SizeType i = 0; i < this->size; i++) {
				newArray[i] = this->elements[i];
			}

			delete[] this->elements;
		}

		this->elements = newArray;
		this->capacity = newCapacity;
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Grow( void ) {
		__IME_ASSERT( this->grow > 0 );

		SizeType growToSize;
		if (NULL == this->capacity) {
			growToSize = this->grow;
		} else {
			// Grow by half of the current capacity, but never more then MaxGrowSize
			SizeType growBy = this->capacity >> 1;

			if (growBy == 0)
				growBy = MinGrowSize;
			else if (growBy > MaxGrowSize)
				growBy = MaxGrowSize;

			/// Setup real grow size
			growToSize = this->capacity + growBy;
		}
		/// Grow to calculated size
		this->GrowTo(growToSize);
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Move(SizeType fromIndex, SizeType toIndex) {
		__IME_ASSERT(this->elements);
		__IME_ASSERT(fromIndex < this->size);

		/// Nothing to move?
		if (fromIndex == toIndex)
			return;

		/// Compute number of elements to move
		SizeType num = this->size - fromIndex;

		/// Check if array needs to grow
		SizeType neededSize = toIndex + num;
		while (neededSize > this->capacity)
			this->Grow();

		if (fromIndex > toIndex) {
			/// This is a backward move
			for (SizeType i = 0; i < num; i++)
				this->elements[toIndex + i] = this->elements[fromIndex + i];
		} else {
			/// This is a forward move
			/// NOTE: this must remain signed for the following loop to work!!!
			for (SizeType i = num - 1; i >= 0; --i)
				this->elements[toIndex + i] = this->elements[fromIndex + i];
		}

		/// Adjust array size
		this->size = toIndex + num;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::SizeType Array<Type>::Append(ConstReference elm) {
		/// Grow allocated space if exhausted
		if (this->size == this->capacity)
			this->Grow();

		__IME_ASSERT(this->elements);

		SizeType currentIdx = this->size;
		this->elements[currentIdx] = elm;
		this->size++;
		return currentIdx;
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::AppendArray(const ThisType& rhs) {
		SizeType num = rhs.Size();
		for (SizeType i = 0; i < num; i++)
			this->Append(rhs[i]);
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Reserve(SizeType num) {
		__IME_ASSERT(num > 0);

		SizeType neededCapacity = this->size + num;
		if (neededCapacity > this->capacity)
			this->GrowTo(neededCapacity);
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::SizeType Array<Type>::Size() const {
		return this->size;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::SizeType Array<Type>::Capacity() const {
		return this->capacity;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::Reference Array<Type>::operator[](SizeType index) const {
		__IME_ASSERT(this->elements && (index < this->size));
		return this->elements[index];
	}

	template<typename Type> 
	__IME_INLINED bool Array<Type>::operator==(const ThisType& rhs) const {
		if (rhs.Size() == this->Size()) {
			for (SizeType i = 0; i < this->Size(); i++) {
				if (!(this->elements[i] == rhs.elements[i])) {
					return false;
				}
			}
			return true;
		} 

		return false;
	}

	template<typename Type> 
	__IME_INLINED bool Array<Type>::operator!=(const ThisType& rhs) const {
		return !(*this == rhs);
	}

	template<typename Type>
	template<typename T>
	__IME_INLINED T Array<Type>::As() const {
		return reinterpret_cast<T*>(this->elements);
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::Reference Array<Type>::Front() const {
		__IME_ASSERT(this->elements && (this->size > 0));
		return this->elements[0];
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::Reference Array<Type>::Back() const {
		__IME_ASSERT(this->elements && (this->size > 0));
		return this->elements[this->size - 1];
	}

	template<typename Type> 
	__IME_INLINED bool Array<Type>::IsEmpty() const {
		return (this->size == 0);
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::EraseIndex(SizeType index) {
		__IME_ASSERT(this->elements && (index < this->size));

		if (index == (this->size - 1)) {
			/// Special case: last element
			this->Destroy(&(this->elements[index]));
			this->size--;
		} else {
			this->Move(index + 1, index);
		}
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::EraseIndexSwap(SizeType index) {
		__IME_ASSERT(this->elements && (index < this->size));

		/// Swap with last element, and destroy last element
		SizeType lastElementIndex = this->size - 1;

		if (index < lastElementIndex) {
			this->elements[index] = this->elements[lastElementIndex];
		}

		this->Destroy(&(this->elements[lastElementIndex]));
		this->size--;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::Iterator Array<Type>::Erase(typename Array<Type>::Iterator iter) {
		__IME_ASSERT(this->elements && (iter >= this->elements) && (iter < (this->elements + this->size)));

		this->EraseIndex(SizeType(iter - this->elements));
		return iter;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::Iterator Array<Type>::EraseSwap(typename Array<Type>::Iterator iter) {
		__IME_ASSERT(this->elements && (iter >= this->elements) && (iter < (this->elements + this->size)));

		this->EraseSwapIndex(SizeType(iter - this->elements));
		return iter;
	}

	template<typename Type>
	__IME_INLINED void Array<Type>::Insert(SizeType index, ConstReference elm) {
		__IME_ASSERT(index <= this->size);

		if (index == this->size) {
			/// Special case: append element to back
			this->Append(elm);
		} else {
			this->Move(index, index + 1);
			this->elements[index] = elm;
		}
	}

	template<typename Type>
	__IME_INLINED void Array<Type>::Clear() {
		for (SizeType i = 0; i < this->size; i++) {
			this->Destroy(&(this->elements[i]));
		}
		this->size = 0;
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Reset() {
		this->size = 0;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::Iterator Array<Type>::Begin() const {
		return this->elements;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::Iterator Array<Type>::End() const {
		return this->elements + this->size;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::Iterator Array<Type>::Find(ConstReference elm) const {
		for (SizeType index = 0; index < this->size; index++) {
			if (this->elements[index] == elm) {
				return &(this->elements[index]);
			}
		}

		return nullptr;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::SizeType Array<Type>::FindIndex(ConstReference elm) const {
		for (SizeType index = 0; index < this->size; index++) {
			if (this->elements[index] == elm) {
				return index;
			}
		}
		return InvalidIndex;
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Fill(SizeType first, SizeType num, ConstReference elm) {
		if ((first + num) > this->size) {
			this->GrowTo(first + num);
		}

		for (SizeType i = first; i < (first + num); i++) {
			this->elements[i] = elm;
		}
	}

	template<typename Type> 
	__IME_INLINED Array<Type> Array<Type>::Difference(const ThisType& rhs) {
		Array<Type> diff;

		for (SizeType i = 0; i < rhs.Size(); i++) {
			if (0 == this->Find(rhs[i])) {
				diff.Append(rhs[i]);
			}
		}

		return diff;
	}

	template<typename Type> 
	__IME_INLINED void Array<Type>::Sort(SortingFunc sorting, ComparatorFunc comparison) {
		sorting(comparison, this->Begin(), this->End());
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::SizeType Array<Type>::BinarySearchIndex(ConstReference elm) const {
		SizeType num = this->Size();
		if (num > 0) {
			SizeType half;
			SizeType lo = 0;
			SizeType hi = num - 1;
			SizeType mid;
			while (lo <= hi) {
				if (0 != (half = num/2)) {
					mid = lo + ((num & 1) ? half : (half - 1));
					if (elm < this->elements[mid]) {
						hi = mid - 1;
						num = num & 1 ? half : half - 1;
					} else if (elm > this->elements[mid]) {
						lo = mid + 1;
						num = half;
					} else {
						return mid;
					}
				} else if (0 != num) {
					if (elm != this->elements[lo]) {
						return InvalidIndex;
					} else {
						return lo;
					}
				} else {
					break;
				}
			}
		}

		return InvalidIndex;
	}

	template<typename Type>
	__IME_INLINED bool Array<Type>::IsSorted() const {
		if (this->size > 1) {
			for (SizeType i = 0; i < this->size - 1; i++) {
				if (this->elements[i] > this->elements[i + 1]) {
					return false;
				}
			}
		}

		return true;
	}

	template<typename Type> 
	__IME_INLINED typename Array<Type>::SizeType Array<Type>::InsertAtEndOfIdenticalRange(SizeType startIndex, ConstReference elm) {
		for (SizeType i = startIndex + 1; i < this->size; i++) {
			if (this->elements[i] != elm) {
				this->Insert(i, elm);
				return i;
			}
		}

		/// Fallthrough: new element needs to be appended to end
		this->Append(elm);
		return (this->Size() - 1);
	}

	template<typename Type>
	__IME_INLINED typename Array<Type>::SizeType Array<Type>::InsertSorted(ConstReference elm) {
		SizeType num = this->Size();
		if (num == 0) {
			/// Array is currently empty
			this->Append(elm);
			return this->Size() - 1;
		} else {
			SizeType half;
			SizeType lo = 0;
			SizeType hi = num - 1;
			SizeType mid;
			while (lo <= hi) {
				if (0 != (half = num/2)) {
					mid = lo + ((num & 1) ? half : (half - 1));
					if (elm < this->elements[mid]) {
						hi = mid - 1;
						num = num & 1 ? half : half - 1;
					} else if (elm > this->elements[mid]) {
						lo = mid + 1;
						num = half;
					} else {
						/// Element already exists at [mid], append the
						/// new element to the end of the range
						return this->InsertAtEndOfIdenticalRange(mid, elm);
					}
				} else if (0 != num) {
					if (elm < this->elements[lo]) {
						this->Insert(lo, elm);
						return lo;
					} else if (elm > this->elements[lo]) {
						this->Insert(lo + 1, elm);
						return lo + 1;
					} else {
						/// Element already exists at [low], append
						/// the new element to the end of the range
						return this->InsertAtEndOfIdenticalRange(lo, elm);
					}
				} else {
					__IME_ASSERT(0 == lo);

					this->Insert(lo, elm);
					return lo;
				}
			}

			if (elm < this->elements[lo]) {
				this->Insert(lo, elm);
				return lo;
			} else if (elm > this->elements[lo]) {
				this->Insert(lo + 1, elm);
				return lo + 1;
			} else {
				__IME_ASSERT(false);
			}
		}

		/// Can't happen, but still here to shut up compiler
		return InvalidIndex;
	}
} /// namespace STL

#endif /// __Foundation__STLArray_h_

/**
	Changes log:

	30-Jan-10	codepoet	method Array<Type>::Move		serious bugfixes!
	07-Nov-11	codepoet	method Array<Type>::Move		bugfix: neededSize >= this->capacity => neededSize > capacity
	26-Apr-11	codepoet	method Array<Type>::Reserve		changes the capacity of the array, not its size.
	14-Mar-15	codepoet	class Array<Type>				code cleaning, added STL type definitions for Array<Type>
	15-Mar-15	codepoet	method Array<Type>::Difference	this method is broken, check test case to see why!
	15-Mar-15	codepoet	method Array<Type>::As<T>		method implemented
	15-Mar-15	codepoet	method Array<Type>::Copy		fixed argument type, because of unable to compile code
*/
