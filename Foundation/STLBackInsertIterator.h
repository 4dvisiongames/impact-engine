/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#ifndef __STLBackInsertIterator_h__
#define __STLBackInsertIterator_h__

/// Shared code
#include <Foundation/STLIteratorDefinition.h>

namespace STL
{
	/// <summary>
	/// <c>BackInsertIterator</c> is a template-based iterator, which acts like iterator, but
	/// when you assign a value to it, it calls AppendBack() on the container with the value.
	/// </summary>
	template< typename Container >
	class BackInsertIterator : public IteratorDefinition< void, void, void, void >
	{
	public:
		typedef Constainer ContainerType;
		typedef typename Container::ConstReference ConstReference;

		explicit BackInsertIterator( const ContainerType& container );
		BackInsertIterator& operator = ( ConstReference value );
		BackInsertIterator& operator * ( void );
		BackInsertIterator& operator ++ ( void );
		BackInsertIterator& operator ++ ( int );

	protected:
		ContainerType& myContainer;
	};

	template< typename Container >
	__IME_INLINED BackInsertIterator< Container >::BackInsertIterator( const ContainerType& container ) : myContainer( container )
	{
		/// Empty
	}

	template< typename Container >
	__IME_INLINED BackInsertIterator< Container >& BackInsertIterator< Container >::operator=( ConstReference value )
	{
		this->myContainer.AppendBack( value );
		return *this;
	}

	template< typename Container >
	__IME_INLINED BackInsertIterator< Container >& BackInsertIterator< Container >::operator*( void ) 
	{
		return *this;
	}

	template< typename Container >
	__IME_INLINED BackInsertIterator< Container >& BackInsertIterator< Container >::operator++( void )
	{
		return *this; //!< This is just because of design
	}

	template< typename Container >
	__IME_INLINED BackInsertIterator< Container >& BackInsertIterator< Container >::operator++( int )
	{
		return *this; //!< This is just because of design
	}
}

#endif /// __STLBackInsertIterator_h__