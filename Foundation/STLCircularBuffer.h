/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _STLCircularBuffer_h_
#define _STLCircularBuffer_h_

/// Engine shared code
#include <Foundation/Shared.h>

/// Standard template library
namespace STL
{
	/// <summary>
	/// <c>STL::CircularBuffer</c> is a "ring" buffer implementation.
	/// See Desc section on top of the STLCircularBuffer.h for details.
	/// </summary>
	template <class TYPE> class CircularBuffer
	{
	public:
		/// default constructor
		CircularBuffer();
		/// constructor with size
		CircularBuffer(SizeT capacity);
		/// copy constructor
		CircularBuffer(const CircularBuffer<TYPE>& rhs);
		/// destructor
		~CircularBuffer();
		/// assignment operator
		void operator=(const CircularBuffer<TYPE>& rhs);
		/// index operator
		TYPE& operator[](SizeT index) const;
    
		/// set capacity (clear previous content)
		void SetCapacity(SizeT newCapacity);
		/// get capacity
		SizeT Capacity() const;
		/// get number of elements in ring buffer
		SizeT Size() const;
		/// add an element to the ring buffer
		void Add(const TYPE& elm);
		/// return true if ring buffer is empty
		bool IsEmpty() const;
		/// reset ring buffer, just reset the head/base indices without calling destructors
		void Reset();
		/// return reference to first element
		TYPE& Front() const;
		/// return reference to last element
		TYPE& Back() const;
		/// return all values as array
		Array<TYPE> AsArray() const;    
		/// get real linear underlying buffer
		TYPE* GetBuffer();
		/// get real linear underlying buffer
		const TYPE* GetBuffer() const;

	private:
		/// allocate element buffer
		void Allocate(SizeT capacity);
		/// delete content
		void Delete();
		/// copy content
		void Copy(const CircularBuffer<TYPE>& src);

		SizeT capacity;
		SizeT size;
		SizeT baseIndex;
		SizeT headIndex;
		TYPE* elements;
	};

	template<class TYPE> void CircularBuffer<TYPE>::Allocate(SizeT c)
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(0 == this->elements);
		__IME_ASSERT(c > 0);
#endif
		this->capacity = c;
		this->size = 0;
		this->baseIndex = 0;
		this->headIndex = 0;
		this->elements = new TYPE[ c ];
	}

	template<class TYPE> void CircularBuffer<TYPE>::Delete()
	{
		this->capacity = 0;
		this->size = 0;
		this->baseIndex = 0;
		this->headIndex = 0;
		if (0 != this->elements)
		{
			delete[] this->elements;
			this->elements = 0;
		}
	}

	template<class TYPE> void CircularBuffer<TYPE>::Copy(const CircularBuffer<TYPE>& rhs)
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(0 == this->elements);
#endif
		this->Allocate(rhs.capacity);
		this->size = rhs.size;
		this->baseIndex = rhs.baseIndex;
		this->headIndex = rhs.headIndex;
		SizeT i;
		for (i = 0; i < rhs.Size(); i++)
		{
			(*this)[i] = rhs[i];
		}
	}

	template<class TYPE> CircularBuffer<TYPE>::CircularBuffer() : capacity(0), size(0), baseIndex(0), headIndex(0), elements(0)
	{
		/// Empty
	}

	template<class TYPE> CircularBuffer<TYPE>::CircularBuffer(SizeT c) : elements(0)
	{
		this->Allocate(c);
	}

	template<class TYPE> CircularBuffer<TYPE>::CircularBuffer(const CircularBuffer<TYPE>& rhs) : elements(0)
	{
		this->Copy(rhs);
	}

	template<class TYPE> CircularBuffer<TYPE>::~CircularBuffer()
	{
		this->Delete();
	}

	template<class TYPE> void CircularBuffer<TYPE>::operator=(const CircularBuffer<TYPE>& rhs)
	{
		this->Delete();
		this->Copy(rhs);
	}

	template<class TYPE> TYPE& CircularBuffer<TYPE>::operator[](SizeT index) const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(this->elements && (index < this->size));
#endif
		SizeT absIndex = index + this->baseIndex;
		if (absIndex >= this->capacity)
		{
			// wrap-around
			absIndex -= this->capacity;
		}
		return this->elements[absIndex];
	}

	template<class TYPE> TYPE& CircularBuffer<TYPE>::Front() const
	{
		return (*this)[0];
	}

	template<class TYPE> TYPE& CircularBuffer<TYPE>::Back() const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(this->size > 0);
#endif
		return (*this)[this->size - 1];
	}

	template<class TYPE> bool CircularBuffer<TYPE>::IsEmpty() const
	{
		return (0 == this->size);
	}

	template<class TYPE> void CircularBuffer<TYPE>::Reset()
	{
		this->size = 0;
		this->baseIndex = 0;
		this->headIndex = 0;
	}

	template<class TYPE> void CircularBuffer<TYPE>::SetCapacity(SizeT newCapacity)
	{
		this->Delete();
		this->Allocate(newCapacity);
	}

	template<class TYPE> SizeT CircularBuffer<TYPE>::Capacity() const
	{
		return this->capacity;
	}

	template<class TYPE> SizeT CircularBuffer<TYPE>::Size() const
	{
		return this->size;
	}

	template<class TYPE> void CircularBuffer<TYPE>::Add(const TYPE& elm)
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(0 != this->elements);
#endif

		// write new element
		this->elements[this->headIndex++] = elm;

		// check if head index should wrap around
		if (this->headIndex >= this->capacity)
		{
			this->headIndex = 0;
		}

		// if we are full, we need to advance the base index
		if (this->size == this->capacity)
		{
			this->baseIndex++;
			if (this->baseIndex >= this->capacity)
			{
				// wraparound base index
				this->baseIndex = 0;
			}
		}
		else
		{
			// not full yet, increment size member
			this->size++;
		}
	}

	template<class TYPE> Array<TYPE> CircularBuffer<TYPE>::AsArray() const
	{
		Array<TYPE> result(this->size, 0);
		SizeT i;
		for (i = 0; i < this->size; i++)
		{
			result.Append((*this)[i]);
		}
		return result;
	}

	template<class TYPE> TYPE* CircularBuffer<TYPE>::GetBuffer()
	{
		__IME_ASSERT(this->elements);
		return this->elements;
	}

	template<class TYPE> const TYPE* CircularBuffer<TYPE>::GetBuffer() const
	{
		__IME_ASSERT(this->elements);
		return this->elements;
	}

} /// namespace STL

#endif /// _STLCircularBuffer_h_