namespace STL
{
	template< typename T >
	struct ContainerTypes
	{
	public:
		typedef T ValueType;
		typedef T* Pointer;
		typedef T& Reference;
		typedef const T* ConstPointer;
		typedef const T& ConstReference;
	};
}