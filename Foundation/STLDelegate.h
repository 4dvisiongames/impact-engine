/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _STLDelegate_h_
#define _STLDelegate_h_

/// Engine shared code
#include <Foundation/Shared.h>

/// Standard template library namespace
namespace STL
{
	template< typename RetType, typename... Args >
	class DelegatedFunction
	{
	public:
		typedef RetType ReturnType;

		template< typename FuncType >
		static DelegatedFunction< RetType, Args... > Bind( FuncType&& function );

		DelegatedFunction( void );
		ReturnType operator()( Args&&... arguments ) const;

	private:
		typedef RetType( *StubbedType )(Args...); //!< Method pointer typedef
		StubbedType myFunction;
	};

	template< typename RetType, typename... Args >
	__IME_INLINED DelegatedFunction< RetType, Args... >::DelegatedFunction( void )
		: myFunction( nullptr ) {
	}

	template< typename RetType, typename... Args >
	template< typename FuncType >
	__IME_INLINED DelegatedFunction< RetType, Args... > DelegatedFunction< RetType, Args... >::Bind( FuncType&& function ) {
		DelegatedFunction< RetType > delegate;
		delegate.myFunction = function;
		return delegate;
	}

	template< typename RetType, typename... Args >
	__IME_INLINED typename DelegatedFunction< RetType, Args... >::ReturnType DelegatedFunction< RetType, Args... >::operator()( Args&&... argument ) const {
		return this->myFunction( argument... );
	}


	template< typename Return, typename Class, typename... Args >
	class DelegatedMethod
	{
	public:
		typedef Return ReturnType;
		typedef Class ClassType;

		template< typename FuncType >
		static DelegatedMethod< ReturnType, ClassType, Args... > Bind( FuncType&& function );

		DelegatedMethod( void );
		ReturnType operator()( ClassType* ptr, Args&&... arguments ) const;

	private:
		typedef ReturnType( ClassType::*StubbedType )(Args...); //!< Method pointer typedef
		StubbedType myFunction;
	};

	template< typename Return, typename Class, typename... Args >
	__IME_INLINED DelegatedMethod< Return, Class, Args... >::DelegatedMethod( void )
		: myFunction( nullptr ) {
	}

	template< typename Return, typename Class, typename... Args >
	template< typename FuncType >
	__IME_INLINED DelegatedMethod< Return, Class, Args... > DelegatedMethod< Return, Class, Args... >::Bind( FuncType&& function ) {
		DelegatedMethod< Return, Class, Args... > delegate;
		delegate.myFunction = function;
		return delegate;
	}

	template< typename Return, typename Class, typename... Args >
	__IME_INLINED typename DelegatedMethod< Return, Class, Args... >::ReturnType DelegatedMethod< Return, Class, Args... >::operator()( ClassType* ptr, Args&&... argument ) const {
		ClassType* classPtr = static_cast<ClassType*>(ptr);
		return (classPtr->*myFunction)(argument...);
	}
}

#endif /// _STLDelegate_h_