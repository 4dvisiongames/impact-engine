/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _STLDictionary_h_
#define _STLDictionary_h_

/// STL pair
#include <Foundation/STLPair.h>

/// Standard template library
namespace STL
{
	/// <summary>
	/// <c>STL::Dictionary</c> is a mapped vector with pair of key and value that
	/// corresponds to key.
	/// </summary>
	template<class KEYTYPE, class VALUETYPE> class Dictionary
	{
	public:
		/// Default constructor
		Dictionary();

		/// Copy constructor
		Dictionary(const Dictionary<KEYTYPE, VALUETYPE>& rhs);

		/// Assignment operator
		void operator=(const Dictionary<KEYTYPE, VALUETYPE>& rhs);

		/// Read/Write [] operator
		VALUETYPE& operator[](const KEYTYPE& key);

		/// Read-only [] operator
		const VALUETYPE& operator[](const KEYTYPE& key) const;

		/// Return number of key/value pairs in the dictionary
		SizeT Size() const;

		/// Clear the dictionary
		void Clear();

		/// Return true if empty
		bool IsEmpty() const;

		/// Reserve space (useful if number of elements is known beforehand)
		void Reserve(SizeT numElements);

		/// Begin a bulk insert (array will be sorted at End)
		void BeginBulkAdd();

		/// Add a key/value pair
		void Add(const STL::Pair<KEYTYPE, VALUETYPE>& kvp);

		/// Add a key and associated value
		void Add(const KEYTYPE& key, const VALUETYPE& value);

		/// End a bulk insert (this will sort the internal array)
		void EndBulkAdd();

		/// Erase a key and its associated value
		void Erase(const KEYTYPE& key);

		/// Erase a key at index
		void EraseAtIndex(SizeT index);

		/// Find index of key/value pair (InvalidIndex if doesn't exist)
		SizeT FindIndex(const KEYTYPE& key) const;

		/// Return true if key exists in the array
		bool Contains(const KEYTYPE& key) const;

		/// Get a key at given index
		const KEYTYPE& KeyAtIndex(SizeT index) const;

		/// Access to value at given index
		VALUETYPE& ValueAtIndex(SizeT index);

		/// Get a value at given index
		const VALUETYPE& ValueAtIndex(SizeT index) const;

		/// Get key/value pair at index
		Pair<KEYTYPE, VALUETYPE>& KeyValuePairAtIndex(SizeT index) const;

		/// Get all keys as an STL::Array
		STL::Array<KEYTYPE> KeysAsArray() const;

		/// Get all keys as an STL::Array
		STL::Array<VALUETYPE> ValuesAsArray() const;

		/// Get all keys as (typically) an array
		template<class RETURNTYPE> RETURNTYPE KeysAs() const;

		/// Get all keys as (typically) an array
		template<class RETURNTYPE> RETURNTYPE ValuesAs() const;

	protected:
		/// make sure the key value pair array is sorted
		void SortIfDirty() const;

		STL::Array< STL::Pair<KEYTYPE, VALUETYPE> > keyValuePairs; //!< Array of key-value pairs
		bool inBulkInsert; //!< Insertion of bulk value
	};

	template<class KEYTYPE, class VALUETYPE> Dictionary<KEYTYPE, VALUETYPE>::Dictionary() : inBulkInsert(false)
	{
		/// Empty
	}

	template<class KEYTYPE, class VALUETYPE> Dictionary<KEYTYPE, VALUETYPE>::Dictionary(const Dictionary<KEYTYPE, VALUETYPE>& rhs) : keyValuePairs(rhs.keyValuePairs), inBulkInsert(false)
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!rhs.inBulkInsert);
#endif
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::operator=(const Dictionary<KEYTYPE, VALUETYPE>& rhs)
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
		__IME_ASSERT(!rhs.inBulkInsert);
#endif
		this->keyValuePairs = rhs.keyValuePairs;
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::Clear()
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		this->keyValuePairs.Clear();
	}

	template<class KEYTYPE, class VALUETYPE> SizeT Dictionary<KEYTYPE, VALUETYPE>::Size() const
	{
		return STL::CountOf(this->keyValuePairs);
	}

	template<class KEYTYPE, class VALUETYPE> bool Dictionary<KEYTYPE, VALUETYPE>::IsEmpty() const
	{
		return (0 == STL::CountOf(this->keyValuePairs));
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::Add(const STL::Pair<KEYTYPE, VALUETYPE>& kvp)
	{
		if (this->inBulkInsert)
		{
			this->keyValuePairs.Append(kvp);
		}
		else
		{
			this->keyValuePairs.InsertSorted(kvp);
		}
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::Reserve(SizeT numElements)
	{
		this->keyValuePairs.Reserve(numElements);
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::BeginBulkAdd()
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		this->inBulkInsert = true;
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::EndBulkAdd()
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(this->inBulkInsert);
#endif
		this->keyValuePairs.Sort();
		this->inBulkInsert = false;
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::Add(const KEYTYPE& key, const VALUETYPE& value)
	{
		Pair<KEYTYPE, VALUETYPE> kvp(key, value);
		if (this->inBulkInsert)
		{
			this->keyValuePairs.Append(kvp);
		}
		else
		{
			this->keyValuePairs.InsertSorted(kvp);
		}
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::Erase(const KEYTYPE& key)
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		SizeT eraseIndex = this->keyValuePairs.BinarySearchIndex(key);
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(InvalidIndex != eraseIndex);
#endif
		this->keyValuePairs.EraseIndex(eraseIndex);
	}

	template<class KEYTYPE, class VALUETYPE> void Dictionary<KEYTYPE, VALUETYPE>::EraseAtIndex(SizeT index)
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		this->keyValuePairs.EraseIndex(index);
	}

	template<class KEYTYPE, class VALUETYPE> SizeT Dictionary<KEYTYPE, VALUETYPE>::FindIndex(const KEYTYPE& key) const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		return this->keyValuePairs.BinarySearchIndex(key);
	}

	template<class KEYTYPE, class VALUETYPE> bool Dictionary<KEYTYPE, VALUETYPE>::Contains(const KEYTYPE& key) const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		return (InvalidIndex != this->keyValuePairs.BinarySearchIndex(key));
	}

	template<class KEYTYPE, class VALUETYPE> const KEYTYPE& Dictionary<KEYTYPE, VALUETYPE>::KeyAtIndex(SizeT index) const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		return this->keyValuePairs[index].GetKey();
	}

	template<class KEYTYPE, class VALUETYPE> VALUETYPE& Dictionary<KEYTYPE, VALUETYPE>::ValueAtIndex(SizeT index)
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		return this->keyValuePairs[index].GetValue();
	}

	template<class KEYTYPE, class VALUETYPE> const VALUETYPE& Dictionary<KEYTYPE, VALUETYPE>::ValueAtIndex(SizeT index) const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		return this->keyValuePairs[index].GetValue();
	}

	template<class KEYTYPE, class VALUETYPE> STL::Pair<KEYTYPE, VALUETYPE>& Dictionary<KEYTYPE, VALUETYPE>::KeyValuePairAtIndex(SizeT index) const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		return this->keyValuePairs[index];
	}

	template<class KEYTYPE, class VALUETYPE> VALUETYPE& Dictionary<KEYTYPE, VALUETYPE>::operator[](const KEYTYPE& key)
	{
		SizeT keyValuePairIndex = this->FindIndex(key);
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(InvalidIndex != keyValuePairIndex);
#endif
		return this->keyValuePairs[keyValuePairIndex].GetValue();
	}

	template<class KEYTYPE, class VALUETYPE> const VALUETYPE& Dictionary<KEYTYPE, VALUETYPE>::operator[](const KEYTYPE& key) const
	{
		SizeT keyValuePairIndex = this->FindIndex(key);
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(InvalidIndex != keyValuePairIndex);
#endif
		return this->keyValuePairs[keyValuePairIndex].GetValue();
	}

	template<class KEYTYPE, class VALUETYPE> template<class RETURNTYPE> RETURNTYPE Dictionary<KEYTYPE, VALUETYPE>::ValuesAs() const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		RETURNTYPE result(this->Size(),this->Size());
		SizeT i;
		for (i = 0; i < this->keyValuePairs.Size(); i++)
		{
			result.Append(this->keyValuePairs[i].GetValue());
		}
		return result;
	}

	template<class KEYTYPE, class VALUETYPE> STL::Array<VALUETYPE> Dictionary<KEYTYPE, VALUETYPE>::ValuesAsArray() const
	{
		return this->ValuesAs<STL::Array<VALUETYPE> >();
	}

	template<class KEYTYPE, class VALUETYPE> template<class RETURNTYPE> RETURNTYPE Dictionary<KEYTYPE, VALUETYPE>::KeysAs() const
	{
#if NDKDebug == NDKDebugEngine
		__IME_ASSERT(!this->inBulkInsert);
#endif
		RETURNTYPE result(this->Size(),this->Size());
		SizeT i;
		for (i = 0; i < this->keyValuePairs.Size(); i++)
		{
			result.Append(this->keyValuePairs[i].GetKey());
		}
		return result;
	}

	template<class KEYTYPE, class VALUETYPE> STL::Array<KEYTYPE> Dictionary<KEYTYPE, VALUETYPE>::KeysAsArray() const
	{
		return this->KeysAs<Array<KEYTYPE> >();
	}
} // namespace STL

#endif /// _STLDictionary_h_
