/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef _STLFastReverseVector_h_
#define _STLFastReverseVector_h_

#include <Foundation/Shared.h>

namespace STL
{
	//! Vector that grows without reallocations, and stores items in the reverse order.
	/** Requires to initialize its first segment with a preallocated memory chunk
		(usually it is static array or an array allocated on the stack).
		The second template parameter specifies maximal number of segments. Each next 
		segment is twice as large as the previous one. **/
	template< typename T, size_t maxSegments = 16 > 
	class FastReverseVector
	{
		T * currentSegement; //! The current (not completely filled) segment
		size_t currentSegmentSize; //! Capacity of m_cur_segment
		size_t insertPos; //! Insertion position in m_cur_segment
		T * segments[maxSegments]; //! Array of segments (has fixed size specified by the second template parameter)
		size_t numSegments; //! Number of segments (the size of m_segments)
		size_t szSize; //! Number of items in the segments in m_segments

	public:
		FastReverseVector( T * initialSegment, size_t segmentSize ) :
			currentSegement( initialSegment ), currentSegmentSize( segmentSize ), insertPos( segmentSize ),
			numSegments( 0 ), szSize( 0 )
		{
			__IME_ASSERT_MSG( initialSegment && segmentSize, "Nonempty initial segment must be supplied" );
		}

		~FastReverseVector( void )
		{
			for( size_t idx = 1; idx < this->numSegments; idx++ ) {
				Memory::Allocator::Free( this->segments[idx] );
			}
		}

		size_t size( void ) const { return this->szSize + this->currentSegmentSize - this->insertPos; }

		void push_back( const T& val )
		{
			if ( !this->insertPos ) {
				if ( !this->numSegments ) this->segments[this->numSegments++] = this->currentSegement;
				this->szSize += this->currentSegmentSize;
				this->currentSegmentSize *= 2;
				this->insertPos = this->currentSegmentSize;
				this->segments[this->numSegments++] = this->currentSegement = static_cast< T* >( Memory::Allocator::Malloc( this->currentSegmentSize * sizeof( T ) ) );
				__IME_ASSERT_MSG( this->numSegments < maxSegments, "Maximal capacity exceeded" );
			}
			this->currentSegement[--this->insertPos] = val;
		}

		void copy_memory( T* dst )
		{
			size_t sz = this->currentSegmentSize - this->insertPos;
			//memcpy( dst, this->currentSegement + this->insertPos, sz * sizeof(T) );
			Memory::LowLevelMemory::CopyMemoryBlock( this->currentSegement + this->insertPos, dst, sz * sizeof(T) );
			dst += sz;
			sz = this->currentSegmentSize / 2;
			for ( long i = (long)this->numSegments - 2; i >= 0; --i ) {
				//memcpy( dst, this->segments[i], sz * sizeof(T) );
				Memory::LowLevelMemory::CopyMemoryBlock( this->segments[i], dst, sz * sizeof(T) );
				dst += sz;
				sz /= 2;
			}
		}
	};
}

#endif /// _FastReverseVector_h_