/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _STLFixedArray_h_
#define _STLFixedArray_h_

/// Engine shared code
#include <Foundation/Shared.h>
/// Fixed array
#include <Foundation/STLArray.h>

/// Standard template library namespace
namespace STL
{
	template<class TYPE> class FixedArray
	{
	public:
		typedef SizeT SizeType;
		/// define element iterator
		typedef TYPE* Iterator;

		/// default constructor
		FixedArray();
		/// constructor with size
		FixedArray(SizeT s);
		/// constructor with size and initial value
		FixedArray(SizeT s, const TYPE& initialValue);
		/// copy constructor
		FixedArray(const FixedArray<TYPE>& rhs);
		/// destructor
		~FixedArray();
		/// assignment operator
		void operator=(const FixedArray<TYPE>& rhs);
		/// write [] operator
		TYPE& operator[](SizeT index) const;
		/// equality operator
		bool operator==(const FixedArray<TYPE>& rhs) const;
		/// inequality operator
		bool operator!=(const FixedArray<TYPE>& rhs) const;

		/// set number of elements (clears existing content)
		void SetSize(SizeT s);
		/// get number of elements
		SizeT Size() const;
		/// resize array without deleting existing content
		void Resize(SizeT newSize);
		/// return true if array if empty (has no elements)
		bool IsEmpty() const;
		/// clear the array, free elements
		void Clear();
		/// fill the entire array with a value
		void Fill(const TYPE& val);
		/// fill array range with element
		void Fill(SizeT first, SizeT num, const TYPE& val);
		/// get iterator to first element
		Iterator Begin() const;
		/// get iterator past last element
		Iterator End() const;
		/// find identical element in unsorted array (slow)
		Iterator Find(const TYPE& val) const;
		/// find index of identical element in unsorted array (slow)
		SizeT FindIndex(const TYPE& val) const;
		/// sort the array
		void Sort();
		/// do a binary search, requires a sorted array
		SizeT BinarySearchIndex(const TYPE& val) const;
		/// return content as Array (slow!)
		Array<TYPE> AsArray() const;

	private:
		/// delete content
		void Delete();
		/// allocate array for given size
		void Alloc(SizeT s);
		/// copy content
		void Copy(const FixedArray<TYPE>& src);

		SizeT size;
		TYPE* elements;
	};

	template<class TYPE> 
	FixedArray<TYPE>::FixedArray( void ) 
		: size(0)
		, elements(0) {
		/// Empty
	}

	template<class TYPE> 
	void FixedArray<TYPE>::Delete( void ) {
		if (this->elements) {
			delete[] this->elements;
			this->elements = 0;
		}
		this->size = 0;
	}

	template<class TYPE> 
	void FixedArray<TYPE>::Alloc( SizeT s ) {
		__IME_ASSERT(0 == this->elements);

		if (s > 0) this->elements = new TYPE[ s ];
		this->size = s;
	}

	/// NOTE: only works on deleted array. This is intended.
	template<class TYPE> 
	void FixedArray<TYPE>::Copy( const FixedArray<TYPE>& rhs ) {
		if (this != &rhs) {
			/// Allocate new copy
			this->Alloc(rhs.size);
			/// Copy to this buffer
			for (SizeT i = 0; i < this->size; i++) {
				this->elements[i] = rhs.elements[i];
			}
		}
	}

	template<class TYPE> 
	FixedArray<TYPE>::FixedArray( SizeT s )
		: size(0)
		, elements(0) {
		this->Alloc(s);
	}

	template<class TYPE> 
	FixedArray<TYPE>::FixedArray( SizeT s, const TYPE& initialValue )
		: size(0)
		, elements(0) {
		this->Alloc(s);
		this->Fill(initialValue);
	}

	template<class TYPE> 
	FixedArray<TYPE>::FixedArray( const FixedArray<TYPE>& rhs ) 
		: size(0)
		, elements(0) {
		this->Copy(rhs);
	}

	template<class TYPE> 
	FixedArray<TYPE>::~FixedArray() {
		this->Delete();
	}

	template<class TYPE> 
	void FixedArray<TYPE>::operator = ( const FixedArray<TYPE>& rhs ) {
		this->Delete();
		this->Copy(rhs);
	}

	template<class TYPE> 
	TYPE& FixedArray<TYPE>::operator[](SizeT index) const {
		__IME_ASSERT(this->elements && (index < this->size));

		return this->elements[index];
	}

	template<class TYPE> 
	bool FixedArray<TYPE>::operator==(const FixedArray<TYPE>& rhs) const {
		if (this->size != rhs.size)	{
			return false;
		} else {
			__IME_ASSERT(this->elements && rhs.elements);

			SizeT num = this->size;
			for ( SizeT i = 0; i < num; i++)
			{
				if (this->elements[i] != rhs.elements[i]) return false;
			}
			return true;
		}
	}

	template<class TYPE> 
	bool FixedArray<TYPE>::operator != ( const FixedArray<TYPE>& rhs ) const {
		return !(*this == rhs);
	}

	template<class TYPE> 
	void FixedArray<TYPE>::SetSize( SizeT s ) {
		this->Delete();
		this->Alloc(s);
	}

	template<class TYPE> 
	void FixedArray<TYPE>::Resize( SizeT newSize ) {
		// allocate new array and copy over old elements
		TYPE* newElements = 0;
		if (newSize > 0) {
			newElements = new TYPE[ newSize ];
			SizeT numCopy = this->size;
			if (numCopy > newSize) numCopy = newSize;
			for ( SizeT i = 0; i < numCopy; i++) newElements[i] = this->elements[i];
		}

		// delete old elements
		this->Delete();

		// set content to new elements
		this->elements = newElements;
		this->size = newSize;
	}

	template<class TYPE> 
	SizeT FixedArray<TYPE>::Size() const {
		return this->size;
	}

	template<class TYPE> 
	bool FixedArray<TYPE>::IsEmpty() const {
		return 0 == this->size;
	}

	template<class TYPE> 
	void FixedArray<TYPE>::Clear() {
		this->Delete();
	}

	template<class TYPE> 
	void FixedArray<TYPE>::Fill(const TYPE& val) {
		for ( SizeT i = 0; i < this->size; i++ ) this->elements[i] = val;
	}       

	template<class TYPE> 
	void FixedArray<TYPE>::Fill(SizeT first, SizeT num, const TYPE& val)
	{
		__IME_ASSERT((first + num) < this->size);
		__IME_ASSERT(0 != this->elements);

		for ( SizeT i = first; i < (first + num); i++ ) 
			this->elements[i] = val;
	}

	template<class TYPE> 
	typename FixedArray<TYPE>::Iterator FixedArray<TYPE>::Begin() const {
		return this->elements;
	}

	template<class TYPE> 
	typename FixedArray<TYPE>::Iterator FixedArray<TYPE>::End() const {
		return this->elements + this->size;
	}

	template<class TYPE> 
	typename FixedArray<TYPE>::Iterator FixedArray<TYPE>::Find(const TYPE& elm) const {
		SizeT i;
		for (i = 0; i < this->size; i++) {
			if (elm == this->elements[i]) return &(this->elements[i]);
		}
		return 0;
	}

	template<class TYPE> 
	SizeT FixedArray<TYPE>::FindIndex(const TYPE& elm) const {
		for (SizeT i = 0; i < this->size; i++) {
			if (elm == this->elements[i]) return i;
		}

		return InvalidIndex;
	}

	template<class TYPE> 
	void FixedArray<TYPE>::Sort( void ) {
		std::sort(this->Begin(), this->End());
	}

	/// TODO: Hmm, this is copy-pasted from Array...
	template<class TYPE> 
	SizeT FixedArray<TYPE>::BinarySearchIndex(const TYPE& elm) const {
		SizeT num = this->Size();
		if (num > 0)
		{
			SizeT half;
			SizeT lo = 0;
			SizeT hi = num - 1;
			SizeT mid;
			while (lo <= hi) 
			{
				if (0 != (half = num/2)) 
				{
					mid = lo + ((num & 1) ? half : (half - 1));
					if (elm < this->elements[mid])
					{
						hi = mid - 1;
						num = num & 1 ? half : half - 1;
					} 
					else if (elm > this->elements[mid]) 
					{
						lo = mid + 1;
						num = half;
					} 
					else
					{
						return mid;
					}
				} 
				else if (num) 
				{
					if (elm != this->elements[lo])
					{
						return InvalidIndex;
					}
					else      
					{
						return lo;
					}
				} 
				else 
				{
					break;
				}
			}
		}
		return InvalidIndex;
	}

	template<class TYPE> 
	Array<TYPE> FixedArray<TYPE>::AsArray() const {
		Array<TYPE> result;
		result.Reserve(this->size);
		for (SizeT i = 0; i < this->size; i++) { 
			result.Append(this->elements[i]); 
		}
		return result;
	}
}

#endif 