/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __STLGenericIterator_h__
#define __STLGenericIterator_h__

/// Iterator definition
#include <Foundation/STLIteratorDefinition.h>

namespace STL
{
	/// Forward declaration
	template< typename Iterator > class GenericIterator;

	/// Default generic iterator definition
	template< typename T > struct IsGenericIteratorType : public Traits::FalseType { };
	template< typename T > struct IsGenericIteratorType< GenericIterator< T > > : public Traits::TrueType { };

	template< typename Iterator >
	class GenericIterator 
	{
	public:
		typedef GenericIterator< Iterator > ThisType;
		typedef typename STL::IteratorTraits< Iterator* >::ValueType ValueType;
		typedef typename STL::IteratorTraits< Iterator* >::DifferenceType DifferenceType;
		typedef typename STL::IteratorTraits< Iterator* >::Reference Reference;
		typedef typename STL::IteratorTraits< Iterator* >::Pointer Pointer;
		typedef SizeT SizeType;

		GenericIterator( Pointer ptr );

		template< typename Iterator2 >
		GenericIterator(typename STL::IteratorTraits< Iterator2 >::Pointer ptr);

		ThisType& operator = (const ThisType& rhs);

		template< typename Iterator2 >
		ThisType& operator = (const GenericIterator<Iterator2>& rhs);

		operator ValueType();

		Reference operator * () const;

		ThisType& operator ++ ();
		ThisType operator ++ ( int );
		ThisType& operator -- ();
		ThisType operator -- ( int );

		Pointer Base() const;

	protected:
		Pointer myIterator;
	};


	/// Implementation of <c>GenericIterator</c>

	template< typename Iterator >
	__IME_INLINED GenericIterator< Iterator >::GenericIterator( Pointer ptr ) 
		: myIterator( ptr ) {
		/// Empty
	}

	template< typename Iterator >
	template< typename Iterator2 >
	__IME_INLINED GenericIterator< Iterator >::GenericIterator(typename STL::IteratorTraits< Iterator2 >::Pointer ptr)
		: myIterator(static_cast<Pointer>(ptr)) {
		/// Empty
	}

	template< typename Iterator >
	__IME_INLINED typename GenericIterator< Iterator >::ThisType& GenericIterator< Iterator >::operator = ( const ThisType& rhs ) {
		this->myIterator = rhs;
		return *this;
	}

	template< typename Iterator >
	template< typename Iterator2 >
	__IME_INLINED typename GenericIterator< Iterator >::ThisType& GenericIterator< Iterator >::operator = (const GenericIterator<Iterator2>& rhs) {
		this->myIterator = static_cast<Pointer>(rhs.myIterator);
		return *this;
	}

	template< typename Iterator >
	__IME_INLINED typename GenericIterator< Iterator >::Reference GenericIterator< Iterator >::operator * () const {
		return *this->myIterator;
	}

	template< typename Iterator >
	__IME_INLINED typename GenericIterator< Iterator >::ThisType& GenericIterator< Iterator >::operator ++ () {
		this->myIterator += sizeof(ValueType);
		return *this;
	}

	template< typename Iterator >
	__IME_INLINED typename GenericIterator< Iterator >::ThisType GenericIterator< Iterator >::operator ++ ( int ) {
		return ThisType( this->myIterator + sizeof(ValueType) );
	}

	template< typename Iterator >
	__IME_INLINED typename GenericIterator< Iterator >::ThisType& GenericIterator< Iterator >::operator -- () {
		this->myIterator -= sizeof(ValueType);
		return *this;
	}

	template< typename Iterator >
	__IME_INLINED typename GenericIterator< Iterator >::ThisType GenericIterator< Iterator >::operator -- ( int ) {
		return ThisType( this->myIterator - sizeof(ValueType) );
	}

	template< typename Iterator >
	__IME_INLINED typename GenericIterator<Iterator>::Pointer GenericIterator<Iterator>::Base() const {
		return this->myIterator;
	}



	template< typename Iterator0, typename Iterator1 >
	__IME_INLINED bool operator == ( const GenericIterator< Iterator0 >& left, const GenericIterator< Iterator1 >& right ) {
		return left.Base() == right.Base();
	}

	template< typename Iterator >
	__IME_INLINED bool operator == ( const GenericIterator< Iterator >& left, const GenericIterator< Iterator >& right ) {
		return left.Base() == right.Base();
	}

	template< typename Iterator0, typename Iterator1 >
	__IME_INLINED bool operator != ( const GenericIterator< Iterator0 >& left, const GenericIterator< Iterator1 >& right ) {
		return left.Base() != right.Base();
	}

	template< typename Iterator >
	__IME_INLINED bool operator != ( const GenericIterator< Iterator >& left, const GenericIterator< Iterator >& right ) {
		return left.Base() != right.Base();
	}

	template< typename Iterator0, typename Iterator1 >
	__IME_INLINED bool operator < ( const GenericIterator< Iterator0 >& left, const GenericIterator< Iterator1 >& right ) {
		return left.Base() < right.Base();
	}

	template< typename Iterator >
	__IME_INLINED bool operator < ( const GenericIterator< Iterator >& left, const GenericIterator< Iterator >& right ) {
		return left.Base() < right.Base();
	}

	template< typename Iterator0, typename Iterator1 >
	__IME_INLINED bool operator > ( const GenericIterator< Iterator0 >& left, const GenericIterator< Iterator1 >& right ) {
		return left.Base() > right.Base();
	}

	template< typename Iterator >
	__IME_INLINED bool operator > ( const GenericIterator< Iterator >& left, const GenericIterator< Iterator >& right ) {
		return left.Base() > right.Base();
	}

	template< typename Iterator0, typename Iterator1 >
	__IME_INLINED bool operator <= ( const GenericIterator< Iterator0 >& left, const GenericIterator< Iterator1 >& right ) {
		return left.Base() <= right.Base();
	}

	template< typename Iterator >
	__IME_INLINED bool operator <= ( const GenericIterator< Iterator >& left, const GenericIterator< Iterator >& right ) {
		return left.Base() <= right.Base();
	}

	template< typename Iterator0, typename Iterator1 >
	__IME_INLINED bool operator >= ( const GenericIterator< Iterator0 >& left, const GenericIterator< Iterator1 >& right ) {
		return left.Base() >= right.Base();
	}

	template< typename Iterator >
	__IME_INLINED bool operator >= ( const GenericIterator< Iterator >& left, const GenericIterator< Iterator >& right ) {
		return left.Base() >= right.Base();
	}
}

#endif /// __STLGenericIterator_h__