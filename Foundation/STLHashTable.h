/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _STLHashTable_h_
#define _STLHashTable_h_

/// STL fixed array
#include <Foundation/STLFixedArray.h>
/// STL array
#include <Foundation/STLArray.h>
/// STL key-value pair
#include <Foundation/STLPair.h>
/// STL algorithm
#include <Foundation/STLAlgorithm.h>

/// Standard template library
namespace STL
{
	template<typename KeyType, 
			 typename ValueType, 
			 typename HashFunction = STL::HashAlgorithm<typename KeyType::ValueType> > 
	class HashTable
	{
	public:
		/// <summary cref="HashTable::HashTable">
		/// Default constructor.
		/// </summary>
		HashTable();

		/// <summary cref="HashTable::HashTable">
		/// Constructor with capacity.
		/// </summary>
		/// <param name="capacity">Start capacity of array of key-value pairs.</param>
		HashTable( int capacity );

		/// <summary cref="HashTable::HashTable">
		/// Copy constructor.
		/// </summary>
		/// <param name="rhs">Reference to the hash table.</returns>
		HashTable( const HashTable& rhs );

		/// <summary cref="HashTable::HashTable">
		/// Assignment operator.
		/// </summary>
		/// <param name="rhs">Reference to the hash table.</returns>
		void operator = ( const HashTable& rhs );

		/// <summary cref="HashTable::operator[]">
		/// Read operator.
		/// </summary>
		/// <returns>Value by key, raises failed assertion if key not found.</returns>
		ValueType& operator [] ( const KeyType& key ) const;

		/// <summary cref="HashTable::Size">
		/// Get size of data inside hash-table array. Read-only.
		/// </summary>
		/// <returns>Current number of values in the hash-table.</returns>
		int Size( void ) const;

		/// <summary cref="HashTable::Capacity">
		/// Get current maximum quantity of key-value pairs inside hash-table array.
		/// </summary>
		/// <returns>Fixed-size capacity of the hash table.</returns>
		int Capacity( void ) const;

		/// <summary cref="HashTable::Clear">
		/// Just clear the hash-table.
		/// </summary>
		void Clear( void );

		/// <summary cref="HashTable::IsEmpty">
		/// Check array if it's empty or not.
		/// </summary>
		/// <returns>True if empty, otherwise false.</returns>
		bool IsEmpty( void ) const;

		/// <summary cref="HashTable::Add">
		/// Add a key-value pair object to the hash-table.
		/// </summary>
		/// <param name="pair">Pair of key and value to be passed into array.</param>
		void Add( const STL::Pair< KeyType, ValueType >& pair );

		/// <summary cref="HashTable::Add">
		/// Add a key and associated value.
		/// </summary>
		/// <param name="key">Key data to be passed into array.</param>
		/// <param name="value">Value assigned with passed key.</param>
		void Add( const KeyType& key, const ValueType& value );

		/// <summary cref="HashTable::Erase">
		/// Erase an entry by provided key.
		/// </summary>
		/// <param name="key">Key data to be used by array searcher.</param> 
		void Erase( const KeyType& key );

		/// <summary cref="HashTable::Contains">
		/// Check by the key if we have *this* value.
		/// </summary>
		/// <returns>True if key exists in the array, otherwise false.</returns>
		bool Contains( const KeyType& key ) const;

		/// <summary cref="HashTable::Content">
		/// Get array of pairs with key-value data.
		/// </summary>
		/// <reutrns>Array of all key-value pairs in the table.</returns>
		/// <remarks>This is really slow operation, use it if you REALLY need it.</remarks>
		STL::Array< STL::Pair< KeyType, ValueType > > Content( void ) const;

	private:
		typedef STL::FixedArray< STL::Array< STL::Pair< KeyType, ValueType > > > HashArrayType;
		HashArrayType hashArray; //!< Fixed array of array tables
		int size; //!< Size of hash table
		HashFunction myHashFunction;
	};

	template<typename KeyType, typename ValueType, typename HashFunction>
	HashTable< KeyType, ValueType, HashFunction >::HashTable() 
		: hashArray( 128 )
		, size( 0 ) {
		/// Empty
	}

	template<typename KeyType, typename ValueType, typename HashFunction>
	HashTable< KeyType, ValueType, HashFunction >::HashTable( int capacity )
		: hashArray( capacity )
		, size( 0 ) {
		/// Empty
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	HashTable< KeyType, ValueType, HashFunction >::HashTable( const HashTable& rhs ) 
		: hashArray( rhs.hashArray )
		, size( rhs.size ) {
		/// Empty
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	void HashTable< KeyType, ValueType, HashFunction >::operator = (const HashTable& rhs) {
		if (this != &rhs) {
			this->hashArray = rhs.hashArray;
			this->size = rhs.size;
		}
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	ValueType& HashTable< KeyType, ValueType, HashFunction >::operator [] ( const KeyType& key ) const {
		/// Get hash code from key, trim to capacity
		SizeT hashIndex = this->myHashFunction(key) % STL::CountOf<HashArrayType>(this->hashArray);
		const STL::Array< STL::Pair< KeyType, ValueType > >& hashElements = this->hashArray[hashIndex];
		SizeT numHashElements = hashElements.Size();
#if defined( DEBUG )    
		__IME_ASSERT( 0 != numHashElements ); // element with key doesn't exist
#endif
		if( 1 == numHashElements ) {
			// no hash collissions, just return the only existing element
			return hashElements[0].GetValue();
		} else {
			// here's a hash collision, find the right key
			// with a binary search
			SizeT hashElementIndex = hashElements.BinarySearchIndex( key );
#if defined( DEBUG )
			__IME_ASSERT( InvalidIndex != hashElementIndex );
#endif
			return hashElements[hashElementIndex].GetValue();
		}
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	int HashTable< KeyType, ValueType, HashFunction >::Size( void ) const {
		return this->size;
	}

	template<typename KeyType, typename ValueType, typename HashFunction>
	int HashTable< KeyType, ValueType, HashFunction >::Capacity( void ) const {
		return STL::CountOf<HashArrayType>(this->hashArray);
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	void HashTable< KeyType, ValueType, HashFunction >::Clear( void ) {
		for (SizeT i = 0; i < STL::CountOf<HashArrayType>(this->hashArray); i++) {
			this->hashArray[i].Clear();
		}
		this->size = 0;
	}

	template<typename KeyType, typename ValueType, typename HashFunction>
	bool HashTable< KeyType, ValueType, HashFunction >::IsEmpty( void ) const {
		return (0 == this->size);
	}

	template<typename KeyType, typename ValueType, typename HashFunction>
	void HashTable< KeyType, ValueType, HashFunction >::Add(const STL::Pair< KeyType, ValueType >& pair) {
#if defined( DEBUG )
		__IME_ASSERT( !this->Contains( pair.GetKey() ) );
#endif
		SizeT hashIndex = this->myHashFunction(pair.GetKey()) % STL::CountOf<HashArrayType>(this->hashArray);
		this->hashArray[hashIndex].InsertSorted(pair);
		this->size++;
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	void HashTable< KeyType, ValueType, HashFunction >::Add( const KeyType& key, const ValueType& value ) {
		STL::Pair< KeyType, ValueType > pair(key, value);
		this->Add(pair);
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	void HashTable< KeyType, ValueType, HashFunction >::Erase( const KeyType& key ) {
#if defined( DEBUG )
		__IME_ASSERT( this->size > 0 );
#endif
		SizeT hashIndex = this->myHashFunction(key) % STL::CountOf<HashArrayType>(this->hashArray);
		STL::Array< STL::Pair< KeyType, ValueType > >& hashElements = this->hashArray[hashIndex];
		SizeT hashElementIndex = hashElements.BinarySearchIndex( key );
#if defined( DEBUG )
		__IME_ASSERT( InvalidIndex != hashElementIndex ); // key doesn't exist
#endif
		hashElements.EraseIndex( hashElementIndex );
		this->size--;
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	bool HashTable< KeyType, ValueType, HashFunction >::Contains( const KeyType& key ) const {
		if (this->size > 0) {
			SizeT hashIndex = this->myHashFunction(key) % STL::CountOf<HashArrayType>(this->hashArray);
			STL::Array< STL::Pair< KeyType, ValueType > >& hashElements = this->hashArray[hashIndex];
			SizeT hashElementIndex = hashElements.BinarySearchIndex( key );
			return( InvalidIndex != hashElementIndex );
		} else {
			return false;
		}
	}

	template<typename KeyType, typename ValueType, typename HashFunction> 
	STL::Array< STL::Pair< KeyType, ValueType > > 
	HashTable< KeyType, ValueType, HashFunction >::Content( void ) const {
		STL::Array< STL::Pair< KeyType, ValueType > > result;
		for (SizeT i = 0; i < STL::CountOf<HashArrayType>(this->hashArray); i++) {
			if( this->hashArray[i].Size() > 0 ) {
				result.AppendArray( this->hashArray[i] );
			}
		}
		return result;
	}
}

#endif /// _STLHashTable_h_