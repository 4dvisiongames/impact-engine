/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __STLInsertIterator_h__
#define __STLInsertIterator_h__

/// Shared code
#include <Foundation/STLIteratorDefinition.h>

namespace STL
{
	/// <summary>
	/// <c>InsertIterator</c> is a template-based iterator, which is like an iterator except 
	/// that when you assign a value to it, the insert_iterator inserts the value into the 
	/// container and increments the iterator.
    /// </summary>
	/// <remarks>
    /// <c>InsertIterator</c> is an iterator adaptor that functions as an OutputIterator: 
    /// assignment through an <c>InsertIterator</c> inserts an object into a container. 
    /// Specifically, if ii is an <c>InsertIterator</c>, then ii keeps track of a container c and 
    /// an insertion point p; the expression *ii = x performs the insertion c.Insert(p, x).
    ///
    /// If you assign through an <c>InsertIterator</c> several times, then you will be inserting 
    /// several elements into the underlying container. In the case of a sequence, they will 
    /// appear at a particular location in the underlying sequence, in the order in which 
    /// they were inserted: one of the arguments to <c>InsertIterator</c>'s constructor is an 
    /// iterator p, and the new range will be inserted immediately before p.
	/// </remarks>
	template< typename Container >
	class InsertIterator : public IteratorDefinition< void, void, void, void >
	{
	public:
		typedef Container ContainerType;
		typedef typename Container::Iterator IteratorType;
		typedef typename Container::ConstReference ConstReference;

		explicit InsertIterator( const ContainerType& container, IteratorType iter );
		InsertIterator& operator = ( ConstReference value );
		InsertIterator& operator = ( const InsertIterator& rhs );
		InsertIterator& operator * ( void );
		InsertIterator& operator ++ ( void );
		InsertIterator& operator ++ ( int );

	protected:
		ContainerType& myContainer;
		IteratorType myIterator;
	};
}

#endif /// __STLInsertIterator_h__