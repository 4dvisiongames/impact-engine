/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Foundation__STLHierarchy_h__
#define __Foundation__STLHierarchy_h__

#include <Foundation/Shared.h>

namespace STL
{
	template< typename T >
	class IntrusiveHierarchy
	{
	public:
		typedef IntrusiveHierarchy< T > ThisType;
		typedef T ValueType;
		typedef T* Pointer;
		typedef T& Reference;
		typedef const T& ConstReference;

		IntrusiveHierarchy( void );
		~IntrusiveHierarchy( void );

		void MakeThisChildTo( Reference node );
		void MakeThisSiblingAfter( Reference node );

		bool IsParentedBy( ConstReference node );

		void RemoveFromParent( void );
		void RemoveFromHierarchy( void );

		Pointer GetParent( void ) const;
		Pointer GetFirstChild( void ) const;
		Pointer GetNextSibling( void ) const;
		Pointer GetPrevSibling( void ) const;
		Pointer GetNext( void ) const;
		Pointer GetNextLeaf( void ) const;

	private:
		Pointer myParent;
		Pointer mySibling;
		Pointer myChild;
	};

	template< typename T >
	__IME_INLINED IntrusiveHierarchy< T >::IntrusiveHierarchy( void )
		: myParent( nullptr )
		, mySibling( nullptr )
		, myChild( nullptr ) {

	}

	template< typename T >
	__IME_INLINED IntrusiveHierarchy< T >::~IntrusiveHierarchy( void ) {
		this->RemoveFromHierarchy();
	}

	template< typename T >
	__IME_INLINED void IntrusiveHierarchy< T >::MakeThisChildTo(Reference rhs) {
		this->RemoveFromParent();

		this->myParent = &rhs;
		this->mySibling = rhs.myChild;
		rhs.myChild = static_cast< Pointer >( this );
	}

	template< typename T >
	__IME_INLINED void IntrusiveHierarchy< T >::MakeThisSiblingAfter( Reference rhs ) {
		this->RemoveFromParent();

		this->myParent = rhs.myParent;
		this->mySibling = rhs.mySibling;
		rhs.mySibling = static_cast< Pointer >( this );
	}

	template< typename T >
	__IME_INLINED bool IntrusiveHierarchy< T >::IsParentedBy( ConstReference rhs ) {
		if ( this->myParent == &rhs ) {
			return true;
		} else if ( this->myParent ) {
			return this->myParent->IsParentedBy( rhs );
		}

		return false;
	}

	template< typename T >
	__IME_INLINED void IntrusiveHierarchy< T >::RemoveFromParent( void ) {
		Pointer previous = nullptr;

		if ( this->myParent ) {
			if ( this->myParent->myChild != this ) {
				Pointer node = this->myParent->myChild;
				while ( (node != this) && (node != nullptr) ) {
					previous = node;
					node = node->mySibling;
				}

				__IME_ASSERT( node == this );
			}

			if ( previous ) {
				previous->mySibling = this->mySibling;
			} else {
				this->myParent->myChild = this->mySibling;
			}
		}

		this->myParent = nullptr;
		this->mySibling = nullptr;
	}

	template< typename T >
	__IME_INLINED void IntrusiveHierarchy< T >::RemoveFromHierarchy( void ) {
		Pointer parentNode = nullptr,
				node = nullptr;

		parentNode = this->myParent;
		this->RemoveFromParent();

		if ( parentNode ) {
			while ( this->myChild ) {
				node = this->myChild;
				node->RemoveFromParent();
				node->MakeThisChildTo( *parentNode );
			}
		} else {
			while ( this->myChild )
				this->RemoveFromParent();
		}
	}

	template< typename T >
	__IME_INLINED typename IntrusiveHierarchy< T >::Pointer IntrusiveHierarchy< T >::GetParent( void ) const {
		return static_cast< Pointer >( this->myParent );
	}

	template< typename T >
	__IME_INLINED typename IntrusiveHierarchy< T >::Pointer IntrusiveHierarchy< T >::GetFirstChild( void ) const {
		return static_cast< Pointer >( this->myChild );
	}

	template< typename T >
	__IME_INLINED typename IntrusiveHierarchy< T >::Pointer IntrusiveHierarchy< T >::GetNextSibling( void ) const {
		return static_cast< Pointer >( this->mySibling );
	}

	template< typename T >
	__IME_INLINED typename IntrusiveHierarchy< T >::Pointer IntrusiveHierarchy< T >::GetPrevSibling( void ) const {
		if ( (!this->myParent) || (this->myParent->myChild == this) ) {
			return nullptr;
		}

		Pointer previous = nullptr,
				node = this->myParent->myChild;

		while ( (node != this) && (node != nullptr) ) {
			previous = node;
			node = node->mySibling;
		}

		__IME_ASSERT( node == this );

		return previous;
	}

	template< typename T >
	__IME_INLINED typename IntrusiveHierarchy< T >::Pointer IntrusiveHierarchy< T >::GetNext( void ) const {
		if ( this->myChild ) {
			return this->myChild;
		} else {
			Pointer node = this;
			while ( node && node->mySibling == nullptr )
				node = node->myParent;

			if ( node )
				return node->mySibling;
			else
				return nullptr;
		}
	}

	template< typename T >
	__IME_INLINED typename IntrusiveHierarchy< T >::Pointer IntrusiveHierarchy< T >::GetNextLeaf( void ) const {
		if ( this->myChild ) {
			Pointer node = this->myChild;
			while ( node->myChild )
				node = node->myChild;

			return node;
		} else {
			Pointer node = this;
			while ( node && node->mySibling == nullptr )
				node = node->myParent;

			if ( node ) {
				node = node->mySibling;
				while ( node->myChild )
					node = node->myChild;

				return node;
			} else {
				return nullptr;
			}
		}
	}
}

#endif /// __Foundation__STLHierarchy_h__