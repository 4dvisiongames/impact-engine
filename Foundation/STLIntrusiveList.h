/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef _STLIntrusiveList_h_
#define _STLIntrusiveList_h_

#include <Foundation/Shared.h>
#include <Foundation/STLContainerTypes.h>

namespace STL
{
	struct IntrusiveListNode 
	{
		IntrusiveListNode* myNext;
		IntrusiveListNode* myPrev;

		IntrusiveListNode() { this->myNext = this->myPrev = this; }
	};

	//! List of element of type T, where T is derived from intrusive_list_node
	/** The class is not thread safe. **/
	template< typename T, typename TypeAdapter = Traits::TypeSelect< Traits::IsSmartPointer< T >::value,
																	 Traits::SmartPtrIntrusiveAdapter< T >,
																	 Traits::DefaultIntrusiveAdapter< T > >::ValueType >
	class IntrusiveList
	{
	public:
		typedef IntrusiveList< T, TypeAdapter > ThisType;
		typedef T ReferencedValueType;
		typedef T& ReferencedRef;
		typedef const T& ReferencedConstRef;
		typedef typename TypeAdapter::ValueType ValueType;
		typedef ValueType* Pointer;
		typedef ValueType& Reference;
		typedef const ValueType* ConstPointer;
		typedef const ValueType& ConstReference;
		typedef SizeT SizeType;

	private:
		//! Pointer to the head node
		IntrusiveListNode myHead;
		//! Number of list elements
		SizeType mySize;

		template< typename Ptr, typename Ref >
		class IteratorImpl {
		protected:
			Ref item () const 
			{
				typedef Traits::TypeSelect< Traits::IsSmartPointer< Ref >::value,
											Traits::SmartPtrIntrusiveIteratorRefAdapter< 
												Ref, 
												IntrusiveListNode 
											>,
											Traits::DefaultIntrusiveIteratorRefAdapter<
												Ref,
												IntrusiveListNode
											> >::ValueType
											InteratorRefAdapter;

				return InteratorRefAdapter::Item( this->myPos );
			}

		public:
			IteratorImpl( IntrusiveListNode* pos = nullptr ) : myPos( pos ) {}
			IteratorImpl(const IteratorImpl< Ptr, Ref >& rhs) : myPos(rhs.myPos) {}
        
			bool operator == (const IteratorImpl& it) const {
				return myPos == it.myPos;
			}

			bool operator != (const IteratorImpl& it) const {
				return myPos != it.myPos;
			}

			IteratorImpl& operator++ () {
				myPos = myPos->myNext;
				return *this;
			}

			IteratorImpl& operator-- () {
				myPos = myPos->myPrev;
				return *this;
			}

			IteratorImpl operator++ (int) {
				Iterator result( *this );
				++(*this);
				return result;
			}

			IteratorImpl operator-- (int) {
				Iterator result( *this );
				--(*this);
				return result;
			}

			Ref operator* () const { return this->item(); }

			bool IsValid( void ) const { return this->myPos != nullptr; }

		private:
			//! Node the iterator points to at the moment
			IntrusiveListNode* myPos;
		};

		void assert_ok () const 
		{
			__IME_ASSERT_MSG( (myHead.myPrev == &myHead && !mySize) || (myHead.myNext != &myHead && mySize >0), "IntrusiveList corrupted" );
			SizeT i = 0;
			for (IntrusiveListNode *n = this->myHead.myNext; n != &this->myHead; n = n->myNext)
				++i;
			__IME_ASSERT_MSG( this->mySize == i, "Wrong size" );
		}

	private:
		typedef typename Traits::TypeSelect< Traits::IsSmartPointer< typename ReferencedValueType >::value,
			typename ReferencedValueType,
			typename Reference >::ValueType IteratorReference;

		typedef const typename IteratorReference ConstIteratorRef;

	public:
		typedef IteratorImpl< Pointer, IteratorReference > Iterator;
		typedef IteratorImpl< ConstPointer, ConstIteratorRef > ConstIterator;

		IntrusiveList( void ) : mySize(0) 
		{
			myHead.myPrev = &myHead;
			myHead.myNext = &myHead;
		}

		bool Empty( void ) const { return myHead.myNext == &myHead; }
		SizeType Size( void ) const { return mySize; }
		Iterator Begin( void ) { return Iterator( myHead.myNext ); }
		Iterator End( void ) { return Iterator( &myHead ); }
		ConstIterator Begin( void ) const { return ConstIterator( myHead.myNext ); }
		ConstIterator End( void ) const { return ConstIterator( &myHead ); }

		void Prepend( ReferencedRef val )
		{
			Pointer Ptr = TypeAdapter::getPointerFrom( val );
			__IME_ASSERT_MSG( Ptr->myPrev == Ptr && Ptr->myNext == Ptr,
							  "Object with intrusive list node can be part of only one intrusive list simultaneously" );

			TypeAdapter::appendedTo( Ptr );

			Ptr->myPrev = &myHead;
			Ptr->myNext = myHead.myNext;
			myHead.myNext->myPrev = Ptr;
			myHead.myNext = Ptr;

			++mySize;
			assert_ok();
		}

		void Remove( ReferencedRef val ) 
		{
			Pointer Ptr = TypeAdapter::getPointerFrom( val );
			__IME_ASSERT_MSG( Ptr->myPrev != Ptr && Ptr->myNext != Ptr, "Element to remove is not in the list" );
			__IME_ASSERT_MSG( Ptr->myPrev->myNext == Ptr && Ptr->myNext->myPrev == Ptr, "Element to remove is not in the list" );

			TypeAdapter::removedFrom( Ptr );

			Ptr->myNext->myPrev = Ptr->myPrev;
			Ptr->myPrev->myNext = Ptr->myNext;
	#if defined( DEBUG )
			Ptr->myPrev = Ptr->myNext = Ptr;
	#endif

			--mySize;
			assert_ok();
		}

		Iterator Erase( Iterator it ) 
		{
			Reference val = *it;
			++it;
			Remove( &val );
			return it;
		}
	};
}

namespace Traits
{
	template< typename Type, typename Adapter >
	struct IsIntrusiveContainer< STL::IntrusiveList< Type, Adapter > > : public TrueType { };
}

#endif /// _STLIntrusiveList_h_