/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__STLIntrusiveSList_h__
#define __Foundation__STLIntrusiveSList_h__

#include <Foundation/Shared.h>
#include <Foundation/STLIteratorSList.h>

namespace STL
{
	template< typename T, typename TypeAdapter = Traits::TypeSelect< Traits::IsSmartPointer< T >::value,
																	 Traits::SmartPtrIntrusiveAdapter< T >,
																	 Traits::DefaultIntrusiveAdapter< T > >::ValueType >
	class IntrusiveSList
	{
	private:
		struct Node
		{
			Node* myNext;

			Node() {
				this->myNext = this;
			}
		};

	public:
		typedef IntrusiveSList< T, TypeAdapter > ThisType;
		typedef T ReferencedValueType;
		typedef T& ReferencedRef;
		typedef const T& ReferencedConstRef;
		typedef typename TypeAdapter::ValueType ValueType;
		typedef ValueType* Pointer;
		typedef ValueType& Reference;
		typedef const ValueType* ConstPointer;
		typedef const ValueType& ConstReference;
		typedef SizeT SizeType;
		typedef STL::SListIterator< Pointer, Reference > Iterator;
		typedef STL::SListIterator< ConstPointer, ConstPointer > ConstIterator;
		typedef Node NodeType;

		IntrusiveSList( void );
		~IntrusiveSList( void );

		void Prepend( ReferencedRef node );
		void InsertAfter( Iterator iter, ReferencedRef node );

		template< typename IteratorType >
		void Assign( IteratorType beginIter, IteratorType endIter );

		bool PeekLast( ReferencedRef result );
		bool RemoveLast( void );

		Iterator Begin( void );
		ConstIterator Begin( void ) const;
		Iterator End( void );
		ConstIterator End( void ) const;

		SizeType Size( void ) const;
		bool IsEmpty( void ) const;

		void Clear( void );

	private:
		NodeType myRoot;
		SizeType myNodesCount;
	};

	template< typename T, typename TypeAdapter >
	__IME_INLINED IntrusiveSList< T, TypeAdapter >::IntrusiveSList( void ) : myNodesCount( 0 ) {
		this->myRoot.myNext = &this->myRoot;
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED IntrusiveSList< T, TypeAdapter >::~IntrusiveSList( void ) {
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED void IntrusiveSList< T, TypeAdapter >::Prepend( ReferencedRef node ) {
		node.myNext = this->myRoot.myNext;
		this->myRoot.myNext = &node;
		++this->myNodesCount;
	}

	template< typename T, typename TypeAdapter >
	template< typename IteratorType >
	__IME_INLINED void IntrusiveSList< T, TypeAdapter >::Assign( IteratorType beginIter, IteratorType endIter ) {
		Iterator iter( &this->myRoot );

		while ( beginIter != endIter ) {
			this->InsertAfter( iter, *beginIter );
			++iter;
			++beginIter;
			++this->myNodesCount;
		}
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED bool IntrusiveSList< T, TypeAdapter >::PeekLast( ReferencedRef node ) {
		if ( this->IsEmpty() )
			return false;

		NodeType* result = this->myRoot.myNext;
		node = *result;

		return true;
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED bool IntrusiveSList< T, TypeAdapter >::RemoveLast( void ) {
		if ( this->IsEmpty() )
			return false;

		NodeType* node = this->myRoot.myNext;
		__IME_ASSERT(this->myRoot.myNext == node);

		this->myRoot.myNext = node->myNext;
		node->myNext = node;

		--this->myNodesCount;

		return true;
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED typename IntrusiveSList< T, TypeAdapter >::Iterator IntrusiveSList< T, TypeAdapter >::Begin( void ) {
		return Iterator( static_cast< Pointer >( this->myRoot.myNext ) );
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED typename IntrusiveSList< T, TypeAdapter >::ConstIterator IntrusiveSList< T, TypeAdapter >::Begin( void ) const {
		return ConstIterator( static_cast<ConstPointer>(const_cast<Pointer>(this->myRoot.myNext)) );
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED typename IntrusiveSList< T, TypeAdapter >::Iterator IntrusiveSList< T, TypeAdapter >::End( void ) {
		return Iterator( static_cast<Pointer>(&this->myRoot) );
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED typename IntrusiveSList< T, TypeAdapter >::ConstIterator IntrusiveSList< T, TypeAdapter >::End( void ) const {
		return ConstIterator( static_cast<ConstPointer>(const_cast<Pointer>(&this->myRoot)) );
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED typename IntrusiveSList< T, TypeAdapter >::SizeType IntrusiveSList< T, TypeAdapter >::Size( void ) const {
		return this->myNodesCount;
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED bool IntrusiveSList< T, TypeAdapter >::IsEmpty( void ) const {
		/// TODO: check via myNodesCount or via recursive referencing, or both??? anyway they are the same...
		return this->myNodesCount != 0 && &this->myRoot == this->myRoot.myNext;
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED void IntrusiveSList< T, TypeAdapter >::Clear( void ) {
		this->myRoot.myNext = &this->myRoot;
		this->myNodesCount = 0;
	}
}

#endif /// __Foundation__STLIntrusiveSList_h__