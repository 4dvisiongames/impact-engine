/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__STLIntrusiveSListQueue_h__
#define __Foundation__STLIntrusiveSListQueue_h__

#include <Foundation/Shared.h>

namespace STL 
{
	template< typename T, typename TypeAdapter = Traits::TypeSelect< Traits::IsSmartPointer< T >::value,
												 Traits::SmartPtrIntrusiveAdapter< T >,
												 Traits::DefaultIntrusiveAdapter< T > >::ValueType >
	class IntrusiveSListQueue
	{
	public:
		struct Node
		{
			Node* myNext;
		};

		typedef IntrusiveSListQueue< T, TypeAdapter > ThisType;
		typedef typename TypeAdapter::ValueType ValueType;
		typedef T ReferencedValueType;
		typedef T& ReferencedRef;
		typedef const T& ReferencedConstRef;
		typedef T* ReferencedPointer;
		typedef ValueType* Pointer;
		typedef const ValueType* ConstPointer;
		typedef ValueType& Reference;
		typedef const ValueType& ConstReference;
		typedef Node NodeType;

		IntrusiveSListQueue();
		~IntrusiveSListQueue();

		void Prepend( ReferencedConstRef rhs );
		void Remove( ReferencedPointer& rhs );

		bool IsEmpty() const;
		void Clear();

	protected:
		Pointer myFirst;
		Pointer* myLast;
	};

	template< typename T, typename TypeAdapter >
	__IME_INLINED IntrusiveSListQueue< T, TypeAdapter >::IntrusiveSListQueue() : myFirst( nullptr ), myLast( &myFirst ) {
		/// Empty
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED IntrusiveSListQueue< T, TypeAdapter >::~IntrusiveSListQueue() {

	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED void IntrusiveSListQueue< T, TypeAdapter >::Prepend( ReferencedConstRef rhs ) {
		Pointer Appended = TypeAdapter::getPointerFrom( rhs );
		Appended.myNext = nullptr;
		*this->myLast = Appended;
		this->myLast = &Appended.myNext;
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED void IntrusiveSListQueue< T, TypeAdapter >::Remove( ReferencedPointer& rhs ) {
		if (this->IsEmpty())
			return;

		Pointer ptr = this->myFirst;
		this->myFirst = ptr->myNext;
		if (!this->myFirst) {
			this->myLast = &this->myFirst;
		}

		TypeAdapter::setPointerTo( ptr, rhs );
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED bool IntrusiveSListQueue< T, TypeAdapter >::IsEmpty() const {
		return !this->myFirst;
	}

	template< typename T, typename TypeAdapter >
	__IME_INLINED void IntrusiveSListQueue< T, TypeAdapter >::Clear() {
		this->myFirst = nullptr;
		this->myLast = &this->myFirst;
	}
}

#endif /// __Foundation__STLIntrusiveSList_h__