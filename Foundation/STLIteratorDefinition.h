/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __STLIteratorDefinition_h__
#define __STLIteratorDefinition_h__

/// Shared code
#include <Foundation/Shared.h>

namespace STL
{
	template< typename T, typename Dist = ptrdiff_t, typename Ptr = T*, typename Ref = T& >
	class IteratorDefinition
	{
	public:
		typedef T ValueType;
		typedef Dist DifferenceType;
		typedef Ptr Pointer;
		typedef Ref Reference;
	};

	template< typename Iter >
	class IteratorTraits
	{
	public:
		typedef typename Iter::ValueType ValueType;
		typedef typename Iter::DifferenceType DifferenceType;
		typedef typename Iter::Pointer Pointer;
		typedef typename Iter::Reference Reference;
	};

	template< typename T >
	class IteratorTraits< T* >
	{
	public:
		typedef T ValueType;
		typedef ptrdiff_t DifferenceType;
		typedef T* Pointer;
		typedef T& Reference;
	};

	template< typename T >
	class IteratorTraits< const T* >
	{
	public:
		typedef T ValueType;
		typedef ptrdiff_t DifferenceType;
		typedef const T* Pointer;
		typedef const T& Reference;
	};
}

#endif /// __STLIteratorDefinition_h__