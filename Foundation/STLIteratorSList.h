/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__STLIteratorSList_h__
#define __Foundation__STLIteratorSList_h__

namespace STL
{
	template< typename Ptr, typename Ref >
	class SListIterator
	{
	public:
		typedef SListIterator< Ptr, Ref > ThisType;
		typedef Ptr Pointer;
		typedef Ref Reference;

		SListIterator( Pointer ptr );
		SListIterator( const ThisType& rhs );

		Reference operator* (void) const;
		Pointer operator-> (void) const;

		ThisType Next( void ) const;

		ThisType& operator ++ (void);
		ThisType operator ++ (int);

		bool operator == (const ThisType& rhs);
		bool operator != (const ThisType& rhs);

	private:
		Pointer myCurrentNode;
	};

	template< typename Ptr, typename Ref >
	__IME_INLINED SListIterator< Ptr, Ref >::SListIterator( Pointer ptr ) : myCurrentNode( ptr ) {
	}

	template< typename Ptr, typename Ref >
	__IME_INLINED SListIterator< Ptr, Ref >::SListIterator( const ThisType& rhs ) : myCurrentNode( rhs.myCurrentNode ) {
	}

	template< typename Ptr, typename Ref >
	__IME_INLINED typename SListIterator< Ptr, Ref >::Reference SListIterator< Ptr, Ref >::operator* (void) const {
		return *this->myCurrentNode;
	}

	template< typename Ptr, typename Ref >
	__IME_INLINED typename SListIterator< Ptr, Ref >::Pointer SListIterator< Ptr, Ref >::operator-> (void) const {
		return this->myCurrentNode;
	}

	template< typename Ptr, typename Ref >
	__IME_INLINED typename SListIterator< Ptr, Ref >::ThisType SListIterator< Ptr, Ref >::Next( void ) const {
		return this->myCurrentNode->myNext;
	}

	template< typename Ptr, typename Ref >
	__IME_INLINED typename SListIterator< Ptr, Ref >::ThisType& SListIterator< Ptr, Ref >::operator ++ (void) {
		this->myCurrentNode = static_cast<Ptr>(this->myCurrentNode->myNext);
		return *this;
	}

	template< typename Ptr, typename Ref >
	__IME_INLINED typename SListIterator< Ptr, Ref >::ThisType SListIterator< Ptr, Ref >::operator ++ (int) {
		ThisType result( *this );
		this->myCurrentNode = static_cast<Pointer>(this->myCurrentNode->myNext);
		return result;
	}

	template< typename Ptr, typename Ref >
	__IME_INLINED bool SListIterator< Ptr, Ref >::operator == (const ThisType& rhs) {
		return this->myCurrentNode == rhs.myCurrentNode;
	}

	template< typename Ptr, typename Ref >
	__IME_INLINED bool SListIterator< Ptr, Ref >::operator != (const ThisType& rhs) {
		return this->myCurrentNode != rhs.myCurrentNode;
	}
}

#endif /// __Foundation__STLIteratorSList_h__