/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef __STLList_h__
#define __STLList_h__

/// STL reverse iterator
#include <Foundation/STLReverseIterator.h>

/// Standard template library namespace
namespace STL
{
	/// <summary>
    /// <c>ListBaseNode</c> is defined a separately from ListNode (below), because it allows
    /// us to have non-templated operations such as insert, remove (below), and it 
    /// makes it so that the list anchor node doesn't carry a T with it, which would
    /// waste space and possibly lead to surprising the user due to extra Ts existing
    /// that the user didn't explicitly create. The downside to all of this is that 
    /// it makes debug viewing of a list harder, given that the node pointers are of 
    /// type ListNodeBase and not ListNode. However, see ListNodeBaseProxy below.
    /// </summary>
	class ListNodeBase
	{
	public:
		ListNodeBase* myNext;
		ListNodeBase* myPrev;

		void Insert( ListNodeBase* next );
		void Remove( void );
		void Splice( ListNodeBase* first, ListNodeBase* last );
		void Reverse( void );
		static void Swap( ListNodeBase& a, ListNodeBase& b );
	};

	__IME_INLINED void ListNodeBase::Insert( ListNodeBase* next )
	{
		this->myNext = next;
		this->myPrev = next->myPrev;
		next->myPrev->myNext = this;
		next->myPrev = this;
	}

	__IME_INLINED void ListNodeBase::Remove( void )
	{
		this->myPrev->myNext = this->myNext;
		this->myNext->myPrev = this->myPrev;
	}

	__IME_INLINED void ListNodeBase::Splice( ListNodeBase* first, ListNodeBase* last )
	{
		/// We assume that [first, last] are not within our list.
		last->myPrev->myNext = this;
		first->myPrev->myNext = last;
		this->myPrev->myNext = first;

		ListNodeBase* const temp = this->myPrev;
		this->myPrev = last->myPrev;
		last->myPrev = first->myPrev;
		first->myPrev = temp;
	}

	__IME_INLINED void ListNodeBase::Reverse( void )
	{
		ListNodeBase* node = this;
		do {
			ListNodeBase* const temp = node->myNext;
			node->myNext = node->myPrev;
			node->myPrev = temp;
			node = node->myPrev;
		} while( node != this );
	}

	__IME_INLINED void ListNodeBase::Swap( ListNodeBase& a, ListNodeBase& b )
	{
		const ListNodeBase temp( a );
		a = b;
		b = temp;

		if( a.myNext == &b )
			a.myNext = a.myPrev = &a;
		else
			a.myNext->myPrev = a.myPrev->myNext = &a;

		if( b.myNext == &a )
			b.myNext = b.myPrev = &b;
		else
			b.myNext->myPrev = b.myPrev->myNext = &b;
	}


#if defined( __IME_USE_DEBUG )
	/// In debug builds, we define ListNodeBaseProxy to be the same thing as
    /// ListNodeBase, except it is templated on the parent ListNode class.
    /// We do this because we want users in debug builds to be able to easily
    /// view the list's contents in a debugger GUI. We do this only in a debug
    /// build for the reasons described above: that ListNodeBase needs to be
    /// as efficient as possible and not cause code bloat or extra function 
    /// calls (inlined or not).
    ///
    /// ListNodeBaseProxy *must* be separate from its parent class ListNode 
    /// because the list class must have a member node which contains no T value.
    /// It is thus incorrect for us to have one single ListNode class which
    /// has mpNext, mpPrev, and mValue. So we do a recursive template trick in 
    /// the definition and use of SListNodeBaseProxy.
	template< typename T >
	struct ListNodeBaseProxy
	{
		T* myNext;
		T* myPrev;
	};

	template< typename T >
	struct ListNode : public ListNodeBaseProxy< ListNode< T > >
	{
		T myValue;
	};

#else
	template< typename T >
	struct ListNode : public ListNodeBase
	{
		T myValue;
	};

#endif


	/// <summary>
	/// </summary>
	template< typename T, typename IntrusivePtr, typename Ref >
	class ListIterator
	{
	public:
		typedef ListIterator< T, IntrusivePtr, Ref > ThisType;
		typedef ListIterator< T, T*, T& > Iterator;
		typedef ListIterator< T, const T*, const T& > ConstIterator;
		typedef SizeT SizeType;
		typedef ptrdiff_t DifferenceType;
		typedef T ValueType;
		typedef ListNode< T > NodeType;
		typedef IntrusivePtr Pointer;
		typedef Ref Reference;

		ListIterator( void );
		ListIterator( const ListNodeBase* node );
		ListIterator( const Iterator& rhs );

		bool IsValid( void ) const;

		Reference operator * ( void ) const;
		Pointer operator -> ( void ) const;

		ThisType& operator ++ ( void );
		ThisType operator ++ ( int );

		ThisType& operator -- ( void );
		ThisType operator -- ( int );

		NodeType* myNode;
	};

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED ListIterator< T, IntrusivePtr, Ref >::ListIterator( void ) : myNode( nullptr )
	{
		/// Empty
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED ListIterator< T, IntrusivePtr, Ref >::ListIterator( const ListNodeBase* node ) : myNode( static_cast< NodeType* >( reinterpret_cast< ListNode< T >* >( const_cast< ListNodeBase* >( node ) ) ) )
	{
		/// Empty
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED ListIterator< T, IntrusivePtr, Ref >::ListIterator( const Iterator& x ) : myNode( const_cast< NodeType* >( x.myNode ) )
	{
		/// Empty
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED bool ListIterator< T, IntrusivePtr, Ref >::IsValid( void ) const
	{
		return this->myNode != nullptr;
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED typename ListIterator< T, IntrusivePtr, Ref >::Reference ListIterator< T, IntrusivePtr, Ref >::operator * ( void ) const
	{
		return this->myNode->myValue;
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED typename ListIterator< T, IntrusivePtr, Ref >::Pointer ListIterator< T, IntrusivePtr, Ref >::operator -> ( void ) const
	{
		return &this->myNode->myValue;
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED typename ListIterator< T, IntrusivePtr, Ref >::ThisType& ListIterator< T, IntrusivePtr, Ref >::operator ++ ( void )
	{
		this->myNode = const_cast< NodeType* >( this->myNode->myNext );
		return *this;
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED typename ListIterator< T, IntrusivePtr, Ref >::ThisType ListIterator< T, IntrusivePtr, Ref >::operator ++ ( int ) 
	{
		ThisType temp( *this );
		this->myNode = const_cast< NodeType* >( this->myNode->myNext );
		return temp;
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED typename ListIterator< T, IntrusivePtr, Ref >::ThisType& ListIterator< T, IntrusivePtr, Ref >::operator -- ( void )
	{
		this->myNode = this->myNode->myPrev;
		return *this;
	}

	template< typename T, typename IntrusivePtr, typename Ref >
	__IME_INLINED typename ListIterator< T, IntrusivePtr, Ref >::ThisType ListIterator< T, IntrusivePtr, Ref >::operator -- ( int )
	{
		ThisType temp( *this );
		this->myNode = this->myNode->myPrev;
		return temp;
	}

	/// The C++ defect report #179 requires that we support comparisons between const and non-const iterators.
    /// Thus we provide additional template paremeters here to support this. The defect report does not
    /// require us to support comparisons between ReverseIterator(s) and ReverseConstIterator(s).
	template< typename T, typename PtrA, typename PtrB, typename RefA, typename RefB >
	__IME_INLINED bool operator == ( const ListIterator< T, PtrA, RefA >& a, const ListIterator< T, PtrB, RefB >& b )
	{
		return a.myNode == b.myNode;
	}

	template< typename T, typename PtrA, typename PtrB, typename RefA, typename RefB >
	__IME_INLINED bool operator != ( const ListIterator< T, PtrA, RefA >& a, const ListIterator< T, PtrB, RefB >& b )
	{
		return a.myNode != b.myNode;
	}

	/// We provide a version of operator== and operator!= for the case where the iterators are of the 
    /// same type. This helps prevent ambiguity errors in the presence of relative operations
	template< typename T, typename PtrA, typename RefA >
	__IME_INLINED bool operator == ( const ListIterator< T, PtrA, RefA >& a, const ListIterator< T, PtrA, RefA >& b )
	{
		return a.myNode == b.myNode;
	}

	template< typename T, typename PtrA, typename RefA >
	__IME_INLINED bool operator != ( const ListIterator< T, PtrA, RefA >& a, const ListIterator< T, PtrA, RefA >& b )
	{
		return a.myNode != b.myNode;
	}


	/// <summary>
	/// </summary>
	template< typename T >
	class ListBase
	{
	public:
		typedef T ValueType;
		typedef ListNode< T > NodeType;
		typedef SizeT SizeType;
		typedef ptrdiff_t DifferenceType;
		
#if defined( __IME_USE_DEBUG )
		typedef ListNodeBaseProxy< ListNode< T > > BaseNodeType;
#else
		typedef ListNodeBase BaseNodeType;
#endif

		static const SizeType theAlignement = Traits::Workarounds::AlignmentBug< sizeof( T ), T >::alignment;
		static const SizeType theAlignmentOffset = 0; //! BUGBUG: there is no way to use offsetof...

	protected:
		ListBase( void );
		~ListBase( void );

		NodeType* AllocateNode( void );
		void FreeNode( NodeType* node );

		void Initialize( void );
		void ClearList( void );

	protected:
		BaseNodeType myNode;
		SizeType myListSize; //!< Cached list size
	};

	template< typename T >
	__IME_INLINED ListBase< T >::ListBase( void ) : myNode(), myListSize( 0 )
	{
		this->Initialize();
	}

	template< typename T >
	__IME_INLINED ListBase< T >::~ListBase( void )
	{
		this->ClearList();
	}

	template< typename T >
	__IME_INLINED typename ListBase< T >::NodeType* ListBase< T >::AllocateNode( void )
	{
		NodeType* result = static_cast< NodeType* >( Memory::Allocator::Malloc( sizeof( NodeType ) ) );
		return result;
	}

	template< typename T >
	__IME_INLINED void ListBase< T >::FreeNode( NodeType* node )
	{
		/// We are not checking for node pointer, because node must be always non-NULL
		Memory::Allocator::Free( node );
	}

	template< typename T >
	__IME_INLINED void ListBase< T >::Initialize( void )
	{
		this->myNode.myNext = reinterpret_cast< ListNode< T >* >( &this->myNode );
		this->myNode.myPrev = reinterpret_cast< ListNode< T >* >( &this->myNode );
	}

	template< typename T >
	__IME_INLINED void ListBase< T >::ClearList( void )
	{
		NodeType* ptr = static_cast< NodeType* >( this->myNode.myNext );
		while( ptr != &this->myNode ) {
			NodeType* const temp = ptr;
			ptr = static_cast< NodeType* >( ptr->myNext );
			temp->~NodeType();
			this->FreeNode( temp );
		}
	}


	/// <summary>
	/// </summary>
	template< typename T >
	class List : public ListBase< T >
	{
		typedef ListBase< T > BaseType;

	public:
		typedef List< T > ThisType;
		typedef T ValueType;
		typedef T* Pointer;
		typedef const T* ConstPointer;
		typedef T& Reference;
		typedef const T& ConstReference;
		typedef ListIterator< T, T*, T& > Iterator;
		typedef ListIterator< T, const T*, const T& > ConstIterator;
		typedef STL::ReverseIterator< Iterator > ReverseIterator;
		typedef STL::ReverseIterator< ConstIterator > ReverseConstIterator;
		typedef typename BaseType::SizeType SizeType;
		typedef typename BaseType::DifferenceType DifferenceType;
		typedef typename BaseType::NodeType NodeType;
		typedef typename BaseType::BaseNodeType BaseNodeType;

		using BaseType::AllocateNode;
		using BaseType::FreeNode;
		using BaseType::Initialize;
		using BaseType::ClearList;

		using BaseType::myNode; //! My referenced node, which is first to be used
		using BaseType::myListSize;

	public:
		List( void );
		explicit List( SizeType toReserve );
		List( SizeType size, const ValueType& value );
		List( const ThisType& rhs );

		template< typename Iter >
		List( Iter firstIter, Iter lastIter );

		ThisType& operator = ( const ThisType& rhs );
		void Swap( ThisType& x );

		void Assign( SizeType num, const ValueType& rhs );

		template< typename Iter >
		void Assign( Iter firstIter, Iter lastIter );

		Iterator Begin( void );
		Iterator End( void );
		ConstIterator Begin( void ) const;
		ConstIterator End( void ) const;

		typename ThisType::ReverseIterator RBegin( void );
		typename ThisType::ReverseIterator REnd( void );
		typename ThisType::ReverseConstIterator RBegin( void ) const;
		typename ThisType::ReverseConstIterator REnd( void ) const;

		bool IsEmpty( void ) const;
		SizeType Size( void ) const;

		void Resize( SizeType num, const ValueType& rhs );
		void Resize( SizeType num );

		Reference Front( void );
		ConstReference Front( void ) const;

		Reference Back( void );
		ConstReference Back( void ) const;

		void AppendFront( const ValueType& rhs );
		Reference AppendFront( void );

		void AppendBack( const ValueType& rhs );
		Reference AppendBack( void );

		void RemoveFront( void );
		void RemoveBack( void );

		Iterator Insert( Iterator position );
		Iterator Insert( Iterator position, const ValueType& rhs );
		void Insert( Iterator position, SizeType num, const ValueType& rhs );

		template< typename Iter >
		void Insert( Iterator position, Iter beginIter, Iter endIter );

		Iterator Erase( Iterator position );
		Iterator Erase( Iterator beginIter, Iterator endIter );
		ReverseIterator Erase( ReverseIterator position );
		ReverseIterator Erase( ReverseIterator beginIter, ReverseIterator endIter );

		void Clear( void );
		void Reset( void );

		void Remove( const ValueType& rhs );

		template< typename Predicate >
		void RemoveIf( Predicate );

		void Reverse( void );

		void Splice( Iterator position, ThisType& x );
		void Splice( Iterator position, ThisType& x, Iterator iter );
		void Splice( Iterator position, ThisType& x, Iterator beginIter, Iterator endIter );

		/// Sorting functionality

		void Merge( ThisType& rhs );

		template< typename Comparator >
		void Merge( ThisType& rhs, Comparator compFunction );

		void Unique( void );

		template< typename BinaryPredicator >
		void Uniqie( BinaryPredicator prediction );

		void Sort( void );
		
		template< typename Comparator >
		void Sort( Comparator compFunction );

		bool Validate( void ) const;
		bool ValidateIterator( ConstIterator iter ) const;

	protected:
		NodeType* CreateNode( void );
		NodeType* CreateNode( const ValueType& rhs );

		template< typename Integer >
		void Assign( Integer num, Integer value, Traits::TrueType );

		template< typename Iter >
		void Assign( Iter beginIter, Iter endIter, Traits::FalseType );

		void AssignValue( SizeType num, const ValueType& value );

		template< typename Integer >
		void Insert( ListNodeBase* node, Integer num, Integer value, Traits::TrueType );

		template< typename Iter >
		void Insert( ListNodeBase* node, Iter beginIter, Iter endIter, Traits::FalseType );

		void InsertValues( ListNodeBase* node, SizeType num, const ValueType& rhs );

		void InsertValue( ListNodeBase* node, const ValueType& rhs );

		void Erase( ListNodeBase* node );
	};

	template< typename T >
	__IME_INLINED List< T >::List( void ) : BaseType()
	{
		/// Empty
	}

	template< typename T >
	__IME_INLINED List< T >::List( SizeType num ) : BaseType()
	{
		this->InsertValues( reinterpret_cast< ListNodeBase* >( &this->myNode ), num, ValueType() );
	}

	template< typename T >
	__IME_INLINED List< T >::List( SizeType num, const ValueType& rhs ) : BaseType()
	{
		this->InsterValues( reinterpret_cast< ListNodeBase* >( &this->myNode ), num, rhs );
	}

	template< typename T >
	__IME_INLINED List< T >::List( const ThisType& rhs ) : BaseType()
	{
		this->Insert( reinterpret_cast< ListNodeBase* >( &this->myNode ), ConstIterator( reinterpret_cast< const ListNodeBase* >( rhs.myNode.myNext ) ), ConstIterator( reinterpret_cast< const ListNodeBase* >( rhs.myNode ) ), Traits::FalseType() );
	}

	template< typename T >
	template< typename Iter >
	__IME_INLINED List< T >::List( Iter beginIter, Iter endIter ) : BaseType()
	{
		this->Insert( reinterpret_cast< ListNodeBase* >( &this->myNode ), beginIter, endIter, Traits::IsIntegralType< Iter >() );
	}

	template< typename T >
	__IME_INLINED typename List< T >::Iterator List< T >::Begin( void )
	{
		return Iterator( reinterpret_cast< ListNodeBase* >( this->myNode.myNext ) );
	}

	template< typename T >
	__IME_INLINED typename List< T >::Iterator List< T >::End( void )
	{
		return Iterator( reinterpret_cast< ListNodeBase* >( &this->myNode ) );
	}

	template< typename T >
	__IME_INLINED typename List< T >::ConstIterator List< T >::Begin( void ) const
	{
		return ConstIterator( reinterpret_cast< const ListNodeBase* >( this->myNode.myNext ) );
	}

	template< typename T >
	__IME_INLINED typename List< T >::ConstIterator List< T >::End( void ) const
	{
		return ConstIterator( reinterpret_cast< const ListNodeBase* >( &this->myNode ) );
	}

	template< typename T >
	__IME_INLINED typename List< T >::ReverseIterator List< T >::RBegin( void )
	{
		return ThisType::ReverseIterator( reinterpret_cast< ListNodeBase* >( &this->myNode ) );
	}

	template< typename T >
	__IME_INLINED typename List< T >::ReverseIterator List< T >::REnd( void )
	{
		return ThisType::ReverseIterator( reinterpret_cast< ListNodeBase* >( this->myNode.myNext ) );
	}

	template< typename T >
	__IME_INLINED typename List< T >::ReverseConstIterator List< T >::RBegin( void ) const
	{
		return ThisType::ReverseConstIterator( reinterpret_cast< const ListNodeBase* >( &this->myNode ) );
	}

	template< typename T >
	__IME_INLINED typename List< T >::ReverseConstIterator List< T >::REnd( void ) const
	{
		return ThisType::ReverseConstIterator( reinterpret_cast< const ListNodeBase* >( this->myNode.myNext ) );
	}

	template< typename T >
	__IME_INLINED bool List< T >::IsEmpty( void ) const
	{
		return static_cast< NodeType* >( this->myNode.myNext ) == &this->myNode;
	}

	template< typename T >
	__IME_INLINED typename List< T >::SizeType List< T >::Size( void ) const
	{
		return this->myListSize;
	}

	template< typename T >
	__IME_INLINED void List< T >::AppendFront( const ValueType& rhs )
	{
		this->InsertValue( reinterpret_cast< ListNodeBase* >( this->myNode.myNext ), rhs );
	}

	template< typename T >
	__IME_INLINED typename List< T >::Iterator List< T >::Erase( Iterator iter )
	{
		++iter;
		this->Erase( reinterpret_cast< ListNodeBase* >( iter.myNode->myPrev ) );
		return iter;
	}

	template< typename T >
	__IME_INLINED void List< T >::Remove( const ValueType& rhs )
	{
		Iterator current( reinterpret_cast< ListNodeBase* >( this->myNode.myNext ) );
		while( current.myNode != &this->myNode ) {
			if( __IME_CONDITION_LIKELY( !(*current == rhs) ) ) {
				++current;
			} else {
				++current;
				this->Erase( reinterpret_cast< ListNodeBase* >( current.myNode->myPrev ) );
			}
		}
	}

	template< typename T >
	__IME_INLINED typename List< T >::NodeType* List< T >::CreateNode( void )
	{
		NodeType* const ptr = this->AllocateNode();
		::new( &ptr->myValue ) ValueType;
		return ptr;
	}

	template< typename T >
	__IME_INLINED typename List< T >::NodeType* List< T >::CreateNode( const ValueType& rhs )
	{
		NodeType* const ptr = this->AllocateNode();
		::new( &ptr->myValue ) ValueType( rhs );
		return ptr;
	}

	template< typename T >
	__IME_INLINED void List< T >::InsertValue( ListNodeBase* ptr, const ValueType& rhs )
	{
		NodeType* const newNode = this->CreateNode( rhs );
		(reinterpret_cast< ListNodeBase* >( newNode ))->Insert( ptr );
		++this->myListSize;
	}

	template< typename T >
	__IME_INLINED void List< T >::Erase( ListNodeBase* ptr )
	{
		ptr->Remove();
		(reinterpret_cast< NodeType* >( ptr ))->~NodeType();
		this->FreeNode( reinterpret_cast< NodeType* >( ptr ) );
		--this->myListSize;
	}
} // namespace STL

#endif /// _STLList_h_
