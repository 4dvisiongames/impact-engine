/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _STLPair_h_
#define _STLPair_h_

/// Engine shared code
#include <Foundation/Shared.h>

/// Standard template library namespace
namespace STL
{
    /// Key-Value pair defintion
	template< class Key, class Value > class Pair
	{
	public:
        /// @brief
		/// Default constructor
		Pair( void );

        /// @brief
		/// Constructor with key and value
		Pair(const Key& k, const Value& v);

        /// @brief
		/// Constructor with key and undefined value
		/// @note
		/// This strange constructor is useful for search-by-key if
		/// the key-value-pairs are stored in a STL::Array.
		Pair(const Key& k);

		/// copy constructor
		Pair(const Pair<Key, Value>& rhs);

        /// @brief
		/// Assignment operator
		void operator=(const Pair<Key, Value>& rhs);

        /// @brief
		/// Equality operator
		bool operator==(const Pair<Key, Value>& rhs) const;

        /// @brief
		/// Inequality operator
		bool operator!=(const Pair<Key, Value>& rhs) const;

        /// @brief
		/// Greater operator
		bool operator>(const Pair<Key, Value>& rhs) const;

        /// @brief
		/// Greater-or-equal operator
		bool operator>=(const Pair<Key, Value>& rhs) const;

        /// @brief
		/// Lesser operator
		bool operator<(const Pair<Key, Value>& rhs) const;

        /// @brief
		/// Lesser-or-equal operator
		bool operator<=(const Pair<Key, Value>& rhs) const;

		/// Read/Write access to key
		Key& GetKey();

        /// @brief
		/// Read/Write access to value
		Value& GetValue();

        /// @brief
		/// Read-only access to key
		const Key& GetKey() const;

        /// @brief
		/// Read-only access to value
		const Value& GetValue() const;

	protected:
		Key keyData; //!< Data for accessig value
		Value valueData; //!< Data with predefined value
	};


	/*template< typename Key, typename Value >
	__IME_INLINED SizeT HashAlgorithm( const STL::Pair< Key, Value >& pair )
	{
		return HashAlgorithm( pair.GetKey() ) ^ HashAlgorithm( pair.GetValue() );
	}*/


	template<class Key, class Value> 
	Pair<Key, Value>::Pair( void ) {
		/// Empty
	}

	template<class Key, class Value> 
	Pair<Key, Value>::Pair(const Key& k, const Value& v) 
		: keyData(k)
		, valueData(v) {
		/// Empty
	}

	template<class Key, class Value> 
	Pair<Key, Value>::Pair(const Key& k) 
		: keyData(k) {
		/// Empty
	}

	template<class Key, class Value> 
	Pair<Key, Value>::Pair(const Pair<Key, Value>& rhs) 
		: keyData(rhs.keyData)
		, valueData(rhs.valueData) {
		/// Empty
	}

	template<class Key, class Value> 
	void Pair<Key, Value>::operator=(const Pair<Key, Value>& rhs) {
		this->keyData = rhs.keyData;
		this->valueData = rhs.valueData;
	}

	template<class Key, class Value> 
	bool Pair<Key, Value>::operator==(const Pair<Key, Value>& rhs) const {
		return (this->keyData == rhs.keyData);
	}

	template<class Key, class Value> 
	bool Pair<Key, Value>::operator!=(const Pair<Key, Value>& rhs) const {
		return (this->keyData != rhs.keyData);
	}

	template<class Key, class Value> 
	bool Pair<Key, Value>::operator>(const Pair<Key, Value>& rhs) const {
		return (this->keyData > rhs.keyData);
	}

	template<class Key, class Value> 
	bool Pair<Key, Value>::operator>=(const Pair<Key, Value>& rhs) const {
		return (this->keyData >= rhs.keyData);
	}

	template<class Key, class Value> 
	bool Pair<Key, Value>::operator<(const Pair<Key, Value>& rhs) const {
		return (this->keyData < rhs.keyData);
	}

	template<class Key, class Value> 
	bool Pair<Key, Value>::operator<=(const Pair<Key, Value>& rhs) const {
		return (this->keyData <= rhs.keyData);
	}

	template<class Key, class Value> 
	Key& Pair<Key, Value>::GetKey() {
		return this->keyData;
	}

	template<class Key, class Value> 
	Value& Pair<Key, Value>::GetValue() {
		return this->valueData;
	}

	template<class Key, class Value> 
	const Key& Pair<Key, Value>::GetKey() const {
		return this->keyData;
	}

	template<class Key, class Value> 
	const Value& Pair<Key, Value>::GetValue() const {
		return this->valueData;
	}
} /// namespace STL

#endif /// _STLPair_h_
