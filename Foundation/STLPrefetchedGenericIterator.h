/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Foundation__STLGenericIterator_h__
#define __Foundation__STLGenericIterator_h__

/// Iterator definition
#include <Foundation/STLIteratorDefinition.h>

namespace STL
{
	/// Forward declaration
	template< typename Iterator, typename Container > class PrefetchedGenericIterator;

	/// Default generic iterator definition
	template< typename T > struct IsGenericIteratorType : public Traits::FalseType { };
	template< typename T, typename C > struct IsGenericIteratorType< PrefetchedGenericIterator< T, C > > : public Traits::TrueType { };

	template< typename Iterator, typename Container = void >
	class PrefetchedGenericIterator 
	{
	public:
		typedef PrefetchedGenericIterator< Iterator, Container > ThisType;
		typedef typename STL::IteratorTraits< Iterator >::ValueType ValueType;
		typedef typename STL::IteratorTraits< Iterator >::DifferenceType DifferenceType;
		typedef typename STL::IteratorTraits< Iterator >::Reference Reference;
		typedef typename STL::IteratorTraits< Iterator >::Pointer Pointer;
		typedef Iterator IteratorType;
		typedef Container ContainerType;

		PrefetchedGenericIterator( void );
		PrefetchedGenericIterator( const IteratorType& rhs );

		template< typename Iterator2 >
		PrefetchedGenericIterator( const PrefetchedGenericIterator< Iterator2, Container >& rhs );

		ThisType& operator = ( const IteratorType& rhs );

		Reference operator * ( void ) const;
		Pointer operator -> ( void ) const;

		ThisType& operator ++ ( void );
		ThisType operator ++ ( int );
		ThisType& operator -- ( void );
		ThisType operator -- ( int );

		Reference operator [] ( const DifferenceType& rhs ) const;
		ThisType& operator += ( const DifferenceType& rhs );
		ThisType operator + ( const DifferenceType& rhs ) const;
		ThisType& operator -= ( const DifferenceType& rhs );
		ThisType operator - ( const DifferenceType& rhs ) const;

		const IteratorType& Base( void ) const;

	protected:
		IteratorType myIterator;
	};


	/// Implementation of <c>GenericIterator</c>

	template< typename Iterator, typename Container >
	__IME_INLINED PrefetchedGenericIterator< Iterator, Container >::PrefetchedGenericIterator( void ) : myIterator( nullptr )
	{
		/// Empty
	}

	template< typename Iterator, typename Container >
	__IME_INLINED PrefetchedGenericIterator< Iterator, Container >::PrefetchedGenericIterator( const IteratorType& rhs ) : myIterator( rhs )
	{
		/// Empty
	}

	template< typename Iterator, typename Container >
	template< typename Iterator >
	__IME_INLINED PrefetchedGenericIterator< Iterator, Container >::PrefetchedGenericIterator( const PrefetchedGenericIterator< Iterator2, Container >& rhs ) : myIterator( rhs.Base() )
	{
		/// Empty
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType& PrefetchedGenericIterator< Iterator, Container >::operator = ( const IteratorType& rhs )
	{
		this->myIterator = rhs;
		return *this;
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::Reference PrefetchedGenericIterator< Iterator, Container >::operator * ( void ) const
	{
		return *this->myIterator;
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::Pointer PrefetchedGenericIterator< Iterator, Container >::operator -> ( void ) const
	{
		return this->myIterator;
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::Reference PrefetchedGenericIterator< Iterator, Container >::operator [] ( const DifferenceType& rhs ) const
	{
		return this->myIterator[rhs];
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType& PrefetchedGenericIterator< Iterator, Container >::operator ++ ( void )
	{
		++this->myIterator;
		return *this;
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType PrefetchedGenericIterator< Iterator, Container >::operator ++ ( int )
	{
		return ThisType( this->myIterator++ );
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType& PrefetchedGenericIterator< Iterator, Container >::operator -- ( void ) 
	{
		--this->myIterator;
		return *this;
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType PrefetchedGenericIterator< Iterator, Container >::operator -- ( int )
	{
		return ThisType( this->myIterator-- );
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType& PrefetchedGenericIterator< Iterator, Container >::operator += ( const DifferenceType& rhs )
	{
		this->myIterator += rhs;
		return *this;
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType PrefetchedGenericIterator< Iterator, Container >::operator + ( const DifferenceType& rhs ) const
	{
		return ThisType( this->myIterator + rhs );
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType& PrefetchedGenericIterator< Iterator, Container >::operator -= ( const DifferenceType& rhs )
	{
		this->myIterator -= rhs;
		return *this;
	}

	template< typename Iterator, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator, Container >::ThisType PrefetchedGenericIterator< Iterator, Container >::operator - ( const DifferenceType& rhs ) const
	{
		return ThisType( this->myIterator - rhs );
	}

	template< typename Iterator, typename Container >
	__IME_INLINED const typename PrefetchedGenericIterator< Iterator, Container >::IteratorType& PrefetchedGenericIterator< Iterator, Contaier >::Base( void ) const
	{
		return this->myIterator;
	}



	template< typename Iterator0, typename Iterator1, typename Container >
	__IME_INLINED bool operator == ( const PrefetchedGenericIterator< Iterator0, Container >& left, const PrefetchedGenericIterator< Iterator1, Container >& right )
	{
		return left.Base() == right.Base();
	}

	template< typename Iterator, typename Container >
	__IME_INLINED bool operator == ( const PrefetchedGenericIterator< Iterator, Container >& left, const PrefetchedGenericIterator< Iterator, Container >& right )
	{
		return left.Base() == right.Base();
	}

	template< typename Iterator0, typename Iterator1, typename Container >
	__IME_INLINED bool operator != ( const PrefetchedGenericIterator< Iterator0, Container >& left, const PrefetchedGenericIterator< Iterator1, Container >& right )
	{
		return left.Base() != right.Base();
	}

	template< typename Iterator, typename Container >
	__IME_INLINED bool operator != ( const PrefetchedGenericIterator< Iterator, Container >& left, const PrefetchedGenericIterator< Iterator, Container >& right )
	{
		return left.Base() != right.Base();
	}

	template< typename Iterator0, typename Iterator1, typename Container >
	__IME_INLINED bool operator < ( const PrefetchedGenericIterator< Iterator0, Container >& left, const PrefetchedGenericIterator< Iterator1, Container >& right )
	{
		return left.Base() < right.Base();
	}

	template< typename Iterator, typename Container >
	__IME_INLINED bool operator < ( const PrefetchedGenericIterator< Iterator, Container >& left, const PrefetchedGenericIterator< Iterator, Container >& right )
	{
		return left.Base() < right.Base();
	}

	template< typename Iterator0, typename Iterator1, typename Container >
	__IME_INLINED bool operator > ( const PrefetchedGenericIterator< Iterator0, Container >& left, const PrefetchedGenericIterator< Iterator1, Container >& right )
	{
		return left.Base() > right.Base();
	}

	template< typename Iterator, typename Container >
	__IME_INLINED bool operator > ( const PrefetchedGenericIterator< Iterator, Container >& left, const PrefetchedGenericIterator< Iterator, Container >& right )
	{
		return left.Base() > right.Base();
	}

	template< typename Iterator0, typename Iterator1, typename Container >
	__IME_INLINED bool operator <= ( const PrefetchedGenericIterator< Iterator0, Container >& left, const PrefetchedGenericIterator< Iterator1, Container >& right )
	{
		return left.Base() <= right.Base();
	}

	template< typename Iterator, typename Container >
	__IME_INLINED bool operator <= ( const PrefetchedGenericIterator< Iterator, Container >& left, const PrefetchedGenericIterator< Iterator, Container >& right )
	{
		return left.Base() <= right.Base();
	}

	template< typename Iterator0, typename Iterator1, typename Container >
	__IME_INLINED bool operator >= ( const PrefetchedGenericIterator< Iterator0, Container >& left, const PrefetchedGenericIterator< Iterator1, Container >& right )
	{
		return left.Base() >= right.Base();
	}

	template< typename Iterator, typename Container >
	__IME_INLINED bool operator >= ( const PrefetchedGenericIterator< Iterator, Container >& left, const PrefetchedGenericIterator< Iterator, Container >& right )
	{
		return left.Base() >= right.Base();
	}

	template< typename Iterator0, typename Iterator1, typename Container >
	__IME_INLINED typename PrefetchedGenericIterator< Iterator0, Container >::DifferenceType operator - ( const PrefetchedGenericIterator< Iterator0, Container >& left, const GenericIterator< Iterator1, Container >& right )
	{
		return left.Base() - right.Base();
	}

	template< typename Iterator, typename Container >
	__IME_INLINED PrefetchedGenericIterator< Iterator, Container > operator + ( typename PrefetchedGenericIterator< Iterator, Container >::DifferenceType num, const PrefetchedGenericIterator< Iterator0, Container >& right )
	{
		return PrefetchedGenericIterator< Iterator, Container >( right.Base() + num );
	}
}

#endif /// __Foundation__STLGenericIterator_h__