/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Engine shared code
#include <Foundation/Shared.h>
/// STL array
#include <Foundation/STLArray.h>

/// Standard template library namespace
namespace STL
{
	/// <summary>
	/// <c>STL::Queue</c> is a FIFO container implementation.
	/// </summary>
	template<typename Type> 
	class Queue
	{
		typedef STL::Array<Type> ImplementorType;

	public:
		typedef Queue<Type> ThisType;
		typedef typename ImplementorType::ValueType ValueType;
		typedef typename ImplementorType::Reference Reference;
		typedef typename ImplementorType::ConstReference ConstReference;
		typedef typename ImplementorType::SizeType SizeType;

		/// constructor
		Queue();
		/// copy constructor
		Queue( const ThisType& rhs );

		/// assignment operator
		void operator=(const ThisType& rhs);
		/// access element by index, 0 is the frontmost element (next to be dequeued)
		Reference operator[]( SizeType index ) const;
		/// equality operator
		bool operator==(const ThisType& rhs) const;
		/// inequality operator
		bool operator!=(const ThisType& rhs) const;
		/// increase capacity to fit N more elements into the queue
		void Reserve( SizeType num );
		/// returns number of elements in the queue
		SizeType Size() const;
		/// return true if queue is empty
		bool IsEmpty() const;
		/// remove all elements from the queue
		void Clear();
		/// return true if queue contains element
		bool Contains( ConstReference e ) const;

		/// add element to the back of the queue
		void Enqueue( ConstReference e );
		/// remove the element from the front of the queue
		ValueType Dequeue();
		/// access to element at front of queue without removing it
		Reference Peek() const;

	protected:
		STL::Array<TYPE> queueArray;
	};

	template<typename TYPE> 
	__IME_INLINED Queue<TYPE>::Queue()
	{
		/// Empty
	}

	template<typename TYPE> 
	__IME_INLINED Queue<TYPE>::Queue( const ThisType& rhs )
	{
		this->queueArray = rhs.queueArray;
	}

	template<typename TYPE> 
	__IME_INLINED void Queue<TYPE>::operator=(const ThisType& rhs)
	{
		this->queueArray = rhs.queueArray;
	}

	template<typename TYPE> 
	__IME_INLINED typename Queue<TYPE>::Reference Queue<TYPE>::operator[]( SizeType index ) const
	{
		return this->queueArray[index];
	}

	template<typename TYPE> 
	__IME_INLINED bool Queue<TYPE>::operator==(const ThisType& rhs) const
	{
		return this->queueArray == rhs.queueArray;
	}

	template<typename TYPE> 
	__IME_INLINED bool Queue<TYPE>::operator!=(const ThisType& rhs) const
	{
		return this->queueArray != rhs.queueArray;
	}

	template<typename TYPE> 
	__IME_INLINED bool Queue<TYPE>::Contains( ConstReference& e ) const
	{
		return (InvalidIndex != this->queueArray.FindIndex( e ));
	}

	template<typename TYPE> 
	__IME_INLINED void Queue<TYPE>::Clear()
	{
		this->queueArray.Clear();
	}

	template<typename TYPE> 
	__IME_INLINED void Queue<TYPE>::Reserve( SizeType num )
	{
		this->queueArray.Reserve( num );
	}

	template<typename TYPE> 
	__IME_INLINED typename Queue<TYPE>::SizeType Queue<TYPE>::Size() const
	{
		return this->queueArray.Size();
	}

	template<typename TYPE> 
	__IME_INLINED bool Queue<TYPE>::IsEmpty() const
	{
		return this->queueArray.IsEmpty();
	}

	template<typename TYPE> 
	__IME_INLINED void Queue<TYPE>::Enqueue( ConstReference& e )
	{
		this->queueArray.Append( e );
	}

	template<typename TYPE> 
	__IME_INLINED Queue<TYPE>::ValueType Queue<TYPE>::Dequeue()
	{
		TYPE e = this->queueArray.Front();
		this->queueArray.EraseIndex( 0 );
		return e;
	}

	template<typename TYPE> 
	__IME_INLINED typename Queue<TYPE>::Reference Queue<TYPE>::Peek() const
	{
		return this->queueArray.Front();
	}
}