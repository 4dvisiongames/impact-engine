/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __STLReverseIterator_h__
#define __STLReverseIterator_h__

/// Shared code
#include <Foundation/STLIteratorDefinition.h>

namespace STL
{
	template< typename Iter >
	class ReverseIterator : public IteratorDefinition< typename IteratorTraits< Iter >::ValueType, 
													   typename IteratorTraits< Iter >::DifferenceType,
													   typename IteratorTraits< Iter >::Pointer,
													   typename IteratorTraits< Iter >::Reference >
	{
	public:
		typedef Iter IteratorType;
		typedef typename IteratorTraits< Iter >::DifferenceType DifferenceType;
		typedef typename IteratorTraits< Iter >::Pointer Pointer;
		typedef typename IteratorTraits< Iter >::Reference Reference;

	public:
		ReverseIterator( void );
		explicit ReverseIterator( IteratorType iterType );
		ReverseIterator( const ReverseIterator& iterRef );

		template< typename Other >
		ReverseIterator( const ReverseIterator< Other >& otherIterRef );

		template< typename Other >
		ReverseIterator& operator = ( const ReverseIterator< Other >& rhs );
		Reference operator * ( void ) const;
		Pointer operator -> ( void ) const;

		ReverseIterator& operator ++ ( void );
		ReverseIterator& operator ++ ( int );
		ReverseIterator& operator -- ( void );
		ReverseIterator& operator -- ( int );

		ReverseIterator operator + ( DifferenceType diff );
		ReverseIterator& operator += ( DifferenceType diff ) const;
		ReverseIterator operator - ( DifferenceType diff );
		ReverseIterator& operator -= ( DifferenceType diff ) const;

		Reference operator[] ( DifferenceType pos ) const;

		IteratorType GetIterator( void ) const;

	protected:
		Iter myIterator;
	};

	// The C++ library working group has tentatively approved the usage of two
    // template parameters (IterLeft and IterRight) in order to allow reverse_iterators
    // and const_reverse iterators to be comparable. This is a similar issue to the 
    // C++ defect report #179 regarding comparison of container iterators and const_iterators.
	template< typename IterLeft, typename IterRight >
	__IME_INLINED bool operator == ( const ReverseIterator< IterLeft >& lhs, const ReverseIterator< IterRight >& rhs )
	{
		return lhs.GetIterator() == rhs.GetIterator();
	}

	template< typename IterLeft, typename IterRight >
	__IME_INLINED bool operator != ( const ReverseIterator< IterLeft >& lhs, const ReverseIterator< IterRight >& rhs )
	{
		return lhs.GetIterator() != rhs.GetIterator();
	}

	template< typename IterLeft, typename IterRight >
	__IME_INLINED bool operator > ( const ReverseIterator< IterLeft >& lhs, const ReverseIterator< IterRight >& rhs )
	{
		return lhs.GetIterator() > rhs.GetIterator();
	}

	template< typename IterLeft, typename IterRight >
	__IME_INLINED bool operator >= ( const ReverseIterator< IterLeft >& lhs, const ReverseIterator< IterRight >& rhs )
	{
		return lhs.GetIterator() >= rhs.GetIterator();
	}

	template< typename IterLeft, typename IterRight >
	__IME_INLINED bool operator < ( const ReverseIterator< IterLeft >& lhs, const ReverseIterator< IterRight >& rhs )
	{
		return lhs.GetIterator() < rhs.GetIterator();
	}

	template< typename IterLeft, typename IterRight >
	__IME_INLINED bool operator <= ( const ReverseIterator< IterLeft >& lhs, const ReverseIterator< IterRight >& rhs )
	{
		return lhs.GetIterator() <= rhs.GetIterator();
	}

	template< typename IterLeft, typename IterRight >
	__IME_INLINED typename ReverseIterator< IterLeft >::DifferenceType operator - ( const ReverseIterator< IterLeft >& lhs, const ReverseIterator< IterRight >& rhs )
	{
		return rhs.GetIterator() - lhs.GetIterator();
	}

	template< typename IterLeft, typename IterRight >
	__IME_INLINED typename ReverseIterator< IterLeft >::DifferenceType operator - ( typename ReverseIterator< IterLeft >::DifferenceType n, const ReverseIterator< IterRight >& lhs )
	{
		return ReverseIterator< IterLeft >( lhs.GetIterator() - n );
	}


	/// Implementation of ReverseIterator template
	
	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >::ReverseIterator( void ) : myIterator()
	{
		/// Empty
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >::ReverseIterator( IteratorType iterType ) : myIterator( iterType )
	{
		/// Empty
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >::ReverseIterator( const ReverseIterator& iterRef ) : myIterator( iterRef.myIterator )
	{
		/// Empty
	}

	template< typename Iter >
	template< typename Other >
	__IME_INLINED ReverseIterator< Iter >::ReverseIterator( const ReverseIterator< Other >& otherIterRef ) : myIterator( otherIterRef.GetIterator() )
	{
		/// Empty
	}

	template< typename Iter >
	template< typename Other >
	__IME_INLINED ReverseIterator< Iter >& ReverseIterator< Iter >::operator=( const ReverseIterator< Other >& rhs )
	{
		this->myIterator = rhs.GetIterator();
		return *this;
	}

	template< typename Iter >
	__IME_INLINED typename ReverseIterator< Iter >::Reference ReverseIterator< Iter >::operator*( void ) const
	{
		IteratorType it( this->myIterator );
		return *--it;
	}

	template< typename Iter >
	__IME_INLINED typename ReverseIterator< Iter >::Pointer ReverseIterator< Iter >::operator->( void ) const
	{
		return &(this->operator*());
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >& ReverseIterator< Iter >::operator++( void )
	{
		--this->myIterator;
		return *this;
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >& ReverseIterator< Iter >::operator++( int )
	{
		ReverseIterator< Iter > result( *this );
		--this->myIterator;
		return result;
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >& ReverseIterator< Iter >::operator--( void )
	{
		++this->myIterator;
		return *this;
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >& ReverseIterator< Iter >::operator--( int )
	{
		ReverseIterator< Iter > result( *this );
		++this->myIterator;
		return result;
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter > ReverseIterator< Iter >::operator+( DifferenceType diff )
	{
		return ReverseIterator< Iter >( this->myIterator - diff );
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >& ReverseIterator< Iter >::operator+=( DifferenceType diff ) const
	{
		this->myIterator -= diff;
		return *this;
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter > ReverseIterator< Iter >::operator-( DifferenceType diff )
	{
		return ReverseIterator< Iter >( this->myIterator + diff );
	}

	template< typename Iter >
	__IME_INLINED ReverseIterator< Iter >& ReverseIterator< Iter >::operator-=( DifferenceType diff ) const
	{
		this->myIterator += diff;
		return *this;
	}

	template< typename Iter >
	__IME_INLINED typename ReverseIterator< Iter >::Reference ReverseIterator< Iter >::operator[]( DifferenceType pos ) const
	{
		return this->myIterator[ -pos - 1 ];
	}

	template< typename Iter >
	__IME_INLINED typename ReverseIterator< Iter >::IteratorType ReverseIterator< Iter >::GetIterator( void ) const
	{
		return this->myIterator;
	}
}

#endif /// __STLReverseIterator_h__