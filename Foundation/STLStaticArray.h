//============= (C) Copyright 2010, PARANOIA Team. All rights reserved. =============
/// Desc: NanoEngine's static array class. This class is also used by most other
///       collection classes.
///
/// Details: The default constructor will not pre-allocate elements, so no space
///			 is wasted as long as no elements are added. As soon as the first element
///			 is added to the array, an initial buffer of 16 elements is created.
///			 Whenever the element buffer would overflow, a new buffer of twice
///			 the size of the previous buffer is created and the existing elements
///			 are then copied over to the new buffer. The element buffer will
///			 never shrink, the only way to reclaim unused memory is to
///			 copy the StaticArray to a new StaticArray object. This is usually not a problem
///			 since most arrays will oscillate around some specific size, so once
///			 the array has reached this specific size, no costly memory free or allocs
///			 will be performed.
///
/// Tip: It is possible to sort the array using the Sort() method, this uses
///		 std::sort (one of the very few exceptions where the STL is used in
///      NanoEngine source code).
///
/// Warning: One should generally be careful with costly copy operators, the StaticArray
///		 	 class (and the other container classes using StaticArray) may do some heavy
///			 element shuffling in some situations (especially when sorting and erasing
///			 elements).
///
/// Notes: Generaly uses as direct replacement for std::vector functionality.
///
/// TODO: May be it's better to replace std::sort with custom sorter? Like
///       radix sorter or other ones. And not forget to make it thread-safe!
///
/// Author: Pavel Umnikov
//====================================================================================

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _STLArray_h_
#define _STLArray_h_

/// Engine shared code
#include <Foundation/Shared.h>
/// STL algorithms
#include <Foundation/STLAlgorithm.h>

/// Standard template library
namespace STL
{
	/// <summary>
	/// <c>STL::StaticArray</c> is a static array implementation like std::array< T, size >.
	/// </summary>
	template<class TYPE, SizeT ArrSize> class StaticArray
	{
	public:
		/// define iterator
		typedef TYPE* Iterator;

		/// <summary cref="StaticArray::ComparatorFunc">
		/// Comparator function
		/// </summary>
		typedef bool (*ComparatorFunc)(const TYPE& first, const TYPE& last);

		/// <summary cref="StaticArray::ComparatorFunc">
		/// Sorting function
		/// </summary>
		typedef void (*SortingFunc)(ComparatorFunc comp_fun, TYPE* first_iter, TYPE* second_iter);

		/// <summary cref="StaticArray::StaticArray">
		/// Constructor with default parameters.
		/// </summary>
		StaticArray();

		/// <summary cref="StaticArray::StaticArray">
		/// Constructor with initial size, grow size and initial values.
		/// </summary>
		/// <param name="initialValue">Initial(first) value.</param>
		StaticArray( const TYPE& initialValue );

		/// <summary cref="StaticArray::StaticArray">
		/// Copy constructor.
		/// </summary>
		/// <param name="rhs">A reference to the StaticArray.</param>
		StaticArray(const StaticArray<TYPE>& rhs);

		/// <summary cref="StaticArray::~StaticArray">
		/// Destructor.
		/// </summary>
		~StaticArray();

		/// <summary cref="StaticArray::operator=">
		/// Assignment operator.
		/// </summary>
		/// <param name="rhs">A reference to the StaticArray.</param>
		void operator=(const StaticArray<TYPE>& rhs);

		/// <summary cref="StaticArray::operator[]">
		/// Read-only access an element from current StaticArray.
		/// </summary>
		/// <param name="index">Index of element in current StaticArray.</param>
		/// <remarks>
		/// This method will NOT grow the array, and instead do a range check, which may throw an assertion.
		/// </remarks>
		/// <returns>Contents from selected index, exception if index is bigger than elements count.</returns>
		TYPE& operator[](IndexT index) const;

		/// <summary cref="StaticArray::operator==">
		/// The equality operator returns true if all elements are identical. The
		/// TYPE class must support the equality operator.
		/// </summary>
		/// <param name="rhs">A reference to the StaticArray.</param>
		/// <remarks>
		/// You must provide 'operator==' definition in class that puts into container
		/// otherwise compiler error.
		/// </remarks>
		/// <returns>True if equals, otherwise false.</returns>
		bool operator==(const StaticArray<TYPE>& rhs) const;

		/// <summary cref="StaticArray::operator!=">
		/// The inequality operator returns true if at least one element in the
		/// array is different, or the array sizes are different.
		/// </summary>
		bool operator!=(const StaticArray<TYPE>& rhs) const;
		/// convert to "anything"
		template<typename T> T As() const;

		/// append element to end of array
		IndexT Append(const TYPE& elm);
		/// append the contents of an array to this array
		void AppendArray(const StaticArray<TYPE>& rhs);

		/// This increases the capacity to make room for N elements. If the
		/// number of elements is known before appending the elements, this
		/// method can be used to prevent reallocation. If there is already
		/// enough room for N more elements, nothing will happen.
		/// NOTE: the functionality of this method has been changed as of 26-Apr-10,
		/// it will now only change the capacity of the array, not its size.
		void Reserve(SizeT num);
		/// get number of elements in array
		SizeT Size() const;
		/// get overall allocated size of array in number of elements
		SizeT Capacity() const;
		/// return reference to first element
		TYPE& Front() const;
		/// return reference to last element
		TYPE& Back() const;
		/// return true if array empty
		bool IsEmpty() const;
		/// erase element at index, keep sorting intact
		void EraseIndex(IndexT index);
		/// erase element pointed to by iterator, keep sorting intact
		Iterator Erase(Iterator iter);

		/// Erase element at index, fill gap by swapping in last element, destroys sorting!
		/// NOTE: this method is fast but destroys the sorting order!
		void EraseIndexSwap(IndexT index);

		/// Erase element at iterator, fill gap by swapping in last element, destroys sorting!
		/// NOTE: this method is fast but destroys the sorting order!
		Iterator EraseSwap(Iterator iter);

		/// Insert element before element at index
		void Insert(IndexT index, const TYPE& elm);

		/// This inserts the element into a sorted array. Returns the index
		/// at which the element was inserted.
		IndexT InsertSorted(const TYPE& elm);

		/// This inserts an element at the end of a range of identical elements
		/// starting at a given index. Performance is O(n). Returns the index
		/// at which the element was added.
		IndexT InsertAtEndOfIdenticalRange(IndexT startIndex, const TYPE& elm);

		/// This tests, whether the array is sorted. This is a slow operation,
		/// function complexity is O(n).
		bool IsSorted() const;

		/// Clear array (calls destructors)
		/// NOTE: The current implementation of this method does not shrink the
		/// preallocated space. It simply sets the array size to 0.
		void Clear();

		/// Reset array (does NOT call destructors)
		/// NOTE: This is identical with Clear(), but does NOT call destructors (it just
		/// resets the size member. USE WITH CARE!
		void Reset();

		/// Return iterator to beginning of array
		Iterator Begin() const;

		/// Return iterator to end of array
		Iterator End() const;

		/// Find element in array, return iterator, or 0 if element not
		/// found.
		Iterator Find(const TYPE& elm) const;

		/// Find element in array, return element index, or InvalidIndex if element not
		/// found.
		IndexT FindIndex(const TYPE& elm) const;

		/// Fills an array range with the given element value. Will grow the
		/// array if necessary
		void Fill(IndexT first, SizeT num, const TYPE& elm);
		/// clear contents and preallocate with new attributes
		void Realloc(SizeT capacity, SizeT grow);

		/// Returns a new array with all element which are in rhs, but not in this.
		/// Carefull, this method may be very slow with large arrays!
		/// TODO: This method is broken, check test case to see why!
		StaticArray<TYPE> Difference(const StaticArray<TYPE>& rhs);

		/// Sorts the array. 
		/// This just calls the STL sort algorithm.
		void Sort(SortingFunc sorting_function = HeapSortingAlgorithm, ComparatorFunc compare_function = CompareLess);

		/// Do a binary search, requires a sorted array
		IndexT BinarySearchIndex(const TYPE& elm) const;

	private:
		/// destroy an element (call destructor without freeing memory)
		void Destroy(TYPE* elm);
		/// copy content
		void Copy(const StaticArray<TYPE>& src);
		/// delete content
		void Delete();
		/// grow array
		void Grow();
		/// grow array to target size
		void GrowTo(SizeT newCapacity);
		/// move elements, grows array if needed
		void Move(IndexT fromIndex, IndexT toIndex);

		enum { capacity = ArrSize };	// number of elements allocated
		SizeT size;						// number of elements in array
		TYPE elements[capacity];		// pointer to element array
	};

	template<class TYPE> StaticArray<TYPE>::StaticArray() : grow(8), capacity(0), size(0), elements(0)
	{
		/// Empty
	}

	template<class TYPE> StaticArray<TYPE>::StaticArray(SizeT _capacity, SizeT _grow) : grow(_grow), capacity(_capacity), size(0) 
	{
		if (0 == this->grow)
		{
			this->grow = 16;
		}
		if (this->capacity > 0)
		{
			this->elements = new TYPE[this->capacity];
		}
		else
		{
			this->elements = 0;
		}
	}

	template<class TYPE> StaticArray<TYPE>::StaticArray(SizeT initialSize, SizeT _grow, const TYPE& initialValue) : grow(_grow), capacity(initialSize), size(initialSize)
	{
		if (0 == this->grow)
		{
			this->grow = 16;
		}
		if (initialSize > 0)
		{
			this->elements = new TYPE[this->capacity];
			IndexT i;
			for (i = 0; i < initialSize; i++)
			{
				this->elements[i] = initialValue;
			}
		}
		else
		{
			this->elements = 0;
		}
	}

	template<class TYPE> void StaticArray<TYPE>::Copy(const StaticArray<TYPE>& src)
	{
#if NDKDebug == NDKDebugEngine
		NDKAssert( NULL == this->elements );
#endif

		this->grow = src.grow;
		this->capacity = src.capacity;
		this->size = src.size;
		if (this->capacity > 0)
		{
			this->elements = new TYPE[this->capacity];
			IndexT i;
			for (i = 0; i < this->size; i++)
			{
				this->elements[i] = src.elements[i];
			}
		}
	}

	template<class TYPE> void StaticArray<TYPE>::Delete( void )
	{
		this->grow = 0;
		this->capacity = 0;
		this->size = 0;
		if (this->elements)
		{
			delete[] this->elements;
			this->elements = 0;
		}
	}

	template<class TYPE> void StaticArray<TYPE>::Destroy( TYPE* elm )
	{
		elm->~TYPE();
	}

	template<class TYPE> StaticArray<TYPE>::StaticArray( const StaticArray<TYPE>& rhs ) : grow( NULL ), capacity( NULL ), size( NULL ), elements( NULL )
	{
		/// Just do copy of contents. See StaticArray::Copy function for details.
		this->Copy( rhs );
	}

	template<class TYPE> StaticArray<TYPE>::~StaticArray()
	{
		/// Delete everything that array has inside. See StaticArray::Delete for details;
		this->Delete();
	}

	template<class TYPE> void StaticArray<TYPE>::Realloc(SizeT _capacity, SizeT _grow)
	{
		/// Codepoet: We are not using C-style reallocation with functions like
		/// Realloc, aligned_realloc, etc, with internal StaticArray Delete fucntion and
		/// new that we already defined here. 
		this->Delete();
		this->grow = _grow;
		this->capacity = _capacity;
		this->size = 0;

		if (this->capacity > 0)
			this->elements = new TYPE[this->capacity];
		else
			this->elements = 0;
	}

	template<class TYPE> void StaticArray<TYPE>::operator = ( const StaticArray<TYPE>& rhs )
	{
		if (this != &rhs)
		{
			if ( ( this->capacity > 0 ) && ( rhs.size <= this->capacity ) )
			{
				// source array fits into our capacity, copy in place
				NDKAssert( NULL != this->elements );
				IndexT i;
				for ( i = 0; i < rhs.size; i++ )
					this->elements[i] = rhs.elements[i];

				// properly destroy remaining original elements
				for ( ; i < this->size; i++ )
					this->Destroy( &(this->elements[i]) );

				/// Define all array corresponding variables
				this->grow = rhs.grow;
				this->size = rhs.size;
			}
			else
			{
				// source array doesn't fit into our capacity, need to reallocate
				this->Delete();
				this->Copy(rhs);
			}
		}
	}

	template<class TYPE> void StaticArray<TYPE>::GrowTo( SizeT newCapacity )
	{
		/// Create new instance of array with capacity
		TYPE* newArray = new TYPE[newCapacity];
		/// If already have element
		if ( this->elements )
		{
			/// Copy over contents
			IndexT i;
			for (i = 0; i < this->size; i++)
			{
				newArray[i] = this->elements[i];
			}

			/// Discard old array and update contents
			delete[] this->elements;
		}

		/// Represent array
		this->elements  = newArray;
		this->capacity = newCapacity;
	}

	template<class TYPE> void StaticArray<TYPE>::Grow( void )
	{
#if NDKDebug == NDKDebugEngine
		NDKAssert( this->grow > 0 );
#endif

		SizeT growToSize;
		if ( NULL == this->capacity )
			growToSize = this->grow;
		else
		{
			// grow by half of the current capacity, but never more then MaxGrowSize
			SizeT growBy = this->capacity >> 1;

			if (growBy == 0)
				growBy = MinGrowSize;
			else if (growBy > MaxGrowSize)
				growBy = MaxGrowSize;

			/// Setup real grow size
			growToSize = this->capacity + growBy;
		}
		/// Grow to calculated size
		this->GrowTo(growToSize);
	}

	/**
		30-Jan-10   codepoet    serious bugfixes!
		07-Nov-11	codepoet	bugfix: neededSize >= this->capacity => neededSize > capacity
	*/
	template<class TYPE> void StaticArray<TYPE>::Move(IndexT fromIndex, IndexT toIndex)
	{
#if NDKDebug == NDKDebugEngine
		NDKAssert(this->elements);
		NDKAssert(fromIndex < this->size);
#endif

		// nothing to move?
		if (fromIndex == toIndex)
		{
			return;
		}

		// compute number of elements to move
		SizeT num = this->size - fromIndex;

		// check if array needs to grow
		SizeT neededSize = toIndex + num;
		while (neededSize > this->capacity)
		{
			this->Grow();
		}

		if (fromIndex > toIndex)
		{
			// this is a backward move
			IndexT i;
			for (i = 0; i < num; i++)
			{
				this->elements[toIndex + i] = this->elements[fromIndex + i];
			}

			// destroy remaining elements
			for (i = (fromIndex + i) - 1; i < this->size; i++)
			{
				this->Destroy(&(this->elements[i]));
			}
		}
		else
		{
			// this is a forward move
			IndexT i;  // NOTE: this must remain signed for the following loop to work!!!
			for (i = num - 1; i >= 0; --i)
			{
				this->elements[toIndex + i] = this->elements[fromIndex + i];
			}

			// destroy freed elements
			for (i = int(fromIndex); i < int(toIndex); i++)
			{
				this->Destroy(&(this->elements[i]));
			}
		}

		// adjust array size
		this->size = toIndex + num;
	}

	template<class TYPE> IndexT StaticArray<TYPE>::Append(const TYPE& elm)
	{
		// grow allocated space if exhausted
		if (this->size == this->capacity)
		{
			this->Grow();
		}
#if defined( DEBUG )
		NDKAssert(this->elements);
#endif
		IndexT currentIdx = this->size;
		this->elements[currentIdx] = elm;
		this->size++;
		return currentIdx;
	}

	template<class TYPE> void StaticArray<TYPE>::AppendArray(const StaticArray<TYPE>& rhs)
	{
		IndexT i;
		SizeT num = rhs.Size();
		for (i = 0; i < num; i++)
		{
			this->Append(rhs[i]);
		}
	}

	/**
		26-Apr-2011		codepoet	changes the capacity of the array, not its size.
	*/
	template<class TYPE> void StaticArray<TYPE>::Reserve(SizeT num)
	{
#if defined( DEBUG )
		NDKAssert(num > 0);
#endif
		SizeT neededCapacity = this->size + num;
		if (neededCapacity > this->capacity)
		{
			this->GrowTo(neededCapacity);
		}
	}

	template<class TYPE> SizeT StaticArray<TYPE>::Size() const
	{
		return this->size;
	}

	template<class TYPE> SizeT StaticArray<TYPE>::Capacity() const
	{
		return this->capacity;
	}

	template<class TYPE> TYPE& StaticArray<TYPE>::operator[](IndexT index) const
	{
#if defined( DEBUG )
		NDKAssert(this->elements && (index < this->size));
#endif
		return this->elements[index];
	}

	template<class TYPE> bool StaticArray<TYPE>::operator==(const StaticArray<TYPE>& rhs) const
	{
		if (rhs.Size() == this->Size())
		{
			IndexT i;
			SizeT num = this->Size();
			for (i = 0; i < num; i++)
			{
				if (!(this->elements[i] == rhs.elements[i]))
				{
					return false;
				}
			}
			return true;
		}
		else
		{
			return false;
		}
	}

	template<class TYPE> bool StaticArray<TYPE>::operator!=(const StaticArray<TYPE>& rhs) const
	{
		return !(*this == rhs);
	}

	template<class TYPE> TYPE& StaticArray<TYPE>::Front() const
	{
	#if _DEBUG
		NDKAssert(this->elements && (this->size > 0));
	#endif
		return this->elements[0];
	}

	template<class TYPE> TYPE& StaticArray<TYPE>::Back() const
	{
#if defined( DEBUG )
		NDKAssert(this->elements && (this->size > 0));
#endif
		return this->elements[this->size - 1];
	}

	template<class TYPE> bool StaticArray<TYPE>::IsEmpty() const
	{
		return (this->size == 0);
	}

	template<class TYPE> void StaticArray<TYPE>::EraseIndex(IndexT index)
	{
#if defined( DEBUG )
		NDKAssert(this->elements && (index < this->size));
#endif
		if (index == (this->size - 1))
		{
			// special case: last element
			this->Destroy(&(this->elements[index]));
			this->size--;
		}
		else
		{
			this->Move(index + 1, index);
		}
	}

	template<class TYPE> void StaticArray<TYPE>::EraseIndexSwap(IndexT index)
	{
#if defined( DEBUG )
		NDKAssert(this->elements && (index < this->size));
#endif

		// swap with last element, and destroy last element
		IndexT lastElementIndex = this->size - 1;
		if (index < lastElementIndex)
		{
			this->elements[index] = this->elements[lastElementIndex];
		}
		this->Destroy(&(this->elements[lastElementIndex]));
		this->size--;
	}

	template<class TYPE> typename StaticArray<TYPE>::Iterator StaticArray<TYPE>::Erase(typename StaticArray<TYPE>::Iterator iter)
	{
#if defined( DEBUG )
		NDKAssert(this->elements && (iter >= this->elements) && (iter < (this->elements + this->size)));
#endif
		this->EraseIndex(IndexT(iter - this->elements));
		return iter;
	}

	template<class TYPE> typename StaticArray<TYPE>::Iterator StaticArray<TYPE>::EraseSwap(typename StaticArray<TYPE>::Iterator iter)
	{
#if defined( DEBUG )
		NDKAssert(this->elements && (iter >= this->elements) && (iter < (this->elements + this->size)));
#endif
		this->EraseSwapIndex(IndexT(iter - this->elements));
		return iter;
	}

	template<class TYPE> void StaticArray<TYPE>::Insert(IndexT index, const TYPE& elm)
	{
#if defined( DEBUG )
		NDKAssert(index <= this->size);
#endif
		if (index == this->size)
		{
			// special case: append element to back
			this->Append(elm);
		}
		else
		{
			this->Move(index, index + 1);
			this->elements[index] = elm;
		}
	}

	template<class TYPE> void StaticArray<TYPE>::Clear()
	{
		IndexT i;
		for (i = 0; i < this->size; i++)
		{
			this->Destroy(&(this->elements[i]));
		}
		this->size = 0;
	}

	template<class TYPE> void StaticArray<TYPE>::Reset()
	{
		this->size = 0;
	}

	template<class TYPE> typename StaticArray<TYPE>::Iterator StaticArray<TYPE>::Begin() const
	{
		return this->elements;
	}

	template<class TYPE> typename StaticArray<TYPE>::Iterator StaticArray<TYPE>::End() const
	{
		return this->elements + this->size;
	}

	template<class TYPE> typename StaticArray<TYPE>::Iterator StaticArray<TYPE>::Find(const TYPE& elm) const
	{
		IndexT index;
		for (index = 0; index < this->size; index++) {
			if (this->elements[index] == elm) {
				return &(this->elements[index]);
			}
		}
		return 0;
	}

	template<class TYPE> IndexT StaticArray<TYPE>::FindIndex(const TYPE& elm) const
	{
		IndexT index;
		for (index = 0; index < this->size; index++)
		{
			if (this->elements[index] == elm)
			{
				return index;
			}
		}
		return InvalidIndex;
	}

	template<class TYPE> void StaticArray<TYPE>::Fill(IndexT first, SizeT num, const TYPE& elm)
	{
		if ((first + num) > this->size)
		{
			this->GrowTo(first + num);
		}
		IndexT i;
		for (i = first; i < (first + num); i++)
		{
			this->elements[i] = elm;
		}
	}

	template<class TYPE> StaticArray<TYPE> StaticArray<TYPE>::Difference(const StaticArray<TYPE>& rhs)
	{
		StaticArray<TYPE> diff;
		IndexT i;
		SizeT num = rhs.Size();
		for (i = 0; i < num; i++)
		{
			if (0 == this->Find(rhs[i]))
			{
				diff.Append(rhs[i]);
			}
		}
		return diff;
	}

	template<class TYPE> void StaticArray<TYPE>::Sort(SortingFunc sorting_function, ComparatorFunc compare_function)
	{
		sorting_function( compare_function, this->Begin(), this->End() );
	}

	template<class TYPE> IndexT StaticArray<TYPE>::BinarySearchIndex(const TYPE& elm) const
	{
		SizeT num = this->Size();
		if (num > 0)
		{
			IndexT half;
			IndexT lo = 0;
			IndexT hi = num - 1;
			IndexT mid;
			while (lo <= hi)
			{
				if (0 != (half = num/2))
				{
					mid = lo + ((num & 1) ? half : (half - 1));
					if (elm < this->elements[mid])
					{
						hi = mid - 1;
						num = num & 1 ? half : half - 1;
					}
					else if (elm > this->elements[mid])
					{
						lo = mid + 1;
						num = half;
					}
					else
					{
						return mid;
					}
				}
				else if (0 != num)
				{
					if (elm != this->elements[lo])
					{
						return InvalidIndex;
					}
					else
					{
						return lo;
					}
				}
				else
				{
					break;
				}
			}
		}
		return InvalidIndex;
	}

	template<class TYPE> bool StaticArray<TYPE>::IsSorted() const
	{
		if (this->size > 1)
		{
			IndexT i;
			for (i = 0; i < this->size - 1; i++)
			{
				if (this->elements[i] > this->elements[i + 1])
				{
					return false;
				}
			}
		}
		return true;
	}

	template<class TYPE> IndexT StaticArray<TYPE>::InsertAtEndOfIdenticalRange(IndexT startIndex, const TYPE& elm)
	{
		IndexT i = startIndex + 1;
		for (; i < this->size; i++)
		{
			if (this->elements[i] != elm)
			{
				this->Insert(i, elm);
				return i;
			}
		}

		// fallthrough: new element needs to be appended to end
		this->Append(elm);
		return (this->Size() - 1);
	}

	template<class TYPE> IndexT StaticArray<TYPE>::InsertSorted(const TYPE& elm)
	{
		SizeT num = this->Size();
		if (num == 0)
		{
			// array is currently empty
			this->Append(elm);
			return this->Size() - 1;
		}
		else
		{
			IndexT half;
			IndexT lo = 0;
			IndexT hi = num - 1;
			IndexT mid;
			while (lo <= hi)
			{
				if (0 != (half = num/2))
				{
					mid = lo + ((num & 1) ? half : (half - 1));
					if (elm < this->elements[mid])
					{
						hi = mid - 1;
						num = num & 1 ? half : half - 1;
					}
					else if (elm > this->elements[mid])
					{
						lo = mid + 1;
						num = half;
					}
					else
					{
						// element already exists at [mid], append the
						// new element to the end of the range
						return this->InsertAtEndOfIdenticalRange(mid, elm);
					}
				}
				else if (0 != num)
				{
					if (elm < this->elements[lo])
					{
						this->Insert(lo, elm);
						return lo;
					}
					else if (elm > this->elements[lo])
					{
						this->Insert(lo + 1, elm);
						return lo + 1;
					}
					else
					{
						// element already exists at [low], append
						// the new element to the end of the range
						return this->InsertAtEndOfIdenticalRange(lo, elm);
					}
				}
				else
				{
#if NDKDebug == NDKDebugEngine
					NDKAssert(0 == lo);
#endif
					this->Insert(lo, elm);
					return lo;
				}
			}
			if (elm < this->elements[lo])
			{
				this->Insert(lo, elm);
				return lo;
			}
			else if (elm > this->elements[lo])
			{
				this->Insert(lo + 1, elm);
				return lo + 1;
			}
			else
			{
				// can't happen(?)
			}
		}
		/// Can't happen
		return InvalidIndex;
	}
} /// namespace STL

#endif /// _STLArray_h_
