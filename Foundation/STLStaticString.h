/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__STLStaticString_h__
#define __Foundation__STLStaticString_h__

/// Reverse iterator
#include <Foundation/STLReverseIterator.h>
/// Mathematics
#include <Foundation/Math.h>
/// Type traits
#include <Foundation/TypeTraits.h>
#include <Foundation/STLStringTraits.h>

namespace STL
{
	template< typename T, SizeT Length >
	class StaticString
	{
	public:
		typedef StaticString< T, Length > ThisType;
		typedef T ValueType;
		typedef T* Pointer;
		typedef const T* ConstPointer;
		typedef T& Reference;
		typedef const T& ConstReference;
		typedef T* Iterator;
		typedef const T* ConstIterator;
		typedef STL::ReverseIterator< Iterator > ReverseIterator;
		typedef STL::ReverseIterator< ConstIterator > ConstReverseIterator;
		typedef USizeT SizeType;
		typedef SSizeT DifferenceType;
		typedef StringTraits< T > TraitsType;

		static const SizeType theNPos = (size_t)-1;
		static const SizeType theMaxSize = (size_t)-2;
		static const SizeType theMinimalSize = 8;
		static const SizeType theTypeAlignment = __IME_ALIGN_OF(ValueType);
		static const SizeType theTypeAlignmentOffset = 0;

		StaticString();
		StaticString( const ThisType& rhs, SizeType position, SizeType num = theNPos );
		StaticString( ConstPointer str, SizeType num );
		StaticString( ConstPointer str );
		StaticString( const ThisType& rhs );
		StaticString( ConstPointer begin, ConstPointer end );
		StaticString( Traits::CtorNotInitialized, SizeType num );
		StaticString( Traits::CtorHasVA, const ValueType* toFormat, ... );
		~StaticString() { }

		/// assignment operators

		ThisType& operator = (const ThisType& rhs);
		ThisType& operator = (ConstPointer cStr);
		ThisType& operator = (ValueType c);

		/// swap

		void Swap(ThisType& rhs);

		ConstPointer AsCharPtr() const;

	private:
		enum { TheSize = Length };
		ValueType myStaticString[TheSize];
		Pointer myBeginIter;
		Pointer myEndIter;
	};

	template< typename T, SizeT Length >
	StaticString< T, Length >::StaticString( ConstPointer str )
		: myBeginIter(&myStaticString[0])
	{
		/// TODO: move these into initialization method

		SizeT strSize = STL::StringTraits<ValueType>::StringLength( str );
		if ( strSize > TheSize )
			strSize = TheSize;

		Memory::LowLevelMemory::CopyMemoryBlock( str, this->myBeginIter, strSize );
	}

	template< typename T, SizeT Length >
	__IME_INLINED typename StaticString< T, Length >::ConstPointer StaticString< T, Length >::AsCharPtr() const
	{
		return this->myBeginIter;
	}
}

#endif /// __Foundation__STLStaticString_h__