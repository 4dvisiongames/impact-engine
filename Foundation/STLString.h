/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Allocator
#include <Foundation/STLAllocator.h>
/// Array
#include <Foundation/STLArray.h>
/// Dictionary
#include <Foundation/STLDictionary.h>
/// Reverse iterator
#include <Foundation/STLReverseIterator.h>
/// Type traits
#include <Foundation/TypeTraits.h>
/// STL string traits
#include <Foundation/STLStringTraits.h>

/// Standard template library namespace
namespace STL
{
	/// <summary>
	/// <c>String</c> is a string provider class, much like the C++ std::basic_string.
	/// The primary distinctions between String and std::basic_string are:
	///    - <c>String</c> has a few extension functions that allow for increased performance.
	///    - <c>String</c> has a few extension functions that make use easier, such as a member sprintf function and
	///		  member tolower/toupper functions.
	///    - <c>String</c> supports debug memory naming natively.
	///    - <c>String</c> is easier to read, debug, and visualize.
	///    - <c>String</c> internally manually expands basic functions such as Begin(), Size(), etc. in order to
	///		 improve debug performance and optimizer success.
	///    - <c>String</c> is savvy to an environment that doesn't have exception handling, as is sometimes the case 
	///		 with console or embedded environments.
	///    - <c>String</c> has less deeply nested function calls and allows the user to enable forced inlining in 
	///		 debug builds in order to reduce bloat.
	///    - <c>String</c> doesn't use char traits. As a result, STL assumes that strings will hold characters and not
	///		 exotic things like widgets. At the very least, basic_string assumes that the value_type is a POD.
	///    - <c>String</c>::SizeType is defined as SizeT instead of size_t in order to save memory and run faster on 
	///		 64 bit systems.
	///    - <c>String</c> data is guaranteed to be contiguous.
	///    - <c>String</c> data is guaranteed to be 0-terminated, and the AsCharPtr() function is guaranteed to return
	///		 the same pointer as the Data() which is guaranteed to be the same value as &string[0].
	///    - <c>String</c> has a SetCapacity() function which frees excess capacity. The only way to do this with
	///		 std::basic_string is via the cryptic non-obvious trick of using: basic_string<char>(x).swap(x);
	///    - <c>String</c> has a force_size() function, which unilaterally moves the string end position (mpEnd) to the
	///		 given location. Useful for when the user writes into the string via some extenal means such as C strcpy or
	///		 sprintf.
	/// </summary>
	/// <remarks>
	/// Copy on Write (CoW)
	/// This string implementation does not do CoW. This is by design, as CoW penalizes 95% of string uses for the 
	/// benefit of only 5% of the uses (these percentages are qualitative, not quantitative). The primary benefit of 
	/// cow is that it allows for the sharing of string data between two string objects.
	/// Thus if you say this:
	///    String<Char8> a("hello");
	///    String<Char8> b(a);
	/// the "hello" will be shared between a and b. If you then say this:
	///    a = "world";
	/// then a will release its reference to "hello" and leave b with the only reference to it. Normally this 
	/// functionality is accomplished via reference counting and with atomic operations or mutexes.
	///
	/// The C++ standard does not say anything about basic_string and CoW. However, for a basic_string implementation
	/// to be standards-conforming, a number of issues arise which dictate some things about how one would have to
	/// implement a CoW string. The discussion of these issues will not be rehashed here, as you can read the 
	/// references below for better detail than can be provided in the space we have here. However, we can say that the
	/// C++ standard is sensible and that anything we try to do here to allow for an efficient cow implementation would
	/// result in a generally unacceptable string interface.
	///
	/// The disadvantages of COW strings are:
	///    - A reference count needs to exist with the string, which increases string memory usage.
	///    - With thread safety, atomic operations and mutex locks are expensive, especially on weaker memory systems
	///		 such as console gaming platforms.
	///    - All non-const string accessor functions need to do a sharing check the the first such check needs to
	///		 detach the string. Similarly, all string assignments need to do a sharing check as well. If you access the
	///		 string before doing an assignment, the assignment doesn't result in a shared string, because the string 
	///      has already been detached.
	///    - String sharing doesn't happen the large majority of the time. In some cases, the total sum of the reference
	///		 count memory can exceed any memory savings gained by the strings that share representations.  
	/// 
	/// The addition of a <c>StringCoW</c> class is under consideration for engine's STL library. There are conceivably
	/// some systems which have string usage patterns which would benefit from COW sharing. Such functionality is best
	/// saved for a separate string implementation so that the other string uses aren't penalized.
	/// 
	/// References:
	///    This is a good starting HTML reference on the topic:
	///       http://www.gotw.ca/publications/optimizations.htm
	///    Here is a Usenet discussion on the topic:String< T >
	///       http://groups-beta.google.com/group/comp.lang.c++.moderated/browse_thread/thread/3dc6af5198d0bf7/886c8642cb06e03d
	///
	/// As of this writing, an insert of a string into itself necessarily triggers a reallocation, even if there is 
	/// enough capacity in self to handle the increase in size. This is due to the slightly tricky nature of the
	/// operation of modifying one's self with one's self, and thus the source and destination are being modified
	/// during the operation. It might be useful to rectify this to the extent possible.
	/// </remarks>
	template< typename T >
	class String
	{
	public:
		typedef String< T > ThisType;
		typedef T ValueType;
		typedef T* Pointer;
		typedef const T* ConstPointer;
		typedef T& Reference;
		typedef const T& ConstReference;
		typedef T* Iterator;
		typedef const T* ConstIterator;
		typedef STL::ReverseIterator< Iterator > ReverseIterator;
		typedef STL::ReverseIterator< ConstIterator > ConstReverseIterator;
		typedef USizeT SizeType;
		typedef SSizeT DifferenceType;
		typedef StringTraits< T > TraitsType;

		static const SizeType theNPos = (size_t)-1;
		static const SizeType theMaxSize = (size_t)-2;
		static const SizeType theMinimalSize = 8;
		static const SizeType theTypeAlignment = __IME_ALIGN_OF( ValueType );
		static const SizeType theTypeAlignmentOffset = 0;


		/// constructor
		String();
		String( const ThisType& rhs, SizeType position, SizeType num = theNPos );
		String( ConstPointer str, SizeType num );
		String( ConstPointer str );
		String( const ThisType& rhs );
		String( ConstPointer begin, const ConstPointer end );
		String( Traits::CtorNotInitialized, SizeType num );
		String( Traits::CtorHasVA, __IME_VERIFY_FORMAT_STRING const ValueType* toFormat, ... );

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
		String( ThisType&& rhs );
#endif

		~String( void );

		/// assignment operators

		ThisType& operator = (const ThisType& rhs);
		ThisType& operator = (ConstPointer cStr);
		ThisType& operator = (ValueType c);

		/// Swap 

		void Swap( ThisType& rhs );

		/// Appending operators

		ThisType& operator += ( const ThisType& rhs );
		ThisType& operator += ( const ValueType* a );
		ThisType& operator += ( ValueType c );
		ThisType& operator += ( float a );
		ThisType& operator += ( int a );
		ThisType& operator += ( unsigned a );
		ThisType& operator += ( bool a );

		/// Manual assignment

		ThisType& Assign( const ThisType& x );
		ThisType& Assign( const ThisType& x, SizeType position, SizeType num );
		ThisType& Assign( const ValueType* cStr, SizeType num );
		ThisType& Assign( const ValueType* cStr );
		ThisType& Assign( SizeType num, ValueType c );
		ThisType& Assign( const ValueType* begin, const ValueType* end );

		/// Iterators

		Iterator Begin( void );
		ConstIterator Begin( void ) const;
		Iterator End( void );
		ConstIterator End( void ) const;
		typename ThisType::ReverseIterator ReverseBegin( void );
		typename ThisType::ConstReverseIterator ReverseBegin( void ) const;
		typename ThisType::ReverseIterator ReverseEnd( void );
		typename ThisType::ConstReverseIterator ReverseEnd( void ) const;

		/// Element access

		Reference operator[]( SizeType i );
		ConstReference operator[]( SizeType i ) const;
		Reference At( SizeType i );
		ConstReference At( SizeType i ) const;
		Reference Front( void );
		ConstReference Front( void ) const;
		Reference Back( void );
		ConstReference Back( void ) const;

		/// Size-related functions

		bool IsEmpty( void ) const;
		SizeType Size( void ) const;
		SizeType Length( void ) const;
		SizeType MaxSize( void ) const;
		SizeType MaxCapacity( void ) const;
		void Resize( SizeType num, ValueType val );
		void Resize( SizeType num );
		void Reserve( SizeType = 0 );
		void SetCapacity( SizeType num = ThisType::theNPos );
		void ForceSize( SizeType num );

		/// Raw access

		const ValueType* Data( void ) const;
		const ValueType* AsCharPtr( void ) const;

		/// Appending functions

		ThisType& Append( const ThisType& str );
		ThisType& Append( const ThisType& str, SizeType position, SizeType num );
		ThisType& Append( const ValueType* str, SizeType num );
		ThisType& Append( const ValueType* str );
		ThisType& Append( SizeType num, ValueType c );
		ThisType& Append( const ValueType* begin, const ValueType* end );
		ThisType& AppendSprintfVA( const ValueType* toFormat, va_list arguments );
		ThisType& AppendSprintf( __IME_VERIFY_FORMAT_STRING const ValueType* toFormat, ... );

		/// Pseudo-iterators support(e.g. Front/Back insertion iterators)

		void AppendBack( ValueType c );
		void GetBack( void );

		/// Insertion

		ThisType& Insert( SizeType position, const ThisType& rhs );
		ThisType& Insert( SizeType position, const ThisType& rhs, SizeType begin, SizeType num );
		ThisType& Insert( SizeType position, const ValueType* ptr, SizeType num );
		ThisType& Insert( SizeType position, const ValueType* ptr );
		ThisType& Insert( SizeType position, SizeType num, ValueType val );
		typename ThisType::Iterator Insert( typename ThisType::Iterator iter, ValueType c );
		void Insert( typename ThisType::Iterator iter, SizeType num, ValueType c );
		void Insert( typename ThisType::Iterator iter, const ValueType* begin, const ValueType* end );

		/// Erasing functions

		ThisType& Erase( SizeType position = 0, SizeType npos = ThisType::theNPos );
		typename ThisType::Iterator Erase( typename ThisType::Iterator iter );
		typename ThisType::Iterator Erase( typename ThisType::Iterator begin, typename ThisType::Iterator end );
		typename ThisType::ReverseIterator Erase( typename ThisType::ReverseIterator iter );
		typename ThisType::ReverseIterator Erase( typename ThisType::ReverseIterator begin, typename ThisType::ReverseIterator end );
		void Clear( void );
		void Reset( void );

		/// Replacement functions

		ThisType& Replace( SizeType position, SizeType num, const ThisType& rhs );
		ThisType& Replace( SizeType pos1, SizeType num1, const ThisType& rhs, SizeType pos2, SizeType num2 );
		ThisType& Replace( SizeType position, SizeType num1, ValueType* ptr, SizeType num2 );
		ThisType& Replace( SizeType position, SizeType num, const ValueType* ptr );
		ThisType& Replace( SizeType position, SizeType num1, SizeType num2, ValueType c );
		ThisType& Replace( typename ThisType::Iterator first, typename ThisType::Iterator last, const ThisType& rhs );
		ThisType& Replace( typename ThisType::Iterator first, typename ThisType::Iterator last, const ValueType* ptr, SizeType num );
		ThisType& Replace( typename ThisType::Iterator first, typename ThisType::Iterator last, const ValueType* ptr );
		ThisType& Replace( typename ThisType::Iterator first, typename ThisType::Iterator last, SizeType num, ValueType c );
		ThisType& Replace( typename ThisType::Iterator first, typename ThisType::Iterator last, const ValueType* begin, const ValueType* end );
		SizeType Copy( ValueType* ptr, SizeType num, SizeType position = 0 ) const;

		/// Find operations

		SizeType Find( const ThisType& rhs, SizeType position = 0 ) const;
		SizeType Find( const ValueType* ptr, SizeType position = 0 ) const;
		SizeType Find( const ValueType* ptr, SizeType position, SizeType num ) const;
		SizeType Find( ValueType c, SizeType position ) const;

		/// Reverse find operations

		SizeType RFind( const ThisType& rhs, SizeType position = ThisType::theNPos ) const;
		SizeType RFind( const ValueType* ptr, SizeType position = ThisType::theNPos ) const;
		SizeType RFind( const ValueType* ptr, SizeType position, SizeType num ) const;
		SizeType RFind( ValueType c, SizeType position ) const;

		/// Find first-of operations

		SizeType FindFirstOf( const ThisType& str, SizeType pos = 0 ) const;
		SizeType FindFirstOf( const ValueType* ptr, SizeType pos = 0 ) const;
		SizeType FindFirstOf( const ValueType* ptr, SizeType pos, SizeType num ) const;
		SizeType FindFirstOf( ValueType c, SizeType num ) const;

		/// Find last-of operations

		SizeType FindLastOf( const ThisType& str, SizeType pos = 0 ) const;
		SizeType FindLastOf( const ValueType* ptr, SizeType pos = 0 ) const;
		SizeType FindLastOf( const ValueType* ptr, SizeType pos, SizeType num ) const;
		SizeType FindLastOf( ValueType c, SizeType num ) const;

		/// Find first not-of operation

		SizeType FindFirstNotOf( const ThisType& str, SizeType pos = 0 ) const;
		SizeType FindFirstNotOf( const ValueType* ptr, SizeType pos = 0 ) const;
		SizeType FindFirstNotOf( const ValueType* ptr, SizeType pos, SizeType num ) const;
		SizeType FindFirstNotOf( ValueType c, SizeType num ) const;
		
		/// Find last not-of operations

		SizeType FindLastNotOf( const ThisType& str, SizeType pos = 0 ) const;
		SizeType FindLastNotOf( const ValueType* ptr, SizeType pos = 0 ) const;
		SizeType FindLastNofOf( const ValueType* ptr, SizeType pos, SizeType num ) const;
		SizeType FindLastNotOf( ValueType c, SizeType num ) const;

		/// Substring

		ThisType SubString( SizeType pos = 0, SizeType num = ThisType::TheNPos ) const;

		/// Comparison functions

		DifferenceType Compare( const ThisType& str ) const;
		DifferenceType Compare( SizeType pos1, SizeType num1, const ThisType& str ) const;
		DifferenceType Compare( SizeType pos1, SizeType num1, const ThisType& str, SizeType pos2, SizeType num2 ) const;
		DifferenceType Compare( const ValueType* ptr ) const;
		DifferenceType Compare( SizeType pos1, SizeType num1, const ValueType* ptr ) const;
		DifferenceType Compare( SizeType pos1, SizeType num1, const ValueType* ptr, SizeType pos2, SizeType num2 ) const;

		/// Case-intensive comparison functions

		DifferenceType CompareI( const ThisType& str ) const;
		DifferenceType CompareI( const ValueType* ptr ) const;

		/// Comparison operators

		bool operator == ( const ThisType& rhs ) const;
		bool operator == ( ConstPointer ptr ) const;
		bool operator != ( const ThisType& rhs ) const;
		bool operator != ( ConstPointer ptr ) const;

		bool operator < (const ThisType& rhs) const;
		bool operator > (const ThisType& rhs) const;

		/// Convertation functionality

		s32 AsInt( void ) const;
		float AsFloat( void ) const;
		bool AsBool( void ) const;

		/// Miscelanceuos functionality

		void MakeUpper( void );
		ThisType Upper( void );
		void MakeLower( void );
		ThisType Lower( void );
		void LeftTrim( void );
		ThisType Left( SizeType num ) const;
		void RightTrim( void );
		ThisType Right( SizeType num ) const;
		void MakeTrim( void );
		ThisType& SprintfVA( const ValueType* fmt, va_list list );
		ThisType& Sprintf( __IME_VERIFY_FORMAT_STRING const ValueType* fmt, ... );

		/// tokenize string into a provided String array (faster if tokens array can be reused)
		SizeType Tokenize( const ThisType& whiteSpace, STL::Array< ThisType >& outTokens ) const;
		/// tokenize string into a provided String array, SLOW since new array will be constructed
		STL::Array< ThisType > Tokenize( const ThisType& whiteSpace ) const;
		/// tokenize string, keep strings within fence characters intact (faster if tokens array can be reused)
		SizeType Tokenize( const ThisType& whiteSpace, ValueType fence, STL::Array< ThisType >& outTokens ) const;
		/// tokenize string, keep strings within fence characters intact, SLOW since new array will be constructed
		STL::Array< ThisType > Tokenize( const ThisType& whiteSpace, ValueType fence) const;


		//// Filesystem manipulations

		ThisType GetFileExtension( void ) const;
		bool GetFileExtension( ThisType& string ) const;
		void ConvertBackslashes( void );
		void RemoveFileExtension( void );
		void ChangeFileExtension( const ThisType& newExt );
		ThisType ExtractFileName( void ) const;
		ThisType ExtractLastDirectory( void ) const;
		ThisType ExtractDirectory( void ) const;

		/// Validation

		bool IsValid( void ) const;
		bool IsValidIterator( typename ThisType::ConstIterator iter ) const;

		/// Variable argument defintion, without calling 
		static ConstPointer VA( __IME_VERIFY_FORMAT_STRING ConstPointer format, ... );
		static ConstPointer StrFind( ConstPointer ptrBegin, ValueType c, SizeType num );
		static Pointer StrUninitCopy( ConstPointer srcBegin, ConstPointer srcEnd, Pointer dstBegin );
		static Pointer StrUninitFillN( Pointer dstBegin, SizeType num, ValueType c );
		static Pointer StrTypeAssignN( Pointer dstBegin, SizeType num, ValueType c );

	private:
		Pointer Allocate( SizeType num );
		void Free( Pointer ptr, SizeType num );
		SizeType GetNewCapacity( SizeType curCapacity );

		void AllocateSelf( void );
		void AllocateSelf( SizeType num );
		void DeallocateSelf( void );
		typename ThisType::Iterator InsertInternal( typename ThisType::Iterator iter, ValueType c );
		void RangeInitialize( ConstPointer begin, ConstPointer end );
		void RangeInitialize( ConstPointer begin );

		ValueType* myBeginIter;
		ValueType* myEndIter;
		ValueType* myCapacityEnd;
	};


	template< typename Str >
	struct StringHashAlgorithm
	{
		__IME_INLINED SizeT operator()( const STL::String< Str >& str ) const {
			SizeT hash = 0;
			for ( const Str* p = str.AsCharPtr(); *p; ++p )
				hash = ((SizeT)(*p)) ^ (hash * theHashMagic);

			return hash;
		}
	};


	template< typename T >
	__IME_INLINED String< T >::String()
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL )
	{
		this->AllocateSelf();
	}

	template< typename T >
	__IME_INLINED String< T >::String( const ThisType& rhs ) 
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL ) {
		this->RangeInitialize( rhs.myBeginIter, rhs.myEndIter );
	}

	template< typename T >
	__IME_INLINED String< T >::String( const ThisType& rhs, 
									   SizeType position, 
									   SizeType num )
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL )
	{
		SizeType offset = Math::TMin< SizeType >( num, reinterpret_cast< SizeType >( rhs.myEndIter - rhs.myBeginIter ) - position );
		this->RangeInitialize( rhs.myBeginIter + position, rhs.myBeginIter + position + offset );
	}

	template< typename T >
	__IME_INLINED String< T >::String( const ValueType* str, SizeType num )
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL )
	{
		this->RangeInitialize( str, str + num );
	}

	template< typename T >
	__IME_INLINED String< T >::String( ConstPointer str )
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL )
	{
		this->RangeInitialize( str );
	}

	template< typename T >
	__IME_INLINED String< T >::String( const ValueType* begin, const ValueType* end )
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL )
	{
		this->RangeInitialize( begin, end );
	}

	template< typename T >
	__IME_INLINED String< T >::String( Traits::CtorNotInitialized, SizeType num ) 
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL )
	{
		this->AllocateSelf( num + 1 );
		*this->myEndIter = 0;
	}

	template< typename T >
	__IME_NO_INLINE String< T >::String( Traits::CtorHasVA, __IME_VERIFY_FORMAT_STRING const ValueType* toFormat, ... )
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL )
	{
		const SizeType num = TraitsType::StringLength( toFormat );
		this->AllocateSelf( num );

		va_list vl;
		va_start( vl, toFormat );
		this->AppendSprintfVA( toFormat, vl );
		va_end( vl );
	}

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
	template< typename T >
	__IME_INLINED String< T >::String( ThisType&& rhs )
		: myBeginIter( NULL )
		, myEndIter( NULL )
		, myCapacityEnd( NULL ) 
	{
		this->Swap( rhs );
	}
#endif

	template< typename T >
	__IME_INLINED String< T >::~String( void )
	{
		this->DeallocateSelf();
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::operator=( const ThisType& rhs ) {
		if( &rhs != this )
			this->Assign( rhs.myBeginIter, rhs.myEndIter );

		return *this;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::operator=( ConstPointer ptr ) 
	{
		return this->Assign( ptr, ptr + TraitsType::StringLength( ptr ) );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::operator=( ValueType c )
	{
		const ThisType::SizeType one = static_cast< SizeType >( 1 );
		return this->Assign( one, c );
	}

	template< typename T >
	__IME_INLINED void String< T >::Swap( ThisType& rhs ) 
	{
		/// Swap data within those strings
		SwapAlgorithm( this->myBeginIter, rhs.myBeginIter );
		SwapAlgorithm( this->myEndIter, rhs.myEndIter );
		SwapAlgorithm( this->myCapacityEnd, rhs.myCapacityEnd );
	}

	template< typename T >
	__IME_INLINED const typename String< T >::ValueType* String< T >::Data( void ) const
	{
		return this->myBeginIter
	}

	template< typename T >
	__IME_INLINED const typename String< T >::ValueType* String< T >::AsCharPtr( void ) const 
	{
		return this->myBeginIter;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Assign( const ThisType& x )
	{
		return this->Assign( x.myBeginIter, x.myEndIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Assign( const ThisType& x, SizeType position, SizeType num ) 
	{
		const SizeType thisStringSize = static_cast< SizeType >( this->myEndIter - this->myBeginIter );
		__IME_ASSERT( position < thisStringSize );

		return this->Assign( x.myBeginIter + position, x.myBeginIter + position + Math::TMin( num, thisStringSize - position ) );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Assign( ConstPointer cStr, SizeType num ) 
	{
		return this->Assign( cStr, cStr + num );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Assign( ConstPointer cStr ) 
	{
		const SizeType cStrSize = TraitsType::StringLength( cStr );
		return this->Assign( cStr, cStr + cStrSize );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Assign( SizeType num, ValueType c ) 
	{
		if( num <= static_cast< SizeType >( this->myEndIter - this->myBeginIter ) ) 
		{
			this->StrTypeAssignN( this->myBeginIter, num, c );
			this->Erase( this->myBeginIter + num, this->myEndIter ); 
		} 
		else 
		{
			this->StrTypeAssignN( this->myBeginIter, thisStrSize, c );
			this->Append( n - static_cast< SizeType >( this->myEndIter - this->myBeginIter ), c );
		}

		return *this;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Assign( ConstPointer begin, ConstPointer end ) 
	{
		const SSizeT cStrSize = end - begin;
		if( static_cast< SizeType >( cStrSize ) <= static_cast< SizeType >( this->myEndIter - this->myBeginIter ) ) 
		{
			Memory::LowLevelMemory::CopyMemoryBlock( begin, this->myBeginIter, static_cast<SizeType>(cStrSize)* sizeof(ValueType) );
			this->Erase( this->myBeginIter + cStrSize, this->myEndIter );
		} 
		else
		{
			Memory::LowLevelMemory::CopyMemoryBlock( begin, this->myBeginIter, static_cast<SizeType>(this->myEndIter - this->myBeginIter) * sizeof(ValueType) );
			this->Append( begin + static_cast< SizeType >( this->myEndIter - this->myBeginIter ), end );
		}

		return *this;
	}

	template< typename T >
	__IME_INLINED typename String< T >::Iterator String< T >::Begin( void ) 
	{
		return this->myBeginIter;
	}

	template< typename T >
	__IME_INLINED typename String< T >::Iterator String< T >::End( void ) 
	{
		return this->myEndIter;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ConstIterator String< T >::Begin( void ) const 
	{
		return this->myBeginIter;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ConstIterator String< T >::End( void ) const 
	{
		return this->myEndIter;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ReverseIterator String< T >::ReverseBegin( void ) 
	{
		return ThisType::ReverseIterator( this->myEndIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ReverseIterator String< T >::ReverseEnd( void ) 
	{
		return ThisType::ReverseIterator( this->myBeginIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ConstReverseIterator String< T >::ReverseBegin( void ) const 
	{
		return ThisType::ConstReverseIterator( this->myEndIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ConstReverseIterator String< T >::ReverseEnd( void ) const 
	{
		return ThisType::ConstReverseIterator( this->myBeginIter );
	}

	template< typename T >
	__IME_INLINED bool String< T >::IsEmpty( void ) const
	{
		return this->myBeginIter == this->myEndIter;
	}

	template< typename T >
	__IME_INLINED typename String< T >::SizeType String< T >::Size( void ) const 
	{
		return static_cast< SizeType >( this->myEndIter - this->myBeginIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::SizeType String< T >::Length( void ) const 
	{
		return static_cast< SizeType >( this->myEndIter - this->myBeginIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::SizeType String< T >::MaxSize( void ) const 
	{
		return ThisType::theMaxSize;
	}

	template< typename T >
	__IME_INLINED typename String< T >::SizeType String< T >::MaxCapacity( void ) const 
	{
		return static_cast< SizeType >( this->myCapacityEnd - this->myBeginIter ) - 1;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ConstReference String< T >::operator[]( SizeType num ) const
	{
		__IME_ASSERT_MSG( num < static_cast< SizeType >( this->myEndIter - this->myBeginIter ), 
						  "Out of range! Cannot access this area." );

		return this->myBeginIter[num];
	}

	template< typename T >
	__IME_INLINED void String< T >::Resize( SizeType num, ValueType c )
	{
		const ThisType::SizeType length = this->Size();

		if( num < length )
			this->Erase( this->myBeginIter + num, this->myEndIter );
		else if( num > length )
			this->Append( num - length, c );
	}

	template< typename T >
	__IME_INLINED void String< T >::Resize( SizeType num ) 
	{
		/// C++ basic_string specifies that Resize(num) is equivalent to Resize(num, ValueType()). 
        /// For built-in types, ValueType() is the same as zero (ValueType(0)).
        /// We can improve the efficiency (especially for long strings) of this 
        /// string class by resizing without assigning to anything.

		const ThisType::SizeType length = this->Size();

		if( num < length )
			this->Erase( this->myBeginIter + num, this->myEndIter );
		else if( num > length )
			this->Append( num - length, ValueType() );
	}

	template< typename T >
	__IME_INLINED void String< T >::Reserve( SizeType size ) 
	{
		__IME_ASSERT( size <= ThisType::theMaxSize );

		/// The C++ standard for String doesn't specify if we should or shouldn't 
        /// downsize the container. The standard is overly vague in its description of reserve:
        ///    The member function Reserve() is a directive that informs a 
        ///    String object of a planned change in size, so that it 
        ///    can manage the storage allocation accordingly.
        /// We will act like the vector container and preserve the contents of 
        /// the container and only reallocate if increasing the size. The user 
        /// can use the SetCapacity function to reduce the capacity.

		size = Math::TMin< SizeType >( size, static_cast< SizeType >( this->myEndIter - this->myBeginIter ) );

		if( size >= static_cast< SizeType >( this->myCapacityEnd - this->myBeginIter ) )
			this->SetCapacity( size );
	}

	/// This function can't be inlined because of big contents of function
	template< typename T >
	__IME_NO_INLINE void String< T >::SetCapacity( SizeType size ) 
	{
		if( size == ThisType::theNPos )
			size = static_cast< SizeType >( this->myEndIter - this->myBeginIter );
		else if( size < static_cast< SizeType >( this->myEndIter - this->myBeginIter ) )
			this->myEndIter = this->myBeginIter + size;

		/// If there is any capacity change...
		if ( size == (static_cast<SizeType>(this->myCapacityEnd - this->myBeginIter) - 1) )
			return;

		if( size )
		{
			ThisType::Pointer ptrNewBegin = this->Allocate( size + 1 );
			ThisType::Pointer ptrNewEnd = ThisType::StrUninitCopy( this->myBeginIter, this->myEndIter, ptrNewBegin );
			*ptrNewEnd = 0;

			this->DeallocateSelf();
			this->myBeginIter = ptrNewBegin;
			this->myEndIter = ptrNewEnd;
			this->myCapacityEnd = ptrNewBegin + ( size + 1 );
		} 
		else 
		{
			this->DeallocateSelf();
			this->AllocateSelf();
		}
	}

	template< typename T >
	__IME_INLINED void String< T >::ForceSize( SizeType size ) 
	{
		__IME_ASSERT( size < static_cast< SizeType >( this->myCapacityEnd - this->myBeginIter ) );
		this->myEndIter = this->myBeginIter + size;
	}

	template< typename T >
	__IME_INLINED void String< T >::Clear( void ) 
	{
		if ( this->myBeginIter == this->myEndIter )
			return;

		*this->myBeginIter = 0;
		this->myEndIter = this->myBeginIter;
	}

	template< typename T >
	__IME_INLINED void String< T >::Reset( void ) 
	{
		/// The reset function is a special extension function which unilaterally 
        /// resets the container to an empty state without freeing the memory of 
        /// the contained objects. This is useful for very quickly tearing down a 
        /// container built into scratch memory.
		this->AllocateSelf();
	}

	template< typename T >
	__IME_INLINED typename String< T >::ConstReference String< T >::At( SizeType num ) const
	{
		__IME_ASSERT( num < static_cast< SizeType >( this->myEndIter - this->myBeginIter ) );
		return this->myBeginIter[num];
	}

	template< typename T >
	__IME_INLINED typename String< T >::Reference String< T >::At( SizeType num ) 
	{
		__IME_ASSERT( num < static_cast< SizeType >( this->myEndIter - this->myBeginIter ) );
		return this->myBeginIter[num];
	}

	template< typename T >
	__IME_INLINED typename String< T >::Reference String< T >::Front() 
	{
		__IME_ASSERT_MSG( this->myBeginIter <= this->myEndIter, "String::Front(): empty string" );
		return *this->myBeginIter;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ConstReference String< T >::Front() const 
	{
		__IME_ASSERT_MSG( this->myBeginIter <= this->myEndIter, "String::Front(): empty string" );
		return *this->myBeginIter;
	}

	template< typename T >
	__IME_INLINED typename String< T >::Reference String< T >::Back() 
	{
		__IME_ASSERT_MSG( this->myBeginIter <= this->myEndIter, "String::End(): empty string" );
		return *(this->myEndIter - 1);
	}

	template< typename T >
	__IME_INLINED typename String< T >::ConstReference String< T >::Back() const 
	{
		__IME_ASSERT_MSG( this->myBeginIter <= this->myEndIter, "String::End(): empty string" );
		return *(this->myEndIter - 1);
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::operator += ( const ThisType& rhs ) 
	{
		return this->Append( rhs );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::operator += ( ConstPointer cStr ) 
	{
		return this->Append( cStr );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::operator += ( ValueType c ) 
	{
		this->AppendBack( c );
		return *this;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Append( const ThisType& rhs )
	{
		this->Append( rhs.myBeginIter, rhs.myEndIter );
		return *this;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Append( const ThisType& rhs, SizeType position, SizeType num ) 
	{
		__IME_ASSERT( position <= static_cast< SizeType >( rhs.myEndIter - rhs.myBeginIter ) );
		return this->Append( rhs.myBeginIter + position, 
							 rhs.myBeginIter + position + Math::TMin( num, static_cast< SizeType >( rhs.myBeginIter - rhs.myEndIter ) - position ) );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Append( ConstPointer cStr, SizeType num ) 
	{
		return this->Append( cStr, cStr + num );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Append( ConstPointer cStr ) 
	{
		const SizeType cStrSize = TraitsType::StringLength( cStr );
		return this->Append( cStr, cStr + cStrSize );
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Append( SizeType num, ValueType c ) 
	{
		const SizeType thisStrSize = static_cast< SizeType >( this->myEndIter - this->myBeginIter );
		__IME_ASSERT( ( num <= theMaxSize ) || ( thisStrSize <= (theMaxSize - num) ) );

		const SizeType thisStrCapacity = static_cast< SizeType >( this->myCapacityEnd - this->myBeginIter ) - 1;

		if( ( thisStrSize + num ) > thisStrCapacity )
			this->Reserve( Math::TMax( this->GetNewCapacity( thisStrCapacity ), thisStrSize + num ) );

		if( num > 0 ) 
		{
			this->StrUninitFillN( this->myEndIter + 1, num - 1, c );
			*this->myEndIter = c;
			this->myEndIter += num;
			*this->myEndIter = 0;
		}

		return *this;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Append( ConstPointer begin, ConstPointer end ) {
		if( begin != end )
		{
			const SizeType thisStrSize = static_cast< SizeType >( this->myEndIter - this->myBeginIter );
			const SizeType inStrSize = static_cast< SizeType >( end - begin );

			__IME_ASSERT( (inStrSize <= theMaxSize) || (thisStrSize <= (theMaxSize - inStrSize) ) );

			const SizeType thisStrCapacity = static_cast< SizeType >( (this->myCapacityEnd - this->myBeginIter) - 1 );

			if( (thisStrSize + inStrSize) > thisStrCapacity )
			{
				// + 1 at the end of the line is to accomodate the trailing 0.
				const SizeType thisStrNewSize = Math::TMax( this->GetNewCapacity( thisStrCapacity ), thisStrSize + inStrSize ) + 1;

				Pointer newBegin = this->Allocate( thisStrNewSize );
				Pointer newEnd = newBegin;

				newEnd = this->StrUninitCopy( this->myBeginIter, this->myEndIter, newBegin );
				newEnd = this->StrUninitCopy( begin, end, newEnd );
				*newEnd = 0;

				this->DeallocateSelf();
				this->myBeginIter = newBegin;
				this->myEndIter = newEnd;
				this->myCapacityEnd = newBegin + thisStrNewSize;
			} 
			else
			{
				ConstPointer ptr = begin;
				++ptr;
				StrUninitCopy( ptr, end, this->myEndIter + 1 );
				this->myEndIter[inStrSize] = 0;
				*this->myEndIter = *begin;
				this->myEndIter += inStrSize;
			}
		}

		return *this;
	}

	template< typename T >
	__IME_NO_INLINE typename String< T >::ThisType& String< T >::AppendSprintfVA( ConstPointer toFormat, va_list arguments ) 
	{
        /// From unofficial C89 extension documentation:
		/// The vsnprintf returns the number of characters written into the array,
        /// not counting the terminating null character, or a negative value
        /// if count or more characters are requested to be generated.
        /// An error can occur while converting a value for output.

        /// From the C99 standard:
        /// The vsnprintf function returns the number of characters that would have
        /// been written had n been sufficiently large, not counting the terminating
        // null character, or a negative value if an encoding error occurred.
        /// Thus, the null-terminated output has been completely written if and only
        /// if the returned value is nonnegative and less than n.

		SizeType thisStringSize = static_cast< SizeType >( this->myEndIter - this->myBeginIter );
		SSizeT iReturnValue = 0;

		if( this->myBeginIter == TraitsType::GetEmptyString(ValueType()) )
			iReturnValue = TraitsType::VariableArgumentPrintf( this->myEndIter, 0, toFormat, arguments );
		else
			iReturnValue = TraitsType::VariableArgumentPrintf( this->myEndIter, static_cast< SizeType >( this->myCapacityEnd - this->myEndIter ), toFormat, arguments );

		if( iReturnValue >= static_cast< SSizeT >( this->myCapacityEnd - this->myEndIter ) ) 
		{
			this->Resize( thisStringSize + iReturnValue );
			iReturnValue = TraitsType::VariableArgumentPrintf( this->myBeginIter + thisStringSize, 
															   iReturnValue + 1, 
															   toFormat, 
															   arguments );
		} 
		else if( iReturnValue < 0 ) 
		{
			SizeType strSize = this->Size();
			SizeType num = Math::TMax( theMinimalSize - 1, strSize * 2 );

			for( /* Nothing */ ; (iReturnValue < 0) && (num < 1000000) ; num *= 2 ) 
			{
				this->Resize( num );

				const SizeType newCapacity = (num + 1) - thisStringSize;
				iReturnValue = TraitsType::VariableArgumentPrintf( this->myBeginIter + thisStringSize, newCapacity, toFormat, arguments );

				if( iReturnValue == static_cast< SSizeT >( newCapacity ) ) 
				{
					this->Reserve( ++num );
					iReturnValue = TraitsType::VariableArgumentPrintf( this->myBeginIter + thisStringSize, newCapacity + 1, toFormat, arguments );
				}
			}
		}

		if( iReturnValue >= 0 )
			/// We are guaranteed from the above logic that mpEnd <= mpCapacity.
			this->myEndIter = this->myBeginIter + thisStringSize + iReturnValue; 

		return *this;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::AppendSprintf( __IME_VERIFY_FORMAT_STRING ConstPointer toFormat, ... ) 
	{
		va_list arguments;
		va_start(arguments, toFormat);
		this->AppendSprintfVA( toFormat, arguments );
		va_end(arguments);
	}

	template< typename T >
	__IME_INLINED void String< T >::AppendBack( ValueType c ) 
	{
		if( ( this->myEndIter + 1 ) == this->myCapacityEnd )
			this->Reserve( Math::TMax( 
				this->GetNewCapacity( static_cast< SizeType >( this->myCapacityEnd - this->myBeginIter ) - 1 ), 
				static_cast< SizeType >( this->myEndIter - this->myBeginIter ) ) );

		*this->myEndIter++ = c;
		*this->myEndIter = 0;
	}

	template< typename T >
	__IME_INLINED void String< T >::GetBack( void ) 
	{
		__IME_ASSERT_MSG( this->myEndIter >= this->myBeginIter, "String::GetBack -- empty string" );
		this->myEndIter[-1] = ValueType(0);
		--this->myEndIter;
	}

	/*template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Insert( SizeType position, const ThisType& rhs )
	{

	}*/

	template< typename T >
	__IME_INLINED typename String< T >::Iterator String< T >::Erase( Iterator begin, Iterator end ) 
	{
		if( begin != end ) 	
		{
			Memory::LowLevelMemory::CopyMemoryBlock( end, begin, 
													 (static_cast<SizeType>(this->myEndIter - end) + 1) * sizeof(ValueType) );

			const Iterator newEnd = (this->myEndIter - ( end - begin ));
			this->myEndIter = newEnd;
		}

		return begin;
	}

	template< typename T >
	__IME_INLINED typename String< T >::DifferenceType String< T >::Compare( const ThisType& str ) const 
	{
		return this->Compare( str.myBeginIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::DifferenceType String< T >::Compare( SizeType pos1, 
																			 SizeType num1, 
																			 const ThisType& str ) const 
	{
		return this->Compare( pos1, num1, str.myBeginIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::DifferenceType String< T >::Compare( SizeType pos1,
																			 SizeType num1,
																			 const ThisType& str,
																			 SizeType pos2,
																			 SizeType num2 ) const 
	{
		return this->Compare( pos1, num1, str.myBeginIter, pos2, num2 );
	}

	template< typename T >
	__IME_INLINED typename String< T >::DifferenceType String< T >::Compare( const ValueType* ptr ) const
	{
		return TraitsType::StringCompare( ptr, this->myBeginIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::DifferenceType String< T >::Compare( SizeType pos1, 
																			 SizeType num1, 
																			 const ValueType* ptr ) const 
	{
		SizeType size = (ptr + pos1) - (ptr + str.Size());
		size = size < num1 ? size : num1;

		Pointer iter1 = static_cast<Pointer>(__alloca16( size ));
		Memory::LowLevelMemory::CopyMemoryBlock16( ptr + pos1, iter1, size );

		return TraitsType::StringCompare( iter1, this->myBeginIter );
	}

	template< typename T >
	__IME_INLINED typename String< T >::DifferenceType String< T >::Compare( SizeType pos1,
																			 SizeType num1,
																			 const ValueType* ptr,
																			 SizeType pos2,
																			 SizeType num2 ) const 
	{
		SizeType size = (ptr + pos1) - (ptr + str.Size());
		size = size < num1 ? size : num1;

		Pointer iter1 = static_cast<Pointer>(__alloca16( size ));
		Memory::LowLevelMemory::CopyMemoryBlock16( ptr + pos1, iter1, size );

		size = (this->myBeginIter + pos2) - (this->myBeginIter + this->Size());
		size = size < num1 ? size : num2;
		Pointer iter2 = static_cast<Pointer>(__alloca16( size ));
		Memory::LowLevelMemory::CopyMemoryBlock16( this->myBeginIter + pos2, iter2, size );

		return TraitsType::StringCompare( iter1, iter2 );
	}

	template< typename T >
	__IME_INLINED bool String< T >::operator == ( const ThisType& rhs ) const
	{
		return TraitsType::StringCompare( this->myBeginIter, rhs.myBeginIter ) == 0;
	}

	template< typename T >
	__IME_INLINED bool String< T >::operator == ( ConstPointer ptr ) const 
	{
		return TraitsType::StringCompare( this->myBeginIter, ptr ) == 0;
	}

	template< typename T >
	__IME_INLINED bool String< T >::operator != ( const ThisType& rhs ) const 
	{
		return TraitsType::StringCompare( this->myBeginIter, rhs.myBeginIter ) != 0;
	}

	template< typename T >
	__IME_INLINED bool String< T >::operator != ( ConstPointer ptr ) const 
	{
		return TraitsType::StringCompare( this->myBeginIter, ptr ) != 0;
	}

	template< typename T >
	__IME_INLINED bool String< T >::operator < (const ThisType& rhs) const 
	{
		return this->Compare( rhs ) > 0;
	}

	template< typename T >
	__IME_INLINED bool String< T >::operator > (const ThisType& rhs) const 
	{
		return this->Compare( rhs ) < 0;
	}

	template< typename T >
	__IME_INLINED s32 String< T >::AsInt( void ) const 
	{
		return atoi( this->myBeginIter );
	}

	template< typename T >
	__IME_INLINED float String< T >::AsFloat( void ) const 
	{
		/// TODO: replace with faster version
		return float( atof( this->myBeginIter ) );
	}

	template< typename T >
	__IME_NO_INLINE bool String< T >::AsBool( void ) const 
	{
		static const char* bools[] = { "no", "yes", "off", "on", "enable", "disable", "false", "true", 0 };
		SizeType i = 0;
		while( bools[i] != 0 ) 
		{
			if( 0 == TraitsType::StringCompare( bools[i], this->myBeginIter ) )
				return 1 == (i & 1);

			i++;
		}

		return false;
	}

	template< typename T >
	__IME_INLINED typename String< T >::SizeType String< T >::Tokenize( const ThisType& whitespace, 
																		STL::Array< ThisType >& output ) const 
	{
		output.Clear();
		ThisType string( *this );
		Pointer strPtr = const_cast< Pointer >( string.AsCharPtr() );
		ConstPointer token;

		while( (token = strtok( strPtr, whitespace.AsCharPtr() )) != 0 ) 
		{
			output.Append( ThisType( token ) );
			strPtr = NULL;
		}

		return output.Size();
	}

	template< typename T >
	__IME_INLINED STL::Array< typename String< T >::ThisType > String< T >::Tokenize( const ThisType& whitespace ) const 
	{
		STL::Array< ThisType > output;
		this->Tokenize( whitespace, output );
		return output;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType String< T >::GetFileExtension( void ) const 
	{
		ConstPointer str = this->myBeginIter;
		ConstPointer ext = TraitsType::StringCharLast( str, '.' );
		if( ext ) 
		{
			ext++;
			return ThisType( ext );
		}

		return ThisType( "" );
	}

	template< typename T >
	__IME_INLINED typename String< T >::SizeType String< T >::Tokenize( const ThisType& whitespace, 
																		ValueType fence, 
																		STL::Array< ThisType >& output ) const {
		output.Clear();
		ThisType string( *this );
		Pointer ptr = string.myBeginIter;
		Pointer end = string.myEndIter;
		while( ptr < end ) 
		{
			Pointer c;
			/// Skip whitespace
			while( *ptr && TraitsType::StringChar( whitespace.AsCharPtr(), *ptr ) )
				ptr++;

			if( *ptr ) 
			{
				/// Check for fenced area
				if( (fence == *ptr) && ( (c = TraitsType::StringChar( ++ptr, fence )) != 0 ) ) 
				{
					*c++ = 0;
					output.Append( ThisType( ptr ) );
					ptr = c;
				} 
				else if( (c = TraitsType::StringPointerBreak( ptr, whitespace.AsCharPtr() ) ) != 0 ) 
				{
					*c++ = 0;
					output.Append( ThisType( ptr ) );
					ptr = c;
				} 
				else
				{
					output.Append( ThisType( ptr ) );
					break;
				}
			}
		}

		return output.Size();
	}

	template< typename T >
	__IME_INLINED STL::Array< typename String< T >::ThisType > String< T >::Tokenize( const ThisType& whitespace, 
																					  ValueType fence ) const 
	{
		STL::Array< ThisType > output;
		this->Tokenize( whitespace, fence, output );
		return output;
	}

	template< typename T >
	__IME_INLINED bool String< T >::IsValid( void ) const 
	{
		if( (this->myBeginIter == NULL) || (this->myEndIter == NULL) )
			return false;

		if( this->myEndIter < this->myBeginIter )
			return false;

		if( this->myCapacityEnd < this->myEndIter )
			return false;

		return true;
	}

	template< typename T >
	__IME_INLINED bool String< T >::IsValidIterator( typename ThisType::ConstIterator iter ) const 
	{
		if( iter >= this->myBeginIter )
		{
			if( iter < this->myEndIter )
				return true;

			if( iter <= this->myEndIter )
				return true;
		}

		return false;
	}

	template< typename T >
	__IME_INLINED typename String< T >::ThisType& String< T >::Sprintf( __IME_VERIFY_FORMAT_STRING const ValueType* fmt, ... ) 
	{
		va_list va;
		va_start( va, fmt );
		this->myEndIter = this->myBeginIter;
		this->AppendSprintfVA( fmt, va );
		va_end( va );

		return *this;
	}

	template< typename T >
	__IME_NO_INLINE typename String< T >::ConstPointer String< T >::VA( __IME_VERIFY_FORMAT_STRING ConstPointer fmt, ... ) 
	{
		va_list argptr;

		NDKThreadLocal static SizeType index = 0;
		NDKThreadLocal static ValueType string[4][16384];	// in case called by nested functions
		Pointer buf;

		buf = string[index];
		index = (index + 1) & 3;

		va_start( argptr, fmt );
		TraitsType::VariableArgumentPrintf( buf, sizeof( string[ index - 1 ] ), fmt, argptr );
		va_end( argptr );

		return buf;
	}

	template< typename T >
	__IME_INLINED typename String< T >::Pointer String< T >::StrUninitCopy( ConstPointer srcBegin, 
																			ConstPointer srcEnd, 
																			Pointer dstBegin ) 
	{
		//memmove( dstBegin, srcBegin, static_cast< SizeType >( srcEnd - srcBegin ) * sizeof( ValueType ) );
		Memory::LowLevelMemory::CopyMemoryBlock( srcBegin, dstBegin, static_cast<SizeType>(srcEnd - srcBegin) * sizeof(ValueType) );
		return dstBegin + ( srcEnd - srcBegin );
	}

	template< typename T >
	__IME_INLINED typename String< T >::Pointer String< T >::StrUninitFillN( 
		Pointer dstBegin, 														 
		SizeType num, 
		ValueType c ) 
	{
		return TraitsType::StringUninitiailizedFill( dstBegin, num, c );
	}

	template< typename T >
	__IME_INLINED typename String< T >::Pointer String< T >::Allocate( SizeType num ) 
	{
		// We want n > 1 because n == 1 is reserved for empty capacity and usage of gEmptyString.
		__IME_ASSERT( num > 1 );
		return static_cast< Pointer >( Memory::Allocator::Malloc( num * sizeof( ValueType ) ) );
	}

	template< typename T >
	__IME_INLINED void String< T >::Free( Pointer ptr, SizeType num ) 
	{
		UNREFERENCED_PARAMETER( num );
		if (ptr) {
			Memory::Allocator::Free( ptr );
		}
	}

	template< typename T >
	__IME_INLINED typename String< T >::SizeType String< T >::GetNewCapacity( SizeType curCapacity ) 
	{
		return (curCapacity > theMinimalSize) ? ( 2 * curCapacity ) : theMinimalSize;
	}

	template< typename T >
	__IME_INLINED void String< T >::AllocateSelf( void ) 
	{
		this->myBeginIter = const_cast< Pointer >( TraitsType::GetEmptyString( ValueType() ) );
		this->myEndIter = this->myBeginIter;
		this->myCapacityEnd = this->myBeginIter + 1;
	}

	template< typename T >
	__IME_INLINED void String< T >::AllocateSelf( SizeType num ) 
	{
		__IME_ASSERT_MSG( num < 0x40000000, "String::Allocate -- too large memory request" );
		__IME_ASSERT_MSG( num < theMaxSize, "String::Allocate -- allocation request exceeded max size" );

		if( num > 1 ) 
		{
			this->myBeginIter = this->Allocate( num );
			this->myEndIter = this->myBeginIter;
			this->myCapacityEnd = this->myBeginIter + num;
		} 
		else 
		{
			this->AllocateSelf();
		}
	}

	template< typename T >
	__IME_INLINED void String< T >::DeallocateSelf( void ) 
	{
		// Note that we compare mpCapacity to mpEnd instead of comparing 
        // mpBegin to &gEmptyString. This is important because we may have
        // a case whereby one library passes a string to another library to 
        // deallocate and the two libraries have idependent versions of gEmptyString.
		if( static_cast< SizeType >( this->myEndIter - this->myBeginIter ) > 1 )
			this->Free( this->myBeginIter, static_cast< SizeType >( this->myEndIter - this->myBeginIter ) );
	}

	template< typename T >
	__IME_INLINED void String< T >::RangeInitialize( ConstPointer begin, ConstPointer end ) 
	{
		const SizeType inStrSize = static_cast< SizeType >( end - begin );
		__IME_ASSERT( begin && ( inStrSize != 0 ) );
		
		/// '+1' so that we have room for the terminating 0.
		this->AllocateSelf( inStrSize + 1 );
		this->myEndIter = StrUninitCopy( begin, end, this->myBeginIter );
		*this->myEndIter = 0;
	}

	template< typename T >
	__IME_INLINED void String< T >::RangeInitialize( ConstPointer cStr ) 
	{
		__IME_ASSERT( cStr );
		this->RangeInitialize( cStr, cStr + TraitsType::StringLength( cStr ) );
	}
}
