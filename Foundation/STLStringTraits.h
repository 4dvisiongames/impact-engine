/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef __STLString_h__
#define __STLString_h__

/// Shared code
#include <Foundation/Shared.h>
/// Safe string routines
#include <strsafe.h>

namespace STL
{
	union GEmptyStringImpl
	{
		uint32_t gEmptyStr;
		Char8 gEmptyStr8[1];
		uint8_t gEmptyStr8U[1];
		Char16 gEmptyStrU16[1];
	};
	extern GEmptyStringImpl GEmptyString;

	/// Forward declaration
	template< typename T > class StringTraits;

	/// <summary>
	/// ASCII string implementation of additional utilities to work with selected type.
	/// </summary>
	template<>
	class StringTraits< Char8 >
	{
	public:
		typedef Char8 ValueType;
		typedef Char8* Pointer;
		typedef const Char8* ConstPointer;
		typedef USizeT SizeType;

		static SizeType VariableArgumentPrintf( Pointer buffer, SizeType bufferSize, ConstPointer src, va_list arguments );
		static SizeType StringLength( ConstPointer string );
		static ConstPointer SubString( ConstPointer str, ConstPointer text );
		static ConstPointer StringPointerBreak( ConstPointer ptr, ConstPointer text );
		static SSizeT StringCompare( ConstPointer str1, ConstPointer str2 );
		static SSizeT StringCaseCompare( ConstPointer str1, ConstPointer str2 );
		static ConstPointer StringChar( ConstPointer str, ValueType c ); 
		static ConstPointer StringCharLast( ConstPointer str, ValueType c );
		static Pointer StringUninitiailizedFill( Pointer src, SizeType num, ValueType c ); 
		static Pointer GetEmptyString(ValueType) { return GEmptyString.gEmptyStr8; }
	};

	__IME_INLINED StringTraits< Char8 >::SizeType StringTraits< Char8 >::VariableArgumentPrintf( Pointer buffer, SizeType bufferSize, ConstPointer src, va_list arguments )
	{
		/// Returns number of written char's or 
		SizeType result = 0;

#if defined( _MSC_VER )
		result = _vsnprintf_s( buffer, bufferSize, _TRUNCATE, src, arguments );
#else
		result = _vsnprintf( buffer, bufferSize, src, arguments );
#endif

		return result;
	}

	__IME_INLINED StringTraits< Char8 >::SizeType StringTraits< Char8 >::StringLength( ConstPointer string )
	{
		if( !string ) 
			return 0;

#if defined( _MSC_VER )
		/// On MSVC this is built-in instrinsic function
		return strlen( string );
#else
		register ConstPointer s;
		for (s = string; *s; ++s);
		return(s - string);
#endif
	}

	__IME_INLINED StringTraits< Char8 >::ConstPointer StringTraits< Char8 >::SubString( ConstPointer str, ConstPointer text )
	{
#if defined( _MSC_VER )
		return strstr( str, text );
#else
		register ValueType c;
		register SizeT len;

		c = *str++;
		if( !c ) {
			return str;	// Trivial empty string case
		}

		len = ThisType::StrLen( text );
		do {
			ValueType sc;

			do {
				sc = *str++;
				if (!sc)
					return (ValueType *) 0;
			} while (sc != c);
		} while( ThisType::StrCmpn( str, text, len ) != 0 );

		return reinterpret_cast< ValueType* >(str - 1);
#endif
	}

	__IME_INLINED StringTraits< Char8 >::ConstPointer StringTraits< Char8 >::StringPointerBreak( ConstPointer src, ConstPointer text )
	{
#if defined( _MSC_VER )
		return strpbrk( src, text );
#else
		register ConstPointer scanp;
		register s32 c, sc;

		while ((c = *str++) != 0) {
			for (scanp = text; (sc = *scanp++) != 0;)
				if (sc == c)
					return const_cast< Pointer >( str - 1 );
		}

		return NULL;
#endif
	}

	__IME_INLINED SSizeT StringTraits< Char8 >::StringCompare( ConstPointer str1, ConstPointer str2 )
	{
#if defined( _MSC_VER )
		return strcmp( str1, str2 );
#else
		for ( ; *str1 == *str2; str1++, str2++)
		if (*str1 == '\0')
			return 0;
		return (( *const_cast< unsigned Pointer >( static_cast< const unsigned Pointer >( str1 ) ) < *const_cast< unsigned Pointer >( static_cast< const unsigned Pointer >( str2 ) ) ) ? -1 : 1);
#endif
	}

	__IME_INLINED SSizeT StringTraits< Char8 >::StringCaseCompare( ConstPointer str1, ConstPointer str2 )
	{
#if defined( _MSC_VER )
		return _stricmp( str1, str2 );
#else
		for ( /* Nothing */ ; size > 0; str1++, str2++, --size)
			if (*str1 != *str2)
				return (( *const_cast< unsigned Pointer >( static_cast< const unsigned Pointer >( str1 ) ) < *const_cast< unsigned Pointer >( static_cast< const unsigned Pointer >( str2 ) ) ) ? -1 : +1);
			else if (*str1 == '\0')
				return 0;
			return 0;
#endif
	}

	__IME_INLINED StringTraits< Char8 >::ConstPointer StringTraits< Char8 >::StringChar( ConstPointer str, ValueType c )
	{
#if defined( _MSC_VER )
		return strchr( str, c );
#else
		do {
			if( *str == chr ) {
				return const_cast< Pointer >( str );
			}
		} while (*str++);

		return NULL;
#endif
	}

	__IME_INLINED StringTraits< Char8 >::ConstPointer StringTraits< Char8 >::StringCharLast( ConstPointer str, ValueType c )
	{
#if defined( _MSC_VER )
		return strrchr( str, c );
#else
		Pointer rtnval = 0;
		do {
		if (*str == chr)
			rtnval = const_cast< Pointer >( str );
		} while (*str++);
		return rtnval;
#endif
	}

	__IME_INLINED StringTraits< Char8 >::Pointer StringTraits< Char8 >::StringUninitiailizedFill( Pointer src, SizeType num, ValueType c )
	{
		if( num )
			memset( src, static_cast< uint8_t >( c ), num );
		return src + num;
	}


	/// <summary>
	/// UTF-8 string implementation of additional utilities to work with selected type.
	/// </summary>
	template<>
	class StringTraits< Char16 >
	{
	public:
		typedef Char16 ValueType;
		typedef Char16* Pointer;
		typedef const Char16* ConstPointer;
		typedef SizeT SizeType;

		static SizeType VariableArgumentPrintf( Pointer buffer, SizeType bufferSize, ConstPointer src, va_list arguments );
		static SizeType StringLength( ConstPointer string );
		static ConstPointer SubString( ConstPointer str, ConstPointer text );
		static ConstPointer StringPointerBreak( ConstPointer ptr, ConstPointer text );
		static SSizeT StringCompare( ConstPointer str1, ConstPointer str2 );
		static SSizeT StringCaseCompare( ConstPointer str1, ConstPointer str2 );
		static ConstPointer StringChar( ConstPointer str, ValueType c ); 
		static ConstPointer StringCharLast( ConstPointer str, ValueType c );
		static Pointer StringUninitiailizedFill( Pointer src, SizeType num, ValueType c );
		static Pointer GetEmptyString(ValueType) { return GEmptyString.gEmptyStrU16; }
	};

	__IME_INLINED StringTraits< Char16 >::SizeType StringTraits< Char16 >::VariableArgumentPrintf( Pointer buffer, SizeType bufferSize, ConstPointer src, va_list arguments )
	{
		/// Returns number of written char's or 
		SizeType result = 0;

#if defined( _MSC_VER )
		result = _vsnwprintf_s( buffer, bufferSize, _TRUNCATE, src, arguments );
#else
		result = _vsnwprintf( buffer, bufferSize, src, arguments );
#endif

		return result;
	}

	__IME_INLINED StringTraits< Char16 >::SizeType StringTraits< Char16 >::StringLength( ConstPointer string )
	{
		if( !string ) 
			return 0;

#if defined( _MSC_VER )
		/// On MSVC this is built-in instrinsic function
		return wcslen( string );
#else
		register ConstPointer s;
		for (s = string; *s; ++s);
		return(s - string);
#endif
	}

	__IME_INLINED StringTraits< Char16 >::ConstPointer StringTraits< Char16 >::SubString( ConstPointer str, ConstPointer text )
	{
#if defined( _MSC_VER )
		return wcsstr( str, text );
#else
		register ValueType c;
		register SizeT len;

		c = *str++;
		if( !c ) {
			return str;	// Trivial empty string case
		}

		len = ThisType::StrLen( text );
		do {
			ValueType sc;

			do {
				sc = *str++;
				if (!sc)
					return (ValueType *) 0;
			} while (sc != c);
		} while( ThisType::StrCmpn( str, text, len ) != 0 );

		return reinterpret_cast< ValueType* >(str - 1);
#endif
	}

	__IME_INLINED StringTraits< Char16 >::ConstPointer StringTraits< Char16 >::StringPointerBreak( ConstPointer src, ConstPointer text )
	{
#if defined( _MSC_VER )
		return wcspbrk( src, text );
#else
		register ConstPointer scanp;
		register s32 c, sc;

		while ((c = *str++) != 0) {
			for (scanp = text; (sc = *scanp++) != 0;)
				if (sc == c)
					return const_cast< Pointer >( str - 1 );
		}

		return NULL;
#endif
	}

	__IME_INLINED SSizeT StringTraits< Char16 >::StringCompare( ConstPointer str1, ConstPointer str2 )
	{
#if defined( _MSC_VER )
		return wcscmp( str1, str2 );
#else
		for ( ; *str1 == *str2; str1++, str2++)
		if (*str1 == '\0')
			return 0;
		return (( *const_cast< unsigned Pointer >( static_cast< const unsigned Pointer >( str1 ) ) < *const_cast< unsigned Pointer >( static_cast< const unsigned Pointer >( str2 ) ) ) ? -1 : 1);
#endif
	}

	__IME_INLINED SSizeT StringTraits< Char16 >::StringCaseCompare( ConstPointer str1, ConstPointer str2 )
	{
#if defined( _MSC_VER )
		return _wcsicmp( str1, str2 );
#else
		for ( /* Nothing */ ; size > 0; str1++, str2++, --size)
			if (*str1 != *str2)
				return (( *const_cast< unsigned Pointer >( static_cast< const unsigned Pointer >( str1 ) ) < *const_cast< unsigned Pointer >( static_cast< const unsigned Pointer >( str2 ) ) ) ? -1 : +1);
			else if (*str1 == '\0')
				return 0;
			return 0;
#endif
	}

	__IME_INLINED StringTraits< Char16 >::ConstPointer StringTraits< Char16 >::StringChar( ConstPointer str, ValueType c )
	{
#if defined( _MSC_VER )
		return wcschr( str, c );
#else
		do {
			if( *str == chr ) {
				return const_cast< Pointer >( str );
			}
		} while (*str++);

		return NULL;
#endif
	}

	__IME_INLINED StringTraits< Char16 >::ConstPointer StringTraits< Char16 >::StringCharLast( ConstPointer str, ValueType c )
	{
#if defined( _MSC_VER )
		return wcsrchr( str, c );
#else
		Pointer rtnval = 0;
		do {
		if (*str == chr)
			rtnval = const_cast< Pointer >( str );
		} while (*str++);
		return rtnval;
#endif
	}

	__IME_INLINED StringTraits< Char16 >::Pointer StringTraits< Char16 >::StringUninitiailizedFill( Pointer src, SizeType num, ValueType c )
	{
		Pointer srcPtr16 = src;
		ConstPointer const srcEnd = src + num;
		while( srcPtr16 < srcEnd )
			*srcPtr16++ = c;
		return src + num;
	}
}

#endif /// __STLString_h__