/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef _NanoSafeCircularSentinel_h_
#define _NanoSafeCircularSentinel_h_

//! Interlocked 
#include <Foundation/Interlocked.h>

//! Threading namespace
namespace Threading
{
	//! Thread-safe circular doubly-linked list with sentinel.
	//! head.next points to the front and head.prev points to the back.
	class CircularListWithSentinel
	{
	public:
		//! Node abstraction
		struct Node
		{
			Node* prev;
			Node* next;
			explicit Node( void ) : next( (Node*)(uintptr_t)0xcdcdcdcd ), prev( (Node*)(uintptr_t)0xcdcdcdcd ) { };
		};

	private:
		//! Count of linked nodes
		volatile size_t myCount;
		//! Head list
		Node myHead;

	public:
		CircularListWithSentinel( void );
		~CircularListWithSentinel( void );
		size_t Size( void ) const;
		bool IsEmpty( void ) const;
		Node* Front( void ) const;
		Node* Last( void ) const;
		Node* Begin( void ) const;
		const Node* End( void ) const;
		void Add( Node* rhs );
		void Remove( Node& rhs );
		void MoveTo( CircularListWithSentinel& rhs );
		void Clear( void );
	};

	__IME_INLINED CircularListWithSentinel::CircularListWithSentinel( void ) {
		this->Clear();
	}

	__IME_INLINED CircularListWithSentinel::~CircularListWithSentinel( void ) {
		__IME_ASSERT_MSG( this->myHead.next == &this->myHead && this->myHead.prev == &this->myHead, "List is not empty!" );
	}

	__IME_INLINED size_t CircularListWithSentinel::Size( void ) const {
		return this->myCount;
	}

	__IME_INLINED bool CircularListWithSentinel::IsEmpty( void ) const {
		return this->Size() == 0;
	}

	__IME_INLINED CircularListWithSentinel::Node* CircularListWithSentinel::Front( void ) const {
		return this->myHead.next;
	}

	__IME_INLINED CircularListWithSentinel::Node* CircularListWithSentinel::Last( void ) const {
		return this->myHead.prev;
	}

	__IME_INLINED CircularListWithSentinel::Node* CircularListWithSentinel::Begin( void ) const {
		return this->Front();
	}

	__IME_INLINED const CircularListWithSentinel::Node* CircularListWithSentinel::End( void ) const {
		return &this->myHead;
	}

	__IME_INLINED void CircularListWithSentinel::Add( CircularListWithSentinel::Node* n ) {
		size_t localLastCount = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCount );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->myCount, localLastCount + 1 );
		n->prev = this->myHead.prev;
		n->next = &this->myHead;
		this->myHead.prev->next = n;
		this->myHead.prev = n;
	}

	__IME_INLINED void CircularListWithSentinel::Remove( CircularListWithSentinel::Node& rhs ) {
		size_t localLastCount = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCount );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->myCount, localLastCount - 1 );
		rhs.prev->next = rhs.next;
		rhs.next->prev = rhs.prev;
	}

	__IME_INLINED void CircularListWithSentinel::MoveTo( CircularListWithSentinel& rhs ) {
		const size_t localCount = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( this->myCount );
		if( localCount ) {
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( rhs.myCount, this->myCount );
			rhs.myHead.next = this->myHead.next;
			rhs.myHead.prev = this->myHead.prev;
			this->myHead.next->prev = &rhs.myHead;
			this->myHead.prev->next = &rhs.myHead;
			this->Clear();
		}
	}

	__IME_INLINED void CircularListWithSentinel::Clear( void ) {
		this->myHead.next = this->myHead.prev = &this->myHead;
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->myCount, 0 );
	}
}

#endif /// _NanoSafeCircularSentinel_h_