/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Shared code
#include <Foundation/Shared.h>

/// Threading sub-system namespace
namespace Threading
{
	/// <summary>
	/// <c>ScopedManagedLock</c> is a RAII class that allows to do manual locking of some section. Can be
	/// used in situations where algorithm can select the best way to produce results using locking results.
	/// </summary>
	template< typename T >
	class ScopedManagedLock : NoCopy
	{
		u8 myLockingPattern;
		T& myCriticalSection;

	public:
		/// <summary>
		/// </summary>
		/// <param name="cs">Reference to a locking object.</param>
		/// <param name="forceLock">Lock by default on true, TryLock on false.</param>
		/// <param name="isScopeLocked">Retrieves result of locking.</param>
		explicit ScopedManagedLock( T& cs, bool forceLock, bool& isScopeLocked );

		/// <summary>
		/// Unlocks locking object.
		/// </summary>
		~ScopedManagedLock( void );
	};

	template< typename T >
	__IME_INLINED ScopedManagedLock< T >::ScopedManagedLock( T& cs, bool forceLock, bool& isScopeLocked )
		: myCriticalSection( cs )
		, myLockingPattern(1)
	{
		if ( forceLock )
		{
			this->myCriticalSection.Lock();
			isScopeLocked = true;
		}
		else
		{
			if ( this->myCriticalSection.TryLock() )
			{
				this->myLockingPattern = 0;
				isScopeLocked = true;
			}
			else
			{
				isScopeLocked = false;
			}
		}
	}

	template< typename T >
	__IME_INLINED ScopedManagedLock< T >::~ScopedManagedLock( void )
	{
		if ( !this->myLockingPattern )
			this->myCriticalSection.Unlock();
	}
}

/*
	Change log:

	05-Jul-15		codepoet		this file		initial version.
*/