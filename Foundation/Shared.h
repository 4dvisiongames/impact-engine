/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#include <Foundation/CompilerConfig.h>
#include <Foundation/CompilerPlatform.h>

//
// Define some things for platform and include needed headers
//
#if defined( __IME_PLATFORM_WIN64 )
#include <Foundation/Win32/Win32Shared.h>
#else
#error "Core shared code is not implemented on this platform!"
#endif

/// Std
#include <typeinfo>

typedef intptr_t SizeT;
typedef intptr_t SSizeT;

typedef size_t USizeT;

/// Result type definition
typedef u32 ResultT;

typedef char Char8;
typedef wchar_t Char16;

//
// Macroses
//

/// Macro for disabling assignments of objects.
#ifdef __NDKCPP0xCompiler_Partial
#define __IME_DISABLE_COPY(name) \
	name(const name&) = delete;

#define __IME_DISABLE_ASSIGNMENT(name) \
      __IME_DISABLE_COPY(name) \
      name& operator=(const name&) = delete;

#else
#define __IME_DISABLE_COPY(name) \
	name(const name&);

#define __IME_DISABLE_ASSIGNMENT(name) \
      __IME_DISABLE_COPY(name) \
      name& operator=(const name&);

#endif

#ifndef __forever
#define __forever for(;;)
#endif /// __forever

/// @def thread_local
/// Thread local storage keyword.
/// A variable that is declared with the \c thread_local keyword makes the
/// value of the variable local to each thread (known as thread-local storage,
/// or TLS). Example usage:
/// @code
/// thread_local int variable; //!< This variable is local to each thread.
/// @endcode
/// @note The \c thread_local keyword is a macro that maps to the corresponding
/// compiler directive (e.g. \c __declspec(thread)). While the C++0x standard
/// allows for non-trivial types (e.g. classes with constructors and
/// destructors) to be declared with the \c thread_local keyword, most pre-C++0x
/// compilers only allow for trivial types (e.g. \c int). So, to guarantee
/// portable code, only use trivial types for thread local storage.
/// @note This directive is currently not supported on Mac OS X (it will give
/// a compiler error), since compile-time TLS is not supported in the Mac OS X
/// executable format. Also, some older versions of MinGW (before GCC 4.x) do
/// not support this directive.
/// @hideinitializer
#if !defined(__NDKCPP0xCompiler) && !defined(thread_local)
#if defined(__GNUC__) || defined(__INTEL_COMPILER) || defined(__SUNPRO_CC) || defined(__IBMCPP__)
#define NDKThreadLocal __thread
#else
#define NDKThreadLocal __declspec(thread)
#endif
#endif

#ifndef UNREFERENCED_PARAMETER
#define UNREFERENCED_PARAMETER( p ) (p)
#endif

// size of of a chunk of the global string buffer for StringAtoms
#define _GLOBAL_STRINGBUFFER_CHUNKSIZE (32 * 1024)
#define _ENABLE_GLOBAL_STRINGBUFFER_GROWTH 1

/// Invalid index definition
#ifndef InvalidIndex
static const SSizeT InvalidIndex = -1;
#endif

#if defined( _MSC_VER )
// Needs to have dll-interface
//#pragma warning(disable: 4251)
#endif

/// Used by spin-waiting objects like fast semaphore or lock-less critical section
static const SizeT gCacheLine = 128;

//! A template to select either 32-bit or 64-bit constant as compile time, depending on machine word size.
//! Explicit cast is needed to avoid compiler warnings about possible truncation.
//! The value of the right size, which is selected by ?:, is anyway not truncated or promoted.
template< unsigned u, unsigned long long ull >
struct SelectSizeTConstant 
{
    static const SizeT value = (SizeT)( ( sizeof( size_t ) == sizeof( u ) ) ? u : ull );
};
static const SizeT gVenomValue = SelectSizeTConstant< 0xDEADBEEFU, 0xDDEEAADDDEADBEEFULL >::value;

/// Default Window class
#ifdef NDKEditorBuild
#define NANOENGINE_DEFAULT_CLASS "NanoEngine"
#else
#define NANOENGINE_DEFAULT_CLASS "NanoEngine"
#endif

/// Null pointer definition for single-type templates
template< typename T >
__IME_INLINED T* NullPointer( void )
{ 
	return nullptr; 
};

//! Basic padding with template selection
template< class T, int S >
struct PaddedBase : T 
{ 
	s8 pad[gCacheLine - sizeof( T ) % gCacheLine]; 
};

//! Basic padding with default "0" selection parameter
template< class T > 
struct PaddedBase< T, 0 > : T 
{
	/* Nothing */
};

//! Pads type T to fill out to a multiple of cache line size.
template< class T >
struct Padded : PaddedBase< T, sizeof( T ) > 
{ 
	/* Nothing */
};

class NoAssign
{
	void operator = ( const NoAssign& );
public:
};

class NoCopy : public NoAssign
{
	NoCopy( const NoCopy& );

public:
	NoCopy( void ) { };
};

#ifndef __IME_CHAR16
#	define __IME_CHAR16( s ) L ## s
#endif


//! Additive inverse of 1 for type T.
/** Various compilers issue various warnings if -1 is used with various integer types.
    The baroque expression below avoids all the warnings (we hope). */
#define NDKMakeMinusOne( x ) (x(x(0)-x(1)))

#include <Foundation/TypeTraits.h>
#include <Foundation/TypeConversion.h>
#include <Foundation/Math.h>
#include <Foundation/MemoryAllocator.h> /// Memory allocator
#include <Foundation/LowLevelMemory.h>	/// Low-Level Memory
#include <Foundation/Boolean.h> /// Boolean
#include <Foundation/Enumeration.h> /// Enumeration
#include <Foundation/DebugMessaging.h> /// Debug messaging system

/*
================================================================================================

	two-complements integer bit layouts

================================================================================================
*/

#define INT8_SIGN_BIT		7
#define INT16_SIGN_BIT		15
#define INT32_SIGN_BIT		31
#define INT64_SIGN_BIT		63

#define INT8_SIGN_MASK		( 1 << INT8_SIGN_BIT )
#define INT16_SIGN_MASK		( 1 << INT16_SIGN_BIT )
#define INT32_SIGN_MASK		( 1UL << INT32_SIGN_BIT )
#define INT64_SIGN_MASK		( 1ULL << INT64_SIGN_BIT )

/*
================================================================================================

	integer sign bit tests

================================================================================================
*/

#define OLD_INT32_SIGNBITSET(i)		(static_cast<const unsigned int>(i) >> INT32_SIGN_BIT)
#define OLD_INT32_SIGNBITNOTSET(i)	((~static_cast<const unsigned int>(i)) >> INT32_SIGN_BIT)

inline int INT32_SIGNBITSET( int i ) {
	int	r = OLD_INT32_SIGNBITSET( i );
	__IME_ASSERT( r == 0 || r == 1 );
	return r;
}

inline int INT32_SIGNBITNOTSET( int i ) {
	int	r = OLD_INT32_SIGNBITNOTSET( i );
	__IME_ASSERT( r == 0 || r == 1 );
	return r;
}
