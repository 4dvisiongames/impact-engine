//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: Singleton per-platform selection.
///
/// Platform: Win32/Xbox360, Linux, MacOSX
///
/// Author: Pavel Umnikov
//=====================================================================================

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _INanoSingleton_h_
#define _INanoSingleton_h_

#if defined( __IME_PLATFORM_WIN64 ) || defined( NDKPlatformWinRT ) || defined( NDKPlatformXbox360 )
/// Win32 singleton defintions
#include <Foundation/Win32/Win32Singleton.h>

#elif defined( __IME_PLATFORM_LINUX64 )
/// POSIX singleton definitions
#include <Foundation/Posix/PosixSingleton.h>

#elif defined( __IME_PLATFORM_OSX_INTEL )
/// OSX singleton definitions
#include <Foundation/OSX/OSXSingleton.h>

#else
#error "Singletons are not implemented on this platform!"
#endif

#endif /// _INanoSignelton_h_