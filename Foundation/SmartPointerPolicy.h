/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#include <Foundation/Interlocked.h>

/// Forward declaration
struct SmartPointerRelaxedOp { static const Threading::MemoryOrder value = Threading::MemoryOrderEnum::Relaxed; };
struct SmartPointerThreadSafeOp { static const Threading::MemoryOrder value = Threading::MemoryOrderEnum::Sequental; };

template< typename T, typename Policy > class SmartPointerPolicy;

template< typename T > 
class SmartPointerPolicy< T, SmartPointerRelaxedOp >
{
public:
	typedef T* Pointer;
	typedef const T* ConstPointer;
	typedef T& Reference;
	typedef const T& ConstReference;
	typedef SmartPointerRelaxedOp ThisOp;

	static Pointer AddRef( Pointer src );
	static Pointer Release( Pointer src );
	static Pointer Assign( Pointer toAssign, Pointer dst );
	static void SetPointer( Pointer src, Pointer& dst );
	static Pointer Access( Pointer src );
};

template< typename T >
__IME_INLINED typename SmartPointerPolicy< T, SmartPointerRelaxedOp >::Pointer SmartPointerPolicy< T, SmartPointerRelaxedOp >::AddRef( Pointer src )
{
	if( src )
	{
		src->AddRef< Threading::MemoryOrderEnum::Relaxed >();
	}

	return src;
}

template< typename T >
__IME_INLINED typename SmartPointerPolicy< T, SmartPointerRelaxedOp >::Pointer SmartPointerPolicy< T, SmartPointerRelaxedOp >::Release( Pointer src )
{
	if( src )
	{
		src->Release< Threading::MemoryOrderEnum::Relaxed >();
	}

	return src;
}

template< typename T >
__IME_INLINED typename SmartPointerPolicy< T, SmartPointerRelaxedOp >::Pointer SmartPointerPolicy< T, SmartPointerRelaxedOp >::Assign( Pointer toAssign, Pointer dst )
{
	if ( dst != toAssign )
	{
		if ( dst )
			dst->Release< Threading::MemoryOrderEnum::Relaxed >();

		dst = toAssign;

		if ( dst )
			dst->AddRef< Threading::MemoryOrderEnum::Relaxed >();
	}

	return dst;
}

template< typename T >
__IME_INLINED void SmartPointerPolicy< T, SmartPointerRelaxedOp >::SetPointer( Pointer src, Pointer& dst )
{
	dst = src;
}

template< typename T >
__IME_INLINED typename SmartPointerPolicy< T, SmartPointerRelaxedOp >::Pointer SmartPointerPolicy< T, SmartPointerRelaxedOp >::Access( Pointer src )
{
	return src;
}


template< typename T > 
class SmartPointerPolicy< T, SmartPointerThreadSafeOp >
{
public:
	typedef T* Pointer;
	typedef const T* ConstPointer;
	typedef T& Reference;
	typedef const T& ConstReference;
	typedef SmartPointerThreadSafeOp ThisOp;

	static Pointer AddRef( Pointer src );
	static Pointer Release( Pointer src );
	static Pointer Assign( Pointer toAssign, Pointer dst );
	static void SetPointer( Pointer src, Pointer& dst );
	static Pointer Access( Pointer src );
};

template< typename T >
__IME_INLINED typename SmartPointerPolicy< T, SmartPointerThreadSafeOp >::Pointer SmartPointerPolicy< T, SmartPointerThreadSafeOp >::AddRef( Pointer src )
{
	if( src )
	{
		src->AddRef< Threading::MemoryOrderEnum::Sequental >();
	}

	return src;
}

template< typename T >
__IME_INLINED typename SmartPointerPolicy< T, SmartPointerThreadSafeOp >::Pointer SmartPointerPolicy< T, SmartPointerThreadSafeOp >::Release( Pointer src )
{
	if( src )
	{
		src->Release< Threading::MemoryOrderEnum::Sequental >();
	}

	return src;
}

template< typename T >
__IME_INLINED typename SmartPointerPolicy< T, SmartPointerThreadSafeOp >::Pointer SmartPointerPolicy< T, SmartPointerThreadSafeOp >::Assign( Pointer toAssign, Pointer dst )
{
	if ( dst != toAssign )
	{
		if ( dst )
			dst->Release< Threading::MemoryOrderEnum::Sequental >();

		dst = toAssign;

		if ( dst )
			dst->AddRef< Threading::MemoryOrderEnum::Sequental >();
	}

	return dst;
}

template< typename T >
__IME_INLINED void SmartPointerPolicy< T, SmartPointerThreadSafeOp >::SetPointer( Pointer src, Pointer& dst )
{
	Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( dst, src );
}

template< typename T >
__IME_INLINED typename SmartPointerPolicy< T, SmartPointerThreadSafeOp >::Pointer SmartPointerPolicy< T, SmartPointerThreadSafeOp >::Access( Pointer src )
{
	return Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( src );
}