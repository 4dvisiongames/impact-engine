/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Smart pointer policy
#include <Foundation/SmartPointerPolicy.h>

/// <summary>
/// <c>IntrusivePtr</c> is a smart pointer system, uses thread-safe reference counting.
/// </summary>
/// <remarks>
/// Please notice, that class, which used with <c>IntrusivePtr</c> must support reference counting
/// through <c>Core::RefereceCounter</c> class.
/// </remarks>
template< typename Typed, typename Policy = SmartPointerPolicy< Typed, SmartPointerRelaxedOp > > 
class IntrusivePtr
{ 
	template< typename T, typename P > class IntrusiveWeakPtr;
	friend IntrusiveWeakPtr< Typed, Policy >;

public:
	typedef IntrusivePtr< Typed, Policy > ThisType;
	typedef Typed ValueType;
	typedef Typed& Reference;
	typedef const Typed& ConstReference;
	typedef Typed* Pointer;
	typedef const Typed* ConstPointer;
	typedef Policy PolicyType;

	/// <summary cref="IntrusivePtr::IntrusivePtr">
	/// Constructor.
	/// </summary>
	IntrusivePtr( void );

	/// <summary cref="IntrusivePtr::IntrusivePtr">
	/// Construct from raw C++ pointer.
	/// </summary>
	/// <param name="p">A pointer to the C++ raw class.</param>
	IntrusivePtr( Pointer p );

	/// <summary cref="IntrusivePtr::IntrusivePtr">
	/// Construct from existing smart pointer.
	/// </summary>
	/// <param name="p">Smart pointer.</param>
	IntrusivePtr( const ThisType& p );

	template< typename OtherType, typename OtherPolicy >
	IntrusivePtr( const IntrusivePtr< OtherType, OtherPolicy >& rhs );

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
	/// <summary cref="IntrusivePtr::IntrusivePtr">
	/// Movable constructor.
	/// </summary>
	IntrusivePtr( ThisType&& mov );

	template< typename OtherType, typename OtherPolicy >
	IntrusivePtr( IntrusivePtr< OtherType, OtherPolicy >&& rhs );
#endif /// __IME_CPP11_MOVE_SEMANTICS_PRESENT

	/// <summary cref="IntrusivePtr::~IntrusivePtr">
	/// Destructor.
	/// </summary>
	~IntrusivePtr( void );

	/// <summary cref="IntrusivePtr::operator=">
	/// Copy assignment operator.
	/// </summary>
	/// <param name="rhs">A reference to the smart pointer.</param>
	ThisType& operator = ( const ThisType& rhs );

	/// <summary cref="IntrusivePtr::operator=">
	/// Copy assignment operator.
	/// </summary>
	template< typename OtherType, typename OtherPolicy >
	ThisType& operator = ( const IntrusivePtr< OtherType, OtherPolicy >& rhs );

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
	/// <summary cref="IntrusivePtr::operator=">
	/// Movable assignment.
	/// </summary>
	void operator = ( ThisType&& mov );

	template< typename OtherType, typename OtherPolicy >
	void operator = ( IntrusivePtr< OtherType, OtherPolicy >&& mov );
#endif /// __IME_CPP11_MOVE_SEMANTICS_PRESENT

	/// <summary cref="IntrusivePtr::operator=">
	/// Assignment operator.
	/// </summary>
	/// <param name="rhs">A pointer to the C++ raw class.</param>
	ThisType& operator = ( Pointer rhs );
	/// equality operator
	bool operator == ( const ThisType& rhs ) const;
	/// inequality operator
	bool operator != ( const ThisType& rhs ) const;
	/// shortcut equality operator
	bool operator == ( ConstPointer rhs ) const;
	/// shortcut inequality operator
	bool operator != ( ConstPointer rhs ) const;
	/// addition operator
	ThisType& operator ++ ( void );
	/// safe -> operator
	Pointer operator -> ( void ) const;
	/// safe dereference operator
	Reference operator * ( void );
	/// safe dereference operator
	ConstReference operator * ( void ) const;
	/// safe pointer cast operator
	operator Pointer ( void );
	operator ConstPointer ( void ) const;
	/// boolean checking operator
	operator bool ( void ) const;

	/// type-safe downcast operator to other smart pointer
	template< typename DerivedClass, typename DerivedPolicy > 
	const typename IntrusivePtr< DerivedClass, DerivedPolicy >::ThisType& Downcast( void ) const;

	template< typename DerivedClass >
	const typename IntrusivePtr< DerivedClass, SmartPointerPolicy< DerivedClass, typename Policy::ThisOp > >::ThisType& Downcast( void ) const;

	/// type-safe upcast operator to other smart pointer
	template< typename BaseClass, typename DerivedPolicy > 
	const typename IntrusivePtr< BaseClass, DerivedPolicy >::ThisType& Upcast( void ) const;

	template< typename BaseClass >
	const typename IntrusivePtr< BaseClass, SmartPointerPolicy< BaseClass, typename Policy::ThisOp > >::ThisType& Upcast( void ) const;

	/// unsafe(!) cast to anything, unless classes have no inheritance-relationship,
	/// call upcast/downcast instead, they are type-safe
	template< typename OtherTypeClass, typename DerivedPolicy > 
	const typename IntrusivePtr< OtherTypeClass, DerivedPolicy >::ThisType& Cast( void ) const;

	template< typename OtherTypeClass >
	const typename IntrusivePtr< OtherTypeClass, SmartPointerPolicy< OtherTypeClass, typename Policy::ThisOp > >::ThisType& Cast( void ) const;

	/// check if pointer is valid
	bool IsValid( void ) const;
	/// return direct pointer (asserts if null pointer)
	Pointer Get( void );
	ConstPointer Get( void ) const;
	/// return direct pointer (returns null pointer)
	Pointer GetUnsafe( void );
	ConstPointer GetUnsafe( void ) const;

	/// Atomic compare and swap on smart pointer
	bool SwapCompared( const ThisType& value, const ThisType& comparand );

	template< typename ValType, typename ValPolicy, typename ComparandType, typename ComparandPolicy >
	bool SwapCompared( const IntrusivePtr< ValType, ValPolicy >& value, const IntrusivePtr< ComparandType, ComparandPolicy >& comparand );

private:
	Typed* myPointer; ///< Pointer of THIS class
};

namespace Traits
{
	template< typename U, typename V > struct IsSmartPointer< IntrusivePtr< U, V > > : public Traits::TrueType{};
}

template< typename Typed, typename Policy >
__IME_INLINED IntrusivePtr< Typed, Policy >::IntrusivePtr( void ) : myPointer( nullptr )
{
	/// Empty
}

template< typename Typed, typename Policy > 
__IME_INLINED IntrusivePtr< Typed, Policy >::IntrusivePtr( Typed* p ) : myPointer( nullptr )
{
	Pointer ptr = Policy::Access( p );
	ptr = PolicyType::AddRef( p );
	Policy::SetPointer( ptr, this->myPointer );
}

template< typename Typed, typename Policy > 
__IME_INLINED IntrusivePtr< Typed, Policy >::IntrusivePtr( const ThisType& p ) : myPointer( nullptr )
{
	Pointer ptr = Policy::Access( p.myPointer );
	ptr = PolicyType::AddRef( p.myPointer );
	PolicyType::SetPointer( ptr, this->myPointer );
}

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
template< typename Typed, typename Policy >
__IME_INLINED IntrusivePtr< Typed, Policy >::IntrusivePtr( ThisType&& mov ) : myPointer( nullptr )
{
	Pointer ptr = Policy::Access( mov.myPointer );
	Policy::SetPointer( ptr, this->myPointer );

	/// After moving, we need to make movable nullfied
	ptr = NullPointer< Typed >();
	Policy::SetPointer( mov.myPointer, ptr );
}
#endif /// __IME_CPP11_MOVE_SEMANTICS_PRESENT

template< typename Typed, typename Policy >
template< typename OtherType, typename OtherPolicy >
__IME_INLINED IntrusivePtr< Typed, Policy >::IntrusivePtr( const IntrusivePtr< OtherType, OtherPolicy >& rhs ) : myPointer( nullptr )
{
	Pointer ptr = const_cast< Pointer >( rhs.GetUnsafe() );
	ptr = Policy::AddRef( ptr );
	Policy::SetPointer( ptr, this->myPointer );
}

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
template< typename Typed, typename Policy >
template< typename OtherType, typename OtherPolicy >
__IME_INLINED IntrusivePtr< Typed, Policy >::IntrusivePtr( IntrusivePtr< OtherType, OtherPolicy >&& mov ) : myPointer( nullptr )
{
	Pointer ptr = Policy::Access( mov.myPointer );
	Policy::SetPointer( ptr, this->myPointer );

	/// After moving, we need to make movable nullfied
	ptr = NullPointer< Typed >();
	OtherPolicy::SetPointer( mov.myPointer, ptr );
}
#endif /// __IME_CPP11_MOVE_SEMANTICS_PRESENT

template< typename Typed, typename Policy >
__IME_INLINED IntrusivePtr< Typed, Policy >::~IntrusivePtr( void )
{
	PolicyType::Release( this->myPointer );
}

template< typename Typed, typename Policy >
__IME_INLINED typename IntrusivePtr< Typed, Policy >::ThisType& IntrusivePtr< Typed, Policy >::operator = ( Typed* rhs )
{
	Pointer src = Policy::Access( rhs );
	Pointer dst = Policy::Access( this->myPointer );
	dst = PolicyType::Assign( src, dst );
	Policy::SetPointer( dst, this->myPointer );
	return *this;
}

template< typename Typed, typename Policy >
__IME_INLINED typename IntrusivePtr< Typed, Policy >::ThisType& IntrusivePtr< Typed, Policy >::operator = ( const ThisType& rhs )
{
	Pointer src = Policy::Access( rhs.myPointer );
	Pointer dst = Policy::Access( this->myPointer );
	dst = PolicyType::Assign( src, dst );
	Policy::SetPointer( dst, this->myPointer );
	return *this;
}

template< typename Typed, typename Policy >
template< typename OtherTyped, typename OtherPolicy >
__IME_INLINED typename IntrusivePtr< Typed, Policy >::ThisType& IntrusivePtr< Typed, Policy >::operator = ( const IntrusivePtr< OtherTyped, OtherPolicy >& rhs )
{
	Pointer src =  const_cast< Pointer >( rhs.GetUnsafe() );
	Pointer dst = Policy::Access( this->myPointer );
	dst = PolicyType::Assign( src, dst );
	Policy::SetPointer( dst, this->myPointer );
	return *this;
}

#if __IME_CPP11_MOVE_SEMANTICS_PRESENT
template< typename Typed, typename Policy >
__IME_INLINED void IntrusivePtr< Typed, Policy >::operator = ( ThisType&& mov )
{
	Pointer src = Policy::Access( mov.myPointer );
	Pointer dst = Policy::Access( this->myPointer );
	dst = PolicyType::Assign( src, dst );
	Policy::SetPointer( dst, this->myPointer );
	
	/// After moving, we need to make movable nullfied
	src = NullPointer< Typed >();
	Policy::SetPointer( mov.myPointer, src );
}

template< typename Typed, typename Policy >
template< typename OtherTyped, typename OtherPolicy >
__IME_INLINED void IntrusivePtr< Typed, Policy >::operator = ( IntrusivePtr< OtherTyped, OtherPolicy >&& mov )
{
	Pointer src = Policy::Access( mov.myPointer );
	Pointer dst = Policy::Access( this->myPointer );
	dst = PolicyType::Assign( src, dst );
	Policy::SetPointer( dst, this->myPointer );
	
	/// After moving, we need to make movable nullfied
	src = NullPointer< Typed >();
	Policy::SetPointer( mov.myPointer, src );
}
#endif /// __IME_CPP11_MOVE_SEMANTICS_PRESENT

template< typename Typed, typename Policy >
__IME_INLINED bool IntrusivePtr< Typed, Policy >::operator == ( const ThisType& rhs ) const
{
	Pointer ptr1 = Policy::Access( rhs.myPointer );
	Pointer ptr2 = Policy::Access( this->myPointer );
	return ptr1 == ptr2;
}

template< typename Typed, typename Policy >
__IME_INLINED bool IntrusivePtr< Typed, Policy >::operator != ( const ThisType& rhs ) const
{
	Pointer ptr1 = Policy::Access( rhs.myPointer );
	Pointer ptr2 = Policy::Access( this->myPointer );
	return ptr1 != ptr2;
}

template< typename Typed, typename Policy >
__IME_INLINED bool IntrusivePtr< Typed, Policy >::operator == ( ConstPointer rhs ) const
{
	Pointer ptr1 = Policy::Access( const_cast< Pointer >( rhs ) );
	Pointer ptr2 = Policy::Access( this->myPointer );
	return ptr1 == ptr2;
}

template< typename Typed, typename Policy > 
__IME_INLINED bool IntrusivePtr< Typed, Policy >::operator != ( ConstPointer rhs ) const
{
	Pointer ptr1 = Policy::Access( const_cast< Pointer >( rhs ) );
	Pointer ptr2 = Policy::Access( this->myPointer );
	return ptr1 != ptr2;
}

template< typename Typed, typename Policy >
typename IntrusivePtr< Typed, Policy >::Pointer IntrusivePtr< Typed, Policy >::operator -> ( void ) const
{
	Pointer ptr = Policy::Access( this->myPointer );
	__IME_ASSERT_MSG( nullptr != ptr, "NULL pointer access in IntrusivePtr::operator->()!" );
	return ptr;
}

template< typename Typed, typename Policy >
__IME_INLINED typename IntrusivePtr< Typed, Policy >::Reference IntrusivePtr< Typed, Policy >::operator * ( void )
{
	Pointer ptr = Policy::Access( this->myPointer );
	__IME_ASSERT_MSG( nullptr != ptr, "NULL pointer access in IntrusivePtr::operator*()!" );
	return *ptr;
}

template< typename Typed, typename Policy >
__IME_INLINED typename IntrusivePtr< Typed, Policy >::ConstReference IntrusivePtr< Typed, Policy >::operator * ( void ) const
{
	Pointer ptr = Policy::Access( this->myPointer );
	__IME_ASSERT_MSG( nullptr != ptr, "NULL pointer access in IntrusivePtr::operator*()!" );
	return *ptr;
}

template< typename Typed, typename Policy >
IntrusivePtr< Typed, Policy >::operator Pointer ( void )
{
	Pointer ptr = Policy::Access( this->myPointer );
	return ptr;
}

template< typename Typed, typename Policy >
IntrusivePtr< Typed, Policy >::operator ConstPointer ( void ) const
{
	Pointer ptr = Policy::Access( this->myPointer );
	return ptr;
}

template< typename Typed, typename Policy >
IntrusivePtr< Typed, Policy >::operator bool( void ) const
{
	Pointer ptr = Policy::Access( this->myPointer );
	return ptr == nullptr;
}

template< typename Typed, typename Policy > 
template< typename DerivedClass, typename DerivedPolicy > 
__IME_INLINED const typename IntrusivePtr< DerivedClass, DerivedPolicy >::ThisType& IntrusivePtr< Typed, Policy >::Downcast( void ) const
{
	typedef typename IntrusivePtr< DerivedClass, DerivedPolicy >::ThisType DerivedType;
#if defined(DEBUG)
	// if DERIVED is not a derived class of TYPE, compiler complains here
	// compile-time inheritance-test
	const DerivedClass *derived = static_cast< const DerivedClass* >( this->myPointer );
	UNREFERENCED_PARAMETER( derived );
#endif
	return *reinterpret_cast< const DerivedType* >( this );
}

template< typename Typed, typename Policy > 
template< typename DerivedClass > 
__IME_INLINED const typename IntrusivePtr< DerivedClass, SmartPointerPolicy< DerivedClass, typename Policy::ThisOp > >::ThisType& IntrusivePtr< Typed, Policy >::Downcast( void ) const
{
	return this->Downcast< DerivedClass, SmartPointerPolicy< DerivedClass, typename Policy::ThisOp > >();
}

template< typename Typed, typename Policy > 
template< typename BaseClass, typename DerivedPolicy > 
__IME_INLINED const typename IntrusivePtr< BaseClass, DerivedPolicy >::ThisType& IntrusivePtr< Typed, Policy >::Upcast( void ) const
{
	typedef typename IntrusivePtr< BaseClass, DerivedPolicy >::ThisType BaseType;
#if defined(DEBUG)
	// if BASE is not a base-class of TYPE, compiler complains here
	// compile-time inheritance-test
	const BaseClass *base = static_cast< const BaseClass* >( this->myPointer );
	UNREFERENCED_PARAMETER( base );
#endif
	return *reinterpret_cast< const BaseType* >( this );
}

template< typename Typed, typename Policy > 
template< typename BaseClass > 
__IME_INLINED const typename IntrusivePtr< BaseClass, SmartPointerPolicy< BaseClass, typename Policy::ThisOp > >::ThisType& IntrusivePtr< Typed, Policy >::Upcast( void ) const
{
	return this->Upcast< BaseClass, SmartPointerPolicy< BaseClass, typename Policy::ThisOp > >();
}

template< typename Typed, typename Policy > 
template< typename OtherTypeClass, typename OtherTypePolicy > 
__IME_INLINED const typename IntrusivePtr< OtherTypeClass, OtherTypePolicy >::ThisType& IntrusivePtr< Typed, Policy >::Cast( void ) const
{
	typedef typename IntrusivePtr< OtherTypeClass, OtherTypePolicy >::ThisType OtherPtrType;
	// note: this is an unsafe cast
	return *reinterpret_cast< const OtherPtrType* >( this );
}

template< typename Typed, typename Policy > 
template< typename OtherTypeClass > 
__IME_INLINED const typename IntrusivePtr< OtherTypeClass, SmartPointerPolicy< OtherTypeClass, typename Policy::ThisOp > >::ThisType& IntrusivePtr< Typed, Policy >::Cast( void ) const
{
	return this->Cast< OtherTypeClass, SmartPointerPolicy< OtherTypeClass, typename Policy::ThisOp > >();
}

template< typename Typed, typename Policy >  
bool IntrusivePtr< Typed, Policy >::IsValid( void ) const
{
	Pointer ptr = Policy::Access( this->myPointer );
	return ptr != nullptr;
}

template< typename Typed, typename Policy > 
typename IntrusivePtr< Typed, Policy >::Pointer IntrusivePtr< Typed, Policy >::Get( void )
{
	Pointer ptr = Policy::Access( this->myPointer );
	__IME_ASSERT_MSG( ptr != nullptr, "NULL pointer access in IntrusivePtr<Typed>::get()!" );
	return ptr;
}

template< typename Typed, typename Policy > 
typename IntrusivePtr< Typed, Policy >::ConstPointer IntrusivePtr< Typed, Policy >::Get( void ) const
{
	Pointer ptr = Policy::Access( this->myPointer );
	__IME_ASSERT_MSG( ptr != nullptr, "NULL pointer access in IntrusivePtr<Typed>::get()!" );
	return ptr;
}

template< typename Typed, typename Policy > 
typename IntrusivePtr< Typed, Policy >::Pointer IntrusivePtr< Typed, Policy >::GetUnsafe( void )
{
	return PolicyType::Access( this->myPointer );
}

template< typename Typed, typename Policy > 
typename IntrusivePtr< Typed, Policy >::ConstPointer IntrusivePtr< Typed, Policy >::GetUnsafe( void ) const
{
	return PolicyType::Access( this->myPointer );
}

template< typename Typed, typename Policy > 
bool IntrusivePtr< Typed, Policy >::SwapCompared( const ThisType& value, const ThisType& comparand )
{
	if( *this == comparand ) 
	{
		*this = value;
		return true;
	}
	else 
	{
		return false;
	}
}

template< typename Typed, typename Policy >
template< typename ValType, typename ValPolicy, typename ComparandType, typename ComparandPolicy >
bool IntrusivePtr< Typed, Policy >::SwapCompared( const IntrusivePtr< ValType, ValPolicy >& value, const IntrusivePtr< ComparandType, ComparandPolicy >& comparand )
{
	if( this == comparand ) 
	{
		this = value;
		return true;
	} 
	else 
	{
		return false;
	}
}