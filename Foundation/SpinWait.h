/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// System
#include <Foundation/System.h>
/// Atomic backoff
#include <Foundation/AtomicBackoff.h>

namespace Threading
{
	/// <summary>
	/// SpinWait provides spin-waiting functionality to prevent kernel-level locking.
	/// </summary>
	/// <remarks>
	/// This particular locking scheme performs well  when lock contention is low, as the while loop overhead is small
	/// and locks are acquired very quickly, but degrades as many callers want the lock and most threads are doing a
	/// lot of interlocked spinning. There are also no guarantees that a caller will ever acquire the lock
	/// </remarks>
	class SpinWait
	{
		typedef AtomicBackoff< BackoffStrategyPause, BackoffStrategyYield > SpinWaitBackoff;

	public:
		/// <summary cref="SpinWait::SpinWait">
		/// </summary>
		SpinWait();

		/// <summary cref="SpinWait::Lock">
		/// Start spin waiting.
		/// </summary>
		void Lock();

		/// <summary cref="SpinWait::TryLock">
		/// Try acquiring lock (non-blocking) and return immediatelly.
		/// </summary>
		/// <returns>True if lock acquired; false otherwise.</returns>		
		bool TryLock();

		/// <summary cref="SpinWait::Unlock">
		/// Stop spin wating.
		/// </summary>
		void Unlock();

	private:
		u8 myLock;
	};

	__IME_INLINED SpinWait::SpinWait() 
		: myLock(0) 
	{
		/* Nothing */
	};

	__IME_INLINED void SpinWait::Lock( void )
	{
		if( !this->TryLock() ) 
		{
			SpinWaitBackoff b;
			do 
			{
				b.Pause();
			} while( !this->TryLock() );
		}
	}

	__IME_INLINED bool SpinWait::TryLock( void )
	{
		/// [Codepoet]: it's vital to cast 0 to u8, because compiler interprets it as u64 and
		/// in 64-bit build this method result is unexpected.
		return Interlocked::CompareAndSwap< u8, u8, u8 >( this->myLock, 1, 0 ) == static_cast<u8>(0);
	}

	__IME_INLINED void SpinWait::Unlock( void )
	{
		Interlocked::Store< MemoryOrderEnum::Release, u8, u8 >( this->myLock, 0 );
	}
}

/*
	change log:

	21-Mar-15		codepoet		method SpinWait::Lock		changed AtomicBackoff declaration to AtomicBackoff<> to accomodate changes
	19-Apr-15		codepoet		class SpinWait				rewritten code to accomodate changes to new AtomicBackoff
	04-Jul-15		codepoet		file						removed ifdef-define Guard
*/