/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Stream
#include <Foundation/Stream.h>

/// Input/Ouput namespace
namespace FileSystem
{
	Stream::Stream( void ) : bIsOpen( false ), bIsMapped( false ), mAccessMode( AccessModeEnum::ReadOnly ), mAccessPattern( AccessPatternEnum::Sequential )
	{
		/// Empty
	}

	Stream::~Stream( void )
	{
		__IME_ASSERT( !this->IsMapped() );
		__IME_ASSERT( !this->IsOpen() );
	}

	bool Stream::CanRead( void ) const
	{
		return false;
	}

	bool Stream::CanWrite( void ) const
	{
		return false;
	}

	bool Stream::CanSeek( void ) const
	{
		return false;
	}

	bool Stream::CanBeMapped( void ) const
	{
		return false;
	}

	void Stream::SetSize( Size s )
	{
		__IME_ASSERT( !this->IsOpen() );
		UNREFERENCED_PARAMETER( s );
	}

	Size Stream::GetSize( void ) const
	{
		return NULL;
	}

	Position Stream::GetPosition( void ) const
	{
		return NULL;
	}

	void Stream::SetAccessMode( AccessMode m )
	{
		__IME_ASSERT( !this->IsOpen() );
		this->mAccessMode = m;
	}

	AccessMode Stream::GetAccessMode( void ) const
	{
		return this->mAccessMode;
	}

	void Stream::SetAccessPattern( AccessPattern m )
	{
		__IME_ASSERT( !this->IsOpen() );
		this->mAccessPattern = m;
	}

	AccessPattern Stream::GetAccessPattern( void ) const
	{
		return this->mAccessPattern;
	}

	bool Stream::Open( void )
	{
		__IME_ASSERT( !this->IsOpen() );
		this->bIsOpen = true;
		return true;
	}

	void Stream::Close( void )
	{
		__IME_ASSERT( this->IsOpen() );
		this->bIsOpen = false;
	}

	void Stream::Write( const void* ptr, Size numBytes )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		UNREFERENCED_PARAMETER( ptr );
		UNREFERENCED_PARAMETER( numBytes );
	}

	Size Stream::Read( void* ptr, Size numBytes )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		UNREFERENCED_PARAMETER( ptr );
		UNREFERENCED_PARAMETER( numBytes );

		return NULL;
	}

	void Stream::Seek( Offset offset, SeekOrigin origin )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
		UNREFERENCED_PARAMETER( offset );
		UNREFERENCED_PARAMETER( origin );
	}

	void Stream::Flush( void )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );
	}

	bool Stream::Eof( void ) const
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( !this->IsMapped() );

		return true;
	}

	void* Stream::Map( void )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( this->CanBeMapped() );
		__IME_ASSERT( !this->IsMapped() );

		this->bIsMapped = true;
		return 0x0;
	}

	void Stream::Unmap( void )
	{
		__IME_ASSERT( this->IsOpen() );
		__IME_ASSERT( this->CanBeMapped() );
		__IME_ASSERT( this->IsMapped() );

		this->bIsMapped = false;
	}
}