/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _NanoStream_h_
#define _NanoStream_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// File system primitives
#include <Foundation/FileSystemTypes.h>

/// Resources namespace
namespace FileSystem
{
	/// <summary>
	/// <c>AccessMode</c> contains definitions: how stream must be opened.
	/// </summary>
	struct AccessModeEnum
	{
		enum List
		{
			ReadOnly, //!< Reading permissions, fail on writing
			WriteOnly, //!< Writing permissions, fail on reading
			ReadWrite, //!< Reading and writing permissions
			Append, //!< Append access
		};
	};
	typedef AccessModeEnum::List AccessMode;

	/// <sumamry>
	/// <c>AccessPattern</c> is an accessing pattern of current file.
	/// </summary>
	struct AccessPatternEnum
	{
		enum List
		{
			Random, //!< Randow file access - best working with multithreading
			Sequential, //!< Sequential file access
		};
	};
	typedef AccessPatternEnum::List AccessPattern;

	/// <summary>
	/// <c>SeekOrigin</c> is a seeking origin defintion for a stream.
	/// </summary>
	struct SeekOriginEnum
	{
		enum List
		{
			Begin, //!< Seek file from begining
			Current, //!< Seek file from current position
			End, //!< Seek file from the end
		};
	};
	typedef SeekOriginEnum::List SeekOrigin;

	/// <summary>
	/// <c>Stream</c> is a core class definition for other streams.
	/// </summary>
	class Stream : public Core::ReferenceCount
	{
	protected:
		bool bIsOpen; //!< Is file opened?
		bool bIsMapped; //!< Is currently mapped to memory?
		Enumeration< SizeT, AccessMode > mAccessMode; //!< Opening mode of current stream
		Enumeration< SizeT, AccessPattern > mAccessPattern; //!< Access pattern

	public:
		/// <summary cref="Stream::Stream">
		/// Constructor.
		/// </summary>
		Stream( void );

		/// <summary cref="Stream::~Stream">
		/// Destructor.
		/// </summary>
		virtual ~Stream( void );

		/// <summary cref="Stream::CanRead">
		/// Check if we can read from stream.
		/// </summary>
		/// <returns>True if the stream supports reading.</returns>
		virtual bool CanRead( void ) const;

		/// <summary cref="Stream::CanWrite">
		/// Check if we can write to the stream.
		/// </summary>
		/// <returns>True if the stream supports writing.</returns>
		virtual bool CanWrite( void ) const;

		/// <summary cref="Stream::CanSeek">
		/// Check if current stream supports seeking.
		/// </summary>
		/// <returns>True if the stream supports seeking.</returns>
		virtual bool CanSeek( void ) const;

		/// <summary cref="Stream::CanBeMapped">
		/// Check if we can map current stream to read/write on CPU.
		/// </summary>
		/// <returns>True if the stream provides direct memory access.</returns>
		virtual bool CanBeMapped( void ) const;

		/// <summary cref="Stream::SetSize">
		/// Set a new size for the stream.
		/// </summary>
		/// <param name="s">Size in bytes.</param>
		virtual void SetSize( Size s );

		/// <summary cref="Stream::GetSize">
		/// Get the size of the stream in bytes.
		/// </summary>
		/// <returns>Size of stream.</returns>
		virtual Size GetSize( void ) const;

		/// <summary cref="Stream::GetPosition">
		/// Get the current position of the read/write cursor.
		/// </summary>
		/// <returns>Position of stream in memory.</param>
		virtual Position GetPosition( void ) const;

		/// <summary cref="Stream::SetAccessMode">
		/// Set the access mode of the stream (default is ReadAccess).
		/// </summary>
		/// <param name="m">Access mode.</param>
		void SetAccessMode( AccessMode m );

		/// <summary cref="Stream::GetAccessMode">
		/// Get the access mode of the stream.
		/// </summary>
		/// <returns>Current access mode
		AccessMode GetAccessMode( void ) const;

		/// <summary cref="Stream::SetAccessPattern">
		/// Set the prefered access pattern (default is Sequential).
		/// </summary>
		/// <param name="p">Access pattern.</param>
		void SetAccessPattern( AccessPattern p );

		/// <summary cref="Stream::GteAccessPattern">
		/// Get the prefered access pattern.
		/// </summary>
		/// <returns>Access pattern.</param>
		AccessPattern GetAccessPattern( void ) const;

		/// <summary cref="Stream::Open">
		/// Open the stream.
		/// </summary>
		virtual bool Open( void );

		/// <summary cref="Stream::Close">
		/// Close the stream.
		/// </summary>
		virtual void Close( void );

		/// <summary cref="Stream::IsOpen">
		/// Check if stream has been opened.
		/// </summary>
		/// <returns>True if currently open, otherwise.</returns>
		bool IsOpen( void ) const;

		/// <summary cref="Stream::Write">
		/// Directly write to the stream.
		/// </summary>
		/// <param name="ptr">A pointer to the stream.</param>
		/// <param name="numBytes">Size of data.</param>
		virtual void Write( const void* ptr, Size numBytes );

		/// <summary cref="Stream::Read">
		/// Directly read from the stream.
		/// </summary>
		/// <param name="ptr">A pointer to the data where we read from.</param>
		/// <param name="numBytes">Size of data.</param>
		/// <returns>Read data in bytes.</returns>
		virtual Size Read( void* ptr, Size numBytes );

		/// <summary cref="Stream::Seek">
		/// Seek in stream.
		/// </summary>
		/// <param name="offset">Offset in bytes.</param>
		/// <param name="origin">Origin in data.</param>
		virtual void Seek( Offset offset, SeekOrigin origin );

		/// <summary cref="Stream::Flush">
		/// Flush unsaved data.
		/// </summary>
		virtual void Flush( void );

		/// <summary cref="Stream::Eof">
		/// End of reading.
		/// </summary>
		/// <returns>True if end-of-stream reached, otherwise false.</returns>
		virtual bool Eof( void ) const;

		/// <summary cref="Stream::Map">
		/// Map stream to memory.
		/// </summary>
		/// <returns>A pointer to the data in stream.</returns>
		virtual void* Map( void );

		/// <summary cref="Stream::Unmap">
		/// Unmap stream.
		/// </summary>
		virtual void Unmap( void );

		/// <summary cref="Stream::IsMapped">
		/// Check if current steam mapped.
		/// </summary>
		/// <returns>True if stream is currently mapped to memory, otherwise false.</returns>
		bool IsMapped( void ) const;
	};

	__IME_INLINED bool Stream::IsOpen( void ) const
	{
		bool result = this->bIsOpen;
		return result;
	}

	__IME_INLINED bool Stream::IsMapped( void ) const
	{
		bool result = this->bIsMapped;
		return result;
	}
}

#endif /// _NanoStream_h_