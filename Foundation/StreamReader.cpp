/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Stream reader
#include <Foundation/StreamReader.h>

/// FileSystem sub-system namespace
namespace FileSystem
{
	StreamReader::StreamReader( void ) : bIsOpen( false ), bStreamWasOpen( false )
	{
		/// Empty
	}

	StreamReader::~StreamReader( void )
	{
		__IME_ASSERT( !this->IsOpen() );
	}

	void StreamReader::SetInput( const IntrusivePtr< Stream >& rhs )
	{
		this->pInputStream = rhs;
	}
	
	const IntrusivePtr< Stream >& StreamReader::GetInput( void ) const
	{
		return this->pInputStream;
	}

	bool StreamReader::HasStream( void ) const
	{
		bool result = this->pInputStream.IsValid();
		return result;
	}

	bool StreamReader::Eof( void ) const
	{
		/// Assertion
		__IME_ASSERT( this->IsOpen() );
		bool result = this->pInputStream->Eof();
		return result;
	}

	bool StreamReader::Open( void )
	{
		__IME_ASSERT( !this->IsOpen() );
		__IME_ASSERT( this->pInputStream.IsValid() );
		__IME_ASSERT( this->pInputStream->CanRead() );

		if( this->pInputStream->IsOpen() ) {
			__IME_ASSERT( (this->pInputStream->GetAccessMode() == AccessModeEnum::ReadOnly) || (this->pInputStream->GetAccessMode() == AccessModeEnum::ReadWrite) );
			this->bStreamWasOpen = true;
			this->bIsOpen = true;
		} else {
			this->pInputStream->SetAccessMode( AccessModeEnum::ReadOnly );
			this->bIsOpen = this->pInputStream->Open();
		}

		return this->IsOpen();
	}

	void StreamReader::Close( void )
	{
		__IME_ASSERT( this->IsOpen() );

		if( (!this->bStreamWasOpen) && (this->pInputStream->IsOpen()) )
			this->pInputStream->Close();

		this->bIsOpen = false;
	}
}