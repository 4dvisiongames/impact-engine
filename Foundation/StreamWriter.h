/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _NanoStreamWriter_h_
#define _NanoStreamWriter_h_

/// Stream
#include <Foundation/Stream.h>

/// FileSystem sub-system namespace
namespace FileSystem
{
	class StreamWriter : public Core::ReferenceCount
	{
		IntrusivePtr< Stream > pInputStream; //!< Stream to read from
		bool bIsOpen; //!< Is reading session opened
		bool bStreamWasOpen; //!< Indicator if stream was opened

	public:
		/// <summary cref="StreamWriter::StreamWriter">
		/// Constructor.
		/// </summary>
		StreamWriter( void );

		/// <summary cref="StreamWriter::~StreamWriter">
		/// Destructor.
		/// </summary>
		virtual ~StreamWriter( void );

		/// <summary cref="StreamWriter::SetInput">
		/// Setup input stream.
		/// </summary>
		/// <param name="rhs">Reference to the input stream to write to.</param>
		void SetInput( const IntrusivePtr< Stream >& rhs );

		/// <summary cref="StreamWriter::GetStream">
		/// Get input stream.
		/// </summary>
		/// <returns>Reference to the input stream.</returns>
		const IntrusivePtr< Stream >& GetInput( void ) const;

		/// <summary cref="StreamWriter::HasStream">
		/// Check if we have an attached stream.
		/// </summary>
		/// <returns>True if stream exists, otherwise false.</returns>
		bool HasStream( void ) const;

		/// <summary cref="StreamWriter::Eof">
		/// Check end of file.
		/// </summary>
		/// <returns>True if it's end of file, otherwise false.</returns>
		bool Eof( void ) const;

		/// <summary cref="StreamWriter::Open">
		/// Open stream for writing.
		/// </summary>
		/// <returns>True if stream reader opened successfull.</returns>
		virtual bool Open( void );

		/// <summary cref="StreamWriter::Close">
		/// Close stream from writing.
		/// </summary>
		virtual void Close( void );

		/// <summary cref="StreamWriter::IsOpen">
		/// Check if we have opened this stream.
		/// </summary>
		/// <returns>True if stream is opened, otherwsie false.</returns>
		bool IsOpen( void ) const;
	};

	__IME_INLINED bool StreamWriter::IsOpen( void ) const
	{
		bool result = this->bIsOpen;
		return result;
	}
}

#endif /// _NanoStreamWriter_h_