/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Engine shared code
#include <Foundation/Shared.h>
/// Atomic string
#include <Foundation/StringAtom.h>
/// Base atomic string table
#include <Foundation/StringAtomBase.h>
/// Global atomic string table
#include <Foundation/GlobalStringAtomTable.h>
/// Local atomic string table
#include <Foundation/LocalStringAtomTable.h>

/// C-Runtime string 
#include <string.h>

/// Standard template library
namespace STL
{
	void StringAtom::Setup(const s8* str)
	{
#if _ENABLE_THREADLOCAL_STRINGATOM_TABLES
		// first check our thread-local string atom table whether the string
		// is already registered there, this does not require any thread
		// synchronisation
		LocalStringAtomTable* localTable = LocalStringAtomTable::Instance();
		this->content = localTable->Find(str);
		if (0 != this->content)
		{
			// yep, the string is in the local table, we're done
			return;
		}
	#endif

		// the string wasn't in the local table (or thread-local tables are disabled), 
		// so we need to check the global table, this involves thread synchronisation
		GlobalStringAtomTable* globalTable = GlobalStringAtomTable::Instance();
		globalTable->Lock();
		this->content = globalTable->Find(str);
		if (0 == this->content)
		{
			// hrmpf, string isn't in global table either yet, so add it...
			this->content = globalTable->Add(str);
		}
		globalTable->Unlock();

#if _ENABLE_THREADLOCAL_STRINGATOM_TABLES
		// finally, add the new string to our local table as well, so the
		// next lookup from our thread of this string will be faster
		localTable->Add(this->content);
	#endif
	}

	bool StringAtom::operator==(const s8* rhs) const
	{
		if (0 == this->content)
		{
			return false;
		}
		else
		{
			return (0 == strcmp(this->content, rhs));
		}
	}

	bool StringAtom::operator!=(const s8* rhs) const
	{
		if (0 == this->content)
		{
			return false;
		}
		else
		{
			return (0 == strcmp(this->content, rhs));
		}
	}

} // namespace STL