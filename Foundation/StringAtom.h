/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

/// String
#include <Foundation/STLString.h>

/// Standard template library
namespace STL
{
	/// Atomic string
	class StringAtom
	{
	public:
		/// Default constructor
		StringAtom();

		/// Copy constructor
		StringAtom(const StringAtom& rhs);

		/// Construct from s8 ptr
		StringAtom(s8* ptr);

		/// Construct from s8 ptr
		StringAtom(const s8* ptr);

		/// Construct from s8 ptr
		StringAtom(u8* ptr);

		/// Construct from s8 ptr
		StringAtom(const u8* ptr);

		/// Construct from string object
		StringAtom(const String< Char8 >& str);

		/// Assignment
		void operator=(const StringAtom& rhs);

		/// Assignment from s8 ptr
		void operator=(const s8* ptr);

		/// Assignment from string object
		void operator=(const String< Char8 >& str);

		/// Equality operator
		bool operator==(const StringAtom& rhs) const;

		/// Inequality operator
		bool operator!=(const StringAtom& rhs) const;

		/// Greater-then operator
		bool operator>(const StringAtom& rhs) const;

		/// Less-then operator
		bool operator<(const StringAtom& rhs) const;

		/// Greater-or-equal operator
		bool operator>=(const StringAtom& rhs) const;

		/// Less-or-equal operator
		bool operator<=(const StringAtom& rhs) const;

		/// Equality with s8* (SLOW!)
		bool operator==(const Char8* rhs) const;

		/// Inequality with s8* (SLOW!)
		bool operator!=(const Char8* rhs) const;

		/// Equality with string object (SLOW!)
		bool operator==(const String< Char8 >& rhs) const;

		/// Inequality with string object (SLOW!)
		bool operator!=(const String< Char8 >& rhs) const;

		/// clear content (becomes invalid)
		void Clear( void );

		/// return true if valid (contains a non-empty string)
		bool IsValid( void ) const;

		/// get contained string as s8 ptr (fast)
		const Char8* Value( void ) const;

		/// get containted string as string object (SLOW!!!)
		String< Char8 > AsString( void ) const;

	private:
		/// setup the string atom from a string pointer
		void Setup(const s8* str);

		const s8* content; //!< Contents of atomic string
	};

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline
	StringAtom::StringAtom() :
		content(0)
	{
		// empty
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline
	StringAtom::StringAtom(const StringAtom& rhs) :
		content(rhs.content)
	{
		// empty
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	StringAtom::StringAtom(s8* str)
	{
		if (0 != str)
		{
			this->Setup(str);
		}
		else
		{
			this->content = 0;
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	StringAtom::StringAtom(const s8* str)
	{
		if (0 != str)
		{
			this->Setup(str);
		}
		else
		{
			this->content = 0;
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	StringAtom::StringAtom(u8* str)
	{
		if (0 != str)
		{
			this->Setup((const s8*)str);
		}
		else
		{
			this->content = 0;
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	StringAtom::StringAtom(const u8* str)
	{
		if (0 != str)
		{
			this->Setup((const s8*)str);
		}
		else
		{
			this->content = 0;
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline
	StringAtom::StringAtom(const String< Char8 >& str)
	{
		this->Setup(str.AsCharPtr());
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline void
	StringAtom::operator=(const StringAtom& rhs)
	{
		this->content = rhs.content;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline void
	StringAtom::operator=(const s8* str)
	{
		if (0 != str)
		{
			this->Setup(str);
		}
		else
		{
			this->content = 0;
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	inline void
	StringAtom::operator=(const String< Char8 >& str)
	{
		this->Setup(str.AsCharPtr());
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline bool
	StringAtom::operator==(const StringAtom& rhs) const
	{
		return this->content == rhs.content;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline bool
	StringAtom::operator!=(const StringAtom& rhs) const
	{
		return this->content != rhs.content;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline bool
	StringAtom::operator>(const StringAtom& rhs) const
	{
		return this->content > rhs.content;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline bool
	StringAtom::operator<(const StringAtom& rhs) const
	{
		return this->content < rhs.content;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline bool
	StringAtom::operator>=(const StringAtom& rhs) const
	{
		return this->content >= rhs.content;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline bool
	StringAtom::operator<=(const StringAtom& rhs) const
	{
		return this->content <= rhs.content;
	}

	//------------------------------------------------------------------------------
	/**
		Compare with String object. Careful, slow!
	*/
	inline bool
	StringAtom::operator==(const String< Char8 >& rhs) const
	{
		if (0 == this->content)
		{
			return false;
		}
		else
		{
			return (rhs == this->content);
		}
	}

	//------------------------------------------------------------------------------
	/**
		Compare with String object. Careful, slow!
	*/
	inline bool
	StringAtom::operator!=(const String< Char8 >& rhs) const
	{
		if (0 == this->content)
		{
			return false;
		}
		else
		{
			return (rhs != this->content);
		}
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline void
	StringAtom::Clear()
	{
		this->content = 0;
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline bool
	StringAtom::IsValid() const
	{
		return (0 != this->content) && (0 != this->content[0]);
	}

	//------------------------------------------------------------------------------
	/**
	*/
	__forceinline const s8*
	StringAtom::Value() const
	{
		return this->content;
	}

	//------------------------------------------------------------------------------
	/**
		SLOW!!!
	*/
	inline String< Char8 > StringAtom::AsString( void ) const
	{
		return String< Char8 >(this->content);
	}
} // namespace STL
//------------------------------------------------------------------------------
