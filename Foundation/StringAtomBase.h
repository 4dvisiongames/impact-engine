/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _StringAtomBase_h_
#define _StringAtomBase_h_

/// Array
#include <Foundation/STLArray.h>

/// Standard template library namespace
namespace STL
{
	/// <summary>
	/// <c>StringAtomTableBase</c> is an atomic string table definition.
	/// </summary>
	class StringAtomTableBase
	{
	public:
		/// <summary cref="StringAtomTableBase::StringAtomTableBase">
		/// Constructor
		/// </summary>
		StringAtomTableBase();

		/// <summary cref="StringAtomTableBase::~StringAtomTableBase">
		/// Destructor
		/// </summary>
		~StringAtomTableBase();

	protected:
		friend class StringAtom; //!< Codepoet: For easier access

		/// <summary cref="StringAtomTableBase::Find">
		/// Find a string pointer in the atom table.
		/// </summary>
		/// <param name="str">What to search.</param>
		/// <returns>Found string inside table.</returns>
		const s8* Find(const s8* str) const;

		/// <summary>
		/// <c>StaticString</c> is a static string class for sorting the array.
		/// </summary>
		/// <notes>
		/// Add explicit thread-safetness, as mentioned before.
		/// </notes>
		struct StaticString
		{
			/// <summary cref="StaticString::operator==">
			/// Equality operator
			/// </summary>
			bool operator==(const StaticString& rhs) const;

			/// <summary cref="StaticString::operator!=">
			/// Inequality operator
			/// </summary>
			bool operator!=(const StaticString& rhs) const;

			/// <summary cref="StaticString::operator<">
			/// Less-then operator
			/// </summary>
			bool operator<(const StaticString& rhs) const;

			/// <summary cref="StaticString::operator">
			/// Greater-then operator
			/// </summary>
			bool operator>(const StaticString& rhs) const;

			s8* ptr; //!< Pointer to saved data
		};

		STL::Array< StaticString > table; //!< Array of static strings
	};
} // namespace STL

#endif /// _STLStringAtomBase_h_
