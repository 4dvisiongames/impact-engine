/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Shared code
#include <Foundation/Shared.h>
/// String buffer
#include <Foundation/StringBuffer.h>

/// C-Runtime string definitions
#include <string.h>

/// STL namespace
namespace STL
{
	StringBuffer::StringBuffer() : chunkSize(0), curPointer(0)
	{
		// empty
	}    

	StringBuffer::~StringBuffer()
	{
		if (this->IsValid())
			this->Discard();
	}

	void StringBuffer::Setup(SizeT size)
	{
		__IME_ASSERT(!this->IsValid());
		__IME_ASSERT(size > 0);
		this->chunkSize = size;    
		this->AllocNewChunk();
	}

	void StringBuffer::Discard()
	{
		__IME_ASSERT(this->IsValid());
		SizeT i;
		for (i = 0; i < this->chunks.Size(); i++)
		{
			/// Free current heap of string
			Memory::Allocator::Free( this->chunks[i] );
			this->chunks[i] = 0;
		}
		this->chunks.Clear();
		this->chunkSize = 0;
		this->curPointer = 0;
	}

	void StringBuffer::AllocNewChunk()
	{
		s8* newChunk = static_cast< s8* >( Memory::Allocator::Malloc( this->chunkSize ) );
		this->chunks.Append(newChunk);
		this->curPointer = newChunk;
	}

	const s8* StringBuffer::AddString(const s8* str)
	{
		__IME_ASSERT(0 != str);
		__IME_ASSERT(this->IsValid());

		// get string length, must be less then chunk size
		SizeT strLength = strlen(str) + 1;
		__IME_ASSERT(strLength < this->chunkSize);

		// check if a new buffer must be allocated
		if ((this->curPointer + strLength) >= (this->chunks.Back() + this->chunkSize))
		{
#if _ENABLE_GLOBAL_STRINGBUFFER_GROWTH
			this->AllocNewChunk();
#else
			Core::System::Error("String buffer full when adding string '%s' (string buffer growth is disabled)!\n", str);
#endif
		}

		// copy string into string buffer
		s8* dstPointer = this->curPointer;
		strcpy(dstPointer, str);
		this->curPointer += strLength;
		return dstPointer;
	}
}; // namespace STL