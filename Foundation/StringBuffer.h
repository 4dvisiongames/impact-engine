/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _StringBuffer_h_
#define _StringBuffer_h_

/// Shared code
#include <Foundation/Shared.h>
/// Array templates
#include <Foundation/STLArray.h>

/// Standard template library
namespace STL
{
	/// String buffer definitions
	class StringBuffer
	{
	public:
		/// Constructor
		StringBuffer();

		/// Destructor
		~StringBuffer();

		/// Setup the string buffer with size in bytes
		void Setup(SizeT size);

		/// Discard the string buffer
		void Discard();

		/// Return true if string buffer has been setup
		bool IsValid() const;

		/// Add a string to the end of the string buffer, return pointer to string
		const s8* AddString(const s8* str);

		/// DEBUG: return next string in string buffer
		const s8* NextString(const s8* prev);

		/// DEBUG: get number of allocated chunks
		SizeT GetNumChunks() const;

	private:
		/// Allocate a new chunk
		void AllocNewChunk();

		STL::Array<s8*> chunks;
		SizeT chunkSize;
		s8* curPointer;
	};

	__IME_INLINED bool StringBuffer::IsValid() const
	{
		return (0 != this->curPointer);
	}

	__IME_INLINED SizeT StringBuffer::GetNumChunks() const
	{
		return this->chunks.Size();
	}

} /// namespace STL

 #endif /// _STLStringBuffer_h_
