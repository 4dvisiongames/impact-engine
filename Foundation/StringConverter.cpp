/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// String converter
#include <Foundation/StringConverter.h>

/// Standard template library
namespace STL
{
	int StringConverter::UTF8ToWide( const Char8* src, Char16* dst, int dstMaxBytes )
	{
		__IME_ASSERT((0 != src) && (0 != dst) && (dstMaxBytes > 2) && ((dstMaxBytes & 1) == 0));
		int numConv = MultiByteToWideChar(CP_UTF8, 0, src, -1, (LPWSTR) dst, (dstMaxBytes / 2) - 1);
		if (numConv > 0)
		{
			dst[numConv] = 0;
			return numConv;
		}
		else
		{
			DWORD errCode = GetLastError();
			STL::String< Char8 > errMessage;
			switch (errCode) {
			case ERROR_INSUFFICIENT_BUFFER: errMessage = STL::String< Char8 >::VA( "ERROR_INSUFFICIENT_BUFFER dstMaxBytes: %d", dstMaxBytes ); break;
			case ERROR_INVALID_FLAGS: errMessage = "ERROR_INVALID_FLAGS"; break;
			case ERROR_INVALID_PARAMETER: errMessage = "ERROR_INVALID_PARAMETER: May occur if src and dst are the same pointer."; break;
			case ERROR_NO_UNICODE_TRANSLATION: errMessage = "ERROR_NO_UNICODE_TRANSLATION... should never happen."; break;
			default: errMessage = "Unknown Error";
			}
			Core::System::Error( STL::String< Char8 >::VA( "STLStringConverter::UTF8ToWide() failed to convert string '%s' to wide s8! Error '%s'", src, errMessage.AsCharPtr() ) );
			return 0;
		}
	}

	STL::String< Char8 > StringConverter::WideToUTF8( const Char16* src )
	{
		__IME_ASSERT( 0 != src );
		STL::String< Char8 > returnString;
		Char8 dstBuf[1024];
		int numBytesWritten = WideCharToMultiByte( CP_UTF8, 0, src, -1, dstBuf, sizeof( dstBuf ), 0, 0 );

		if ( numBytesWritten > 0 )
		{
			__IME_ASSERT( numBytesWritten < sizeof( dstBuf ) );
			returnString.Assign( dstBuf, numBytesWritten );
		}
		else
		{
			Core::System::Error( "STLStringConverter::WideToUTF8() Failed to convert string!" );
		}

		return returnString;
	};

	STL::String< Char16 > StringConverter::UTF8ToWide( const Char8* src )
	{
		__IME_ASSERT( 0 != src );
		STL::String< Char16 > returnString;
		Char16 dstBuf[1024];

		StringConverter::UTF8ToWide( src, dstBuf, sizeof( dstBuf ) );

		return returnString;
	};
};