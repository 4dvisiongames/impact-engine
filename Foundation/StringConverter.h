/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// String
#include <Foundation/STLString.h>
/// System
#include <Foundation/System.h>

/// Standard template library
namespace STL
{
	/// String converter for RFString
	class StringConverter
	{
	public:
		/// convert from UTF-8 encoded string object to wide string, return number of used bytes
		static int UTF8ToWide( const STL::String< Char8 >& src, Char16* dst, int dstMaxBytes );

		/// <summary>
		/// Converts ASCII string to Unicode 1-byte string.
		/// </summary>
		/// <param name="src">Pointer to the source ASCII string.</param>
		/// <param name="dst">Pointer to the destination preallocated 1-byte Unicode string.</param>
		/// <param name="dsmMaxBytes">Size of 
		/// <returns>Size of used bytes converted raw 1-byte Unicode string.</returns> 
		static int UTF8ToWide( const Char8* src, Char16* dst, int dstMaxBytes );
		
		/// <summary>
		/// Converts 1-byte Unicode string to ASCII.
		/// </summary>
		/// <param name="src">Pointer to the 1-byte Unicode string.</param>
		/// <returns>Prebuilt converted ASCII string object.</returns> 
		static STL::String< Char8 > WideToUTF8( const Char16* src );
		
		/// <summary>
		/// Converts ASCII string to Unicode 1-byte string.
		/// </summary>
		/// <param name="src">Pointer to the ASCII string.</param>
		/// <returns>Prebuilt converted 1-byte Unicode string object.</returns> 
		static STL::String< Char16 > UTF8ToWide( const Char8* src );
	};

	__IME_INLINED int StringConverter::UTF8ToWide( const STL::String< Char8 >& src, wchar_t* dst, int dstMaxBytes )
	{
		return UTF8ToWide( src.AsCharPtr(), dst, dstMaxBytes );
	}
}