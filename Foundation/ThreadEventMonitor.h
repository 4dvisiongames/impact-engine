/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Foundation__ThreadEventMonitor_h__
#define __Foundation__ThreadEventMonitor_h__

/// Interlocked
#include <Foundation/Interlocked.h>
/// Event
#include <Foundation/Event.h>

namespace Threading
{
	class EventMonitor
	{
	public:
		class Ticket
		{
			friend class EventMonitor;
			Threading::InterlockedPODType< size_t > myEpoch;
		};

	private:
		Ticket myTicket;
		Threading::InterlockedPODType< bool > myWaitStatus;
		bool isSpurious;
		Threading::Event myEvent;

	public:
		EventMonitor( void );

		//! Prepare waiting for incoming signal for thread blocking
		void PrepareWait( Ticket& ticket );
		void CommitWait( Ticket& ticket );
		void CancelWait( void );

		void Notify( void );
	};

	__IME_INLINED EventMonitor::EventMonitor( void ) : isSpurious( false )
	{
		myTicket.myEpoch.data = 0;
		myWaitStatus.data = false;
	}

	__IME_INLINED void EventMonitor::Notify( void )
	{
		Interlocked::FetchAdd< MemoryOrderEnum::Sequental >( this->myTicket.myEpoch.data, 1 );
		bool doSignal = Interlocked::FetchStore< MemoryOrderEnum::Sequental >( this->myWaitStatus.data, false );
		if( doSignal )
			this->myEvent.Signal();
	}

	__IME_INLINED void EventMonitor::PrepareWait( Ticket& ticket )
	{
		if( this->isSpurious )
		{
			this->isSpurious = false;
			this->myEvent.Wait();
		}

		this->myTicket = ticket;
		Interlocked::Store< MemoryOrderEnum::Sequental >( this->myWaitStatus, true );
	}

	__IME_INLINED void EventMonitor::CommitWait( Ticket& ticket )
	{
		size_t currentEpoch = Interlocked::Fetch< MemoryOrderEnum::Acquire >( this->myTicket.myEpoch.data );
		size_t ticketsEpoch = Interlocked::Fetch< MemoryOrderEnum::Acquire >( ticket.myEpoch.data );

		bool doIt = (currentEpoch == ticketsEpoch);
		if( doIt )
			this->myEvent.Wait();
		else
			this->CancelWait();
	}

	__IME_INLINED void EventMonitor::CancelWait( void )
	{
		this->isSpurious = !Interlocked::FetchStore< MemoryOrderEnum::Sequental >( this->myWaitStatus.data, false );
	}
}

#endif /// __Foundation__ThreadEventMonitor_h__