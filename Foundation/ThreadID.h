/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#if defined( _MSC_VER )
#pragma once
#endif

#ifndef _NanoThreadID_h_
#define _NanoThreadID_h_

/// Engine shared code
#include <Foundation/Shared.h>

namespace Threading
{
	/// <summary>
	/// The thread ID is a unique identifier for each thread. So <c>ThreadID</c>
	/// helps us to access thread ID of current thread.
	/// </summary>
	/// <remarks>
	/// This is just utility class, and we dont need to make Core::ReferenceCount
	/// as dependency, because reference of ThreadID object always in static memory,
	/// preallocated locally.
	/// </remarks>
	class ThreadID
	{
	private:
		u32 ulID; //!< Unique identificator for thread

	public:
		/// <summary cref="ThreadID::ThreadID">
		/// Constructor.
		/// </summary>
		ThreadID( void );

		/// <summary cref="ThreadID::ThreadID">
		/// Constructor with unique identificator parameter.
		/// </summary>
		/// <param name="id">Unique identificator.</param>
		ThreadID( u32 id );

		/// <summary cref="ThreadID::~ThreadID">
		/// Destructor.
		/// </summary>
		ThreadID( const ThreadID& rhs );

		/// <summary cref="ThreadID::operator=">
		/// Assignment operator.
		/// </summary>
		/// <param name="rhs">A reference to ThreadID.</param>
		ThreadID& operator = ( const ThreadID& rhs );

		/// <summary cref="ThreadID::operator u32">
		/// </summary>
		operator u32 ( void );

		/// <summary cref="ThreadID::operator==">
		/// Equality operator.
		/// </summary>
		/// <param name="rhs">A reference to ThreadID.</param>
		/// <returns>True if equals, otherwise false.</returns>
		bool operator == ( const ThreadID& rhs );

		/// <summary cref="ThreadID::operator!=">
		/// Unequality operator.
		/// </summary>
		/// <param name="rhs">A reference to ThreadID.</param>
		/// <returns>True if unequals, otherwise false.</returns>
		bool operator != ( const ThreadID& rhs );
		bool operator <= ( const ThreadID& rhs );
		bool operator >= ( const ThreadID& rhs );
		bool operator < ( const ThreadID& rhs );
		bool operator > ( const ThreadID& rhs );
	};

	__IME_INLINED ThreadID::ThreadID( void ) : ulID( 0 )
	{
	}

	__IME_INLINED ThreadID::ThreadID( u32 id ) : ulID( id )
	{
	}

	__IME_INLINED ThreadID::ThreadID( const ThreadID& rhs ) : ulID( rhs.ulID )
	{
	}

	__IME_INLINED ThreadID& ThreadID::operator = ( const ThreadID& rhs )
	{
		this->ulID = rhs.ulID;
		return *this;
	}

	__IME_INLINED ThreadID::operator u32( void )
	{
		return this->ulID;
	}

	__IME_INLINED bool ThreadID::operator == ( const ThreadID& rhs )
	{
		return (this->ulID == rhs.ulID);
	}

	__IME_INLINED bool ThreadID::operator != ( const ThreadID& rhs )
	{
		return (this->ulID != rhs.ulID);
	}

	__IME_INLINED bool ThreadID::operator <= ( const ThreadID& rhs )
	{
		return (this->ulID <= rhs.ulID);
	}

	__IME_INLINED bool ThreadID::operator >= ( const ThreadID& rhs )
	{
		return (this->ulID >= rhs.ulID);
	}

	__IME_INLINED bool ThreadID::operator < ( const ThreadID& rhs )
	{
		return (this->ulID < rhs.ulID);
	}

	__IME_INLINED bool ThreadID::operator > ( const ThreadID& rhs )
	{
		return (this->ulID > rhs.ulID);
	}
}

#endif /// _NanoThreadID_h_