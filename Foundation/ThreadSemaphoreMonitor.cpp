/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Foundation/ThreadSemaphoreMonitor.h>
#include <Foundation/ScopedLock.h>

namespace Threading
{
	void SemaphoreMonitor::Context::Initialize( void )
	{
		this->myReadyState = true;
	}

	SemaphoreMonitor::SemaphoreMonitor( void )
	{
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed, unsigned int, unsigned int >( this->myEpoch, 0 );
	}

	SemaphoreMonitor::~SemaphoreMonitor( void )
	{
		this->AbortAll();
		__IME_ASSERT_MSG( this->myWaitSet.IsEmpty(), "myWaitSet is not empty! Critical invariant!" );
	}

	void SemaphoreMonitor::PrepareWait( Context& ctx, uintptr_t ctxIdx )
	{
		if( !ctx.myReadyState ) {
			ctx.Initialize();
		} else if( ctx.mySpuriousState ) {
			ctx.mySpuriousState = false;
			ctx.mySemaphore.Wait();
		}

		ctx.myContext = ctxIdx;
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( ctx.myInWait, true );

		//! It's vital to lock mutex here, even myEpoch getting and setting is relaxed. It's still relaxed in 98% of
		//! all events, but 2% still happens in race conditions... Another trick known as memory barrier has been used,
		//! this reduced to 1%, but this is not good at all. 
		{
			Threading::ScopedLock< Threading::SpinWait > lock( this->myMutex );
			volatile unsigned int lastEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( ctx.myEpoch, lastEpoch );
			this->myWaitSet.Add( static_cast< CircularListWithSentinel::Node* >( &ctx ) );
		}
		__IME_MEMORY_READWRITE_BARRIER;
	}

	bool SemaphoreMonitor::CommitWait( Context& ctx ) 
	{
		unsigned int thisEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch );
		unsigned int contextEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( ctx.myEpoch );
		const bool localProcessState = contextEpoch == thisEpoch;
		// this check is just an optimization
		if( localProcessState ) {
			__IME_ASSERT_MSG( ctx.myReadyState,  "Use of CommitWait() without prior PrepareWait()" );
			ctx.mySemaphore.Wait();

#if defined( _DEBUG )
			bool queueAssert = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( ctx.myInWait );
			__IME_ASSERT_MSG( !queueAssert, "Still in the queue?" );
#endif
			if( ctx.myAbortState ) {
				/// TODO: just raise exception and exit from function
			}
		} else {
			this->CancelWait( ctx );
		}

		return localProcessState;
	}

	void SemaphoreMonitor::CancelWait( Context& ctx )
	{
		//! Spurious wakeup will be pumped in the following PrepareWait()
		ctx.mySpuriousState = true;

		//! Try to remove node from wait set
		bool threadInWaitSet = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( ctx.myInWait );
		if( threadInWaitSet ) {
			Threading::ScopedLock< Threading::SpinWait > lock( this->myMutex );
			if( threadInWaitSet ) {
				//! Successfully removed from waitset, so there will be no spurious wakeup
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( ctx.myInWait, false );
				ctx.mySpuriousState = false;
				this->myWaitSet.Remove( static_cast< CircularListWithSentinel::Node& >( ctx ) );
			}
		}
	}

	void SemaphoreMonitor::NotifyOneRelaxed( void )
	{
		if( this->myWaitSet.IsEmpty() ) {
			return;
		}

		CircularListWithSentinel::Node* node;
		const CircularListWithSentinel::Node* end = this->myWaitSet.End();

		{
			Threading::ScopedLock< Threading::SpinWait > lock( this->myMutex );
			volatile unsigned int lastEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch, lastEpoch + 1 );

			node = this->myWaitSet.Front();
			if( node != end ) {
				this->myWaitSet.Remove( *node );
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->ToThreadContext( node )->myInWait, false );
			}
		}

		if( node != end ) {
			this->ToThreadContext( node )->mySemaphore.Signal();
		}
	}

	void SemaphoreMonitor::NotifyAllRelaxed( void )
	{
		if( this->myWaitSet.IsEmpty() ) {
			return;
		}

		CircularListWithSentinel list;
		const CircularListWithSentinel::Node* end;

		{
			Threading::ScopedLock< Threading::SpinWait > lock( this->myMutex );
			volatile unsigned int lastEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch, lastEpoch + 1 );
			this->myWaitSet.MoveTo( list );
			end = list.End();

			for( CircularListWithSentinel::Node* node = list.Front(); node != end; node = node->next ) {
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->ToThreadContext( node )->myInWait, false );
			}
		}

		CircularListWithSentinel::Node* next;
		for( CircularListWithSentinel::Node* node = list.Front(); node != end; node = next ) {
			next = node->next;
			this->ToThreadContext( node )->mySemaphore.Signal();
		}

#if defined( DEBUG )
		list.Clear();
#endif
	}

	void SemaphoreMonitor::AbortAllRelaxed( void ) {
		if( this->myWaitSet.IsEmpty() ) {
			return;
		}

		CircularListWithSentinel list;
		const CircularListWithSentinel::Node* end;

		{
			Threading::ScopedLock< Threading::SpinWait > lock( this->myMutex );
			volatile unsigned int lastEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch, lastEpoch + 1 );
			this->myWaitSet.MoveTo( list );
			end = list.End();

			for( CircularListWithSentinel::Node* node = list.Front(); node != end; node = node->next ) {
				Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->ToThreadContext( node )->myInWait, false );
			}
		}

		CircularListWithSentinel::Node* next;
		for( CircularListWithSentinel::Node* node = list.Front(); node != end; node = next ) {
			next = node->next;
			this->ToThreadContext( node )->myAbortState = true;
			this->ToThreadContext( node )->mySemaphore.Signal();
		}

#if defined( DEBUG )
		list.Clear();
#endif
	}
}