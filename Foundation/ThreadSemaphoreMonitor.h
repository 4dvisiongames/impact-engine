/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/
#pragma once

#ifndef __Foundation__ThreadSemaphoreMonitor_h__
#define __Foundation__ThreadSemaphoreMonitor_h__

//! Spin wait
#include <Foundation/SpinWait.h>
//! Event
#include <Foundation/Event.h>
//! Interlocked
#include <Foundation/Interlocked.h>
//! Circular doubly-linked list with sentinel
#include <Foundation/SafeCircularSentinel.h>

namespace Threading
{
	//! User-Space semaphore implementation known as SemaphoreMonitor Object like in C# or Java
	class SemaphoreMonitor
	{
		__IME_DISABLE_ASSIGNMENT( SemaphoreMonitor );

	public:
		//! Pre-thread descriptor for concurrency monitor
		class Context : public CircularListWithSentinel::Node
		{
			__IME_DISABLE_ASSIGNMENT( Context );
			friend class SemaphoreMonitor;

			Threading::Event mySemaphore;
			volatile unsigned int myEpoch;
			volatile bool myInWait;
			bool mySpuriousState;
			bool myAbortState;
			bool myReadyState;
			uintptr_t myContext;

			//! The method for lazy initialization of the thread_context's semaphore.
			//! Inlining of the method is undesirable, due to extra instructions for
			//! exception support added at caller side.
			__IME_NO_INLINE void Initialize( void );

		public:
			Context( void );
			~Context( void );
		};

	private:
		SpinWait myMutex;
		CircularListWithSentinel myWaitSet;
		unsigned int myEpoch;

		//! Just converts circular node implementation to thread context list
		Context* ToThreadContext( CircularListWithSentinel::Node* node );

	public:
		SemaphoreMonitor( void );
		~SemaphoreMonitor( void );

		//! Prepare wait by inserting 'thr' into the wailt queue
		void PrepareWait( Context& ctx, uintptr_t ctxIdx = 0 );

		//! Commit wait if event count has not changed; otherwise, cancel wait.
		/** Returns true if committed, false if canceled. */
		bool CommitWait( Context& ctx );

		//! Cancel the wait. Removes the thread from the wait queue if not removed yet.
		void CancelWait( Context& ctx );

		//! Wait for a condition to be satisfied with waiting-on context
		template< typename Time, typename Ctx >
		void Wait( Time waitUntile, Ctx ctx );

		//! Notify one thread about the event
		void NotifyOne( void );

		//! Notify one thread about the event. Relaxed version.
		void NotifyOneRelaxed( void );

		//! Notify all waiting threads of the event
		void NotifyAll( void );

		//! Notify all waiting threads of the event. Relaxed version
		void NotifyAllRelaxed( void );

		//! Notify waiting threads of the event that satisfies the given predicate
		template< typename Predicate >
		void Notify( Predicate& predicate );

		//! Notify waiting threads of the event that satisfies the given predicate. Relaxed version
		template< typename Predicate >
		void NotifyRelaxed( Predicate& predicate );

		//! Abort any sleeping threads at the time of the call
		void AbortAll( void );

		//! Abort any sleeping threads at the time of the call. Relaxed version
		void AbortAllRelaxed( void );
	};

	__IME_INLINED SemaphoreMonitor::Context::Context( void ) 
		: mySpuriousState( false )
		, myAbortState( false )
		, myReadyState( false )
		, myContext( 0 ) 
	{
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->myEpoch, 0 );
		Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( this->myInWait, false );
	}

	__IME_INLINED SemaphoreMonitor::Context::~Context( void ) 
	{
		if( this->myReadyState ) 
		{
			if( this->mySpuriousState )
				this->mySemaphore.Wait();
		}
	}

	__IME_INLINED SemaphoreMonitor::Context* SemaphoreMonitor::ToThreadContext( CircularListWithSentinel::Node* node ) 
	{
		return static_cast< SemaphoreMonitor::Context* >( node );
	}

	template< typename Time, typename Ctx >
	__IME_INLINED void SemaphoreMonitor::Wait( Time waitUntil, Ctx ctx ) 
	{
		bool slept = false;
		Context localCtx;
		this->PrepareWait( localCtx, ctx() );

		while( !waitUntil() )
		{
			if( (slept = this->CommitWait()) != false ) 
			{
				if( waitUntile() ) 
					break;
			}

			slept = false;
			this->PrepareWait( localCtx, ctx() );
		}

		if( !slept ) 
			this->CancelWait( localCtx );
	}

	__IME_INLINED void SemaphoreMonitor::NotifyOne( void ) 
	{
		__IME_MEMORY_READWRITE_BARRIER;
		this->NotifyOneRelaxed();
	}

	__IME_INLINED void SemaphoreMonitor::NotifyAll( void ) 
	{
		__IME_MEMORY_READWRITE_BARRIER;
		this->NotifyAllRelaxed();
	}

	template< typename Predicate >
	__IME_INLINED void SemaphoreMonitor::NotifyRelaxed( Predicate& predicate ) 
	{
		if( this->myWaitSet.IsEmpty() )
			return;

		CircularListWithSentinel list;
		CircularListWithSentinel::Node* next;
		const CircularListWithSentinel::Node* end = this->myWaitSet.End();

		{
			Threading::ScopedLock<Threading::SpinWait> lock( this->myMutex );
			volatile unsigned int lastEpoch = Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch );
			Threading::Interlocked::Store< Threading::MemoryOrderEnum::Relaxed >( this->myEpoch, lastEpoch + 1 );

			for( CircularListWithSentinel::Node* node = this->myWaitSet.Last(); node != end; node = next ) 
			{
				next = node->prev;
				Context* ctx = this->ToThreadContext( node );
				if( predicate( ctx->myContext ) )
				{
					this->myWaitSet.Remove( *node );
					Threading::Interlocked::Store< Threading::MemoryOrderEnum::Release >( ctx->myInWait, false );
					list.Add( node );
				}
			}
		}

		end = list.End();
		for( CircularListWithSentinel::Node* node = list.Front(); node != end; node = next ) 
		{
			next = node->next;
			this->ToThreadContext( node )->mySemaphore.Signal();
		}

#if defined( DEBUG )
		list.Clear();
#endif
	}

	template< typename Predicate >
	__IME_INLINED void SemaphoreMonitor::Notify( Predicate& predicate ) 
	{
		__IME_MEMORY_READWRITE_BARRIER;
		this->NotifyRelaxed< Predicate >( predicate );
	}

	__IME_INLINED void SemaphoreMonitor::AbortAll( void ) 
	{
		__IME_MEMORY_READWRITE_BARRIER;
		this->AbortAllRelaxed();
	}
}

#endif /// __Foundation__ThreadSemaphoreMonitor_h__