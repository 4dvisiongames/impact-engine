/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

union Int32Float
{
	Int32Float(const float Value) : Float(Value) { }
	Int32Float(const s32 Value) : Int32(Value) { }

	float Float;
	s32 Int32;
};

union Uint32Float
{
	Uint32Float(const float Value) : Float(Value) { }
	Uint32Float(const u32 Value) : Uint32(Value) { }

	float Float;
	u32 Uint32;
};

union Packed2x32to64Bit
{
public:
	Packed2x32to64Bit( int64_t val ) : m_64bit( val ) {
	}

	Packed2x32to64Bit( int32_t upper, uint32_t lower ) {
		m_32bits.upper = upper;
		m_32bits.lower = lower;
	}

	int64_t m_64bit;
	struct
	{
		int32_t upper;
		uint32_t lower;
	} m_32bits;
};