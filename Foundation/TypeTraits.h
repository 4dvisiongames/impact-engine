/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

namespace Traits
{
	namespace Internal
	{
		//! Custom aligned 16-byte object
		__IME_ALIGNED_MSVC( __IME_DEFAULT_VEC_ALIGNMENT )
		struct TypeWithAlignmentCustom16
		{
			u32 member [16 / sizeof( u32 )];
		} __IME_ALIGNED_GCC( __IME_DEFAULT_VEC_ALIGNMENT );

		//! Custom aligned 32-byte object
		__IME_ALIGNED_MSVC( 32 )
		struct TypeWithAlignmentCustom32
		{
			u32 member [32 / sizeof( u32 )];
		} __IME_ALIGNED_GCC( 32 );

		//! Custom aligned 64-byte object
		__IME_ALIGNED_MSVC( 64 )
		struct TypeWithAlignmentCustom64
		{
			u32 member [64 / sizeof( u32 )];
		} __IME_ALIGNED_GCC( 64 );
	}

	namespace Workarounds
	{
		//! Work around for bug in GNU 3.2 and MSVC compilers.
		/** Bug is that compiler sometimes returns 0 for __alignof(T) when T has not yet been instantiated.
			The work-around forces instantiation by forcing computation of sizeof(T) before __alignof(T). */
		template< size_t size, typename T >
		struct AlignmentBug { static const size_t alignment = __IME_ALIGN_OF( T ); };
	}

	/// Utility helper structure to ease overload resolution
	template< int TVal > struct IntToType { enum { value = TVal }; };

	//! Abstractive type with alignment definition
	template< size_t size > struct TypeWithAlignment;

	//! Platform-dependent enumeration
	template< size_t size > struct TypeEnumerationTrait;
	template<> struct TypeEnumerationTrait < 4 > { typedef u32 ValueType; };
	template<> struct TypeEnumerationTrait < 8 > { typedef u64 ValueType; };
	struct TypeEnumeration { typedef TypeEnumerationTrait< sizeof( size_t ) >::ValueType ValueType; };

	//! Size of pointer in current compilator and OS
	struct SizeOfPtr { enum { value = sizeof( uintptr_t ) }; };

	//! Implementation of alignment definitions
	template<> struct TypeWithAlignment < 1 > { s8 member; };
	template<> struct TypeWithAlignment < 2 > { s16 member; };
	template<> struct TypeWithAlignment < 4 > { s32 member; };
	template<> struct TypeWithAlignment < 8 > { s64 member; };
	template<> struct TypeWithAlignment < 16 > { Internal::TypeWithAlignmentCustom16 member; };
	template<> struct TypeWithAlignment < 32 > { Internal::TypeWithAlignmentCustom32 member; };
	template<> struct TypeWithAlignment < 64 > { Internal::TypeWithAlignmentCustom64 member; };


	/// Hacks to support various features

	/// CtorNotInitialized exists so that we can create a constructor that allocates but doesn't 
	/// initialize and also doesn't collide with any other constructor declaration.
	struct CtorNotInitialized { /* Empty */ };

	/// CtorHasVA so that we can create a constructor that accepts printf-style  
	/// arguments but also doesn't collide with any other constructor declaration.
	struct CtorHasVA { /* Empty */ };

	/// Intrusive pointer to instrusive list converter from smart pointer
	template< typename T >
	struct SmartPtrIntrusiveAdapter
	{
		typedef typename T::ValueType ValueType;

		static ValueType* getPointerFrom( T& rhs ) {
			return rhs.Get();
		}

		static void setPointerTo( ValueType* Output, T* rhs ) {
			*rhs = Output;
		}

		static void appendedTo( ValueType* p ) {
			typedef typename T::PolicyType PolicyType;
			typedef typename PolicyType::ThisOp OpType;
			p->AddRef< OpType::value >();
		}

		static void removedFrom( ValueType* p ) {
			typedef typename T::PolicyType PolicyType;
			typedef typename PolicyType::ThisOp OpType;
			p->Release< OpType::value >();
		}
	};

	template< typename T >
	struct DefaultIntrusiveAdapter
	{
		typedef T ValueType;

		static ValueType* getPointerFrom( T& rhs ) {
			return &rhs;
		}

		static void setPointerTo( ValueType* Output, T*& rhs ) {
			rhs = Output;
		}

		static void appendedTo( ValueType* p ) { }
		static void removedFrom( ValueType* p ) { }
	};


	template< typename T, typename Node >
	struct SmartPtrIntrusiveIteratorRefAdapter
	{
		typedef T Reference;
		typedef Node* NodePointer;

		static Reference Item( NodePointer pointer ) {
			return T( static_cast<typename T::Pointer>(pointer) );
		}
	};

	template< typename T, typename Node >
	struct DefaultIntrusiveIteratorRefAdapter
	{
		typedef T& Reference;
		typedef Node* NodePointer;

		static Reference Item( NodePointer pointer ) {
			return *static_cast<T*>(pointer);
		}
	};

	/// This is the base class for various type traits, as defined by the proposed
	/// C++ standard. This is essentially a utility base class for defining properties
	/// as both class constants (value) and as types (type).
	template< typename Type, Type v >
	struct IntegralConstant
	{
		typedef Type ValueType;
		typedef IntegralConstant< Type, v > ThisType;
		static const ValueType value = v;
	};

	typedef IntegralConstant< bool, true > TrueType;
	typedef IntegralConstant< bool, false > FalseType;


	/// Forward declaration for condition selector
	template< bool condition, typename ConditionIsTrueType, typename ConditionIsFalseType > struct TypeSelect;

	template< class ConditionIsTrueType, class ConditionIsFalseType >
	struct TypeSelect < false, ConditionIsTrueType, ConditionIsFalseType >
	{
		typedef ConditionIsFalseType ValueType;
	};

	template< class ConditionIsTrueType, class ConditionIsFalseType >
	struct TypeSelect < true, ConditionIsTrueType, ConditionIsFalseType >
	{
		typedef ConditionIsTrueType ValueType;
	};

	template<bool> struct Prediction : public FalseType { };
	template<> struct Prediction<true> : public TrueType{ };

	template< typename T > struct IsEmpty
	{
	private:
		template<typename> struct __first { };
		template<typename T > struct __second : public T { };

	public:
		enum { value = sizeof( __first ) == sizeof( __second ); };
	};

	template< typename Base, typename Derived >
	struct AutoConvertOf
	{
		operator Base*() const;
		operator Derived*();
	};

	template< typename Base, typename Derived >
	struct IsBaseOf
	{
		typedef char( &yes ) [1];
		typedef char( &no ) [2];

		template< typename Type >
		static yes Check( Derived*, Type );
		static no Check( Base*, int32_t );

		static const bool value = sizeof( Check( AutoConvertOf<Base, Derived>(), int32_t() ) ) == sizeof( yes );
	};

	/// Default definition of <c>IsVoidType</c>
	template< typename T > struct IsVoidType : public FalseType { };
	template<> struct IsVoidType< void > : public TrueType{ };
	template<> struct IsVoidType< void const > : public TrueType{ };
	template<> struct IsVoidType< void volatile > : public TrueType{ };
	template<> struct IsVoidType< void const volatile > : public TrueType{ };

	/// Default definition of <c>IsIntegralType</c>
	template< typename T > struct IsIntegralType : public FalseType { };

	template<> struct IsIntegralType< uint8_t > : public TrueType{ };
	template<> struct IsIntegralType< uint8_t const > : public TrueType{ };
	template<> struct IsIntegralType< uint8_t volatile > : public TrueType{ };
	template<> struct IsIntegralType< uint8_t const volatile > : public TrueType{ };

	template<> struct IsIntegralType< uint16_t > : public TrueType{ };
	template<> struct IsIntegralType< uint16_t const > : public TrueType{ };
	template<> struct IsIntegralType< uint16_t volatile > : public TrueType{ };
	template<> struct IsIntegralType< uint16_t const volatile > : public TrueType{ };

	template<> struct IsIntegralType< uint32_t > : public TrueType{ };
	template<> struct IsIntegralType< uint32_t const > : public TrueType{ };
	template<> struct IsIntegralType< uint32_t volatile > : public TrueType{ };
	template<> struct IsIntegralType< uint32_t const volatile > : public TrueType{ };

	template<> struct IsIntegralType< uint64_t > : public TrueType{ };
	template<> struct IsIntegralType< uint64_t const > : public TrueType{ };
	template<> struct IsIntegralType< uint64_t volatile > : public TrueType{ };
	template<> struct IsIntegralType< uint64_t const volatile > : public TrueType{ };

	template<> struct IsIntegralType< int8_t > : public TrueType{ };
	template<> struct IsIntegralType< int8_t const > : public TrueType{ };
	template<> struct IsIntegralType< int8_t volatile > : public TrueType{ };
	template<> struct IsIntegralType< int8_t const volatile > : public TrueType{ };

	template<> struct IsIntegralType< int16_t > : public TrueType{ };
	template<> struct IsIntegralType< int16_t const > : public TrueType{ };
	template<> struct IsIntegralType< int16_t volatile > : public TrueType{ };
	template<> struct IsIntegralType< int16_t const volatile > : public TrueType{ };

	template<> struct IsIntegralType< int32_t > : public TrueType{ };
	template<> struct IsIntegralType< int32_t const > : public TrueType{ };
	template<> struct IsIntegralType< int32_t volatile > : public TrueType{ };
	template<> struct IsIntegralType< int32_t const volatile > : public TrueType{ };

	template<> struct IsIntegralType< int64_t > : public TrueType{ };
	template<> struct IsIntegralType< int64_t const > : public TrueType{ };
	template<> struct IsIntegralType< int64_t volatile > : public TrueType{ };
	template<> struct IsIntegralType< int64_t const volatile > : public TrueType{ };

	template<> struct IsIntegralType< bool > : public TrueType{ };
	template<> struct IsIntegralType< bool const > : public TrueType{ };
	template<> struct IsIntegralType< bool volatile > : public TrueType{ };
	template<> struct IsIntegralType< bool const volatile > : public TrueType{ };

	template<> struct IsIntegralType< Char8 > : public TrueType{ };
	template<> struct IsIntegralType< Char8 const > : public TrueType{ };
	template<> struct IsIntegralType< Char8 volatile > : public TrueType{ };
	template<> struct IsIntegralType< Char8 const volatile > : public TrueType{ };

	template<> struct IsIntegralType< Char16 > : public TrueType{ };
	template<> struct IsIntegralType< Char16 const > : public TrueType{ };
	template<> struct IsIntegralType< Char16 volatile > : public TrueType{ };
	template<> struct IsIntegralType< Char16 const volatile > : public TrueType{ };


	/// Default definition of floating point type trait
	template< typename T > struct IsFloatingPointType : public FalseType { };
	template<> struct IsFloatingPointType< float > : public TrueType{ };
	template<> struct IsFloatingPointType< float const > : public TrueType{ };
	template<> struct IsFloatingPointType< float volatile > : public TrueType{ };
	template<> struct IsFloatingPointType< float const volatile > : public TrueType{ };

	template<> struct IsFloatingPointType< double > : public TrueType{ };
	template<> struct IsFloatingPointType< double const > : public TrueType{ };
	template<> struct IsFloatingPointType< double volatile > : public TrueType{ };
	template<> struct IsFloatingPointType< double const volatile > : public TrueType{ };

	template<> struct IsFloatingPointType< long double > : public TrueType{ };
	template<> struct IsFloatingPointType< long double const > : public TrueType{ };
	template<> struct IsFloatingPointType< long double volatile > : public TrueType{ };
	template<> struct IsFloatingPointType< long double const volatile > : public TrueType{ };

	template< typename T > struct IsArithmeticType : public FalseType { };
	template< typename T > struct IsSmartPointer : public FalseType { };
	template< typename T > struct IsSmartPointer< T* > : public FalseType{ };
	template< typename T > struct IsSmartPointer< T* const > : public FalseType{ };
	template< typename T > struct IsSmartPointer< T* volatile > : public FalseType{ };
	template< typename T > struct IsSmartPointer< T* const volatile > : public FalseType{ };

	template< typename T > struct IsIntrusiveContainer : public FalseType { };

	template< typename Type > struct RemoveReference { typedef Type ValueType; };
	template< typename Type > struct RemoveReference < Type& > { typedef Type ValueType; };
	template< typename Type > struct RemoveReference < Type&& > { typedef Type ValueType; };

	template< typename Type > struct RemoveConst { typedef Type ValueType; };
	template< typename Type > struct RemoveConst < const Type > { typedef Type ValueType; };
	template< typename Type > struct RemoveConst < const Type[] > { typedef Type ValueType[]; };
	template< typename Type, SizeT Len > struct RemoveConst < const Type [Len] > { typedef Type ValueType [Len]; };

	template< typename Type > struct RemoveVolatile { typedef Type ValueType; };
	template< typename Type > struct RemoveVolatile < volatile Type > { typedef Type ValueType; };
	template< typename Type > struct RemoveVolatile < volatile Type[] > { typedef Type ValueType[]; };
	template< typename Type, SizeT Len > struct RemoveVolatile < volatile Type [Len] > { typedef Type ValueType [Len]; };

	template< typename Type > struct RemoveConstVolatile { typedef typename RemoveConst< typename RemoveVolatile< Type >::ValueType >::ValueType ValueType; };

	template< typename Type > struct RemoveArrayExtent { typedef Type ValueType; };
	template< typename Type > struct RemoveArrayExtent < Type[] > { typedef Type ValueType; };
	template< typename Type, SizeT Len > struct RemoveArrayExtent < Type [Len] > { typedef Type ValueType; };

	template< typename Type > struct IsLValueReference : public FalseType { };
	template< typename Type > struct IsLValueReference<Type&> : public TrueType{ };

	template< typename Type > struct IsRValueReference : public FalseType { };
	template< typename Type > struct IsRValueReference< Type&& > : public TrueType{ };

	template< typename Type > struct IsReference : public Prediction < IsLValueReference<Type>::value || IsRValueReference<Type>::value > { };

	template< typename Type > struct IsArrayType : public FalseType { };
	template< typename Type > struct IsArrayType< Type[] > : public TrueType{ };
	template< typename Type, SizeT Len > struct IsArrayType< Type [Len] > : public TrueType{ };

	template< typename Type > struct IsFunctionPointer : public FalseType { };
	template< typename Type > struct IsMethodPointer : public FalseType { };

	template< typename Return, typename... Args >
	struct IsFunctionPointer< Return( __cdecl* )(Args...) > : public TrueType{ };

	template< typename Return, typename Class, typename... Args >
	struct IsMethodPointer< Return( __cdecl Class::* )(Args...) const > : public TrueType{ };

	template< typename Return, typename Class, typename... Args >
	struct IsMethodPointer< Return( __cdecl Class::* )(Args...) volatile > : public TrueType{ };

	template< typename Return, typename Class, typename... Args >
	struct IsMethodPointer< Return( __cdecl Class::* )(Args...) const volatile > : public TrueType{ };

	template< typename Type > struct AddPointer { typedef typename RemoveReference<Type>::type* ValueType; };

	template< typename Type >
	class Decay
	{
		typedef typename RemoveReference< Type >::type DecayType;

		typedef typename TypeSelect < IsArrayType< DecayType >::value,
			typename RemoveArrayExtent< DecayType >::ValueType*,
			typename TypeSelect < IsFunctionPointer< DecayType >::value,
			typename AddPointer< DecayType >::ValueType,
			typename RemoveConstVolatile< DecayType >::ValueType > ::ValueType > ::ValueType ValueType;
	};

	/// Helper structure to convert unsigned types for templates into signed when using
	/// decrement
	template< typename T >
	struct SignedUnsignedInversion
	{
		typedef T Type;
	};

	template<>
	struct SignedUnsignedInversion < u8 >
	{
		typedef s8 Type;
	};

	template<>
	struct SignedUnsignedInversion < u16 >
	{
		typedef s16 Type;
	};

	template<>
	struct SignedUnsignedInversion < u32 >
	{
		typedef s32 Type;
	};

	template<>
	struct SignedUnsignedInversion < u64 >
	{
		typedef s64 Type;
	};

	template<>
	struct SignedUnsignedInversion < unsigned long >
	{
		typedef long Type;
	};

	/// number | twos complement     | ones complement     | sign/magnitude
	/// == == =|= == == == == == == == == == == == == == ==|= == == == == == == =
	///	-1     | 1111 1111 1111 1111 | 1111 1111 1111 1110 | 1000 0000 0000 0001
	template< typename Type >
	struct IsSignedUnsignedComplementSystem
	{
		typedef Type UnsignedType;
		typedef typename SignedUnsignedInversion<Type>::Type SignedType;

		static const bool Value = (static_cast<SignedType>(~(UnsignedType())) == static_cast<SignedType>(-1));
	};
}