/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Smart pointer
#include <Foundation/SmartPtr.h>

/// <summary>
/// <c>IntrusiveWeakPtr</c> is a smart pointer which does not change the reference count of the
/// target object. Use this to prevent cyclic dependencies between objects.
/// </summary>
template< typename Type, typename Policy = SmartPointerPolicy< Type, SmartPointerRelaxedOp > >
class IntrusiveWeakPtr
{
	friend class IntrusivePtr< Type, Policy >;

public:
	typedef IntrusiveWeakPtr< Type, Policy > ThisType;
	typedef IntrusivePtr< Type, Policy > ThisSmartPtr;
	typedef Type ValueType;
	typedef Type& Reference;
	typedef const Type& ConstReference;
	typedef Type* Pointer;
	typedef const Type* ConstPointer;
	typedef Policy PolicyType;

	/// Default constructor
	IntrusiveWeakPtr( void );
	/// Constructor with C++ pointer.
	IntrusiveWeakPtr( Pointer ptr );
	/// Constructor with weak pointer
	IntrusiveWeakPtr( const ThisType& ptr );
	/// Constructor with smart pointer.
	IntrusiveWeakPtr( const ThisSmartPtr& ptr );

	/// Constructor in case of other types and policies used with this pointer
	template< typename OtherType, typename OtherPolicy >
	IntrusiveWeakPtr( const IntrusiveWeakPtr< OtherType, OtherPolicy >& rhs );

	/// Constructor in case of other smart pointer types and policies used with this pointer
	template< typename OtherType, typename OtherPolicy >
	IntrusiveWeakPtr( const IntrusivePtr< OtherType, OtherPolicy >& rhs );

	/// Default destructor
	~IntrusiveWeakPtr( void );
	/// Assignment operator
	ThisType& operator = ( const ThisSmartPtr& rhs );
	/// Assignment operator
	ThisType& operator = ( const ThisType& rhs );
	/// Assignment operator
	ThisType& operator = ( Pointer ptr );
	/// Safe access operator
	Pointer operator -> ( void ) const;
	/// Safe dereference operator
	Reference operator* ( void ) const;
	/// Safe pointer cast operator
	operator Pointer ( void ) const;
	/// Check if pointer is valid
	bool IsValid( void ) const;
	/// Get pointer directly. Safe version(asserts in debug mode on null-pointer access)
	Pointer Get( void ) const;
	/// Get pointer directly. Unsafe version(doesn't assert on null-pointer access)
	Pointer GetUnsafe( void ) const;

private:
	Pointer myPointer; //!< Pointer to the stored object
};

namespace Traits
{
	template< typename U, typename V > struct IsSmartPointer< IntrusiveWeakPtr< U, V > > : public Traits::TrueType{};
}

template< typename Type, typename Policy >
__IME_INLINED IntrusiveWeakPtr< Type, Policy >::IntrusiveWeakPtr( void ) : myPointer( nullptr )
{
	/// Empty
}

template< typename Type, typename Policy > 
__IME_INLINED IntrusiveWeakPtr< Type, Policy >::IntrusiveWeakPtr( Pointer ptr ) : myPointer( nullptr )
{
	Pointer dst = PolicyType::AddRef( ptr );
	PolicyType::SetPointer( dst, this->myPointer );
}

template< typename Type, typename Policy > 
__IME_INLINED IntrusiveWeakPtr< Type, Policy >::IntrusiveWeakPtr( const ThisSmartPtr& rhs ) : myPointer( nullptr )
{
	Pointer src = const_cast< typename ThisSmartPtr::Pointer >( rhs.GetUnsafe() );
	src = PolicyType::AddRef( src );
	PolicyType::SetPointer( src, this->myPointer );
}

template< typename Type, typename Policy > 
__IME_INLINED IntrusiveWeakPtr< Type, Policy >::IntrusiveWeakPtr( const ThisType& rhs ) : myPointer( nullptr )
{
	Pointer src = PolicyType::Access( rhs.myPointer );
	PolicyType::SetPointer( src, this->myPointer );
}

template< typename Type, typename Policy >
template< typename OtherType, typename OtherPolicy >
__IME_INLINED IntrusiveWeakPtr< Type, Policy >::IntrusiveWeakPtr( const IntrusiveWeakPtr< OtherType, OtherPolicy >& rhs ) 
{
	typedef IntrusiveWeakPtr< OtherType, OtherPolicy > OtherWeakPtr;
	Pointer src = OtherWeakPtr::PolicyType::Access( rhs.myPointer );
	PolicyType::SetPointer( src, this->myPointer );
}

template< typename Type, typename Policy >
template< typename OtherType, typename OtherPolicy >
__IME_INLINED IntrusiveWeakPtr< Type, Policy >::IntrusiveWeakPtr( const IntrusivePtr< OtherType, OtherPolicy >& rhs )
{
	typedef IntrusivePtr< OtherType, OtherPolicy > OtherPtr;
	Pointer src = const_cast< typename OtherPtr::Pointer >(rhs.GetUnsafe());
	src = PolicyType::AddRef( src );
	PolicyType::SetPointer( src, this->myPointer );
}

template< typename Type, typename Policy >
__IME_INLINED IntrusiveWeakPtr< Type, Policy >::~IntrusiveWeakPtr( void )
{
	Pointer src = PolicyType::Access( this->myPointer );
	// Codepoet: we don't need to assign anything, as function asks, because we destruct this object
	PolicyType::Release( src );
}

template< typename Type, typename Policy > 
__IME_INLINED typename IntrusiveWeakPtr< Type, Policy >::ThisType& IntrusiveWeakPtr< Type, Policy >::operator = ( const ThisSmartPtr& rhs )
{
	Pointer src = const_cast< typename ThisSmartPtr::Pointer >(rhs.GetUnsafe());
	Pointer dst = PolicyType::Access( this->myPointer );
	dst = PolicyType::Assign( src, dst );
	PolicyType::SetPointer( dst, this->myPointer );
	return *this;
}

template< typename Type, typename Policy >
__IME_INLINED typename IntrusiveWeakPtr< Type, Policy >::ThisType& IntrusiveWeakPtr< Type, Policy >::operator = ( const ThisType& rhs )
{
	/// Assign weak pointers
	Pointer src = PolicyType::Access( rhs.myPointer );
	PolicyType::SetPointer( src, this->myPointer );
	return *this;
}

template< typename Type, typename Policy >
__IME_INLINED typename IntrusiveWeakPtr< Type, Policy >::ThisType& IntrusiveWeakPtr< Type, Policy >::operator = ( Pointer ptr )
{
	/// Release pointer reference counter
	Pointer src = PolicyType::Access( ptr );
	Pointer dst = PolicyType::Access( this->myPointer );
	this->myPointer = PolicyType::Assign( src, dst );
	return *this;
}

template< typename Type, typename Policy > 
__IME_INLINED typename IntrusiveWeakPtr< Type, Policy >::Pointer IntrusiveWeakPtr< Type, Policy >::operator -> ( void ) const
{
	return PolicyType::Access( this->myPointer );
}

template< typename Type, typename Policy > 
__IME_INLINED typename IntrusiveWeakPtr< Type, Policy >::Reference IntrusiveWeakPtr< Type, Policy >::operator * ( void ) const
{
	Pointer src = PolicyType::Access( this->myPointer );
	return PolicyType::Deference( src );
}

template< typename Type, typename Policy > 
__IME_INLINED IntrusiveWeakPtr< Type, Policy >::operator Pointer ( void ) const
{
	Pointer src = PolicyType::Access( this->myPointer );
	__IME_ASSERT( nullptr != src );
	return src;
}

template< typename Type, typename Policy > 
__IME_INLINED bool IntrusiveWeakPtr< Type, Policy >::IsValid( void ) const
{
	Pointer src = PolicyType::Access( this->myPointer );
	bool result = ( nullptr != src );
	return result;
}

template< typename Type, typename Policy > 
__IME_INLINED typename IntrusiveWeakPtr< Type, Policy >::Pointer IntrusiveWeakPtr< Type, Policy >::Get( void ) const
{
	Pointer src = PolicyType::Access( this->myPointer );
	__IME_ASSERT( nullptr != src );
	return src;
}

template< typename Type, typename Policy >
__IME_INLINED typename IntrusiveWeakPtr< Type, Policy >::Pointer IntrusiveWeakPtr< Type, Policy >::GetUnsafe( void ) const
{
	return PolicyType::Access( this->myPointer );
}