/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Foundation__Win32__Win32CPUCaching_h__
#define __Foundation__Win32__Win32CPUCaching_h__

/* constants for use with _mm_prefetch */
#ifndef _MM_HINT_T0
#define _MM_HINT_T0     1
#endif /// _MM_HINT_T0

#ifndef _MM_HINT_T1
#define _MM_HINT_T1     2
#endif /// _MM_HINT_T1

#ifndef _MM_HINT_T2
#define _MM_HINT_T2     3
#endif /// _MM_HINT_T2

/// Early definition
extern void _mm_prefetch(char const*_A, int _Sel);

namespace Kernel
{
	struct CPUCachingLevelEnum
	{
		enum List
		{
			L1 = _MM_HINT_T0,
			L2 = _MM_HINT_T1,
			L3 = _MM_HINT_T2
		};
	};
	typedef CPUCachingLevelEnum::List CPUCachingLevel;

	template< CPUCachingLevel Level, typename T >
	void CPUPrefetch( _In_ T* ptr )
	{
		_mm_prefetch( reinterpret_cast< const char* >( ptr ), Level );
	};
}

#endif /// __Foundation__Win32__Win32CPUCaching_h__