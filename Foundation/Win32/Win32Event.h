/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef _Win32Event_h_
#define _Win32Event_h_

/// Engine shared code
#include <Foundation/Shared.h>

/// Win32 threading namespace
namespace Threading
{
	/// <summary>
	/// <c>Event</c> is a Win32 platform Event definition.
	/// </summary>
	class Event
	{
	public:
		/// <summary cref="Event::Event">
		/// Constructor.
		/// </summary>
		/// <param name="manualReset">Manual event reset. Always false.</param>
		Event( bool manualReset = false );

		/// <summary cref="Event::~Event">
		/// Destructor.
		/// </summary>
		~Event( void );

		/// <summary cref="Event::Singnal">
		/// Signal the event.
		/// </summary>
		void Signal();

		/// <summary cref="Event::Reset">
		/// Reset the event (only if manual reset).
		/// </summary>
		void Reset( void );

		/// <summary cref="Event::Wait">
		/// Wait for the event to become signalled.
		/// </summary>
		void Wait( void ) const;

		/// <summary cref="Event::WaitTimeout">
		/// Waits for the event to become signaled with a specified timeout
		/// in milli seconds.
		/// </summary>
		/// <param name="ms">Time to wait.</param>
		/// <returns>
		/// If the method times out it will return false,
		/// if the event becomes signalled within the timeout it will return 
		/// true.
		/// </returns>
		bool WaitTimeout( int ms ) const;

		/// <summary cref="Event::Peek">
		/// This checks if the event is signalled and returnes immediately.
		/// </summary>
		/// <returns>True if peeked, false otherwise.</returns>
		bool Peek( void ) const;

	private:
		HANDLE hEvent; //!< Event
	};

	__IME_INLINED Event::Event( bool manualReset )
	{
		this->hEvent = CreateEvent( NULL, manualReset, FALSE, NULL );
		__IME_ASSERT( this->hEvent != NULL );
	}

	__IME_INLINED Event::~Event()
	{
		CloseHandle(this->hEvent);
		this->hEvent = 0;
	}

	__IME_INLINED void Event::Signal()
	{
		SetEvent(this->hEvent);
	}

	__IME_INLINED void Event::Reset()
	{
		ResetEvent(this->hEvent);
	}

	__IME_INLINED void Event::Wait() const
	{
		WaitForSingleObject( this->hEvent, INFINITE );
	}

	__IME_INLINED bool Event::WaitTimeout( int timeoutInMilliSec ) const
	{
		DWORD res = WaitForSingleObject( this->hEvent, timeoutInMilliSec );
		return (WAIT_TIMEOUT == res) ? false : true;
	}

	__IME_INLINED bool Event::Peek() const
	{
		DWORD res = WaitForSingleObject( this->hEvent, NULL );
		return (WAIT_TIMEOUT == res) ? false : true;
	}
}

#endif /// _Win32Event_h_