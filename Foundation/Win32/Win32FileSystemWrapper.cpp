/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Win32 FileSystem OS wrapper
#include <Foundation/Win32/Win32FileSystemWrapper.h>
/// System functions
#include <Foundation/System.h>
/// String converter
#include <Foundation/StringConverter.h>

/// Shell object
#include <shlobj.h>
/// Shell Windows API
#include <Shlwapi.h>

/// Win32 FileSystem namespace
namespace FileSystem
{
	OSWrapper::Handle OSWrapper::OpenFile( const FileSystem::Path& path, 
										   FileSystem::AccessMode accessMode, 
										   FileSystem::AccessPattern accessPattern, 
										   DWORD flagsAndAttributes )
	{
		DWORD access = 0;
		DWORD disposition = 0;
		DWORD shareMode = 0;
		switch (accessMode) {
		case FileSystem::AccessModeEnum::ReadOnly:
				access = GENERIC_READ;            
				disposition = OPEN_EXISTING;
				shareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
				break;

		case FileSystem::AccessModeEnum::WriteOnly:
				access = GENERIC_WRITE;
				disposition = CREATE_ALWAYS;
				shareMode = FILE_SHARE_READ;
				break;

		case FileSystem::AccessModeEnum::ReadWrite:
		case FileSystem::AccessModeEnum::Append:
				access = GENERIC_READ | GENERIC_WRITE;
				disposition = OPEN_ALWAYS;
				shareMode = FILE_SHARE_READ | FILE_SHARE_WRITE;
				break;
		}

		switch (accessPattern) {
		case FileSystem::AccessPatternEnum::Random:
				flagsAndAttributes |= FILE_FLAG_RANDOM_ACCESS;
				break;

		case FileSystem::AccessPatternEnum::Sequential:
				flagsAndAttributes |= FILE_FLAG_SEQUENTIAL_SCAN;
				break;
		}

		/// Open/create the file
#if defined( __IME_PLATFORM_XBOX_ONE )
		Handle handle = CreateFile( path.AsCharPtr(),		 // lpFileName
									access,                  // dwDesiredAccess
									shareMode,               // dwShareMode
									0,                       // lpSecurityAttributes
									disposition,             // dwCreationDisposition,
									flagsAndAttributes,      // dwFlagsAndAttributes
									NULL );                  // hTemplateFile
#else
		Handle handle = CreateFileW(path.AsCharPtr(),		 // lpFileName
									access,                  // dwDesiredAccess
									shareMode,               // dwShareMode
									0,                       // lpSecurityAttributes
									disposition,             // dwCreationDisposition,
									flagsAndAttributes,      // dwFlagsAndAttributes
									NULL);                   // hTemplateFile
#endif

		// TODO: remember what is this and why did I did this?
		DWORD lastError = ::GetLastError();
		UNREFERENCED_PARAMETER( lastError );

		/// If we successfully created file session
		if (handle != INVALID_HANDLE_VALUE) {
			// in append mode, we need to seek to the end of the file
			if ( FileSystem::AccessModeEnum::Append == accessMode ) {
				/// Setup pointer inside file of data offset to the end
				SetFilePointer( handle, 0, NULL, FILE_END );
			}
			/// Get created handle
			return handle;
		} else {
			/// We made error somewhere, so return nullfied pointer
			return NULL;
		}
	}

	void OSWrapper::CloseFile( Handle handle )
	{
		__IME_ASSERT( 0 != handle );
		CloseHandle( handle );
	}

	void OSWrapper::Write( Handle handle, const void* buf, FileSystem::Size numBytes )
	{
		/// Assertions
		__IME_ASSERT( NULL != handle );
		__IME_ASSERT( buf != NULL );
		__IME_ASSERT( numBytes > 0 );

#if defined(__IME_PLATFORM_WIN64)
		DWORD bytesToWrite;
		Packed2x32to64Bit conv( numBytes );
		bytesToWrite = conv.m_32bits.upper;
#else
		DWORD bytesToWrite = numBytes;
#endif

		DWORD bytesWritten;
		/// Write all data to the file
		BOOL result = WriteFile( handle, buf, bytesToWrite, &bytesWritten, NULL );
		if ( (NULL == result) || ((DWORD)numBytes != bytesWritten) ) {
			Core::System::Error("OSWrapper: WriteFile() failed!");
		}
	}

	FileSystem::Size OSWrapper::Read( Handle handle, void* buf, FileSystem::Size numBytes )
	{
		__IME_ASSERT( NULL != handle );
		__IME_ASSERT( buf != NULL );
		__IME_ASSERT( numBytes > 0 );

#if defined(__IME_PLATFORM_WIN64)
		DWORD bytesToRead;
		Packed2x32to64Bit conv(numBytes);
		bytesToRead = conv.m_32bits.upper;
#else
		DWORD bytesToRead = numBytes;
#endif

		DWORD bytesRead;
		BOOL result = ReadFile( handle, buf, bytesToRead, &bytesRead, NULL );
		if( NULL == result ) {
			Core::System::Error("OSWrapper: ReadFile() failed!");
		}

#if defined(__IME_PLATFORM_WIN64)
		conv.m_32bits.upper = bytesRead;
		return conv.m_64bit;
#else
		return bytesRead;
#endif
	}

	void OSWrapper::Seek( Handle handle, FileSystem::Offset offset, FileSystem::SeekOrigin orig )
	{
		__IME_ASSERT( NULL != handle );
		DWORD moveMethod;
		DWORD offsetBytes;

		/// Switch through over enumeration
		switch( orig ) {
		case FileSystem::SeekOriginEnum::Begin:
				moveMethod = FILE_BEGIN;
				break;
		case FileSystem::SeekOriginEnum::Current:
				moveMethod = FILE_CURRENT;
				break;
		case FileSystem::SeekOriginEnum::End:
				moveMethod = FILE_END;
				break;
		default:
				// can't happen
				moveMethod = FILE_BEGIN;
				break;
		}

#if defined(__IME_PLATFORM_WIN64)
		Packed2x32to64Bit conv( offset );
		offsetBytes = conv.m_32bits.upper;
#else
		offsetBytes = offset;
#endif

		SetFilePointer( handle, offsetBytes, NULL, moveMethod );
	}

	FileSystem::Position OSWrapper::Tell( Handle handle )
	{
		__IME_ASSERT( NULL != handle );
		DWORD result = SetFilePointer( handle, 0, NULL, FILE_CURRENT );

#if defined(__IME_PLATFORM_WIN64)
		Packed2x32to64Bit conv( result, 0 );
		return conv.m_64bit;
#else
		return result;
#endif
	}

	void OSWrapper::Flush( Handle handle )
	{
		__IME_ASSERT( NULL != handle );
		FlushFileBuffers(handle);
	}

	bool OSWrapper::Eof( Handle handle )
	{
		__IME_ASSERT( NULL != handle );
		DWORD fpos = SetFilePointer( handle, 0, NULL, FILE_CURRENT );
		DWORD size = ::GetFileSize( handle, NULL );

		// NOTE: THE '>=' IS NOT A BUG!!!
		return fpos >= size;
	}

	FileSystem::Size OSWrapper::GetFileSize( Handle handle )
	{
		__IME_ASSERT( NULL != handle );
		return ::GetFileSize( handle, NULL );
	}

	void OSWrapper::SetReadOnly( const FileSystem::Path& path, bool readOnly ) {
#if defined( __IME_PLATFORM_WIN64 )
		/// Assertion
		__IME_ASSERT(path.IsValid());
		LPCWSTR widePath = path.AsCharPtr();
		/// Get file attributes
		DWORD fileAttrs = GetFileAttributesW( widePath );
		if (readOnly) {
			fileAttrs |= FILE_ATTRIBUTE_READONLY;
		} else {
			fileAttrs &= ~FILE_ATTRIBUTE_READONLY;
		}
		/// Setup file attributes
		SetFileAttributesW( widePath, fileAttrs );
#else
		/// NOT AVAILABLE ON XBOX360
#endif
	}

	bool OSWrapper::IsReadOnly( const FileSystem::Path& path )
	{
#if defined( __IME_PLATFORM_WIN64 )
		/// Assertion
		__IME_ASSERT( path.IsValid() );
		LPCWSTR widePath = reinterpret_cast< LPCWSTR >( path.AsCharPtr() );
		/// Get file attributes
		DWORD fileAttrs = GetFileAttributesW( widePath );
		return( fileAttrs & FILE_ATTRIBUTE_READONLY );
#else
		// always read-only on the 360
		return true;
#endif
	}

	bool OSWrapper::DeleteFile(const FileSystem::Path& path)
	{
		/// Assertion
		__IME_ASSERT(path.IsValid());
#if defined( __IME_PLATFORM_XBOX_ONE )
		/// Get path and convert to native type
		STL::String nativePath = path;
		nativePath.SubstituteChar('/', '\\');
		/// Delete file
		return (0 != ::DeleteFileA(nativePath.AsCharPtr()));
#else
		LPCWSTR widePath = reinterpret_cast< LPCWSTR >( path.AsCharPtr() );
		/// Delete file
		return( NULL != ::DeleteFileW( widePath ) );
#endif
	}

	bool OSWrapper::DeleteDirectory(const FileSystem::Path& path)
	{
		__IME_ASSERT(path.IsValid());
#if defined( __IME_PLATFORM_XBOX_ONE )
		/// Get path and convert to native type
		STL::String nativePath = path;
		nativePath.SubstituteChar('/', '\\');
		/// Delete file
		return( NULL != ::RemoveDirectoryA(nativePath.AsCharPtr()) );
#else
		LPCWSTR widePath = reinterpret_cast< LPCWSTR >( path.AsCharPtr() );
		/// Delete file
		return( NULL != ::RemoveDirectoryW( widePath ) );
#endif
	}

	bool OSWrapper::FileExists(const FileSystem::Path& path)
	{
		__IME_ASSERT(path.IsValid());

#if defined( __IME_PLATFORM_XBOX_ONE )
		DWORD fileAttrs = GetFileAttributesA(nativePath.AsCharPtr());
#else
		LPCWSTR widePath = path.AsCharPtr();
		DWORD fileAttrs = GetFileAttributesW( widePath );
#endif

		return ((-1 != fileAttrs) && (0 == (FILE_ATTRIBUTE_DIRECTORY & fileAttrs))) ? true : false;
	}

	bool OSWrapper::DirectoryExists( const FileSystem::Path& path )
	{
		__IME_ASSERT(path.IsValid());

#if defined( __IME_PLATFORM_XBOX_ONE )
		DWORD fileAttrs = GetFileAttributesA(nativePath.AsCharPtr());
#else
		LPCWSTR widePath = reinterpret_cast< LPCWSTR >( path.AsCharPtr() );
		DWORD fileAttrs = GetFileAttributesW( (LPCWSTR)widePath );
#endif

		return ((-1 != fileAttrs) && (0 != (FILE_ATTRIBUTE_DIRECTORY & fileAttrs))) ? true : false;
	}

	void OSWrapper::SetFileWriteTime( const FileSystem::Path& path, FileSystem::FileTime fileTime )
	{
		/// Assertion
		__IME_ASSERT(path.IsValid());
		/// Try open file with read-write permission and sequental access pattern
		Handle h = OSWrapper::OpenFile( path, FileSystem::AccessMode::ReadWrite, FileSystem::AccessPattern::Sequential );
		if( h != NULL ) {
			/// Setup time of file(change, creation, etc.)
			SetFileTime( h, NULL, NULL, &fileTime.GetFileTime() );
			/// Close handle
			OSWrapper::CloseFile( h );
		} else {
			/// Raise error on screen
			Core::System::Error( STL::String< Char8 >::VA( "OSWrapper::SetFileWriteTime(): failed to open file '%s'!",
														   STL::StringConverter::WideToUTF8( path.AsCharPtr() ).AsCharPtr() ) );
		}
	}

	FileSystem::FileTime OSWrapper::GetFileWriteTime( const FileSystem::Path& path )
	{
		/// Assertion
		__IME_ASSERT( path.IsValid() );
		FileSystem::FileTime fileTime;
		/// Try open file with read-write permission and sequental access pattern
		Handle h = OSWrapper::OpenFile( path, FileSystem::AccessMode::ReadWrite, FileSystem::AccessPattern::Sequential );
		if ( h != NULL ) {
			/// Get file time properties
			GetFileTime( h, NULL, NULL, &fileTime.GetFileTime() );
			OSWrapper::CloseFile( h );
		}
		else
		{
			/// do not fail hard if file does not exist
			Debug::Messaging::Send( Debug::EMessageType::Warning, 
									STL::String< Char8 >::VA( "Win360FSWrapper::GetFileWriteTime(): failed to open file '%s'!", 
															  STL::StringConverter::WideToUTF8(path.AsCharPtr()).AsCharPtr()) );
		}
		return fileTime;
	}

	bool OSWrapper::CreateDirectory( const FileSystem::Path& path )
	{
		/// Assertion
		__IME_ASSERT( path.IsValid() );

#if defined( __IME_PLATFORM_XBOX_ONE )
		return (0 != ::CreateDirectoryA(nativePath.AsCharPtr(), NULL));
#else
		LPCWSTR widePath = reinterpret_cast< LPCWSTR >( path.AsCharPtr() );
		return( NULL != ::CreateDirectoryW( widePath, NULL ) );
#endif
	}

	STL::Array< FileSystem::Path > OSWrapper::ListFiles( const FileSystem::Path& dirPath, const FileSystem::Path& pattern )
	{
		__IME_ASSERT( dirPath.IsValid() );
		__IME_ASSERT( pattern.IsValid() );
    
		STL::Array< FileSystem::Path > result;
		FileSystem::Path pathWithPattern;
		HANDLE hFind;

#if defined( __IME_PLATFORM_XBOX_ONE )
		pathWithPattern.Append( dirPath );
		pathWithPattern.Append( NDKOSPath( "\\" ) );
		pathWithPattern.Append( pattern );

		WIN32_FIND_DATA findFileData;
		hFind = FindFirstFileA( pathWithPattern.AsCharPtr(), &findFileData );
		if( INVALID_HANDLE_VALUE != hFind ) {
			do {
				if (0 == (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
					result.Append(findFileData.cFileName);
				}
			} while (FindNextFile(hFind, &findFileData) != 0);
			FindClose(hFind);
		}
#else
		pathWithPattern.Append( dirPath );
		pathWithPattern.Append( NDKOSPath( "/" ) );
		pathWithPattern.Append( pattern );

		LPCWSTR widePath = pathWithPattern.AsCharPtr();

		WIN32_FIND_DATAW findFileData;
		hFind = FindFirstFileW( widePath, &findFileData );  
		if (INVALID_HANDLE_VALUE != hFind) {
			do {
				if (0 == (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) {
					result.Append( FileSystem::Path( findFileData.cFileName ) );
				}
			} while (FindNextFileW( hFind, &findFileData ) != 0);
			FindClose(hFind);
		}
#endif

		return result;
	}

	STL::Array< FileSystem::Path > OSWrapper::ListDirectories( const FileSystem::Path& dirPath, const FileSystem::Path& pattern )
	{
		__IME_ASSERT( dirPath.IsValid() );
		__IME_ASSERT( pattern.IsValid() );
    
		STL::Array< FileSystem::Path > result;
		HANDLE hFind; //!< First found file handle
		FileSystem::Path pathWithPattern; 

#if defined( __IME_PLATFORM_XBOX_ONE )
		pathWithPattern.Append( dirPath );
		pathWithPattern.Append( NDKOSPath( "\\" ) );
		pathWithPattern.Append( pattern );

		WIN32_FIND_DATA findFileData;
		hFind = FindFirstFile(pathWithPattern.AsCharPtr(), &findFileData);
		if (INVALID_HANDLE_VALUE != hFind) {
			do {
				String fileName = findFileData.cFileName;
				if ((0 != (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) && (fileName != "..") && (fileName != ".")) {
					result.Append(findFileData.cFileName);
				}
			} while (FindNextFile(hFind, &findFileData) != 0);
			FindClose(hFind);
		}
#else
		pathWithPattern.Append( dirPath );
		pathWithPattern.Append( NDKOSPath( "/" ) );
		pathWithPattern.Append( pattern );

		LPCWSTR widePath = pathWithPattern.AsCharPtr();

		WIN32_FIND_DATAW findFileData;
		hFind = FindFirstFileW( (LPCWSTR)widePath, &findFileData );  
		if (INVALID_HANDLE_VALUE != hFind) {
			do {
				FileSystem::Path fileName;
				fileName.Assign( findFileData.cFileName );
				if ((0 != (findFileData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)) && (fileName != NDKOSPath( ".." ) ) && (fileName != NDKOSPath( "." ) ) ) {
					result.Append( fileName );
				}
			} while (FindNextFileW( hFind, &findFileData ) != 0);
			FindClose(hFind);
		}
#endif
		return result;
	}

	FileSystem::Path OSWrapper::GetUserDirectory( void )
	{
#if defined( __IME_PLATFORM_WIN64 )
		wchar_t wideBuffer[512] = { 0 };
		HRESULT hr = SHGetFolderPathW(NULL, 
										CSIDL_PERSONAL | CSIDL_FLAG_CREATE, 
										NULL, 
										0,
										(LPWSTR)wideBuffer);
		__IME_ASSERT( SUCCEEDED(hr) );
		FileSystem::Path result( Traits::CtorHasVA(), 
								 NDKOSPath( "file:///%s" ), wideBuffer );
		return result;
#else
		// there is no user: assign on the 360
		return "";
#endif
	}

	FileSystem::Path OSWrapper::GetAppDataDirectory( void )
	{    
#if defined( __IME_PLATFORM_WIN64 )
		wchar_t wideBuffer[512] = { 0 };
		HRESULT hr = SHGetFolderPathW(NULL, 
										CSIDL_APPDATA | CSIDL_FLAG_CREATE, 
										NULL, 
										0,
										(LPWSTR)wideBuffer);
		__IME_ASSERT( SUCCEEDED(hr) );
		FileSystem::Path result( Traits::CtorHasVA(),
								 NDKOSPath( "file:///%s" ), wideBuffer );
		return result;
#else
		// there is no user: assign on the 360
		return "";
#endif
	}

	FileSystem::Path OSWrapper::GetProgramsDirectory( void )
	{
#if defined( __IME_PLATFORM_WIN64 )
		wchar_t wideBuffer[512] = { 0 };
		HRESULT hr = SHGetFolderPathW(NULL,
										CSIDL_PROGRAM_FILES,
										NULL,
										0,
										(LPWSTR)wideBuffer);
		__IME_ASSERT( SUCCEEDED(hr) );
		FileSystem::Path result( Traits::CtorHasVA(),
								 NDKOSPath("file:///%s"), wideBuffer);
		return result;
#else
		// there is no programs: assign on the 360
		return "";
#endif
	}

	FileSystem::Path OSWrapper::GetTempDirectory( void )
	{
#if defined( __IME_PLATFORM_WIN64 )
		wchar_t wideBuffer[512] = { 0 };
		GetTempPathW( sizeof(wideBuffer) / 2, (LPWSTR)wideBuffer );
		FileSystem::Path result(Traits::CtorHasVA(),
								NDKOSPath("file:///%s"), wideBuffer);
		return result;
#else
			// @todo: CAREFUL, THIS ONLY EXISTS ON A XBOX360 DEVKIT!
			return "file:///DEVKIT:";
#endif            
	}

	FileSystem::Path OSWrapper::GetBinDirectory( void )
	{
#if defined( __IME_PLATFORM_WIN64 )
		wchar_t wideBuffer[512];
		DWORD res = GetModuleFileNameW( NULL, (LPWSTR)wideBuffer, sizeof(wideBuffer) / 2 );
		__IME_ASSERT( NULL != res );
		::PathRemoveFileSpecW( wideBuffer );
		FileSystem::Path result = wideBuffer;
		return wideBuffer;
#else
		return NDKOSPath("file:///GAME:/");
#endif
	}

	/*FileSystem::Path OSWrapper::GetHomeDirectory()
	{
#if defined( __IME_PLATFORM_WIN64 )
		wchar_t wideBuffer[512];
		DWORD res = GetModuleFileNameW( NULL, (LPWSTR)wideBuffer, sizeof(wideBuffer) / 2 );
		__IME_ASSERT( NULL != res );

		STL::String pathToExe = STL::StringConverter::WideToUTF8( wideBuffer );
		pathToExe.ConvertBackslashes();

		// check if executable resides in a win32 directory
		STL::String pathToDir = pathToExe.ExtractLastDirName();
		if( STL::String::StrCmp( pathToDir.AsCharPtr(), "win32" ) == NULL ) {
			/// normal home:bin/win32 directory structure
			/// strip bin/win32
			STL::String homePath = pathToExe.ExtractDirName();
			homePath = homePath.ExtractDirName();
			homePath = homePath.ExtractDirName();
			homePath.TrimRight("/");
			return STL::String("file:///") + homePath;
		} else {
			// not in normal home:bin/win32 directory structure, 
			// use the exe's directory as home path
			STL::String homePath = pathToExe.ExtractDirName();
			return STL::String("file:///") + homePath;
		}
#else
		// Xbox360 case is a bit simpler...
		return "file:///GAME:/";
#endif
	}*/

	bool OSWrapper::IsDeviceName( const FileSystem::Path& str )
	{
#if defined( __IME_PLATFORM_WIN64 )
		if (str.Length() == 1) {
			Char16 c = str[0];
			if (((c >= 'A') && (c <= 'Z')) || ((c >= 'a') && (c <= 'z'))) {
				return true;
			}
		}
		return false;
#else
		// on Xbox360:
		if (str == "GAME") return true;
		else if (str == "DEVKIT") return true;
		else return false;
#endif
	}
}