/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef _Win32FileSystemWrapper_h_
#define _Win32FileSystemWrapper_h_

/// Shared code
#include <Foundation/Shared.h>
/// File system types
#include <Foundation/FileSystemTypes.h>
/// STL array
#include <Foundation/STLArray.h>
/// Stream
#include <Foundation/Stream.h>
/// File time
#include <Foundation/FileTime.h>

/// Filesystem namespace
namespace FileSystem
{
	/// <summary>
	/// <c>OSWrapper</c> is a wrapper around Windows API file system.
	/// </summary>
	class OSWrapper
	{
	public:
		typedef HANDLE Handle; //!< Handle type definition

#if defined( __IME_PLATFORM_XBOX_ONE )
		static const u8 FileDelimiter = '\\';
#endif

		/// <summary cref="OSWrapper::OpenFile">
		/// Open new file session.
		/// </summary>
		/// <param name="path">Path to the file, includin file name with extension.</param>
		/// <param name="accessMode">Access mode of current session.</param>
		/// <param name="accessPattern">Access pattern of current session.</param>
		/// <param name="flagsAndAttributes">Additional flags and attributes for platform-specific situation.</param>
		/// <returns>Valid handle(e.g. pointer) to the object to use it.</returns>
		/// <remarks>
		/// Open a file using the Xbox360 function CreateFile(). Returns a handle
		/// to the file which must be passed to the other Win360FSWrapper file methods.
		/// If opening the file fails, the function will return 0. The filename
		/// must be a native Xbox360 path (no assigns, etc...).
		/// </remarks>
		static Handle OpenFile( const FileSystem::Path& path, FileSystem::AccessMode accessMode, FileSystem::AccessPattern accessPattern, 
								DWORD flagsAndAttributes = 0 );
		
		/// <summary cref="OSWrapper::CloseFile">
		/// Close current file session with selected handler.
		/// </summary>
		/// <param name="h">Handler of opened file session.</param>
		static void CloseFile( Handle h );

		/// <summary cref="OSWrapper::Write">
		/// Write operation to the opened file session.
		/// </summary>
		/// <param name="h">Handle to the opened session.</param>
		/// <param name="buf">Buffer to be written into the file session.</param>
		/// <param name="numBytes">Size of written data.</param>
		static void Write( Handle h, const void* buf, FileSystem::Size numBytes );

		/// <summary cref="OSWrapper::Read">
		/// Write operation to the opened file session.
		/// </summary>
		/// <param name="h">Handle to the opened session.</param>
		/// <param name="buf">Buffer to be read from the file session.</param>
		/// <param name="numBytes">Size of buffer data. Just to prevent buffer overflow.</param>
		/// <returns>Size of read data.</returns>
		static FileSystem::Size Read( Handle h, void* buf, FileSystem::Size numBytes );

		/// <summary cref="OSWrapper::Seek">
		/// Seek in a file.
		/// </summary>
		/// <param name="h">Handle to the opened session.</param>
		/// <param name="offset">Offset(in bytes) to the seek position.</param>
		/// <param name="orig">Origin of the seeking in opened session.</param>
		static void Seek( Handle h, FileSystem::Offset offset, FileSystem::SeekOrigin orig );

		/// <summary cref="OSWrapper::Tell">
		/// Get position in file.
		/// </summary>
		/// <returns>Position of current opened session.</returns>
		static FileSystem::Position Tell( Handle h );

		/// <summary cref="OSWrapper::Flush">
		/// Flush a file.
		/// </summary>
		/// <param name="h">Handle to the opened session.</param>
		static void Flush( Handle h );

		/// <summary cref="OSWrapper::Eof">
		/// Check if we are in the end of file.
		/// </summary>
		/// <param name="h">Handle to the opened session.</param>
		/// <returns>True if at end-of-file, otherwise false.</returns>
		static bool Eof( Handle h );

		/// <summary cref="OSWrapper::GetFileSize">
		/// Get size of a file in bytes.
		/// </summary>
		/// <param name="h">Handle to the opened session.</param>
		/// <returns>Size of file.</returns>
		static FileSystem::Size GetFileSize( Handle h );

		/// <summary cref="OSWrapper::SetReadOnly">
		/// Setup file as read-only, or not.
		/// </summary>
		/// <param name="path">Path to the file, including file name.</param>
		/// <param name="readOnly">Read-only attribute.</param>
		/// <remarks>This method does nothing on Xbox 360.</remarks>
		static void SetReadOnly( const FileSystem::Path& path, bool readOnly = true );

		/// <summary cref="OSWrapper::IsReadOnly">
		/// Get read-only status of a file.
		/// </summary>
		/// <param name="path">Path to the file, including file name.</param>
		/// <returns>True if file is read-only.</returns>
		static bool IsReadOnly( const FileSystem::Path& path );

		/// <summary cref="OSWrapper::DeleteFile">
		/// Delete a file.
		/// </summary>
		/// <param name="path">Path to the file, including file name.</param>
		/// <returns>True if file exists and was deleted, otherwise false.</returns>
		static bool DeleteFile( const FileSystem::Path& path );

		/// <summary cref="OSWrapper::DeleteDirectory">
		/// Delete an empty directory.
		/// </summary>
		/// <param name="path">Path to the file, including file name.</param>
		/// <returns>True if proposed directory is empty and was deleted, otherwise false.</returns>
		static bool DeleteDirectory( const FileSystem::Path& path );

		/// <summary cref="OSWrapper::FileExists">
		/// Check if file really exists.
		/// </summary>
		/// <param name="path">Path to the file, including file name.</param>
		/// <returns>True if a file exists, otherwise false.</returns>
		static bool FileExists( const FileSystem::Path& path );

		/// <summary cref="OSWrapper::DirectoryExists">
		/// Check if directory really exists.
		/// </summary>
		/// <param name="path">Path to the file, including file name.</param>
		/// <returns>True if a directory exists, otherwise false.</returns>
		static bool DirectoryExists( const FileSystem::Path& path );

		/// <sumary cref="OSWrapper::SetFileWriteTime">
		/// Set the write-access time stamp of a file.
		/// </summary>
		/// <param name="path">Path to the file, including file name.</param>
		/// <param name="fileTime">Time of the file.</param>
		static void SetFileWriteTime( const FileSystem::Path& path, FileSystem::FileTime fileTime );

		/// <summary cref="OSWrapper::GetFileWriteTime">
		/// Get the last write-access time stamp of a file.
		/// </summary>
		/// <param name="path">Path to the file, including file name.</param>
		/// <returns>Time of the last write.</returns>
		static FileSystem::FileTime GetFileWriteTime( const FileSystem::Path& path );

		/// <summary cref="OSWrapper::CreateDirectory">
		/// Create a directory.
		/// </summary>
		/// <param name="path">Path to the directory including new directory name.</param>
		/// <returns>True if directory has been successfully created, otherwise false.</returns>
		static bool CreateDirectory( const FileSystem::Path& path );

		/// <summary cref="OSWrapper::ListFiles">
		/// List all files in a directory.
		/// </summary>
		/// <param name="dirPath">Reference to the full name of directory.</param>
		/// <param name="pattern">Reference to the pattern specialized for listing.</param>
		/// <returns>Array of listed files.</returns>
		static STL::Array< FileSystem::Path > ListFiles( const FileSystem::Path& dirPath, const FileSystem::Path& pattern );

		/// <summary cref="OSWrapper::ListDirectories">
		/// List all subdirectories in a directory.
		/// </summary>
		/// <param name="dirPath">Reference to the full name of directory.</param>
		/// <param name="pattern">Reference to the pattern specialized for listing.</param>
		/// <returns>Array of listed directories.</returns>
		static STL::Array< FileSystem::Path > ListDirectories( const FileSystem::Path& dirPath, const FileSystem::Path& pattern );

		/// <summary cref="OSWrapper::GetUserDirectory">
		/// Get path to the current user's home directory (for user: standard assign).
		/// </summary>
		/// <returns>Pre-built string with user directory.</returns>
		static FileSystem::Path GetUserDirectory( void );

		/// <summary cref="OSWrapper::GetAppDataDirectory">
		/// Get path to the current user's appdata directory (for appdata: standard assign).
		/// </summary>
		/// <returns>Pre-built string with application data directory.</returns>
		static FileSystem::Path GetAppDataDirectory( void );

		/// <summary cref="OSWrapper::GetTempDirectory">
		/// Get path to the current user's temp directory (for temp: standard assign).
		/// </summary>
		/// <returns>Pre-built string with temporary directory.</returns>
		static FileSystem::Path GetTempDirectory( void );

		/// <summary cref="OSWrapper::GetHomeDirectory">
		/// Get path to the current application directory (for home: standard assign).
		/// </summary>
		/// <returns>Pre-built string with home directory.</returns>
		static FileSystem::Path GetHomeDirectory( void );

		/// <summary cref="OSWrapper::GetBinDirectory">
		/// Get path to the current binaries directory (for bin: standard assign).
		/// </summary>
		/// <returns>Pre-built string with binary directory.</returns>
		static FileSystem::Path GetBinDirectory( void );

		/// <summary cref="OSWrapper::GetProgramsDirectory">
		/// Get path to the "c:/program files" directory.
		/// </summary>
		/// <returns>Pre-built string with program files directory.</returns>
		static FileSystem::Path GetProgramsDirectory( void );

		/// <summary cref="OSWrapper::IsDeviceName">
		/// Check if string has device name.
		/// </summary>
		/// <returns>True when the string is a device name (e.g. "C:").</returns>
		static bool IsDeviceName( const FileSystem::Path& str );
	};
}

#endif /// _Win32FileSystemWrapper_h_