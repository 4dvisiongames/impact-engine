/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Win32 file time
#include <Foundation/Win32/Win32FileTime.h>

/// Win32FileSystem namespace
namespace FileSystem
{
	FileTime::FileTime( const STL::String< Char8 >& str )
	{
		STL::Array< STL::String< Char8 > > tokens;
		str.Tokenize( "#", tokens );
		__IME_ASSERT( tokens.Size() == 2 );
		this->time.dwHighDateTime = tokens[0].AsInt();
		this->time.dwLowDateTime = tokens[1].AsInt();
	}

	STL::String< Char8 > FileTime::AsString( void ) const
	{
		STL::String< Char8 > str( STL::String< Char8 >::VA( "%d#%d", this->time.dwHighDateTime, this->time.dwLowDateTime ) );
		return str;
	}
}