/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef _Win32FileTime_h_
#define _Win32FileTime_h_

/// Engine shared code
#include <Foundation/Shared.h>
/// STL string
#include <Foundation/STLString.h>

/// Win32FileSystem namespace
namespace FileSystem
{
	/// <summary>
	/// <c>FileTime</c> is a file time defintion for Windows platform.
	/// </summary>
	class FileTime
	{
		FILETIME time; //!< File time definition in Windows

	public:
		/// <summary cref="FileTime::FileTime">
		/// Constructor.
		/// </summary>
		FileTime( void );

		/// <summary cref="FileTime::FileTime">
		/// Constructor.
		/// </summary>
		/// <param name="str">String passed to the class.</param>
		FileTime( const STL::String< Char8 >& str );

		/// Equality operators.
		friend bool operator == ( const FileTime& a, const FileTime& b );
		friend bool operator != ( const FileTime& a, const FileTime& b );

		/// Comparison operators.
		friend bool operator > ( const FileTime& a, const FileTime& b );
		friend bool operator < ( const FileTime& a, const FileTime& b );

		/// <summary cref="FileTime::AsString">
		/// Convert to STL::String.
		/// </summary>
		/// <returns>Converted to string current time of file.</returns>
		STL::String< Char8 > AsString( void ) const;

		/// <summary cref="FileTime::GetFileTime">
		/// Get FILETIME object of current file.
		/// </summary>
		/// <returns>Reference to the object of FILETIME.</returns>
		FILETIME& GetFileTime( void );

		/// <summary cref="FileTime::GetFileTime">
		/// Get FILETIME object of current file. Read-only.
		/// </summary>
		/// <returns>Reference to the object of FILETIME.</returns>
		const FILETIME& GetFileTime( void ) const;
	};

	__IME_INLINED FileTime::FileTime( void )
	{
		time.dwHighDateTime = NULL;
		time.dwLowDateTime = NULL;
	}

	__IME_INLINED bool operator == (const FileTime& a, const FileTime& b)
	{
		const FILETIME &firstTime = a.GetFileTime();
		const FILETIME &secondTile = b.GetFileTime();
		return(0 == CompareFileTime(&firstTime, &secondTile));
	}

	__IME_INLINED bool operator != (const FileTime& a, const FileTime& b)
	{
		const FILETIME &firstTime = a.GetFileTime();
		const FILETIME &secondTile = b.GetFileTime();
		return(0 != CompareFileTime(&firstTime, &secondTile));
	}

	__IME_INLINED bool operator > (const FileTime& a, const FileTime& b)
	{
		const FILETIME &firstTime = a.GetFileTime();
		const FILETIME &secondTile = b.GetFileTime();
		return(1 != CompareFileTime(&firstTime, &secondTile));
	}

	__IME_INLINED bool operator < (const FileTime& a, const FileTime& b)
	{
		const FILETIME &firstTime = a.GetFileTime();
		const FILETIME &secondTile = b.GetFileTime();
		return(-1 != CompareFileTime(&firstTime, &secondTile));
	}

	__IME_INLINED FILETIME& FileTime::GetFileTime( void )
	{
		return this->time;
	}

	__IME_INLINED const FILETIME& FileTime::GetFileTime( void ) const
	{
		return this->time;
	}
}

#endif /// _Win32FileTime_h_