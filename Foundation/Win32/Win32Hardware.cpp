/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <NTstatus.h>
#define WIN32_NO_STATUS

/// Win32 hardware
#include <Foundation/Win32/Win32Hardware.h>
/// Low-level memory
#include <Foundation/LowLevelMemory.h>
/// String
#include <Foundation/STLString.h>

#include <powrprof.h>

typedef struct _PROCESSOR_POWER_INFORMATION
{
	ULONG  Number;
	ULONG  MaxMhz;
	ULONG  CurrentMhz;
	ULONG  MhzLimit;
	ULONG  MaxIdleState;
	ULONG  CurrentIdleState;
} PROCESSOR_POWER_INFORMATION, *PPROCESSOR_POWER_INFORMATION;

/// Windows core namespace
namespace Core
{
	Hardware::Hardware( void ) 
		: bSupportsSSE2(false)
		, bSupportsSSE3(false)
		, bSupportsAVX(false)
		, bSupportsCMPXCHG8B(false)
		, bSupportsHT(false) {
		/// CPU-related features
		s32 CPUInfo[4] = { -1 };
		s32 mainFeatures = 0;
		s32 additionalFeatures = 0;
		char cpuName[0x20];
		char cpuVendor[0x40];
		/// Get runtime-info from Windows
		SYSTEM_INFO sysInfo;
		::GetSystemInfo(&sysInfo);
		/// Write contents to variables
		this->numCpuCores = sysInfo.dwNumberOfProcessors;
		this->pageSize = sysInfo.dwPageSize;

		/// Get CPUID of vendor information
		__cpuid(CPUInfo, 0);
		mainFeatures = CPUInfo[0];

		/// Clear CPU name string
		Memory::LowLevelMemory::EraseMemoryBlock( cpuName, sizeof( s8 ) * 0x20 );
		/// Retrieve name of CPU name
		*((int*)cpuName) = CPUInfo[1];
		*((int*)(cpuName+4)) = CPUInfo[3];
		*((int*)(cpuName+8)) = CPUInfo[2];

		// Calling __cpuid with 0x80000000 as the InfoType argument
		// gets the number of valid extended IDs.
		__cpuid(CPUInfo, 0x80000000);
		additionalFeatures = CPUInfo[0];

		/// Clear CPU vendor string
		Memory::LowLevelMemory::EraseMemoryBlock( cpuVendor, sizeof( s8 ) * 0x40 );
		for( int idx = 0x80000000; idx <= additionalFeatures; ++idx) {
			__cpuid(CPUInfo, idx);
			// Interpret CPU brand string and cache information.
			if(idx == 0x80000002)
				Memory::LowLevelMemory::CopyMemoryBlock( CPUInfo, cpuVendor, sizeof( CPUInfo ) );
			else if(idx == 0x80000003)
				Memory::LowLevelMemory::CopyMemoryBlock( CPUInfo, cpuVendor + 16, sizeof( CPUInfo ) );
			else if(idx == 0x80000004)
				Memory::LowLevelMemory::CopyMemoryBlock( CPUInfo, cpuVendor + 32, sizeof( CPUInfo ) );
			else
				continue;
		}

		/// Check for additional features
		for( int idx = 0; idx <= mainFeatures; ++idx ) {
			__cpuid(CPUInfo, idx);
			if ( idx == 1 ) {
				this->bSupportsSSE2 = (CPUInfo[3] & (1 << 26)) ? true : false;
				this->bSupportsSSE3 = ((CPUInfo[2] & 0x1) || false) ? true : false;
				this->bSupportsCMPXCHG8B = (CPUInfo[3] & (1 << 8)) ? true : false;
				this->bSupportsHT = (CPUInfo[3] & (1 << 28)) ? true : false;

				bool tempAVX = (CPUInfo[2] & (1 << 27)) && ((CPUInfo[2] & (1 << 28)));
				if ( tempAVX ) {
					u64 xrcFeatureMask = _xgetbv( _XCR_XFEATURE_ENABLED_MASK );
					this->bSupportsAVX = (tempAVX && (xrcFeatureMask & 0x6)) ? true : false;
				}
			}
		}

		/// Clear memory
		Memory::LowLevelMemory::EraseMemoryBlock( cpuString, sizeof( char ) * 80 );
		/// Get CPU brand name
		const Char8* localCpu = STL::String< Char8 >::VA( "CPU: %s (%s)", cpuName, cpuVendor );
		STL::String< Char8 >::SizeType localCpuSize = STL::String< Char8 >::TraitsType::StringLength( localCpu );
		if( localCpuSize > sizeof( cpuString ) ) {
			localCpuSize = sizeof( cpuString );
		}
		Memory::LowLevelMemory::CopyMemoryBlock( localCpu, cpuString, localCpuSize );
	}

	s32 Hardware::GetConcurrency( void ) const
	{
		return this->numCpuCores;
	}

	s32 Hardware::GetPageSize( void ) const
	{
		return this->pageSize;
	}
} /// namespace Win32Core