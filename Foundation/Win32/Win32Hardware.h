/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef _Win32Hardware_h_
#define _Win32Hardware_h_

/// Shared code
#include <Foundation/Shared.h>

/// Windows core namespace
namespace Core
{
	class Hardware
	{
	private:
		s32 numCpuCores; //!< Number of CPU cores(including logical from Intel's HT)
		s32 pageSize; //!< Size of page


		bool bSupportsSSE2; //!< Supports SSE2
		bool bSupportsSSE3; //!< Supports SSE3
		bool bSupportsAVX; //!< Supports AVX
		bool bSupportsCMPXCHG8B; //!< Supports CompageExchange8 hardware
		bool bSupportsHT; //!< Supports Hyper-Threading feature
		s8 cpuString[80]; //!< CPU String

	public:
		/// Constructor
		Hardware( void );

		/// Get hardware concurrency
		s32 GetConcurrency( void ) const;
		/// Get disk page size
		s32 GetPageSize( void ) const;
		/// If CPU supports SSE2
		bool SupportsSSE2( void ) const;
		/// If CPU supports SSE3
		bool SupportsSSE3( void ) const;
		/// If CPU supports AVX
		bool SupportAVX( void ) const;
		/// If CPU supports CompageExchange8
		bool SupportsCMPXCHG8B( void ) const;
		/// If CPU supports CompageExchange8
		bool SupportsHT( void ) const;
		/// Get CPU name and brand-name
		const s8* GetCPUString( void );
	};

	__IME_INLINED bool Hardware::SupportsSSE2( void ) const {
		return this->bSupportsSSE2;
	}

	__IME_INLINED bool Hardware::SupportsSSE3( void ) const {
		return this->bSupportsSSE3;
	}

	__IME_INLINED bool Hardware::SupportAVX( void ) const {
		return this->bSupportsAVX;
	}

	__IME_INLINED bool Hardware::SupportsCMPXCHG8B( void ) const {
		return this->bSupportsCMPXCHG8B;
	}

	__IME_INLINED bool Hardware::SupportsHT( void ) const {
		return this->bSupportsHT;
	}

	__IME_INLINED const s8* Hardware::GetCPUString( void ) {
		return &(this->cpuString[0]);
	}
}; /// namespace Win32Core

#endif /// _Win32Hardware_h_