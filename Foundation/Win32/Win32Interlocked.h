/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef _Win32Interlocked_h_
#define _Win32Interlocked_h_

#pragma intrinsic(_InterlockedCompareExchange)
#pragma intrinsic(_InterlockedExchangeAdd)
#pragma intrinsic(_InterlockedExchange)
#pragma intrinsic(_InterlockedAnd)
#pragma intrinsic(_InterlockedOr)
#pragma intrinsic(_InterlockedXor)

// MSVC 2010 And later provide intrinsics for 8 And 16 bit integers
#pragma intrinsic(_InterlockedCompareExchange8)
#pragma intrinsic(_InterlockedExchangeAdd8)
#pragma intrinsic(_InterlockedExchange8)
#pragma intrinsic(_InterlockedAnd8)
#pragma intrinsic(_InterlockedOr8)
#pragma intrinsic(_InterlockedXor8)

#pragma intrinsic(_InterlockedCompareExchange16)
#pragma intrinsic(_InterlockedExchangeAdd16)
#pragma intrinsic(_InterlockedExchange16)
#pragma intrinsic(_InterlockedAnd16)
#pragma intrinsic(_InterlockedOr16)
#pragma intrinsic(_InterlockedXor16)

#if defined( __IME_x86_64 )
#pragma intrinsic(_InterlockedCompareExchange64)
#pragma intrinsic(_InterlockedExchangeAdd64)
#pragma intrinsic(_InterlockedExchange64)
#pragma intrinsic(_InterlockedAnd64)
#pragma intrinsic(_InterlockedOr64)
#pragma intrinsic(_InterlockedXor64)
#else
extern "C" __int64 _MyInterlockedCompareExchange64(volatile void *ptr, __int64 value, __int64 comparand );
extern "C" __int64 _MyInterlockedExchangeAdd64(volatile void *ptr, __int64 value );
extern "C" __int64 _MyInterlockedExchange64(volatile void *ptr, __int64 value );
#endif

#define ATOMIC_COMPARE_EXCHANGE_1( ptr, value, comparand ) _InterlockedCompareExchange8( (volatile CHAR*)ptr, (CHAR)(value), (CHAR)(comparand) )
#define ATOMIC_EXCHANGE_ADD_1( ptr, addend ) _InterlockedExchangeAdd8( (volatile CHAR*)(ptr), (CHAR)(addend) )
#define ATOMIC_EXCHANGE_1( ptr, value ) _InterlockedExchange8( (volatile CHAR*)(ptr), (CHAR)(value) );

#define ATOMIC_COMPARE_EXCHANGE_2( ptr, value, comparand ) _InterlockedCompareExchange16( (volatile SHORT*)ptr, (SHORT)(value), (SHORT)(comparand) )
#define ATOMIC_EXCHANGE_ADD_2( ptr, addend ) _InterlockedExchangeAdd16( (volatile SHORT*)(ptr), (SHORT)(addend) )
#define ATOMIC_EXCHANGE_2( ptr, value ) _InterlockedExchange16( (volatile SHORT*)(ptr), (SHORT)(value) );

#define ATOMIC_COMPARE_EXCHANGE_4( ptr, value, comparand ) _InterlockedCompareExchange( (volatile long*)ptr, (long)(value), (long)(comparand) )
#define ATOMIC_EXCHANGE_ADD_4( ptr, addend ) _InterlockedExchangeAdd( (volatile long*)(ptr), (long)(addend) )
#define ATOMIC_EXCHANGE_4( ptr, value ) _InterlockedExchange( (volatile long*)(ptr), (long)(value) );

#define ATOMIC_LOGICAL_OR_1( ptr, addend ) _InterlockedOr8( (volatile CHAR*)ptr, (CHAR)addend );
#define ATOMIC_LOGICAL_AND_1( ptr, addend ) _InterlockedAnd8( (volatile CHAR*)ptr, (CHAR)addend );
#define ATOMIC_LOGICAL_XOR_1( ptr, addend ) _InterlockedXor8( (volatile CHAR*)ptr, (CHAR)addend );

#define ATOMIC_LOGICAL_OR_2( ptr, addend ) _InterlockedOr16( (volatile SHORT*)ptr, (SHORT)addend );
#define ATOMIC_LOGICAL_AND_2( ptr, addend ) _InterlockedAnd16( (volatile SHORT*)ptr, (SHORT)addend );
#define ATOMIC_LOGICAL_XOR_2( ptr, addend ) _InterlockedXor16( (volatile SHORT*)ptr, (SHORT)addend );

#if defined( __IME_x86_64 )
#define ATOMIC_COMPARE_EXCHANGE_8( ptr, value, comparand ) _InterlockedCompareExchange64( (volatile __int64*)ptr, (__int64)(value), (__int64)(comparand) )
#define ATOMIC_EXCHANGE_ADD_8( ptr, addend ) _InterlockedExchangeAdd64( (volatile __int64*)ptr, (__int64)(addend) )
#define ATOMIC_EXCHANGE_8( ptr, value ) _InterlockedExchange64( (volatile __int64*)ptr, (__int64)(value) );

#define ATOMIC_LOGICAL_OR( ptr, addend ) _InterlockedOr64( (volatile __int64*)ptr, (__int64)addend );
#define ATOMIC_LOGICAL_AND( ptr, addend ) _InterlockedAnd64( (volatile __int64*)ptr, (__int64)addend );
#define ATOMIC_LOGICAL_XOR( ptr, addend ) _InterlockedXor64( (volatile __int64*)ptr, (__int64)addend );
#else
#define ATOMIC_COMPARE_EXCHANGE_8( ptr, value, comparand ) _MyInterlockedCompareExchange64( ptr, (__int64)(value), (__int64)(comparand) )
#define ATOMIC_EXCHANGE_ADD_8( ptr, addend ) _MyInterlockedExchangeAdd64( ptr, (__int64)(addend) )
#define ATOMIC_EXCHANGE_8( ptr, value ) _MyInterlockedExchange64( ptr, (__int64)(value) );

#define ATOMIC_LOGICAL_OR( ptr, addend ) _InterlockedOr( (volatile long*)ptr, (long)addend );
#define ATOMIC_LOGICAL_AND( ptr, addend ) _InterlockedAnd( (volatile long*)ptr, (long)addend );
#define ATOMIC_LOGICAL_XOR( ptr, addend ) _InterlockedXor( (volatile long*)ptr, (long)addend );
#endif

#endif /// _Win32Interlocked_h_
