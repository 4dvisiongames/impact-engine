; ============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
;  Desc: Windows-compatible interlocked functions definition for NanoEngine.
;
;  Author: Pavel Umnikov
; =====================================================================================

.686
.model flat,c
.code 
	ALIGN 4
	PUBLIC c _MyInterlockedExchangeAdd64
_MyInterlockedExchangeAdd64:
	push ebx
	push edi
	mov edi,12[esp]
	mov eax,[edi]
	mov edx,4[edi]
_MyInterlockedExchangeAdd64_loop:
	mov ebx,16[esp]
	mov ecx,20[esp]
	add ebx,eax
	adc ecx,edx
	lock cmpxchg8b qword ptr [edi]
	jnz _MyInterlockedExchangeAdd64_loop
	pop edi
	pop ebx
	ret
.code 
	ALIGN 4
	PUBLIC c _MyInterlockedExchange64
_MyInterlockedExchange64:
	push ebx
	push edi
	mov edi,12[esp]
	mov ebx,16[esp]
	mov ecx,20[esp]
	mov eax,[edi]
	mov edx,4[edi]
_MyInterlockedExchange64_loop:
	lock cmpxchg8b qword ptr [edi]
	jnz _MyInterlockedExchange64_loop
	pop edi
	pop ebx
	ret
.code 
	ALIGN 4
	PUBLIC c _MyInterlockedCompareExchange64
_MyInterlockedCompareExchange64:
	push ebx
	push edi
	mov edi,12[esp]
	mov ebx,16[esp]
	mov ecx,20[esp]
	mov eax,24[esp]
	mov edx,28[esp]
	lock cmpxchg8b qword ptr [edi]
	pop edi
	pop ebx
	ret
end