/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Foundation/LowLevelMemory.h>

namespace Memory
{
	void LowLevelMemory::CopyMemoryBlock16( _In_reads_bytes_(memSize) const void* __restrict vsrc,
											_Out_writes_bytes_(memSize) void* __restrict vdest,
											size_t memSize )
	{
		__IME_ASSERT_MSG( vsrc, "Source data must be provided to copy from" );
		__IME_ASSERT_MSG( vdest, "Destination point must be valid!" );
		__IME_ASSERT_MSG( memSize, "Size of copyable data must be higher than zero!" );

		__IME_ASSERT( Math::IsAligned( vsrc, 16 ) );
		__IME_ASSERT( Math::IsAligned( vdest, 16 ) );

		u8* src = const_cast<u8*>(static_cast<const u8*>(vsrc));
		u8* dst = static_cast<u8*>(vdest);

		size_t i = 0;
		for( ; i + 128 <= memSize; i += 128 ) {
			__m128i d0 = _mm_load_si128( (__m128i *)&src[i + 0 * 16] );
			__m128i d1 = _mm_load_si128( (__m128i *)&src[i + 1 * 16] );
			__m128i d2 = _mm_load_si128( (__m128i *)&src[i + 2 * 16] );
			__m128i d3 = _mm_load_si128( (__m128i *)&src[i + 3 * 16] );
			__m128i d4 = _mm_load_si128( (__m128i *)&src[i + 4 * 16] );
			__m128i d5 = _mm_load_si128( (__m128i *)&src[i + 5 * 16] );
			__m128i d6 = _mm_load_si128( (__m128i *)&src[i + 6 * 16] );
			__m128i d7 = _mm_load_si128( (__m128i *)&src[i + 7 * 16] );
			_mm_stream_si128( (__m128i *)&dst[i + 0 * 16], d0 );
			_mm_stream_si128( (__m128i *)&dst[i + 1 * 16], d1 );
			_mm_stream_si128( (__m128i *)&dst[i + 2 * 16], d2 );
			_mm_stream_si128( (__m128i *)&dst[i + 3 * 16], d3 );
			_mm_stream_si128( (__m128i *)&dst[i + 4 * 16], d4 );
			_mm_stream_si128( (__m128i *)&dst[i + 5 * 16], d5 );
			_mm_stream_si128( (__m128i *)&dst[i + 6 * 16], d6 );
			_mm_stream_si128( (__m128i *)&dst[i + 7 * 16], d7 );
		}
		for( ; i + 16 <= memSize; i += 16 ) {
			__m128i d = _mm_load_si128( (__m128i *)&src[i] );
			_mm_stream_si128( (__m128i *)&dst[i], d );
		}
		for( ; i + 4 <= memSize; i += 4 ) {
			*reinterpret_cast<u32*>(&dst[i]) = *reinterpret_cast<const u32*>(&src[i]);
		}
		for( ; i < memSize; i++ ) {
			dst[i] = src[i];
		}
		_mm_sfence( );
	}
}