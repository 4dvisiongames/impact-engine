/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Shared data
#include <Foundation/Shared.h>

namespace Memory
{
	class LowLevelMemory
	{
	public:
		/// Memory copy routines
		static void CopyMemoryBlock( _In_reads_bytes_(memSize) const void* __restrict src, 
									 _Out_writes_bytes_(memSize) void* __restrict dest, 
									 size_t memSize );

		static void CopyMemoryBlock16( _In_reads_bytes_(memSize) const void* __restrict src,
									   _Out_writes_bytes_(memSize) void* __restrict dest,
									   size_t memSize );

		/// Memory nullfiedinf rountine with custom size of erasing
		static void EraseMemoryBlock( _In_reads_bytes_(size) void* __restrict src, 
									  size_t size );
	};

	__IME_INLINED void LowLevelMemory::CopyMemoryBlock( _In_reads_bytes_(memSize) const void* __restrict src,
														_Out_writes_bytes_(memSize) void* __restrict dest,
														size_t memSize) {
		memcpy( dest, src, memSize );
	}

	__IME_INLINED void LowLevelMemory::EraseMemoryBlock( _In_reads_bytes_(size) void* __restrict src,
														 size_t size) {
		char *s1 = (char*)src;
		for(; 0<size; --size)
			*s1++ = 0;
	}
}; /// namespace Memory