/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef _Win32MapMemory_h_
#define _Win32MapMemory_h_

/// Engine shared code
#include <Foundation/Shared.h>

/// Memory namespace
namespace Memory
{
	/// TODO: 
	///  This would be better to move on NUMA page allocator, it's thread-safe.
	///  By making this we will stuck with Windows 7 and higher.

	void* MapMemory( u32 bytes )
	{
		/// [Codepoet]: On Windows platforms it's better to use VirtualAlloc
		return ::VirtualAllocEx( ::GetCurrentProcess(), NULL, bytes, (MEM_RESERVE | MEM_COMMIT | MEM_TOP_DOWN), PAGE_READWRITE );
	}

	int UnmapMemory( void* ptr, u32 bytes )
	{
		/// [Codepoet]: See, may be it's better to show how much do we release, but
		/// MSDN says that it's not good to point how much memory. Investigate it.
		BOOL result = ::VirtualFreeEx( ::GetCurrentProcess(), ptr, NULL, MEM_RELEASE );
		return !result;
	}
}

#endif /// _Win32MapMemory_h_