/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Foundation/Shared.h>

namespace Memory
{
	/// Helper function for Heap16 functions: aligns pointer to 16 byte and 
    /// writes padding mask to byte before returned pointer.
	__forceinline _Ret_notnull_ unsigned char* __HeapAlignPointerAndWritePadding16(_In_ unsigned char* ptr) {
		unsigned char paddingMask = uintptr_t(ptr) & 15;
		ptr = reinterpret_cast<unsigned char*>((uintptr_t(ptr + 16) & ~15));
		ptr[-1] = paddingMask;
		return ptr;
	};

	/// Helper function for Heap16 functions: "un-aligns" pointer through
    /// the padding mask stored in the byte before the pointer.
	__forceinline unsigned char* __HeapUnalignPointerFrom16Bytes(unsigned char* ptr)
	{
		return reinterpret_cast<unsigned char*>((uintptr_t(ptr - 16) | ptr[-1]));
	};

	/// HeapAlloc replacement which always returns 16-byte aligned addresses
	__forceinline
	_Ret_notnull_ _Post_writable_byte_size_(dwBytes)
	void* __HeapAllocAlign16Bytes(_In_ HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes) {
#if defined( XBOX360 )
		return ::HeapAlloc(hHeap, dwFlags, dwBytes);
#else
		/// Allocate new memory with 16 bytes offset
		unsigned char* ptr = static_cast<unsigned char*>(HeapAlloc(hHeap, dwFlags, dwBytes + 16));
		__IME_ASSERT_MSG(ptr, "Failed at HeapAlloc, cannot allocate memory from heap!");

		ptr = __HeapAlignPointerAndWritePadding16(ptr);
		return (void*) ptr;
#endif
	};

	/// HeapReAlloc replacement for 16-byte alignment, except Xbox 360.
	__forceinline
	_Ret_notnull_ _Post_writable_byte_size_(dwBytes)
	void* __HeapReAllocAlign16Bytes(_In_ HANDLE hHeap, DWORD dwFlags, _In_ void* lpMem, SIZE_T dwBytes) {
#if defined( XBOX360 )
		return HeapReAlloc(hHeap, dwFlags, lpMem, dwBytes);
#else
		/// Restore unaligned pointer
		unsigned char* ptr = static_cast<unsigned char*>(lpMem);
		unsigned char* rawPtr = __HeapUnalignPointerFrom16Bytes(ptr); 

		/// Rerform re-alloc, NOTE: if re-allocation can't happen in-place,
		/// we need to handle the allocation ourselves, in order not to destroy 
		/// the original data because of different alignment!!!
		ptr = static_cast<unsigned char*>(HeapReAlloc(hHeap, (dwFlags | HEAP_REALLOC_IN_PLACE_ONLY), rawPtr, dwBytes + 16));
		if (0 == ptr) {                   
			SIZE_T rawSize = ::HeapSize(hHeap, dwFlags, rawPtr);
			/// Assert if new allocation bigger than raw allcoation
			__IME_ASSERT(dwBytes + 16 >= rawSize);

			/// Re-allocate manually because padding may be different!
			ptr = static_cast<unsigned char*>(HeapAlloc(hHeap, dwFlags, dwBytes + 16));
			__IME_ASSERT_MSG(ptr, "Failed at HeapAlloc, cannot allocate memory from heap!");

			ptr = __HeapAlignPointerAndWritePadding16(ptr);
			::CopyMemory(ptr, lpMem, rawSize - 16);    
			/// Release old mem block
			::HeapFree(hHeap, dwFlags, rawPtr);
		} else {
			// Was re-allocated in place
			ptr = __HeapAlignPointerAndWritePadding16(ptr);
		}
		return reinterpret_cast<void*>(ptr);
#endif
	};

	/// HeapFree replacement which always returns 16-byte aligned addresses.
	__forceinline BOOL __HeapFreeAlign16Bytes(_In_ HANDLE hHeap, DWORD dwFlags, _In_ void* lpMem) {
#if defined( XBOX360 )
		return ::HeapFree(hHeap, dwFlags, lpMem);
#else
		unsigned char* ptr = static_cast<unsigned char*>(lpMem);
		ptr = __HeapUnalignPointerFrom16Bytes(ptr);
		return HeapFree(hHeap, dwFlags, ptr);
#endif
	};

	/// HeapSize replacement function.
	__forceinline SIZE_T __HeapSize16(_In_ HANDLE hHeap, DWORD dwFlags, _In_ void* lpMem) {
#if defined( XBOX360 )
		return ::HeapSize(hHeap, dwFlags, lpMem);
#else
		unsigned char* ptr = static_cast<unsigned char*>(lpMem);
		ptr = __HeapUnalignPointerFrom16Bytes(ptr);
		return HeapSize(hHeap, dwFlags, ptr);
#endif
	};

	_Ret_maybenull_ _Post_writable_byte_size_(size)
	void* Allocator::Malloc(const SizeT size, const SizeT alignment) {
		if (!Math::IsPowerOfTwo(alignment) || 0 == size) 
			return nullptr;

		return __HeapAllocAlign16Bytes(::GetProcessHeap(), 0, size);
	}

	_Ret_maybenull_ _Post_writable_byte_size_(size)
	void* Allocator::Realloc(_In_opt_ void *ptr, const SizeT size, const SizeT alignment) {
		if (!Math::IsPowerOfTwo(alignment) || 0 == size)
			return nullptr;

		return __HeapReAllocAlign16Bytes(::GetProcessHeap(), 0, ptr, size);
	}

	bool Allocator::Free(_In_opt_ void* _ptr) {
		if (!_ptr)
			return false;

		return !!__HeapFreeAlign16Bytes(::GetProcessHeap(), 0, _ptr);
	}

	size_t Allocator::Size(_In_opt_ void* _ptr) {
		if (!_ptr)
			return 0;

		return __HeapSize16( ::GetProcessHeap(), 0, _ptr );
	}
}

/*
	change log:

	17-Mar-2015		codepoet	file	Changed to explicit type conversion in all functions. 
*/