//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: Windows/Xbox360 version.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _Win32MemoryAllocator_h_
#define _Win32MemoryAllocator_h_

/// Memory allocation system
#include <NanoShared.h>
/// Critical Section
#include <NanoCriticalSection.h>
/// System
#include <NanoSystem.h>

/// Win32 memory allocation unit system
namespace _AlignedAllocationUnit
{
	extern HANDLE volatile mHeaps[Memory::Policy::NumPolicyTypes]; //!< Heap types array
	extern bool volatile bHeapsSetupped; //!< Check if all heap information has been setupped

	/// Setup heap information for engine
	__forceinline void SetupHeaps( void )
	{
		/// Do it, if heaps are not setupped
		if( !bHeapsSetupped )
		{
			const SizeT kilobyte = 1024;
			const SizeT megabyte = 1024 * 1024;

			unsigned int szHeaps;
			for( szHeaps = NULL ; szHeaps < Memory::Policy::NumPolicyTypes; szHeaps++ )
			{
				NDKAssert( NULL == mHeaps[szHeaps] );

				SizeT initialSize = NULL;
				SizeT maxSize = NULL;
				bool bUseLowFragHeap = false;

				switch( szHeaps )
				{
				case Memory::Policy::DefaultHeap:
				case Memory::Policy::ObjectHeap:
				case Memory::Policy::ObjectArrayHeap:
				case Memory::Policy::ScaleformHeap:
					initialSize = 4 * megabyte;
					bUseLowFragHeap = true;
					break;

				case Memory::Policy::ResourceHeap:
				case Memory::Policy::PhysicsHeap:
				case Memory::Policy::AppHeap:
				case Memory::Policy::NetworkHeap:
					initialSize = 8 * megabyte;
					break;

				case Memory::Policy::ScratchHeap:
					initialSize = 8 * megabyte;
					break;

				case Memory::Policy::StringDataHeap:
					initialSize = 2 * megabyte;
					bUseLowFragHeap = true;
					break;

				case Memory::Policy::StreamDataHeap:
					initialSize = 16 * megabyte;
					break;

				case Memory::Policy::TaskHeap:
					initialSize = 16 * megabyte;
					bUseLowFragHeap = true;
					break;

				case Memory::Policy::Xbox360GraphicsHeap:
				case Memory::Policy::Xbox360AudioHeap:
					// the special Xbox360 write combined heap, this is handled as a 
					// special case in Memory::Alloc()
					initialSize = 0;
					break;

				default:
					Core::System::Error( "Invalid heap type in _AlignedAllocationUnit::SetupHeaps() (NanoMemoryAllocator.cpp)!" );
					break;
				}

				/// If initial size of data is not NULL
				if ( NULL != initialSize )
				{
					/// Create new heap with proposed sizes
					mHeaps[szHeaps] = HeapCreate(0, initialSize, maxSize);
					/// Use low fragmentation heap as always
					if (bUseLowFragHeap)
					{
#if defined( WIN32 )
						/// Enable the Win32 LowFragmentationHeap
						ULONG heapFragValue = 2;
						/// Set heap information
						HeapSetInformation(mHeaps[szHeaps], HeapCompatibilityInformation, &heapFragValue, sizeof(heapFragValue));
#endif
					}
				}
				else
				{
						/// Otherwise heap is nullfied
						mHeaps[szHeaps] = NULL;
				}
			}
		}

		bHeapsSetupped = true; //!< Heaps are setupped, so use definitions
	}

	/// Delete heap information from the engine
	__forceinline void DeleteHeaps( void )
	{
		if( bHeapsSetupped )
		{
			unsigned int szHeaps;
			for( szHeaps = NULL ; szHeaps < Memory::Policy::NumPolicyTypes; szHeaps++ )
			{
				if( nullptr != mHeaps[szHeaps] )
				{
					if( 0 == HeapDestroy( mHeaps[szHeaps] ) )
					{
						Core::System::Error( "Failed to destroy created heap! (NanoMemoryAllocator.cpp)!" );
					}
				}
			}

			bHeapsSetupped = false;
		}
	}

	/// Helper function for Heap16 functions: aligns pointer to 16 byte and 
    /// writes padding mask to byte before returned pointer.
	__forceinline unsigned char* __HeapAlignPointerAndWritePadding16(unsigned char* ptr)
	{
		unsigned char paddingMask = DWORD(ptr) & 15;
		ptr = (unsigned char*)(DWORD(ptr + 16) & ~15);
		ptr[-1] = paddingMask;
		return ptr;
	};

	/// Helper function for Heap16 functions: "un-aligns" pointer through
    /// the padding mask stored in the byte before the pointer.
	__forceinline unsigned char* __HeapUnalignPointerFrom16Bytes(unsigned char* ptr)
	{
		return (unsigned char*)(DWORD(ptr - 16) | ptr[-1]);
	};

	/// HeapAlloc replacement which always returns 16-byte aligned addresses, except Xbox 360
	__forceinline void* __HeapAllocAlign16Bytes(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes)
	{
#if defined( XBOX360 )
		/// ZONER: Xbox 360 works with unaligned memory
		return ::HeapAlloc(hHeap, dwFlags, dwBytes);
#else
		/// Allocate new memory with 16 bytes offset
		unsigned char* ptr = (unsigned char*) ::HeapAlloc(hHeap, dwFlags, dwBytes + 16);
		ptr = __HeapAlignPointerAndWritePadding16(ptr);
		return (void*) ptr;
#endif
	};

	/// HeapReAlloc replacement for 16-byte alignment, except Xbox 360.
	__forceinline void* __HeapReAllocAlign16Bytes(HANDLE hHeap, DWORD dwFlags, void* lpMem, SIZE_T dwBytes)
	{
#if defined( XBOX360 )
		/// ZONER: Xbox 360 works with unaligned memory
		return HeapReAlloc(hHeap, dwFlags, lpMem, dwBytes);
#else
		/// Restore unaligned pointer
		unsigned char* ptr = (unsigned char*)lpMem;
		unsigned char* rawPtr = __HeapUnalignPointerFrom16Bytes(ptr); 

		/// Rerform re-alloc, NOTE: if re-allocation can't happen in-place,
		/// we need to handle the allocation ourselves, in order not to destroy 
		/// the original data because of different alignment!!!
		ptr = (unsigned char*) ::HeapReAlloc(hHeap, (dwFlags | HEAP_REALLOC_IN_PLACE_ONLY), rawPtr, dwBytes + 16);
		if (0 == ptr)
		{                   
			DWORD rawSize = ::HeapSize(hHeap, dwFlags, rawPtr);
			/// Assert if new allocation bigger than raw allcoation
			NDKAssert(dwBytes + 16 >= rawSize);
			/// Re-allocate manually because padding may be different!
			ptr = (unsigned char*) ::HeapAlloc(hHeap, dwFlags, dwBytes + 16);
			ptr = __HeapAlignPointerAndWritePadding16(ptr);
			::CopyMemory(ptr, lpMem, rawSize - 16);    
			/// Release old mem block
			::HeapFree(hHeap, dwFlags, rawPtr);
		}
		else
		{
			// Was re-allocated in place
			ptr = __HeapAlignPointerAndWritePadding16(ptr);
		}
		return (void*) ptr;
#endif
	};

	/// HeapFree replacement which always returns 16-byte aligned addresses.
	__forceinline BOOL __HeapFreeAlign16Bytes(HANDLE hHeap, DWORD dwFlags, void* lpMem)
	{
#if defined( XBOX360 )
		/// ZONER: Xbox 360 works with unaligned memory
		return ::HeapFree(hHeap, dwFlags, lpMem);
#else
		unsigned char* ptr = (unsigned char*) lpMem;
		ptr = __HeapUnalignPointerFrom16Bytes(ptr);
		return ::HeapFree(hHeap, dwFlags, ptr);
#endif
	};

	/// HeapSize replacement function.
	__forceinline SIZE_T __HeapSize16(HANDLE hHeap, DWORD dwFlags, void* lpMem)
	{
#if defined( XBOX360 )
		/// ZONER: Xbox 360 works with unaligned memory
		return ::HeapSize(hHeap, dwFlags, lpMem);
#else
		unsigned char* ptr = (unsigned char*) lpMem;
		ptr = __HeapUnalignPointerFrom16Bytes(ptr);
		return ::HeapSize(hHeap, dwFlags, ptr);
#endif
	};
};

#endif /// _Win32MemoryAllocator_h_