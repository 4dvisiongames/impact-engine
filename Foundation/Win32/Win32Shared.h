/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__Win32__Win32Shared_h__
#define __Foundation__Win32__Win32Shared_h__

///
/// Windows platform code
///
#ifndef WIN32_LEAN_AND_MEAN
#	define WIN32_LEAN_AND_MEAN
#endif

#ifndef NOMINMAX
#define NOMINMAX
#endif

#ifndef _CRT_SECURE_NO_WARNINGS
#	define _CRT_SECURE_NO_WARNINGS
#endif

#include <Windows.h>

#ifdef DeleteFile
#	undef DeleteFile
#endif

#ifdef CopyFile
#	undef CopyFile
#endif

#ifdef GetObject
#	undef GetObject
#endif


//
// Typedefs
//
#include <stdint.h>
#if _MSC_VER

typedef __int8 s8;
typedef unsigned __int8 u8;
typedef __int16 s16;
typedef unsigned __int16 u16;
typedef __int32 s32;
typedef unsigned __int32 u32;
typedef __int64 s64;
typedef unsigned __int64 u64;

#else

typedef int8_t s8;
typedef uint8_t u8;
typedef int16_t s16;
typedef uint16_t u16;
typedef int32_t s32;
typedef uint32_t u32;
typedef int64_t s64;
typedef uint64_t u64;

#endif


///
/// Code analysis
///
#include <sal.h>
#include <CodeAnalysis/sourceannotations.h>
#define __IME_VERIFY_FORMAT_STRING [SA_FormatString(Style="printf")]


///
/// Debugging
///
#if defined(DEBUG) || defined(_DEBUG) || defined(__DEBUG)
#	include <crtdbg.h>
#endif

#if __IME_USE_ASSERT
#	define __IME_ASSERT(x) (void)((!!(x)) || (1 != _CrtDbgReport( _CRT_ASSERT, __FILE__, __LINE__, NULL, #x )) || (__debugbreak(), 0))
#	define __IME_ASSERT_MSG(x, m) (void) ((!!(x)) || (1 != _CrtDbgReport( _CRT_ASSERT, __FILE__, __LINE__, NULL, m )) || (__debugbreak(), 0))

#else
#	define __IME_ASSERT(expr) ((void)NULL)
#	define __IME_ASSERT_MSG(expr, msg) ((void)NULL)

#endif


/// 
/// C/C++ Runtime and other headers
///
#include <stdlib.h>
#include <process.h>
#include <intrin.h>


///
/// Engine-related code
///
#define _ENABLE_THREADLOCAL_STRINGATOM_TABLES (1)

#define __IME_SLEEPING_SWITCH_TO_THREAD ::SwitchToThread()

#define __IME_SLEEPING_PAUSE _mm_pause()


/// WARNING: these functions/classes/structs/etc. are Win32-dependent, don't use them in any shareble code between
/// different platforms, used them in platform-specific implementations.

namespace Win32
{
	template< typename Type >
	__IME_INLINED void COMObjectSafeRelease( Type*& object )
	{
		if ( object )
		{
			object->Release();
			object = NULL;
		}
	}

	template< typename Type >
	union COMObjectAndRawPtr
	{
		Type* myComObject;
		void* myRawPtr;

		operator Type*() { return this->myComObject; }
		operator void*() { return this->myRawPtr; }
	};
}

#endif /// __Foundation__Win32__Win32Shared_h__