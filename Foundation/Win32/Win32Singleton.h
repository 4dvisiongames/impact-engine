/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// __IME_DECLARE_SINGLETON - declare singleton for class.
#define __IME_DECLARE_SINGLETON( type ) \
	public: \
	static type * Singleton; \
	static type * Instance( void ) { __IME_ASSERT( Singleton != nullptr ); return Singleton; }; \
	static bool HasInstance( void ) { return Singleton != nullptr; }; \
	private:

#define __IME_DECLARE_THREAD_LOCAL_SINGLETON( type ) \
	public: \
	__declspec( thread ) static type * Singleton; \
	static type * Instance( void ) { __IME_ASSERT( Singleton != nullptr ); return Singleton; }; \
	static bool HasInstance( void ) { return Singleton != nullptr; }; \
	private:

/// __IME_IMPLEMENT_SINGLETON - define default class singleton as nullptr(note that you must use 
/// this implementation routine with __IME_DECLARE_SINGLETON).
#define __IME_IMPLEMENT_SINGLETON( type ) \
	type * type::Singleton = nullptr;

#define __IME_IMPLEMENT_THREAD_LOCAL_SINGLETON( type ) \
	__declspec( thread ) type* type::Singleton = nullptr;

/// NDKConstuctSingleton - constructs singleton.
#define __IME_CONSTRUCT_SINGLETON \
	__IME_ASSERT( Singleton == nullptr ); Singleton = this;

///	__IME_DESTRUCT_SINGLETON - destructs singleton.
#define __IME_DESTRUCT_SINGLETON \
	if( Singleton ) { __IME_ASSERT( Singleton ); Singleton = nullptr; }
