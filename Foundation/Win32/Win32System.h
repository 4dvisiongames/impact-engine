/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__Win32System_h__
#define __Foundation__Win32System_h__

/// Engine shared code
#include <Foundation/Shared.h>
#include <Foundation/SleepingPolicy.h>

/// Forward declaration
namespace STL
{
	class LocalStringAtomTable;
	class GlobalStringAtomTable;
}

namespace Core
{
	/// Forward declaration
	class Hardware;

	/// <summary>
	/// <c>System</c> is a layer for engine to work with OS routines.
	/// </summary>
	class System
	{
	public:
		/// <summary cref="System::Setup">
		/// Setup everything that we need for smooth engine startup.
		/// </summary>
		static void Setup( bool console = false );

		/// <summary cref="System::Exit">
		/// Exit process with some exit code.
		/// </summary>
		/// <param name="exitCode">Exit code as C standard requires.</param>
		static void Exit( int exitCode );

		/// <summary cref="System::Error">
		/// Show error message box on display.
		/// </summary>
		/// <param name="error">Error message.</param>
		static void Error( _In_ const s8* error );

		/// <summary cref="System::DebugOutput">
		/// Print to the debug output, e.g for in-engine console.
		/// </summary>
		/// <note>
		/// Engine console must be initialized. Beforce outputting
		/// any text, engine checks for console instance, but do not be surprized
		/// when you can't find what you sent to console port.
		/// </note>
		/// <param name="message">A message for console output.</param>
		static void DebugOutput( _In_ const s8* message );

		/// <summary cref="System::GetMilliseconds">
		/// </summary>
		static uintptr_t GetMilliseconds( void );

		/// <summary cref="System::Sleep">
		/// Sleep thread for some time without passing any parameter
		/// to specify sleeping. Fully Windows version using SwithToThread()
		/// </summary>
		/// <remarks>
		/// Warning: This function only works on ONE processing unit threads
		/// cannot be replanned on other CPUs, only on current CPU.
		/// </remarks>
		template< typename Exec >
		static void Sleep( u32 delay = 0 );

		/// <summary cref="System::GetConsoleMode">
		/// Get current console mode of the engine.
		/// </summary>
		static bool GetConsoleMode( void );

		/// <summary cref="System::GetHardwareInformation">
		/// Get hardware information.
		/// </summary>
		/// <returns>Instance to the Win32Hardware object.</returns>
		static Hardware* GetHardwareInformation( void );

		/// <summary cref="System::GetGlobalStringTable">
		/// Get global string table instance.
		/// </summary>
		static STL::GlobalStringAtomTable* GetGlobalStringTable( void );

#ifdef _ENABLE_THREADLOCAL_STRINGATOM_TABLES
		/// <summary cref="System::GetLocalStringTable">
		/// Get local string table instance.
		/// </summary>
		static STL::LocalStringAtomTable* GetLocalStringTable( void );
#endif

	private:
		static bool volatile SetupCalled; //!< Engine setup already called
		static bool bIsConsoleMode; //!< Are we using DOS-like console?
		static Hardware hardware_instance; //!< Instance for hardware information

		/// STL system pointers
		static STL::GlobalStringAtomTable* _globalStringTable; //!< Global string table
#ifdef _ENABLE_THREADLOCAL_STRINGATOM_TABLES
		static STL::LocalStringAtomTable* _localStringTable; //!< Local string table
#endif
	};

	template< typename Exec >
	__IME_INLINED void System::Sleep( u32 delay )
	{
		/// Replan all threads to switch all low-priority to
		/// higher priority and try them to execute data that must
		/// be computed
		SleepingPolicy< Exec > execPolicy;
		execPolicy(delay);
	}

	__IME_INLINED bool System::GetConsoleMode( void )
	{
		return bIsConsoleMode;
	}
}

#endif /// __Foundation__Win32System_h__