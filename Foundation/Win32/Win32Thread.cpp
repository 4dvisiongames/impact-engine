/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Thread ID
#include <Foundation/ThreadID.h>
/// Thread implementation
#include <Foundation/Win32/Win32Thread.h>
/// Atomics
#include <Foundation/AtomicDoOnce.h>
/// Local string table
#include <Foundation/LocalStringAtomTable.h>

/// COM Object Base
#include <ObjBase.h>

const SIZE_T JOB_THREAD_STACK_SIZE = 1024 * 1024;
static const WORD THREAD_ALL_PROCESSOR_GROUPS = 0xffff;

const DWORD MS_VC_EXCEPTION=0x406D1388;
typedef struct tagTHREADNAME_INFO
{
	DWORD dwType; // Must be 0x1000.
	LPCSTR szName; // Pointer to name (in user addr space).
	DWORD dwThreadID; // Thread ID (-1=caller thread).
	DWORD dwFlags; // Reserved for future use, must be zero.
} THREADNAME_INFO;

// Statically allocate an array for processor group information.
// Windows 7 supports maximum 4 groups, but let's look ahead a little.
static const WORD MaxProcessorGroups = 64;

struct ProcessorGroupInfo
{
	DWORD_PTR   mask;                   ///< Affinity mask covering the whole group
	int         numProcs;               ///< Number of processors in the group
	int         numProcsRunningTotal;   ///< Subtotal of processors in this and preceding groups

	//! Total number of processor groups in the system
	static int NumGroups; 

	//! Index of the group with a slot reserved for the first master thread
	/** In the context of multiple processor groups support current implementation
		defines "the first master thread" as the first thread to invoke
		AvailableHwConcurrency(). 

		TODO:   Implement a dynamic scheme remapping workers depending on the pending
				master threads affinity. **/
	static int HoleIndex;
};

int ProcessorGroupInfo::NumGroups = 1;
int ProcessorGroupInfo::HoleIndex = 0;
ProcessorGroupInfo theProcessorGroups[MaxProcessorGroups];

//! Indicator of dynamic linking library
volatile Threading::DoOnceState::List libraryInit;
//! Initialized internal threading functions
static void Internal_InitializeThreading( void ) 
{
	SYSTEM_INFO si;
	GetNativeSystemInfo( &si );
	DWORD_PTR pam, sam, m = 1;
	GetProcessAffinityMask( ::GetCurrentProcess(), &pam, &sam );
	int nproc = 0;
	for( size_t idx = 0; idx < sizeof( DWORD_PTR ) * CHAR_BIT; idx++, m <<= 1 ) {
		if( pam & m ) {
			++nproc;
		}
	}

	__IME_ASSERT( nproc <= (int)si.dwNumberOfProcessors );
	// By default setting up a number of processors for one processor group
	theProcessorGroups[0].numProcs = theProcessorGroups[0].numProcsRunningTotal = nproc;
	// Setting up processor groups in case the process does not restrict affinity mask and more than one processor group is present
	if ( nproc == (int)si.dwNumberOfProcessors ) {
		// The process does not have restricting affinity mask and multiple processor groups are possible
		ProcessorGroupInfo::NumGroups = (int)GetActiveProcessorGroupCount();
		__IME_ASSERT( ProcessorGroupInfo::NumGroups <= MaxProcessorGroups );
		// Fail safety bootstrap. Release versions will limit available concurrency
		// level, while debug ones would assert.
		if ( ProcessorGroupInfo::NumGroups > MaxProcessorGroups ) {
			ProcessorGroupInfo::NumGroups = MaxProcessorGroups;
		}

		if ( ProcessorGroupInfo::NumGroups > 1 ) {
			GROUP_AFFINITY ga;
			if ( GetThreadGroupAffinity( GetCurrentThread(), &ga ) ) {
				ProcessorGroupInfo::HoleIndex = ga.Group;
			}

			int nprocs = 0;
			for ( WORD i = 0; i < ProcessorGroupInfo::NumGroups; ++i ) {
				ProcessorGroupInfo  &pgi = theProcessorGroups[i];
				pgi.numProcs = (int)GetActiveProcessorCount(i);
				__IME_ASSERT( pgi.numProcs <= (int)sizeof(DWORD_PTR) * CHAR_BIT );
				pgi.mask = pgi.numProcs == sizeof(DWORD_PTR) * CHAR_BIT ? ~(DWORD_PTR)0 : (DWORD_PTR(1) << pgi.numProcs) - 1;
				pgi.numProcsRunningTotal = nprocs += pgi.numProcs;
			}
			__IME_ASSERT( nprocs == (int)GetActiveProcessorCount( THREAD_ALL_PROCESSOR_GROUPS ) );
		}
	}
}

static int NumberOfProcessorGroups() {
	return ProcessorGroupInfo::NumGroups;
}

// Offset for the slot reserved for the first master thread
#define HoleAdjusted(procIdx, grpIdx) (procIdx + (holeIdx <= grpIdx))

static int FindProcessorGroupIndex ( int procIdx ) {
	// In case of oversubscription spread extra workers in a round robin manner
	int holeIdx;
	const int numProcs = theProcessorGroups[ProcessorGroupInfo::NumGroups - 1].numProcsRunningTotal;
	if ( procIdx >= numProcs - 1 ) {
		holeIdx = INT_MAX;
		procIdx = (procIdx - numProcs + 1) % numProcs;
	} else {
 		holeIdx = ProcessorGroupInfo::HoleIndex;
	}

	// Approximate the likely group index assuming all groups are of the same size
	int i = procIdx / theProcessorGroups[0].numProcs;
	// Make sure the approximation is a valid group index
	if (i >= ProcessorGroupInfo::NumGroups) i = ProcessorGroupInfo::NumGroups - 1;
	// Now adjust the approximation up or down
	if ( theProcessorGroups[i].numProcsRunningTotal > HoleAdjusted(procIdx, i) ) {
		while ( theProcessorGroups[i].numProcsRunningTotal - theProcessorGroups[i].numProcs > HoleAdjusted(procIdx, i) ) {
			__IME_ASSERT( i > 0 );
			--i;
		}
	} else {
		do {
			++i;
		} while ( theProcessorGroups[i].numProcsRunningTotal < HoleAdjusted(procIdx, i) );
	}
	__IME_ASSERT( i < ProcessorGroupInfo::NumGroups );
	return i;
}

static void MoveThreadIntoProcessorGroup( void * myThread, int groupIndex ) 
{
	GROUP_AFFINITY ga = { theProcessorGroups[groupIndex].mask, (WORD)groupIndex, {0,0,0} };
	SetThreadGroupAffinity( myThread, &ga, NULL );
}

static void __Win32SetThreadName( const Char8* name )
{
	THREADNAME_INFO info;
	info.dwType = 0x1000;
	info.szName = name;
	info.dwThreadID = Threading::Thread::GetLocalThreadId();
	info.dwFlags = 0;

	__try {
		RaiseException( MS_VC_EXCEPTION, 0, sizeof(info) / sizeof(ULONG_PTR), reinterpret_cast< const ULONG_PTR* >(&info) );
	} __except( _exception_code() == MS_VC_EXCEPTION ? EXCEPTION_EXECUTE_HANDLER : EXCEPTION_CONTINUE_SEARCH ) {
		// this much is just to keep /analyze quiet
		info.dwFlags = 0;
	}
}

namespace Threading
{
	//! Sees current count of threads used for parallelism
	static volatile int currentNumberOfThreads = 1;

	DWORD WINAPI Thread::MyWrapperFunction( LPVOID arg )
	{
		__IME_ASSERT( nullptr != arg );
		IntrusivePtr< Thread > thread = static_cast< Thread* >( arg );

		// Deal with 64K aliasing.  The formula for "offset" is a Fibonacci hash function,
		// which has the desirable feature of spreading out the offsets fairly evenly
		// without knowing the total number of offsets,ent la and furthermore unlikely to
		// accidentally cancel out other 64K aliasing schemes that Microsoft might implement later.
		// See Knuth Vol 3. "Theorem S" for details on Fibonacci hashing.
		// The second statement is really does need "volatile", otherwise the compiler might remove the _alloca.
		const size_t offset = ( thread->myThreadIndex + 1 ) * 40503U % ( 1U << 16 );
		void * volatile dummyValueForAlloca = _alloca( offset );
		UNREFERENCED_PARAMETER( dummyValueForAlloca );

		__Win32SetThreadName( thread->myThreadName.AsCharPtr() );

		STL::LocalStringAtomTable localStringAtomic;
		try {
			/// Every thread neeeds it's own instance of COM Object, so initialize here
			if( FAILED( CoInitializeEx( NULL, COINIT_MULTITHREADED ) ) )
				Debug::Messaging::Send( Debug::EMessageType::FatalError, STL::String< Char8 >::VA( "(Thread: %i) Failed to initialize Component Object Model instance for thread!", GetCurrentThreadId() ) );

			thread->Executor();
		} catch( ... ) {
			/// Ternimate thread because failed to startup propetly or got some weird exception that refuses to 
			/// execute this thread function. According to C++11 standard.

			/// TODO: CppCheck says that we don't need this because of not proper thread clean up.
			TerminateThread( ::GetCurrentThread(), 0 );
		}

		CoUninitialize();
		Debug::Messaging::Send( Debug::EMessageType::Normal, STL::String< Char8 >::VA( "Exiting Task Worker thread... My thread id is: %i", GetCurrentThreadId() ) );

		return 0;
	}

	Thread::Thread( void ) : myThread( NULL ), myThreadId( 0 ), myThreadIndex( 0 )
	{
		/// Empty
	}

	Thread::~Thread( void )
	{
		/// Destroy thread
		this->Destroy();
	}

	void Thread::Setup( size_t stackSize, bool manualResume, ThreadPriorities prio, const s8* name )
	{
		/// Debug assertion: thread must not be created
		__IME_ASSERT( !this->myThread ); 
		DWORD flags = 0;

		//! Set thread name
		if( name )
			this->myThreadName = name;
		else
			this->myThreadName = "WorkerThread";

		// Initialize threading functions and system information
		if (Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( libraryInit ) != Threading::DoOnceState::InitializationComplete)
			Threading::AtomicDoOnce( Internal_InitializeThreading, libraryInit );

		int numProcessorGroups = NumberOfProcessorGroups();

		// Without this flag the 'dwStackSize' parameter to CreateThread specifies the "Stack Commit Size"
		// and the "Stack Reserve Size" is set to the value specified at link-time.
		// With this flag the 'dwStackSize' parameter to CreateThread specifies the "Stack Reserve Size"
		// and the �Stack Commit Size� is set to the value specified at link-time.
		// For various reasons (some of which historic) we reserve a large amount of stack space in the
		// project settings. By setting this flag and by specifying 64 kB for the "Stack Commit Size" in
		// the project settings we can create new threads with a much smaller reserved (and committed)
		// stack space. It is very important that the "Stack Commit Size" is set to a small value in
		// the project settings. If it is set to a large value we may be both reserving and committing
		// a lot of memory by setting the STACK_SIZE_PARAM_IS_A_RESERVATION flag. There are some
		// 50 threads allocated for normal game play. If, for instance, the commit size is set to 16 MB
		// then by adding this flag we would be reserving and committing 50 x 16 = 800 MB of memory.
		// On the other hand, if this flag is not set and the "Stack Reserve Size" is set to 16 MB in the
		// project settings, then we would still be reserving 50 x 16 = 800 MB of virtual address space.
		flags |= STACK_SIZE_PARAM_IS_A_RESERVATION;
		flags |= CREATE_SUSPENDED;

		// Job stack size
		stackSize = stackSize ? stackSize :	JOB_THREAD_STACK_SIZE;

		DWORD localIndex = 0;
		/// Create thread and pass thread arguments to function
		//this->myThread = reinterpret_cast< HANDLE >( _beginthreadex( NULL, stackSize, this->MyWrapperFunction, this, flags, &localIndex ) );
		this->myThread = ::CreateThread( NULL, stackSize, this->MyWrapperFunction, this, flags, &localIndex );
		this->myThreadId = localIndex;

		if( !this->myThread )
			NDKThrowException( Debug::EExceptionCodes::InternalError, "Failed to create thread!", "Win32Thread::Setup()" );

		switch( prio ) 
		{
		case ThreadPrioritiesEnum::Low:
			SetThreadPriority( this->myThread, THREAD_PRIORITY_LOWEST );
			break;

		case ThreadPrioritiesEnum::Medium:
			SetThreadPriority( this->myThread, THREAD_PRIORITY_NORMAL );
			break;

		case ThreadPrioritiesEnum::High:
			SetThreadPriority( this->myThread, THREAD_PRIORITY_HIGHEST );
			break;
		};

		this->myThreadIndex = Threading::Interlocked::FetchAdd< Threading::MemoryOrderEnum::Sequental >( currentNumberOfThreads, 1 );

		if( numProcessorGroups > 1 )
			MoveThreadIntoProcessorGroup( this->myThread, FindProcessorGroupIndex( this->myThreadIndex ) );

		if( !manualResume )
			ResumeThread( this->myThread );
	}

	void Thread::Destroy( void )
	{
		/// If this is a THREAD, destroy it
		CloseHandle( this->myThread );
	}

	void Thread::Resume( void )
	{
		ResumeThread( this->myThread );
	}

	void Thread::Join( void )
	{
		WaitForSingleObject( this->myThread, INFINITE );
	}

	bool Thread::IsJoinable( void ) const
	{
		if( this->myThread ) {
			DWORD exitCode = 0;
			if( GetExitCodeThread( this->myThread, &exitCode ) ) {
				if( STILL_ACTIVE == exitCode ) {
					return false;
				}
			}
		}
		return true;
	}

	bool Thread::IsPaused( void ) const
	{
		DWORD retCode = WaitForSingleObject( this->myThread, NULL );
		return retCode != 0;
	}

	SizeT Thread::GetAvailableConcurrency( void )
	{
		if( Threading::Interlocked::Fetch< Threading::MemoryOrderEnum::Acquire >( libraryInit ) != Threading::DoOnceState::InitializationComplete ) {
			Threading::AtomicDoOnce( Internal_InitializeThreading, libraryInit );
		}
		return theProcessorGroups[ProcessorGroupInfo::NumGroups - 1].numProcsRunningTotal;
	}

	u32 Thread::GetLocalThreadId( void )
	{
		u32 result = static_cast< u32 >( ::GetCurrentThreadId() );
		return result;
	}
}

/*
	change log:

	15-Mar-2015		codepoet	function FindProcessorGroupIndex	fixed possible read overrun in while()
	15-Mar-2015		codepoet	method Thread::Setup				fix of unitialized memory warning, now DWORD flags = 0
*/