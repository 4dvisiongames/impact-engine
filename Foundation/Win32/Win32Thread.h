/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__Win32Thread_h__
#define __Foundation__Win32Thread_h__

/// Reference count
#include <Foundation/RefCount.h>
/// Thread pirorities
#include <Foundation/ThreadPriority.h>
/// Event
#include <Foundation/Event.h>

//! Threading namespace
namespace Threading
{
	//! Thread implementation for Win32 platform
	class Thread : public Core::ReferenceCount
	{
		//! Win32 platform thread handle
		HANDLE myThread;
		//! Thread index, filled up by _beginthreadex
		size_t myThreadId;
		//! Thread index, filled up manually, uses always internally
		s32 myThreadIndex;
		//! Current names of my thread
		STL::String< Char8 > myThreadName;

		//! This is just wrapper around to work best with _beginthreadex function. 
		static DWORD WINAPI MyWrapperFunction( LPVOID arg );

	protected:
		//! Execution function used in thread function.
		virtual void Executor( void ) { /* Override in subclass */ }

	public:
		Thread( void );
		virtual ~Thread( void );

		//! Construct a thread object with a new thread of execution.
		void Setup( size_t stackSize = 0, bool manualResume = false, ThreadPriorities prio = ThreadPrioritiesEnum::Medium, const s8* name = NULL );
		//! Exit from current thread.
		void Destroy( void );
		//! Resume thread
		void Resume( void );
		//! Wait for the thread to finish (join execution flows).
		void Join( void );
		//! Check if the thread is joinable.
		//! A thread object is joinable if it has an associated thread of execution.
		bool IsJoinable( void ) const;
		//! Is current thread worker paused
		bool IsPaused( void ) const;
		//! Return the thread ID of a thread object.
		Threading::ThreadID GetID( void ) const;
		//! Return thread index from it's array of threads
		SizeT GetCurrentThreadIndex( void ) const;
		//! Get name of currently created thread
		const STL::String< Char8 >& GetName( void ) const;
		//! Return the number of logical processors in the system.
		static SizeT GetAvailableConcurrency( void );
		//! Get current thread identification
		static u32 GetLocalThreadId( void );
	};

	__IME_INLINED const STL::String< Char8 >& Thread::GetName( void ) const
	{
		return this->myThreadName;
	}

	__IME_INLINED SizeT Thread::GetCurrentThreadIndex( void ) const
	{
		return this->myThreadIndex;
	}
}

#endif /// __Foundation__Win32Thread_h__