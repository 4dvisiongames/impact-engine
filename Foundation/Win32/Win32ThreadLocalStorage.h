/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Foundation__Win32ThreadLocalStorage_h__
#define __Foundation__Win32ThreadLocalStorage_h__

/// Win32 threading namespace
namespace Threading
{
	/// Internal methods and classes
	namespace Internal
	{
		/// <summary>
		/// <c>TLSBase</c> is a Win32 implementatio of thread-local storage.
		/// </summary>
		template< typename T > class TLSBase
		{
			DWORD tlsData; //!< Thread-local data

		public:
			/// <summary cref="TLSBase::CreateStorage">
			/// Create new Thread-Local storage in memory.
			/// </summary>
			/// <returns>Zero if all is OK, otherwise is bad.</returns>
			int CreateStorage( void );

			/// <summary cref="TLSBase::ReleaseStorage">
			/// Release and delete storage.
			/// </summary>
			void ReleaseStorage( void );

			/// <summary cref="TLSBase::Set">
			/// Set thread-local data.
			/// </summary>
			/// <param name=data">Data to load to storage.</param>
			void Set( T data );

			/// <summary cref="TLSBase::Get">
			/// Get thread-local data.
			/// </summary>
			/// <returns>TLS data.</returns>
			T Get( void );
		};

		template< typename T > 
		__IME_INLINED int TLSBase< T >::CreateStorage( void )
		{
			/// Temporary TLS data
			DWORD temp = TlsAlloc();
			/// Check if TLS data is out of indexes
			if( temp == TLS_OUT_OF_INDEXES )
				return TLS_OUT_OF_INDEXES;
			/// Make temporary data as current
			this->tlsData = temp;
			/// We success fully created storage, so return 0
			return NULL;
		}

		template< typename T > 
		__IME_INLINED void TLSBase< T >::ReleaseStorage( void )
		{
			/// Free our current storage
			TlsFree( this->tlsData );
			/// Make all nullfied
			this->tlsData = NULL;
		}

		template< typename T > 
		__IME_INLINED void TLSBase< T >::Set( T data )
		{
			/// Setup out thread-local storage
			TlsSetValue( this->tlsData, (LPVOID)data );
		}

		template< typename T > 
		__IME_INLINED T TLSBase< T >::Get( void )
		{
			/// Get data from our storage
			return (T)TlsGetValue(this->tlsData);
		}
	}

	/// <summary>
	/// <c>ThreadLocalStorage</c> is a Win32 implementatio of thread-local storage.
	/// </summary>
	template< typename T > class ThreadLocalStorage : protected Internal::TLSBase< T >
	{
	public:
		ThreadLocalStorage( void );
		~ThreadLocalStorage( void );

		ThreadLocalStorage< T >& operator = ( T value );

		operator T( void );
	};

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T >::ThreadLocalStorage( void )
	{
		/// Create new storage
		this->CreateStorage();
	}

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T >::~ThreadLocalStorage( void )
	{
		/// Release storage
		this->ReleaseStorage();
	}

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T >& ThreadLocalStorage< T >::operator = ( T value )
	{
		/// Create new storage
		this->Set(value);
		return *this;
	}

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T >::operator T( void )
	{
		/// Get thread-local variable
		return this->Get();
	}

	/// <summary>
	/// <c>ThreadLocalStorage</c> is a Win32 implementation of thread-local storage pointer.
	/// </summary>
	template< typename T > class ThreadLocalStorage< T* > : protected Internal::TLSBase< T* >
	{
	public:
		ThreadLocalStorage( void );
		ThreadLocalStorage( T* ptr );
		~ThreadLocalStorage( void );

		ThreadLocalStorage< T* >& operator = ( T* value );
		template< typename U > ThreadLocalStorage< T* >& operator = ( U* value );

		bool operator == ( T* ptr );
		template< typename U > bool operator == ( U* ptr );
		bool operator != ( T* ptr );
		template< typename U > bool operator != ( U* ptr );

		operator T*( void );
		T* operator->();
		T& operator*(); 
	};

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T* >::ThreadLocalStorage( void )
	{
		this->CreateStorage();
		this->Set( nullptr );
	}

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T* >::ThreadLocalStorage( T* ptr )
	{
		this->CreateStorage();
		this->Set( ptr );
	}

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T* >::~ThreadLocalStorage( void )
	{
		this->ReleaseStorage();
	}

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T* >& ThreadLocalStorage< T* >::operator = ( T* value )
	{
		this->Set( value );
		return *this;
	}

	template< typename T > 
	template< typename U >
	__IME_INLINED ThreadLocalStorage< T* >& ThreadLocalStorage< T* >::operator = ( U* value )
	{
		this->Set( reinterpret_cast< T* >( value ) );
		return *this;
	}

	template< typename T > 
	__IME_INLINED bool ThreadLocalStorage< T* >::operator == ( T* ptr )
	{
		return this->Get() == ptr;
	}

	template< typename T > 
	template< typename U > 
	__IME_INLINED bool ThreadLocalStorage< T* >::operator == ( U* ptr )
	{
		return this->Get() == ptr;
	}

	template< typename T > 
	__IME_INLINED bool ThreadLocalStorage< T* >::operator != ( T* ptr )
	{
		return this->Get() != ptr;
	}

	template< typename T > 
	template< typename U > 
	__IME_INLINED bool ThreadLocalStorage< T* >::operator != ( U* ptr )
	{
		return this->Get() != ptr;
	}

	template< typename T > 
	__IME_INLINED ThreadLocalStorage< T* >::operator T *( void )
	{
		T* ptr = this->Get();
		return ptr;
	}

	template< typename T > 
	__IME_INLINED T* ThreadLocalStorage< T* >::operator -> ( void )
	{
		T* ptr = this->Get();
		return ptr;
	}

	template< typename T > 
	__IME_INLINED T& ThreadLocalStorage< T* >::operator *( void )
	{
		T* ptr = this->Get();
		return *ptr;
	}
}

#endif /// __Foundation__Win32ThradLocalStorage_h__