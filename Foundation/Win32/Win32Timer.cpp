/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Engine shared code
#include <Foundation/Shared.h>
/// Win32 timer definitions
#include <Foundation/Win32/Win32Timer.h>

/// Win32 core namespace
namespace Win32Core
{
	Win32Timer::Win32Timer() 
		: m_bTimerStopped( true )
		, m_llQPFTicksPerSec( NULL )
		, m_llStopTime( NULL )
		, m_llLastElapsedTime( NULL )
		, m_llBaseTime( NULL ) {
		/// Use QueryPerformanceFrequency to get the frequency of the counter
		LARGE_INTEGER qwTicksPerSec = { 0 };
		QueryPerformanceFrequency( &qwTicksPerSec );
		m_llQPFTicksPerSec = qwTicksPerSec.QuadPart;
	}

	void Win32Timer::Reset() {
		LARGE_INTEGER qwTime = GetAdjustedCurrentTime();

		m_llBaseTime = qwTime.QuadPart;
		m_llLastElapsedTime = qwTime.QuadPart;
		m_llStopTime = 0;
		m_bTimerStopped = FALSE;
	}

	void Win32Timer::Start() {
		// Get the current time
		LARGE_INTEGER qwTime = { 0 };
		QueryPerformanceCounter( &qwTime );

		if (m_bTimerStopped) {
			m_llBaseTime += qwTime.QuadPart - m_llStopTime;
		}

		m_llStopTime = 0;
		m_llLastElapsedTime = qwTime.QuadPart;
		m_bTimerStopped = FALSE;
	}

	void Win32Timer::Stop() {
		if( !m_bTimerStopped ) {
			LARGE_INTEGER qwTime = { 0 };
			QueryPerformanceCounter( &qwTime );
			m_llStopTime = qwTime.QuadPart;
			m_llLastElapsedTime = qwTime.QuadPart;
			m_bTimerStopped = TRUE;
		}
	}

	void Win32Timer::Advance() {
		m_llStopTime += m_llQPFTicksPerSec / 10;
	}

	double Win32Timer::GetAbsoluteTime() {
		LARGE_INTEGER qwTime = { 0 };
		QueryPerformanceCounter( &qwTime );
		double fTime = qwTime.QuadPart / ( double )m_llQPFTicksPerSec;
		return fTime;
	}

	double Win32Timer::GetTime() {
		LARGE_INTEGER qwTime = GetAdjustedCurrentTime();
		double fAppTime = ( double )( qwTime.QuadPart - m_llBaseTime ) / ( double )m_llQPFTicksPerSec;
		return fAppTime;
	}

	void Win32Timer::GetTimeValues( double* pfTime, double* pfAbsoluteTime, float* pfElapsedTime ) {
		__IME_ASSERT( pfTime && pfAbsoluteTime && pfElapsedTime );

		LARGE_INTEGER qwTime = GetAdjustedCurrentTime();

		float fElapsedTime = (float) ((double) ( qwTime.QuadPart - m_llLastElapsedTime ) / (double) m_llQPFTicksPerSec);
		m_llLastElapsedTime = qwTime.QuadPart;

		// Clamp the timer to non-negative values to ensure the timer is accurate.
		// fElapsedTime can be outside this range if processor goes into a 
		// power save mode or we somehow get shuffled to another processor.  
		// However, the main thread should call SetThreadAffinityMask to ensure that 
		// we don't get shuffled to another processor.  Other worker threads should NOT call 
		// SetThreadAffinityMask, but use a shared copy of the timer data gathered from 
		// the main thread.
		if (fElapsedTime < 0.0f) {
			fElapsedTime = 0.0f;
		}

		*pfAbsoluteTime = qwTime.QuadPart / ( double )m_llQPFTicksPerSec;
		*pfTime = ( qwTime.QuadPart - m_llBaseTime ) / ( double )m_llQPFTicksPerSec;
		*pfElapsedTime = fElapsedTime;
	}

	float Win32Timer::GetElapsedTime() {
		LARGE_INTEGER qwTime = GetAdjustedCurrentTime();

		double fElapsedTime = (float) ((double) ( qwTime.QuadPart - m_llLastElapsedTime ) / (double) m_llQPFTicksPerSec);
		m_llLastElapsedTime = qwTime.QuadPart;

		// See the explanation about clamping in CDXUTTimer::GetTimeValues()
		if (fElapsedTime < 0.0f) {
			fElapsedTime = 0.0f;
		}

		return ( float )fElapsedTime;
	}

	LARGE_INTEGER Win32Timer::GetAdjustedCurrentTime() {
		LARGE_INTEGER qwTime;

		if (m_llStopTime != 0) {
			qwTime.QuadPart = m_llStopTime;
		}
		else {
			QueryPerformanceCounter(&qwTime);
		}

		return qwTime;
	}

	bool Win32Timer::IsStopped() {
		return m_bTimerStopped;
	}

	void Win32Timer::LimitThreadAffinityToCurrentProc() {
		HANDLE hCurrentProcess = GetCurrentProcess();

		// Get the processor affinity mask for this process
		DWORD_PTR dwProcessAffinityMask = 0;
		DWORD_PTR dwSystemAffinityMask = 0;

		if( GetProcessAffinityMask( hCurrentProcess, &dwProcessAffinityMask, &dwSystemAffinityMask ) != 0 &&
			dwProcessAffinityMask ) {
			// Find the lowest processor that our process is allows to run against
			DWORD_PTR dwAffinityMask = ( dwProcessAffinityMask & ( ( ~dwProcessAffinityMask ) + 1 ) );

			// Set this as the processor that our thread must always run against
			// This must be a subset of the process affinity mask
			HANDLE hCurrentThread = GetCurrentThread();
			if( INVALID_HANDLE_VALUE != hCurrentThread ) {
				SetThreadAffinityMask( hCurrentThread, dwAffinityMask );
				CloseHandle( hCurrentThread );
			}
		}

		CloseHandle( hCurrentProcess );
	}
}