/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

namespace Win32Core
{
	/// <summary>
	/// <c>Win32Timer</c> is timer implementation for Windows platform.
	/// </summary>
	class Win32Timer
	{
	public:
		/// <summary cref="Win32Timer::Win32Timer">
		/// Constructor.
		/// </summary>
		Win32Timer();

		void Reset(); // resets the timer
		void Start(); // starts the timer
		void Stop();  // stop (or pause) the timer
		void Advance(); // advance the timer by 0.1 seconds
		double GetAbsoluteTime(); // get the absolute system time
		double GetTime(); // get the current time
		float GetElapsedTime(); // get the time that elapsed between Get*ElapsedTime() calls
		void GetTimeValues( double* pfTime, double* pfAbsoluteTime, float* pfElapsedTime ); // get all time values at once
		bool IsStopped(); // returns true if timer stopped

		// Limit the current thread to one processor (the current one). This ensures that timing code runs
		// on only one processor, and will not suffer any ill effects from power management.
		void LimitThreadAffinityToCurrentProc();

	protected:
		LARGE_INTEGER   GetAdjustedCurrentTime();

		bool m_bUsingQPF;
		bool m_bTimerStopped;
		LONGLONG m_llQPFTicksPerSec;

		LONGLONG m_llStopTime;
		LONGLONG m_llLastElapsedTime;
		LONGLONG m_llBaseTime;
	};
}