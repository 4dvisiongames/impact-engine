/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#ifndef __Console__ConsoleObject_h__
#define __Console__ConsoleObject_h__

/// Reference counting
#include <Foundation/RefCount.h>
/// STL string
#include <Foundation/STLString.h>
/// Console variable flags
#include <Framework/Console/ConsoleVariableFlags.h>

namespace Framework
{
	class ConsoleObject : public Core::ReferenceCount
	{
	public:
		typedef Enumeration< Traits::TypeEnumeration::ValueType, ConsoleVariableFlags > VariableFlagsType;

		ConsoleObject( void );
		virtual ~ConsoleObject(void) { }

		void SetHelpLines( const STL::String< Char8 >& str );
		const STL::String< Char8 >& GetHelpLines( void ) const;

		/// <summary cref="ConsoleObject::GetFlags">
		/// Get current attached flags of console object.
		/// </summary>
		/// <remarks>
		/// Current implementation doesn't need to be thread-safe from scratch. In any
		/// critical section programmer must provide synchronization, otherwise just use
		/// relaxed read-modify-write operations.
		/// </remarks>
		VariableFlagsType& GetFlags( void );

	protected:
		VariableFlagsType myFlags;
		STL::String< Char8 > myHelpLines;
	};

	__IME_INLINED ConsoleObject::ConsoleObject( void ) 
		: myFlags( ConsoleVariableFlagsEnum::Default ), myHelpLines( "N/A" ) {
		/// Empty
	}

	__IME_INLINED void ConsoleObject::SetHelpLines( const STL::String< Char8 >& str ) {
		this->myHelpLines = str;
	}

	__IME_INLINED const STL::String< Char8 >& ConsoleObject::GetHelpLines( void ) const {
		return this->myHelpLines;
	}

	__IME_INLINED ConsoleObject::VariableFlagsType& ConsoleObject::GetFlags( void ) {
		return this->myFlags;
	}
}

#endif /// __Console__ConsoleObject_h__