#ifndef __Framework__Console__ConsoleObjectProvider_h__
#define __Framework__Console__ConsoleObjectProvider_h__

#include <Framework/Console/ConsoleObject.h>
#include <Foundation/STLHashTable.h>

namespace Framework
{
	class ConsoleObjectProvider : public Core::ReferenceCount
	{
	public:
		ConsoleObjectProvider(const STL::String<Char8>& name) { }
		virtual ~ConsoleObjectProvider() { }

		void Register( const STL::String<Char8>& cvar, const IntrusivePtr< ConsoleObject >& object ) {
			this->myVariablesTable.Add( cvar, object );
		}

		template< typename VariableType >
		IntrusivePtr< VariableType > GetVariableAs( const STL::String<Char8>& cvar );

	private:
		typedef STL::HashTable< STL::String< Char8 >, 
								IntrusivePtr< ConsoleObject >,
								STL::StringHashAlgorithm< Char8 > > ConsoleVariablesTable;
		ConsoleVariablesTable myVariablesTable;
	};

	template< typename VariableType >
	__IME_INLINED IntrusivePtr< VariableType > ConsoleObjectProvider::GetVariableAs( const STL::String<Char8>& cvar ) {
		if ( !this->myVariablesTable.Contains( cvar ) )
			return nullptr;

		return this->myVariablesTable[cvar].Upcast< VariableType >();
	}
}

#endif /// __Framework__Console__ConsoleObjectProvider_h__