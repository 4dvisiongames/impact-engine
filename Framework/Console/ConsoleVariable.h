#ifndef __Framework__Console__ConsoleVariable_h__
#define __Framework__Console__ConsoleVariable_h__

#include <Framework/Console/ConsoleObject.h>

namespace Framework
{
	template< typename Dto >
	class ConsoleVariable : public ConsoleObject
	{
	public:
		typedef Dto ValueType;
		typedef Dto& Reference;
		typedef Dto* Pointer;
		typedef const Dto& ConstReference;
		typedef const Dto* ConstPointer;

		ConsoleVariable( const STL::String<Char8>& help, Reference dto );
		virtual ~ConsoleVariable();

		void SetValue( Reference val );
		ConstReference GetValue() const;

	private:
		ValueType myValueType;
	};

	template< typename Dto >
	__IME_INLINED ConsoleVariable<Dto>::ConsoleVariable( const STL::String<Char8>& help, Reference dto ) 
		: myValueType( dto ) {
		this->myHelpLines = help;
	}

	template< typename Dto >
	__IME_INLINED ConsoleVariable<Dto>::~ConsoleVariable() {

	}

	template< typename Dto >
	__IME_INLINED void ConsoleVariable<Dto>::SetValue( Reference val ) {

	}

	template< typename Dto >
	__IME_INLINED typename ConsoleVariable<Dto>::ConstReference ConsoleVariable<Dto>::GetValue() const {
		return this->myValueType;
	}
}

#endif /// __Framework__Console__ConsoleVariable_h__