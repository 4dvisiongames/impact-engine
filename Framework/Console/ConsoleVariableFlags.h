/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#ifndef __Console__ConsoleVariableFlags_h__
#define __Console__ConsoleVariableFlags_h__

namespace Framework
{
	struct ConsoleVariableFlagsEnum
	{
		enum List
		{
			/// Default flag, nothing to set for command
			Default = 0x0,

			/// Cheating flag is used mainly for debugging proposes. So in release builds all command
			/// variable marked with this flag cannot be changed by the player.
			Cheat = 0x1,

			/// Additional flag for console variable, which indicates variable changing
			Changed = 0x2,

			/// Console variable cannot be changed by the player from in-game console, but that variable
			/// can be changed from code and information files(.ini, .xml, etc.)
			ReadOnly = 0x4,

			/// Used for previously registered console variables, when you cal unregistering command from
			/// console manager. This is used especially for "hot" command swapping.
			Unregistered = 0x8,

			/// Indicates when console command registeres by the code, but it's contents are loaded from 
			/// information file(.ini, .xml, etc.)
			CreatedFromInfo = 0x16,

			/// Maintaines thread-local safety, so when it's changed on changer thread, the engine will
			/// safely copy to another thread-local storages.
			ThreadCritical = 0x32
		};
	};

	typedef ConsoleVariableFlagsEnum::List ConsoleVariableFlags;
}

#endif /// __Console__ConsoleVariableFlags_h__