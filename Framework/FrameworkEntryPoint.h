/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Framework__FrameworkEntryPoint_h__
#define __Framework__FrameworkEntryPoint_h__

#include <Foundation/Thread.h>
#include <Framework/SchedulerServer.h>

namespace Framework
{
	/// <summary>
	/// <c>EntryPoint</c> is a main entry point for the engine. This is engine's idiom is async work
	/// and we cannot block UI thread.
	/// </summary>
	class EntryPoint : private Threading::Thread
	{
	public:
		EntryPoint();
		virtual ~EntryPoint();

		void Setup( void );
		void Dispose( void );

		/// <summary cref="EntryPoint::IsValid">
		/// Check if current entry point is valid.
		/// </summary>
		bool IsValid( void ) const;

		/// <summary cref="EntryPoint::OnPrepare">
		/// </summary>
		virtual void OnPrepare( IntrusivePtr< SchedulerServer >& scheduler ) { /* Override in subclass */ }

		/// <summary cref="EntryPoint::OnDestroy">
		/// </summary>
		virtual void OnDestroy( IntrusivePtr< SchedulerServer >& scheduler ) { /* Override in subclass */ }

	private:
		virtual void Executor( void );

		bool myExitingState;
		bool myValidation;

		IntrusivePtr< SchedulerServer > mySchedulingServer;
	};

	__IME_INLINED bool EntryPoint::IsValid( void ) const {
		bool result = this->myValidation;
		return result;
	}
}

#endif /// __Framework__FrameworkEntryPoint_h__