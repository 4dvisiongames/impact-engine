/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Intrusive reference counter
#include <Foundation/IntrusiveRefCount.h>
/// URL
#include <Framework/Resources/ResourceURL.h>
///
#include <Foundation/BinaryReader.h>
#include <Foundation/WeakPtr.h>

/// Resource management layer namespace
namespace Framework
{
	class ResourceHandler;

	/// <summary>
	/// <c>ResourceStateEnum</c> is a resource state type enumeration.
	/// </summary>
	struct ResourceStateEnum
	{
		enum List
		{
			Pending, //!< Waiting for processing
			Ready, //!< Resource it ready
			Processing, //!< Some processing is going on with the resource
		};
	};
	typedef ResourceStateEnum::List ResourceState;

	/// <summary>
	/// <c>Resource</c> is a resource definitions.
	/// </summary>
	class Resource : public Core::ReferenceCount
	{
	protected:
		SizeT mResType; //!< Current resource type
		ResourceState mResState; //!< Current resource state
		Framework::URL myFileURL; //!< Full resource path
		IntrusiveWeakPtr< ResourceHandler > myResourceHandle; //!< Current resource handler(used for streaming)

	public:
		/// <summary cref="Resource::Resource">
		/// Constructor.
		/// </summary>
		Resource( void );

		/// <summary cref="Resource::~Resource">
		/// Destructor.
		/// </summary>
		virtual ~Resource( void );

		/// <summary cref="Resource::IsLoaded">
		/// Ask resource if it is already loaded.
		/// </summary>
		/// <returns>True if resource state is Ready, otherwise false.</returns>
		bool IsLoaded( void ) const;

		/// <summary cref="Resource::GetURL">
		/// Get resource's path to the resource.
		/// </summary>
		/// <returns>A reference to the path.</returns>
		Framework::URL& GetURL( void );

		/// <summary cref="Resource::GetType">
		/// Get resource type.
		/// </summary>
		/// <returns>Current resource type.</returns>
		USizeT GetType( void ) const;

		/// <summary cref="Resource::GetState">
		/// Get resource resource.
		/// </summary>
		/// <returns>Current resource state.</returns>
		ResourceState GetState( void ) const;

		/// <summary cref="Resource::GetID">
		/// Resolve resource's unique identifier.
		/// </summary>
		/// <returns>Resolved resource unique identificator.</returns>
		IntrusivePtr< ResourceHandler > GetID( void ) const;
	};

	__IME_INLINED bool Resource::IsLoaded( void ) const
	{
		bool result = (ResourceStateEnum::Ready == this->mResState);
		__IME_MEMORY_READWRITE_BARRIER;
		return result;
	}

	__IME_INLINED Framework::URL& Resource::GetURL( void )
	{
		return this->myFileURL;
	}

	__IME_INLINED USizeT Resource::GetType( void ) const
	{
		return this->mResType;
	}

	__IME_INLINED ResourceState Resource::GetState( void ) const
	{
		return this->mResState;
	}

	__IME_INLINED IntrusivePtr< ResourceHandler > Resource::GetID( void ) const
	{
		return this->myResourceHandle.Get();
	}
}