/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Resource
#include <Framework/Resources/Resource.h>
/// File system URI
#include <Framework/Resources/ResourceURL.h>
/// SList
#include <Foundation/IntrusiveSListMPMC.h>

/// Resource management layer namespace
namespace Framework
{
	/// <summary>
	/// <c>ResourceState</c> is a resource state definitions.
	/// </summary>
	struct CommandStateEnumList
	{
		enum List
		{
			Load, //!< Load resource from drive
			Save, //!< Save resource on drive
			Unload, //!< It's unloading now
			CheckForReferences, //!< First check for reference count, then release resource
			Nothing //!< Nothing to do here!
		};
	};
	typedef CommandStateEnumList::List CommandStateEnum;

	/// <summary>
	/// <c>CommandList</c> is a command list definition.
	/// </summary>
	class CommandList : public Threading::IntrusiveSListMPMCNode
	{
		Framework::CommandStateEnum myCommandState; //!< Current command state of resource handing.
		IntrusivePtr< Framework::Resource > myResource; //!< Pointer to the resource used when we loading, unloading or saving it.
		Framework::URL myLocalURL; //!< Resource URL used when first load, or saving in different place.

		__IME_DECLARE_CUSTOM_ALLOCATOR;

	public:
		/// <summary cref="CommandList::CommandList">
		/// Constructor.
		/// </summary>
		CommandList( void );

		/// <summary cref="CommandList::Setup">
		/// Setup command.
		/// </summary>
		/// <param name="state">State to setup with resource.</param>
		/// <param name="idx">Pointer to the specified resource. Can be nullptr.</param>
		/// <param name="uri">URI used to do manipulations with file. Can be nullptr.</param>
		/// <remarks>
		/// Strategy of command state working is that: first check current command state.
		/// And here are different path of next logic:
		///	1) Load: dispatched job checks for specified pointer to the resource, and URL to the
		///	   resource. If something is missed, then exit from loading job, otherwise job performs
		///	   full resource loading.
		///	2) Save: dispatcher job checks for specified pointer to the resource, partially URL pointer.
		///	   If resource pointer is missed, then exit from job, otherwise contiue working. The next step
		///	   is checking for specified URL. If specified URL was valid, then use it, otherwise, it will
		///	   use URL specified in resource.
		///	3) Unload: dispatcher job checks for specified pointer to the resource. URL is ignored this time.
		///	   If resource pointer is missed, then exit from job, otherwise perform full resource unloading.
		/// 4) CheckForReferences: dispatcher job(job count - as number of cores inside PC) checks every
		///	   resources reference counter associated with resource management registry. If resource count
		///	   is zero then set deferred command with Unload command and specified resource pointer.
		/// 5) Nothing: is used for debugging purposes, and never used in production code.
		/// </remarks>
		void Setup( Framework::CommandStateEnum state, IntrusivePtr< Framework::Resource >& resource, Framework::URL* uri );

		/// <summary cref="CommandList::GetResource">
		/// Get resource associated with command.
		/// </summary>
		/// <returns>Pointer to the resource.</returns>
		IntrusivePtr< Framework::Resource >& GetResource( void );

		/// <summary cref="CommandList::GetURL">
		/// Get specified URL with this command, not with resource.
		/// </summary>
		Framework::URL& GetURL( void );

		/// <summary cref="CommandList::GetCommandType">
		/// Get command type associated with command.
		/// </summary>
		/// <returns>Reference to the resource.</returns>
		Framework::CommandStateEnum GetCommandType( void ) const;
	};

	__IME_INLINED CommandList::CommandList( void ) 
		: myCommandState( CommandStateEnumList::Nothing )
		, myResource( nullptr )
		, myLocalURL() {
		this->myLocalURL.SetScheme( NDKOSPath("invalid") );
	}

	__IME_INLINED void CommandList::Setup( Framework::CommandStateEnum state, 
										   IntrusivePtr< Framework::Resource >& resource, 
										   Framework::URL* url ) {
		this->myCommandState = state;

		if( resource )
			this->myResource = resource;
		
		if( url )
			this->myLocalURL = *url;
	}

	__IME_INLINED IntrusivePtr< Framework::Resource >& CommandList::GetResource( void ) {
		return this->myResource;
	}

	__IME_INLINED Framework::URL& CommandList::GetURL( void ) {
		return this->myLocalURL;
	}

	__IME_INLINED CommandStateEnum CommandList::GetCommandType( void ) const {
		return this->myCommandState;
	}
}