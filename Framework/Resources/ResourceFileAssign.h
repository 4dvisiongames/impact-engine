/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// STL Pair
#include <Foundation/STLPair.h>
/// STL string
#include <Foundation/STLString.h>
/// File system primitives
#include <Foundation/FileSystemTypes.h>

/// FileSystem sub-system namespace
namespace Framework
{
	/// <summary>
	/// <c>Assign</c> is a assigning class that helps convert name of resource to
	/// global path and vise versa.
	/// </summary>
	class Assign : public STL::Pair< FileSystem::Path, FileSystem::Path >
	{
	public:
		/// <summary cref="Assign::Assign">
		/// Constructor.
		/// </summary>
		Assign( void );

		/// <summary cref="Assign::Assign">
		/// Constructor.
		/// </summary>
		/// <param name="name">Name of the resource.</param>
		/// <param name="path">Path to the file including file name.</param>
		Assign( const FileSystem::Path& name, const FileSystem::Path& path );

		/// <summary cref="Assign::GetName">
		/// Get name assigned to the path.
		/// </summary>
		/// <returns>Assigned name.</returns>
		const FileSystem::Path& GetName( void ) const;

		/// <summary cref="Assign::GetPath">
		/// Get full global path of resource.
		/// </summary>
		/// <returns>Global path.</returns>
		const FileSystem::Path& GetPath( void ) const;
	};

	__IME_INLINED Assign::Assign( void )
	{
		/// Empty
	}

	__IME_INLINED Assign::Assign( const FileSystem::Path& name, const FileSystem::Path& path ) : STL::Pair< FileSystem::Path, FileSystem::Path >::Pair( name, path )
	{
		/// Assertions
		__IME_ASSERT( name.IsValid() );
		__IME_ASSERT( path.IsValid() );
	}

	__IME_INLINED const FileSystem::Path& Assign::GetName( void ) const
	{
		/// Assertion
		__IME_ASSERT( this->keyData.IsValid() );
		/// Get key data
		return this->keyData;
	}

	__IME_INLINED const FileSystem::Path& Assign::GetPath( void ) const
	{
		/// Assertion
		__IME_ASSERT( this->valueData.IsValid() );
		/// Get value data
		return this->valueData;
	}
}