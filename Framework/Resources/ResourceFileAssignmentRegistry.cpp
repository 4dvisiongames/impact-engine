/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Assign registry
#include <Framework/Resources/ResourceFileAssignmentRegistry.h>
/// OS File system wrapper
#include <Foundation/FileSystemWrapper.h>
/// Default scoped locker
#include <Foundation/ScopedLock.h>

/// FileSystem sub-system namespace
namespace Framework
{
	/// Implement singleton
	__IME_IMPLEMENT_SINGLETON( AssignRegistry );

	AssignRegistry::AssignRegistry( void ) : bIsValid( false )
	{
		/// Construct singleton object
		__IME_CONSTRUCT_SINGLETON;
	}

	AssignRegistry::~AssignRegistry( void )
	{
		/// Release contents if registry is still valid
		if( this->IsValid() )
		{
			this->Discard();
		}
		/// Destruct singleton object
		__IME_DESTRUCT_SINGLETON;
	}

	void AssignRegistry::Setup( void )
	{
		Threading::ScopedLock< Threading::SpinWait > lock( this->mySW );

		/// Assertion
		__IME_ASSERT( !this->IsValid() );
		/// Validate 
		this->bIsValid = true;
		/// Setup all assignments
		this->SetupSystemAssigns();
		this->SetupProjectAssigns();
	}

	void AssignRegistry::Discard( void )
	{
		Threading::ScopedLock< Threading::SpinWait > lock( this->mySW );

		/// Assertion
		__IME_ASSERT( this->IsValid() );
		/// Clear assignment table
		this->mAssignTable.Clear();
		/// Unvalidate
		this->bIsValid = false;
	}

	void AssignRegistry::SetupSystemAssigns( void )
	{
		Framework::Assign binFolder( NDKOSPath( "home" ), FileSystem::OSWrapper::GetBinDirectory() );
		this->mAssignTable.Add( binFolder );
	}

	void AssignRegistry::SetupProjectAssigns( void )
	{
	}

	FileSystem::Path AssignRegistry::ResolveAssign( const Framework::URL& url ) const
	{
		FileSystem::Path resultPath;
		resultPath.Reserve( 50 ); //!< This is overall size of path of 80% of pathes with file names.

		resultPath.Assign( this->GetAssign( url.Scheme() ) );
		resultPath.AppendBack( '/' );
		resultPath.Append( url.LocalPath() );

		return resultPath;
	}
}