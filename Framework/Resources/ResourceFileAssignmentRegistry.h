/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// Reference counter
#include <Foundation/RefCount.h>
/// Singleton object
#include <Foundation/Singleton.h>
/// Assign
#include <Framework/Resources/ResourceFileAssign.h>
/// URI
#include <Framework/Resources/ResourceURL.h>
/// STL hash-table
#include <Foundation/STLHashTable.h>
/// Spin wait
#include <Foundation/SpinWait.h>

/// FileSystem sub-system namespace
namespace Framework
{
	/// <summary>
	/// <c>AssignRegistry</c> is an global assignment registry system that helps Framework Server.
	/// </summary>
	class AssignRegistry : public Core::ReferenceCount
	{
		/// Declare singleton
		__IME_DECLARE_SINGLETON( AssignRegistry );

		bool bIsValid; //!< Validation parameter of registry
		Threading::SpinWait mySW; //!< Critical section
		STL::HashTable< FileSystem::Path, 
						FileSystem::Path,
						STL::StringHashAlgorithm<FileSystem::Path::ValueType>> mAssignTable; //!< Assign hash-table

		/// <summary cref="AssignRegistry::SetupSystemAssigns">
		/// Setup system assignments, e.g. 'home:', 'shaders', etc.
		/// </summary>
		void SetupSystemAssigns( void );

		/// <summary cref="AssignRegistry::SetupProjectAssigns">
		/// Setup project-related assignments, e.g. 'models', 'textures', etc.
		/// </summary>
		void SetupProjectAssigns( void );

	public:
		/// <summary cref="AssignRegistry::AssignRegistry">
		/// Constructor.
		/// </summary>
		AssignRegistry( void );
		
		/// <summary cref="AssignRegistry::~AssignRegistry">
		/// Destructor.
		/// </summary>
		virtual ~AssignRegistry( void );

		/// <summary cref="AssignRegistry::Setup">
		/// Setup assign registry.
		/// </summary>
		void Setup( void );

		/// <summary cref="AssignRegistry::Discard">
		/// Discard assign registry.
		/// </summary>
		void Discard( void );

		/// <summary cref="AssignRegistry::IsValid">
		/// Get validation status of assign registry.
		/// </summary>
		/// <returns>True if validation successfull, otherwise false.</returns>
		bool IsValid( void ) const;

		/// <summary cref="AssignRegistry::SetAssign">
		/// Setup new assign for registry.
		/// </summary>
		/// <param name="assign">Reference to the assign.</param>
		void SetAssign( const Assign& assign );

		/// <summary cref="AssignRegistry::HasAssign">
		/// Check if we have already created assign.
		/// </summary>
		/// <param name="assign">Reference to the string with assign name.</param>
		/// <returns>True if we have assign, otherwise false.</returns>
		bool HasAssign( const FileSystem::Path& assign ) const;

		/// <summary cref="AssignRegistry::GetAssign">
		/// Get assign from registry.
		/// </summary>
		/// <param name="assign">Reference to the string with assign name.</param>
		/// <returns>Full path of specialized assign.</returns>
		FileSystem::Path& GetAssign( const FileSystem::Path& assign ) const;

		/// <summary cref="AssignRegistry::ClearAssign">
		/// Clear assign that is registered in registry.
		/// </summary>
		/// <param name="assign">Reference to the string with assign name.</param>
		void ClearAssign( const FileSystem::Path& assign );

		/// <summary cref="AssignRegistry::GetAllAssigns">
		/// Get all assigns from registry.
		/// </summary>
		/// <returns>Array of assigns registered in registry.</returns>
		STL::Array< Assign > GetAllAssigns( void ) const;

		/// <summary cref="AssignRegistry::ResolveAssigns">
		/// Resolve any assigns in proposed URI.
		/// </summary>
		/// <param name="uri">Reference to the unique resource identificator.</param>
		/// <returns>Resolved assigned URI into unassigned.</returns>
		FileSystem::Path ResolveAssign( const URL& uri ) const;
	};

	__IME_INLINED bool AssignRegistry::IsValid( void ) const
	{
		bool result = this->bIsValid;
		return result;
	}
}