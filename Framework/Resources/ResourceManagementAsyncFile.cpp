/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Framework/Resources/Resource.h>
#include <Framework/Resources/ResourceManagementAsyncFile.h>
#include <Framework/Resources/ResourceHandler.h>
#include <Foundation/StreamReader.h>
#include <Foundation/BinaryReader.h>

namespace Framework
{
	AsyncFileJob::AsyncFileJob( const IntrusivePtr< Framework::Resource >& resource, const FileSystem::Path& path ) 
		: myResource( resource )
		, mySavedPath( path )
	{
	}

	Jobs::Job* AsyncFileJob::Execute()
	{
		/// TODO: make resource stream handling

		IntrusivePtr< FileSystem::BinaryReader > reader = new FileSystem::BinaryReader;
		IntrusivePtr< FileSystem::FileStream > stream = new FileSystem::FileStream;

		stream->SetPath( this->mySavedPath );
		reader->SetInput( stream.Downcast< FileSystem::Stream >() );
		reader->Open();

		//this->myResource->DownloadFromStorage(reader);

		return nullptr;
	}
}