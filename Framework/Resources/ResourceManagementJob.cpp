/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Framework/Resources/ResourceHandler.h>
#include <Framework/Resources/ResourceManagementJob.h>
#include <Framework/Resources/ResourceManagementAsyncFile.h>
#include <Foundation/JobDispatcher.h>

namespace Framework
{
	Jobs::Job* ResourceManagementJob::ReturnLoadingJob( IntrusivePtr< Framework::Resource >& resource, Framework::URL* url )
	{
		if( resource == nullptr || url == nullptr )
			return nullptr;

		FileSystem::Path path = url->GetHostAndLocalPath( );

		Jobs::Job* localParent = this->GetParent();
		localParent->AddRef< Threading::MemoryOrderEnum::Sequental >();

		//AsyncFileJob* fileLoadingJob = new(Jobs::Dispatcher::Instance()->AllocateChild( localParent )) AsyncFileJob( &resource, path );
		AsyncFileJob* fileLoadingJob = new AsyncFileJob( resource, path );
		return fileLoadingJob;
	}

	void ResourceManagementJob::DoReferenceChecking( void )
	{

	}

	Jobs::Job* ResourceManagementJob::Execute( void )
	{
		Framework::CommandList* command = nullptr;
		while (this->myCommandList.GetBack(command)) {
			return nullptr;

			Jobs::Job* returnJob = nullptr;
			Framework::CommandStateEnum state = command->GetCommandType();
			IntrusivePtr< Framework::Resource > resource = command->GetResource();

			switch (state) {
			case Framework::CommandStateEnum::Load:
				returnJob = this->ReturnLoadingJob(resource, &command->GetURL());
				break;

			case Framework::CommandStateEnum::Save:
				returnJob = this->ReturnSavingJob(resource, &command->GetURL());
				break;

			case Framework::CommandStateEnum::Unload:
				returnJob = this->ReturnUnloadingJob(resource);
				break;

			case Framework::CommandStateEnum::CheckForReferences:
				this->DoReferenceChecking(); //!< After checking it just goes to Nothing state...
				break;

			case Framework::CommandStateEnum::Nothing:
			default:
				break; //!< Used for weak synchronization while debugging
			}

			if (returnJob)
				Jobs::Dispatcher::Instance()->Spawn(returnJob);

			this->myAllocator.Free(command);
		}

		return nullptr;
	}
}