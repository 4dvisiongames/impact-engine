/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Job
#include <Foundation/Job.h>
/// Resource commands list
#include <Framework/Resources/ResourceCommandList.h>
/// Intrusive SList
#include <Foundation/IntrusiveSListMPMC.h>
/// Lock-less Free list allocator
#include <Foundation/MemoryLocklessFreeList.h>

namespace Framework
{
	class ResourceManagementJob : public Jobs::Job
	{
		Memory::LocklessFreeList< Framework::CommandList >& myAllocator; //!< Allocator which is used for allocation-deallocation
		Threading::IntrusiveSListMPMC< Framework::CommandList >& myCommandList; //!< My input command list

		Jobs::Job* ReturnLoadingJob( IntrusivePtr< Framework::Resource >& resource, Framework::URL* url );
		Jobs::Job* ReturnSavingJob( IntrusivePtr< Framework::Resource >& resource, Framework::URL* url );
		Jobs::Job* ReturnUnloadingJob( IntrusivePtr< Framework::Resource >& resource );
		void DoReferenceChecking( void );

	public:
		ResourceManagementJob( Memory::LocklessFreeList< Framework::CommandList >& myAllocator, 
							   Threading::IntrusiveSListMPMC< Framework::CommandList >& rhs );

		virtual Jobs::Job* Execute( void );
	};

	__IME_INLINED ResourceManagementJob::ResourceManagementJob( Memory::LocklessFreeList< Framework::CommandList >& alloc,
																Threading::IntrusiveSListMPMC< Framework::CommandList >& rhs ) 
																: myCommandList( rhs )
																, myAllocator( alloc )
	{
		/// Empty
	}
}