/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Resource management scheduling
#include <Framework/Resources/ResourceManagementScheduling.h>
/// Resource management job
#include <Framework/Resources/ResourceManagementJob.h>
/// Resource management registry
#include <Framework/Resources/ResourceFileAssignmentRegistry.h>
/// Jobs dispatcher
#include <Foundation/JobDispatcher.h>
/// Core mathematics
#include <Foundation/Math.h>

namespace Framework
{
	class ResourceManagerInitJob : public Jobs::Job
	{
		IntrusivePtr< Framework::AssignRegistry > myAssignmentRegistry;

	public:
		ResourceManagerInitJob( IntrusivePtr< Framework::AssignRegistry >& registry );
		Jobs::Job* Execute( void );
	};

	Jobs::Job* ResourceManagerInitJob::Execute( void )
	{
		__IME_ASSERT( !this->myAssignmentRegistry.IsValid() );
		IntrusivePtr< Framework::AssignRegistry > registry = new Framework::AssignRegistry;

		registry->Setup();

		/// Assign registry
		this->myAssignmentRegistry = registry;

		return nullptr;
	}

	__IME_IMPLEMENT_SINGLETON( Framework::ResourceManagementScheduling );

	ResourceManagementScheduling::ResourceManagementScheduling( void ) 
		: Framework::ScheduledObject( "ResourceManager" )
		, myFirstList()
		, mySecondList()
	{
		__IME_CONSTRUCT_SINGLETON;
	}

	ResourceManagementScheduling::~ResourceManagementScheduling( void )
	{
		__IME_DESTRUCT_SINGLETON;
	}

	void ResourceManagementScheduling::QueueResource( Framework::CommandStateEnum state, 
													  IntrusivePtr< Framework::Resource >& resource, 
													  Framework::URL* url )
	{
		Framework::CommandList* mCommand = this->myCommandListAllocator.Allocate();
		__IME_ASSERT( mCommand );

		mCommand->Setup( state, resource, url );

		/// Select primary or deferred command list for resource processing
		if( this->PeekCompleteness() )
			this->myFirstList.Prepend( *mCommand );
		else
			this->mySecondList.Prepend( *mCommand );
	}

	Jobs::Job* ResourceManagementScheduling::AllocateInitialization( void )
	{
		return new ResourceManagerInitJob(this->myAssignmentRegistry);
	}

	USizeT ResourceManagementScheduling::Process( Jobs::Job* parentJob )
	{
		static const SizeT granularityJobs = 64;

		SizeT references = parentJob->GetRefCount< Threading::MemoryOrderEnum::Acquire >();
		SizeT temp = 0;

		SizeT deferredCmds = STL::CountOf(this->mySecondList);
		if( deferredCmds > 0 ) {
			deferredCmds = (deferredCmds > granularityJobs) ? deferredCmds : granularityJobs;
			temp += deferredCmds;
		}

		SizeT currentCmds = STL::CountOf(this->myFirstList);
		if( currentCmds > 0 ) {
			currentCmds = (currentCmds > granularityJobs) ? currentCmds : granularityJobs;
			temp += currentCmds;
		}

		references += temp;
		parentJob->SetRefCount< Threading::MemoryOrderEnum::Release >( references );
		if( !this->mySecondList.IsEmpty() ) {
			for( SizeT i = 0; i < deferredCmds; ++i ) {
				Framework::ResourceManagementJob* j = new Framework::ResourceManagementJob( this->myCommandListAllocator, this->mySecondList );
				j->SetParent( parentJob );
				Jobs::Dispatcher::Instance()->Spawn( j );
			}
		}

		if( !this->myFirstList.IsEmpty() ) {
			for( SizeT i = 0; i < currentCmds; ++i ) {
				Framework::ResourceManagementJob* j = new Framework::ResourceManagementJob( this->myCommandListAllocator, this->mySecondList );
				j->SetParent( parentJob );
				Jobs::Dispatcher::Instance()->Spawn( j );
			}
		}

		return temp;
	}
}