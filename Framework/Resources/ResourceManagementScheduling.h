/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Resource
#include <Framework/Resources/ResourceCommandList.h>
/// Framework URL
#include <Framework/Resources/ResourceURL.h>
/// Resource handler
#include <Framework/Resources/ResourceHandler.h>
/// Scheduled object
#include <Framework/ScheduledObject.h>
/// STL intrusive list
#include <Foundation/IntrusiveSListMPMC.h>
/// Lockless free-list
#include <Foundation/MemoryLocklessFreeList.h>
/// Dictionary
#include <Foundation/STLDictionary.h>
#include <Foundation/STLHashTable.h>
/// Singleton
#include <Foundation/Singleton.h>
/// Rtti
//#include <Foundation/Rtti.h>

namespace Framework
{
	/// Forward declaration
	class AssignRegistry;

	/// <summary>
	/// <c>ResourceManagementScheduling</c> is an interface to do resource management in the engine.
	/// Resources are created through jobs distribution to make use of multithreading, also every resource
	/// type must have it's own handling policy. Every resource management job calls resource's handler
	/// to ensure how to load. 
	/// All methods are implicitly thread-safe through lock-free calls.
	/// </summary>
	/// <remarks>
	/// Most important part of management is how every resource managed. Many rendering subsystems cannot
	/// make use of multithreaded resource creation. So resource manager will do final preparation on
	/// selected thread by some policy. 
	/// Also resource management system uses file assignment registry to do file readings from specified 
	/// paths. This is because resource paths are stored in platform-independent URL, which could be only
	/// resolved by the assignment registry.
	/// </remarks>
	class ResourceManagementScheduling : public Framework::ScheduledObject
	{
		__IME_DECLARE_SINGLETON( ResourceManagementScheduling );
		friend class ResourceServer;

	public:
		ResourceManagementScheduling( void );
		virtual ~ResourceManagementScheduling( void );

		//! Add new resource to the global queue for later loading
		IntrusivePtr< Framework::Resource > QueueLoading( SizeT resType, Framework::URL& uri, bool userCall = true );
		//! Save resource in the other space, specified by the URI.
		void QueueSaving( IntrusivePtr< Framework::Resource >& resource, Framework::URL& uri );
		//! Save resource at the same position, where original lives.
		void QueueSaving( IntrusivePtr< Framework::Resource >& resource );
		//! manual resource unloading from the global registry.
		void QueueUnloading( IntrusivePtr< Framework::Resource >& resource );
		//! Find resource in global registry.
		bool Search( const STL::String< Char8 >& name ) const;
		//! Get resource by the indexed value.
		IntrusivePtr< Framework::Resource >& GetResource( SizeT idx );

		//!< Release all unused resources
		void QueueReleaseUnusedResources( void );
		//!< Request data streaming. This forces manager to stream more data into resource for using. Resources that
		//!< are not suited for streaming will fail without crash, but such calling could slow down performance.
		void RequestResourceStream(const IntrusivePtr< Resource >& resource);

	private:
		virtual Jobs::Job* AllocateInitialization(void);
		virtual USizeT Process( Jobs::Job* parentJob );

		void QueueResource( Framework::CommandStateEnum state, 
							IntrusivePtr< Framework::Resource >& resource, 
							Framework::URL* uri );
		
		IntrusivePtr< Framework::AssignRegistry > myAssignmentRegistry; //!< Singleton object implacement

		//typedef STL::Dictionary< SizeT, Utils::Rtti< Resource >* > ResourceRTTIRegistry;
		typedef Threading::SpinWait ResourceRTTISW;
		//ResourceRTTIRegistry myResourcesRTTIRegistry; //!< Resource type registry
		ResourceRTTISW myResourceRTTISW;

		typedef STL::HashTable< STL::String<Char8>, 
								IntrusivePtr< Framework::ResourceHandler >,
								STL::StringHashAlgorithm<Char8>> ResourcesRegistry;
		typedef Threading::SpinWait ResourcesRegistrySW;
		ResourcesRegistry myActiveResources;
		ResourcesRegistrySW myActiveResourcesSW;

		//! Memory allocator for commands. This must be thread-safe.
		typedef Memory::LocklessFreeList< Framework::CommandList > CommandListAllocator;
		CommandListAllocator myCommandListAllocator;

		//! Two command list used for primary and deferred resource command recording
		//! While first is primary, second becomes deferred where all next command are written
		//! on next iteration they are just swapped.

		typedef Threading::IntrusiveSListMPMC< Framework::CommandList > CommandListHead;
		CommandListHead myFirstList;
		CommandListHead mySecondList;
	};

	__IME_INLINED IntrusivePtr< Framework::Resource > ResourceManagementScheduling::QueueLoading( SizeT resType, Framework::URL& url, bool userCall )
	{
		// TODO: repair me!
		IntrusivePtr< Framework::Resource > resource; //= this->myResourcesRTTIRegistry[resType]->Invoke();
		this->QueueResource( Framework::CommandStateEnumList::Load, resource, &url );
		return resource;
	}

	__IME_INLINED void ResourceManagementScheduling::QueueSaving( IntrusivePtr< Framework::Resource >& resource, Framework::URL& url )
	{
		__IME_ASSERT( resource.IsValid() );
		this->QueueResource( Framework::CommandStateEnumList::Save, resource, &url );
	}

	__IME_INLINED void ResourceManagementScheduling::QueueSaving( IntrusivePtr< Framework::Resource >& resource )
	{
		__IME_ASSERT( resource.IsValid() );
		this->QueueResource( Framework::CommandStateEnumList::Save, resource, &resource->GetURL( ) );
	}

	__IME_INLINED void ResourceManagementScheduling::QueueUnloading( IntrusivePtr< Framework::Resource >& resource )
	{
		__IME_ASSERT( resource.IsValid() );
		this->QueueResource( Framework::CommandStateEnumList::Unload, resource, nullptr );
	}
}