/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// STL string
#include <Foundation/STLString.h>
/// STL array
#include <Foundation/STLArray.h>

/// Resource management layer namespace
namespace Framework
{
	/// <summary>
	/// <c>MediaType</c> is a media type definition.
	/// </summary>
	class MediaType
	{
		STL::String< Char8 > type; //!< Type of media
		STL::String< Char8 > subtype; //!< Subtype of media

	public:
		/// <summary cref="MediaType::MediaType">
		/// Constructor.
		/// </summary>
		MediaType( void );

		/// <summary cref="MediaType::MediaType">
		/// Initialize constructor from string.
		/// </summary>
		/// <param name="str">Reference to the string.</param>
		MediaType( const STL::String< Char8 >& str );

		/// <summary cref="MediaType::MediaType">
		/// Initialize constructor from type and subtype.
		/// </summary>
		/// <param name="type">Media type.</param>
		/// <param name="subType">Sub-type of current media.</param>
		MediaType( const STL::String< Char8 >& type, const STL::String< Char8 >& subType );

		/// <summary cref="MediaType::MediaType">
		/// Copy constructor.
		/// </summary>
		/// <param name="rhs">A reference to the media type.</param>
		MediaType( const MediaType& rhs );

		/// <summary cref="MediaType::operator=">
		/// Assignment operator.
		/// </summary>
		/// <param name="rhs">A reference to the media type.</param>
		void operator = ( const MediaType& rhs );

		/// <summary cref="MediaType::operator!=">
		/// Unequality operator.
		/// </summary>
		/// <param name="rhs">A reference to the media type.</param>
		bool operator != ( const MediaType& rhs );

		/// <summary cref="MediaType::operator==">
		/// Equality operator.
		/// </summary>
		/// <param name="rhs">A reference to the media type.</param>
		bool operator == ( const MediaType& rhs );

		/// <summary cref="MediaType::IsValid">
		/// Validate media type.
		/// </summary>
		/// <returns>True if it is valid, otherwise false.</returns>
		bool IsValid( void ) const;

		/// <summary cref="MediaType::Clear">
		/// Clear the media type object.
		/// </summary>
		void Clear( void );

		/// <summary cref="MediaType::Set">
		/// Setup media type as string.
		/// </summary>
		/// <param name="str">String of media type.</param>
		void Set( const STL::String< Char8 >& str );

		/// <summary cref="MediaType::Set">
		/// Setup media as type plus subtype.
		/// </summary>
		/// <param name="type">Type of media.</param>
		/// <param name="subtype">Subtype.</param>
		void Set( const STL::String< Char8 >& type, const STL::String< Char8 >& subtype );

		/// <summary cref="MediaType::AsString">
		/// Build type as string and return it.
		/// </summary>
		STL::String< Char8 > AsString( void ) const;

		/// <summary cref="MediaType::GetType">
		/// Get media type.
		/// </summary>
		const STL::String< Char8 >& GetType( void ) const;

		/// <summary cref="MediaType::GetSubtype">
		/// Get media subtype.
		/// </summary>
		const STL::String< Char8 >& GetSubtype( void ) const;
	};

	__IME_INLINED MediaType::MediaType( void )
	{
		/// Empty
	}

	__IME_INLINED void MediaType::Set( const STL::String< Char8 >& str )
	{
		__IME_ASSERT( str.IsValid() );
		STL::Array< STL::String< Char8 > > tokens;
		str.Tokenize( "/", tokens );
		__IME_ASSERT(STL::CountOf(tokens) == 2);
		this->type = tokens[0];
		this->subtype = tokens[1];
	}

	__IME_INLINED MediaType::MediaType( const STL::String< Char8 >& str )
	{
		this->Set( str );
	}

	__IME_INLINED MediaType::MediaType( const STL::String< Char8 >& t, const STL::String< Char8 >& s ) : type( t ), subtype( s )
	{
		/// Some assertions
		__IME_ASSERT( type.IsValid() );
		__IME_ASSERT( subtype.IsValid() );
	}

	__IME_INLINED MediaType::MediaType( const MediaType& rhs ) : type( rhs.type ), subtype( rhs.subtype )
	{
		/// Empty
	}

	__IME_INLINED void MediaType::operator = ( const MediaType& rhs )
	{
		this->type = rhs.type;
		this->subtype = rhs.subtype;
	}

	__IME_INLINED bool MediaType::operator == ( const MediaType& rhs )
	{
		return( (this->type == rhs.type) && (this->subtype == rhs.subtype) );
	}

	__IME_INLINED bool MediaType::operator != ( const MediaType& rhs )
	{
		return !(*this == rhs);
	}

	__IME_INLINED void MediaType::Set( const STL::String< Char8 >& t, const STL::String< Char8 >& s )
	{
		__IME_ASSERT( t.IsValid() && s.IsValid() );
		this->type = t;
		this->subtype = s;
	}

	__IME_INLINED STL::String< Char8 > MediaType::AsString( void ) const
	{
		STL::String< Char8 > str;
		str.Reserve( 128 );
		str = this->type;
		str.Append( "/" );
		str.Append( this->subtype );
		return str;
	}

	__IME_INLINED const STL::String< Char8 >& MediaType::GetType( void ) const
	{
		return this->type;
	}

	__IME_INLINED const STL::String< Char8 >& MediaType::GetSubtype( void ) const
	{
		return this->subtype;
	}

	__IME_INLINED bool MediaType::IsValid( void ) const
	{
		return (this->type.IsValid() && this->subtype.IsValid());
	}

	__IME_INLINED void MediaType::Clear( void )
	{
		this->type.Clear();
		this->subtype.Clear();
	}
}