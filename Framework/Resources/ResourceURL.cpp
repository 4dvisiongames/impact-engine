/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// URL
#include <Framework/Resources/ResourceURL.h>
/// String utilities
#include <Foundation/StringConverter.h>
/// File system wraper
#include <Foundation/FileSystemWrapper.h>

/// Input/Output sub-system namespace
namespace Framework
{
	bool URL::Split( const FileSystem::Path& s )
	{
		return true;
	}

	URL::URL( const FileSystem::Path& s ) : isEmpty(true)
	{
		bool validURL = this->Split(s);
		__IME_ASSERT_MSG( validURL, STL::StringConverter::WideToUTF8( s.AsCharPtr() ).AsCharPtr() );
	}

	URL::URL( const FileSystem::Path::ValueType* s ) : isEmpty(true)
	{
		bool validURL = this->Split( s );
		__IME_ASSERT_MSG( validURL, STL::StringConverter::WideToUTF8( s ).AsCharPtr() );
	}

	URL::URL(const URL& rhs) :
		isEmpty(rhs.isEmpty),
		scheme(rhs.scheme),
		userInfo(rhs.userInfo),
		host(rhs.host),
		port(rhs.port),
		localPath(rhs.localPath),
		fragment(rhs.fragment),
		query(rhs.query)
	{
		/// Empty
	}    

	void URL::operator=(const URL& rhs)
	{
		this->isEmpty = rhs.isEmpty;
		this->scheme = rhs.scheme;
		this->userInfo = rhs.userInfo;
		this->host = rhs.host;
		this->port = rhs.port;
		this->localPath = rhs.localPath;
		this->fragment = rhs.fragment;
		this->query = rhs.query;
	}

	bool URL::operator == (const URL& rhs) const
	{
		if (this->isEmpty && rhs.isEmpty)
		{
			return true;
		}
		else
		{
			return ((this->scheme == rhs.scheme) &&
					(this->userInfo == rhs.userInfo) &&
					(this->host == rhs.host) &&
					(this->port == rhs.port) &&
					(this->localPath == rhs.localPath) &&
					(this->fragment == rhs.fragment) &&
					(this->query == rhs.query));
		}
	}

	void URL::Clear( void ) 
	{
		this->isEmpty = true;
		this->scheme.Clear();
		this->userInfo.Clear();
		this->host.Clear();
		this->port.Clear();
		this->localPath.Clear();
		this->fragment.Clear();
		this->query.Clear();
	}

	FileSystem::Path URL::GetHostAndLocalPath() const
	{
		FileSystem::Path str;
		str.Reserve(this->host.Length() + this->localPath.Length() + 8);
		if (!this->host.IsEmpty())
		{
			str.Append( NDKOSPath( "//" ) );
			str.Append( this->host.AsCharPtr() );
			str.Append( NDKOSPath( "/" ) );
		}
		str.Append(this->localPath);
		return str;
	}
}