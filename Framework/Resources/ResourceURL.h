/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

/// File system types
#include <Foundation/FileSystemTypes.h>
/// STL array
#include <Foundation/STLArray.h>

/// Resource management layer namespace
namespace Framework
{
	/// <summary>
	/// <c>URL</c> is a unique resource link. 
	/// </summary>
	class URL
	{
	public:
		/// default constructor
		URL( void );
		/// init constructor
		URL( const FileSystem::Path& s );
		/// init constructor
		URL( const FileSystem::Path::ValueType* s );
		/// copy constructor
		URL( const URL& rhs );
		/// assignmnent operator
		void operator = ( const URL& rhs );
		/// equality operator
		bool operator == ( const URL& rhs ) const;
		/// inequality operator
		bool operator != ( const URL& rhs ) const;
    
		/// set complete URL string
		void Set( const FileSystem::Path& s );
		/// return as concatenated string
		FileSystem::Path AsString( void ) const;

		/// return true if the URL is empty
		bool IsEmpty( void ) const;
		/// return true if the URL is not empty
		bool IsValid( void ) const;
		/// clear the URL
		void Clear( void );
		/// set Scheme component (ftp, http, etc...)
		void SetScheme( const FileSystem::Path& s );
		/// get Scheme component (default is file)
		const FileSystem::Path& Scheme( void ) const;
		/// set UserInfo component
		void SetUserInfo( const FileSystem::Path& s );
		/// get UserInfo component (can be empty)
		const FileSystem::Path& UserInfo( void ) const;
		/// set Host component
		void SetHost( const FileSystem::Path& s );
		/// get Host component (can be empty)
		const FileSystem::Path& Host() const;
		/// set Port component
		void SetPort( const FileSystem::Path& s );
		/// get Port component (can be empty)
		const FileSystem::Path& Port( void ) const;
		/// set LocalPath component
		void SetLocalPath( const FileSystem::Path& s );
		/// get LocalPath component (can be empty)
		const FileSystem::Path& LocalPath( void ) const;
		/// append an element to the local path component
		void AppendLocalPath( const FileSystem::Path& pathComponent );
		/// set Fragment component
		void SetFragment( const FileSystem::Path& s );
		/// get Fragment component (can be empty)
		const FileSystem::Path& Fragment( void ) const;
		/// set Query component
		void SetQuery( const FileSystem::Path& s );
		/// get Query component (can be empty)
		const FileSystem::Path& Query( void ) const;
		/// parse query parameters into a dictionary
		STL::Dictionary< FileSystem::Path, FileSystem::Path > ParseQuery( void ) const;
		/// get the "tail" (path, query and fragment)
		FileSystem::Path GetTail( void ) const;
		/// get the host and path without scheme
		FileSystem::Path GetHostAndLocalPath() const;

	private:
		/// split string into components
		bool Split( const FileSystem::Path& s );
		/// build string from components
		FileSystem::Path Build() const;

		bool isEmpty; //!< Is current URL empty?
		FileSystem::Path scheme; //!< Scheme name
		FileSystem::Path userInfo; //!< User information
		FileSystem::Path host; //!< Host part of URL(optional)
		FileSystem::Path port; //!< Port part of URL(optional)
		FileSystem::Path localPath; //!< Local path
		FileSystem::Path fragment;
		FileSystem::Path query;
	};

	__IME_INLINED URL::URL( void ) : isEmpty(true)
	{
		/// Empty
	}

	__IME_INLINED bool URL::operator != (const URL& rhs) const
	{
		return !(*this == rhs);
	}

	__IME_INLINED bool URL::IsEmpty( void ) const
	{
		return this->isEmpty;
	}

	__IME_INLINED bool URL::IsValid( void ) const
	{
		return !(this->isEmpty);
	}

	__IME_INLINED void URL::Set( const FileSystem::Path& s )
	{
		this->Split(s);
	}

	__IME_INLINED FileSystem::Path URL::AsString( void ) const
	{
		return this->Build();
	}

	__IME_INLINED void URL::SetScheme( const FileSystem::Path& s )
	{
		this->isEmpty = false;
		this->scheme = s;
	}

	__IME_INLINED const FileSystem::Path& URL::Scheme( void ) const
	{
		return this->scheme;
	}

	__IME_INLINED void URL::SetUserInfo( const FileSystem::Path& s )
	{
		this->isEmpty = false;
		this->userInfo = s;
	}

	__IME_INLINED const FileSystem::Path& URL::UserInfo( void ) const
	{
		return this->userInfo;
	}

	__IME_INLINED void URL::SetHost( const FileSystem::Path& s )
	{
		this->isEmpty = false;
		this->host = s;
	}

	__IME_INLINED const FileSystem::Path& URL::Host() const
	{
		return this->host;
	}

	__IME_INLINED void URL::SetPort( const FileSystem::Path& s )
	{
		this->isEmpty = false;
		this->port = s;
	}

	__IME_INLINED const FileSystem::Path& URL::Port( void ) const
	{
		return this->port;
	}

	__IME_INLINED void URL::SetLocalPath( const FileSystem::Path& s )
	{
		this->isEmpty = false;
		this->localPath = s;
	}

	__IME_INLINED const FileSystem::Path& URL::LocalPath( void ) const
	{
		return this->localPath;
	}

	__IME_INLINED void URL::SetFragment( const FileSystem::Path& s )
	{
		this->isEmpty = false;
		this->fragment = s;
	}

	__IME_INLINED const FileSystem::Path& URL::Fragment() const
	{
		return this->fragment;
	}

	__IME_INLINED void URL::SetQuery( const FileSystem::Path& s )
	{
		this->isEmpty = false;
		this->query = s;
	}

	__IME_INLINED const FileSystem::Path& URL::Query() const
	{
		return this->query;
	}

}