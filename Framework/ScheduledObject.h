/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Reference counting
#include <Foundation/RefCount.h>
/// Job
#include <Foundation/Job.h>
/// Memory barriers
#include <Foundation/MemoryBarriers.h>
/// STL intrusive list
#include <Foundation/STLIntrusiveList.h>
/// STL hierarchy
#include <Foundation/STLIntrusiveHierarchy.h>

namespace Framework
{
	class ScheduledObject : public STL::IntrusiveListNode, 
							public STL::IntrusiveHierarchy< ScheduledObject >, 
							public Core::ReferenceCount
	{
		friend class SchedulerServer;

	public:
		virtual ~ScheduledObject( void ) { }
		
		virtual Jobs::Job* AllocateInitialization() { return nullptr; };
		virtual Jobs::Job* AllocateShutdown() { return nullptr; };
		
		/// Do processing of jobs, associated with this scheduled object
		virtual USizeT Process( Jobs::Job* parentJob ) = 0;

		/// Peek object completeness
		bool PeekCompleteness( void ) const;

		/// Get time of processing
		float GetProcessingTime( void ) const;

		/// Get job count which were processed within scheduling
		USizeT GetJobsProcessed( void ) const;

		/// Get name of current scheduled object
		const Char8* GetName( void ) const;

	protected:
		ScheduledObject( const Char8* scheduledName ) : myScheduledName( scheduledName ) { }

	private:
		float UpdateState( Jobs::Job* parentJob, USizeT& processedJobsCount );
		STL::String< Char8 > myScheduledName; //!< Name of jobs group that are processed
		float myProcessingTime;
		USizeT myProcessedJobsCount;
		Jobs::Job* myParentJob; //!< Used for checking current status of processed jobs
	};

	__IME_INLINED bool ScheduledObject::PeekCompleteness( void ) const
	{
		SizeT jobs = 0;

		if( this->myParentJob )
			jobs = this->myParentJob->GetRefCount< Threading::MemoryOrderEnum::Acquire >();

		return jobs == 0;
	}

	__IME_INLINED float ScheduledObject::UpdateState( Jobs::Job* parentJob, USizeT& processedJobsCount )
	{
		/// This must be before any execution
		this->myParentJob = parentJob;

		USizeT result = this->Process( parentJob );
		processedJobsCount = result;

		this->myProcessedJobsCount = result;
		__IME_MEMORY_WRITE_BARRIER;
		this->myProcessingTime = 0.0f;
		__IME_MEMORY_WRITE_BARRIER;

		return this->myProcessingTime;
	}

	__IME_INLINED float ScheduledObject::GetProcessingTime( void ) const
	{
		float result = this->myProcessingTime;
		return result;
	}

	__IME_INLINED USizeT ScheduledObject::GetJobsProcessed( void ) const
	{
		USizeT result = this->myProcessedJobsCount;
		return result;
	}

	__IME_INLINED const Char8* ScheduledObject::GetName( void ) const
	{
		return this->myScheduledName.AsCharPtr();
	}
}
