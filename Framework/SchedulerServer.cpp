/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// Scheduler server
#include <Framework/SchedulerServer.h>
/// Dispatcher
#include <Foundation/JobDispatcher.h>
/// Default scoped locker
#include <Foundation/ScopedLock.h>

namespace Framework
{
	__IME_IMPLEMENT_SINGLETON( SchedulerServer );

	SchedulerServer::SchedulerServer( void ) 
		: myJobsServer( nullptr )
		//, myPrevSyncJob( nullptr )
		, myLastJobPreparationTime( 0.0f )
		, myJobPreparationTime( 0.0f )
		, myJobCountFromAll( 0 )
	{
		__IME_CONSTRUCT_SINGLETON;
	}

	SchedulerServer::~SchedulerServer( void )
	{
		__IME_DESTRUCT_SINGLETON;
	}

	void SchedulerServer::Setup( void )
	{
		/// Create scheduling server
		IntrusivePtr< Jobs::Server > server = new Jobs::Server;
		server->Setup();
		this->myJobsServer = server;

		this->PrepareScheduledObjects();
	}

	void SchedulerServer::Discard( void )
	{
		this->DestroyScheduledObjects();

		//delete this->myPrevSyncJob;

		//this->myPrevSyncJob = nullptr;
		this->myJobsServer->Discard();
		this->myJobsServer = nullptr;
	}

	void SchedulerServer::InsertObjectIntoList( const IntrusivePtr< ScheduledObject >& object )
	{
		Threading::ScopedLock< SchedObjectLocker > lock( this->myObjectsListSW );
		SchedObjectList& objectList = this->myObjectsList;
		objectList.Prepend( const_cast< IntrusivePtr< ScheduledObject >& >( object ) );
	}

	void SchedulerServer::RemoveObjectFromList( const IntrusivePtr< ScheduledObject >& object )
	{
		Threading::ScopedLock< SchedObjectLocker > lock( this->myObjectsListSW );
		SchedObjectList& objectList = this->myObjectsList;
		objectList.Remove( const_cast< IntrusivePtr< ScheduledObject >& >(object) );
	}

	void SchedulerServer::Process( void )
	{
		if( this->myObjectsList.Empty() )
			return;

		float thisPreparationTime = 0.0f;
		USizeT thisJobCount = 0U;

		SchedObjectIterator previous;
		for( SchedObjectIterator current = this->myObjectsList.Begin(); current != this->myObjectsList.End(); previous = current++ ) 
		{
			USizeT theJobsCount = 0U;
			float thePreparationTime = 0.0f;
			IntrusivePtr< ScheduledObject > object = *current;

			Jobs::Job* theSyncJob = new Jobs::Job();
			theSyncJob->SetRefCount< Threading::MemoryOrderEnum::Relaxed >( 1 );
			theSyncJob->Setup();

			thePreparationTime = object->UpdateState( theSyncJob, theJobsCount );
			thisPreparationTime += thePreparationTime;
			thisJobCount += theJobsCount;

			/// Wait for previous jobs
			/// This is made for better concurrency, so other threads will not wait for new 
			/// jobs spawned for them.
			this->WaitForObjectComplete( previous );

			//STL::SwapAlgorithm( theSyncJob, this->myPrevSyncJob );

			if( theSyncJob )
				delete theSyncJob;
		}

		/// This is because of algorithm, and we need to wait for previous object to be completed
		this->WaitForObjectComplete( previous );

		this->myJobPreparationTime = (thisPreparationTime + this->myLastJobPreparationTime) * 0.5f;
		this->myLastJobPreparationTime = thisPreparationTime;
		this->myJobCountFromAll = thisJobCount;
	}

	void SchedulerServer::WaitForObjectComplete( SchedObjectIterator iter )
	{
		if( !iter.IsValid() )
			return;

		/*if( this->myPrevSyncJob ) {
			if( this->myPrevSyncJob->GetRefCount< Threading::MemoryOrderEnum::Acquire >() > 1 ) {
				float executionTimerMS = 0.0f;
				Jobs::Dispatcher::Instance()->Wait( this->myPrevSyncJob );
				IntrusivePtr< ScheduledObject > prevObject = *iter;
				if( prevObject.IsValid() ) {
					prevObject->myProcessingTime = executionTimerMS;
				}
			}
		}*/
	}

	void SchedulerServer::PrepareScheduledObjects() 
	{
		STL::IntrusiveSList< Jobs::Job > myInitializationList;

		STL::ForEach( this->myObjectsList.Begin(), this->myObjectsList.End(),
					  [&myInitializationList]( IntrusivePtr< ScheduledObject >& obj ) {
			Jobs::Job* j = obj->AllocateInitialization();
			if ( j )
				myInitializationList.Prepend( *j );
		} );

		if ( !myInitializationList.IsEmpty() ) 
		{
			Jobs::Job* myInitSyncJob = new Jobs::Job();
			myInitSyncJob->SetRefCount< Threading::MemoryOrderEnum::Relaxed >(STL::CountOf(myInitializationList) + 1);
			myInitSyncJob->Setup();

			STL::ForEach( myInitializationList.Begin(), myInitializationList.End(), [&myInitSyncJob]( Jobs::Job& j ) {
				j.SetParent( myInitSyncJob );
				j.Setup();
				Jobs::Dispatcher::Instance()->Spawn( &j );
			});

			Jobs::Dispatcher::Instance()->Wait( myInitSyncJob );
		}
	}

	void SchedulerServer::DestroyScheduledObjects() 
	{
		STL::IntrusiveSList< Jobs::Job > myInitializationList;

		STL::ForEach( this->myObjectsList.Begin(), this->myObjectsList.End(),
					  [&myInitializationList]( IntrusivePtr< ScheduledObject >& obj ) {
			Jobs::Job* j = obj->AllocateShutdown();
			if ( j )
				myInitializationList.Prepend( *j );
		} );

		if ( !myInitializationList.IsEmpty() ) 
		{
			Jobs::Job* myInitSyncJob = new Jobs::Job();
			myInitSyncJob->SetRefCount< Threading::MemoryOrderEnum::Relaxed >(STL::CountOf(myInitializationList) + 1);
			myInitSyncJob->Setup();

			STL::ForEach( myInitializationList.Begin(), myInitializationList.End(), [myInitSyncJob]( Jobs::Job& j ) {
				j.SetParent( myInitSyncJob );
				j.Setup();
				Jobs::Dispatcher::Instance()->Spawn( &j );
			});

			Jobs::Dispatcher::Instance()->Wait( myInitSyncJob );
		}
	}
}