//============= (C) Copyright 2014, PARANOIA Team. All rights reserved. =============
/// Desc: Scheduling system for all scheduled objects of all engine.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

/// Scheduled object
#include <Framework/ScheduledObject.h>
/// Jobs server
#include <Foundation/JobServer.h>
/// Job
#include <Foundation/Job.h>
/// Singleton
#include <Foundation/Singleton.h>
/// Thread local enumerable storage
#include <Foundation/ThreadLocalStorage.h>

namespace Framework
{
	class SchedulerServer : public Core::ReferenceCount
	{
		__IME_DECLARE_SINGLETON( SchedulerServer );

	public:
		SchedulerServer( void );
		virtual ~SchedulerServer( void );

		void Setup( void );
		void Discard( void );

		void InsertObjectIntoList( const IntrusivePtr< ScheduledObject >& object );
		void RemoveObjectFromList( const IntrusivePtr< ScheduledObject >& object );

		/// Force scheduler to make engine update
		void Process( void );

		/// Get job preparation time
		float GetJobPreparationTime( void ) const;

		/// Get job count within last iteration
		USizeT GetJobCount( void ) const;

	private:
		typedef STL::IntrusiveList< IntrusivePtr< ScheduledObject > > SchedObjectList;
		typedef SchedObjectList::Iterator SchedObjectIterator;
		typedef Threading::SpinWait SchedObjectLocker;

		void WaitForObjectComplete( SchedObjectIterator iter );

		void PrepareScheduledObjects();
		void DestroyScheduledObjects();

		IntrusivePtr< Jobs::Server > myJobsServer; //!< Server that process all jobs
		Threading::ThreadLocalStorage< Jobs::Job* > myThreadLocalJobs; //!< 
		Jobs::Job* myThreadLocalJobWaitSync; //!< Used for waiting on exit from execution loop

		SchedObjectLocker myObjectsListSW;
		SchedObjectList myObjectsList;

		float myLastJobPreparationTime; //!< Time preparation from previous iterationing
		float myJobPreparationTime; //!< Time to prepare all jobs to be allocated and spawned
		USizeT myJobCountFromAll; //!< Overall jobs count from all scheduled objects
	};

	__IME_INLINED float SchedulerServer::GetJobPreparationTime( void ) const {
		float result = this->myJobPreparationTime;
		return result;
	}

	__IME_INLINED USizeT SchedulerServer::GetJobCount( void ) const {
		USizeT result = this->myJobCountFromAll;
		return result;
	}
}