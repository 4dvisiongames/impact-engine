/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Framework/Win32/Win32Window.h>

namespace Framework
{
	Window::Window(const IntrusivePtr<WindowMessageLoop>& messageLoop) 
		: myMessageLoop(messageLoop) {
	}

	Window::~Window() {

	}

	void Window::Setup() {
		SetWindowLongPtr(this->myWindowHandle, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
	}

	LRESULT CALLBACK Window::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) {
		IntrusivePtr<Window> theWindow = reinterpret_cast<Window*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));
		IntrusivePtr<WindowMessageLoop> theMessageLoop = theWindow->myMessageLoop;

		switch (msg) {
		case WM_CREATE:
			theMessageLoop->Win32MessageOnCreate();
			break;

		case WM_SYSCOMMAND:
			break;

		case WM_ERASEBKGND:
			return FALSE;

		case WM_GETTEXT:
			return DefWindowProc(hWnd, msg, wParam, lParam);

		case WM_DESTROY:
			theMessageLoop->Win32MessageOnDestroy(hWnd);
			break;
		}

		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}