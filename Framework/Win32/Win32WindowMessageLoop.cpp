#include <Framework/Win32/Win32WindowMessageLoop.h>

namespace Framework
{
	void WindowMessageLoop::Win32MessageOnDestroy(HWND hWnd) {
		if (!this->IsInjected()) {
			ShowWindow(hWnd, 0);
			PostQuitMessage(0);
		}
	}
}