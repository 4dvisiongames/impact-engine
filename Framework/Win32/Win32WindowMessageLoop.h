/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Framework__WindowMessageLoop_h__
#define __Framework__WindowMessageLoop_h__

#include <Foundation/RefCount.h>

namespace Framework
{
	class WindowMessageLoop : public Core::ReferenceCount
	{
		friend class Window;

		bool myInjectionState;

	public:
		WindowMessageLoop(bool isInjected = false);

		/// <summary cref="WindowMessageLoop::AttachEventHandler"
		/// </summary>
		void AttachEventHandler();

		/// <summary cref="WindowMessageLoop::IsInjected">
		/// </summary>
		bool IsInjected() const;

	private:
		void Win32MessageOnCreate();
		void Win32MessageOnDestroy(HWND hWnd);
	};

	__IME_INLINED bool WindowMessageLoop::IsInjected() const {
		return this->myInjectionState;
	}
}

#endif /// __Framework__WindowMessageLoop_h__