#pragma once

#ifndef __Game__Actor_h__
#define __Game__Actor_h__

/// Reference counter
#include <Game/Entity.h>

namespace Game
{
	class Actor : public Entity
	{
		NDKDeclareClass( Actor );

	public:
		Actor( void );
		virtual ~Actor( void );
	};
}

#endif /// __Game__Actor_h__