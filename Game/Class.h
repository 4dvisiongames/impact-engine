/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Game__Class_h__
#define __Game__Class_h__

#include <Game/ClassTypeInfo.h>

/// These utility preprocessor definitions used for simplified game-related class creation
/// - __IME_DECLARE_GAME_CLASS is used for declaration inside you game class derived from Game::Class. nameOfClass is a type name.
/// - __IME_IMPLEMENT_GAME_CLASS is used for definition in your *.cpp file where is Game::Class-derived class implemented.

#define __IME_DECLARE_GAME_CLASS( nameOfClass ) \
	public: \
		static ClassTypeInfo TypeInfo; \
		static class Class* CreateInstance( void ); \
		virtual ClassTypeInfo& GetTypeInfo( void ) const; \
	private:

#define __IME_IMPLEMENT_GAME_CLASS( nameOfClass, superClass ) \
	Game::ClassTypeInfo nameOfClass::TypeInfo( #nameOfClass, superClass, STL::DelegatedFunction< Class* >::Bind( nameOfClass::CreateInstance ), \
		STL::DelegatedMethod< void, Class, SaveGame* >::Bind( &nameOfClass::Save ), STL::DelegatedMethod< void, Class, RestoreGame* >::Bind( &nameOfClass::Restore ) ); \
	Class* nameOfClass::CreateInstance( void ) { return new nameOfClass; } \
	ClassTypeInfo& nameOfClass::GetTypeInfo( void ) const { return nameOfClass::TypeInfo; }

namespace Game
{
	/// Forward declaration
	class SaveGame;
	class RestoreGame;

	/// <summary>
	/// <c>Class</c> is a base class used for all game-related entities to be accessiable from logic and
	/// editor instances.
	/// </summary>
	class Class : public Core::ReferenceCount
	{
		__IME_DECLARE_GAME_CLASS( Class );

	public:
		virtual ~Class( void );

		/// <summary cref="Class::Save">
		/// Save method does all saving routines from game class to save current game state.
		/// </summary>
		/// <param name="ptr">Pointer to the <c>SaveGame</c> instance</param>
		/// <remarks>
		/// This must be non-virtual method because of delegate using inside <c>ClassTypeInfo</c>.
		/// </remarks>
		void Save( SaveGame* ptr ) { /* Implement me in subclass */ }

		/// <summary cref="Class::Restore">
		/// Restore method does all reading routines from game class to restore previous game state.
		/// </summary>
		/// <param name="ptr">Pointer to the <c>RestoreGame</c> instance</param>
		/// <remarks>
		/// This must be non-virtual method because of delegate using inside <c>ClassTypeInfo</c>.
		/// </remarks>
		void Restore( RestoreGame* ptr ) { /* Implement me in subclass */ }


	};
}

#endif /// __Game__Class_h__