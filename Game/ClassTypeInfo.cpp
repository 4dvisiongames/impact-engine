#include <Game/ClassTypeInfo.h>

namespace Game
{
	ClassTypeInfo::ClassTypeInfo( const STL::String< Char8 >& className, ClassTypeInfo* parentClass, CreateInstanceProc createFunc,
								  SaveGameProc saveGameFunc, RestoreGameProc restoreGameFunc )
	: myClassType( className )
	, myCreatorFunction( createFunc )
	, mySaveGameFunction( saveGameFunc ) 
	, myRestoreGameFunction( restoreGameFunc ) {

	}
}