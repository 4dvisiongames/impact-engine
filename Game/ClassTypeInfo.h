#pragma once

#ifndef __Game__ClassTypeInfo_h__
#define __Game__ClassTypeInfo_h__

#include <Foundation/RefCount.h>
#include <Foundation/STLIntrusiveHierarchy.h>
#include <Foundation/STLString.h>
#include <Foundation/STLDelegate.h>

namespace Game
{
	class Class;
	class SaveGame;
	class RestoreGame;

	class ClassTypeInfo : public STL::IntrusiveHierarchy< ClassTypeInfo >
	{
	public:
		typedef STL::DelegatedFunction< Class* > CreateInstanceProc;
		typedef STL::DelegatedMethod< void, Class, SaveGame* > SaveGameProc;
		typedef STL::DelegatedMethod< void, Class, RestoreGame* > RestoreGameProc;

		ClassTypeInfo( const STL::String< Char8 >& className, ClassTypeInfo* parentClass, CreateInstanceProc createFunc,
					   SaveGameProc saveGameFunc, RestoreGameProc restoreGameFunc );

		const STL::String< Char8 >& GetClassType( void ) const;

	private:
		STL::String< Char8 > myClassType;

		CreateInstanceProc myCreatorFunction;
		SaveGameProc mySaveGameFunction;
		RestoreGameProc myRestoreGameFunction;
	};

	__IME_INLINED const STL::String< Char8 >& ClassTypeInfo::GetClassType( void ) const {
		return this->myClassType;
	}
}

#endif /// __Game__ClassTypeInfo_h__