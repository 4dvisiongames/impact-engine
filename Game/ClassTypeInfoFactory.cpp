#include <Game/ClassTypeInfo.h>
#include <Game/ClassTypeInfoFactory.h>
#include <Game/Class.h>

namespace Game
{
	ClassTypeInfoFactory::ClassTypeInfoFactory( void )
		: myRootClassHierarchy( &Game::Class::TypeInfo )
		, myRegisteredClasses(512) {

	}

	void ClassTypeInfoFactory::Register( ClassTypeInfo& info ) {
		if ( !this->IsExists( info ) ) {
			const char* str = info.GetClassType().AsCharPtr();
			Debug::Messaging::Send( Debug::EMessageType::Error, STL::String<Char8>::VA( "[Game::ClassTypeInfoFactory] Class '%s' has been already registered!", str ) );
			return;
		}

		this->myRegisteredClasses.Add( info.GetClassType(), &info );
	}

	bool ClassTypeInfoFactory::IsExists( const ClassTypeInfo& type ) {
		const STL::String< Char8 >& className = type.GetClassType();
		return this->myRegisteredClasses.Contains( className );
	}

	const ClassTypeInfo* ClassTypeInfoFactory::GetClassType( const STL::String< Char8 >& className ) {
		const ClassTypeInfo* result = nullptr;

		if ( this->myRegisteredClasses.Contains( className ) )
			result = this->myRegisteredClasses[className];

		return result;
	}
}