#pragma once

#ifndef __Game__ClassTypeInfoFactory_h__
#define __Game__ClassTypeInfoFactory_h__

#include <Foundation/STLString.h>
#include <Foundation/STLHashTable.h>

namespace Game
{
	class ClassTypeInfo;

	class ClassTypeInfoFactory
	{
	private:
		ClassTypeInfoFactory( void );

	public:
		static ClassTypeInfoFactory* Instance( void );
		void Register( ClassTypeInfo& info );
		class Class* Create( const STL::String< Char8 >& className );
		bool IsExists( const ClassTypeInfo& type );
		const ClassTypeInfo* GetClassType( const STL::String< Char8 >& className );

	private:
		ClassTypeInfo* myRootClassHierarchy;
		STL::HashTable< STL::String< Char8 >, const ClassTypeInfo* > myRegisteredClasses;
	};

	ClassTypeInfoFactory* ClassTypeInfoFactory::Instance( void ) {
		static ClassTypeInfoFactory theInstance;
		return &theInstance;
	}
}

#endif /// __Game__ClassTypeInfoFactory_h__