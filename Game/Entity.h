#pragma once

#ifndef __Game__Entity_h__
#define __Game__Entity_h__

#include <Game/Class.h>

namespace Game
{
	class Entity : public Class
	{
		__IME_DECLARE_GAME_CLASS( Entity );

	public:
		Entity( void );
		virtual ~Entity( void );

		/// State updating

		void Think( void );
		bool IsThinking( void ) const;
	};
}

#endif /// __Game__Entity_h__