/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

/// Reference counter
#include <Foundation/RefCount.h>
/// Singleton
#include <Foundation/Singleton.h>

/// PhysX API
#include <PxPhysicsAPI.h>
/// PhysX memory allocator
#include <Game/Physics/PhysXMemoryAllocator.h>
#include <Game/Physics/PhysXErrorCallback.h>
#include <Game/Physics/PhysXAssertHandler.h>

namespace Game
{
	namespace Physics
	{
		/// <summary>
		/// </summary>
		class PhysXBridge : public Core::ReferenceCount
		{
			__IME_DECLARE_SINGLETON(PhysXBridge);

			physx::PxFoundation* myFoundation;
			physx::PxPhysics* myPhysics;
			physx::PxVisualDebuggerConnection* myVdbConnection;
			
			PhysXAssertHandler myAssertHandler;
			PhysXErrorCallback myErrorCallback;
			PhysXMemoryAllocator myMemoryAllocator;

		public:
			/// <summary cref="PhysXBridge::PhysXBridge">
			/// </summary>
			PhysXBridge();

			/// <summary cref="PhysXBridge::~PhysXBridge">
			/// </summary>
			virtual ~PhysXBridge();

			/// <summary cref="PhysXBridge::Setup">
			/// </summary>
			void Setup();

			/// <summary cref="PhysXBridge::Discard">
			/// </summary>
			void Discard();

			/// <summary cref="PhysXBridge::IsValid">
			/// </summary>
			bool IsValid();

			/// <summary cref="PhysXBridge::GetPhysics">
			/// </summary>
			physx::PxPhysics* GetPhysics();

			/// <summary cref="PhysXBridge::GetVisualDebuggerConnection">
			/// </summary>
			physx::PxVisualDebuggerConnection* GetVisualDebuggerConnection();
		};
	}
}