/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Game__Physics__PhysXMemoryAllocator_h__
#define __Game__Physics__PhysXMemoryAllocator_h__

#include <foundation/PxAllocatorCallback.h>

namespace Game
{
	namespace Physics
	{
		class PhysXMemoryAllocator : public physx::PxAllocatorCallback
		{
		public:
			virtual ~PhysXMemoryAllocator();

			/**
			\brief Allocates size bytes of memory, which must be 16-byte aligned.

			This method should never return NULL.  If you run out of memory, then
			you should terminate the app or take some other appropriate action.

			<b>Threading:</b> This function should be thread safe as it can be called in the context of the user thread
			and physics processing thread(s).

			\param size			Number of bytes to allocate.
			\param typeName		Name of the datatype that is being allocated
			\param filename		The source file which allocated the memory
			\param line			The source line which allocated the memory
			\return				The allocated block of memory.
			*/
			virtual void* allocate(size_t size, const char* typeName, const char* filename, int line);

			/**
			\brief Frees memory previously allocated by allocate().

			<b>Threading:</b> This function should be thread safe as it can be called in the context of the user thread
			and physics processing thread(s).

			\param ptr Memory to free.
			*/
			virtual void deallocate(void* ptr);
		};
	}
}

#endif /// __Game__Physics__PhysXMemoryAllocator_h__