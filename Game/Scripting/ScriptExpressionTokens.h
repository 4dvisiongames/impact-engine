/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#ifndef __Scripting__ScriptExpressionTokens_h__
#define __Scripting__ScriptExpressionTokens_h__

namespace Game
{
	namespace Scripting
	{
		struct ExpressionTokensEnum
		{
			enum List
			{
				Local,
				RefCountedObject,
				Return,
				Jmp,
				Jne,
				Assertion,
				Nop,
				Assign,
				Cast,
				AsBoolean,
				EndParameterValue,
				EndFunctionParameters,
				This,
				Skippable,
				Context,
				ContextFailure,
				Virtual,
				Final,
				AsConstInt,
				AsConstFloat,
				AsConstString,
				AsConstRefCountedObject,
				AsConstNamed,
				AsConstMatrix34,
				AsConstVec4f,
				AsZero,
				AsOne,
				AsBooleanTrue,
				AsBooleanFalse,
				NoObject,
				MutableConstant,
				AsConstByte,
				DynamicCasting,
				StaticCasting,
				ReinterpretCasting,
				PrimitiveCasting,
				AsConstStruct,
				EndStruct,
				AssignValArray,
				EndArray,
				AsConstWString,
				StructMemberContext,
				AssignMulticastDelegate,
				AssignDelegate,
				LocalReferencedVariable, // passed by reference
				ReferencedCloneDelegate, // const reference to delegate object
				StartExecutionFlow,
				EndExecutionFlow,
				CustomJmp,
				EndExecutionFlowNe,
				DebugBreakpoint,
				InterfaceCtx,
				InterfaceCast,
				EndOfByteCode,
				DebugTracepoint,
				AsSkipoffsetConst,
				AddMulticastDelegate,
				CleanMulticastDelegate,
				AssignRefCountedObject,
				AssignAsWeakPointer,
				PrebindDelegate,
				RemoveMulticastDelegate,
				CallMulticastDelegate
			};
		};

		typedef ExpressionTokensEnum::List ExpressionTokens;
	}
}

#endif /// __Scripting__ScriptExpressionTokens_h__