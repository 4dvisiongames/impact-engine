#include <Game/Scripting/ScriptFiber.h>
#include <Foundation/MemoryLocklessFreeList.h>

namespace Game
{
	namespace Scripting
	{
		typedef Memory::LocklessFreeList< Fiber > ScriptFiberAllocator;

		__IME_IMPLEMENT_GAME_CLASS( Fiber, &Class::TypeInfo );
		__IME_IMPLEMENT_CUSTOM_ALLOCATOR( Fiber, ScriptFiberAllocator );

		Fiber::Fiber( void )
			: myValidation( false ) {

		}

		Fiber::~Fiber( void ) {
			if ( this->IsValid() )
				this->Discard();
		}

		void Fiber::Setup( void ) {

		}

		void Fiber::Discard( void ) {

		}
	}
}