/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Game__Scripting__ScriptFiber_h__
#define __Game__Scripting__ScriptFiber_h__

#include <Game/Entity.h>

namespace Game
{
	namespace Scripting
	{
		/// <summary>
		/// <c>Fiber</c> is a script execution unit that does local context management for one
		/// script and makes it isolated for execution.
		/// </summary>
		class Fiber : public Class
		{
			__IME_DECLARE_GAME_CLASS( Fiber );
			__IME_DECLARE_CUSTOM_ALLOCATOR;

		public:
			Fiber( void );

			/// <summary cref="Fiber::Setup">
			/// Setups selected fiber for the first time. Calling this more times will crash app in release
			/// mode, but in debug mode app will stop with assertion failure.
			/// </summary>
			void Setup( void );

			/// <summary cref="Fiber::Discard">
			/// </summary>
			void Discard( void );

			/// <summary cref="Fiber::IsValid">
			/// Validates current fiber.
			/// </summary>
			/// <returns>True if <c>myValidation</c> is valid with true, otherwise false.</returns>
			bool IsValid( void ) const;

			/// Saving & loading

			/// <summary cref="Fiber::Save">
			/// </summary>
			/// <param name="save">Pointer to the save context.</param>
			void Save( SaveGame* save );

			/// <summary cref="Fiber::Restore">
			/// </summary>
			/// <param name="restore">Pointer to the restore context.</param>
			void Restore( RestoreGame* restore );

			/// Scripting-related 

			/// <summary cref="Fiber::Execute">
			/// Calls function to execute with selected arguments.
			/// </summary>
			void Execute( void );

			/// <summary cref="Fiber::WaitForFiber">
			/// Schedules current fiber to wait for execution of other fiber, because it might be
			/// on the other thread.
			/// </summary>
			void WaitForFiber( IntrusivePtr< Fiber >& fiber );

		private:
			bool myValidation;
		};

		__IME_INLINED bool Fiber::IsValid( void ) const {
			bool result = this->myValidation;
			return result;
		}
	}
}

#endif /// __Game__Scripting__ScriptFiber_h__