/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#ifndef __Scripting__ScriptFunctionFlags_h__
#define __Scripting__ScriptFunctionFlags_h__

namespace Game
{
	namespace Scripting
	{
		struct FunctionFlagsEnum
		{
			enum List
			{
				Public,
				Private,
				Protected,

				/// Function cannot be non-overrable and prebindable, just as Java-like final
				Final,

				/// Function can be executed from every command line(line console)
				CmdLineExecution,

				/// Function can be called from user code(otherwise engine can only do this)
				Callable,

				Native,
				Static,
				Const,
				Delegate,
				Event,

				/// 
				MulticastDelegate,
				HasDefaults,
				HasOutputParameters,

				/// Function is just for network use
				Network,

				/// Function will only run if the object has network authority
				NetworkAutohority,

				/// Function is used on non-dedicated, peer-to-peer servers
				NetworkNonDedicatedServer,

				/// Function show produce work for reliable connection and data exchanging
				NetworkReliable,

				/// Function is requesting data from client/server
				NetworkRequest,

				/// Function is a responce for requesting function
				NetworkResponse,

				/// Function is always executed on servers
				NetworkServer,

				/// Function is broadcast exection of function for all clients
				NetworkServerMulticast,

				/// Function is executed only on clients
				NetworkClient,

				/// Function is used for validation
				NetworkValidate,


				CombinationInherit = CmdLineExecution | Event | Callable | NetworkAutohority | NetworkNonDedicatedServer,
				CombinationOverride = CmdLineExecution | Final | Static | Public | Protected | Private,
				CombinationNetworkOnly = Network | NetworkReliable | NetworkServer | NetworkClient | NetworkServerMulticast,
				CombinationAccess = Public | Protected | Private
			};
		};

		typedef FunctionFlagsEnum::List FunctionFlags;
	}
}

#endif /// __Scripting__ScriptFunctionFlags_h__