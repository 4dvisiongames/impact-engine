#pragma once

#ifndef __Game__WorldScheduling_h__
#define __Game__WorldScheduling_h__

/// Scheduled object
#include <Framework/ScheduledObject.h>
/// Singleton
#include <Foundation/Singleton.h>

/// Array
#include <Foundation/STLArray.h>

namespace Game
{
	/// Forward declaration
	class Actor;

	class WorldScheduledObject : public Framework::ScheduledObject
	{
	public:
		WorldScheduledObject( void );
		virtual ~WorldScheduledObject( void );

		void AddActor( const IntrusivePtr< Actor >& actor );
		void RemoveActor( const IntrusivePtr< Actor >& actor );

	private:
		STL::Array< IntrusivePtr< Actor > > myActors; //!< Actors to be simulted for game logics
	};
}

#endif /// __Game__WorldScheduling_h__