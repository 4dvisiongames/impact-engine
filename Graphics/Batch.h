/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#ifndef __Graphics__Batch_h__
#define __Graphics__Batch_h__

#include <Foundation/RefCount.h>
#include <Foundation/WeakPtr.h>
#include <Foundation/MemoryLocklessFreeList.h>
#include <Foundation/STLArray.h>

#include <Graphics/Geometry.h>
#include <Graphics/BatchType.h>

namespace Graphics
{
	class Light;

	class Batch : public Core::ReferenceCount
	{
	protected:
		Enumeration< u32, BatchType > myBatchType;

		IntrusiveWeakPtr< Batch > myParentBatch; //!< Batch that dependens on this(used for multipass)
		IntrusivePtr< Geometry > myGeometrySubset; //!< Geometry object to be rendered

		SizeT myStartVertex;
		SizeT myVertexCount;
		SizeT myIndexCount;

		Batch(BatchType type);

	public:
		virtual ~Batch();

		void SetParent(const IntrusiveWeakPtr< Batch >& myParentBatch);
		IntrusiveWeakPtr< Batch >& GetParent();

		BatchType GetType() const;
	};

	__IME_INLINED void Batch::SetParent(const IntrusiveWeakPtr< Batch >& parent) {
		this->myParentBatch = parent;
	}

	IntrusiveWeakPtr< Batch >& Batch::GetParent() {
		return this->myParentBatch;
	}

	__IME_INLINED BatchType Batch::GetType() const {
		return this->myBatchType;
	}
}

#endif /// __Graphics__Batch_h__

/*
	change log:

	15-Mar-15	codepoet	class Batch		__IME_DECLARE_CUSTOM_ALLOCATOR now written without type of allocator, because we don't need it anymore
*/