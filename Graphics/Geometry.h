//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Geometry implementation using some renderer, defined in compiling settings.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _Graphics_Geometry_h_
#define __Graphics__Geometry_h__

#include <Rendering/StreamSource.h>
#include <Rendering/IndexBuffer.h>
#include <Rendering/VertexElement.h>
#include <Graphics/RenderMode.h>

namespace Graphics
{
	class Geometry : public Core::ReferenceCount
	{
		int myFVFHashCode; // This hash code used for easier vertex declaration access
		RenderMode myRenderMode;
		Renderer::StreamSource myVertexStreams[ Renderer::StreamSource::MaxStreams ];
		IntrusivePtr< Renderer::IndexBuffer > myIndexBuffer;

	public:
		Geometry( void );
		virtual ~Geometry( void );
		void Setup( RenderMode mode, const Renderer::StreamSource& primarySrc, const IntrusivePtr< Renderer::IndexBuffer >& indexBuffer );
		void Discard( void );
	};

	__IME_INLINED void Geometry::Setup( RenderMode mode, const Renderer::StreamSource& primarySrc, const IntrusivePtr< Renderer::IndexBuffer >& indexBuffer )
	{
		this->myRenderMode = mode;
		this->myVertexStreams[0] = primarySrc;
		this->myIndexBuffer = indexBuffer;
	}
}

#endif /// _Graphics_Geometry_h_