/*

Copyright (c) 2015, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

/// Graphics sub-system
#include <Graphics/GraphicsScheduling.h>
#include <Foundation/Job.h>

/// Graphics sub-system namespace
namespace Graphics
{
	NDKImplementSingleton( Graphics::GraphicsScheduling );

	GraphicsScheduling::GraphicsScheduling( void ) 
		: Framework::ScheduledObject("Graphics")
		, pDisplayAdapter(nullptr)
		, pRenderDevice(nullptr)
		, pRenderContext(nullptr)
		, pVertexCache(nullptr)
		, pRenderTargetManager(nullptr) {
		NDKConstructSingleton;
	}

	GraphicsScheduling::~GraphicsScheduling( void ) {
		NDKDestructSingleton;
	}

	Jobs::Job* GraphicsScheduling::AllocateInitialization(Jobs::Job* parentJob) { 
		auto myFunction = [this]()-> void {
				/// Create display adapter manager
				pDisplayAdapter = new Renderer::DisplayAdapter();
				/// Create rendering sub-system instance
				pRenderDevice = new Renderer::RenderingDevice(pDisplayAdapter);
				/// Create vertex cache manager
				pVertexCache = new Graphics::VertexCache();
				/// Create render target manager
				pRenderTargetManager = new Graphics::RenderTargetManager();

				/// Open sub-systems
				pDisplayAdapter->Setup();
				pRenderDevice->Setup();
				Graphics::VertexCache::Instance()->Open();
				Graphics::RenderTargetManager::Instance()->Open();

				/// Create main rendering context
				IntrusivePtr< Renderer::RenderingContext > pMainContext = new Renderer::RenderingContext(pRenderDevice);
				/// Open rendering context
				pMainContext->Setup();
				/// Append main context to the thread-local storage
				this->mThreadLocalContexts.Add(GetCurrentThreadId(), pMainContext);
		};

		return new Jobs::LambdaJob<decltype(myFunction)>(STL::Move(myFunction));
	}

	Jobs::Job* GraphicsScheduling::AllocateShutdown() {
		auto myFunction = [this]()-> void {
			/// Some assertions
			__IME_ASSERT(pDisplayAdapter != nullptr);
			__IME_ASSERT(pRenderDevice != nullptr);
			__IME_ASSERT(pRenderContext != nullptr);
			__IME_ASSERT(pVertexCache != nullptr);
			__IME_ASSERT(pRenderTargetManager != nullptr);

			/// Close rendering contexts
			for (SizeT idx = 0; idx < this->mThreadLocalContexts.Size(); ++idx)
				this->mThreadLocalContexts.ValueAtIndex(idx)->Close();

			/// Close or discard sub-systems
			pRenderDevice->Discard();
			pDisplayAdapter->Discard();
		};

		return new Jobs::LambdaJob<decltype(myFunction)>(STL::Move(myFunction));
	}

	USizeT GraphicsScheduling::Process(Jobs::Job* parentJob) {

	}
}

/*
	change log:

	18-Mar-15		codepoet		class GraphicsScheduling							major refactoring, Open() and Close() methods are gone now
	18-Mar-15		codepoet		method GraphicsScheduling::AllocateInitialization	implemented
	18-Mar-15		codepoet		method GraphicsScheduling::AllocateShutdown			implemented
*/