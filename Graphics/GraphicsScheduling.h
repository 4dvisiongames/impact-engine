/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef _NanoGraphics_h_
#define _NanoGraphics_h_

/// Scheduled object
#include <Framework/ScheduledObject.h>
/// Singleton
#include <Foundation/Singleton.h>

/// Render system
#include <Rendering/RenderingContext.h>
#include <Rendering/RenderingDevice.h>
/// Display adpater
#include <Rendering/DisplayAdapter.h>
/// Render target managing
#include <Graphics/RenderTargetManager.h>
/// Hardware vertex cacher
#include <Graphics/VertexCache.h>

#include <Graphics/RenderingEvent.h>

/// Graphics sub-system namespace
namespace Graphics
{
	/// <summary>
	/// <c>GraphicsSystem</c> is core graphics rendering system.
	/// </summary>
	class GraphicsScheduling : public Framework::ScheduledObject
	{
		IntrusivePtr< Renderer::DisplayAdapter > pDisplayAdapter;
		IntrusivePtr< Renderer::RenderingDevice > pRenderDevice;
		IntrusivePtr< Renderer::RenderingContext > pRenderContext;
		IntrusivePtr< Graphics::VertexCache > pVertexCache; //!< Vertex cache manager
		IntrusivePtr< Graphics::RenderTargetManager > pRenderTargetManager; //!< Render target manager

	public:
		/// <summary cref="GraphicsSystem::GraphicsSystem">
		/// Constructor.
		/// </summary>
		GraphicsScheduling( void );

		/// <summary cref="GraphicsSystem::~GraphicsSystem">
		/// Destructor.
		/// </summary>
		virtual ~GraphicsScheduling( void );

		virtual Jobs::Job* AllocateInitialization();
		virtual Jobs::Job* AllocateShutdown();

		/// Do processing of jobs, associated with this scheduled object
		virtual USizeT Process( Jobs::Job* parentJob );

		/// Append event to the graphics queue
		void SendEvent( Graphics::RenderingEvent::List eventType, bool blockUntilComplete, s32 firstArg, s32 secondArg );
	};
}

#endif /// _NanoGraphics_h_

/*
	change log:

	18-Mar-15		codepoet		method GraphicsScheduling::Open		removed
	18-Mar-15		codepoet		method GraphicsScheduling::Close	removed
*/