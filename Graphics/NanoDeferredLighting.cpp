//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Deferred lighting system.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Deferred lighting system
#include <NanoDeferredLighting.h>
/// Depth buffer
#include <NanoDepthBuffer.h>
/// Render target
#include <NanoRenderTarget.h>
/// Render target manager
#include <NanoRenderTargetManager.h>

/// Graphics sub-system namespace
namespace Graphics
{
	/// Implement singleton
	NDKImplementSingleton( Graphics::DeferredLighting );

	DeferredLighting::DeferredLighting( void )
	{
		NDKConstructSingleton;
	}

	DeferredLighting::~DeferredLighting( void )
	{
		NDKDestructSingleton;
	}

	void DeferredLighting::Setup( void )
	{
		/// Get screen sizes
		unsigned int uiWidth = 1280;
		unsigned int uiHeight = 720;

		/// Create all Render Target texture
		Ptr< Renderer::Texture > pTex0 = new Renderer::Texture();
		Ptr< Renderer::Texture > pTex1 = new Renderer::Texture();
		Ptr< Renderer::Texture > pTex2 = new Renderer::Texture();

		/// Setup texture sizes
		pTex0->SetWidth( uiWidth );
		pTex0->SetHeight( uiHeight );
		pTex1->SetWidth( uiWidth );
		pTex1->SetHeight( uiHeight );
		pTex2->SetWidth( uiWidth );
		pTex2->SetHeight( uiHeight );

		/// Setup render formats
		pTex0->SetRenderFormat( Renderer::TextureFormat::DepthAccessiable );
		pTex1->SetRenderFormat( Renderer::TextureFormat::RGBA8 );
		pTex2->SetRenderFormat( Renderer::TextureFormat::RGBA8 );

		/// Setup flags
		/// Depth buffer
		pTex0->SetUsage( Renderer::UsageEnum::Default );
		pTex0->SetShaderResourceType( Renderer::ShaderResourceType::ResourceObject2D );
		pTex0->SetTextureBind( Renderer::TextureBindEnum::DepthStencilView );
		pTex0->SetTextureBind( Renderer::TextureBindEnum::ShaderResourceView );
		/// First RT
		pTex1->SetUsage( Renderer::UsageEnum::Default );
		pTex1->SetShaderResourceType( Renderer::ShaderResourceType::ResourceObject2D );
		pTex1->SetTextureBind( Renderer::TextureBindEnum::RenderTargetView );
		pTex1->SetTextureBind( Renderer::TextureBindEnum::ShaderResourceView );
		/// Second RT
		pTex2->SetUsage( Renderer::UsageEnum::Default );
		pTex2->SetShaderResourceType( Renderer::ShaderResourceType::ResourceObject2D );
		pTex2->SetTextureBind( Renderer::TextureBindEnum::RenderTargetView );
		pTex2->SetTextureBind( Renderer::TextureBindEnum::ShaderResourceView );

		/// Create all render targets
		pTex0->Setup();
		pTex1->Setup();
		pTex2->Setup();

		/// Make them current
		this->pDepthRT = pTex0;
		this->pFirstRT = pTex1;
		this->pSecondRT = pTex2;

		/// Create Depth stencil buffer
		Ptr< Renderer::DepthBuffer > pDepthBuffer = new Renderer::DepthBuffer();
		Ptr< Renderer::RenderTarget > pGBuffer = new Renderer::RenderTarget();

		/// Attach textures
		pDepthBuffer->AttachTexture( pTex0 );
		pGBuffer->AttachTexture( pTex1 );
		pGBuffer->AttachTexture( pTex2 );

		/// Setup flags
		pDepthBuffer->SetName( "GBufferDS" );
		pGBuffer->SetName( "GBufferMRT" );

		/// Setup buffers
		pDepthBuffer->Setup();
		pGBuffer->Setup();

		/// Add them into render target manager
		Graphics::RenderTargetManager::Instance()->RegisterDepthBuffer( pDepthBuffer );
		Graphics::RenderTargetManager::Instance()->RegisterRenderTarget( pGBuffer );
	}

	void DeferredLighting::Discard( void )
	{
		/// TODO: Add something to destroy
	}
}