//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Deferred lighting system.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoDeferredLighting_h_
#define _NanoDeferredLighting_h_

/// Constant buffer
#include <Rendering/ConstantBuffer.h>
/// Texture
#include <Rendering/Texture.h>

/// Graphics sub-system namespace
namespace Graphics
{
	/// <summary>
	/// <c>DeferredLighting</c> is a deferred lighting server, that do everything what
	/// we need to do lighting and shading on surface.
	/// </summary>
	class DeferredLighting : public Core::ReferenceCount
	{
		IntrusivePtr< Renderer::Texture > pDepthRT; //!< Depth-Stencil RT
		IntrusivePtr< Renderer::Texture > pFirstRT; //!< NormalMaterial RT
		IntrusivePtr< Renderer::Texture > pSecondRT; //!< DiffuseGloss RT

	public:
		/// <summary cref="DeferredLighting::DeferredLighting">
		/// Constructor.
		/// </summary>
		DeferredLighting( void );

		/// <summary cref="DeferredLighting::~DeferredLighting">
		/// Destructor.
		/// </summary>
		virtual ~DeferredLighting( void );

		/// <summary cref="DeferredLighting::Setup">
		/// Setup deferred lighting system.
		/// </summary>
		void Setup( void );

		/// <summary cref="DeferredLighting::Discard">
		/// Discard deferred lighting system.
		/// </summary>
		void Discard( void );
	};
}

#endif /// _NanoDeferredLighting_h_