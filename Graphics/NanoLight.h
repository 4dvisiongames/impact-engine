//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Light definitions
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef _NanoLight_h_
#define _NanoLight_h_

/// Reference counter
#include <INanoRefCount.h>

/// Graphics namespace
namespace Graphics
{	
	/// <summary>
	/// <c>Light</c> is a light type definition.
	/// </summary>
	class Light : public Core::ReferenceCount
	{
	};
}

#endif /// _NanoLight_h_