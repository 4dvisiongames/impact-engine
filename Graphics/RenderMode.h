//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Rendering mode definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _Graphics_StreamSource_h_
#define _Graphics_StreamSource_h_

namespace Graphics
{
	struct RenderModeEnum
	{
		enum List
		{
			Solid,
			Tranparent,
			Translucent
		};
	};

	typedef RenderModeEnum::List RenderMode;
}

#endif /// _Graphics_StreamSource_h_