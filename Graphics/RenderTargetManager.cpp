//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Render targets and buffers managing sub-system.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Render target manager
#include <NanoRenderTargetManager.h>
/// Render target
#include <NanoRenderTarget.h>
/// Depth buffer
#include <NanoDepthBuffer.h>
/// Logger
#include <NanoLogger.h>
/// Exception
#include <NanoException.h>

/// Graphics sub-system namespace
namespace Graphics
{
	/// Implement singleton
	NDKImplementSingleton( Graphics::RenderTargetManager );

	RenderTargetManager::RenderTargetManager( void )
	{
		/// Construct singleton object
		NDKConstructSingleton;
		/// Reserve some space for buffers to speed up application
		this->mDepthBuffers.Reserve( 5 );
		this->mRenderTargets.Reserve( 20 );
	}

	RenderTargetManager::~RenderTargetManager( void )
	{
		/// Destruct singleton object
		NDKDestructSingleton;
	}

	void RenderTargetManager::Open( void )
	{
		/// Empty
	}

	void RenderTargetManager::Close( void )
	{
		/// Check if all our targets are empty
		if( !this->mRenderTargets.IsEmpty() )
		{
			/// Delete contents
			this->mRenderTargets.Clear();
		}

		/// Check if all depth buffers are empty
		if( !this->mDepthBuffers.IsEmpty() )
		{
			/// Delete all contents
			this->mDepthBuffers.Clear();
		}
	}

	void RenderTargetManager::RegisterRenderTarget( const Ptr< Renderer::RenderTarget >& rt )
	{
		/// Some assertions
		NDKAssert( rt->IsValid() );

		/// Try to insert render target to free slot
		for( IndexT idx = 0; idx < this->mRenderTargets.Size(); ++idx )
		{
			/// Get temporary pointer to the object
			Ptr< Renderer::RenderTarget >& temp = this->mRenderTargets[idx];
			/// Check same render targets name
			if( temp != 0x0 )
			{
				if( temp->GetName() == rt->GetName() )
				{
					/// Error
					Lexical::String error;
					/// Format error string 
					error.Format( "Cannot write Render Target into buffer array, because render target with '%s' name exist", rt->GetName().AsCharPtr() );
					/// Throw exception
					NDKThrowException( Exception::ExceptionCodes::ERR_INVALIDPARAMS,
									   error, "RenderTargetManager::AttachRenderTarget( const Ptr< Renderer::RenderTarget >& )" );
				}
			}
			/// If we have free object
			if( temp == 0x0 )
			{
				/// Assign resource with free slot
				temp = rt;
				/// Write index
				return;
			}
		}

		/// Add to the rendering target buffer
		this->mRenderTargets.Append( rt );
	}

	void RenderTargetManager::RegisterDepthBuffer( const Ptr< Renderer::DepthBuffer >& ds )
	{
		/// Some assertions
		NDKAssert( ds->IsValid() );

		/// Try to insert render target to free slot
		for( IndexT idx = 0; idx < this->mDepthBuffers.Size(); ++idx )
		{
			/// Get temporary pointer to the object
			Ptr< Renderer::DepthBuffer >& temp = this->mDepthBuffers[idx];
			/// Check same render targets name
			if( temp != 0x0 )
			{
				if( temp->GetName() == ds->GetName() )
				{
					/// Error
					Lexical::String error;
					/// Format error string 
					error.Format( "Cannot write Render Target into buffer array, because render target with '%s' name exist", ds->GetName().AsCharPtr() );
					/// Throw exception
					NDKThrowException( Exception::ExceptionCodes::ERR_INVALIDPARAMS,
									   error, "RenderTargetManager::AttachRenderTarget( const Ptr< Renderer::RenderTarget >& )" );
				}
			}
			/// If we have free object
			if( temp == 0x0 )
			{
				/// Assign resource with free slot
				temp = ds;
				/// Write index
				return;
			}
		}

		/// Add to depth buffer buffer
		this->mDepthBuffers.Append( ds );
	}

	Ptr< Renderer::RenderTarget >& RenderTargetManager::GetRenderTarget( Lexical::String& RTName )
	{
		/// Try to find render target
		for( IndexT idx = NULL; idx < this->mRenderTargets.Size(); idx++ )
		{
			/// Setup temporary reference to the render target object from buffer
			Ptr< Renderer::RenderTarget >& temp = this->mRenderTargets[idx];
			/// Try to find equlity
			if( temp->GetName() == RTName )
			{
				/// We found it, return
				return temp;
			}
		}

		/// Error code
		Lexical::String error;
		/// Format error code
		error.Format( "Failed to find Render Target resource with name '%s' in render target manager!", RTName.AsCharPtr() );
		/// Write into logger stream
		Utils::Logger::Instance()->Write( error, Utils::LogType::Critical );
		/// Failed to find render target
		return this->mRenderTargets[InvalidIndex];
	}

	Ptr< Renderer::DepthBuffer >& RenderTargetManager::GetDepthBuffer( Lexical::String& RBName )
	{
		/// Try to find render target
		for( IndexT idx = NULL; idx < this->mDepthBuffers.Size(); idx++ )
		{
			/// Setup temporary reference to the render target object from buffer
			Ptr< Renderer::DepthBuffer >& temp = this->mDepthBuffers[idx];
			/// Try to find equlity
			if( temp->GetName() == RBName )
			{
				/// We found it, return
				return temp;
			}
		}

		/// Error code
		Lexical::String error;
		/// Format error code
		error.Format( "Failed to find Render Target resource with name '%s' in render target manager!", RBName.AsCharPtr() );
		/// Write into logger stream
		Utils::Logger::Instance()->Write( error, Utils::LogType::Critical );
		/// Failed to find render target
		return this->mDepthBuffers[InvalidIndex];
	}
}