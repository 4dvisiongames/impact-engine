//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Render targets and buffers managing sub-system.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef _NanoRenderTargetManager_h_
#define _NanoRenderTargetManager_h_

/// Rendering context
#include <Rendering/RenderingContext.h>
/// Singleton
#include <Foundation/Singleton.h>

/// Forward declarations
namespace Renderer
{
	class RenderTarget; //!< Render target/buffer
	class DepthBuffer; //!< Depth buffer declaration
	class Texture;
}

/// Graphics sub-system namespace
namespace Graphics
{
	/// <summary>
	/// <c>RenderTargetManager</c> is a high-level rendering targets and buffer managing system.
	/// Uses for automatic handling of low-level routines.
	/// </summary>
	class RenderTargetManager : public Core::ReferenceCount
	{
		/// Declare singleton
		NDKDeclareSingleton( RenderTargetManager );

		STL::Array< IntrusivePtr< Renderer::RenderTarget > > mRenderTargets; //!< Automated Render Targets
		STL::Array< IntrusivePtr< Renderer::DepthBuffer > > mDepthBuffers; //!< Automated Depth Buffers

	public:
		/// <summary cref="RenderTargetManager::RenderTargetManager">
		/// Constructor.
		/// </summary>
		RenderTargetManager( void );

		/// <summary cref="RenderTargetManager::~RenderTargetManager">
		/// Destructor.
		/// </summary>
		virtual ~RenderTargetManager( void );

		/// <summary cref="RenderTargetManager::Open">
		/// Open manager.
		/// </summary>
		void Open( void );

		/// <summary cref="RenderTargetManager::Close">
		/// Close manager.
		/// </summary>
		void Close( void );

		/// <summary cref="RenderSystem::CreateRenderTarget">
		/// Register rendering object for some events, that could interest THIS
		/// object. Like window resizing, texture resizing, format changing, etc.
		/// </summary>
		/// <param name="name">A reference to the render target object.</param>
		void RegisterRenderTarget( const IntrusivePtr< Renderer::RenderTarget >& rt );

		/// <summary cref="RenderSystem::RegisterForEvents">
		/// Register rendering object for some events, that could interest THIS
		/// object. Like window resizing, texture resizing, format changing, etc.
		/// </summary>
		/// <param name="name">A reference to the depth buffer object.</param>
		void RegisterDepthBuffer( const IntrusivePtr< Renderer::DepthBuffer >& ds );

		/// <summart cref="RenderSystem::GetRenderBuffer">
		/// Get allocated rendering buffer from registry.
		/// </summary>
		IntrusivePtr< Renderer::RenderTarget >& GetRenderTarget( const STL::String< Char8 >& RTName );

		/// <summart cref="RenderSystem::GetDepthBuffer">
		/// Get allocated rendering buffer from registry.
		/// </summary>
		IntrusivePtr< Renderer::DepthBuffer >& GetDepthBuffer( const STL::String< Char8 >& RBName );
	};
}

#endif /// _NanoRenderTargetManager_h_