//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Rendering events.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoRenderingEvent_h_
#define _NanoRenderingEvent_h_

/// Rendering sub-system namespace
namespace Graphics
{
	struct RenderingEvent
	{
		enum List
		{
			ResizeBuffers, //!< Do resize of engine's rendering target
			SkipUpdate, //!< Skip do rendering
		};
	};
}

#endif /// _NanoRenderingEvent_h_