//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: High-Level rendering interface.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef _NanoRenderingInterface_h_
#define _NanoRenderingInterface_h_

/// Pipeline
#include <NanoPipeline.h>
/// Command list
#include <NanoCommandList.h>
/// STL dictionary
#include <STLDictionary.h>
/// Singleton
#include <INanoSingleton.h>

namespace Graphics
{
	/// Forward declaration
	class Material;
	class Model;

	/// <summary>
	/// <c>RenderingInterface</c> is a rendering interface definition.
	/// </summary>
	class RenderingInterface : public Core::ReferenceCount
	{
		/// Declare singleton
		NDKDeclareSingleton( RenderingInterface );

	public:
		/// <summary cref="RenderingInterface::RenderingInterface">
		/// Constructor.
		/// </summary>
		RenderingInterface( void );

		/// <summary cref="RenderingInterface::RenderingInterface">
		/// Destructor.
		/// </summary>
		virtual ~RenderingInterface( void );

		/// <summary cref="RenderingInterface::Open">
		/// Open rendering interface.
		/// </summary>
		void Open( void );

		/// <summary cref="RenderingInterface::Close">
		/// Close rendering interface.
		/// </summary>
		void Close( void );

		/// <summary cref="RenderingInterface::PrepareCompilationThread">
		/// Send all needed data to make batches to draw.
		/// </summary>
		void PrepareCommandList( Ptr< Material >& material, Ptr< Model >& model );

		/// <summary cref="RenderingInterface::DrawDeferred">
		/// Draw deferred command list.
		/// </summary>
		void DrawCommandLists( void );

		/// <summary cref="RenderingInterface::GetThreadLocalPipeline">
		/// Get thread-local pipline to use it. Use it only for 
		/// </summary>
		Ptr< Renderer::Pipeline >& GetThreadLocalPipeline( void );
	};
}

#endif /// _NanoRenderingInterface_h_