//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Texture resource loader definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Texture loader
#include <RML/Textures/TextureLoader.h>

namespace RML
{
	TextureLoaderJob::TextureLoaderJob( void )
	{
		/// Empty
	}

	TextureLoaderJob::~TextureLoaderJob( void )
	{
		/// Empty
	}

	Job* TextureLoaderJob::Execute( void )
	{
		/// Create new file stream
		IntrusivePtr< FileSystem::FileStream > pFileStream = new FileSystem::FileStream;
		IntrusivePtr< FileSystem::StreamReader > pFileReader = new FileSystem::StreamReader;
		/// Setup stream
		pFileStream->SetURI( this->pTextureResource->GetURI() );
		pFileReader->SetInput( pFileStream.Downcast< FileSystem::Stream >() );
		/// Open reader
		pFileReader->Open();

		/// Read all data into buffer
		u32 sizeOfTexture = pFileStream->GetSize();
		s8* buffer = static_cast< s8* >( Memory::scratchHeapAllocator.Allocate( sizeOfTexture ) );
		pFileStream->Read( buffer, sizeOfTexture );
		/// Extract texture size from extension
		u32 rectSize = pFileStream->GetURI().LocalPath().GetFileExtension().As< u32 >();

		/// Create texture
		IntrusivePtr< Renderer::Texture > pTexture = new Renderer::Texture;
		pTexture->SetWidth( rectSize );
		pTexture->SetHeight( rectSize );
		pTexture->SetCPUAccess( Renderer::CPUAccessEnum::CPUWriteAccess );
		/// Setup format
		switch( rectSize )
		{
		case 512:
			if( sizeOfTexture == 174776 ) pTexture->SetRenderFormat( Renderer::TextureFormat::DXT1 );
			else pTexture->SetRenderFormat( Renderer::TextureFormat::DXT5 );
			break;

		case 1024:
			if( sizeOfTexture == 524288 ) pTexture->SetRenderFormat( Renderer::TextureFormat::DXT1 );
			else pTexture->SetRenderFormat( Renderer::TextureFormat::DXT5 );
			break;

		case 2048:
			if( sizeOfTexture == 2097152 ) pTexture->SetRenderFormat( Renderer::TextureFormat::DXT1 );
			else pTexture->SetRenderFormat( Renderer::TextureFormat::DXT5 );
			break;
		}
		
		/// Get thread-local pipeline to upload data concurrently
		Graphics::RenderingInterface::Instance()->GetThreadLocalPipeline()->UploadTexture( pTexture, buffer );
		/// Release created data block
		Memory::scratchHeapAllocator.Free( buffer );

		/// Close reader
		pFileReader->Close();
	}
}