//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Texture resource loader definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef __Graphics__TextureLoader_h__
#define __Graphics__TextureLoader_h__

/// Task
#include <Foundation/Job.h>
/// Resource handler
#include <RML/ResourceHandler.h>
/// Texture resource
#include <RML/Textures/TextureResource.h>

/// Resource management layer namespace
namespace RML
{
	/// <summary>
	/// <c>TextureLoaderJob</c> is a texture loading job executed concurrently.
	/// </summary>
	class TextureLoaderJob : public Jobs::Job
	{
		friend class TextureResource;
		IntrusivePtr< TextureResource > pTextureResource; //!< Pointer to the texture resource

	public:
		/// <summary cref="TextureLoaderJob::TextureLoaderJob">
		/// Constructor.
		/// </summary>
		TextureLoaderJob( void );

		/// <summary cref="TextureLoaderJob::TextureLoaderJob">
		/// Destructor.
		/// </summary>
		virtual ~TextureLoaderJob( void );

		/// <summary cref="TextureLoaderJob::Execute">
		/// Do resource loading task.
		/// </summary>
		Job* Execute( void );
	}; 
}

#endif /// __Graphics__TextureLoader_h__