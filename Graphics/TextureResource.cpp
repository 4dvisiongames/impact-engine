#include <RML/Textures/TextureResource.h>
#include <Foundation/MemoryLocklessFreeList.h>
#include <Foundation/FileStream.h>

namespace RML
{
	__IME_IMPLEMENT_CUSTOM_ALLOCATOR(TextureResource, Memory::LocklessFreeList<TextureResource>);

	void TextureResource::DownloadFromStorage(const IntrusivePtr< FileSystem::BinaryReader >& reader)
	{
		IntrusivePtr< FileSystem::FileStream > fStream = reader->GetInput();

		/// Read all data into buffer
		u32 sizeOfTexture = fStream->GetSize();
		s8* buffer = static_cast< s8* >(Memory::Allocator::Malloc(sizeOfTexture));
		fStream->Read(buffer, sizeOfTexture);
		/// Extract texture size from extension
		u32 rectSize = fStream->GetURI().LocalPath().GetFileExtension().As< u32 >();

		/// Create texture
		Renderer::Texture2DDesc desc;
		IntrusivePtr< Renderer::Texture > pTexture = new Renderer::Texture;
		desc.myWidth = desc.myHeight = rectSize;
		/// Setup format
		switch (rectSize)
		{
		case 512:
			if (sizeOfTexture == 174776) desc.myFormat = Renderer::TextureFormat::DXT1;
			else desc.myFormat = Renderer::TextureFormat::DXT5;
			break;

		case 1024:
			if (sizeOfTexture == 524288) desc.myFormat = Renderer::TextureFormat::DXT1;
			else desc.myFormat = Renderer::TextureFormat::DXT5;
			break;

		case 2048:
			if (sizeOfTexture == 2097152) desc.myFormat = Renderer::TextureFormat::DXT1;
			else desc.myFormat = Renderer::TextureFormat::DXT5;
			break;
		}

		pTexture->Setup(desc, 0, Renderer::UsageFlagsEnum::Static, 0, buffer, sizeOfTexture);
		/// Release created data block
		Memory::Allocator::Free(buffer);
	}
}