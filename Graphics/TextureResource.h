//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Texture resource definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef __RML__Textures__TextureResources_h__
#define __RML__Textures__TextureResources_h__

/// Resource
#include <RML/Resource.h>
/// Texture
#include <Rendering/Texture.h>

/// Resource management layer namespace
namespace RML
{
	/// <summary>
	/// <c>TextureResource</c> is a texture resource definition.
	/// </summary>
	class TextureResource : public Resource
	{
		__IME_DECLARE_CUSTOM_ALLOCATOR(Memory::LocklessFreeList<TextureResource>);
		IntrusivePtr< Renderer::Texture > pTexture; //!< Pointer to the texture

	public:
		/// Creator function for resource object
		static Resource* TextureResourceCreator();

		/// <summary cref="TextureResource::TextureResource">
		/// Constructor.
		/// </summary>
		TextureResource( void );

		/// <summary cref="TextureResource::~TextureResource">
		/// Destructor.
		/// </summary>
		virtual ~TextureResource( void );

		/// <summary cref="Resource::DownloadFromStorage">
		/// Function that implements resource downloading from disk to memory.
		/// </summary>
		virtual void DownloadFromStorage(const IntrusivePtr< FileSystem::BinaryReader >& reader);

		/// <summary cref="Resource::UploadToStorage">
		/// Function that implements resource uploading to storage from memory.
		/// </summary>
		virtual void UploadToStorage(const IntrusivePtr< FileSystem::BinaryReader >& writer);

		/// <summary cref="TextureResource::GetResource">
		/// Get pointer to the resource.
		/// </summary>
		IntrusivePtr< Renderer::Texture >& GetResource( void );
	};

	__IME_INLINED Resource* TextureResource::TextureResourceCreator()
	{
		return new TextureResource();
	}

	__IME_INLINED IntrusivePtr< Renderer::Texture >& TextureResource::GetResource( void )
	{
		return this->pTexture;
	}
}

#endif /// __RML__Textures__TextureResources_h__