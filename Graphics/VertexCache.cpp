//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Vertex cache manager.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Vertex cache
#include <NanoVertexCache.h>
/// Vertex buffer object
#include <NanoVertexBuffer.h>
/// Index buffer object
#include <NanoIndexBuffer.h>
/// Logger
#include <NanoLogger.h>
/// Exceptions
#include <NanoException.h>

#pragma warning( push )
#pragma warning( disable:4172 )

/// Graphics sub-system namespace
namespace Graphics
{
	/// Implement singleton
	NDKImplementSingleton( Graphics::VertexCache );

	VertexCache::VertexCache( void )
	{
		NDKConstructSingleton;
	}

	VertexCache::~VertexCache( void )
	{
		NDKDestructSingleton;
	}

	void VertexCache::Open( void )
	{
		/// Write that we created Vertex Cache manager
		if( this->hasInstance() )
			Utils::Logger::Instance()->Write( "[Frontend] Vertex cache manager has been created!" );
	}

	void VertexCache::Close( void )
	{
		for( IndexT idx = 0; idx < this->mVertexInformation.Size(); ++idx )
		{
			/// Release vertex buffer
			this->mVertexInformation[idx].vboRef->Discard();
			this->mVertexInformation[idx].vboRef = nullptr; //!< Locally nullfied
			/// Release index buffer
			this->mVertexInformation[idx].iboRef->Discard();
			this->mVertexInformation[idx].iboRef = nullptr; //!< Locally nullfied

#if NDKDebug == NDKDebugEngine
			/// Write about releasing
			Lexical::String released;
			released.Format( "[Frontend] Vertex cache information '%s' has been released.", this->mVertexInformation[idx].name.AsCharPtr() );
			Utils::Logger::Instance()->Write( released );
#endif

			/// Erase vertex cache information portion
			this->mVertexInformation.EraseIndex( idx );
		}

		/// Write that we released Vertex Cache manager
		Utils::Logger::Instance()->Write( "[Frontend] Vertex cache manager has been released!" );
	}

	void VertexCache::Add( const VertexInformation& info )
	{
		/// Just add to the buffer
		this->mVertexInformation.Append( info );
	}

	bool VertexCache::Find( const Lexical::String& name )
	{
		for( IndexT idx = NULL; idx < this->mVertexInformation.Size(); ++idx )
		{
			if( this->mVertexInformation[idx].name == name ) return true;
		}

		return false;
	}

	const Ptr< Renderer::VertexBuffer >& VertexCache::GetVBO( const Lexical::String& infoname ) const
	{
		for( IndexT idx = NULL; idx < this->mVertexInformation.Size(); ++idx )
		{
			VertexInformation& temp = this->mVertexInformation[idx];
			if( temp.name == infoname ) 
			{
				return temp.vboRef;
			}
		}

		/// Error code
		Lexical::String error;
		/// Format error code
		error.Format( "Failed to vertex cache resource with name '%s' in vertex cache manager!", infoname.AsCharPtr() );
		/// Write into logger stream
		Utils::Logger::Instance()->Write( error, Utils::LogType::Critical );
		/// Return invalid index which would raise error
		return this->mVertexInformation[InvalidIndex].vboRef;
	}

	const Ptr< Renderer::IndexBuffer >& VertexCache::GetIBO( const Lexical::String& infoname ) const
	{
		for( IndexT idx = NULL; idx < this->mVertexInformation.Size(); ++idx )
		{
			VertexInformation& temp = this->mVertexInformation[idx];
			if( temp.name == infoname ) 
			{
				return temp.iboRef;
			}
		}

		/// Error code
		Lexical::String error;
		/// Format error code
		error.Format( "Failed to vertex cache resource with name '%s' in vertex cache manager!", infoname.AsCharPtr() );
		/// Write into logger stream
		Utils::Logger::Instance()->Write( error, Utils::LogType::Critical );
		/// Return invalid index which would raise error
		return this->mVertexInformation[InvalidIndex].iboRef;
	}
}

#pragma warning( pop )