//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Vertex cache manager.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoVertexCache_h_
#define _NanoVertexCache_h_

/// Reference counter
#include <INanoRefCount.h>
/// Singleton
#include <INanoSingleton.h>
/// STL array
#include <STLArray.h>

/// Forward declaration
namespace Renderer
{
	class VertexBuffer;
	class IndexBuffer;
}

/// Rendering front-end namespace
namespace Graphics
{
	/// <summary>
	/// <c>VertexInformation</c> is a vertex and index buffer's cache information.
	/// </summary>
	struct VertexInformation
	{
		Lexical::String name; //!< Cache name
		Ptr< Renderer::VertexBuffer > vboRef; //!< Vertex buffer reference
		Ptr< Renderer::IndexBuffer > iboRef; //!< Index buffer reference

		/// <summary cref="VertexInfomation::VertexInformation">
		/// Constructor.
		/// </summary>
		VertexInformation( void ) : name( "Empty" ), vboRef( 0 ), iboRef( 0 )
		{
			/// Empty
		}

		/// <summary cref="VertexInformation::VertexInformation">
		/// Copy constructor.
		/// </summary>
		/// <param name="rhs">A reference to the vertex information structure.</param>
		VertexInformation( const VertexInformation& rhs ) : name( rhs.name ), vboRef( rhs.vboRef ), iboRef( rhs.iboRef )
		{
			/// Empty
		}

		/// <summary cref="VertexInformation::~VertexInformation">
		/// Destructor.
		/// </summary>
		~VertexInformation( void )
		{
			/// Empty
		}

		/// <summary cref="VertexInformation::operator=">
		/// Assignment operator.
		/// </summary>
		/// <param name="rhs">A reference to the vertex information structure.</param>
		void operator = ( const VertexInformation& rhs )
		{
			this->name = rhs.name;
			this->vboRef = rhs.vboRef;
			this->iboRef = rhs.iboRef;
		}

		/// <summary cref="VertexInformation::operator==">
		/// Equality checking operator.
		/// </summary>
		/// <param name="rhs">A reference to the vertex information structure.</param>
		/// <returns>True if equals, otherwise false.</reutrns>
		bool operator == ( const VertexInformation& rhs )
		{
			return ((this->name == rhs.name) && (this->vboRef == rhs.vboRef) && (this->iboRef == rhs.iboRef));
		}

		/// <summary cref="VertexInformation::operator!=">
		/// Unequality checking operator.
		/// </summary>
		/// <param name="rhs">A reference to the vertex information structure.</param>
		/// <returns>True if unequals, otherwise false.</reutrns>
		bool operator != ( const VertexInformation& rhs )
		{
			return !(*this == rhs);
		}
	};

	/// <summary>
	/// <c>VertexCache</c> is a vertex cache sub-system definition.
	/// </summary>
	class VertexCache : public Core::ReferenceCount
	{
		/// Declare singleton
		NDKDeclareSingleton( VertexCache );

		STL::Array< VertexInformation > mVertexInformation;

	public:
		/// <summary cref="VertexCache::VertexCache">
		/// Constructor.
		/// </summary>
		VertexCache( void );

		/// <summary cref="VertexCache::~VertexCache">
		/// Destructor.
		/// </summary>
		virtual ~VertexCache( void );

		/// <summary cref="VertexCache::Open">
		/// Open vertex caching server.
		/// </summary>
		void Open( void );

		/// <summary cref="VertexCache::Close">
		/// Close vertex caching server.
		/// </summary>
		void Close( void );

		/// <summary cref="VertexCache::Add">
		/// Add new vertex cache information portion to the array.
		/// </summary>
		/// <param name="info">Vertex cache information structure.</param>
		void Add( const VertexInformation& info );

		/// <summary cref="VertexCache::Find">
		/// Find vertex cache information portion from array.
		/// </summary>
		/// <param name="name">Vertex cache information portion name.</param>
		/// <returns>True if exists, otherwise false.</returns>
		bool Find( const Lexical::String& name );

		/// <summary cref="VertexCache::GetVBO">
		/// Find vertex buffer object definition inside array from vertex
		/// cache information portion.
		/// </summary>
		/// <param name="name">Vertex cache information portion name.</param>
		/// <returns>Reference to the vertex buffer object pointer.</returns>
		const Ptr< Renderer::VertexBuffer >& GetVBO( const Lexical::String& infoname ) const;

		/// <summary cref="VertexCache::GetIBO">
		/// Find index buffer object definition inside array from vertex
		/// cache information portion.
		/// </summary>
		/// <param name="name">Vertex cache information portion name.</param>
		/// <returns>Reference to the index buffer object pointer.</returns>
		const Ptr< Renderer::IndexBuffer >& GetIBO( const Lexical::String& infoname ) const;
	};
}

#endif /// _NanoVertexCache_h_