//============= (C) Copyright 2011, 4DVision Technologic Team. All rights reserved. =============
/// Desc: Stubbed version of engine loader, loads all main engine systems + plugins.
///
/// Author: Pavel Umnikov
//===============================================================================================

/// Stubbed loader
#include <Launcher_PC/StubbedLoader.h>

StubbedLoader TheEngine;

StubbedLoader::StubbedLoader( void )
{
	/// Empty
}

StubbedLoader::~StubbedLoader( void )
{
	/// Empty
}

void StubbedLoader::LoadPlugins( void )
{
	this->mySchedulerServer = new Framework::SchedulerServer;
	this->myGraphicsObject = new Graphics::GraphicsScheduling;

	this->mySchedulerServer->InsertObjectIntoList( this->myGraphicsObject.Cast< Framework::ScheduledObject >() );
}

void StubbedLoader::StartEngine( HWND windowHandle )
{
	__IME_ASSERT( this->mySchedulerServer.IsValid() );
	this->mySchedulerServer->Setup();
}

void StubbedLoader::StopEngine( void )
{
	/// Close physics 
	Physics::PhysicsSystem::Instance()->Close();
	gpPhysics = 0x0;

	/// Release pipeline automatically
	Graphics::GraphicsSystem::Instance()->Close();
	gpGraphics = 0x0;

	/// Exit from the engine
	Core::KernelServer::Instance()->Close();
	_kernel = 0x0;

	/// Uninstall engine
	Core::System::Exit( 0 );

	/// Self-destroy instance;
	delete instance;
}