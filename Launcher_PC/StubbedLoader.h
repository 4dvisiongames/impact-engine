//============= (C) Copyright 2011, 4DVision Technologic Team. All rights reserved. =============
/// Desc: Stubbed version of engine loader, loads all main engine systems + plugins.
///
/// Author: Pavel Umnikov
//===============================================================================================

#ifndef _StubbedLoader_h_
#define _StubbedLoader_h_

/// Kernel
#include <Framework/SchedulerServer.h>
/// Event server
#include <Framework/EventServer.h>
/// Graphics
#include <Graphics/GraphicsScheduling.h>
/// Game
#include <Game/WorldScheduling.h>

class StubbedLoader
{
private:
	IntrusivePtr< Framework::EventServer > myEventServer;
	IntrusivePtr< Framework::SchedulerServer > mySchedulerServer;
	IntrusivePtr< Graphics::GraphicsScheduling > myGraphicsObject; //!< Graphics main sub-system
	IntrusivePtr< Game::WorldScheduledObject > myWorldObject; //!< Game main sub-system

public:
	StubbedLoader( void );
	~StubbedLoader( void );

	void LoadPlugins( void );
	void StartEngine( HWND windowHandle );
	void StopEngine( void );
};

extern StubbedLoader TheEngine;

#endif /// _StubbedLoader_h_