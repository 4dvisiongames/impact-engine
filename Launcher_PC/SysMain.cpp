//-----------------------------------------------------------------------------
// Stub exe for Reality Engine.
//
// Author: Tim Johnson
// Modifications: Miguel Posada (Immersion Software & Graphics)
//					- Some little changes for UNICODE support.
//-----------------------------------------------------------------------------

/// Typed s8
#include <tchar.h>
/// Main STD functions
#include <stdlib.h>
/// STD string
#include <string.h>
/// Windows header
#include <windows.h>
/// Exe's resource
#include "resource.h"

#include <Foundation/System.h>
#include <Foundation/MathVector.h>
#include <Foundation/JobArea.h>
#include <Foundation/JobDispatcher.h>
#include <Foundation/JobServer.h>
#include <Foundation/JobList.h>
#include <Foundation/FileStream.h>
#include <Foundation/StreamWriter.h>
#include <Foundation/StreamReader.h>
#include <Framework/SchedulerServer.h>
#include <Rendering/DisplayAdapter.h>
#include <Rendering/RenderingDevice.h>
#include <Rendering/RenderingContext.h>
#include <Rendering/CommandList.h>
#include <Rendering/DepthBuffer.h>
#include <Rendering/Pipeline.h>
#include <Rendering/ShaderCompiler.h>
#include <Rendering/Texture.h>
#include <Rendering/RenderTarget.h>
#include <Rendering/VertexDeclaration.h>
#include <Rendering/SwapChain.h>


/// Stubbed loader
//#include "StubbedLoader.h"
/// Input events
//#include <Input/InputEventHandler.h>

/*#if defined( DEBUG )
#pragma comment( lib, "foundation_d.lib" )
#pragma comment( lib, "mathlibrary_d.lib" )
#pragma comment( lib, "audio_d.lib" )
#pragma comment( lib, "rendering_d.lib" )
#pragma comment( lib, "graphics_d.lib" )
#pragma comment( lib, "physics_d.lib" )
#else
#pragma comment( lib, "foundation.lib" )
#pragma comment( lib, "mathlibrary.lib" )
#pragma comment( lib, "audio.lib" )
#pragma comment( lib, "rendering.lib" )
#pragma comment( lib, "graphics.lib" )
#pragma comment( lib, "physics.lib" )
#endif*/

/// PhysX API
/*#if defined( DEBUG )
#pragma comment( lib, "PhysX3CHECKED_x86.lib" )
#pragma comment( lib, "PhysX3CommonCHECKED_x86.lib" )
#pragma comment( lib, "PxTaskCHECKED.lib" )
#pragma comment( lib, "PhysXProfileSDKCHECKED.lib" )
#else
#pragma comment( lib, "PhysX3_x86.lib" )
#pragma comment( lib, "PhysX3Common_x86.lib" )
#pragma comment( lib, "PxTask.lib" )
#pragma comment( lib, "PhysXProfileSDK.lib" )
#endif*/

#define NANOENGINE_DEFAULT_CLASS "NanoEngine"

// Global Variables:
HINSTANCE hInst; // current instance
HWND GMainWindowHWND;

IntrusivePtr< Renderer::DisplayAdapter > myDisplayAdapter;
IntrusivePtr< Renderer::RenderingDevice > myRenderingDevice;
IntrusivePtr< Renderer::RenderingContext > myRenderingContext;
IntrusivePtr< Renderer::SwapChain > mySwapChain;

IntrusivePtr< Renderer::RenderTarget > myBackBufferRT;
IntrusivePtr< Renderer::CommandList > myCommandList;

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);

void PreloadEngine( void )
{
	Core::System::Setup();

	myDisplayAdapter = new Renderer::DisplayAdapter();
	myDisplayAdapter->Setup();

	myRenderingDevice = new Renderer::RenderingDevice( myDisplayAdapter );
	myRenderingDevice->Setup();

	myRenderingContext = new Renderer::RenderingContext( myRenderingDevice );
	myRenderingContext->Setup();

	IntrusivePtr< Renderer::Texture > myBackBufferTex = new Renderer::Texture( myRenderingDevice );
	mySwapChain = new Renderer::SwapChain( myRenderingDevice );
	mySwapChain->Setup(GMainWindowHWND, 1280, 720, false);
	mySwapChain->GetSurface( myBackBufferTex );

	myBackBufferRT = new Renderer::RenderTarget( myRenderingDevice );
	myBackBufferRT->Setup( myBackBufferTex );
	myBackBufferTex = nullptr;

	/// Build main precompiled list of rendering
	Renderer::Pipeline myPipeline;
	myCommandList = new Renderer::CommandList( myRenderingDevice );
	myCommandList->Setup( myRenderingContext, true );

	myPipeline.AttachCommandList( myCommandList );
	myPipeline.Begin();
	myPipeline.BindClearState( myBackBufferRT, 0.0f, 0.5f, 0.25f, 0.0f );
	myPipeline.BindRenderTarget( 1, &myBackBufferRT );
	myPipeline.End();
}

void DoRendering( void )
{
	myRenderingDevice->BeginRendering( myRenderingContext );
	myRenderingContext->PlayCommandList( myCommandList );
	myRenderingDevice->EndRendering( mySwapChain, Renderer::SwapEffectEnum::NoSync );

	Sleep( 100 );
}

void ReleaseEngine( void )
{
	myCommandList = nullptr;
	myBackBufferRT = nullptr;
	mySwapChain = nullptr;
	myRenderingContext = nullptr;
	myRenderingDevice = nullptr;
	myDisplayAdapter = nullptr;

	Core::System::Exit(0);
}

int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	//TheEngine.LoadPlugins();

	if( !MyRegisterClass(hInstance) ) return FALSE;
	if (!InitInstance (hInstance, SW_HIDE)) return FALSE;

	//TheEngine.StartEngine( GMainWindowHWND );
	PreloadEngine();
	ShowWindow( GMainWindowHWND,nCmdShow );
	UpdateWindow( GMainWindowHWND );

	// Now we're ready to receive and process Windows messages.
    bool bGotMsg;
    MSG  msg;
    msg.message = WM_NULL;
    PeekMessage( &msg, NULL, 0U, 0U, PM_NOREMOVE );

    while( WM_QUIT != msg.message ) {
        // Use PeekMessage() so we can use idle time to render the scene. 
        bGotMsg = ( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) != 0 );

        if( bGotMsg ) {
			TranslateMessage( &msg );
			DispatchMessage( &msg );
        } else {
			/// Update engine state
			//DoRendering();
        }
    }

	return (int) msg.wParam;
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage are only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_HELIXCORE);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(DKGRAY_BRUSH);//(COLOR_WINDOW+1);
	wcex.lpszMenuName	= (LPCTSTR)IDC_HELIXCORE;
	wcex.lpszClassName	= NANOENGINE_DEFAULT_CLASS;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_HELIXCORE);

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	HWND hWnd;

	hInst = hInstance; // Store instance handle in our global variable

	// Set the window's initial style
	DWORD m_dwWindowStyle = WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX | WS_VISIBLE;

	hWnd = CreateWindow( NANOENGINE_DEFAULT_CLASS, 
						 "NanoEngine Build 5739 '16 november 2012'", 
						 m_dwWindowStyle, 
						 CW_USEDEFAULT, 
						 NULL, 
						 1280, 
						 720, 
						 NULL, 
						 NULL, 
						 hInstance, 
						 NULL);

	GMainWindowHWND = hWnd;

	if (!hWnd) return FALSE;
	return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;

	switch ( message ) {
	case WM_SYSCOMMAND:
		break;

	case WM_ERASEBKGND:
		return FALSE;

	//  Fixes an anomally with the window's caption being all screwed up.
	case WM_GETTEXT:
		return DefWindowProc(hWnd, message, wParam, lParam);

	case WM_CREATE:
		break;

	case WM_PAINT:
		//if( ::IsWindow(GMainWindowHWND) || ::IsIconic(GMainWindowHWND) )
			//Framework::EventServer::Instance()->SendEvent( Graphics::RenderingEvent::SkipUpdate, false, 0, 0 );
		/// Update engine state
		DoRendering();
		break;

	case WM_SIZE:
		{
			s32 width = LOWORD(lParam);
			s32 height = HIWORD(lParam);
			//Framework::EventServer::Instance()->SendEvent( Graphics::RenderingEvent::ResizeBuffers, true, width, height );
		}
		break;

	case WM_DESTROY:
		ShowWindow( GMainWindowHWND, 0 );
		//TheEngine.StopEngine();
		ReleaseEngine();
		PostQuitMessage(0);
		break;
	}

	return DefWindowProc(hWnd, message, wParam, lParam);;
}