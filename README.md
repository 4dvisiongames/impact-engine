# Impact Engine v0.05.579(Unstable) #
This is early pre-alpha version of the engine. Many parts of the engine are not completed, so they are not uploaded here.

** Caution! **
Some parts of the engine(Foundation, Framework, Rendering) can be compiled and used as components. Graphics, sound modules are on their way. Game component will be released as soon as modules are.

## Roadmap

**Version 0.0X:**

* FMOD sound module by default.
* PhysX module for game physics in core game module.
* Complete core game module.
* Complete LeveeEditor for game world construction.

**Version 0.07:**

* Remove all 32-bit code and move only on 64-bit.

**Version 0.06:**

* Add animation support in core game module.
* Add model objects and their streaming/loading code in graphics module.
* Add particles rendering and their physics simulation(possibly GPU physics).
* Add deferred shading and clustered shading extension.

## Build Status

[![Build status](https://ci.appveyor.com/api/projects/status/bitbucket/4dvisiongames/impact-engine?retina=true)](https://ci.appveyor.com/project/pavel-xrayz13/impact-engine)

## Change log

**Version 0.05:**

* Updated AtomicBackoff code.
* Added new strategies of spinning control of backoff.
* Fast semaphore is fully optional and can be used for a weak thread access controlling.
* Sleeping policies for Core::System::Sleep are generalized now.

**Version 0.04:**

* The engine is under BSD license now.
* The job management layer is heavily modified: jobs use their own memory manager instead of dispatcher's, Jobs::Area can accept jobs by the programmer(see Jobs::Area::SpawnOnPool method).
* STL performance improvements and new algorithms.
* Resource management layer is now a part of framework.
* Direct3D11 rendering sub-system has a lot of implements.
* Modules are in early prototype, so the are not fully uploaded.

**Note:**

This reposity is not always up to date and the code of the engine is under heavy development. Many part of the engine might change with new commits and not be backward compatible with earlier case code usage. Please, don't use any parts of the engine in production until first full and stable commit.