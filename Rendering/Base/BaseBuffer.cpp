//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Buffer utils. Shared code for all buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Base buffer definitions
#include <Rendering/Base/BaseBuffer.h>

/// Resource sub-system namespace
namespace Base
{
	BaseBuffer::BaseBuffer( void ) 
		: myDataSize( 0 )
		, myShadowBuffer( nullptr )
		, myCPUAccessMask( 0 )
		, myUsageMask( Renderer::UsageFlagsEnum::Dynamic )
	{
		/// Empty
	}

	BaseBuffer::~BaseBuffer( void )
	{
		/// Empty
	}

	void BaseBuffer::Setup( u32 cpuAccess, Renderer::UsageFlags usage, void* data, SizeT size, bool allocate )
	{
		this->myCPUAccessMask = cpuAccess;
		this->myUsageMask = usage;

		if( allocate ) {
			__IME_ASSERT( size > 0 );
			this->myShadowBuffer = Memory::Allocator::Malloc( size );
			if( data ) {
				__IME_ASSERT( Math::IsAligned( data, 16 ) );
				Memory::LowLevelMemory::CopyMemoryBlock16( data, this->myShadowBuffer, size );
			} else {
				Memory::LowLevelMemory::EraseMemoryBlock( data, size );
			}
		}

		this->myDataSize = size;
	}
}