//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Buffer utils. Shared code for all buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _BaseBuffer_h_
#define _BaseBuffer_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// CPU access
#include <Rendering/CPUAccess.h>
/// Usage
#include <Rendering/UsageType.h>

/// Resource sub-system namespace
namespace Base
{
	/// <summary>
	/// <c>Buffer</c> is a core buffer definition that uses by all other buffers,
	/// that are derived from this class.
	/// </summary>
	class BaseBuffer : public Core::ReferenceCount
	{
	protected:
		static const SizeT theLockedBuffer = 1 << 2;

		u32 myCPUAccessMask; //!< CPU access mask
		Renderer::UsageFlags myUsageMask; //!< Usage mask
		void* myShadowBuffer; //!< Shadow buffer used for necessary things
		SizeT myDataSize; //!< Size of written data

	public:
		/// <summary cref="Buffer::Buffer">
		/// Constructor.
		/// </summary>
		BaseBuffer( void );

		/// <summary cref="Buffer::~Buffer">
		/// Destructor.
		/// </summary>
		virtual ~BaseBuffer( void );

		/// <summary cref="Buffer::Setup">
		/// Setup basic buffer routines.
		/// </summary>
		/// <param name="usage">How would that resource be used by the GPU?</param>
		/// <param name="data">Pointer to the data to upload to the videocard.</param>
		/// <param name="size">Size of data to pass into the buffer.</param>
		void Setup( u32 cpuAccessMask, Renderer::UsageFlags usage, void* data = nullptr, SizeT size = 0, bool allocateShadow = false );

		/// <summary cref="Buffer::Discard">
		/// Discard all basic buffer routines.
		/// </summary>
		void Discard( void );

		/// <summary cref="BaseBuffer::IsMapped">
		/// Do we mapping out buffer?
		/// </summary>
		/// <returns>True if mapping count is higher that zero, otherwise false.</returns>
		bool IsMapped( void ) const;

		/// <summary cref="BaseBuffer::GetSize">
		/// Get buffer size.
		/// </summary>
		/// <returns>Size of current buffer.</returns>
		SizeT GetSize( void ) const;

		/// <summary cref="BaseBuffer::GetCPUAccess">
		/// CPU access status.
		/// </summary>
		bool GetCPUAccess( Renderer::CPUAccessFlags flag );

		/// <summary cref="BaseBuffer::GetUsage">
		/// Get usage hint of buffer.
		/// </summary>
		/// <returns>Type of current buffer.</returns>
		Renderer::UsageFlags GetUsage( void );
	};

	__IME_INLINED bool BaseBuffer::IsMapped( void ) const
	{
		return (this->myDataSize & theLockedBuffer) != 0;
	}

	__IME_INLINED SizeT BaseBuffer::GetSize( void ) const
	{
		SizeT result = (this->myDataSize & ~theLockedBuffer);
		return result;
	}

	__IME_INLINED bool BaseBuffer::GetCPUAccess( Renderer::CPUAccessFlags checkFlag )
	{
		SizeT flag = static_cast<SizeT>(checkFlag);
		return (this->myCPUAccessMask & flag) != 0;
	}

	__IME_INLINED Renderer::UsageFlags BaseBuffer::GetUsage( void )
	{
		return this->myUsageMask;
	}
}

#endif /// _NanoBuffer_h_