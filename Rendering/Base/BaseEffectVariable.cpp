//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Abstractive constant/uniform buffer definitions. Made with template view
///		  to be more efficient and easier to understand by compiler and programmer.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Base effect variable
#include <Foundation/Base/BaseEffectVariable.h>

namespace Base
{
	BaseEffectVariable::~BaseEffectVariable( void )
	{
		/// Empty
	}
}