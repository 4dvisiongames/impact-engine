//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Abstractive effect variable definition for rendering subsystem shader
///		  effect subsystem.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _BaseConstantBuffer_h_
#define _BaseConstantBuffer_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// Effect variable type definitions
#include <Foundation/EffectVariableType.h>

/// Resources sub-system namespace
namespace Base
{
	/// <summary>
	/// <c>ConstantBuffer</c> is an abstraction for engines definition of
	/// declarative constant buffers through template.
	/// Parameter <c>Type</c> holds your constant buffer structure type for
	/// local access of variables.
	/// </summary>
	class BaseEffectVariable : public Core::ReferenceCount
	{
		STL::String< Char8 > mEffectVariableName; //!< Name of the effect variable to link to.
		Renderer::EffectVariableType mEffectVariableType; //!< Type of the effect variable

	public:
		/// <summary cref="BaseEffectVariable::BaseEffectVariable">
		/// Constructor.
		/// </summary>
		/// <param name="type">Type of effect variable.</param>
		BaseEffectVariable( Renderer::EffectVariableType type );

		/// <summary cref="BaseEffectVariable::~BaseEffectVariable">
		/// Destructor.
		/// </summary>
		virtual ~BaseEffectVariable( void );

		/// <summary cref="BaseEffectVariable::Setup">
		/// Setup effect variable.
		/// </summary>
		/// <param name="name">Name of the variable to be linked to effect.</param>
		void Setup( const STL::String< Char8 >& name );
	};

	__IME_INLINED BaseEffectVariable::BaseEffectVariable( Renderer::EffectVariableType type ) : mEffectVariableType( type )
	{
		/// Empty
	}

	__IME_INLINED void BaseEffectVariable::Setup( const STL::String< Char8 >& name )
	{
		/// Assign effect variable names
		this->mEffectVariableName = name;
	}
}

#endif /// _NanoConstantBuffer_h_