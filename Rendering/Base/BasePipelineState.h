//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Pipeline state definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _BasePipelineState_h_
#define _BasePipelineState_h_

#include <Foundation/Shared.h>

namespace Base
{
	struct PipelineStateEnum
	{
		enum List
		{
			//! Main invalid value, used to prevent errors
			InvalidCall = 100,
			ResetPipelineState,
			BindVertexBuffer,
			BindIndexBuffer,
			BindConstatBuffer,
			UnbindVertexBuffer,
			UnbindIndexBuffer,
			UnbindConstantBuffer,
			BindRenderTargets,
			BindShaderProgram,
			UnbindShaderProgram,
			BindRenderState,
			BindClearState,
			UnbindlClearState,
			BindPrimitiveTopology,
			UpdateTexture,
			DispatchComputeDimensions,
			Draw,
			DrawNonIndexed,
			DrawNonIndexedWithInstancing,
			DrawWithInstancing,

			//! Indicator used for additional process in pipeline
			ProcessVertexShader,
			ProcessFragmentShader,
			ProcessGeometryShader,
			ProcessHullShader,
			ProcessDomainShader,
			ProcessComputeShader,

			ProcessDepthStencilState,
			ProcessRasterizerState,
			ProcessBlendState,
			ProcessSamplerState,

			ProcessRenderTarget,
			ProcessDepthStencil
		};
	};
	typedef PipelineStateEnum::List PipelineState;

	struct PipelineEntryDef 
	{
		intptr_t primaryCode;
		intptr_t secondaryCode;
		char myAdditionalData[80]; //!< Maximum is 128 bytes as 16x doubles

		PipelineEntryDef(void) : primaryCode(PipelineStateEnum::InvalidCall), secondaryCode(0) {}
	};
}

#endif /// _BasePipelineState_h_
