//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Generalized rendering systems.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Render system
#include <Rendering/Base/BaseRenderSystem.h>

namespace Base
{
	BaseRenderSystem::BaseRenderSystem( void )
	{
		/// Empty
	}

	BaseRenderSystem::~BaseRenderSystem( void )
	{
		/// Empty
	}
}