//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Generalized rendering systems.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _BaseRenderSystem_h_
#define _BaseRenderSystem_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// Rendering output
#include <Rendering/RenderingOutput.h>
/// STL Array
#include <Foundation/STLArray.h>

/// Rendering sub-system namespace
namespace Base
{
	/// <summary>
	/// <c>RenderSystem</c> is an abstracted rendering system for easier use
	/// of API power.
	/// </summary>
	class BaseRenderSystem : public Core::ReferenceCount
	{
	protected:
		Renderer::RenderingOutput mRenderingOutput; //!< Rendering output information

	public:
		/// <summary cref="BaseRenderSystem::BaseRenderSystsem">
		/// Constructor with singleton construction.
		/// </summary>
		BaseRenderSystem( void );

		/// <summary cref="BaseRenderSystem::~BaseRenderSystem">
		/// Destructor with singleton destruction.
		/// </summary>
		virtual ~BaseRenderSystem( void );

		/// <summary cref="BaseRenderSystem::SetRenderingOutput">
		/// Setup rendering output information.
		/// </summary>
		/// <param name="outinfo">Output information of rendering window.</param>
		void SetRenderingOutput( const Renderer::RenderingOutput& outinfo );

		/// <summary cref="BaseRenderSystem::GetRenderingOutput">
		/// Get rendering output information.
		/// </summary>
		/// <returns>Output information of rendering window.</returns>
		const Renderer::RenderingOutput& GetRenderingOutput( void ) const;
	};

	__IME_INLINED void BaseRenderSystem::SetRenderingOutput( const Renderer::RenderingOutput& outinfo )
	{
		/// Assign these ones
		this->mRenderingOutput = outinfo;
	}

	__IME_INLINED const Renderer::RenderingOutput& BaseRenderSystem::GetRenderingOutput( void ) const
	{
		/// Get rendering output structure
		return this->mRenderingOutput;
	}
}

#endif /// _NanoRenderSystem_h_