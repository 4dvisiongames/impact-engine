//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Basic texture definitions used in engine.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Base texture
#include <Rendering/Base/BaseTexture.h>

/// Base rendering sub-system namespace
namespace Base
{
	BaseTexture::BaseTexture( void ) 
		: myWidth( 1 )
		, myHeight( 0 )
		, myDepth( 0 )
		, mySlices( 1 )
		, myMSAALevel( 1 )
		, myMSAAQuality( 0 )
		, myFormat( Renderer::TextureFormat::RGBA8 )
		, myType( Renderer::TextureTypeEnum::Tex1D )
	{
		/// Empty
	}			

	BaseTexture::~BaseTexture( void )
	{
		/// Empty
	}

	void BaseTexture::Setup( u32 width, u32 height, u32 depth, u16 slices, u32 msaaSamples, u32 msaaQuality, Renderer::TextureFormat format, Renderer::TextureType type, bool isRendereable )
	{
		/// Setup texture information
		this->myWidth = width;
		this->myHeight = height;
		this->myDepth = depth;
		this->mySlices = slices;
		this->myMSAALevel = msaaSamples;
		this->myMSAAQuality = msaaQuality;
		this->myFormat = format;
		this->myType = static_cast< SizeT >( type );
		if( isRendereable )
			this->myType |= Renderer::TextureTypeEnum::TexRendereable;
	}
}