//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Basic texture definitions used in engine.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _BaseTexture_h_
#define _BaseTexture_h_

/// Reference count
#include <Foundation/RefCount.h>
/// Texture flags
#include <Rendering/TextureFlags.h>
/// CPU Access flags
#include <Rendering/CPUAccess.h>
/// Texture format
#include <Rendering/TextureFormat.h>
/// Usage type
#include <Rendering/UsageType.h>
/// Texture types
#include <Rendering/TextureType.h>

/// Base rendering sub-system namespace
namespace Base
{
	/// <summary>
	/// <c>BaseTexture</c> is a basic texture definition.
	/// </summary>
	class BaseTexture : public Core::ReferenceCount
	{
	protected:
		u32 myWidth; //!< Texture width
		u32 myHeight; //!< Texture height
		u32 myDepth; //!< Texture depth
		u16 mySlices; //! Array size
		u32 myMSAALevel; //!< MSAA level
		u32 myMSAAQuality; //!< MSAA quality
		Renderer::TextureFormat myFormat; //!< Current render target format
		SizeT myType; //!< Type of current texture

	public:
		/// <summary cref="BaseTexture::BaseTexture">
		/// Constructor.
		/// </summary>
		BaseTexture( void );

		/// <summary cref="BaseTexture::~BaseTexture">
		/// Destructor.
		/// </summary>
		virtual ~BaseTexture( void );

		/// <summary cref="BaseTexture::Setup">
		/// Setup current texture.
		/// </summary>
		void Setup( u32 width, u32 height, u32 depth, u16 slices, u32 msaaSamples, u32 msaaQuality, Renderer::TextureFormat format, Renderer::TextureType type, bool isRendereable );

		/// <summary cref="BaseTexture::IsDepthTexture">
		/// Checks if current texture is depth format.
		/// </summary>
		bool IsDepthTexture( void ) const;

		/// <summary cref="BaseTexture::IsCompressed">
		/// Checks if current texture is compressed format.
		/// </summary>
		bool IsCompressed( void ) const;

		/// <summary cref="BaseTexture::IsRendereable">
		/// Check if current texture is render target compatible
		/// </summary>
		bool IsRendereable( void ) const;

		/// <summary cref="BaseTexture::GetWidth">
		/// Get width of current render target.
		/// </summary>
		/// <returns>Current width.</returns>
		u32 GetWidth( void ) const;

		/// <summary cref="BaseTexture::GetHeigth">
		/// Get heigth of current render target.
		/// </summary>
		/// <returns>Current heigth.</returns>
		u32 GetHeight( void ) const;

		/// <summary cref="BaseTexture::GetDepth">
		/// Get depth of current render target.
		/// </summary>
		/// <returns>Current depth.</returns>
		u32 GetDepth( void ) const;

		/// <summary cref="BaseTexture::GetSamples">
		/// </summary>
		u32 GetSamples( void ) const;

		/// <summary cref="BaseTexture::GetSamplesQuality">
		/// </summary>
		u32 GetSamplesQuality( void ) const;

		/// <summary cref="BaseTexture::GetFormat">
		/// Get rendering format of current render target.
		/// </summary>
		/// <returns>Current render target.</returns>
		Renderer::TextureFormat GetFormat( void ) const;

		/// <summary cref="Texture::GetResourceType">
		/// Get resource type of texture.
		/// </summary>
		/// <returns>Resource type of current texture.</returns>
		Renderer::TextureType GetTextureType( void ) const;
	};

	__IME_INLINED bool BaseTexture::IsDepthTexture( void ) const
	{
		return (this->myFormat == Renderer::TextureFormatEnum::Depth24Stencil8) ||
			(this->myFormat == Renderer::TextureFormatEnum::Depth32) ||
			(this->myFormat == Renderer::TextureFormatEnum::DepthAccessiable) ||
			(this->myFormat == Renderer::TextureFormatEnum::DepthStencilAccessiable);
	}

	__IME_INLINED bool BaseTexture::IsCompressed( void ) const
	{
		return (this->myFormat == Renderer::TextureFormatEnum::DXT1) ||
			(this->myFormat == Renderer::TextureFormatEnum::DXT3) ||
			(this->myFormat == Renderer::TextureFormatEnum::DXT5);
	}

	__IME_INLINED bool BaseTexture::IsRendereable( void ) const
	{
		return (this->myType & Renderer::TextureTypeEnum::TexRendereable) != 0;
	}

	__IME_INLINED u32 BaseTexture::GetWidth( void ) const
	{
		return this->myWidth;
	}

	__IME_INLINED u32 BaseTexture::GetHeight( void ) const
	{
		return this->myHeight;
	}

	__IME_INLINED u32 BaseTexture::GetDepth( void ) const
	{
		return this->myDepth;
	}

	__IME_INLINED u32 BaseTexture::GetSamples( void ) const
	{
		return this->myMSAALevel;
	}

	__IME_INLINED u32 BaseTexture::GetSamplesQuality( void ) const
	{
		return this->myMSAAQuality;
	}

	__IME_INLINED Renderer::TextureType BaseTexture::GetTextureType( void ) const
	{
		SizeT original = (this->myType & ~Renderer::TextureTypeEnum::TexRendereable);
		return static_cast<Renderer::TextureType>(original);
	}

	__IME_INLINED Renderer::TextureFormat BaseTexture::GetFormat( void ) const
	{
		return this->myFormat;
	}
}

#endif /// _BaseTexture_h_