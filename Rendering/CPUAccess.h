//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: CPU access flags.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoCPUAccess_h_
#define _NanoCPUAccess_h_

/// STL string
#include <Foundation/STLString.h>

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>TextureFlags</c> is a texture flag definitions.
	/// </summary>
	struct CPUAccessFlagsEnum
	{
		enum List
		{
			WriteAccess = (1 << 1), //!< CPU can write to this buffer
			ReadAccess = (1 << 2), //!< CPU can read from this buffer
		};
	};
	typedef CPUAccessFlagsEnum::List CPUAccessFlags;
}

#endif /// _NanoCPUAccess_h_