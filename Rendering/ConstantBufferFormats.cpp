/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Rendering/ConstantBufferFormats.h>

namespace Renderer
{
	static u32 toSize[ ConstantBufferFormats::MaxCount ] = {
		0, // Invalid
		sizeof( float ) * 16, // Matrix4x4
		sizeof( float ) * 12, // Matrix4x3
		sizeof( float ) * 8, // Matrix4x2
		sizeof( float ) * 9, // Matrix3x3
		sizeof( float ) * 6, // Matrix3x2
		sizeof( float ) * 4, // Float4
		sizeof( float ) * 3, // Float3
		sizeof( float ) * 2, // Float2
		sizeof( float ), // Float
		sizeof( s32 ) * 4, // Int4
		sizeof( s32 ) * 3, // Int3
		sizeof( s32 ) * 2, // Int2
		sizeof( s32 ) // Int
	};

	static STL::String< Char8 > toString[ ConstantBufferFormats::MaxCount ] =
	{
		"Invalid"
		"Matrix4x4",
		"Matrix4x3",
		"Matrix4x2",
		"Matrix3x3",
		"Matrix3x2",
		"Float4",
		"Float3",
		"Float2",
		"Float",
		"Int4",
		"Int3",
		"Int2",
		"Int"
	};

	u32 ConstantBufferFormatConverter::ToSize( const ConstantBufferFormats::List fmt )
	{
		__IME_ASSERT( fmt < ConstantBufferFormats::MaxCount );
		return toSize[ fmt ];
	}

	STL::String< Char8 > ConstantBufferFormatConverter::ToString( const ConstantBufferFormats::List fmt )
	{
		__IME_ASSERT( fmt < ConstantBufferFormats::MaxCount );
		return toString[ fmt ];
	}
}