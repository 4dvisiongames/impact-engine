/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#ifndef __Rendering__ConstantBufferFormat_h__
#define __Rendering__ConstantBufferFormat_h__

/// STL string
#include <Foundation/STLString.h>

namespace Renderer
{
	struct ConstantBufferFormats
	{
		enum List
		{
			Invalid,
			Matrix4x4,
			Matrix4x3,
			Matrix4x2,
			Matrix3x3,
			Matrix3x2,
			Float4,
			Float3,
			Float2,
			Float,
			Int4,
			Int3,
			Int2,
			Int,

			MaxCount,
		};
	};

	class ConstantBufferFormatConverter
	{
	public:
		static u32 ToSize( const ConstantBufferFormats::List fmt );
		static STL::String< Char8 > ToString( const ConstantBufferFormats::List fmt );
	};
}

#endif /// __Rendering__ConstantBufferFormat_h__