//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Constant buffer element definition.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#include <Foundation/STLString.h>
#include <Rendering/ConstantBufferFormats.h>

namespace Renderer
{
	/// <summary>
	/// <c>ConstantBufferLayout</c> is a layout definition for single element of constant buffer.
	/// </summary>
	struct ConstantBufferLayout
	{
		STL::String< Char8 > myElementName;
		s32 myElementIndex;
		Renderer::ConstantBufferFormats::List myElementFormat;
	};
}