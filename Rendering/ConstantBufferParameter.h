//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Constant buffer parameter accessor for constant buffer objects.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

namespace Renderer
{
	template< typename T >
	class ConstantBufferParameter
	{
		T* myParamPtr;

	public:
		ConstantBufferParameter( void );
		ConstantBufferParameter( const ConstantBufferParameter& rhs );

		ConstantBufferParameter& operator = ( const ConstantBufferParameter& rhs );
		ConstantBufferParameter& operator = ( T* ptr );

		void Set( T variable );
		T Get( void ) const;
	};

	template< typename T >
	ConstantBufferParameter< T >::ConstantBufferParameter( void ) : myParamPtr( nullptr )
	{
	}

	template< typename T >
	ConstantBufferParameter< T >::ConstantBufferParameter( const ConstantBufferParameter< T >& rhs ) : myParamPtr( rhs.myParamPtr )
	{
	}

	template< typename T >
	ConstantBufferParameter< T >& ConstantBufferParameter< T >::operator = ( const ConstantBufferParameter< T >& rhs )
	{
		if( rhs.myParamPtr != nullptr )
		{
			this->myParamPtr = rhs.myParamPtr;
		}

		return *this;
	}

	template< typename T >
	ConstantBufferParameter< T >& ConstantBufferParameter< T >::operator = ( T* ptr )
	{
		if( ptr != nullptr )
		{
			this->myParamPtr = ptr;
		}

		return *this;
	}

	template< typename T >
	void ConstantBufferParameter< T >::Set( T parameter )
	{
		__IME_ASSERT( myParamPtr );
		*this->myParamPtr = parameter;
	}

	template< typename T >
	T ConstantBufferParameter< T >::Get( void ) const
	{
		__IME_ASSERT( myParamPtr );
		T result = *this->myParamPtr;
		return result;
	}
}