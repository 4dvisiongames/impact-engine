//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Depth(-stencil) definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoDepthBuffer_h_
#define _NanoDepthBuffer_h_

#if defined( __IME_PLATFORM_WIN64 )
/// Direct3D11 depth buffer
#include <Rendering/Direct3D11/D3D11DepthBuffer.h>
#else
#error Depth Buffer is not implemented on this platform!
#endif

#endif /// _NanoDepthBuffer_h_