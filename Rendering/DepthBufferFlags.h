//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Depth buffer flags.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoDepthBufferFlags_h_
#define _NanoDepthBufferFlags_h_

/// STL String
#include <Foundation/STLString.h>

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>DepthBufferFlags</c> is a depth(-stencil) buffer flags.
	/// </summary>
	struct DepthBufferFlags
	{
		enum List
		{
			Depth, //!< Clear depth buffer
			Stencil, //!< Clear stencil values
		};
	};
}

#endif /// _NanoDepthBufferFlags_h_