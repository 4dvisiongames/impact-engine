//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Direct3D11 command lists defintions.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Command list
#include <Rendering/Direct3D11/D3D11CommandList.h>
/// Direct3D11 rendering device
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>

/// Base pipeline
#include <Rendering/Base/BasePipelineState.h>

namespace Renderer
{
	CommandList::CommandList( const IntrusivePtr< Renderer::RenderingDevice >& device ) 
		: myDevice( device )
		, myCommandBuffer( nullptr )
	{
		/// Empty
	}

	CommandList::~CommandList( void )
	{
		if( this->IsValid() )
			this->Discard();
	}

	void CommandList::Setup( const IntrusivePtr< Renderer::RenderingContext >& rhs, bool isBundle )
	{
		__IME_ASSERT( !this->IsValid() );

		this->myCommandBuffer = new FileSystem::MemoryStream;
		this->myCommandBuffer->SetAccessMode( FileSystem::AccessMode::ReadWrite );
		this->myCommandBuffer->SetAccessPattern( FileSystem::AccessPattern::Sequential );
		this->myCommandBuffer->SetSize( sizeof( Base::PipelineEntryDef ) * 64 ); //!< That must be enough for command buffer
		this->myCommandBuffer->Open();

		this->myBundleState = isBundle;
	}

	void CommandList::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		if( this->myCommandBuffer->IsOpen() )
			this->myCommandBuffer->Close();

		this->myCommandBuffer = nullptr;
		this->myDevice = nullptr;
	}
}