//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Front-End command list.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _NanoCommandList_h_
#define _NanoCommandList_h_

/// Rendering context
#include <Rendering/RenderingContext.h>
/// Memory stream object
#include <Foundation/MemoryStream.h>

// Forward declaration
struct ID3D11CommandList;

namespace Renderer
{
	/// <summary>
	/// <c>CommandList</c> is a Direct3D 11 command list implementation.
	/// </summary>
	class CommandList : public Core::ReferenceCount
	{
		/// Make them friends
		friend class Pipeline;
		friend class RenderingContext;

		bool myBundleState; //!< Check if this is immutable command list(like Direct3D12 bundles)
		IntrusivePtr< Renderer::RenderingDevice > myDevice;
		IntrusivePtr< FileSystem::MemoryStream > myCommandBuffer;

	public:
		/// <summary cref="CommandList::CommandList">
		/// Constructor.
		/// </summary>
		CommandList( const IntrusivePtr< Renderer::RenderingDevice >& device );

		/// <summary cref="CommandList::~CommandList">
		/// Destructor.
		/// </summary>
		virtual ~CommandList( void );

		/// <summary cref="CommandList::Setup">
		/// Setup command list.
		/// </summary>
		void Setup( const IntrusivePtr< Renderer::RenderingContext >& rhs, bool isBundle = false );

		/// <summary cref="CommandList::Discard">
		/// Release command list.
		/// </summary>
		void Discard( void );

		/// <summary cref="CommandList::IsValid">
		/// Validate command list
		/// </summary>
		bool IsValid( void ) const;
	};

	__IME_INLINED bool CommandList::IsValid( void ) const
	{
		/// TODO: make somthing better than just check for pointer validness
		return this->myCommandBuffer.IsValid();
	}
}

#endif /// _NanoCommandList_h_