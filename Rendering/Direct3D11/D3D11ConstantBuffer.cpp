//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Direct3D11 constant/uniform buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

/// D3D11 constant buffer
#include <Rendering/Direct3D11/D3D11ConstantBuffer.h>
/// D3D11 rendering device
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// CPU access patterns
#include <Rendering/CPUAccess.h>
/// STL Atomic string
#include <Foundation/StringAtom.h>

// Direct3D11
#include <D3D11.h>

namespace Renderer
{
	ConstantBuffer::ConstantBuffer( const IntrusivePtr< RenderingDevice >& device ) 
		: myD3D11CB( nullptr )
		, myDevice( device )
	{
		this->myLayout.Reserve( 16 ); //!< Reserve memory for 16 layout objects
	}

	ConstantBuffer::~ConstantBuffer( void )
	{
		/// Release constant buffer
		if( this->IsValid() ) {
			this->Discard();
		}
	}

	void ConstantBuffer::Setup( const STL::Array< ConstantBufferLayout >& layouts )
	{
		__IME_ASSERT( !layouts.IsEmpty() );

		SizeT byteWidth = 0;
		ID3D11Device* device = this->myDevice->myD3D11Device;

		this->myLayout = layouts;
		for( SizeT idx = 0; idx < STL::CountOf(this->myLayout); ++idx )
			byteWidth += ConstantBufferFormatConverter::ToSize( this->myLayout[idx].myElementFormat );

		/// Base buffer setup
		Base::BaseBuffer::Setup( Renderer::CPUAccessFlagsEnum::WriteAccess, 
								 Renderer::UsageFlags::Dynamic,
								 nullptr,
								 byteWidth,
								 true );

		/// Define structure for buffer
		D3D11_BUFFER_DESC desc;
		desc.ByteWidth = static_cast< UINT >( byteWidth );
		desc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		desc.CPUAccessFlags |= D3D11_CPU_ACCESS_READ; //!< Read from CPU
		desc.CPUAccessFlags |= D3D11_CPU_ACCESS_WRITE; //!< Write from CPU

		/// If failed to create, then write error
		if( FAILED( device->CreateBuffer( &desc, NULL, &this->myD3D11CB ) ) ) {
			NDKThrowException( Debug::EExceptionCodes::RendererError,
							   "[D3D11] Failed to create Direct3D constant buffer!",
							   "D3D11ConstantBuffer::Setup( const STL::Array< ConstantBufferLayout >& )" );
		}
	}

	void* ConstantBuffer::GetRawParameter( const STL::String< Char8 >& str )
	{
		__IME_ASSERT( this->myShadowBuffer );

		bool found = false;
		u32 offset = 0;
		for (SizeT idx = 0; idx < STL::CountOf(this->myLayout); ++idx) {
			if( this->myLayout[idx].myElementName == str ) {
				found = true;
				break;
			}
			offset += ConstantBufferFormatConverter::ToSize( this->myLayout[idx].myElementFormat );
		}

		if( found ) {
			char* result = static_cast< char* >( this->myShadowBuffer );
			if( offset > 0 ) {
				result += offset;
			}
			return result;
		}

		return nullptr;
	}

	void ConstantBuffer::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );
		__IME_ASSERT( !this->IsMapped() );

		this->myD3D11CB->Release(); 
		this->myD3D11CB = nullptr;

		this->myLayout.Clear();
		this->myDevice = nullptr;
	}
}