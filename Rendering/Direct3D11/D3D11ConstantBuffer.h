//============= (C) Copyright 2014, PARANOIA Team. All rights reserved. =============
/// Desc: Direct3D11 constant/uniform buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef _D3D11ConstantBuffer_h_
#define _D3D11ConstantBuffer_h_

/// Constant buffer
#include <Rendering/Base/BaseBuffer.h>
#include <Rendering/ConstantBufferParameter.h>
#include <Rendering/ConstantBufferLayout.h>
#include <Foundation/STLString.h>

// Forward declaration
struct ID3D11Buffer;

/// Rendering subsystem namespace
namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	/// <summary>
	/// <c>ConstantBuffer</c> is a constant buffer definition for OpenGL 4.3 rendering system.
	/// </summary>
	class ConstantBuffer : public Base::BaseBuffer
	{
		friend class Pipeline; //!< This is necesseary

		IntrusivePtr< RenderingDevice > myDevice;
		STL::Array< ConstantBufferLayout > myLayout;
		ID3D11Buffer* myD3D11CB;

		void* GetRawParameter( const STL::String< Char8 >& str );

	public:
		/// <summary cref="D3D11ConstantBuffer::D3D11ConstantBuffer">
		/// Constructor.
		/// </summary>
		ConstantBuffer( const IntrusivePtr< RenderingDevice >& device );

		/// <summary cref="D3D11ConstantBuffer::~D3D11ConstantBuffer">
		/// Destructor.
		/// </summary>
		virtual ~ConstantBuffer( void );

		/// <summary cref="D3D11ConstantBuffer::Finalize">
		/// Finalize constant buffer creation in Direct3D11 rendering sub-system.
		/// </summary>
		/// <param name="layout">Pointer to the cbuffer layout array.</param>
		/// <param name="num">Number of layout entries.</param>
		void Setup( const STL::Array< ConstantBufferLayout >& layouts );

		/// <summary cref="D3D11ConstantBuffer::ReleaseBuffer">
		/// Release buffer with data.
		/// </summary>
		void Discard( void );

		/// <summary cref="D3D11ConstantBuffer::IsValid">
		/// </summary>
		bool IsValid( void ) const;

		/// <summary cref="D3D11ConstantBuffer::Update">
		/// Uploads all data from CPU shadow buffer into GPU memory.
		/// </summary>
		void Update( void );

		/// <summary cref="D3D11ConstantBuffer::GetParameter">
		/// Parameter getter helper function.
		/// </summary>
		/// <remarks>
		/// Since you've got ConstantBufferParameter, then you just have to work with
		/// that variable handler to change or read current parameters.
		/// </remarks>
		template< typename T >
		ConstantBufferParameter< T > GetParameter( const STL::String< Char8 >& str );
	};

	__IME_INLINED bool ConstantBuffer::IsValid( void ) const
	{
		return this->myD3D11CB != nullptr;
	}

	template< typename T >
	__IME_INLINED ConstantBufferParameter< T > ConstantBuffer::GetParameter( const STL::String< Char8 >& str )
	{
		return static_cast< T* >( this->GetRawParameter( str ) );
	}
}

#endif /// _GLConstantBuffer_h_