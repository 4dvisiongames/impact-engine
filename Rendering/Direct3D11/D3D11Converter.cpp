//============== (C) Copyright 2014, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 conversion routines.
///
/// Author: Pavel Umnikov
//=====================================================================================

#include <D3D11.h>
#include <Rendering/Direct3D11/D3D11Converter.h>

namespace Direct3D11
{
	D3D_PRIMITIVE_TOPOLOGY D3D11Converter::ToD3D( const Renderer::PrimitiveTopology::List topology )
	{
		switch( topology )
		{
		case Renderer::PrimitiveTopology::TriangleList: return D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
		case Renderer::PrimitiveTopology::TriangleStrip: return D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
		default: return D3D_PRIMITIVE_TOPOLOGY_UNDEFINED;
		}
	}

	DXGI_FORMAT D3D11Converter::ToD3D( const Renderer::IndexBufferPrecision precision )
	{
		switch( precision )
		{
		case Renderer::IndexBufferPrecision::Idx16: return DXGI_FORMAT_R16_FLOAT;
		case Renderer::IndexBufferPrecision::Idx32: return DXGI_FORMAT_R32_FLOAT;
		default: return DXGI_FORMAT_UNKNOWN;
		}
	}

	D3D11_CULL_MODE ConvertCullMode( Renderer::CullMode mode )
	{
		switch( mode )
		{
		case Renderer::CullModeTypeList::Back: return D3D11_CULL_BACK;
		case Renderer::CullModeTypeList::Front: return D3D11_CULL_FRONT;
		case Renderer::CullModeTypeList::None: 
		default: return D3D11_CULL_NONE;
		}
	}

	D3D11_FILL_MODE ConvertFillMode( Renderer::FillMode mode )
	{
		switch( mode )
		{
		case Renderer::FillModeTypeList::Wireframe: return D3D11_FILL_WIREFRAME;
		case Renderer::FillModeTypeList::Solid: return D3D11_FILL_SOLID;
		default: return D3D11_FILL_SOLID;
		}
	}

	D3D11_STENCIL_OP ConvertStencilOperaton( Renderer::StencilOperation mode )
	{
		switch( mode )
		{
		case Renderer::StencilOperationTypeList::Decrease: return D3D11_STENCIL_OP_DECR;
		case Renderer::StencilOperationTypeList::DecreaseSaturate: return D3D11_STENCIL_OP_DECR_SAT;
		case Renderer::StencilOperationTypeList::Increase: return D3D11_STENCIL_OP_INCR;
		case Renderer::StencilOperationTypeList::IncreaseSaturate: return D3D11_STENCIL_OP_INCR_SAT;
		case Renderer::StencilOperationTypeList::Invert: return D3D11_STENCIL_OP_INVERT;
		case Renderer::StencilOperationTypeList::Replace: return D3D11_STENCIL_OP_REPLACE;
		case Renderer::StencilOperationTypeList::Zero: return D3D11_STENCIL_OP_ZERO;
		case Renderer::StencilOperationTypeList::Keep: 
		default: return D3D11_STENCIL_OP_KEEP;
		}
	}

	D3D11_COMPARISON_FUNC ConvertComparisonFunction( Renderer::Comparison mode )
	{
		switch( mode )
		{
		case Renderer::ComparisonTypeList::Equal: return D3D11_COMPARISON_EQUAL;
		case Renderer::ComparisonTypeList::Greater: return D3D11_COMPARISON_GREATER;
		case Renderer::ComparisonTypeList::GreaterEqual: return D3D11_COMPARISON_GREATER_EQUAL;
		case Renderer::ComparisonTypeList::Less: return D3D11_COMPARISON_LESS;
		case Renderer::ComparisonTypeList::LessEqual: return D3D11_COMPARISON_LESS_EQUAL;
		case Renderer::ComparisonTypeList::Never: return D3D11_COMPARISON_NEVER;
		case Renderer::ComparisonTypeList::NotEqual: return D3D11_COMPARISON_NOT_EQUAL;
		case Renderer::ComparisonTypeList::Always:
		default: return D3D11_COMPARISON_ALWAYS;
		}
	}

	D3D11_DEPTH_WRITE_MASK ConvertDepthWriteMask( Renderer::DepthWriteMask mode )
	{
		switch( mode )
		{
		case Renderer::DepthWriteMaskTypeList::DisableZWrite: return D3D11_DEPTH_WRITE_MASK_ZERO;
		case Renderer::DepthWriteMaskTypeList::EnableZWrite:
		default: return D3D11_DEPTH_WRITE_MASK_ALL;
		}
	}

	D3D11_BLEND_OP ConvertBlendOperation( Renderer::BlendOperation mode )
	{
		switch( mode )
		{
		case Renderer::BlendOperationTypeList::Maximum: return D3D11_BLEND_OP_MAX;
		case Renderer::BlendOperationTypeList::Minimum: return D3D11_BLEND_OP_MIN;
		case Renderer::BlendOperationTypeList::ReverseSubtractive: return D3D11_BLEND_OP_REV_SUBTRACT;
		case Renderer::BlendOperationTypeList::Subtractive: return D3D11_BLEND_OP_SUBTRACT;
		case Renderer::BlendOperationTypeList::Additive: 
		default: return D3D11_BLEND_OP_ADD;
		}
	}

	D3D11_BLEND ConvertBlending( Renderer::Blend mode )
	{
		switch( mode )
		{
		case Renderer::BlendTypeList::BlendFactor: return D3D11_BLEND_BLEND_FACTOR;
		case Renderer::BlendTypeList::DestinationAlpha: return D3D11_BLEND_DEST_ALPHA;
		case Renderer::BlendTypeList::DestinationColour: return D3D11_BLEND_DEST_COLOR;
		case Renderer::BlendTypeList::InvBlendFactor: return D3D11_BLEND_INV_BLEND_FACTOR;
		case Renderer::BlendTypeList::InvDestinationAlpha: return D3D11_BLEND_INV_DEST_ALPHA;
		case Renderer::BlendTypeList::InvDestinationColour: return D3D11_BLEND_INV_DEST_COLOR;
		case Renderer::BlendTypeList::InvSource1Alpha: return D3D11_BLEND_INV_SRC1_ALPHA;
		case Renderer::BlendTypeList::InvSource1Colour: return D3D11_BLEND_INV_SRC1_COLOR;
		case Renderer::BlendTypeList::InvSourceColour: return D3D11_BLEND_INV_SRC_COLOR;
		case Renderer::BlendTypeList::InvSourceAlpha: return D3D11_BLEND_INV_SRC_ALPHA;
		case Renderer::BlendTypeList::One: return D3D11_BLEND_ONE;
		case Renderer::BlendTypeList::Source1Alpha: return D3D11_BLEND_SRC1_ALPHA;
		case Renderer::BlendTypeList::Source1Colour: return D3D11_BLEND_SRC1_COLOR;
		case Renderer::BlendTypeList::SourceAlpha: return D3D11_BLEND_SRC_ALPHA;
		case Renderer::BlendTypeList::SourceAlphaSaturate: return D3D11_BLEND_SRC_ALPHA_SAT;
		case Renderer::BlendTypeList::SourceColour: return D3D11_BLEND_SRC_COLOR;
		case Renderer::BlendTypeList::Zero:
		default: return D3D11_BLEND_ZERO;
		}
	}

	D3D11_FILTER ConvertFilter( Renderer::Filter mode )
	{
		switch( mode )
		{
		case Renderer::FilterTypeList::CompareMinLinear_MagMipPoint: return D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_MIP_POINT;
		case Renderer::FilterTypeList::CompareMinLinear_MagPoint_MipLinear: return D3D11_FILTER_COMPARISON_MIN_LINEAR_MAG_POINT_MIP_LINEAR;
		case Renderer::FilterTypeList::CompareMinMagLinear_MipPoint: return D3D11_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
		case Renderer::FilterTypeList::CompareMinMagMipLinear: return D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
		case Renderer::FilterTypeList::CompareMinMagMipPoint: return D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
		case Renderer::FilterTypeList::CompareMinMagPoint_MipLinear: return D3D11_FILTER_COMPARISON_MIN_MAG_POINT_MIP_LINEAR;
		case Renderer::FilterTypeList::CompareMinPoint_MagLinear_MipPoint: return D3D11_FILTER_COMPARISON_MIN_POINT_MAG_LINEAR_MIP_POINT;
		case Renderer::FilterTypeList::CompareMinPoint_MagMipLinear: return D3D11_FILTER_COMPARISON_MIN_POINT_MAG_MIP_LINEAR;
		case Renderer::FilterTypeList::MinLinear_MagMipPoint: return D3D11_FILTER_MIN_LINEAR_MAG_MIP_POINT;
		case Renderer::FilterTypeList::MinLinear_MagPoint_MipLinear: return D3D11_FILTER_MIN_LINEAR_MAG_POINT_MIP_LINEAR;
		case Renderer::FilterTypeList::MinMagLinear_MipPoint: return D3D11_FILTER_MIN_MAG_LINEAR_MIP_POINT;
		case Renderer::FilterTypeList::MinMagMipLinear: return D3D11_FILTER_MIN_MAG_MIP_LINEAR;
		case Renderer::FilterTypeList::MinMagMipPoint: return D3D11_FILTER_MIN_MAG_MIP_POINT;
		case Renderer::FilterTypeList::MinMagPoint_MipLinear: return D3D11_FILTER_MIN_MAG_POINT_MIP_LINEAR;
		case Renderer::FilterTypeList::MinPoint_MagLinear_MipPoint: return D3D11_FILTER_MIN_POINT_MAG_LINEAR_MIP_POINT;
		case Renderer::FilterTypeList::MinPoint_MagMipLinear: return D3D11_FILTER_MIN_POINT_MAG_MIP_LINEAR;
		case Renderer::FilterTypeList::CompareAnisotropic: return D3D11_FILTER_COMPARISON_ANISOTROPIC;
		case Renderer::FilterTypeList::Anisotropic: 
		default: return D3D11_FILTER_ANISOTROPIC;
		}
	}

	D3D11_TEXTURE_ADDRESS_MODE ConvertTextureAddressMode( Renderer::TextureAddressMode mode )
	{
		switch( mode )
		{
		case Renderer::TextureAddressModeTypeList::Border: return D3D11_TEXTURE_ADDRESS_BORDER;
		case Renderer::TextureAddressModeTypeList::Clamp: return D3D11_TEXTURE_ADDRESS_CLAMP;
		case Renderer::TextureAddressModeTypeList::Mirror: return D3D11_TEXTURE_ADDRESS_MIRROR;
		case Renderer::TextureAddressModeTypeList::MirrorOnce: return D3D11_TEXTURE_ADDRESS_MIRROR_ONCE;
		case Renderer::TextureAddressModeTypeList::Wrap:
		default: return D3D11_TEXTURE_ADDRESS_WRAP;
		}
	}

	D3D11_DEPTH_STENCILOP_DESC ConvertDepthStencilOperation( Renderer::DepthStencilOperationDesc& desc )
	{
		D3D11_DEPTH_STENCILOP_DESC d3d11Desc;
		Memory::LowLevelMemory::EraseMemoryBlock( &d3d11Desc, sizeof( D3D11_DEPTH_STENCILOP_DESC ) );

		d3d11Desc.StencilDepthFailOp = ConvertStencilOperaton( desc.stencilDepthFailOp );
		d3d11Desc.StencilFailOp = ConvertStencilOperaton( desc.stencilFailOp );
		d3d11Desc.StencilFunc = ConvertComparisonFunction( desc.stencilFunction );
		d3d11Desc.StencilPassOp = ConvertStencilOperaton( desc.stencilPassOp );

		return d3d11Desc;
	}

	D3D11_RENDER_TARGET_BLEND_DESC ConvertRTBlend( Renderer::RenderTargetBlendDesc& desc )
	{
		D3D11_RENDER_TARGET_BLEND_DESC d3d11Desc;
		Memory::LowLevelMemory::EraseMemoryBlock( &d3d11Desc, sizeof( D3D11_RENDER_TARGET_BLEND_DESC ) );

		d3d11Desc.BlendEnable = static_cast< BOOL >( desc.bEnableBlend );
		d3d11Desc.BlendOp = ConvertBlendOperation( desc.blendOp );
		d3d11Desc.BlendOpAlpha = ConvertBlendOperation( desc.blendOpAlpha );
		d3d11Desc.DestBlend = ConvertBlending( desc.destBlend );
		d3d11Desc.DestBlendAlpha = ConvertBlending( desc.destBlendAlpha );
		d3d11Desc.SrcBlend = ConvertBlending( desc.srcBlend );
		d3d11Desc.SrcBlendAlpha = ConvertBlending( desc.srcBlendAlpha );
		d3d11Desc.RenderTargetWriteMask = static_cast< u8 >( desc.uiMask );

		return d3d11Desc;
	}

	D3D11_RASTERIZER_DESC D3D11Converter::ToD3D( Renderer::RasterizerStateDesc& desc )
	{
		D3D11_RASTERIZER_DESC d3d11Desc; //!< Description
		Memory::LowLevelMemory::EraseMemoryBlock( &d3d11Desc, sizeof( D3D11_RASTERIZER_DESC ) );

		d3d11Desc.AntialiasedLineEnable = static_cast< BOOL >( desc.bAALineEnabled );
		d3d11Desc.CullMode = ConvertCullMode( desc.cullMode );
		d3d11Desc.DepthBias = desc.iDepthBias;
		d3d11Desc.DepthBiasClamp = desc.fDepthBiasClamp;
		d3d11Desc.DepthClipEnable = static_cast< BOOL >( desc.bDepthClipEnable );
		d3d11Desc.FillMode = ConvertFillMode( desc.fillMode );
		d3d11Desc.FrontCounterClockwise = static_cast< BOOL >( desc.bFrontFacing );
		d3d11Desc.MultisampleEnable = static_cast< BOOL >( desc.bMSAAEnable );
		d3d11Desc.ScissorEnable = static_cast< BOOL >( desc.bScissorEnable );
		d3d11Desc.SlopeScaledDepthBias = desc.fSlopeScaledDepthBias;

		return d3d11Desc;
	}

	D3D11_DEPTH_STENCIL_DESC D3D11Converter::ToD3D( Renderer::DepthStencilStateDesc& desc )
	{
		D3D11_DEPTH_STENCIL_DESC d3d11Desc;
		Memory::LowLevelMemory::EraseMemoryBlock( &d3d11Desc, sizeof( D3D11_DEPTH_STENCIL_DESC ) );

		d3d11Desc.BackFace = ConvertDepthStencilOperation( desc.backFace );
		d3d11Desc.FrontFace = ConvertDepthStencilOperation( desc.frontFace );
		d3d11Desc.DepthEnable = static_cast< BOOL >( desc.bEnableDepth );
		d3d11Desc.DepthFunc = ConvertComparisonFunction( desc.depthCompare );
		d3d11Desc.DepthWriteMask = ConvertDepthWriteMask( desc.depthWriteMask );
		d3d11Desc.StencilEnable = static_cast< BOOL >( desc.bEnableStencil );
		d3d11Desc.StencilReadMask = static_cast< u8 >( desc.stencilReadMask );
		d3d11Desc.StencilWriteMask = static_cast< u8 >( desc.stencilWriteMask );

		return d3d11Desc;
	}

	D3D11_BLEND_DESC D3D11Converter::ToD3D( Renderer::BlendStateDesc& desc )
	{
		D3D11_BLEND_DESC d3d11Desc;
		Memory::LowLevelMemory::EraseMemoryBlock( &d3d11Desc, sizeof( D3D11_BLEND_DESC ) );

		d3d11Desc.AlphaToCoverageEnable = desc.bEnableAlphaToCoverage;
		d3d11Desc.IndependentBlendEnable = desc.bIndependentBlendEnable;

		for( SizeT idx = 0; idx < 8; ++idx ) {
			d3d11Desc.RenderTarget[idx] = ConvertRTBlend( desc.RenderTarget[idx] );
		}

		return d3d11Desc;
	}

	D3D11_SAMPLER_DESC D3D11Converter::ToD3D( Renderer::SamplerStateDesc& desc )
	{
		D3D11_SAMPLER_DESC d3d11Desc;
		Memory::LowLevelMemory::EraseMemoryBlock( &d3d11Desc, sizeof( D3D11_SAMPLER_DESC ) );

		d3d11Desc.Filter = ConvertFilter( desc.mSampleFilter );
		d3d11Desc.AddressU = ConvertTextureAddressMode( desc.mAddressU );
		d3d11Desc.AddressV = ConvertTextureAddressMode( desc.mAddressV );
		d3d11Desc.AddressW = ConvertTextureAddressMode( desc.mAddressW );
		d3d11Desc.MipLODBias = desc.fMipLODBias;
		d3d11Desc.MaxAnisotropy = desc.iMaxAnisotropy;
		d3d11Desc.ComparisonFunc = ConvertComparisonFunction( desc.mComparisonFunc );
		d3d11Desc.MaxLOD = desc.fMaxLOD;
		d3d11Desc.MinLOD = desc.fMinLOD;
		
		for( SizeT idx = 0; idx < 4; ++idx ) {
			d3d11Desc.BorderColor[idx] = desc.fBorderColor[idx];
		}

		return d3d11Desc;
	}

	DXGI_FORMAT D3D11Converter::ToD3D( Renderer::TextureFormat format )
	{
		switch( format )
		{
		case Renderer::TextureFormat::RGBA8: return DXGI_FORMAT_R8G8B8A8_UNORM;
		case Renderer::TextureFormat::RGB8E5: return DXGI_FORMAT_R9G9B9E5_SHAREDEXP;
		case Renderer::TextureFormat::R32F: return DXGI_FORMAT_R32_FLOAT;
		case Renderer::TextureFormat::RG16F: return DXGI_FORMAT_R16G16_FLOAT;
		case Renderer::TextureFormat::RGBA16F: return DXGI_FORMAT_R16G16B16A16_FLOAT;
		case Renderer::TextureFormat::Depth32: return DXGI_FORMAT_D32_FLOAT;
		case Renderer::TextureFormat::Depth24Stencil8: return DXGI_FORMAT_D24_UNORM_S8_UINT;
		case Renderer::TextureFormat::DepthAccessiable: return DXGI_FORMAT_R32_TYPELESS;
		case Renderer::TextureFormat::DepthStencilAccessiable: return DXGI_FORMAT_R24G8_TYPELESS;
		default: return DXGI_FORMAT_UNKNOWN;
		}
	}

	DXGI_FORMAT D3D11Converter::ToD3DSRV( Renderer::TextureFormat format ) {
		switch( format ) {
			case Renderer::TextureFormatEnum::Depth32:
			case Renderer::TextureFormatEnum::DepthAccessiable: 
				return DXGI_FORMAT_R32_FLOAT;

			case Renderer::TextureFormatEnum::Depth24Stencil8:
			case Renderer::TextureFormatEnum::DepthStencilAccessiable: 
				return DXGI_FORMAT_R24_UNORM_X8_TYPELESS;

			case Renderer::TextureFormatEnum::DXT1: return DXGI_FORMAT_BC1_UNORM;
			default: return DXGI_FORMAT_UNKNOWN;
		}
	}

	DXGI_FORMAT D3D11Converter::ToD3DDepthBuffer( Renderer::TextureFormat format )
	{
		switch( format )
		{
		case Renderer::TextureFormatEnum::DepthAccessiable: return DXGI_FORMAT_D32_FLOAT;
		case Renderer::TextureFormatEnum::DepthStencilAccessiable: return DXGI_FORMAT_D24_UNORM_S8_UINT;
		default: return DXGI_FORMAT_UNKNOWN;
		}
	}

	Renderer::UsageFlags D3D11Converter::ToEngine( const D3D11_USAGE type )
	{
		switch( type ) {
		case D3D11_USAGE_IMMUTABLE:
			return Renderer::UsageFlagsEnum::Static;
		case D3D11_USAGE_DYNAMIC:
			return Renderer::UsageFlagsEnum::Dynamic;
		case D3D11_USAGE_DEFAULT:
		default:
			return Renderer::UsageFlagsEnum::Default;
		};
	}

	D3D11_USAGE D3D11Converter::ToD3D( const Renderer::UsageFlags type )
	{
		switch( type ) {
		case Renderer::UsageFlagsEnum::Static:
			return D3D11_USAGE_IMMUTABLE;
		case Renderer::UsageFlagsEnum::Dynamic:
			return D3D11_USAGE_DYNAMIC;
		case Renderer::UsageFlagsEnum::Default:
		default:
			return D3D11_USAGE_DEFAULT;
		};
	}

	DXGI_FORMAT D3D11Converter::ToD3D( const Renderer::VertexElementFormat::List format )
	{
		switch( format )
		{
		case Renderer::VertexElementFormat::Float_Single: return DXGI_FORMAT_R32_FLOAT;
		case Renderer::VertexElementFormat::Float_XY: return DXGI_FORMAT_R32G32_FLOAT;
		case Renderer::VertexElementFormat::Float_XYZW: return DXGI_FORMAT_R32G32B32A32_FLOAT;
		case Renderer::VertexElementFormat::Half_Single: return DXGI_FORMAT_R16_FLOAT;
		case Renderer::VertexElementFormat::Half_XY: return DXGI_FORMAT_R16G16_FLOAT;
		case Renderer::VertexElementFormat::Half_XYZW: return DXGI_FORMAT_R16G16B16A16_FLOAT;
		case Renderer::VertexElementFormat::Integer_Single: return DXGI_FORMAT_R32_UINT;
		case Renderer::VertexElementFormat::Integer_XY: return DXGI_FORMAT_R32G32_UINT;
		case Renderer::VertexElementFormat::Integer_XYZW: return DXGI_FORMAT_R32G32B32A32_UINT;
		default: return DXGI_FORMAT_UNKNOWN;
		}
	}

	Renderer::VertexElementFormat::List D3D11Converter::ToEngine( const DXGI_FORMAT format )
	{
		switch( format )
		{
		case DXGI_FORMAT_R32_FLOAT: return Renderer::VertexElementFormat::Float_Single;
		case DXGI_FORMAT_R32G32_FLOAT: return Renderer::VertexElementFormat::Float_XY;
		case DXGI_FORMAT_R32G32B32A32_FLOAT: return Renderer::VertexElementFormat::Float_XYZW;
		case DXGI_FORMAT_R16_FLOAT: return Renderer::VertexElementFormat::Half_Single;
		case DXGI_FORMAT_R16G16_FLOAT: return Renderer::VertexElementFormat::Half_XY;
		case DXGI_FORMAT_R16G16B16A16_FLOAT: return Renderer::VertexElementFormat::Half_XYZW;
		case DXGI_FORMAT_R32_UINT: return Renderer::VertexElementFormat::Integer_Single;
		case DXGI_FORMAT_R32G32_UINT: return Renderer::VertexElementFormat::Integer_XY;
		case DXGI_FORMAT_R32G32B32A32_UINT: return Renderer::VertexElementFormat::Integer_XYZW;
		default: return Renderer::VertexElementFormat::Unknown;
		}
	}

	D3D11_INPUT_CLASSIFICATION D3D11Converter::ToD3D( const Renderer::VertexElementGroup::List group )
	{
		switch( group )
		{
		case Renderer::VertexElementGroup::PerInstance: return D3D11_INPUT_PER_INSTANCE_DATA;
		case Renderer::VertexElementGroup::PerVertex: return D3D11_INPUT_PER_VERTEX_DATA;
		default: return D3D11_INPUT_PER_VERTEX_DATA;
		}
	}

	Renderer::TextureFormat D3D11Converter::ToEngineTexture( const DXGI_FORMAT format )
	{
		switch( format )
		{
		case DXGI_FORMAT_R8G8B8A8_UNORM:
			return Renderer::TextureFormatEnum::RGBA8;

		default:
			return Renderer::TextureFormatEnum::Unknown;
		}
	}
}