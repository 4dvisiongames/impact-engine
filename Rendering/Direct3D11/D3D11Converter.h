//============== (C) Copyright 2014, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 conversion routines.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef __d3d11_h__
#error "D3D11.h must be included before this header!"
#endif /// _d3d11_h_

#pragma once

/// Index buffer precision
#include <Rendering/IndexBufferPrecision.h>
/// Primitive topology
#include <Rendering/PrimitiveTopology.h>
/// Render states descriptions
#include <Rendering/RenderStateDesc.h>
/// Render target format
#include <Rendering/TextureFormat.h>
/// Usage flags
#include <Rendering/UsageType.h>
/// Vertex element format
#include <Rendering/VertexElementFormat.h>
/// Vertex element group
#include <Rendering/VertexElementGroup.h>

/// Direct3D11 rendering sub-system namespace
namespace Direct3D11
{
	class D3D11Converter
	{
	public:
		static D3D_PRIMITIVE_TOPOLOGY ToD3D( const Renderer::PrimitiveTopology::List topology );
		static DXGI_FORMAT ToD3D( const Renderer::IndexBufferPrecision precision );

		/// <summary cref="D3D11RenderStateConverter::ToD3D11">
		/// Converts Rasterizer description of NanoEngine to Direct3D11-compatible description.
		/// </summary>
		static D3D11_RASTERIZER_DESC ToD3D( Renderer::RasterizerStateDesc& desc );

		/// <summary cref="D3D11RenderStateConverter::ToD3D11">
		/// Converts Depth-Stencil description of NanoEngine to Direct3D11-compatible description.
		/// </summary>
		static D3D11_DEPTH_STENCIL_DESC ToD3D( Renderer::DepthStencilStateDesc& desc );

		/// <summary cref="D3D11RenderStateConverter::ToD3D11">
		/// Converts Blending description of NanoEngine to Direct3D11-compatible description.
		/// </summary>
		static D3D11_BLEND_DESC ToD3D( Renderer::BlendStateDesc& desc );

		/// <summary cref="D3D11RenderStateConverter::ToD3D11">
		/// Converts Sampler description of NanoEngine to Direct3D11-compatible description.
		/// </summary>
		static D3D11_SAMPLER_DESC ToD3D( Renderer::SamplerStateDesc& desc );

		/// <summary cref="D3D11TextureFormatConverter::ToDXGI">
		/// Converts NanoEngine Format to DXGI Format.
		/// </summary>
		/// <param name="format">Input format.</param>
		/// <returns>DXGI Format.</returns>
		static DXGI_FORMAT ToD3D( Renderer::TextureFormat format );

		/// <summary cref="D3D11Converter::ToD3DSRV">
		/// </summary>
		static DXGI_FORMAT ToD3DSRV( Renderer::TextureFormat format );

		/// <summary cref="D3D11TextureFormatConverter::ToDepthBuffer">
		/// Convert specified typeless format to the depth buffer analogue.
		/// </summary>
		/// <param name="format">Input format.</param>
		/// <returns>DXGI Format.</returns>
		static DXGI_FORMAT ToD3DDepthBuffer( Renderer::TextureFormat format );

		static Renderer::UsageFlags ToEngine( const D3D11_USAGE type );
		static D3D11_USAGE ToD3D( const Renderer::UsageFlags type );

		static Renderer::TextureFormat ToEngineTexture( const DXGI_FORMAT format );
		static DXGI_FORMAT ToD3D( const Renderer::VertexElementFormat::List format );
		static Renderer::VertexElementFormat::List ToEngine( const DXGI_FORMAT format );

		static D3D11_INPUT_CLASSIFICATION ToD3D( const Renderer::VertexElementGroup::List group );
	};
}