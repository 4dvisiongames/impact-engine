//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 depth(-stencil) definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#include <d3d11.h>

/// Direct3D11 depth buffer
#include <Rendering/Direct3D11/D3D11DepthBuffer.h>
/// Render system
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// Direct3D11 texture format
#include <Rendering/Direct3D11/D3D11Converter.h>

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	DepthBuffer::DepthBuffer( const IntrusivePtr< Renderer::RenderingDevice >& device ) 
		: myDepthTexture( nullptr )
		, myDevice( device )
		, myDSV( nullptr )
	{
		/// Empty
	}

	DepthBuffer::~DepthBuffer( void )
	{
		/// Release depth buffer contents
		if( this->IsValid() ) {
			this->Discard();
		}
	}

	void DepthBuffer::Setup( const IntrusivePtr< Renderer::Texture >& tex, bool change )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid() ); //!< Validate pointer
		__IME_ASSERT( this->myDevice->IsValid() ); //!< Validate device

		/// Result
		HRESULT hr = S_FALSE;
		/// Define inputs
		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		/// Pre-erase Render Target View to ensure that everything is good
		Memory::LowLevelMemory::EraseMemoryBlock( &dsvDesc, sizeof( dsvDesc ) );
		/// Define Render Target View format from input texture
		dsvDesc.Format = Direct3D11::D3D11Converter::ToD3DDepthBuffer( this->myDepthTexture->GetFormat() );
		/// Define Render Target View shader resource type
		switch( this->myDepthTexture->GetTextureType() )
		{
			case Renderer::TextureTypeEnum::Tex1D:
				dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE1D;
				dsvDesc.Texture1D.MipSlice = 0;
				break;

			case Renderer::TextureTypeEnum::Tex2D:
				if( this->myDepthTexture->GetSamples() > 1 )
					dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
				else
					dsvDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;

				dsvDesc.Texture2D.MipSlice = 0;
				break;

			default:
				__IME_ASSERT( false );
		}

		/// We don't need access prevention flags
		dsvDesc.Flags = NULL;

		/// Create render target view
		ID3D11Device* device = this->myDevice->myD3D11Device;
		hr = device->CreateDepthStencilView( this->myDepthTexture->myTexture, &dsvDesc, &this->myDSV );
		if( FAILED( hr ) )
			NDKThrowException( Debug::EExceptionCodes::RendererError,
							   this->myDevice->GetLastError().AsCharPtr(), 
							   "D3D11DepthBuffer::Setup( void )" );
	}

	void DepthBuffer::Update( void )
	{
		/// Result
		HRESULT hr = S_FALSE;
		/// Define inputs
		D3D11_DEPTH_STENCIL_VIEW_DESC dsvDesc;
		this->myDSV->GetDesc( &dsvDesc );
		/// Release old render target view
		this->myDSV->Release(); this->myDSV = nullptr;

		if( !this->myDepthTexture->IsDepthTexture() ) {
			NDKThrowException( Debug::EExceptionCodes::RendererError,
							   "Failed to select proper format for depth buffer!",
							   "D3D11RenderTarget::UpdateRenderTarget( void )" );
		}

		/// Define Render Target View format from input texture
		DXGI_FORMAT format = Direct3D11::D3D11Converter::ToD3D( this->myDepthTexture->GetFormat() );
		if( dsvDesc.Format != format )
			dsvDesc.Format = format;

		/// Check for MSAA changing
		if( this->myDepthTexture->GetTextureType() == Renderer::TextureTypeEnum::Tex2D ) {
			D3D11_DSV_DIMENSION viewDimension = (this->myDepthTexture->GetSamples( ) > 1) ? D3D11_DSV_DIMENSION_TEXTURE2DMS : D3D11_DSV_DIMENSION_TEXTURE2D;
			if( dsvDesc.ViewDimension != viewDimension )
				dsvDesc.ViewDimension = viewDimension;
		}

		/// Create render target view
		ID3D11Device* device = this->myDevice->myD3D11Device;
		hr = device->CreateDepthStencilView( this->myDepthTexture->myTexture, &dsvDesc, &this->myDSV );
		if( FAILED( hr ) )
			NDKThrowException( Debug::EExceptionCodes::RendererError,
								"Failed to update/recreate Render Target for offsceen buffer!",
								"D3D11RenderTarget::UpdateRenderTarget( void )" );
	}

	void DepthBuffer::Discard( void )
	{
		/// Release depth stencil view
		if( this->myDSV ) { this->myDSV->Release(); this->myDSV = nullptr; }
		this->myDepthTexture = nullptr;
		this->myDevice = nullptr;
	}
}