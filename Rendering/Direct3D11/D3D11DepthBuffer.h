//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 depth(-stencil) definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _D3D11DepthBuffer_h_
#define _D3D11DepthBuffer_h_

/// Direct3D11 texture
#include <Rendering/Direct3D11/D3D11Texture.h>
/// Texture format
#include <Rendering/TextureFormat.h>
/// Depth buffer flags
#include <Rendering/DepthBufferFlags.h>

/// Forward declaration
struct ID3D11DepthStencilView;

/// Base rendering sub-system namespace
namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	/// <summary>
	/// <c>DepthBuffer</c> is a Direct3D11 depth buffer implementation.
	/// </summary>
	class DepthBuffer : public Core::ReferenceCount
	{
		/// Make them friends to be happy!
		friend class Pipeline;
		friend class RenderingContext;

		IntrusivePtr< Renderer::Texture > myDepthTexture;
		IntrusivePtr< Renderer::RenderingDevice > myDevice;
		ID3D11DepthStencilView* myDSV; //!< Depth-stencil view

	public:
		/// <summary cref="DepthBuffer::DepthBuffer">
		/// Default constructor.
		/// </summary>
		DepthBuffer( const IntrusivePtr< Renderer::RenderingDevice >& device );

		/// <summary cref="DepthBuffer::~DepthBuffer">
		/// Destructor.
		/// </summary>
		virtual ~DepthBuffer( void );

		/// <summary cref="DepthBuffer::Setup">
		/// Setup depth buffer.
		/// </summary>
		void Setup( const IntrusivePtr< Renderer::Texture >& tex, bool change = false );

		/// <summary cref="DepthBuffer::Update"
		/// </summary>
		void Update( void );

		/// <summary cref="DepthBuffer::Discard">
		/// Discard depth buffer.
		/// </summary>
		void Discard( void );

		/// <summary cref="DepthBuffer::IsValid">
		/// Validate depth buffer.
		/// </summary>
		bool IsValid( void ) const;
	};

	__IME_INLINED bool DepthBuffer::IsValid( void ) const
	{
		return this->myDSV != nullptr;
	}
}

#endif /// _D3D11DepthBuffer_h_