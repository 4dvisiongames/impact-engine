//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: DXGI Display and Adapter event handler.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// STL string
#include <Foundation/STLString.h>
/// STL converter
#include <Foundation/StringConverter.h>
/// Direct3D11 display adapter
#include <Rendering/Direct3D11/D3D11DisplayAdapter.h>

#include <D3D11.h>

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	static IDXGIFactory1* theDXGIFactory = nullptr;

	DisplayAdapter::DisplayAdapter( void )
		: pAdapter( nullptr )
		, myFactoryPtr( nullptr )
		, iAdapterOrdinal( 0 )
		, iOutputCount( 0 )
		, myValidation( false )
	{
	}

	DisplayAdapter::~DisplayAdapter( void )
	{
		if( this->IsValid() )
			this->Discard();
	}

	void DisplayAdapter::Setup( void )
	{
		__IME_ASSERT( !this->IsValid() );

		HRESULT hr = S_FALSE; 
		/// Create DXGI 1.1 Factory
		if( !theDXGIFactory ) {
			Win32::COMObjectAndRawPtr<IDXGIFactory1> dxgiObjectAndPtr;
			hr = ::CreateDXGIFactory1( __uuidof(IDXGIFactory1), &dxgiObjectAndPtr.myRawPtr );

			if( FAILED( hr ) ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   "[DXGI] Failed to create DXGI Factory with version 1.1 or 1.0!",
								   "D3D11DisplayAdapter::Open( void )" );
			}

			theDXGIFactory = dxgiObjectAndPtr;
		}

		__IME_ASSERT( theDXGIFactory );
		this->myFactoryPtr = theDXGIFactory;
		this->myFactoryPtr->AddRef();

		/// Enumerate all outputs
		hr = this->myFactoryPtr->EnumAdapters1( static_cast< UINT >( iAdapterOrdinal ), &pAdapter );

		if( FAILED( hr ) ) {
			NDKThrowException( Debug::EExceptionCodes::RendererError,
							   "[DXGI] Failed to enumerate adapters using!",
							   "D3D11DisplayAdapter::Open( void )" );
		} else {
			/// Get adapter information
			DXGI_ADAPTER_DESC adapterDesc;
			pAdapter->GetDesc( &adapterDesc );
			Debug::Messaging::Send( Debug::EMessageType::Normal, 
									STL::String< Char8 >::VA( "[DXGI] Current output adapter is: %s", STL::StringConverter::WideToUTF8( adapterDesc.Description ).AsCharPtr( ) ) );
		}

		/// Output counting
		/*for( iOutputCount = 0; ; ++iOutputCount )
		{
			 IDXGIOutput* pOutput = nullptr;
			 if( FAILED( pAdapter->EnumOutputs( static_cast< UINT >( iOutputCount ), &pOutput ) ) )
				 break;

			 if( pOutput ) {
				 this->pOutputs.Append( pOutput );
			 }
		};

		if( iOutputCount > 1 ) {
			Debug::Messaging::Send( Debug::EMessageType::Normal, 
									STL::String< Char8 >::VA( "[DXGI] Multiple output devices(displays) are attached[%i].", iOutputCount ) );
		}*/

		this->myValidation = true;
	}

	void DisplayAdapter::Discard( void )
	{
		/// Utility reference
		UINT references = NULL;

		/// Release output array
		for( SizeT i = NULL; i < iOutputCount; i++ ) {
			if( this->pOutputs[i] ) {
				this->pOutputs[i]->Release();
				this->pOutputs[i] = nullptr;
			}
		}

		/// Release adapter
		if( this->pAdapter ) {
			this->pAdapter->Release();
		}

		references = this->myFactoryPtr->Release();
		if( references == 1 ) {
			/// Release DXGI, if there is nothing to do here
			if( theDXGIFactory ) {
				references = theDXGIFactory->Release();
				if( references == 0 )
					theDXGIFactory = nullptr;
			}
		}

		this->myValidation = false;
	}

	SizeT DisplayAdapter::GetAvailableMemory( void ) const
	{
		__IME_ASSERT( this->IsValid() );

		DXGI_ADAPTER_DESC desc;
		if( FAILED( this->pAdapter->GetDesc( &desc ) ) )
			return 0;

		/// We just need dedicated memory for current adapter
		SIZE_T size = desc.DedicatedVideoMemory;
		if( size == 0 ) {
			size = desc.DedicatedSystemMemory;
			if( size == 0 ) {
				size = desc.SharedSystemMemory;
			}
		} 

		return size >> 20;
	}
}