//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: DXGI Display and Adapter event handler.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _GLDisplayAdapter_h_
#define _GLDisplayAdapter_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// Singleton
#include <Foundation/Singleton.h>

struct IDXGIAdapter1;
struct IDXGIOutput;
struct IDXGIFactory1;

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>DisplayAdapter</c> is a OpenGL display adapter implementation. This class represents
	/// low-level structure of graphics adapter.
	/// </summary>
	class DisplayAdapter : public Core::ReferenceCount
	{
		friend class RenderingDevice;
		friend class SwapChain;

		IDXGIAdapter1* pAdapter;
		IDXGIFactory1* myFactoryPtr;
		STL::Array< IDXGIOutput* > pOutputs;
		SizeT iAdapterOrdinal; //! Adapters count
		SizeT iOutputCount; //!< Output devices
		bool myValidation; //!< Validation

	public:
		/// <summary cref="DisplayAdapter::DisplayAdapter">
		/// Default constructor.
		/// </summary>
		DisplayAdapter( void );

		/// <summary cref="DisplayAdapter::~DisplayAdapter">
		/// Destructor.
		/// </summary>
		virtual ~DisplayAdapter( void );

		/// <summary cref="DisplayAdapter::Open">
		/// Open display adapter.
		/// </summary>
		void Setup( void );

		/// <summary cref="DisplayAdapter::Close">
		/// Close display adapter.
		/// </summary>
		void Discard( void );

		/// <summary cref="DisplayAdapter::IsValid">
		/// Validation
		/// </summary>
		bool IsValid( void ) const;

		/// <summary cref="DisplayAdapter::GetAvailableMemory">
		/// Retrieves size of memory available with current adapter.
		/// </summary>
		SizeT GetAvailableMemory( void ) const;
	};

	__IME_INLINED bool DisplayAdapter::IsValid( void ) const
	{
		return this->myValidation;
	}
}

#endif /// _GLDisplayAdapter_h_