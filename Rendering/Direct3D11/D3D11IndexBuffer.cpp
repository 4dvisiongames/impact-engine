//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Direct3D11 index buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Direct3D11 index buffer
#include <Rendering/Direct3D11/D3D11IndexBuffer.h>
/// Direct3D11 rendering device
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// Direct3D11 
#include <d3d11.h>
/// Direct3D11 converter
#include <Rendering/Direct3D11/D3D11Converter.h>

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	IndexBuffer::IndexBuffer( const IntrusivePtr< Renderer::RenderingDevice >& device ) : myDevice( device ), myBuffer( nullptr )
	{
		/// Empty
	}

	IndexBuffer::~IndexBuffer( void )
	{
		if( this->IsValid() )
			this->Discard();
	}

	void IndexBuffer::Setup( const Renderer::IndexBufferDesc& desc )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( desc.myIndexCount > 0 );

		ID3D11Device* device = this->myDevice->myD3D11Device;

		/// Result handlder
		HRESULT hr = S_FALSE;
		/// Read/Write permissions for CPU
		UINT cpuAccess = 0;
		if( (desc.myCPUAccessMask & Renderer::CPUAccessFlags::ReadAccess) != 0 ) 
			cpuAccess |= D3D11_CPU_ACCESS_READ;
		if( (desc.myCPUAccessMask & Renderer::CPUAccessFlags::WriteAccess) != 0 ) 
			cpuAccess |= D3D11_CPU_ACCESS_WRITE;

		/// Index buffer description
		D3D11_BUFFER_DESC ibDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &ibDesc, sizeof(D3D11_BUFFER_DESC) );
		ibDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		ibDesc.ByteWidth = static_cast< UINT >( desc.myIndexCount * desc.myPrecision ); //!< Can be NULL if no user data written
		ibDesc.CPUAccessFlags = cpuAccess;
		ibDesc.Usage = Direct3D11::D3D11Converter::ToD3D( desc.myUsageFlags );

#if defined( DEBUG )
		if( ibDesc.Usage == D3D11_USAGE_IMMUTABLE )
			__IME_ASSERT_MSG( desc.myData != nullptr, "Index buffer is defined as static, you must provide data to store" );
#endif

		D3D11_SUBRESOURCE_DATA ibData;
		if( desc.myData ) {
			Memory::LowLevelMemory::EraseMemoryBlock( &ibData, sizeof(D3D11_SUBRESOURCE_DATA) );
			ibData.pSysMem = desc.myData;
			ibData.SysMemPitch = desc.myPrecision;
		}

		hr = device->CreateBuffer( &ibDesc, (desc.myData != nullptr) ? &ibData : nullptr, &this->myBuffer );

		/// Check if we failed with Index Buffer
		if( FAILED( hr ) )
			NDKThrowException( Debug::EExceptionCodes::RendererError,
							   myDevice->GetLastError().AsCharPtr(), 
							   "VertexBuffer::Setup( void )" );
	}

	void IndexBuffer::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		this->myBuffer->Release();
		this->myBuffer = nullptr;

		this->myDevice = nullptr;
	}
}