//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Direct3D11 index buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _D3D11IndexBuffer_h_
#define _D3D11IndexBuffer_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// Index buffer description
#include <Rendering/IndexBufferDesc.h>

/// Forward declaration
struct ID3D11Buffer;

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	/// <summary>
	/// <c>IndexBuffer</c> is a Direct3D11 index buffer implementation.
	/// </summary>
	class IndexBuffer : public Core::ReferenceCount
	{
		friend class Pipeline; //!< This is necesseary
		friend class RenderingContext;

		IntrusivePtr< Renderer::RenderingDevice > myDevice;
		ID3D11Buffer* myBuffer; //!< Index buffer

	public:
		/// <summary cref="IndexBuffer::IndexBuffer">
		/// Constructor.
		/// </summary>
		IndexBuffer( const IntrusivePtr< Renderer::RenderingDevice >& device );

		/// <summary cref="IndexBuffer::~IndexBuffer">
		/// Destructor.
		/// </summary>
		virtual ~IndexBuffer( void );

		/// <summary cref="IndexBuffer::Setup">
		/// Create new index buffer instance.
		/// </summary>
		void Setup( const Renderer::IndexBufferDesc& desc );

		/// <summary cref="IndexBuffer::Discard">
		/// Release index buffer.
		/// </summary>
		void Discard( void );

		/// <summary cref="IndexBuffer::IsValid">
		/// Validate current index buffer.
		/// </summary>
		bool IsValid( void ) const;
	};

	__IME_INLINED bool IndexBuffer::IsValid( void ) const
	{
		return this->myBuffer != nullptr;
	}
}

#endif /// _D3D11IndexBuffer_h_