//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 pipeline definition for engine.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Context pipeline
#include <Rendering/Direct3D11/D3D11Pipeline.h>
/// Direct3D11 vertex buffer
#include <Rendering/Direct3D11/D3D11VertexBuffer.h>
/// Direct3D11 index buffer
#include <Rendering/Direct3D11/D3D11IndexBuffer.h>
/// Direct3D11 contant buffer
#include <Rendering/Direct3D11/D3D11ConstantBuffer.h>
/// Direct3D11 render state
#include <Rendering/Direct3D11/D3D11RenderState.h>
/// Direct3D11 render target
#include <Rendering/Direct3D11/D3D11RenderTarget.h>
/// Direct3D11 depth-stencil
#include <Rendering/Direct3D11/D3D11DepthBuffer.h>
/// Direct3D11 texture
#include <Rendering/Direct3D11/D3D11Texture.h>
/// Direct3D11 shadeer
#include <Rendering/Direct3D11/D3D11Shader.h>
/// Texture format converters
#include <Rendering/TextureFormat.h>
/// Render state object
#include <Rendering/RenderStateType.h>
/// Pipeline state objects naming
#include <Rendering/Base/BasePipelineState.h>

/// Rendering sub-system namespace
namespace Renderer
{
	Pipeline::Pipeline( void ) : myCommandList( nullptr )
	{
		/// Empty
	}

	Pipeline::~Pipeline( void )
	{
		/// Empty
	}

	bool Pipeline::Begin( void )
	{
		bool r = this->myCommandCompiler.Begin( this->myCommandList->myCommandBuffer );
		__IME_ASSERT( this->myCommandCompiler.IsValid() );
		return r;
	}

	void Pipeline::End( void )
	{
		this->myCommandCompiler.End();
	}

	void Pipeline::BindClearState( IntrusivePtr< Renderer::RenderTarget >& rhs, float R, float G, float B, float A )
	{
		RenderTarget* renderTarget = rhs.Get();
		renderTarget->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindClearState );
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessRenderTarget );
		this->myCommandCompiler.Write( renderTarget );
		this->myCommandCompiler.Write( R );
		this->myCommandCompiler.Write( G );
		this->myCommandCompiler.Write( B );
		this->myCommandCompiler.Write( A );
	}

	void Pipeline::BindClearState( IntrusivePtr< Renderer::DepthBuffer >& rhs, float clearedDepth, u8 clearedStencil )
	{
		DepthBuffer* depthBuffer = rhs.Get();
		depthBuffer->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindClearState );
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessDepthStencil );
		this->myCommandCompiler.Write( depthBuffer );
		this->myCommandCompiler.Write( clearedDepth );
		this->myCommandCompiler.Write( clearedStencil );
	}

	void Pipeline::BindVertexBuffer( u32 slot, IntrusivePtr< Renderer::VertexBuffer >& rhs )
	{
		VertexBuffer* vertexBuffer = rhs.Get();
		vertexBuffer->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindVertexBuffer );
		this->myCommandCompiler.Write( vertexBuffer );
		this->myCommandCompiler.Write( slot );
	}

	void Pipeline::BindIndexBuffer( IntrusivePtr< Renderer::IndexBuffer >& rhs )
	{
		IndexBuffer* indexBuffer = rhs;
		indexBuffer->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindIndexBuffer );
		this->myCommandCompiler.Write( indexBuffer );
	}

	void Pipeline::BindConstantBuffer( Renderer::ShaderType shaderType, u32 slot, IntrusivePtr< Renderer::ConstantBuffer >& rhs )
	{
		/// Get buffer pointer from constant buffer
		ConstantBuffer* constantBuffer = rhs;
		constantBuffer->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindConstatBuffer );

		//! Iterate through all shader types, and attach constant buffer to the specified pipelined
		//! shader object.
		switch( shaderType )
		{
		case Renderer::ShaderType::Vertex:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessVertexShader );
			break;

		case Renderer::ShaderType::Geometry:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessGeometryShader );
			break;

		case Renderer::ShaderType::Hull:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessHullShader );
			break;

		case Renderer::ShaderType::Domain:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessDomainShader );
			break;

		case Renderer::ShaderType::Pixel:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessFragmentShader );
			break;

		case Renderer::ShaderType::Compute: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessComputeShader );
			break;

		default:
			__IME_ASSERT( false );
		}

		this->myCommandCompiler.Write( constantBuffer );
		this->myCommandCompiler.Write( slot );
	}

	void Pipeline::UnbindVertexBuffer( u32 slot )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::UnbindVertexBuffer );
		this->myCommandCompiler.Write( slot );
	}

	void Pipeline::UnbindIndexBuffer( void )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::UnbindIndexBuffer );
	}

	void Pipeline::UnbindConstantBuffer( Renderer::ShaderType shaderType, u32 slot )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::UnbindConstantBuffer );

		/// Iterate through all shader types
		switch( shaderType )
		{
		case Renderer::ShaderType::Vertex: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessVertexShader );
			break;

		case Renderer::ShaderType::Geometry: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessGeometryShader );
			break;

		case Renderer::ShaderType::Hull: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessHullShader ); 
			break;

		case Renderer::ShaderType::Domain: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessDomainShader );
			break;

		case Renderer::ShaderType::Pixel: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessFragmentShader );
			break;

		case Renderer::ShaderType::Compute: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessComputeShader );
			break;
		}

		this->myCommandCompiler.Write( slot );
	}

	void Pipeline::BindRenderState( IntrusivePtr< Renderer::RenderState >& rhs )
	{
		RenderState* state = rhs;
		state->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindRenderState );

		switch( rhs->GetRenderState() ) {
		case RenderStateTypeList::Blend:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessBlendState );
			break;

		case RenderStateTypeList::DepthStencil:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessDepthStencilState );
			break;

		case RenderStateTypeList::Rasterizer:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessRasterizerState );
			break;

		default:
			__IME_ASSERT( false );
		}

		this->myCommandCompiler.Write( state );
	}

	void Pipeline::BindSamplerState( IntrusivePtr< Renderer::RenderState >& rhs, u32 slot )
	{
		RenderState* state = rhs;
		state->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessSamplerState );
		this->myCommandCompiler.Write( slot );
		this->myCommandCompiler.Write( state );
	}

	void Pipeline::BindRenderTarget( SizeT renderTargetsCount, IntrusivePtr< Renderer::RenderTarget >* renderTargets, IntrusivePtr< Renderer::DepthBuffer >& depthBuffer )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindRenderTargets );

		if( depthBuffer.IsValid() ) {
			DepthBuffer* depth = depthBuffer.Get();
			__IME_ASSERT( depth->IsValid() );
			depth->AddRef< Threading::MemoryOrderEnum::Relaxed >();

			this->myCommandCompiler.Write( depth );
		} else {
			this->myCommandCompiler.Write( reinterpret_cast< uintptr_t >( nullptr ) );
		}

		this->myCommandCompiler.Write( renderTargetsCount );
		if( renderTargetsCount > 0 ) {
			for( SizeT idx = 0; idx < renderTargetsCount; idx++ ) {
				RenderTarget* renderTarget = renderTargets[idx].Get();
				__IME_ASSERT( renderTarget->IsValid() );
				renderTarget->AddRef< Threading::MemoryOrderEnum::Relaxed >();

				this->myCommandCompiler.Write( renderTarget );
			}
		}
	}

	void Pipeline::BindShader( Renderer::ShaderType shaderType, IntrusivePtr< Renderer::Shader >& shader )
	{
		Shader* localShader = shader.Get();
		localShader->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindShaderProgram );
		
		switch( shaderType ) {
		case ShaderTypeEnum::Vertex:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessVertexShader );
			break;

		case ShaderTypeEnum::Geometry:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessGeometryShader );
			break;

		case ShaderTypeEnum::Hull:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessHullShader );
			break;

		case ShaderTypeEnum::Domain:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessDomainShader );
			break;

		case ShaderTypeEnum::Pixel:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessFragmentShader );
			break;

		case ShaderTypeEnum::Compute:
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessComputeShader );
			break;

		default:
			__IME_ASSERT( false );
		};

		this->myCommandCompiler.Write( localShader );
	}

	void Pipeline::UnbindShader( Renderer::ShaderType shaderType )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::UnbindShaderProgram );

		switch( shaderType )
		{
		case Renderer::ShaderType::Vertex: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessVertexShader );
			break;

		case Renderer::ShaderType::Geometry: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessGeometryShader );
			break;

		case Renderer::ShaderType::Hull: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessHullShader );
			break;

		case Renderer::ShaderType::Domain: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessDomainShader );
			break;

		case Renderer::ShaderType::Pixel: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessFragmentShader );
			break;

		case Renderer::ShaderType::Compute: 
			this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::ProcessComputeShader );
			break;

		default:
			__IME_ASSERT( false );
		}
	}

	/*void Pipeline::BindClearState( Ptr< ClearState >& rhs )
	{
		ClearState* state = rhs;
		this->myCompiler.Write( Base::PipelineStateEnum::BindClearState );
		this->myCompiler.Write( state );
	}*/

	void Pipeline::SetPrimitiveTopology( Renderer::PrimitiveTopology::List type )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::BindPrimitiveTopology );
		this->myCommandCompiler.Write< SizeT >( type );
	}

	void Pipeline::UploadTexture( IntrusivePtr< Renderer::Texture >& rhs, void* data )
	{
		Texture* texture = rhs;
		texture->AddRef< Threading::MemoryOrderEnum::Relaxed >();

		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::UpdateTexture );
		this->myCommandCompiler.Write( texture );
		//! Note: this is needed because sometimes compiler may generate BAD code with wrong pointer
		//! which needs on reading and command list execution.
		this->myCommandCompiler.Write( reinterpret_cast< uintptr_t >( data ) );
	}

	/*void Pipeline::SetViewport( const Ptr< Viewport >& viewport )
	{

	}*/

	void Pipeline::DrawNonIndexed( u32 iVertexCount, u32 iStartVertex )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::DrawNonIndexed );
		this->myCommandCompiler.Write( iVertexCount );
		this->myCommandCompiler.Write( iStartVertex );
	}

	void Pipeline::DrawNonIndexed( u32 iVertexCount, u32 iStartVertex, u32 iInstances, u32 iStartInstanceIndex )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::DrawNonIndexedWithInstancing );
		this->myCommandCompiler.Write( iVertexCount );
		this->myCommandCompiler.Write( iStartVertex );
		this->myCommandCompiler.Write( iInstances );
		this->myCommandCompiler.Write( iStartInstanceIndex );
	}

	void Pipeline::DrawIndexed( u32 iIndexCount, u32 iFirstIndex, u32 iStartVertex )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::Draw );
		this->myCommandCompiler.Write( iIndexCount );
		this->myCommandCompiler.Write( iFirstIndex );
		this->myCommandCompiler.Write( iStartVertex );
	}

	void Pipeline::DrawIndexed( u32 iIndexCount, u32 iFirstIndex, u32 iStartVertex, u32 iInstances, u32 iStartInstanceIndex )
	{
		this->myCommandCompiler.Write< SSizeT >( Base::PipelineStateEnum::DrawWithInstancing );
		this->myCommandCompiler.Write( iIndexCount );
		this->myCommandCompiler.Write( iFirstIndex );
		this->myCommandCompiler.Write( iStartVertex );
		this->myCommandCompiler.Write( iInstances );
		this->myCommandCompiler.Write( iStartInstanceIndex );
	}
}