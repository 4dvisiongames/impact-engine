//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Pipeline definition for engine.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _D3D11Pipeline_h_
#define _D3D11Pipeline_h_

/// Command list
#include <Rendering/CommandList.h>
/// Command compiler
#include <Foundation/CommandCompiler.h>
/// Shader type definitions
#include <Rendering/ShaderType.h>
/// Primitive topology
#include <Rendering/PrimitiveTopology.h>

/// Renderer sub-system
namespace Renderer
{
	class VertexBuffer;
	class IndexBuffer;
	class ConstantBuffer;
	class RenderState;

	class Pipeline
	{
		/// Make them friends to be happy!
		friend class RenderTarget;
		friend class DepthBuffer;
		friend class Texture;
		friend class Shader;
		friend class CommandList;
		friend class RenderingContext;

		Kernel::CommandCompiler myCommandCompiler;
		IntrusivePtr< Renderer::CommandList > myCommandList;

	public:
		Pipeline( void );
		~Pipeline( void );

		void AttachCommandList( const IntrusivePtr< Renderer::CommandList >& list );

		//! Begin commands recording
		bool Begin( void );
		//! End commands recording
		void End( void );
		//! Bind color or depth(-stencil) buffer clear state.
		void BindClearState( IntrusivePtr< Renderer::RenderTarget >& rhs, float R, float G, float B, float A );
		void BindClearState( IntrusivePtr< Renderer::DepthBuffer >& rhs, float clearedDepth, u8 clearedStencil );
		void BindVertexBuffer( u32 slot, IntrusivePtr< Renderer::VertexBuffer >& rhs );
		void BindIndexBuffer( IntrusivePtr< Renderer::IndexBuffer >& rhs );
		void BindConstantBuffer( Renderer::ShaderType shaderType, u32 slot, IntrusivePtr< Renderer::ConstantBuffer >& rhs );
		void BindRenderState( IntrusivePtr< Renderer::RenderState >& rhs );
		void BindSamplerState( IntrusivePtr< Renderer::RenderState >& rhs, u32 slot );
		void BindRenderTarget( SizeT renderTargetsCount, IntrusivePtr< Renderer::RenderTarget >* renderTargets, 
							   IntrusivePtr< Renderer::DepthBuffer >& depthBuffer = IntrusivePtr< Renderer::DepthBuffer >( nullptr ) );
		void BindShader( Renderer::ShaderType shaderType, IntrusivePtr< Renderer::Shader >& shader );
		void UnbindVertexBuffer( u32 slot );
		void UnbindIndexBuffer( void );
		void UnbindConstantBuffer( Renderer::ShaderType shaderType, u32 slot );
		void UnbindShader( Renderer::ShaderType shaderType );
		void SetPrimitiveTopology( Renderer::PrimitiveTopology::List type );
		//! Upload texture data immediatelly wihtout rectangle locking, just fill everything.
		void UploadTexture( IntrusivePtr< Texture >& texture, void* data );
		//! Dispath compute cores(X*Y*Z)
		void DispatchCompute( u32 coreX, u32 coreY, u32 coreZ );
		//! Issue all commands and draw primitives to the attached context.
		void DrawNonIndexed( u32 iVertexCount, u32 iFirstVertex );
		void DrawIndexed( u32 iIndexCount, u32 iFirstIndex, u32 iStartVertex );
		//! Issue all commands and draw primitives instances to the attached context.
		void DrawNonIndexed(u32 iVertexCount, u32 iFirstVertex, u32 iInstances, u32 iStartInstanceIndex );
		void DrawIndexed( u32 iIndexCount, u32 iFirstIndex, u32 iStartVertex, u32 iInstances, u32 iStartInstanceIndex );
	};

	__IME_INLINED void Pipeline::AttachCommandList( const IntrusivePtr< Renderer::CommandList >& rhs )
	{
		this->myCommandList = rhs;
	}
}

#endif /// _D3D11Pipeline_h_