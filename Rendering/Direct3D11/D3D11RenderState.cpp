//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Render state front-end
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Direct3D11 render state object
#include <Rendering/Direct3D11/D3D11RenderState.h>
/// Direct3D11 rendering device
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// Debugger
#include <Foundation/DebugMessaging.h>

/// Direct3D11 render system namespace
namespace Renderer
{
	RenderState::RenderState( const IntrusivePtr< RenderingDevice >& device ) 
		: myType( Renderer::RenderStateTypeList::Unknown )
		, myValidation( false )
		, myRenderState( nullptr )
		, myDevice( device )
	{
		/// Empty
	}

	RenderState::~RenderState( void )
	{
		if( this->IsValid() ) {
			this->Discard();
		}
	}

	void RenderState::Setup( Renderer::DepthStencilStateDesc& dsDesc )
	{
		/// Debug assertion: render state must not be created at setup time
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid() );
		/// Setup render state type
		this->myType = Renderer::RenderStateTypeList::DepthStencil;
	}

	void RenderState::Setup( Renderer::RasterizerStateDesc& dsDesc )
	{
		/// Debug assertion: render state must not be created at setup time
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid( ) );
		/// Setup render state type
		this->myType = Renderer::RenderStateTypeList::Rasterizer;
	}

	void RenderState::Setup( Renderer::BlendStateDesc& dsDesc )
	{
		/// Debug assertion: render state must not be created at setup time
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid( ) );
		/// Setup render state type
		this->myType = Renderer::RenderStateTypeList::Blend;
	}

	void RenderState::Setup( Renderer::SamplerStateDesc& dsDesc )
	{
		/// Debug assertion: render state must not be created at setup time
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid( ) );
		/// Setup render state type
		this->myType = Renderer::RenderStateTypeList::Sampler;
	}

	void RenderState::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );
		this->myValidation = false;
	}
}