//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Render state front-end
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoRenderState_h_
#define _NanoRenderState_h_

/// Memory stream object
#include <Foundation/RefCount.h>
/// Render state descriptions
#include <Rendering/RenderStateDesc.h>
/// Render state type
#include <Rendering/RenderStateType.h>

/// Forward declaration
struct ID3D11DeviceChild;

//! Renderer namespace
namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	//! RenderState is a generalized Render State Object definitions as precompiled commands list.
	class RenderState : public Core::ReferenceCount
	{
		ID3D11DeviceChild* myRenderState;
		//! My holding device
		IntrusivePtr< RenderingDevice > myDevice;
		//! My render state type
		Renderer::RenderStateType myType;
		//! Validation parameter
		bool myValidation;

	public:
		RenderState( const IntrusivePtr< RenderingDevice >& device );
		virtual ~RenderState( void );

		//! Setup render state as depth-stencil state.
		void Setup( DepthStencilStateDesc& dsDesc );
		//! Setup render state as rasterizer state.
		void Setup( RasterizerStateDesc& dsDesc );
		//! Setup render state as rasterizer state.
		void Setup( BlendStateDesc& dsDesc );
		//! Setup render state as sampler state.
		void Setup( SamplerStateDesc& dsDesc );
		//! Discard all render state writings from this object.
		void Discard( void );
		//! Check for render state validness.
		bool IsValid( void ) const;
		//! Get render state type
		RenderStateType GetRenderState( void );
	};

	__IME_INLINED RenderStateType RenderState::GetRenderState( void )
	{
		return this->myType;
	}

	__IME_INLINED bool RenderState::IsValid( void ) const
	{
		return this->myValidation;
	}
}

#endif /// _NanoRenderState_h_