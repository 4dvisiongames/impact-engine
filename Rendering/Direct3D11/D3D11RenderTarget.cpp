//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: 2-Dimentional render target definitions, abstracted from rendering API.
///
/// Author: Pavel Umnikov
//=====================================================================================

#include <d3d11.h>

/// Direct3D11 render system
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// Direct3D11 render target
#include <Rendering/Direct3D11/D3D11RenderTarget.h>
/// Direct3D11 texture format
#include <Rendering/Direct3D11/D3D11Converter.h>

/// Rendering sub-system namespace
namespace Renderer
{
	RenderTarget::RenderTarget( const IntrusivePtr< Renderer::RenderingDevice >& device )
		: myDevice( device )
		, myBoundedTexture( nullptr )
		, myRTV( nullptr )
	{
		/// Empty
	}

	RenderTarget::~RenderTarget( void )
	{
		/// Release current render target
		if( this->IsValid() )
		{
			this->Discard();
		}
	}

	void RenderTarget::Setup( const IntrusivePtr< Texture >& rhs )
	{
		__IME_ASSERT( !this->IsValid() );

		__IME_ASSERT( this->myDevice.IsValid() );
		__IME_ASSERT( this->myDevice->IsValid() );

		__IME_ASSERT( rhs.IsValid() );
		__IME_ASSERT( rhs->IsValid() );
		__IME_ASSERT( rhs->IsRendereable() );

		this->myBoundedTexture = rhs;

		ID3D11RenderTargetView* rtv = NULL;

		/// Define Render Target View shader resource type
		switch( this->myBoundedTexture->GetTextureType() )
		{
			case Renderer::TextureTypeEnum::Tex1D: 
				rtv = this->CreateTexture1D( this->myBoundedTexture );
				break;

			case Renderer::TextureTypeEnum::Tex2D:
				rtv = this->CreateTexture2D( this->myBoundedTexture );
				break;

			case Renderer::TextureTypeEnum::Tex3D:
				rtv = this->CreateTexture3D( this->myBoundedTexture );
				break;
		}

		if( !rtv ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging( ) ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11RenderTarget::Setup( void )" );
			} else
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create render target view from texture resource!",
									"D3D11RenderTarget::Setup( void )" );
		}

		this->myRTV = rtv;
	}

	void RenderTarget::Discard( void )
	{
		if( this->myRTV ) { 
			this->myRTV->Release( );
			this->myRTV = nullptr;
		}

		this->myBoundedTexture = nullptr;
		this->myDevice = nullptr;
	}

	ID3D11RenderTargetView* RenderTarget::CreateTexture1D( IntrusivePtr< Texture >& rhs )
	{
		D3D11_RENDER_TARGET_VIEW_DESC mDesc;
		D3D11_TEXTURE1D_DESC mTex1DDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &mDesc, sizeof( mDesc ) );
		ID3D11Texture1D* pTex1D = static_cast< ID3D11Texture1D* >( rhs->myTexture );
		pTex1D->GetDesc( &mTex1DDesc );
		mDesc.Format = mTex1DDesc.Format;
		mDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE1D;

		ID3D11RenderTargetView* rtv = NULL;
		ID3D11Device* device = this->myDevice->myD3D11Device;
		device->CreateRenderTargetView( pTex1D, &mDesc, &rtv );

		return rtv;
	}

	ID3D11RenderTargetView* RenderTarget::CreateTexture2D( IntrusivePtr< Texture >& rhs )
	{
		D3D11_RENDER_TARGET_VIEW_DESC mDesc;
		D3D11_TEXTURE2D_DESC mTex2DDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &mDesc, sizeof( mDesc ) );
		ID3D11Texture2D* pTex2D = static_cast< ID3D11Texture2D* >( rhs->myTexture );
		pTex2D->GetDesc( &mTex2DDesc );
		mDesc.Format = mTex2DDesc.Format;

		if( rhs->GetSamples() > 1 )
			mDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2DMS;
		else
			mDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;

		ID3D11RenderTargetView* rtv = NULL;
		ID3D11Device* device = this->myDevice->myD3D11Device;
		device->CreateRenderTargetView( pTex2D, &mDesc, &rtv );

		return rtv;
	}

	ID3D11RenderTargetView* RenderTarget::CreateTexture3D( IntrusivePtr< Texture >& rhs )
	{
		D3D11_RENDER_TARGET_VIEW_DESC mDesc;
		D3D11_TEXTURE3D_DESC mTex3DDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &mDesc, sizeof( mDesc ) );
		ID3D11Texture3D* pTex3D = static_cast< ID3D11Texture3D* >(rhs->myTexture);
		pTex3D->GetDesc( &mTex3DDesc );
		mDesc.Format = mTex3DDesc.Format;
		mDesc.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE3D;

		ID3D11RenderTargetView* rtv = NULL;
		ID3D11Device* device = this->myDevice->myD3D11Device;
		device->CreateRenderTargetView( pTex3D, &mDesc, &rtv );

		return rtv;
	}
}