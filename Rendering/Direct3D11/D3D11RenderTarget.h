//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Render target definitions, Direct3D11 level API.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _D3D11RenderTarget_h_
#define _D3D11RenderTarget_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// Texture
#include <Rendering/Texture.h>

/// Forward declaration
struct ID3D11RenderTargetView;

/// Rendering namespace
namespace Renderer
{
	/// <summary>
	/// <c>RenderTarget</c> is a Direct3D11 multiple render target implementation.
	/// </summary>
	class RenderTarget : public Core::ReferenceCount
	{
		/// Make them friends to be happy!
		friend class Pipeline;
		friend class RenderingContext;

		IntrusivePtr< Renderer::RenderingDevice > myDevice; //!< Current bounded device
		IntrusivePtr< Renderer::Texture > myBoundedTexture; //!< Current bounded texture for render target
		ID3D11RenderTargetView* myRTV; //!< Render target view

	public:
		/// <summary cref="RenderTarget::RenderTarget">
		/// Constructor.
		/// </summary>
		RenderTarget( const IntrusivePtr< Renderer::RenderingDevice >& device );

		/// <summary cref="RenderTarget::~RenderTarget">
		/// Destructor.
		/// </summary>
		virtual ~RenderTarget( void );

		/// <summary cref="RenderTarget::CreateRenderTarget">
		/// Create multiple render target view.
		/// </summary>
		/// <remarks>Add render target creation from Texture2D.</remarks>
		void Setup( const IntrusivePtr< Texture >& rhs );
		
		/// <summary cref="RenderTarget::ReleaseRenderTarget">
		/// Release multiple render target view.
		/// </summary>
		void Discard( void );

		/// <summary cref="RenderTarget::IsValid">
		/// Validation function.
		/// </summary>
		bool IsValid( void ) const;

	private:
		ID3D11RenderTargetView* RenderTarget::CreateTexture1D( IntrusivePtr< Texture >& rhs );
		ID3D11RenderTargetView* RenderTarget::CreateTexture2D( IntrusivePtr< Texture >& rhs );
		ID3D11RenderTargetView* RenderTarget::CreateTexture3D( IntrusivePtr< Texture >& rhs );
	};

	__IME_INLINED bool RenderTarget::IsValid( void ) const
	{
		return this->myRTV != nullptr;
	}
}

#endif /// _D3D11RenderTarget_h_