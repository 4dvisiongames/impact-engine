//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 rendering context definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Direct3D11 rendering context
#include <Rendering/Direct3D11/D3D11RenderingContext.h>
/// Direct3D11 rendering device
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// Direct3D11 Constant buffer
#include <Rendering/Direct3D11/D3D11ConstantBuffer.h>
/// Direct3D11 Index buffer
#include <Rendering/Direct3D11/D3D11IndexBuffer.h>
/// Direct3D11 Vertex buffer
#include <Rendering/Direct3D11/D3D11VertexBuffer.h>
/// Direct3D11 Render target
#include <Rendering/Direct3D11/D3D11RenderTarget.h>
/// Direct3D11 Depth buffer
#include <Rendering/Direct3D11/D3D11DepthBuffer.h>
/// Driect3D11 Shader
#include <Rendering/Direct3D11/D3D11Shader.h>
/// Direct3D11 Render state
#include <Rendering/Direct3D11/D3D11RenderState.h>
/// Direct3D11 pipeline
#include <Rendering/Direct3D11/D3D11Pipeline.h>
/// Viewport
//#include <Rendering/Viewport.h>
/// Mapping type
#include <Rendering/MappingType.h>
/// Primitive type
#include <Rendering/PrimitiveTopology.h>
/// Shader type
#include <Rendering/ShaderType.h>
/// Pipeline codes
#include <Rendering/Base/BasePipelineState.h>
/// Command decompiler
#include <Foundation/CommandDecompiler.h>

/// Direct3D11
#include <D3D11.h>
/// Direct3D11 converter
#include <Rendering/Direct3D11/D3D11Converter.h>

/// Direct3D11 rendering context
namespace Renderer
{
	RenderingContext::RenderingContext( const IntrusivePtr< Renderer::RenderingDevice >& device )
		: myDevice( device )
		, myD3D11RenderContext( nullptr )
	{
		/// Empty
	}

	RenderingContext::~RenderingContext( void )
	{
		if( this->IsValid() )
			this->Discard();
	}

	void RenderingContext::Setup( void )
	{
		__IME_ASSERT( !this->IsValid( ) );

		ID3D11Device* device = this->myDevice->myD3D11Device;
		ID3D11DeviceContext* context = nullptr;

		device->GetImmediateContext( &context );
		if( !context )
			NDKThrowException( Debug::EExceptionCodes::RendererError,
								"[D3D11] Failed to get rendering context from device!",
								"RenderingContext::Setup(void)" );

		this->myD3D11RenderContext = context;
	}

	void RenderingContext::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		this->myD3D11RenderContext->Release();
		this->myD3D11RenderContext = nullptr;

		this->myDevice = nullptr;
	}

	void RenderingContext::PlayCommandList( const IntrusivePtr< Renderer::CommandList >& list )
	{
		if( !list.IsValid() )
			return;

		Kernel::CommandDecompiler decompiler;
		ID3D11Debug* debugInterface = this->myDevice->myD3D11DebugInterface;
		ID3D11DeviceContext* deviceContext = this->myD3D11RenderContext;

		bool r = decompiler.Begin( list->myCommandBuffer );
		if( !r )
			return; 

		while( !list->myCommandBuffer->Eof() ) {
			SSizeT commandCode = 0;
			if( decompiler.Read< SSizeT >( commandCode ) ) {
				switch( commandCode ) {
					case Base::PipelineStateEnum::BindClearState:
						decompiler.Read< SSizeT >( commandCode );
						switch( commandCode ) {
							case Base::PipelineStateEnum::ProcessDepthStencil:
								{
									Renderer::DepthBuffer* depthBuffer = nullptr;
									decompiler.Read( depthBuffer );
									__IME_ASSERT( depthBuffer );

									float clearedDepth = 0.0f;
									decompiler.Read( clearedDepth );

									u8 clearedStencil = 0;
									decompiler.Read( clearedStencil );

									ID3D11DepthStencilView* vDSV = depthBuffer->myDSV;
									deviceContext->ClearDepthStencilView( vDSV, D3D11_CLEAR_DEPTH, clearedDepth, clearedStencil );

									depthBuffer->Release< Threading::MemoryOrderEnum::Relaxed >();

									if (list->myBundleState)
										depthBuffer->AddRef< Threading::MemoryOrderEnum::Relaxed >();
								}
								break;

							case Base::PipelineStateEnum::ProcessRenderTarget:
								{
									Renderer::RenderTarget* renderTarget = nullptr;
									decompiler.Read( renderTarget );
									__IME_ASSERT( renderTarget );

									float clearColours[4] = { 0.0f, 0.0f, 0.0f, 0.0f };
									decompiler.Read( clearColours[0] );
									decompiler.Read( clearColours[1] );
									decompiler.Read( clearColours[2] );
									decompiler.Read( clearColours[3] );

									ID3D11RenderTargetView* vRTV = renderTarget->myRTV;
									deviceContext->ClearRenderTargetView( vRTV, clearColours );

									renderTarget->Release< Threading::MemoryOrderEnum::Relaxed >();

									if (list->myBundleState)
										renderTarget->AddRef< Threading::MemoryOrderEnum::Relaxed >();
								}
								break;
						}
						break;

					case Base::PipelineState::BindShaderProgram:
						decompiler.Read< SSizeT >( commandCode );
						switch( commandCode ) {
							case Base::PipelineStateEnum::ProcessVertexShader:
								{
									Renderer::Shader* shader = nullptr;
									decompiler.Read( shader );
									__IME_ASSERT( shader );
									__IME_ASSERT( shader->IsValid() );

									ID3D11VertexShader* vShader = static_cast< ID3D11VertexShader* >( shader->pShader );
									deviceContext->VSSetShader( vShader, NULL, NULL );

									Debug::Messaging::Send( Debug::EMessageType::Normal, "[D3D11] Parsed 'BindShaderProgram' message in command list." );

									if( !list->myBundleState )
										shader->Release< Threading::MemoryOrderEnum::Relaxed >();
								}
								break;

							case Base::PipelineStateEnum::ProcessFragmentShader:
								{
									Renderer::Shader* shader = nullptr;
									decompiler.Read( shader );
									__IME_ASSERT( shader );
									__IME_ASSERT( shader->IsValid() );

									ID3D11PixelShader* pShader = static_cast< ID3D11PixelShader* >( shader->pShader );
									deviceContext->PSSetShader( pShader, NULL, NULL );

									if( !list->myBundleState )
										shader->Release< Threading::MemoryOrderEnum::Relaxed >();
								}
								break;
						}
						break;

					case Base::PipelineStateEnum::BindRenderTargets:
						{
							ID3D11DepthStencilView* depthStencil = NULL;
							ID3D11RenderTargetView* renderTargets[8] = { NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL };

							Renderer::DepthBuffer* depthBuffer = nullptr;
							decompiler.Read(depthBuffer);
							if( depthBuffer )
								depthStencil = depthBuffer->myDSV;

							SizeT renderTargetsCount = 0;
							decompiler.Read(renderTargetsCount);
							__IME_ASSERT(renderTargetsCount >= 0);
							
							if( renderTargetsCount > 0 ) {
								for( SizeT index = 0; index < renderTargetsCount; index++ ) {
									Renderer::RenderTarget* target = nullptr;
									decompiler.Read< uintptr_t >( (uintptr_t&)target);
									__IME_ASSERT(target);
									__IME_ASSERT(target->IsValid());

									ID3D11RenderTargetView* rtv = target->myRTV;
									renderTargets[index] = rtv;

									if( !list->myBundleState )
										target->Release< Threading::MemoryOrderEnum::Relaxed >();
								}
							}

							deviceContext->OMSetRenderTargets(8, renderTargets, depthStencil);

							if( !list->myBundleState )
								if( depthBuffer )
									depthBuffer->Release< Threading::MemoryOrderEnum::Relaxed >();
						}
						break;

					case Base::PipelineStateEnum::BindVertexBuffer:
						{
							ID3D11Buffer* buffer = nullptr;
							Renderer::VertexBuffer* vb = nullptr;
							decompiler.Read(vb);
							__IME_ASSERT(vb);
							__IME_ASSERT(vb->IsValid());
							buffer = vb->myBuffer;

							USizeT slotIndex = 0;
							decompiler.Read(slotIndex);

							USizeT vertexSizes = 0;
							decompiler.Read(vertexSizes);
							__IME_ASSERT(vertexSizes > 0);

							USizeT offsets = 0;

							UINT d3dSlotIndex = static_cast< UINT >( slotIndex );
							UINT d3dVertexSizes = static_cast< UINT >( vertexSizes );
							UINT d3doffsets = static_cast< UINT >( offsets );
							deviceContext->IASetVertexBuffers(d3dSlotIndex, 1, &buffer, &d3dVertexSizes, &d3doffsets);

							if( !list->myBundleState )
								vb->Release< Threading::MemoryOrderEnum::Relaxed >();
						}
						break;

					case Base::PipelineStateEnum::BindIndexBuffer:
						{
							ID3D11Buffer* buffer = nullptr;
							Renderer::IndexBuffer* ib = nullptr;
							decompiler.Read(ib);
							__IME_ASSERT(ib);
							__IME_ASSERT(ib->IsValid());
							buffer = ib->myBuffer;

							deviceContext->IASetIndexBuffer( buffer, DXGI_FORMAT_R32_FLOAT, 0 );

							if( !list->myBundleState )
								ib->Release< Threading::MemoryOrderEnum::Relaxed >();
						}
						break;

					case Base::PipelineStateEnum::BindPrimitiveTopology:
						{
							D3D11_PRIMITIVE_TOPOLOGY outTopology;
							Renderer::PrimitiveTopology::List inputTopology;
							decompiler.Read(inputTopology);

							outTopology = Direct3D11::D3D11Converter::ToD3D(inputTopology);
							__IME_ASSERT(outTopology != D3D_PRIMITIVE_TOPOLOGY_UNDEFINED);
							deviceContext->IASetPrimitiveTopology(outTopology);
						}
						break;

					case Base::PipelineStateEnum::Draw:
						{
#if defined( DEBUG )
							ID3D11Buffer* indexBuffer = NULL;
							DXGI_FORMAT inputFormat;
							UINT offset = 0;
							this->myD3D11RenderContext->IAGetIndexBuffer( &indexBuffer, &inputFormat, &offset );
							if( !indexBuffer ) {
								NDKThrowException( Debug::EExceptionCodes::RendererError,
												   "[D3D11] Index buffer must be bound to render context, before indexed rendering",
												   "RenderingContext::PlayCommandList(const IntrusivePtr<Renderer::CommandList>&)" );
							}

							indexBuffer->Release();

							if( inputFormat == DXGI_FORMAT_UNKNOWN ) {
								NDKThrowException( Debug::EExceptionCodes::RendererError,
												   "[D3D11] Valid format must be provided with index buffer!",
												   "RenderingContext::PlayCommandList(const IntrusivePtr<Renderer::CommandList>&)" );
							}

							/// [Codepoet]: In debug builds always validate pipeline before doing rendering
							if( debugInterface ) {
								if( FAILED( debugInterface->ValidateContext( this->myD3D11RenderContext ) )) {
									Debug::Messaging::Send( Debug::EMessageType::FatalError,
															"[D3D11] Renderer sub-system cannot be used, main rendering context is not valid!" );
								}
							}
#endif

							u32 indexCount = 0;
							decompiler.Read(indexCount);
							__IME_ASSERT(indexCount > 0);

							u32 firstIndex = 0;
							decompiler.Read(firstIndex);

							u32 startVertex = 0;
							decompiler.Read(startVertex);

							deviceContext->DrawIndexed(indexCount, firstIndex, startVertex);
						}
						break;

					case Base::PipelineStateEnum::DrawWithInstancing:
						{
#if defined( DEBUG )
							ID3D11Buffer* indexBuffer = NULL;
							DXGI_FORMAT inputFormat;
							UINT offset = 0;
							this->myD3D11RenderContext->IAGetIndexBuffer( &indexBuffer, &inputFormat, &offset );
							if( !indexBuffer ) {
								NDKThrowException( Debug::EExceptionCodes::RendererError,
												   "[D3D11] Index buffer must be bound to render context, before indexed rendering",
												   "RenderingContext::PlayCommandList(const IntrusivePtr<Renderer::CommandList>&)" );
							}

							indexBuffer->Release();

							if( inputFormat == DXGI_FORMAT_UNKNOWN ) {
								NDKThrowException( Debug::EExceptionCodes::RendererError,
												   "[D3D11] Valid format must be provided with index buffer!",
												   "RenderingContext::PlayCommandList(const IntrusivePtr<Renderer::CommandList>&)" );
							}

							/// [Codepoet]: In debug builds always validate pipeline before doing rendering
							if( debugInterface ) {
								if( FAILED( debugInterface->ValidateContext( this->myD3D11RenderContext ) )) {
									Debug::Messaging::Send( Debug::EMessageType::FatalError,
															"[D3D11] Renderer sub-system cannot be used, main rendering context is not valid!" );
								}
							}
#endif

							u32 indexCount = 0;
							decompiler.Read(indexCount);
							__IME_ASSERT(indexCount > 0);

							u32 firstIndex = 0;
							decompiler.Read(firstIndex);

							u32 startVertex = 0;
							decompiler.Read(startVertex);

							u32 instances = 0;
							decompiler.Read(instances);
							__IME_ASSERT(instances > 0);

							u32 startInstanceIndex = 0;
							decompiler.Read(startInstanceIndex);

							deviceContext->DrawIndexedInstanced(indexCount, instances, firstIndex, startVertex, startInstanceIndex);
						}
						break;

					case Base::PipelineStateEnum::DrawNonIndexed:
						{
#if defined( DEBUG )
							/// [Codepoet]: In debug builds always validate pipeline before doing rendering
							if( debugInterface ) {
								if( FAILED( debugInterface->ValidateContext( this->myD3D11RenderContext ) )) {
									Debug::Messaging::Send( Debug::EMessageType::FatalError,
															"[D3D11] Renderer sub-system cannot be used, main rendering context is not valid!" );
								}
							}
#endif

							u32 vertexCount = 0;
							decompiler.Read(vertexCount);
							__IME_ASSERT(vertexCount > 0);

							u32 firstVertex = 0;
							decompiler.Read(firstVertex);

							deviceContext->Draw( vertexCount, firstVertex );
						}
						break;

					case Base::PipelineStateEnum::DrawNonIndexedWithInstancing:
						{
#if defined( DEBUG )
							/// [Codepoet]: In debug builds always validate pipeline before doing rendering
							if( debugInterface ) {
								if( FAILED( debugInterface->ValidateContext( this->myD3D11RenderContext ) )) {
									Debug::Messaging::Send( Debug::EMessageType::FatalError,
															"[D3D11] Renderer sub-system cannot be used, main rendering context is not valid!" );
								}
							}
#endif

							u32 vertexCount = 0;
							decompiler.Read(vertexCount);
							__IME_ASSERT(vertexCount > 0);

							u32 firstVertex = 0;
							decompiler.Read(firstVertex);

							u32 instances = 0;
							decompiler.Read(instances);
							__IME_ASSERT(instances > 0);

							u32 startInstanceIndex = 0;
							decompiler.Read(startInstanceIndex);

							deviceContext->DrawInstanced( vertexCount, instances, firstVertex, startInstanceIndex );
						}
						break;

					case Base::PipelineStateEnum::DispatchComputeDimensions:
						{
#if defined( DEBUG )
							/// [Codepoet]: In debug builds always validate pipeline before doing rendering
							if( debugInterface ) {
								if( FAILED( debugInterface->ValidateContextForDispatch( this->myD3D11RenderContext ) )) {
									Debug::Messaging::Send( Debug::EMessageType::FatalError,
															"[D3D11] Renderer sub-system cannot be used, main rendering context is not valid!" );
								}
							}
#endif

							u32 dimX = 0;
							decompiler.Read( dimX );
							__IME_ASSERT( dimX > 0 );

							u32 dimY = 0;
							decompiler.Read( dimY );

							u32 dimZ = 0;
							decompiler.Read( dimZ );

							deviceContext->Dispatch( dimX, dimY, dimZ );
						}
						break;

					case Base::PipelineStateEnum::ResetPipelineState:
						{
							deviceContext->ClearState();
						}
						break;

					default:
						break;
				}
			}
		}

		decompiler.End();
	}
}