//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 rendering context definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _D3D11RenderingContext_h_
#define _D3D11RenderingContext_h_

/// Reference counter
#include <Foundation/RefCount.h>

/// Forward declaration
struct ID3D11DeviceContext;

/// Direct3D11 rendering namespace
namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	/// <summary>
	/// <c>RenderingContext</c> is a Direct3D11 rendering context defintion.
	/// </summary>
	class RenderingContext : public Core::ReferenceCount
	{
		/// Make them friends to be happy! :)
		friend class Pipeline;
		friend class CommandList;
		friend class RenderingDevice;

		IntrusivePtr< Renderer::RenderingDevice > myDevice;
		ID3D11DeviceContext* myD3D11RenderContext;

	public:
		/// <summary cref="RenderingContext::RenderingContext">
		/// Constructor.
		/// </summary>
		RenderingContext( const IntrusivePtr< Renderer::RenderingDevice >& device );

		/// <summary cref="RenderingContext::~RenderingContext">
		/// Destuructor.
		/// </summary>
		virtual ~RenderingContext( void );

		/// <summary cref="RenderingContext::Open">
		/// Open new rendering context.
		/// </summary>
		/// <param name="pipeline">Pipeline object used to modify rendering context states.</param>
		/// <param name="cmdList">Command list to be attached to this rendering context for further command generation.</param>
		void Setup( void );

		/// <summary cref="RenderingContext::Close">
		/// Close existing rendering context.
		/// </summary>
		void Discard( void );

		/// <summary cref="RenderingContext::IsValid">
		/// Check if current rendering context has been created and we can use it.
		/// </summary>
		bool IsValid( void ) const;

		/// <summary cref="RenderingContext::PlayCommandList">
		/// </summary>
		void PlayCommandList( const IntrusivePtr< Renderer::CommandList >& list );
	};

	__IME_INLINED bool RenderingContext::IsValid( void ) const
	{
		return this->myD3D11RenderContext != nullptr;
	}
}

#endif /// _D3D11RenderingContext_h_