/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

/// STL converter
#include <Foundation/StringConverter.h>
/// Display adapter
#include <Rendering/Direct3D11/D3D11DisplayAdapter.h>
/// Direct3D11 rendering device
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// Direct3D11 rendering context
#include <Rendering/Direct3D11/D3D11RenderingContext.h>
/// Direct3D11 swap chain
#include <Rendering/Direct3D11/D3D11SwapChain.h>
/// System
#include <Foundation/System.h>

#include <d3d11.h>
/// Direct3D11 feature levels
#include <Rendering/Direct3D11/Internal/D3D11FeatureLevels.h>

/// Rendering sub-system namespace
namespace Renderer
{
	RenderingDevice::RenderingDevice( const IntrusivePtr< DisplayAdapter >& adapter )
		: myD3D11Device( nullptr )
		, myD3D11ImmediateContext( nullptr )
		, myD3D11DebugInterface( nullptr )
		, myD3D11InfoQueue( nullptr )
		, myD3D11SyncQuery( nullptr )
		, currentLevel( Renderer::FeatureLevelEnum::ShaderModel4_0 )
		, myDisplayAdapter( adapter )
		, myValidation( false )
	{
	}

	RenderingDevice::~RenderingDevice( void )
	{
		if( this->IsValid() )
			this->Discard();
	}

	void RenderingDevice::Setup( void )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDisplayAdapter.IsValid() );

		HRESULT hr = S_FALSE;

		unsigned int mCreateDeviceFlags = 0;
#if defined( DEBUG )
		mCreateDeviceFlags = D3D11_CREATE_DEVICE_DEBUG; //!< Always create debug device on Debug Builds
#endif
		D3D_FEATURE_LEVEL ftrLevels[3] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_1, D3D_FEATURE_LEVEL_10_0 };
		D3D_FEATURE_LEVEL ftr;
		ID3D11DeviceContext* deviceCtx = nullptr;
		ID3D11Device* device = nullptr;
		IDXGIAdapter* adapter = this->myDisplayAdapter->pAdapter;
		hr = ::D3D11CreateDevice( adapter, //!< Current adapter
								  D3D_DRIVER_TYPE_UNKNOWN, //!< Always unknown, because of some bug of DirectX ><
								  NULL, //!< No software rendering
								  mCreateDeviceFlags, //!< Device creation flags
								  ftrLevels, //!< A pointer to feature levels
								  3, //!< Number of feature levels
								  D3D11_SDK_VERSION, //!< Version of Direct3D11
								  &device, //!< A pointer to the device
								  &ftr, //!< Feature level
								  &deviceCtx ); //!< Main Device context

		if( FAILED( hr ) ) {
			NDKThrowException( Debug::EExceptionCodes::RendererError,
							   "Failed to create main Direct3D11 device!",
							   "D3D11RenderSystem::Open( void )" );
		}

		this->myD3D11Device = device;
		this->myD3D11ImmediateContext = deviceCtx;
		this->currentLevel = Direct3D11::D3D11FeatureLevelConverter::ToNDKFeatureLevel( ftr );

		ID3D11Debug* pDebugger = nullptr;
		hr = device->QueryInterface( __uuidof(ID3D11Debug), (void**)&pDebugger );
		this->myD3D11DebugInterface = pDebugger;

#if defined( DEBUG )
		if( FAILED( hr ) ) {
			Debug::Messaging::Send( Debug::EMessageType::Warning, "[D3D11] Failed to get Debug Device. No debugging this time." );
		} else {
			this->myD3D11DebugInterface = pDebugger;
			Debug::Messaging::Send( Debug::EMessageType::Normal, "[D3D11] Debugging layer has been created." );
		}
#endif

		D3D11_FEATURE_DATA_THREADING ThreadingOptions;
		device->CheckFeatureSupport( D3D11_FEATURE_THREADING, &ThreadingOptions, sizeof(ThreadingOptions) );

#if defined( DEBUG )
		if( ThreadingOptions.DriverCommandLists )
			Debug::Messaging::Send( Debug::EMessageType::Normal, "[D3D11] Multithreading: Supports hardware command lists recording." );

		if( ThreadingOptions.DriverConcurrentCreates )
			Debug::Messaging::Send( Debug::EMessageType::Normal, "[D3D11] Multithreading: Supports concurrent creation of resources." );
#endif

		if( !ThreadingOptions.DriverCommandLists && !ThreadingOptions.DriverConcurrentCreates ) {
			NDKThrowException( Debug::EExceptionCodes::RendererError, 
							   "Your driver doesn't support Multithreading, please update it. \nNVIDIA: www.nvidia.com \nAMD: www.amd.com",
							   "D3D11RenderSystem::Open( void )" );
		}

#if defined( DEBUG )
		ID3D11InfoQueue* infoQueue = nullptr;
		hr = device->QueryInterface( __uuidof(ID3D11InfoQueue), reinterpret_cast< void** >(&infoQueue) );
		if( FAILED( hr ) ) { this->myD3D11InfoQueue = nullptr; }
		this->myD3D11InfoQueue = infoQueue;
#endif

		/// Create synchronization event for explicit GPU commands flushing
		ID3D11Query* query = nullptr;
		D3D11_QUERY_DESC queryDesc;
		queryDesc.Query = D3D11_QUERY_EVENT;
		queryDesc.MiscFlags = 0;
		hr = this->myD3D11Device->CreateQuery( &queryDesc, &query );
		if( FAILED(hr) ) {
			NDKThrowException( Debug::EExceptionCodes::RendererError, 
							   "Failed to create GPU sync event!",
							   "D3D11RenderSystem::Open( void )" );
		}

		this->myD3D11SyncQuery = query;

#if defined( DEBUG )
		Debug::Messaging::Send( Debug::EMessageType::Normal, 
								STL::String< Char8 >::VA( "[D3D11] Renderer sub - system has been successfully created with %s", Direct3D11::D3D11FeatureLevelConverter::ToString( this->currentLevel ).AsCharPtr( )) );
#endif

		this->myValidation = true;
	}

	void RenderingDevice::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		UINT references = NULL;

		if( this->myD3D11SyncQuery ) {
			this->myD3D11SyncQuery->Release();
			this->myD3D11SyncQuery = nullptr;
		}

#if defined( DEBUG )
		if( this->myD3D11InfoQueue ) {
			this->myD3D11InfoQueue->Release( );
			this->myD3D11InfoQueue = nullptr;
		}
#endif

		if( this->myD3D11ImmediateContext ) {
			this->myD3D11ImmediateContext->ClearState();
			this->myD3D11ImmediateContext->Flush();
			this->myD3D11ImmediateContext->Release();
			this->myD3D11ImmediateContext = nullptr;
		}

#if defined( DEBUG )
		if( this->myD3D11DebugInterface ) {
			this->myD3D11DebugInterface->ReportLiveDeviceObjects( D3D11_RLDO_DETAIL );
			this->myD3D11DebugInterface->Release();
			this->myD3D11DebugInterface = nullptr;
		}
#endif

		if( this->myD3D11Device ) {
			references = this->myD3D11Device->Release( );
#if defined( DEBUG )
			if( references > 0 ) { 
				Debug::Messaging::Send( Debug::EMessageType::Warning,
										STL::String< Char8 >::VA( "[D3D11] Referece count is not zero(currently '%i'), meaning some object were not released.", references ) );
			}
#endif

			this->myD3D11Device = nullptr;
		}

#if defined( DEBUG )
		Debug::Messaging::Send( Debug::EMessageType::Normal,
								"[D3D11] Successfully exited from sub-system!" );
#endif

		this->myValidation = false;
	}

	STL::String< Char8 > RenderingDevice::GetLastError( void )
	{
		STL::String< Char8 > error = "null";
		if( this->myD3D11InfoQueue ) {
			u32 numMessages = (u32)this->myD3D11InfoQueue->GetNumStoredMessages( );
			if( numMessages > NULL ) {
				for( unsigned int messageID = NULL; messageID < numMessages; messageID++ ) {
					SIZE_T messageLength = NULL;
					this->myD3D11InfoQueue->GetMessage( messageID, NULL, &messageLength );
					D3D11_MESSAGE * pMessage = static_cast< D3D11_MESSAGE* >( Memory::Allocator::Malloc( messageLength ) );
					myD3D11InfoQueue->GetMessage( messageID, pMessage, &messageLength );
					error = pMessage->pDescription;
					Memory::Allocator::Free( pMessage );
				}
			}
		}

		return error;
	}

	bool RenderingDevice::BeginRendering( const IntrusivePtr< RenderingContext >& context )
	{
		ID3D11Device* d3d11Device = this->myD3D11Device;
		ID3D11DeviceContext* d3d11Context = context->myD3D11RenderContext;
		HRESULT hr = d3d11Device->GetDeviceRemovedReason();

		switch( hr ) {
			case DXGI_ERROR_DEVICE_HUNG:
				Debug::Messaging::Send( Debug::EMessageType::Error, "[D3D11] Device is not responding, application cannot start rendering." );
				return false;

			case DXGI_ERROR_DEVICE_REMOVED:
				Debug::Messaging::Send( Debug::EMessageType::Error, "[D3D11] Device or driver has been removed." );
				return false;

			case DXGI_ERROR_DEVICE_RESET:
				Debug::Messaging::Send( Debug::EMessageType::Error, "[D3D11] Device has been reseted. All data has been lost..." );
				return false;

			case DXGI_ERROR_INVALID_CALL:
				Debug::Messaging::Send( Debug::EMessageType::Error, "[D3D11] Invalid call of DXGI. Is Device setted up properly?" );
				return false;

			case DXGI_ERROR_DRIVER_INTERNAL_ERROR:
				Debug::Messaging::Send( Debug::EMessageType::Error, "[D3D11] Driver has been restored after failure!" );
				return false;

			case S_OK:
				break;
		}

		IDXGIDevice* dev = nullptr;
		hr = this->myD3D11Device->QueryInterface( __uuidof(IDXGIDevice), (void **)&dev );

		INT currentGPUPriority = 0;
		hr = dev->GetGPUThreadPriority( &currentGPUPriority );

		if( currentGPUPriority < 0 )
			hr = dev->SetGPUThreadPriority( 0 );

		dev->Release();
		dev = nullptr;

		return true;
	}

	bool RenderingDevice::EndRendering( IntrusivePtr< Renderer::SwapChain >& swapChain, Renderer::SwapEffect effect )
	{
		__IME_ASSERT( swapChain.IsValid() );

		bool result = true;
		IntrusivePtr< Renderer::SwapChain > chain = swapChain;
		IDXGISwapChain* d3d11SwapChain = chain->myD3D11SwapChain;
		ID3D11Query* query = this->myD3D11SyncQuery;

		HRESULT hr = d3d11SwapChain->Present( 1, 0 );
		switch(hr) {
			case DXGI_STATUS_OCCLUDED:
				return false;

			case DXGI_ERROR_DEVICE_REMOVED:
				Debug::Messaging::Send( Debug::EMessageType::Error, "[D3D11] Device or driver has been removed." );
				return false;

			case DXGI_ERROR_DEVICE_RESET:
				Debug::Messaging::Send( Debug::EMessageType::Error, "[D3D11] Device has been reseted. All data has been lost..." );
				return false;
		}

		if( effect == Renderer::SwapEffectEnum::Smart ) {
			BOOL result = FALSE;
			hr = this->myD3D11ImmediateContext->GetData( query, &result, sizeof(BOOL), 0 );
			if( FAILED(hr) ) {
				result = false;
			}
		}

		return result;
	}
}