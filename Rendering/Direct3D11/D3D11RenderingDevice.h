//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 rendering sub-system extension.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _GLRenderSystem_h_
#define _GLRenderSystem_h_

/// Base render system
#include <Rendering/Base/BaseRenderSystem.h>
/// Feature level
#include <Rendering/FeatureLevels.h>
/// Swap effect
#include <Rendering/SwapEffect.h>

/// Forward declaration
struct ID3D11Device;
struct ID3D11DeviceContext;
struct ID3D11Debug;
struct ID3D11InfoQueue;
struct ID3D11Query;

/// Rendering sub-system namespace
namespace Renderer
{
	/// Forward declaration
	class DisplayAdapter;
	class RenderingContext;

	/// <summary>
	/// <c>RenderingDevice</c> is D3D11 rendering sub-system extension.
	/// </summary>
	class RenderingDevice : public Base::BaseRenderSystem
	{
		friend class Texture;
		friend class DepthBuffer;
		friend class ConstantBuffer;
		friend class VertexBuffer;
		friend class IndexBuffer;
		friend class Shader;
		friend class VertexDeclaration;
		friend class RenderingContext;
		friend class RenderTarget;
		friend class SwapChain;

		bool myValidation; //!< Creation status of the engine
		bool bOnResize; //!< Resizing status of the renderer
		FeatureLevel currentLevel; // Current feature level
		STL::String< Char8 > myLastError; // Last error, got from OpenGL

		ID3D11Device* myD3D11Device;
		ID3D11DeviceContext* myD3D11ImmediateContext;
		ID3D11Debug* myD3D11DebugInterface;
		ID3D11InfoQueue* myD3D11InfoQueue;
		ID3D11Query* myD3D11SyncQuery;
		IntrusivePtr< DisplayAdapter > myDisplayAdapter;

	public:
		/// <summary cref="RenderingDevice::RenderingDevice">
		/// Constructor.
		/// </summary>
		RenderingDevice( const IntrusivePtr< DisplayAdapter >& adapter );

		/// <summary cref="RenderingDevice::~RenderingDevice">
		/// Destructor.
		/// </summary>
		virtual ~RenderingDevice( void );

		/// <summary cref="RenderingDevice::Open">
		/// Create primary device routine.
		/// </summary>
		/// <param name="level">Current feature level.</param>
		void Setup( void );

		/// <summary cref="RenderingDevice::Close">
		/// Release primary device routine.
		/// </summary>
		void Discard( void );

		/// <summary cref="RenderingDevice::IsValid">
		/// </summary>
		bool IsValid( void ) const;

		/// <summary cref="RenderingDevice::IsDebugging">
		/// Check if current rendering device is created with debugging support.
		/// </summary>
		bool IsDebugging( void ) const;

		/// <summary cref="RenderingDevice::BeginRendering">
		/// Start rendering phase.
		/// </summary>
		bool BeginRendering( const IntrusivePtr< RenderingContext >& context );

		/// <summary cref="RenderingDevice::EndRendering">
		/// End rendering phase.
		/// </summary>
		bool EndRendering( IntrusivePtr< Renderer::SwapChain >& swapChain, Renderer::SwapEffect effect );

		/// <summary cref="RenderingDevice::GetLastError">
		/// Get last error description from queue.
		/// </summary>
		/// <returns>Information portion from queue.</returns>
		STL::String< Char8 > GetLastError( void );

		/// <summary cref="RenderingDevice::GetCurrentFeatureLevel">
		/// Get current and active feature level.
		/// </summary>
		FeatureLevel GetCurrentFeatureLevel( void ) const;

		/// <summary cref="RenderingDevice::IsMultiDevice">
		/// Check if there are more than one GPU in the system.
		/// </summary>
		/// <return>True if there are more then one, otherwise false.</return>
		static bool IsMultiDevice( void );
	};

	__IME_INLINED bool RenderingDevice::IsValid( void ) const
	{
		return this->myValidation;
	}

	__IME_INLINED bool RenderingDevice::IsDebugging( void ) const
	{
		return this->myD3D11DebugInterface != nullptr;
	}

	__IME_INLINED FeatureLevel RenderingDevice::GetCurrentFeatureLevel( void ) const
	{
		return this->currentLevel;
	}
}

#endif /// _GLRenderSystem_h_