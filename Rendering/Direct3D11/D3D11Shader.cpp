//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 base shader-inherited shader definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Direct3D11 shader defintions
#include <Rendering/Direct3D11/D3D11Shader.h>
/// Direct3D11 Render system
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>

#include <d3d11.h>

namespace Renderer
{
	Shader::Shader( const IntrusivePtr< RenderingDevice >& device ) 
		: pShader( NULL )
		, myDevice( device )
	{
		// empty
	}

	Shader::~Shader( void )
	{
		/// Check if shader is valid
		if( this->IsValid() ) {
			this->Discard();
		}
	}

	void Shader::Setup( Renderer::ShaderType shaderType, Memory::Blob* pBlob )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid() );

		// Result
		HRESULT hr = S_OK;
		ID3D11Device* device = this->myDevice->myD3D11Device;

		this->mShaderType = shaderType;

		// Compile shader
		switch( this->mShaderType )
		{
			case Renderer::ShaderType::Vertex: 
			{
				ID3D11VertexShader* ppShader = NULL;
				hr = device->CreateVertexShader( pBlob->GetPtr(), pBlob->Size(), NULL, &ppShader );
				this->pShader = ppShader;
			}
			break;

			case Renderer::ShaderType::Pixel:
			{
				ID3D11PixelShader* ppShader = NULL;
				hr = device->CreatePixelShader( pBlob->GetPtr(), pBlob->Size(), NULL, &ppShader );
				this->pShader = ppShader;
			}
			break;

			case Renderer::ShaderType::Geometry:
			{
				ID3D11GeometryShader* ppShader = NULL;
				hr = device->CreateGeometryShader( pBlob->GetPtr(), pBlob->Size(), NULL, &ppShader );
				this->pShader = ppShader;
			}
			break;

			case Renderer::ShaderType::Hull:
			{
				ID3D11HullShader* ppShader = NULL;
				hr = device->CreateHullShader( pBlob->GetPtr(), pBlob->Size(), NULL, &ppShader );
				this->pShader = ppShader;
			}
			break;

			case Renderer::ShaderType::Domain:
			{
				ID3D11DomainShader* ppShader = NULL;
				hr = device->CreateDomainShader( pBlob->GetPtr(), pBlob->Size(), NULL, &ppShader );
				this->pShader = ppShader;
			}
			break;

			case Renderer::ShaderType::Compute:
			{
				ID3D11ComputeShader* ppShader = NULL;
				hr = device->CreateComputeShader( pBlob->GetPtr(), pBlob->Size(), NULL, &ppShader );
				this->pShader = ppShader;
			}
			break;

		default:
			hr = S_FALSE; //!< This must never happen
		}

		__IME_ASSERT( this->pShader );

		if(FAILED(hr)) {
			NDKThrowException(Debug::EExceptionCodes::RendererError, "Failed to create shader of Direct3D11 device!", "D3D11Shader::Setup");
		}
	}

	void Shader::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );
		
		if( nullptr != this->pShader ) {
			this->pShader->Release();
			this->pShader = nullptr;
		}

		this->myDevice = nullptr;
	}
}