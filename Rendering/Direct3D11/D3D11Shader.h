//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 base shader-inherited shader definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _D3D11ShaderEffect_h_
#define _D3D11ShaderEffect_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// Shader type
#include <Rendering/ShaderType.h>
/// Lexcial string
#include <Foundation/STLString.h>
/// Memory blob
#include <Foundation/MemoryBlob.h>

/// Forward declaration
struct ID3D11DeviceChild;

namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	/// <summary>
	/// <c>Shader</c> is a shader effect definition.
	/// </summary>
	class Shader : public Core::ReferenceCount
	{
		// Make them friends to be happy!
		friend class Pipeline;
		friend class RenderingContext;

		ID3D11DeviceChild* pShader; //!< Abstracted shader
		IntrusivePtr< RenderingDevice > myDevice;
		Renderer::ShaderType mShaderType; // Shader type associated to this hardware programm

	public:
		/// <summary cref="Shader::Shader">
		/// Constructor.
		/// </summary>
		Shader( const IntrusivePtr< RenderingDevice >& device );

		/// <summary cref="Shader::~Shader">
		/// Destructor.
		/// </summary>
		virtual ~Shader( void );

		/// <summary cref="Shader::Setup">
		/// Setup shader information.
		/// </summary>
		/// <param name="shaderType">Shader type to ensure how programm will be compiled.</param>
		/// <param name="size">Size of shader code.</param>
		/// <param name="src">Shader source code. Must be compiler binary.</param>
		void Setup( Renderer::ShaderType shaderType, Memory::Blob* pBlob );

		/// <summary cref="Shader::Discard">
		/// Discard shader.
		/// </summary>
		void Discard( void );

		/// <summary cref="Shader::IsValid">
		/// </summary>
		bool IsValid( void ) const;
	};

	__IME_INLINED bool Shader::IsValid( void ) const
	{
		return this->pShader != NULL;
	}
}

#endif /// _D3D11ShaderEffect_h_