//============== (C) Copyright 2014, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 shader compiler.
///
/// Author: Pavel Umnikov
//=====================================================================================

#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
#include <Rendering/Direct3D11/D3D11ShaderCompiler.h>
#include <Rendering/Direct3D11/Internal/D3D11ShaderVersionConverter.h>
#include <Rendering/Direct3D11/Internal/D3D11ShaderInclude.h>
#include <Rendering/PrimitiveTopology.h>

#include <D3D11.h>
#include <D3Dcompiler.h>
#include <Rendering/Direct3D11/D3D11Converter.h>

namespace Renderer
{
	bool ShaderCompiler::Compile( _Out_ Memory::Blob** outputBytecode,
								  _In_ const Memory::Blob* inputShaderCode,
								  const CompilerParameters& params,
								  const ShaderType type,
								  const FeatureLevel featureLevel,
								  _In_opt_ const STL::Array< Renderer::ShaderPreprocessor >* preprocessor,
								  const STL::String< Char8 >& shaderName,
								  const STL::String< Char8 >& function,
								  const IntrusivePtr< FileSystem::Stream >& shaderLog )
	{
		__IME_ASSERT_MSG( outputBytecode, "Please produce valid pointer to memory block where bytecode will live!" );
		__IME_ASSERT_MSG( !function.IsEmpty(), "Valid function name must be passed here!" );
		__IME_ASSERT_MSG( !shaderName.IsEmpty(), "Valid shader name must be passed here!" );
		__IME_ASSERT_MSG( inputShaderCode->IsValid(), "Valid input shader text must be produced before compiling!" );

#if defined(DEBUG)
		if (shaderLog.IsValid())
			__IME_ASSERT_MSG(shaderLog->IsOpen(), "Shader log must be opened!");
#endif

		HRESULT hr = S_OK;
		UINT flags1 = 0;
		UINT flags2 = 0;

		D3D_SHADER_MACRO* macroses = NULL;
		if( preprocessor ) {
			SizeT macrosSize = preprocessor->Size(); //!< Compiler needs closure, the last one is NULL
			macroses = static_cast<D3D_SHADER_MACRO*>(__alloca16( sizeof(D3D_SHADER_MACRO)* (macrosSize + 1) ));
			macroses[macrosSize].Name = NULL;
			macroses[macrosSize].Definition = NULL;

			for( SizeT idx = 0; idx < macrosSize; idx++ ) {
				Renderer::ShaderPreprocessor& macro = (*preprocessor)[idx];
				macroses[idx].Name = macro.GetName();
				macroses[idx].Definition = macro.GetDefinition();
			}
		}

		Direct3D11::D3D11ShaderInclude includeParser;
		Renderer::FeatureLevel currentFeatureLevel = featureLevel;
		const s8* shaderTypeStr;
		switch( type ) {
			case Renderer::ShaderType::Vertex:
				shaderTypeStr = Direct3D11::D3D11ShaderVersionConverter::ConvertToVertex( currentFeatureLevel );
				break;

			case Renderer::ShaderType::Pixel:
				shaderTypeStr = Direct3D11::D3D11ShaderVersionConverter::ConvertToPixel( currentFeatureLevel );
				break;

			case Renderer::ShaderType::Geometry:
				shaderTypeStr = Direct3D11::D3D11ShaderVersionConverter::ConvertToGeometry( currentFeatureLevel );
				break;

			case Renderer::ShaderType::Hull:
				shaderTypeStr = "hs_5_0";
				break;

			case Renderer::ShaderType::Domain:
				shaderTypeStr = "ds_5_0";
				break;

			case Renderer::ShaderType::Compute:
				shaderTypeStr = "cs_5_0";
				break;

			default:
				shaderTypeStr = nullptr; //!< this is just to shut up a compiler...
		}

#if defined( DEBUG )
		// Default compiler definitions, used only in debug builds
		flags1 = D3DCOMPILE_DEBUG;
#endif

		flags1 |= D3DCOMPILE_ENABLE_STRICTNESS;
		flags1 |= D3DCOMPILE_IEEE_STRICTNESS;

		if( params.partialPrecision )
			flags1 |= D3DCOMPILE_PARTIAL_PRECISION;
		if( params.threatWarningsAsErrors )
			flags1 |= D3DCOMPILE_WARNINGS_ARE_ERRORS;

		ID3DBlob* pCompileResult = NULL;
		ID3DBlob* pShaderCompilationLog = NULL;

		hr = D3DCompile( inputShaderCode->GetPtr(), 
						 static_cast< SIZE_T >( inputShaderCode->Size() ), 
						 shaderName.AsCharPtr(), 
						 macroses, 
						 static_cast< ID3DInclude* >( &includeParser ), 
						 function.AsCharPtr(), 
						 shaderTypeStr, 
						 flags1,
						 flags2, 
						 &pCompileResult, 
						 &pShaderCompilationLog );
		
		if( pShaderCompilationLog ) {
			FileSystem::Size compilerLogSize = static_cast< FileSystem::Size >( pShaderCompilationLog->GetBufferSize() ); 
			shaderLog->Write( pShaderCompilationLog->GetBufferPointer(), compilerLogSize );
			shaderLog->Flush();
			pShaderCompilationLog->Release();
			pShaderCompilationLog = NULL;
		}

		if( FAILED( hr ) ) {
			return false;
		} else {
			Memory::Blob* result = *outputBytecode;
			result->Set( pCompileResult->GetBufferPointer(), pCompileResult->GetBufferSize() );

			pCompileResult->Release();
			pCompileResult = NULL;

			return true;
		}
	}

	bool ShaderCompiler::Reflect( _In_ const Memory::Blob* inputShaderCode, 
								  const IntrusivePtr< FileSystem::Stream >& shaderLog )
	{
		HRESULT hr = S_OK;
		STL::String< Char8 > reflectionText;
		ID3D11ShaderReflection* reflection = NULL;

		if( !inputShaderCode )
			return false;

		hr = ::D3DReflect( inputShaderCode->GetPtr(), inputShaderCode->Size(), 
						   /*__uuidof(ID3D11ShaderReflection)*/ IID_ID3D11ShaderReflection, 
						   reinterpret_cast< void** >(&reflection) );

		reflectionText.Reserve( 50 );
		//reflection->GetRequiresFlags();

		if( SUCCEEDED(hr) ) {
			D3D11_SHADER_DESC desc;

			reflectionText.Reserve( 1500 );
			reflectionText = "[Debug] Information about shader:\n";
			reflectionText.Append( STL::String< Char8 >::VA( "\tMax MOV instrusions count: %i\n", 
															 reflection->GetMovInstructionCount() ) );
			reflectionText.Append( STL::String< Char8 >::VA( "\tMax MOVC instructions count: %i\n", 
															 reflection->GetMovcInstructionCount() ) );
			reflectionText.Append( STL::String< Char8 >::VA( "\tShader interfaces slots: %i\n", 
															 reflection->GetNumInterfaceSlots() ) );
					
			hr = reflection->GetDesc( &desc );
			if( SUCCEEDED(hr) ) {
				reflectionText.Append( STL::String< Char8 >::VA( "\tArray instrusions count: %i\n", 
																 desc.ArrayInstructionCount ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tBounded shader resources count: %i\n", 
																 desc.BoundResources ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tShader interfaces slots: %i\n",	
																 desc.cBarrierInstructions ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tHull to Domain shader control points count: %i\n", 
																 desc.cControlPoints ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tGeometry shader instances count: %i\n", 
																 desc.cGSInstanceCount ) );

				reflectionText.Append( STL::String< Char8 >::VA( "\tInterlocked functions in shader: %i\n", 
																 desc.cInterlockedInstructions ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tConstant buffers count in shader: %i\n", 
																 desc.ConstantBuffers ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tShader creator name: %s\n", 
																 desc.Creator ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tShader texture writes instructions count: %i\n", 
																 desc.cTextureStoreInstructions ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tShader cut instruction count: %i\n", 
																 desc.CutInstructionCount ) );

				reflectionText.Append( STL::String< Char8 >::VA( "\tShader input/output entries count: %i\n", 
																 desc.DclCount ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tShader constant definitions count: %i\n", 
																 desc.DefCount ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tShader dynamic flow control count: %i\n", 
																 desc.DynamicFlowControlCount ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tShader 'emit' instructions used: %i\n", 
																 desc.EmitInstructionCount ) );
				reflectionText.Append( STL::String< Char8 >::VA( "\tShader FPU instructions used: %i\n", 
																 desc.FloatInstructionCount ) );
			}
					
			reflectionText.Append( STL::String< Char8 >::VA( "\tGeometry shader input primitive type: %i", 
															 reflection->GetGSInputPrimitive() ) );

			UINT tX, tY, tZ, tOverall;
			tOverall = reflection->GetThreadGroupSize( &tX, &tY, &tZ );
			reflectionText.Append( STL::String< Char8 >::VA( "\tCompute shader thread group size: %i(x: %i; y: %i; z: %i)", 
															 tOverall, 
															 tX, 
															 tY, 
															 tZ ) );
					
			ID3DBlob* disassembled = NULL;
			hr = ::D3DDisassemble( inputShaderCode->GetPtr(), inputShaderCode->Size(), 0, 0, &disassembled );
			if( SUCCEEDED( hr ) ) {
				reflectionText.Append( reinterpret_cast< Char8* >( disassembled->GetBufferPointer() ) );
			}
			disassembled->Release();
			disassembled = NULL;

			FileSystem::Size reflectionTextSize = static_cast< FileSystem::Size >(STL::CountOf(reflectionText));
			shaderLog->Write( reflectionText.AsCharPtr(), reflectionTextSize );
			reflection->Release();
			reflection = nullptr;

			return true;								
		} else {
			reflectionText = "[Debug] Failed to retrieve debug information from Shader Reflection.";
			FileSystem::Size reflectionTextSize = static_cast< FileSystem::Size >(STL::CountOf(reflectionText));
			shaderLog->Write( reflectionText.AsCharPtr(), reflectionTextSize );

			return false;
		}
	}
}