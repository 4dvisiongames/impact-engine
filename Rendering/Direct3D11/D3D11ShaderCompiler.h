//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 shader compiler.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#include <Foundation/STLString.h>
#include <Foundation/MemoryBlob.h>
#include <Foundation/Stream.h>
#include <Rendering/FeatureLevels.h>
#include <Rendering/ShaderType.h>
#include <Rendering/ShaderCompilerParameters.h>
#include <Rendering/ShaderPreprocessor.h>

namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	class ShaderCompiler
	{
	public:
		/// <summary cref="ShaderCompiler::Compiler">
		/// Compiled shader plain text to Direct3D-compatible bytecode via D3DCompile.
		/// </summary>
		/// <param name="outputBytecode">Pointer to the output compiled shader bytecode memory blob pointer.</param>
		/// <param name="inputShaderCode">Pointer to the input shader plain text code.</param>
		/// <param name="params">Compilation parameters used for shader building.</param>
		/// <param name="type">Type of shader to be processed.</param>
		/// <param name="featureLevel">Feature level(shader mode version) to be used while compilation.</param>
		/// <param name="preprocessor">Pointer to the array of preprocessor definitions.</param>
		/// <param name="shaderName">Name of the shader.</param>
		/// <param name="function">Shader entry function.</param>
		/// <param name="shaderLog">Reference to the log where compilation information will live.</param>
		/// <param name="provideDebugInformation">Should compiler provide debgginh information like assembly bytecode?</param>
		static bool Compile( _Out_ Memory::Blob** outputBytecode,
							 _In_ const Memory::Blob* inputShaderCode,
							 const Renderer::CompilerParameters& params, 
							 const Renderer::ShaderType type, 
							 const Renderer::FeatureLevel featureLevel, 
							 _In_opt_ const STL::Array< Renderer::ShaderPreprocessor >* preprocessor, 
							 const STL::String< Char8 >& shaderName, 
							 const STL::String< Char8 >& function, 
							 const IntrusivePtr< FileSystem::Stream >& shaderLog );

		static bool Reflect( _In_ const Memory::Blob* inputShaderCode,
							 const IntrusivePtr< FileSystem::Stream >& shaderLog );
	};
}