//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: DXGI swap chain defintions.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Direct3D11 swap chain
#include <Rendering/Direct3D11/D3D11SwapChain.h>
/// Display adapter
#include <Rendering/Direct3D11/D3D11DisplayAdapter.h>
/// D3D11 rendering device
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// D3D11 texture
#include <Rendering/Direct3D11/D3D11Texture.h>

#include <D3D11.h>
#include <Rendering/Direct3D11/D3D11Converter.h>

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	SwapChain::SwapChain( const IntrusivePtr< RenderingDevice >& device ) 
		: myD3D11SwapChain( nullptr )
		, myRenderingDevice( device )
	{
		/// Empty
	}

	SwapChain::~SwapChain( void )
	{
		if( this->IsValid() )
			this->Discard();
	}

	void SwapChain::Setup( HWND hwnd, u32 width, u32 height, bool fullscreen, SizeT outputBuffers )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myRenderingDevice.IsValid() );

		DXGI_SWAP_CHAIN_DESC desc = { 0 };
		desc.OutputWindow = hwnd;
		/// Rendering view
		desc.Windowed = fullscreen ? 0 : 1;
		desc.BufferDesc.Width = width;
		desc.BufferDesc.Height = height;
		desc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		desc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		desc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		desc.BufferDesc.RefreshRate.Numerator = 60;
		desc.BufferDesc.RefreshRate.Denominator = 1;
		desc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		/// Misc
		desc.BufferCount = static_cast< UINT >( outputBuffers ); //!< Two prebuild buffers for ultra-smooth switching
		desc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT | DXGI_USAGE_BACK_BUFFER; //!< Output it only to render target
		/// MSAA
		desc.SampleDesc.Count = 1;
		desc.SampleDesc.Quality = 0;

		/// Create selected swap chain
		ID3D11Device* pDevice = this->myRenderingDevice->myD3D11Device;

		IDXGIDevice1* dxgiDevice = NULL;
		HRESULT hr = pDevice->QueryInterface( IID_IDXGIDevice1, reinterpret_cast< void** >( &dxgiDevice ) );

		IDXGIAdapter1* dxgiAdapter = NULL;
		hr = dxgiDevice->GetParent( IID_IDXGIAdapter1, reinterpret_cast< void** >( &dxgiAdapter ) );

		IDXGIFactory1* dxgiFactory = NULL;
		hr = dxgiAdapter->GetParent( IID_IDXGIFactory1, reinterpret_cast< void** >( &dxgiFactory ) );

		hr = dxgiFactory->CreateSwapChain( pDevice, &desc, &this->myD3D11SwapChain );

		dxgiDevice->Release();
		dxgiAdapter->Release();
		dxgiFactory->Release();

		if( FAILED( hr ) )
			NDKThrowException( Debug::EExceptionCodes::RendererError,
							   "[DXGI] Failed to create swap chain! Please update drivers and check your video-adapter.",
							   "D3D11SwapChain::CreateSwapChain( HWND, u32, u32, bool )" );
	}

	void SwapChain::Discard( void )
	{
		/// Release swap chain
		if( this->myD3D11SwapChain ) { this->myD3D11SwapChain->Release(); this->myD3D11SwapChain = nullptr; }
		this->myRenderingDevice = nullptr;
	}

	bool SwapChain::GetProperties( u32* width, u32* height, Renderer::TextureFormat* format )
	{
		__IME_ASSERT( this->IsValid() );

		DXGI_SWAP_CHAIN_DESC desc;
		if( FAILED( this->myD3D11SwapChain->GetDesc( &desc ) ) )
			return false;

		if( width )
			*width = desc.BufferDesc.Width;

		if( height )
			*height = desc.BufferDesc.Height;

		if( format )
			*format = Direct3D11::D3D11Converter::ToEngineTexture( desc.BufferDesc.Format );

		return false;
	}

	bool SwapChain::GetSurface( IntrusivePtr< Renderer::Texture >& rhs )
	{
		if( !rhs.IsValid() )
			return false;

		if( rhs->IsValid() )
			return false;

		DXGI_SWAP_CHAIN_DESC desc;
		this->myD3D11SwapChain->GetDesc( &desc );
		rhs.Downcast< Base::BaseTexture >()->Setup( desc.BufferDesc.Width, 
													desc.BufferDesc.Height,
													0,
													1,
													desc.SampleDesc.Count,
													desc.SampleDesc.Quality,
													Direct3D11::D3D11Converter::ToEngineTexture(desc.BufferDesc.Format),
													Renderer::TextureTypeEnum::Tex2D,
													true );

		ID3D11Texture2D* pBackbuffer = nullptr;
		if( FAILED( this->myD3D11SwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), reinterpret_cast< void** >( &pBackbuffer ) ) ) )
			return false;

		rhs->myTexture = static_cast< ID3D11Resource* >( pBackbuffer );

		return true;
	}

	void SwapChain::ResizeBackBuffer( bool fullscreen )
	{
		__IME_ASSERT( this->IsValid() );

		IDXGIOutput* myDXGIOutput = NULL;
		BOOL fullScreenState = fullscreen;
		BOOL currentScreenState = 0;

		this->myD3D11SwapChain->GetFullscreenState( &currentScreenState, &myDXGIOutput );
		if( fullScreenState != currentScreenState )
			this->myD3D11SwapChain->SetFullscreenState( fullScreenState, NULL );

		this->myD3D11SwapChain->ResizeBuffers( 2, 0, 0, DXGI_FORMAT_UNKNOWN, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH );
	}
}