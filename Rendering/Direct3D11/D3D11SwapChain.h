//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: DXGI swap chain defintions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _D3D11SwapChain_h_
#define _D3D11SwapChain_h_

/// Reference counter
#include <Foundation/RefCount.h>
/// Engine formats
#include <Rendering/TextureFormat.h>

struct IDXGISwapChain;
struct ID3D11RenderTargetView;

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	class SwapChain : public Core::ReferenceCount
	{
		friend class RenderingDevice;
		friend class Texture;

		IDXGISwapChain* myD3D11SwapChain; //!< Device swap chain
		IntrusivePtr< RenderingDevice > myRenderingDevice;
		bool myFullScreenState;

	public:
		SwapChain( const IntrusivePtr< RenderingDevice >& device );
		virtual ~SwapChain( void );

		void Setup( HWND hwnd, u32 width, u32 height, bool fullscreen, SizeT outputBuffers = 1 );
		void Discard( void );
		bool IsValid( void ) const;

		bool GetProperties( u32* width, u32* height, Renderer::TextureFormat* format );

		/// <summary cref="SwapChain::GetSurface">
		/// Copies pointer of the backbuffer of swap chain to texture object.
		/// </summary>
		/// <remarks>Please note that texture must be just allocated and must not be valid(e.g. !IsValid)</remarks>
		bool GetSurface( IntrusivePtr< Renderer::Texture >& rhs );

		/// <summary cref="SwapChain::SwitchResize">
		/// Switches current adapter to fullscreen if current output is to windowed mode, or
		/// vise versa.
		/// </summary>
		void ResizeBackBuffer( bool fullscreen = false );
	};

	__IME_INLINED bool SwapChain::IsValid( void ) const
	{
		return this->myD3D11SwapChain != nullptr;
	}
}

#endif /// _D3D11SwapChain_h_