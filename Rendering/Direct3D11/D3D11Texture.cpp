//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 texture definitions used in engine.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Renderer
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// D3D11 texture
#include <Rendering/Direct3D11/D3D11Texture.h>

/// Direct3D11
#include <D3D11.h>
#include <Rendering/Direct3D11/D3D11Converter.h>

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	static __IME_INLINED UINT Tex_CalculatePitch( Renderer::TextureFormat format, SizeT MyWidth )
	{
		UINT result = 0;
		UINT width = static_cast< UINT >( MyWidth );

		switch( format ) {
			case Renderer::TextureFormatEnum::RGBA8:
			case Renderer::TextureFormatEnum::RG16F:
			case Renderer::TextureFormatEnum::R32F:
				result = width * 4;
				break;

			case Renderer::TextureFormatEnum::DXT1:
				result = Math::TMax< UINT >( width / 4U, 1U ) * 8;
				break;

			case Renderer::TextureFormatEnum::DXT3:
			case Renderer::TextureFormatEnum::DXT5:
				result = Math::TMax< UINT >( width / 4U, 1U ) * 16;
				break;

			case Renderer::TextureFormatEnum::RGBA16F:
				result = width * 16;
				break;

			case Renderer::TextureFormatEnum::RGB8E5:
				result = width * 7;
		}

		return result;
	}

	static __IME_INLINED UINT Tex_GetMipMaps( SizeT width, SizeT height, SizeT depth )
	{
		UINT mipCount = 1;
		while( width > 1 || height > 1 || depth > 1 ) {
			width >>= 1;
			height >>= 1;
			depth >>= 1;
			mipCount++;
		}

		return mipCount;
	}


	Texture::Texture( const IntrusivePtr< RenderingDevice >& device ) 
		: Base::BaseTexture()
		, myDevice( device )
		, myTexture( nullptr )
		, myShaderResourceView( nullptr )
	{
		/// Empty
	}

	Texture::~Texture( void )
	{
		/// Release texture object
		if( this->IsValid() ) {
			this->Discard();
		}
	}

	void Texture::Setup( Texture1DDesc& desc, u32 bindFlags, UsageFlags usageFlag, u32 cpuAccess, void* data, u32 size )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid() ); //!< Pointer validation
		__IME_ASSERT( this->myDevice->IsValid() ); //!< Device validation

		__IME_ASSERT( desc.myWidth > 0 );

		Base::BaseTexture::Setup( desc.myWidth, 0, 0, desc.mySlices, 1, 0, desc.myFormat, TextureTypeEnum::Tex1D, desc.rendereable );
		ID3D11Device* device = this->myDevice->myD3D11Device;
		UINT d3dCpuAccess = 0;
		UINT d3dBindFlags = D3D11_BIND_SHADER_RESOURCE;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::DepthStencilView) != 0 )
			d3dBindFlags |= D3D11_BIND_DEPTH_STENCIL;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::RenderTargetView) != 0 )
			d3dBindFlags |= D3D11_BIND_RENDER_TARGET;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::UnorederedAccessView) != 0 )
			d3dBindFlags |= D3D11_BIND_UNORDERED_ACCESS;

		/// Define texture
		D3D11_TEXTURE1D_DESC texdesc;
		/// Erase memory block
		Memory::LowLevelMemory::EraseMemoryBlock( &texdesc, sizeof(D3D11_TEXTURE1D_DESC) );
		texdesc.Width = this->myWidth;
		texdesc.MipLevels = 1;
		texdesc.ArraySize = (UINT)this->mySlices;
		texdesc.Format = Direct3D11::D3D11Converter::ToD3D( this->myFormat );
		texdesc.Usage = Direct3D11::D3D11Converter::ToD3D( usageFlag );
		texdesc.BindFlags = d3dBindFlags;
		texdesc.CPUAccessFlags = d3dCpuAccess;
		texdesc.MiscFlags = NULL;

		HRESULT hr = S_OK;
		ID3D11Texture1D* texture = NULL;
		D3D11_SUBRESOURCE_DATA subres;
		if( data != nullptr && size > 0 ) {
			Memory::LowLevelMemory::EraseMemoryBlock( &subres, sizeof( D3D11_SUBRESOURCE_DATA ) );
			subres.pSysMem = data;
		}

		hr = device->CreateTexture1D( &texdesc, (data != nullptr) ? &subres : NULL, &texture );

		if( FAILED( hr ) ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging( ) ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11Texture::CreateTexture( void )" );
			} else
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create 1-dimentional texture resource!",
									"D3D11Texture::CreateTexture( void )" );
		}

		ID3D11ShaderResourceView* srv = NULL;
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &srvDesc, sizeof(srvDesc) );
		srvDesc.Format = texdesc.Format;
		
		if( desc.mySlices > 1 ) {
			srvDesc.Texture1DArray.ArraySize = texdesc.ArraySize;
			srvDesc.Texture1DArray.FirstArraySlice = 0;
			srvDesc.Texture1DArray.MipLevels = -1;
			srvDesc.Texture1DArray.MostDetailedMip = 0;
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1DARRAY;
		} else {
			srvDesc.Texture1D.MipLevels = -1;
			srvDesc.Texture1D.MostDetailedMip = 0;
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE1D;
		}

		hr = device->CreateShaderResourceView( texture, &srvDesc, &srv );

		if( FAILED( hr ) ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging( ) ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11Texture::CreateTexture( void )" );
			} else
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create 1-dimentional texture shader resource view!",
									"D3D11Texture::CreateTexture( void )" );
		}

		this->myTexture = texture;
		this->myShaderResourceView = srv;
	}

	void Texture::Setup( Texture2DDesc& desc, u32 bindFlags, UsageFlags usageFlag, u32 cpuAccess, void* data, u32 size )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid() );
		__IME_ASSERT( this->myDevice->IsValid() );

		__IME_ASSERT( desc.myWidth > 0 );
		__IME_ASSERT( desc.myHeight > 1 );

		Base::BaseTexture::Setup( desc.myWidth, desc.myHeight, 0, desc.mySlices, desc.myMSAA.mySampleCount, desc.myMSAA.mySampleQuality, desc.myFormat, TextureTypeEnum::Tex2D, desc.rendereable );
		ID3D11Device* device = this->myDevice->myD3D11Device;
		UINT d3dCpuAccess = 0;
		UINT d3dBindFlags = D3D11_BIND_SHADER_RESOURCE;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::DepthStencilView) != 0 )
			d3dBindFlags |= D3D11_BIND_DEPTH_STENCIL;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::RenderTargetView) != 0 )
			d3dBindFlags |= D3D11_BIND_RENDER_TARGET;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::UnorederedAccessView) != 0 )
			d3dBindFlags |= D3D11_BIND_UNORDERED_ACCESS;

		/// Define texture
		D3D11_TEXTURE2D_DESC texdesc;
		/// Erase memory block
		Memory::LowLevelMemory::EraseMemoryBlock( &texdesc, sizeof(D3D11_TEXTURE2D_DESC) );
		texdesc.Width = this->myWidth;
		texdesc.Height = this->myHeight;
		texdesc.MipLevels = 1;
		texdesc.ArraySize = (UINT)this->mySlices;
		texdesc.Format = Direct3D11::D3D11Converter::ToD3D( this->myFormat );
		texdesc.SampleDesc.Count = this->myMSAALevel;
		texdesc.SampleDesc.Quality = this->myMSAAQuality;
		texdesc.Usage = Direct3D11::D3D11Converter::ToD3D( usageFlag );
		texdesc.BindFlags = d3dBindFlags;
		texdesc.CPUAccessFlags = d3dCpuAccess;
		texdesc.MiscFlags = NULL;
		
		HRESULT hr = S_OK;
		ID3D11Texture2D* texture = NULL;
		D3D11_SUBRESOURCE_DATA subres;
		if( data != nullptr && size > 0 ) {
			Memory::LowLevelMemory::EraseMemoryBlock( &subres, sizeof( D3D11_SUBRESOURCE_DATA ) );
			subres.pSysMem = data;
			subres.SysMemPitch = Tex_CalculatePitch( this->myFormat, this->myWidth );
		} 

		hr = device->CreateTexture2D( &texdesc, (data != nullptr) ? &subres : NULL, &texture );

		if( FAILED( hr ) ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging() ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11Texture::CreateTexture( void )" );
			} else
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create 2-dimentional texture resource!",
									"D3D11Texture::CreateTexture( void )" );
		}


		ID3D11ShaderResourceView* srv = NULL;
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC) );
		srvDesc.Format = texdesc.Format;

		if( desc.myMSAA.mySampleCount > 1 ) {
			if( desc.mySlices > 1 ) {
				srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMSARRAY;
				srvDesc.Texture2DMSArray.ArraySize = texdesc.ArraySize;
				srvDesc.Texture2DMSArray.FirstArraySlice = 0;
			} else {
				srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DMS;
			}
		} else {
			if( desc.mySlices > 1 ) {
				srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
				srvDesc.Texture2D.MipLevels = -1;
				srvDesc.Texture2D.MostDetailedMip = 0;
			} else {
				srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
				srvDesc.Texture2DArray.ArraySize = texdesc.ArraySize;
				srvDesc.Texture2DArray.FirstArraySlice = 0;
				srvDesc.Texture2DArray.MipLevels = -1;
				srvDesc.Texture2DArray.MostDetailedMip = 0;
			}
		}

		hr = device->CreateShaderResourceView( texture, &srvDesc, &srv );

		if( FAILED( hr ) ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging() ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11Texture::CreateTexture( void )" );
			} else 
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create 2-dimentional texture shader resource view!",
									"D3D11Texture::CreateTexture( void )" );
		}

		this->myTexture = texture;
		this->myShaderResourceView = srv;
	}

	void Texture::Setup( Texture3DDesc& desc, u32 bindFlags, UsageFlags usageFlag, u32 cpuAccess, void* data, u32 size )
	{
		__IME_ASSERT( !this->IsValid( ) );
		__IME_ASSERT( this->myDevice.IsValid( ) );
		__IME_ASSERT( this->myDevice->IsValid( ) );

		__IME_ASSERT( desc.myWidth > 0 );
		__IME_ASSERT( desc.myHeight > 1 );
		__IME_ASSERT( desc.myDepth > 1 );

		Base::BaseTexture::Setup( desc.myWidth, desc.myHeight, desc.myDepth, desc.mySlices, 1, 0, desc.myFormat, TextureTypeEnum::Tex2D, desc.rendereable );
		ID3D11Device* device = this->myDevice->myD3D11Device;
		UINT d3dCpuAccess = 0;
		UINT d3dBindFlags = D3D11_BIND_SHADER_RESOURCE;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::DepthStencilView) != 0 )
			d3dBindFlags |= D3D11_BIND_DEPTH_STENCIL;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::RenderTargetView) != 0 )
			d3dBindFlags |= D3D11_BIND_RENDER_TARGET;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::UnorederedAccessView) != 0 )
			d3dBindFlags |= D3D11_BIND_UNORDERED_ACCESS;

		/// Define texture
		D3D11_TEXTURE3D_DESC texdesc;
		/// Erase memory block
		Memory::LowLevelMemory::EraseMemoryBlock( &texdesc, sizeof(D3D11_TEXTURE3D_DESC) );
		texdesc.Width = this->myWidth;
		texdesc.Height = this->myHeight;
		texdesc.Depth = this->myDepth;
		texdesc.MipLevels = 1;
		texdesc.Format = Direct3D11::D3D11Converter::ToD3D( this->myFormat );
		texdesc.Usage = Direct3D11::D3D11Converter::ToD3D( usageFlag );
		texdesc.BindFlags = d3dBindFlags;
		texdesc.CPUAccessFlags = d3dCpuAccess;
		texdesc.MiscFlags = NULL;
		
		HRESULT hr = S_OK;
		ID3D11Texture3D* texture = NULL;
		D3D11_SUBRESOURCE_DATA subres;
		if( data != nullptr && size > 0 ) {
			Memory::LowLevelMemory::EraseMemoryBlock( &subres, sizeof(D3D11_SUBRESOURCE_DATA) );
			subres.pSysMem = data;
		}

		hr = device->CreateTexture3D( &texdesc, (data != nullptr) ? &subres : NULL, &texture );

		if( FAILED( hr ) ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging( ) ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11Texture::CreateTexture( void )" );
			} else
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create 3-dimentional texture resource!",
									"D3D11Texture::CreateTexture( void )" );
		}

		ID3D11ShaderResourceView* srv = NULL;
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &srvDesc, sizeof(srvDesc) );
		srvDesc.Format = texdesc.Format;

		srvDesc.Texture3D.MipLevels = -1;
		srvDesc.Texture3D.MostDetailedMip = 0;
		srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE3D;

		hr = device->CreateShaderResourceView( texture, &srvDesc, &srv );

		if( FAILED( hr ) ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging( ) ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11Texture::CreateTexture( void )" );
			} else
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create 3-dimentional texture shader resource view!",
									"D3D11Texture::CreateTexture( void )" );
		}

		this->myTexture = texture;
		this->myShaderResourceView = srv;
	}

	void Texture::Setup( TextureCubeDesc& desc, u32 bindFlags, UsageFlags usageFlag, u32 cpuAccess, void* data, u32 size )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( this->myDevice.IsValid() );
		__IME_ASSERT( this->myDevice->IsValid() );

		__IME_ASSERT( desc.myWidth > 0 );
		__IME_ASSERT( desc.myHeight > 1 );

		Base::BaseTexture::Setup( desc.myWidth, desc.myHeight, 0, desc.mySlices, 1, 0, desc.myFormat, TextureTypeEnum::TexCube, desc.rendereable );
		ID3D11Device* device = this->myDevice->myD3D11Device;
		UINT d3dCpuAccess = 0;
		UINT d3dBindFlags = D3D11_BIND_SHADER_RESOURCE;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::DepthStencilView) != 0 )
			d3dBindFlags |= D3D11_BIND_DEPTH_STENCIL;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::RenderTargetView) != 0 )
			d3dBindFlags |= D3D11_BIND_RENDER_TARGET;

		if( (bindFlags & Renderer::TextureBindFlagsEnum::UnorederedAccessView) != 0 )
			d3dBindFlags |= D3D11_BIND_UNORDERED_ACCESS;

		/// Define texture
		D3D11_TEXTURE2D_DESC texdesc;
		/// Erase memory block
		Memory::LowLevelMemory::EraseMemoryBlock( &texdesc, sizeof(D3D11_TEXTURE2D_DESC) );
		texdesc.Width = this->myWidth;
		texdesc.Height = this->myHeight;
		texdesc.MipLevels = 1;
		texdesc.ArraySize = (UINT)this->mySlices * 6;
		texdesc.Format = Direct3D11::D3D11Converter::ToD3D( this->myFormat );
		texdesc.SampleDesc.Count = this->myMSAALevel;
		texdesc.SampleDesc.Quality = this->myMSAAQuality;
		texdesc.Usage = Direct3D11::D3D11Converter::ToD3D( usageFlag );
		texdesc.BindFlags = d3dBindFlags;
		texdesc.CPUAccessFlags = d3dCpuAccess;
		texdesc.MiscFlags = NULL;
		
		HRESULT hr = S_OK;
		ID3D11Texture2D* texture = NULL;
		D3D11_SUBRESOURCE_DATA subres;
		if( data != nullptr && size > 0 ) {
			Memory::LowLevelMemory::EraseMemoryBlock( &subres, sizeof( D3D11_SUBRESOURCE_DATA ) );
			subres.pSysMem = data;
			subres.SysMemPitch = Tex_CalculatePitch( this->myFormat, this->myWidth );
		} 

		hr = device->CreateTexture2D( &texdesc, (data != nullptr) ? &subres : NULL, &texture );

		if( FAILED( hr ) ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging() ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11Texture::CreateTexture( void )" );
			} else
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create cube texture resource!",
									"D3D11Texture::CreateTexture( void )" );
		}


		ID3D11ShaderResourceView* srv = NULL;
		D3D11_SHADER_RESOURCE_VIEW_DESC srvDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &srvDesc, sizeof(D3D11_SHADER_RESOURCE_VIEW_DESC) );
		srvDesc.Format = texdesc.Format;

		if( desc.mySlices > 1 ) {
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBEARRAY;
			srvDesc.TextureCubeArray.First2DArrayFace = 0;
			srvDesc.TextureCubeArray.NumCubes = (UINT)desc.mySlices;
			srvDesc.TextureCubeArray.MipLevels = -1;
			srvDesc.TextureCubeArray.MostDetailedMip = 0;
		} else {
			srvDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURECUBE;
			srvDesc.TextureCube.MipLevels = -1;
			srvDesc.TextureCube.MostDetailedMip = 0;
		}

		hr = device->CreateShaderResourceView( texture, &srvDesc, &srv );

		if( FAILED( hr ) ) {
#if defined( DEBUG )
			if( this->myDevice->IsDebugging() ) {
				NDKThrowException( Debug::EExceptionCodes::RendererError,
								   this->myDevice->GetLastError().AsCharPtr(),
								   "D3D11Texture::CreateTexture( void )" );
			} else 
#endif
				NDKThrowException( Debug::EExceptionCodes::RendererError,
									"Failed to create cube texture shader resource view!",
									"D3D11Texture::CreateTexture( void )" );
		}

		this->myTexture = texture;
		this->myShaderResourceView = srv;
	}

	void Texture::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		/// Release texture definitions
		if( this->myTexture ) { 
			this->myTexture->Release(); 
			this->myTexture = nullptr;
		}

		if( this->myShaderResourceView ) {
			this->myShaderResourceView->Release();
			this->myShaderResourceView = nullptr;
		}

		this->myDevice = nullptr;
	}
}