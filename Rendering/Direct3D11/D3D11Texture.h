//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 texture definitions used in engine.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _D3D11Texture_h_
#define _D3D11Texture_h_

/// Base texture
#include <Rendering/Base/BaseTexture.h>
/// Texture flags
#include <Rendering/TextureFlags.h>
/// CPU Access flags
#include <Rendering/CPUAccess.h>
/// Usage type
#include <Rendering/UsageType.h>
/// Texture descriptions
#include <Rendering/TextureDesc.h>

/// Forward declaration
struct ID3D11Resource;
struct ID3D11ShaderResourceView;

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	/// <summary>
	/// <c>Texture</c> is a Direct3D11 texture implementation.
	/// </summary>
	class Texture : public Base::BaseTexture
	{
	protected:
		friend class DepthBuffer;
		friend class RenderTarget;
		friend class SwapChain;

		IntrusivePtr< RenderingDevice > myDevice;
		ID3D11Resource* myTexture; //!< Abstracted texture as resource
		ID3D11ShaderResourceView* myShaderResourceView; //! My shader resource view attached to a resource

	public:
		/// <summary cref="Texture::Texture">
		/// Constructor.
		/// </summary>
		Texture( const IntrusivePtr< RenderingDevice >& device );

		/// <summary cref="Texture::~Texture">
		/// Destructor.
		/// </summary>
		virtual ~Texture( void );
		
		/// <summary cref="Texture::Texture">
		/// Create 1D texture resource.
		/// </summary>
		/// <param name="in">Input 1D texture parameters.</param>
		/// <param name="bindFlag"></param>
		void Setup( Texture1DDesc& desc, u32 bindFlags, UsageFlags usageFlag, u32 cpuAccess = 0, void* data = nullptr, u32 size = 0 );

		/// <summary cref="Texture::Texture">
		/// Create 2D texture resource.
		/// </summary>
		/// <param name="in">Input 2D texture parameters.</param>
		/// <param name="bindFlag"></param>
		void Setup( Texture2DDesc& desc, u32 bindFlags, UsageFlags usageFlag, u32 cpuAccess = 0, void* data = nullptr, u32 size = 0 );

		/// <summary cref="Texture::Texture">
		/// Create 3D texture resource.
		/// </summary>
		/// <param name="in">Input 3D texture parameters.</param>
		/// <param name="bindFlag"></param>
		void Setup( Texture3DDesc& desc, u32 bindFlags, UsageFlags usageFlag, u32 cpuAccess = 0, void* data = nullptr, u32 size = 0 );

		void Setup( TextureCubeDesc& desc, u32 bindFlags, UsageFlags usageFlag, u32 cpuAccess = 0, void* data = nullptr, u32 size = 0 );

		/// <summary cref="Texture::ReleaseTexture">
		/// Release already created texture.
		/// </summary>
		void Discard( void );

		/// <summary cref="Texture::IsValid">
		/// </summary>
		bool IsValid( void ) const;
	};

	__IME_INLINED bool Texture::IsValid( void ) const
	{
		return this->myTexture != nullptr;
	}
}

#endif /// _D3D11Texture_h_