//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Direct3D11 vertex buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Direct3D11 vertex buffer
#include <Rendering/Direct3D11/D3D11VertexBuffer.h>
/// Direct3D11 rendering device
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
/// Direct3D11
#include <D3D11.h>
/// Direct3D11 converter
#include <Rendering/Direct3D11/D3D11Converter.h>

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	VertexBuffer::VertexBuffer( const IntrusivePtr< Renderer::RenderingDevice >& device ) 
		: myDevice( device )
		, myBuffer( nullptr )
	{
		/// Empty
	}

	VertexBuffer::~VertexBuffer( void )
	{
		if( this->IsValid() )
			this->Discard();
	}

	void VertexBuffer::Setup( const VertexBufferDesc& desc )
	{
		/// Some assertions
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( desc.myVertexCount > 0 );
		__IME_ASSERT( desc.myVertexSize > 0 );

		ID3D11Device* device = this->myDevice->myD3D11Device;

		HRESULT hr = S_FALSE;
		/// Read/Write permissions for CPU
		UINT cpuAccess = 0;
		if( (desc.myCPUAccessMask & CPUAccessFlags::ReadAccess) != 0 ) 
			cpuAccess |= D3D11_CPU_ACCESS_READ;
		if( (desc.myCPUAccessMask & CPUAccessFlags::WriteAccess) != 0 ) 
			cpuAccess |= D3D11_CPU_ACCESS_WRITE;

		/// Vertex buffer description
		D3D11_BUFFER_DESC vbDesc;
		Memory::LowLevelMemory::EraseMemoryBlock( &vbDesc, sizeof( D3D11_BUFFER_DESC ) );
		vbDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vbDesc.ByteWidth = static_cast< UINT >( desc.myVertexSize * desc.myVertexCount );
		vbDesc.CPUAccessFlags = cpuAccess;
		vbDesc.Usage = Direct3D11::D3D11Converter::ToD3D( desc.myUsageFlags );

#if defined( DEBUG )
		if( vbDesc.Usage == D3D11_USAGE_IMMUTABLE )
			__IME_ASSERT_MSG( desc.myData != nullptr, "Index buffer is defined as static, you must provide data to store" );
#endif

		/// Data to write to the buffer
		D3D11_SUBRESOURCE_DATA subRes;
		if( desc.myData ) {
			Memory::LowLevelMemory::EraseMemoryBlock( &subRes, sizeof(D3D11_SUBRESOURCE_DATA) );
			subRes.pSysMem = desc.myData;
			subRes.SysMemPitch = static_cast< UINT >( desc.myVertexSize );
		}

		/// Create vertex buffer
		if( FAILED( device->CreateBuffer( &vbDesc, (desc.myData != nullptr) ? &subRes : nullptr, &this->myBuffer ) ) ) {
			NDKThrowException( Debug::EExceptionCodes::RendererError,
								"Failed to create 2-dimentional texture resource!",
								"VertexBuffer::Setup( void*, SizeT )" );
		}
	}

	void VertexBuffer::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		this->myBuffer->Release();
		this->myBuffer = nullptr;

		this->myDevice = nullptr;
	}
}