//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Direct3D11 vertex buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _D3D11VertexBuffer_h_
#define _D3D11VertexBuffer_h_

/// Reference counting
#include <Foundation/RefCount.h>
/// Vertex buffer description
#include <Rendering/VertexBufferDesc.h>

/// Forward declaration
struct ID3D11Buffer;

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	/// Forward declaration
	class RenderingDevice;

	/// <summary>
	/// <c>D3D11VertexBuffer</c> is a Direct3D11 vertex buffer definiton.
	/// </summary>
	class VertexBuffer : public Core::ReferenceCount
	{
	protected:
		friend class RenderingContext;

		IntrusivePtr< Renderer::RenderingDevice > myDevice;
		ID3D11Buffer* myBuffer;

	public:
		/// <summary cref="D3D11VertexBuffer::D3D11VertexBuffer">
		/// Constructor.
		/// </summary>
		VertexBuffer( const IntrusivePtr< Renderer::RenderingDevice >& device );

		/// <summary cref="D3D11VertexBuffer::~D3D11VertexBuffer">
		/// Destructor.
		/// </summary>
		virtual ~VertexBuffer( void );

		/// <summary cref="D3D11VertexBuffer::CreateVertexBuffer">
		/// Creates vertex buffer instance and fills with needed data.
		/// </summary>
		/// <param name="desc">Reference to the description.</param>
		void Setup( const VertexBufferDesc& desc );

		/// <summary cref="D3D11VertexBuffer::ReleaseVertexBuffer">
		/// Release existing vertex buffer.
		/// </summary>
		void Discard( void );

		/// <summary cref="D3D11VertexBuffer::IsValid">
		/// Validate current vertex buffer.
		/// </summary>
		bool IsValid( void ) const;
	};

	__IME_INLINED bool VertexBuffer::IsValid( void ) const
	{
		return this->myBuffer != nullptr;
	}
}

#endif /// _D3D11VertexBuffer_h_