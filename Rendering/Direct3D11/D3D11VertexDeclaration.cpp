/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <Rendering/Direct3D11/D3D11VertexDeclaration.h>
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
#include <Foundation/MemoryScopedStackAllocator.h>

#include <d3d11.h>
#include <Rendering/Direct3D11/D3D11Converter.h>

namespace Renderer
{
	VertexDeclaration::VertexDeclaration( const IntrusivePtr< Renderer::RenderingDevice >& device )
		: myDevice( device )
		, myInputLayout( nullptr )
		, myValidation( false )
	{
	}

	VertexDeclaration::~VertexDeclaration( void )
	{
		if( this->IsValid() )
			this->Discard();
	}

	void VertexDeclaration::Setup( const STL::String< Char8 >& name,
								   const STL::Array< Renderer::VertexElement >& rhs,
								   const Memory::Blob* compiledProgram )
	{
		__IME_ASSERT( !this->IsValid() );
		__IME_ASSERT( name.IsValid() );
		__IME_ASSERT( !rhs.IsEmpty( ) );
		__IME_ASSERT( compiledProgram->IsValid() );

		Memory::ScopedStack< 2048 > stackAlloc;

		HRESULT hr = S_OK;
		SizeT vertexElementsCount = rhs.Size();
		ID3D11Device* device = this->myDevice->myD3D11Device;

		stackAlloc << [&]() {
			auto elements = stackAlloc.ConstructArray<D3D11_INPUT_ELEMENT_DESC>(vertexElementsCount);
			Memory::LowLevelMemory::EraseMemoryBlock(&elements, sizeof(D3D11_INPUT_ELEMENT_DESC) * vertexElementsCount);

			this->myName = name;

			for (SizeT index = 0; index < vertexElementsCount; index++) {
				Renderer::VertexElement& element = rhs[index];
				D3D11_INPUT_ELEMENT_DESC& desc = elements[index];
				desc.InputSlot = static_cast<UINT>(index);
				desc.SemanticName = element.elementName.AsCharPtr();
				desc.AlignedByteOffset = D3D11_APPEND_ALIGNED_ELEMENT;
				desc.Format = Direct3D11::D3D11Converter::ToD3D(element.format);
				desc.InputSlotClass = Direct3D11::D3D11Converter::ToD3D(element.attrib);
			}

			hr = device->CreateInputLayout(elements, static_cast<UINT>(vertexElementsCount), compiledProgram->GetPtr(),
				compiledProgram->Size(), &this->myInputLayout);
		};

		if( FAILED( hr ) )
			NDKThrowException( Debug::EExceptionCodes::RendererError,
								STL::String< Char8 >::VA( "[D3D11] Failed to create vertex declaration '%s'", this->myName.AsCharPtr() ),
								"VertexDeclaration::Setup(const STL::String< Char8 >&, const STL::Array< Renderer::VertexElement >&, const Memory::Blob*)" );

		this->myValidation = true;
	}

	void VertexDeclaration::Discard( void )
	{
		__IME_ASSERT( this->IsValid() );

		if( this->myInputLayout ) {
			this->myInputLayout->Release();
			this->myInputLayout = nullptr;
		}

		this->myDevice = nullptr;
		this->myValidation = false;
	}
}