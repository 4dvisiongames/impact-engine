//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: OpenGL vertex declaration definiton.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef __Rendering__Direct3D11__D3D11VertexDeclaration_h__
#define __Rendering__Direct3D11__D3D11VertexDeclaration_h__

/// Reference counter
#include <Foundation/RefCount.h>
/// Vertex element definition
#include <Rendering/VertexElement.h>
/// Shader object
#include <Rendering/Shader.h>

/// Forward declaration
struct ID3D11InputLayout;

/// OpenGL rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>VertexDeclaration</c> is a vertex declaration definiton of Direct3D11
	/// rendering sub-system.
	/// </summary>
	class VertexDeclaration : public Core::ReferenceCount
	{
		// My rendering device
		IntrusivePtr< Renderer::RenderingDevice > myDevice;
		// My vertex declaration name
		STL::String< Char8 > myName;
		// My D3D11 input layout
		ID3D11InputLayout* myInputLayout;
		// My validation parameter
		bool myValidation;

	public:
		/// <summary cref="VertexDeclaration::VertexDeclaration">
		/// Constructor.
		/// </summary>
		VertexDeclaration( const IntrusivePtr< Renderer::RenderingDevice >& device );
		
		/// <summary cref="VertexDeclaration::~VertexDeclaration">
		/// Destructor.
		/// </summary>
		virtual ~VertexDeclaration( void );

		/// <summary cref="VertexDeclaration::Setup">
		/// Create new input layout for Direct3D11 renderer.
		/// </summary>
		/// <param name="name">Name of vertex declaration.</param>
		/// <param name="rhs">Reference to array of vertex elements.</param>
		/// <param name="compiledProgram">Pointer to the loaded binary bytecode.</param>
		void Setup( const STL::String< Char8 >& name, 
					const STL::Array< Renderer::VertexElement >& rhs,
					const Memory::Blob* compiledProgram );

		/// <summary cref="VertexDeclaration::Discard">
		/// Releases all data in this class.
		/// </summary>
		void Discard( void );

		/// <summary cref="VertexDeclaration::IsValid">
		/// Validation of vertex declaration instance. 
		/// </summary>
		bool IsValid( void ) const;
	};

	__IME_INLINED bool VertexDeclaration::IsValid( void ) const
	{
		return this->myValidation;
	}
}

#endif /// __Rendering__Direct3D11__D3D11VertexDeclaration_h__