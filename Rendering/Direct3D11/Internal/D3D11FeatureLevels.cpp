//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 renderer available feature levels manager.
///
/// Author: Pavel Umnikov
//=====================================================================================

#include <D3D11.h>
/// D3D11 feature levels
#include <Rendering/Direct3D11/Internal/D3D11FeatureLevels.h>

namespace Direct3D11
{
	D3D_FEATURE_LEVEL D3D11FeatureLevelConverter::ToD3D11FeatureLevel( Renderer::FeatureLevel level )
	{
		switch( level )
		{
		case Renderer::FeatureLevelEnum::ShaderModel4_0:
			return D3D_FEATURE_LEVEL_10_0;

		case Renderer::FeatureLevelEnum::ShaderModel4_1:
			return D3D_FEATURE_LEVEL_10_1;

		case Renderer::FeatureLevelEnum::ShaderModel5_0:
			return D3D_FEATURE_LEVEL_11_0;

		default:
			return D3D_FEATURE_LEVEL_9_1; //!< By default(error)
		}
	}

	D3D_FEATURE_LEVEL D3D11FeatureLevelConverter::ToD3D11FeatureLevel( int index )
	{
		__IME_ASSERT( (index < 4)&&(index > -1) ); 
		if( index == 0 ) { return D3D_FEATURE_LEVEL_10_0; }
		else if( index == 1 ) { return D3D_FEATURE_LEVEL_10_1; }
		else if( index == 2 ) { return D3D_FEATURE_LEVEL_11_0; }
		else { return D3D_FEATURE_LEVEL_9_1; }
	}

	Renderer::FeatureLevel D3D11FeatureLevelConverter::ToNDKFeatureLevel( D3D_FEATURE_LEVEL level )
	{
		switch( level )
		{
		case D3D_FEATURE_LEVEL_10_0:
			return Renderer::FeatureLevelEnum::ShaderModel4_0;

		case D3D_FEATURE_LEVEL_10_1:
			return Renderer::FeatureLevelEnum::ShaderModel4_1;

		case D3D_FEATURE_LEVEL_11_0:
			return Renderer::FeatureLevelEnum::ShaderModel5_0;

		default:
			return Renderer::FeatureLevelEnum::AllFeatureLevels;
		}
	}

	Renderer::FeatureLevel D3D11FeatureLevelConverter::ToNDKFeatureLevel( int level )
	{
		if( level == 0 ) { return Renderer::FeatureLevelEnum::ShaderModel4_0; }
		else if( level == 1 ) { return Renderer::FeatureLevelEnum::ShaderModel4_1; }
		else if( level == 2 ) { return Renderer::FeatureLevelEnum::ShaderModel5_0; }
		else { return Renderer::FeatureLevelEnum::AllFeatureLevels; };
	}

	STL::String< Char8 > D3D11FeatureLevelConverter::ToString( D3D_FEATURE_LEVEL level )
	{
		switch( level )
		{
		case D3D_FEATURE_LEVEL_10_0:
			return "Direct3D10";

		case D3D_FEATURE_LEVEL_10_1:
			return "Direct3D10.1";

		case D3D_FEATURE_LEVEL_11_0:
			return "Direct3D11";

		default:
			return "Unknown";
		}
	}

	STL::String< Char8 > D3D11FeatureLevelConverter::ToString( int level )
	{
		if( level == 0 ) { return "Direct3D10"; }
		else if( level == 1 ) { return "Direct3D10.1"; }
		else if( level == 2 ) { return "Direct3D11"; }
		else { return "Unknown"; };
	}
}