//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 renderer available feature levels manager.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef __d3d11_h__
#error "This file must be included after d3d11.h header!"
#endif /// __d3d11_h__

#ifndef _D3D11FeatureLevels_h_
#define _D3D11FeatureLevels_h_

/// Feature levels
#include <Rendering/FeatureLevels.h>

/// Rendering sub-system namespace
namespace Direct3D11
{
	/// <summary>
	/// <c>D3D11FeatureLevelConverter</c> is utility class for feature level converting
	/// </summary>
	class D3D11FeatureLevelConverter
	{
	public:
		static D3D_FEATURE_LEVEL ToD3D11FeatureLevel( Renderer::FeatureLevel level );
		static D3D_FEATURE_LEVEL ToD3D11FeatureLevel( int index );
		static Renderer::FeatureLevel ToNDKFeatureLevel( D3D_FEATURE_LEVEL level );
		static Renderer::FeatureLevel ToNDKFeatureLevel( int index );
		static STL::String< Char8 > ToString( D3D_FEATURE_LEVEL level );
		static STL::String< Char8 > ToString( int level );
	};
}

#endif /// _D3D11FeatureLevels_h_