#include <Foundation/Shared.h>
#include <Foundation/FileStream.h>
#include <Foundation/StreamReader.h>
#include <Foundation/StringConverter.h>
#include <Rendering/Direct3D11/Internal/D3D11ShaderInclude.h>

namespace Direct3D11
{
	D3D11ShaderInclude::D3D11ShaderInclude( void )
	{
	}

	D3D11ShaderInclude::~D3D11ShaderInclude( void )
	{
	}

	HRESULT __stdcall D3D11ShaderInclude::Open( D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID* ppData, UINT* pBytes )
	{
		IntrusivePtr< FileSystem::FileStream > fileStream = new FileSystem::FileStream;
		IntrusivePtr< FileSystem::StreamReader > fileReader = new FileSystem::StreamReader;

		FileSystem::Path myPathToInclusion;
		switch( IncludeType ) {
			case D3D_INCLUDE_LOCAL:
			case D3D_INCLUDE_SYSTEM:
				myPathToInclusion = STL::StringConverter::UTF8ToWide( pFileName );
			default:
				__IME_ASSERT( false ); //!< This must never fail
		}

		fileStream->SetPath( myPathToInclusion );
		fileReader->SetInput( fileStream.Downcast< FileSystem::Stream >() );
		if( !fileReader->Open() )
			return S_FALSE;

		void* includeFileData = Memory::Allocator::Malloc( fileStream->GetSize() + 1 );
		__IME_ASSERT( includeFileData );
		fileStream->Read( includeFileData, fileStream->GetSize() );

		*ppData = includeFileData;

		Packed2x32to64Bit packedBytes(fileStream->GetSize());
		*pBytes = packedBytes.m_32bits.upper;

		fileStream->Close();

		fileReader = nullptr;
		fileStream = nullptr;

		return S_OK;
	}

	HRESULT __stdcall D3D11ShaderInclude::Close( LPCVOID pData )
	{
		__IME_ASSERT( pData );
		Memory::Allocator::Free( const_cast< void* >( pData ) );
		return S_OK;
	}
}