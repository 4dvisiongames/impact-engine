//============== (C) Copyright 2014, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 shader inclusion controller.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

/// Direct3D compiler
#include <D3Dcompiler.h>

namespace Direct3D11
{
	class D3D11ShaderInclude : public ID3DInclude
	{
	public:
		D3D11ShaderInclude( void );
		virtual ~D3D11ShaderInclude( void );

		HRESULT __stdcall Open( D3D_INCLUDE_TYPE IncludeType, LPCSTR pFileName, LPCVOID pParentData, LPCVOID* ppData, UINT* pBytes );
		HRESULT __stdcall Close( LPCVOID pData );
	};
}