//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 shader version converter.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Direct3D11 shader version converter
#include <Rendering/Direct3D11/Internal/D3D11ShaderVersionConverter.h>

namespace Direct3D11
{
	const s8* D3D11ShaderVersionConverter::ConvertToVertex( Renderer::FeatureLevel featureLevel )
	{
		switch( featureLevel )
		{
			case Renderer::FeatureLevelEnum::ShaderModel4_1: return "vs_4_1";
			case Renderer::FeatureLevelEnum::ShaderModel5_0: return "vs_5_0";
			default: return "vs_4_0";
		}
	}

	const s8* D3D11ShaderVersionConverter::ConvertToPixel( Renderer::FeatureLevel featureLevel )
	{
		switch( featureLevel )
		{
			case Renderer::FeatureLevelEnum::ShaderModel4_1: return "ps_4_1";
			case Renderer::FeatureLevelEnum::ShaderModel5_0: return "ps_5_0";
			default: return "ps_4_0";
		}
	}

	const s8* D3D11ShaderVersionConverter::ConvertToGeometry( Renderer::FeatureLevel featureLevel )
	{
		switch( featureLevel )
		{
			case Renderer::FeatureLevelEnum::ShaderModel4_1: return "gs_4_1";
			case Renderer::FeatureLevelEnum::ShaderModel5_0: return "gs_5_0";
			default: return "gs_4_0";
		}
	}
}