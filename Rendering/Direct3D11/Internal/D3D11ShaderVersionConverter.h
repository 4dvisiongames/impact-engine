//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 shader version converter.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _D3D11ShaderVersionConverter_h_
#define _D3D11ShaderVersionConverter_h_

/// Direct3D11 Feature levels
#include <Rendering/FeatureLevels.h>

namespace Direct3D11
{
	/// <summary>
	/// <c>D3D11ShaderVersionConverter</c> is a converter for various shader types. Converts feature level to string typeof shader for
	/// shader compiler.
	/// </summary>
	class D3D11ShaderVersionConverter
	{
	public:
		/// <summary cref="D3D11ShaderVersionConverter::ConvertToVertex">
		/// Convert vertex shader version from know feature level.
		/// </summary>
		/// <param name="rfl">Current render feature level.</param>
		/// <returns>Converted string of pixel version.</returns>
		static const s8* ConvertToVertex( Renderer::FeatureLevel featureLevel );

		/// <summary cref="D3D11ShaderVersionConverter::ConvertToPixel">
		/// Convert pixel shader version from know feature level.
		/// </summary>
		/// <param name="rfl">Current render feature level.</param>
		/// <returns>Converted string of vertex version.</returns>
		static const s8* ConvertToPixel( Renderer::FeatureLevel featureLevel );

		/// <summary cref="D3D11ShaderVersionConverter::ConvertToGeometry">
		/// Convert geometry shader version from know feature level.
		/// </summary>
		/// <param name="rfl">Current render feature level.</param>
		/// <returns>Converted string of geometry version.</returns>
		static const s8* ConvertToGeometry( Renderer::FeatureLevel featureLevel );
	};
}

#endif /// _D3D11ShaderVersionConverter_h_