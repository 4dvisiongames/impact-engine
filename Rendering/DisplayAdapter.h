//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Abstracted Display and Adapter event handler.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoDisplayAdapter_h_
#define _NanoDisplayAdapter_h_

#if defined( __IME_PLATFORM_WIN64 )
/// Direct3D11 display adapter
#include <Rendering/Direct3D11/D3D11DisplayAdapter.h>
#else
#error Display Adapter is not implemented on this platfom!
#endif

#endif /// _NanoDisplayAdapter_h_