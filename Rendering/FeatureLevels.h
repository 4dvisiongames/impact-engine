//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Renderer available feature levels manager.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoFeatureLevels_h_
#define _NanoFeatureLevels_h_

/// STL string
#include <Foundation/STLString.h>

/// Renderer sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>FeatureLevelEnum</c> is a feature definitions.
	/// </summary>
	struct FeatureLevelEnum
	{
		enum List
		{
			//! Direct3D10-compatible video-card
			ShaderModel4_0,
			//! Direct3D10.1-compatible video-card
			ShaderModel4_1,
			//! Direct3D11-compatible video-card
			ShaderModel5_0,

			//! All feature levels count(uses by D3D11 renderer)
			AllFeatureLevels,
		};
	};
	typedef FeatureLevelEnum::List FeatureLevel;

	//! Feature level converter object
	class FeatureLevelConverter
	{
	public:
		//! Converts to string only name
		static STL::String< Char8 > ToString( FeatureLevel level );
		//! Converts to feature level type(string must have only feature name)
		static FeatureLevel ToFeatureLevel( const STL::String< Char8 >& str );
	};
}

#endif /// _NanoFeatureLevels_h_