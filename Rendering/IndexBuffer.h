//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Index buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoIndexBuffer_h_
#define _NanoIndexBuffer_h_

#if defined( __IME_PLATFORM_WIN64 )
/// OpenGL index buffer
#include <Rendering/Direct3D11/D3D11IndexBuffer.h>
#else
#error Index buffer is not implemented on this platfom!
#endif 

#endif /// _NanoIndexBuffer_h_