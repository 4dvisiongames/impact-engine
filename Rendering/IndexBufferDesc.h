#pragma once

#ifndef __Rendering__IndexBufferDesc_h__
#define __Rendering__IndexBufferDesc_h__

/// CPU access
#include <Rendering/CPUAccess.h>
/// Usage
#include <Rendering/UsageType.h>
/// Index buffer precision
#include <Rendering/IndexBufferPrecision.h>

namespace Renderer
{
	struct IndexBufferDesc
	{
		SizeT myCPUAccessMask;
		Renderer::UsageFlags myUsageFlags;
		Renderer::IndexBufferPrecision myPrecision;
		SizeT myIndexCount;

		/// These used only on first time data uploading
		void* myData; //!< Pointer to the data

		IndexBufferDesc( void );
		IndexBufferDesc( SizeT accessMask, Renderer::UsageFlags flag, Renderer::IndexBufferPrecision precision, SizeT indices, void* data );
	};

	__IME_INLINED IndexBufferDesc::IndexBufferDesc( void )
		: myCPUAccessMask( 0 )
		, myUsageFlags( Renderer::UsageFlagsEnum::Default )
		, myPrecision( Renderer::IndexBufferPrecision::Idx32 )
		, myIndexCount( 0 )
		, myData( nullptr )
	{
		/// Empty
	}

	__IME_INLINED IndexBufferDesc::IndexBufferDesc( SizeT accessMask, 
													Renderer::UsageFlags flag, 
													Renderer::IndexBufferPrecision precision, 
													SizeT indices, 
													void* data )
		: myCPUAccessMask( accessMask )
		, myUsageFlags( flag )
		, myPrecision( precision )
		, myIndexCount( indices )
		, myData( data )
	{
		/// Empty
	}
}

#endif /// __Rendering__IndexBufferDesc_h__