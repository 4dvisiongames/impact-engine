//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: Index buffer precision definitions
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoIndexBufferPrecision_h_
#define _NanoIndexBufferPrecision_h_

/// Renderer sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>IndexBufferPrecision</c> is a precision type for indices inside index buffer.
	/// </summary>
	struct IndexBufferPrecisionEnum
	{
		enum List
		{
			Idx16 = 2, //!< 16-bit indices 
			Idx32 = 4 //!< 32-bit indices
		};
	};
	typedef IndexBufferPrecisionEnum::List IndexBufferPrecision;
}

#endif /// _NanoIndexBufferPrecision_h_