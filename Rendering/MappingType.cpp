//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Mapping type for buffers and textures.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Mapping type
#include <Rendering/MappingType.h>

/// Rendering sub-system namespace
namespace Renderer
{
	STL::String< Char8 > MappingTypeConverter::ToString( const MappingType type )
	{
		switch( type )
		{
		case MappingTypeEnum::MapRead: return "MappedToRead";
		case MappingTypeEnum::MapWrite: return "MappedToWrite";
		case MappingTypeEnum::MapReadWrite: return "MappedToReadAndWrite";
		case MappingTypeEnum::MapWriteDiscard: return "MappedToDiscardAndWrite";
		case MappingTypeEnum::MapWriteNoOverwrite: return "MappedToWriteNoDiscard";
		default: return "Unknown";
		}
	}
}