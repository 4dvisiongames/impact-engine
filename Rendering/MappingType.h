//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Mapping type for buffers and textures.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoMappingTypes_h_
#define _NanoMappingTypes_h_

/// STL String
#include <Foundation/STLString.h>

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>MappingTypeEnum</c> is a mapping type enumeration
	/// </summary>
	struct MappingTypeEnum
	{
		enum List
		{
			MapRead, //!< Read-only
			MapWrite, //!< Write-only
			MapReadWrite, //!< Access wwith read and write permission
			MapWriteDiscard, //!< Write and discard all previous results
			MapWriteNoOverwrite, //!< Write data without any overwrite
		};
	};
	typedef MappingTypeEnum::List MappingType;

	/// <summary>
	/// <c>MappingTypeConverter</c> is a converter of mappings of buffers and textures types to string.
	/// </summary>
	class MappingTypeConverter
	{
	public:
		/// <summary cref="MappingTypeConverter::ToString">
		/// Convert type to string.
		/// </summary>
		/// <param name="type">Input type of mapping.</param>
		/// <returns>Pre-built string with converted data.</returns>
		static STL::String< Char8 > ToString( const MappingType type );
	};
}

#endif /// _NanoMappingTypes_h_