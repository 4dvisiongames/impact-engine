//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Pipeline definition for engine.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoPipeline_h_
#define _NanoPipeline_h_

#include <Rendering/Direct3D11/D3D11Pipeline.h>

#endif /// _NanoPipeline_h_