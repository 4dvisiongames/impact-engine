//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Primitive topology enumerations and converters.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Primitive topology
#include <Rendering/PrimitiveTopology.h>

/// Rendering sub-system
namespace Renderer
{
	STL::String< Char8 > PrimitiveTopologyConverter::ToString( PrimitiveTopology::List param )
	{
		switch( param )
		{
		case PrimitiveTopology::TriangleList: return "TriangleList";
		case PrimitiveTopology::TriangleStrip: return "TriangleStrip";
		default: 
			{
				NDKThrowException( Debug::EExceptionCodes::InvalidParams,
									"Invalid argument for parameter 'param'",
									"PrimitiveTopologyConverter::ToString( PrimitiveTopology::List )" );
				return "Unknown";
			}
		};
	}

	PrimitiveTopology::List PrimitiveTopologyConverter::ToPrimitiveTopology( const STL::String< Char8 >& str )
	{
		if( str == "TriangleList" ) return PrimitiveTopology::TriangleList;
		else if( str == "TriangleStrip" ) return PrimitiveTopology::TriangleStrip;
		else 
		{
			NDKThrowException( Debug::EExceptionCodes::InvalidParams,
								"Invalid argument for parameter 'str'",
								"PrimitiveTopologyConverter::ToPrimitiveTopology( const STL::String< Char8 >& )" );
			return PrimitiveTopology::Unknown;
		}
	}
}