//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Primitive topology enumerations and converters.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef _NanoPrimitiveTopology_h_
#define _NanoPrimitiveTopology_h_

/// STL string
#include <Foundation/STLString.h>

namespace Renderer
{
	/// Primitive topology list
	struct PrimitiveTopology
	{
		enum List
		{
			TriangleList, //!< Triangles list
			TriangleStrip,  //!< Trianles strip
			Unknown, //!< Error
		};
	};

	/// <summary>
	/// <c>PrimitiveTopologyConverter</c> is an abstractive primitive topology definition.
	/// </summary>
	class PrimitiveTopologyConverter
	{
	public:
		/// Convert primitive topology data to string
		static STL::String< Char8 > ToString( PrimitiveTopology::List param );
		/// Convert string data to primitive topology
		static PrimitiveTopology::List ToPrimitiveTopology( const STL::String< Char8 >& str );
	};
}

#endif /// _NanoPrimitiveTopology_h_