//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Render state front-end
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoRenderState_h_
#define _NanoRenderState_h_

/// Memory stream object
#include <Foundation/MemoryStream.h>
/// Render state types
#include <Rendering/RenderStateType.h>
/// Render state descriptions
#include <Rendering/RenderStateDesc.h>

//! Renderer namespace
namespace Renderer
{
	//! RenderState is a generalized Render State Object definitions as precompiled commands list.
	class RenderState : public Core::ReferenceCount
	{
		//! Rendering state object type
		RenderStateType myRenderStateType;
		//! Rendering state object command list pointer
		IntrusivePtr< FileSystem::MemoryStream > myRenderStateObject;
		//! Validation parameter
		bool myValidation;

	public:
		RenderState( void );
		virtual ~RenderState( void );

		//! Setup render state as depth-stencil state.
		void Setup( DepthStencilStateDesc& dsDesc );
		//! Setup render state as rasterizer state.
		void Setup( RasterizerStateDesc& dsDesc );
		//! Setup render state as rasterizer state.
		void Setup( BlendStateDesc& dsDesc );
		//! Setup render state as sampler state.
		void Setup( SamplerStateDesc& dsDesc );
		//! Discard all render state writings from this object.
		void Discard( void );
		//! Get render state type
		RenderStateType GetRenderState( void );
		//! Check for render state validness.
		bool IsValid( void ) const;
	};

	__IME_INLINED RenderStateType RenderState::GetRenderState( void )
	{
		return this->myRenderStateType;
	}

	__IME_INLINED bool RenderState::IsValid( void ) const
	{
		return this->myValidation;
	}
}

#endif /// _NanoRenderState_h_