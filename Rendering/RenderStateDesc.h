//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Depth stencil description.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef _NanoRenderStateDesc_h_
#define _NanoRenderStateDesc_h_

/// Render system namespace
namespace Renderer
{
	/// <summary>
	/// <c>DepthWriteMaskTypeList</c> is a Depth-Write Mask list of enumerations for Depth-Stencil State Objects.
	/// </summary>
	struct DepthWriteMaskTypeList
	{
		enum List
		{
			DisableZWrite = 0, //!< Turn off writes to the depth buffer.
			EnableZWrite = 1 //!< Turn on writes to the depth buffer.
		};
	};
	typedef DepthWriteMaskTypeList::List DepthWriteMask;

	/// <summary>
	/// <c>FillModeTypeList</c> is a pixel filling mode for Rasterization State Objects.
	/// </summary>
	struct FillModeTypeList
	{
		enum List
		{
			Wireframe = 2, //!< Draw lines connecting the vertices. Adjacent vertices are not drawn.
			Solid = 3 //!< Fill the triangles formed by the vertices. Adjacent vertices are not drawn.
		};
	};
	typedef FillModeTypeList::List FillMode;

	/// <summary>
	/// <c>CullModeTypeList</c> is a triangle culling mode for Rasterization State Objects.
	/// </summary>
	struct CullModeTypeList
	{
		enum List
		{
			None = 0, //!< Always draw all triangles.
			Front = 1, //!< Do not draw triangles that are front-facing.
			Back = 2 //!< Do not draw triangles that are back-facing.
		};
	};
	typedef CullModeTypeList::List CullMode;

	/// <summary>
	/// <c>StencilOperationTypeList</c> is a per-fragment Stencil Blend Operation list of enumeration for
	/// Depth-Stencil State Objects.
	/// </summary>
	struct StencilOperationTypeList
	{
		enum List
		{
			Keep = 1, //!< Keep the existing stencil data.
			Zero = 2, //!< Set the stencil data to 0.
			Replace = 3, //!< Set the stencil data to the reference value by renderer blend state
			IncreaseSaturate = 4, //!< Increment the stencil value by 1, and clamp the result.
			DecreaseSaturate = 5, //!< Decrement the stencil value by 1, and clamp the result.
			Invert = 6, //!< Invert the stencil data.
			Increase = 7, //!< Increment the stencil value by 1, and wrap the result if necessary.
			Decrease = 8 //!< Decrement the stencil value by 1, and wrap the result if necessary.
		};
	};
	typedef StencilOperationTypeList::List StencilOperation;

	/// <summary>
	/// <c>BlendTypeList</c> is a per-pixel blending list of enumerations for Blending State Objects.
	/// </summary>
	struct BlendTypeList
	{
		enum List
		{
			Zero = 1, //!< The data source is the color black (0, 0, 0, 0). No pre-blend operation.
			One = 2, //!< The data source is the color white (1, 1, 1, 1). No pre-blend operation.
			SourceColour = 3, //!< The data source is color data (RGB) from a pixel shader. No pre-blend operation.
			InvSourceColour = 4, //!< The data source is color data (RGB) from a pixel shader. The pre-blend operation inverts the data, generating 1 - RGB.
			SourceAlpha = 5, //!< The data source is alpha data (A) from a pixel shader. No pre-blend operation.
			InvSourceAlpha = 6, //!< The data source is alpha data (A) from a pixel shader. The pre-blend operation inverts the data, generating 1 - A.
			DestinationAlpha = 7, //!< The data source is alpha data from a rendertarget. No pre-blend operation.
			InvDestinationAlpha = 8, //!< The data source is alpha data from a rendertarget. The pre-blend operation inverts the data, generating 1 - A.
			DestinationColour = 9, //!< The data source is color data from a rendertarget. No pre-blend operation.
			InvDestinationColour = 10, //!< The data source is color data from a rendertarget. The pre-blend operation inverts the data, generating 1 - RGB.
			SourceAlphaSaturate = 11, //!< The data source is alpha data from a pixel shader. The pre-blend operation clamps the data to 1 or less.
			BlendFactor = 14, //!< The data source is the blend factor set with SetBlendState by renderer: OpenGL or DirectX10, etc. No pre-blend operation.
			InvBlendFactor = 15, //!< The data source is the blend factor set with SetBlendState by renderer: OpenGL or DirectX10, etc. The pre-blend operation inverts the blend factor, generating 1 - blend_factor.
			Source1Colour = 16, //!< The data sources are both color data output by a pixel shader. There is no pre-blend operation. This options supports dual-source color blending.
			InvSource1Colour = 17, //!< The data sources are both color data output by a pixel shader. The pre-blend operation inverts the data, generating 1 - RGB. This options supports dual-source color blending.
			Source1Alpha = 18, //!< The data sources are alpha data output by a pixel shader. There is no pre-blend operation. This options supports dual-source color blending.
			InvSource1Alpha = 19 //!< The data sources are alpha data output by a pixel shader. The pre-blend operation inverts the data, generating 1 - A. This options supports dual-source color blending.
		};
	};
	typedef BlendTypeList::List Blend;

	/// Colour defitions
	struct ColourMaskTypeList
	{
		enum List
		{
			Red = 1, //!< Red colour mask
			Green = 2, //!< Green colour mask
			Blue = 4, //!< Blue colour mask
			Alpha = 8, //!< Alpha colour mask
			All = ( ( ( Red | Green ) | Blue ) | Alpha ) //!< Compact all colours
		};
	};
	typedef ColourMaskTypeList::List ColourMask;

	/// Per-Pixel/Per-Fragment Blending Operation
	struct BlendOperationTypeList
	{
		enum List
		{
			Additive = 1, //!< Add source 1 and source 2.
			Subtractive = 2, //!< Subtract source 1 from source 2.
			ReverseSubtractive = 3, //!< Subtract source 2 from source 1.
			Minimum = 4, //!< Find the minimum of source 1 and source 2.
			Maximum = 5 //!< Find the maximum of source 1 and source 2.
		};
	};
	typedef BlendOperationTypeList::List BlendOperation;

	/// Comparison
	struct ComparisonTypeList
	{
		enum List
		{
			Never = 1, //!< Never pass the comparison.
			Less = 2, //!< If the source data is less than the destination data, the comparison passes.
			Equal = 3, //!< If the source data is equal to the destination data, the comparison passes.
			LessEqual = 4, //!< If the source data is less than or equal to the destination data, the comparison passes.
			Greater = 5, //!< If the source data is greater than the destination data, the comparison passes.
			NotEqual = 6, //!< If the source data is not equal to the destination data, the comparison passes.
			GreaterEqual = 7, //!< If the source data is greater than or equal to the destination data, the comparison passes.
			Always = 8 //!< Always pass the comparison
		};
	};
	typedef ComparisonTypeList::List Comparison;

	struct FilterTypeList
	{
		enum List 
		{ 
			MinMagMipPoint = 0, //!< Use point sampling for minification, magnification, and mip-level sampling.
			MinMagPoint_MipLinear = 1, //!< Use point sampling for minification and magnification; use linear interpolation for mip-level sampling.
			MinPoint_MagLinear_MipPoint = 2, //!< Use point sampling for minification; use linear interpolation for magnification; use point sampling for mip-level sampling.
			MinPoint_MagMipLinear = 3, //!< Use point sampling for minification; use linear interpolation for magnification and mip-level sampling.
			MinLinear_MagMipPoint = 4, //!< Use linear interpolation for minification; use point sampling for magnification and mip-level sampling.
			MinLinear_MagPoint_MipLinear = 5, //!< Use linear interpolation for minification; use point sampling for magnification; use linear interpolation for mip-level sampling.
			MinMagLinear_MipPoint = 6, //!< Use linear interpolation for minification and magnification; use point sampling for mip-level sampling.
			MinMagMipLinear = 7, //!< Use linear interpolation for minification, magnification, and mip-level sampling.
			Anisotropic = 8, //!< Use anisotropic interpolation for minification, magnification, and mip-level sampling.
			CompareMinMagMipPoint = 9, //!< Use point sampling for minification, magnification, and mip-level sampling. Compare the result to the comparison value.
			CompareMinMagPoint_MipLinear = 10, //!< Use point sampling for minification and magnification; use linear interpolation for mip-level sampling. Compare the result to the comparison value.
			CompareMinPoint_MagLinear_MipPoint = 11, //!< Use point sampling for minification; use linear interpolation for magnification; use point sampling for mip-level sampling. Compare the result to the comparison value.
			CompareMinPoint_MagMipLinear = 12, //!< Use point sampling for minification; use linear interpolation for magnification and mip-level sampling. Compare the result to the comparison value.
			CompareMinLinear_MagMipPoint = 13, //!< Use linear interpolation for minification; use point sampling for magnification and mip-level sampling. Compare the result to the comparison value.
			CompareMinLinear_MagPoint_MipLinear = 14, //!< Use linear interpolation for minification; use point sampling for magnification; use linear interpolation for mip-level sampling. Compare the result to the comparison value.
			CompareMinMagLinear_MipPoint = 15, //!< Use linear interpolation for minification and magnification; use point sampling for mip-level sampling. Compare the result to the comparison value.
			CompareMinMagMipLinear = 16, //!< Use linear interpolation for minification, magnification, and mip-level sampling. Compare the result to the comparison value.
			CompareAnisotropic = 17, //!< Use anisotropic interpolation for minification, magnification, and mip-level sampling. Compare the result to the comparison value.
		};
	};
	typedef FilterTypeList::List Filter;

	struct TextureAddressModeTypeList
	{
		enum List
		{
			Wrap = 1, //!< Tile the texture at every (u,v) integer junction.
			Mirror = 2, //!< Flip the texture at every (u,v) integer junction
			Clamp = 3, //!< Texture coordinates outside the range [0.0, 1.0] are set to the texture color at 0.0 or 1.0, respectively.
			Border = 4, //!< Texture coordinates outside the range [0.0, 1.0] are set to the border color specified in Sampler State Object.
			MirrorOnce = 5 //!< Takes the absolute value of the texture coordinate (thus, mirroring around 0), and then clamps to the maximum value.
		};
	};
	typedef TextureAddressModeTypeList::List TextureAddressMode;

	/// Depth-Stencil Operation
	struct DepthStencilOperationDesc
	{
		StencilOperation stencilFailOp; //!< The stencil operation to perform when stencil testing fails.
		StencilOperation stencilDepthFailOp; //!< The stencil operation to perform when stencil testing passes and depth testing fails.
		StencilOperation stencilPassOp; //!< The stencil operation to perform when stencil testing and depth testing both pass.
		Comparison stencilFunction; //!< A function that compares stencil data against existing stencil data. 

		DepthStencilOperationDesc() : stencilFailOp( StencilOperationTypeList::Keep ), stencilDepthFailOp( StencilOperationTypeList::Keep ), stencilPassOp( StencilOperationTypeList::Keep ),
									  stencilFunction( ComparisonTypeList::Always )
		{
			/// Empty
		}

		DepthStencilOperationDesc( StencilOperation stencilFailOperation, StencilOperation stencilDepthFailOperation, StencilOperation stencilPassOperation, Comparison stencilFunc ) : 
			stencilFailOp( stencilFailOperation ), stencilDepthFailOp( stencilDepthFailOperation ), stencilPassOp( stencilPassOperation ), stencilFunction( stencilFunc )
		{
			/// Empty
		}
	};

	/// Per-Render Target Blending
	struct RenderTargetBlendDesc
	{
		bool bEnableBlend; //!< Enable (or disable) blending.
		Blend srcBlend; //!< This blend option specifies the operation to perform on the RGB value that the pixel shader outputs. 
		Blend destBlend; //!< This blend option specifies the operation to perform on the current RGB value in the render target. 
		BlendOperation blendOp; //!< This blend operation defines how to combine the <c>srcBlend</c> and <c>destBlend</c> operations.
		Blend srcBlendAlpha; //!< This blend option specifies the operation to perform on the alpha value that the pixel shader outputs.
		Blend destBlendAlpha; //!< This blend option specifies the operation to perform on the current alpha value in the render target. 
		BlendOperation blendOpAlpha; //!< This blend operation defines how to combine the <c>srcBlendAlpha</c> and <c>destBlendAlpha</c> operations.
		ColourMask uiMask; //!< A write colour mask.

		RenderTargetBlendDesc( void ) : bEnableBlend( false ), srcBlend( BlendTypeList::One ), destBlend( BlendTypeList::Zero ), blendOp( BlendOperationTypeList::Additive ), srcBlendAlpha( BlendTypeList::One ), 
				destBlendAlpha( BlendTypeList::Zero ), blendOpAlpha( BlendOperationTypeList::Additive ), uiMask( ColourMaskTypeList::All )
		{
			/// Empty
		}

		RenderTargetBlendDesc( bool enableBlend, Blend sourceBlend, Blend destinationBlend, BlendOperation blendOperation, Blend sourceBlendAlpha, Blend destinationBlendAlpha, BlendOperation blendOperationAlpha,
				ColourMask mask ) : bEnableBlend( enableBlend ), srcBlend( sourceBlend ), destBlend( destinationBlend ), blendOp( blendOperation ), srcBlendAlpha( sourceBlendAlpha ), destBlendAlpha( destinationBlendAlpha ),
				blendOpAlpha( blendOperationAlpha ), uiMask( mask )
		{
			/// Empty
		}
	};

	//////////////////////////////
	/// STATES
	//////////////////////////////

	/// Blend state object
	struct BlendStateDesc
	{
		bool bEnableAlphaToCoverage; //!< Specifies whether to use alpha-to-coverage as a multisampling technique when setting a pixel to a render target.
		bool bIndependentBlendEnable; //!< Specifies whether to enable independent blending in simultaneous render targets.
		RenderTargetBlendDesc RenderTarget[8]; //!< An array of <c>RenderTargetBlendDesc</c> structures that describe the blend states for render targets;

		BlendStateDesc( void ) : bEnableAlphaToCoverage( false ), bIndependentBlendEnable( false )
		{
			/// Empty
		}

		BlendStateDesc( bool alphaToCoverage, bool independentBlendEnable, RenderTargetBlendDesc rtBlend[8] ) : bEnableAlphaToCoverage( false ), bIndependentBlendEnable( false )
		{
			/// Setup all render target blending states
			for( unsigned int idx = 0; idx < 8; ++idx )
			{
				RenderTarget[idx] = rtBlend[idx];
			}
		}
	};

	/// Depth-Stencil state object
	struct DepthStencilStateDesc
	{
		bool bEnableDepth; //!< Enable depth testing.
		bool bEnableStencil; //!< Enable stencil testing.
		DepthWriteMask depthWriteMask; //!< Identify a portion of the depth-stencil buffer that can be modified by depth data.
		u8 stencilWriteMask; //!< Identify a portion of the depth-stencil buffer for writing stencil data.
		u8 stencilReadMask; //!< Identify a portion of the depth-stencil buffer for reading stencil data.
		Comparison depthCompare; //!< A function that compares depth data against existing depth data.
		DepthStencilOperationDesc frontFace; //!< Identify how to use the results of the depth test and the stencil test for pixels whose surface normal is facing towards the camera.
		DepthStencilOperationDesc backFace; //!< Identify how to use the results of the depth test and the stencil test for pixels whose surface normal is facing away from the camera.

		DepthStencilStateDesc( void ) : bEnableDepth( true ), bEnableStencil( false ), depthWriteMask( DepthWriteMaskTypeList::EnableZWrite ), stencilWriteMask( 0xFF ), stencilReadMask( 0xFF ), 
			depthCompare( ComparisonTypeList::Always )
		{
			/// Empty
		} 

		DepthStencilStateDesc( bool enableDepth, bool enableStencil, DepthWriteMask depthWriteMaskType, u8 stencilWriteMaskDef, u8 stencilReadMaskDef, Comparison depthComparison,
			DepthStencilOperationDesc frontFaceOp, DepthStencilOperationDesc backFaceOp ) : bEnableDepth( enableDepth ), bEnableStencil( enableStencil ), depthWriteMask( depthWriteMaskType ), 
			stencilWriteMask( stencilWriteMaskDef ), stencilReadMask( stencilReadMaskDef ), depthCompare( depthComparison ), frontFace( frontFaceOp ), backFace( backFaceOp )
		{
			/// Empty
		} 
	};

	/// Rasterizer state object 
	struct RasterizerStateDesc
	{
		FillMode fillMode; //!< Determines the fill mode to use when rendering 
		CullMode cullMode; //!< Indicates triangles facing the specified direction are not drawn
		bool bFrontFacing; //!< Determines if a triangle is front- or back-facing. TRUE when trianlge front-facing and FALSE - back-facing
		int iDepthBias; //!< Depth value added to a given pixel. 
		float fDepthBiasClamp; //!< Maximum depth bias of a pixel. 
		float fSlopeScaledDepthBias; //!< Scalar on a given pixel's slope.
		bool bDepthClipEnable; //!< Enable clipping based on distance.
		bool bScissorEnable; //!< Enable scissor-rectangle culling. All pixels outside an active scissor rectangle are culled.
		bool bMSAAEnable; //!< Specifies whether to use the quadrilateral or alpha line anti-aliasing algorithm on multisample antialiasing (MSAA) render targets.
		bool bAALineEnabled; //!< Specifies whether to enable line antialiasing

		RasterizerStateDesc( void ) : fillMode( FillModeTypeList::Solid ), cullMode( CullModeTypeList::Back ), bFrontFacing( false ), iDepthBias( 0 ), fDepthBiasClamp( 0.0f ), fSlopeScaledDepthBias( 0.0f ),
			bDepthClipEnable( true ), bScissorEnable( false ), bMSAAEnable( false ), bAALineEnabled( false )
		{
			/// Empty
		}

		RasterizerStateDesc( FillMode fill, CullMode cull, bool frontFacing, int depthBias, float depthBiasClamp, float slopeScaledDepthBias, bool depthClipEnable, bool scissorEnable, bool msaa, bool msaaLine ) : 
			fillMode( fill ), cullMode( cull ), bFrontFacing( frontFacing ), iDepthBias( depthBias ), fDepthBiasClamp( depthBiasClamp ), fSlopeScaledDepthBias( slopeScaledDepthBias ),
			bDepthClipEnable( depthClipEnable ), bScissorEnable( scissorEnable ), bMSAAEnable( msaa ), bAALineEnabled( msaaLine )
		{
			/// Empty
		}
	};

	/// Sampler state object
	struct SamplerStateDesc
	{
		Filter mSampleFilter; //!< Filtering method to use when sampling a texture
		TextureAddressMode mAddressU; //!< Method to use for resolving a U texture coordinate that is outside the 0 to 1 range
		TextureAddressMode mAddressV; //!< Method to use for resolving a V texture coordinate that is outside the 0 to 1 range
		TextureAddressMode mAddressW; //!< Method to use for resolving a W texture coordinate that is outside the 0 to 1 range
		float fMipLODBias; //!< Offset from the calculated mipmap level.
		unsigned int iMaxAnisotropy; //!< Clamping value used if anisotropic filtration has been enabled
		Comparison mComparisonFunc; //!< A function that compares sampled data against existing sampled data. 
		float fBorderColor[4]; //!< Border color to use if border sampling used
		float fMinLOD; //!< Lower end of the mipmap range to clamp access to, where 0 is the largest and most detailed mipmap level and any level higher than that is less detailed.
		float fMaxLOD; //!< Upper end of the mipmap range to clamp access to, where 0 is the largest and most detailed mipmap level and any level higher than that is less detailed. 

		SamplerStateDesc( void ) : mSampleFilter( FilterTypeList::MinMagMipLinear ), mAddressU( TextureAddressModeTypeList::Clamp ), mAddressV( TextureAddressModeTypeList::Clamp ), mAddressW( TextureAddressModeTypeList::Clamp ),
			fMipLODBias( 0.0f ), iMaxAnisotropy( 16 ), mComparisonFunc( ComparisonTypeList::Never ), fMinLOD( -3.402823466e+38F ), fMaxLOD( 3.402823466e+38F )
		{
			for( unsigned int idx = 0; idx < 4; ++idx )
			{
				this->fBorderColor[idx] = 0.0f;
			}
		}

		SamplerStateDesc( Filter sampleFilter, TextureAddressMode addressU, TextureAddressMode addressV, TextureAddressMode addressW, float mipLODBias, unsigned int maxAnisotropy, Comparison comparisonFunc,
			float borderColor[4], float minLOD, float maxLOD ) : mSampleFilter( sampleFilter ), mAddressU( addressU ), mAddressV( addressV ), mAddressW( addressW ), fMipLODBias( mipLODBias ), iMaxAnisotropy( maxAnisotropy ), 
			mComparisonFunc( comparisonFunc ), fMinLOD( minLOD ), fMaxLOD( maxLOD )
		{
			/// Setup border colours
			for( unsigned int idx = 0; idx < 4; ++idx )
			{
				this->fBorderColor[idx] = borderColor[idx];
			}
		}
	};
}

#endif /// _NanoRenderStateDesc_h_