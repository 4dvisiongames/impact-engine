//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Render state types.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Render state type
#include <Rendering/RenderStateType.h>

/// Render system namespace
namespace Renderer
{
	RenderStateType RenderStateTypeConverter::FromString( STL::String< Char8 > rs )
	{
		if( rs == "DepthStencil" ) { return RenderStateTypeList::DepthStencil; }
		else if( rs == "Rasterizer" ) { return RenderStateTypeList::Rasterizer; }
		else if( rs == "Blend" ) { return RenderStateTypeList::Blend; }
		else if( rs == "Sampler" ) { return RenderStateTypeList::Sampler; }
		else { return RenderStateTypeList::Unknown; }
	}

	STL::String< Char8 > RenderStateTypeConverter::ToString( RenderStateType rs )
	{
		switch( rs )
		{
		case RenderStateTypeList::DepthStencil: return "DepthStencil"; 
		case RenderStateTypeList::Rasterizer: return "Rasterizer";
		case RenderStateTypeList::Blend: return "Blend";
		case RenderStateTypeList::Sampler: return "Sampler";
		case RenderStateTypeList::Unknown:
		default: return "Unknown";
		}
	}
}