//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Render state types.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef _NanoRenderStateType_h_
#define _NanoRenderStateType_h_

/// STL string
#include <Foundation/STLString.h>

/// Render system namespace
namespace Renderer
{
	/// <summary>
	/// <c>RenderStateTypeList</c> is render state type defintion.
	/// </summary>
	struct RenderStateTypeList
	{
		enum List
		{
			Unknown, //!< Unknown render state
			DepthStencil, //!< Depth-stencil type
			Rasterizer, //!< Rasterizer state
			Blend, //!< Blend state
			Sampler //!< Sampler state
		};
	};
	typedef RenderStateTypeList::List RenderStateType;

	/// <summary>
	/// <c>RenderStateTypeConverter</c>
	/// </summary>
	class RenderStateTypeConverter
	{
	public:
		/// <summary cref="RenderStateTypeConverter::FromString">
		/// Convert render state type from string.
		/// </summary> 
		static RenderStateType FromString( STL::String< Char8 > rs );

		/// <summary cref="RenderStateTypeConverter::ToString">
		/// Convert render state type to string.
		/// </summary> 
		static STL::String< Char8 > ToString( RenderStateType rs );
	};
}

#endif /// _NanoRenderStateType_h_