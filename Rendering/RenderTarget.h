//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Render target.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoRenderTarget_h_
#define _NanoRenderTarget_h_

#if defined( __IME_PLATFORM_WIN64 )
/// OpenGL render target
#include <Rendering/Direct3D11/D3D11RenderTarget.h>
#else
#error Render Target is not implemented on this platfom!
#endif

#endif /// _NanoRenderTarget_h_