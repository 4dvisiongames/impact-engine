//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Front-End rendering context.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _NanoRenderingContext_h_
#define _NanoRenderingContext_h_

#if defined( __IME_PLATFORM_WIN64 ) || defined( NDKPlatformWin64 )
/// Direct3D11 rendering context
#include <Rendering/Direct3D11/D3D11RenderingContext.h>
#else
#error Rendering context is not implemented on this platfom!
#endif 

#endif /// _NanoRenderingContext_h_