//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Summarized rendering systems
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _NanoRenderSystem_h_
#define _NanoRenderSystem_h_

#if defined( __IME_PLATFORM_WIN64 )
/// Direct3D11 renderer
#include <Rendering/Direct3D11/D3D11RenderingDevice.h>
#else
/// Error in other case
#error Render System is not implemented on this platfom!
#endif 

#endif /// _NanoRenderSystem_h_