//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Rendering output information.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoRenderingOutput_h_
#define _NanoRenderingOutput_h_

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>RenderingOutput</c> is a rendering output information
	/// </summary>
	struct RenderingOutput
	{
		unsigned int uWidth, uHeight; //!< Width and height of output
#if defined( WIN32 )
		bool bFullScreen; //!< Fullscreen handler
		HWND hWnd; //!< Windows handler
#endif
	
		/// <summary cref="RenderingOutput::RenderingOutput">
		/// Constructor.
		/// </summary>
		RenderingOutput( void );

		/// <summary cref="RenderingOutput::~RenderingOutput">
		/// Destructor.
		/// </summary>
		~RenderingOutput( void );

		/// <summary cref="RenderingOutput::operator=">
		/// Assignment operator.
		/// </summary>
		/// <param name="rhs">Reference to the object.</param>
		void operator = ( const RenderingOutput& rhs );
	};

	__IME_INLINED RenderingOutput::RenderingOutput( void ) : uWidth( 0 ), uHeight( 0 )
#if defined( WIN32 )
		,bFullScreen( false ), hWnd( NULL )
#endif
	{
		/// Empty
	}

	__IME_INLINED RenderingOutput::~RenderingOutput( void )
	{
		/// Empty
	}

	__IME_INLINED void RenderingOutput::operator = ( const RenderingOutput& rhs )
	{
		/// assign all definitions
#if defined( WIN32 )
		this->bFullScreen = rhs.bFullScreen;
		this->hWnd = rhs.hWnd;
#endif
		/// Rendering output dimentions
		this->uHeight = rhs.uHeight;
		this->uWidth = rhs.uWidth;
	}
}

#endif /// _NanoRenderingOutput_h_