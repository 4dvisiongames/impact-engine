//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Shader definition front-end.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _NanoShader_h_
#define _NanoShader_h_

#if defined( __IME_PLATFORM_WIN64 )
/// Direct3D11 shader
#include <Rendering/Direct3D11/D3D11Shader.h>
#else
#error Pipeline is not implemented on this platfom!
#endif

#endif /// _NanoShader_h_