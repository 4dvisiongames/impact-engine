//============== (C) Copyright 2014, PARANOIA Team. All rights reserved. ==============
/// Desc: Shader compiler selector.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#if defined( __IME_PLATFORM_WIN64 )
/// Direct3D11 shader compiler implementation
#include <Rendering/Direct3D11/D3D11ShaderCompiler.h>
#else
#error Pipeline is not implemented on this platfom!
#endif