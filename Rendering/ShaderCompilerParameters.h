//============== (C) Copyright 2014, PARANOIA Team. All rights reserved. ==============
/// Desc: Shader compiler parameters.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

namespace Renderer
{
	struct CompilerParameters
	{
		bool threatWarningsAsErrors;
		bool partialPrecision;
		u16 warningsLevel;
	};
}