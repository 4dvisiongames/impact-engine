//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: Shader model converting system. Constant for consoles and MacOSX. But 
///       variable for Windows and Linux.
///
/// Author: Pavel Umnikov
//=====================================================================================

/// Shader model converter
#include <Rendering/ShaderModelConverter.h>

/// Rendering sub-system
namespace Renderer
{
	STL::String< Char8 > ShaderModelConverter::ToString( ShaderModelBit::List param )
	{
		switch( param )
		{
		case ShaderModelBit::SHADERMODEL_3_0: return "ShaderModel_3_0";
		case ShaderModelBit::SHADERMODEL_4_0: return "ShaderModel_4_0";
		case ShaderModelBit::SHADERMODEL_4_1: return "ShaderModel_4_1";
		case ShaderModelBit::SHADERMODEL_5_0: return "ShaderModel_5_0";
		default: 
			NDKThrowException( Debug::EExceptionCodes::InvalidParams, 
								"Invalid parameter for ShaderModelConverter to STL::String< Char8 >", 
								"ShaderModelConverter::ToString( ShaderModelConverter::ShaderModelBit::List )" );
			return "";
		}
	}

	ShaderModelBit::List ShaderModelConverter::ToShaderModel( const STL::String< Char8 >& str )
	{
		if( str == "ShaderModel_3_0" ) return ShaderModelBit::SHADERMODEL_3_0;
		else if( str == "ShaderModel_4_0" ) return ShaderModelBit::SHADERMODEL_4_0;
		else if( str == "ShaderModel_4_1" ) return ShaderModelBit::SHADERMODEL_4_1;
		else if( str == "ShaderModel_5_0" ) return ShaderModelBit::SHADERMODEL_5_0;
		else
		{	 
			NDKThrowException( Debug::EExceptionCodes::InvalidParams, 
							   "Invalid parameter for STL::String< Char8 > to ShaderModelBit", 
							   "ShaderModelConverter::ToShaderModel( const STL::String< Char8 >& )" );
			/// Dummy
			return ShaderModelBit::UNKNOWN;
		};
	}

} /// namespace Renderer