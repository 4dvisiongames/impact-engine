//============= (C) Copyright 2011, NanoEngine Team. All rights reserved. =============
/// Desc: Shader model converting system. Constant for consoles and MacOSX. But 
///       variable for Windows.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _NanoShaderModelConverter_h_
#define _NanoShaderModelConverter_h_

/// STL string
#include <Foundation/STLString.h>

/// Rendering sub-system
namespace Renderer
{
	/// Shader model version
	struct ShaderModelBit
	{
		enum List
		{
			SHADERMODEL_3_0, //!< Shader model 3.0 for Direct3D9-capable videocards and for Xbox360 and PS3.
			SHADERMODEL_4_0, //!< Shader model 4.0 for Direct3D10-capable videocards and MacOSX
			SHADERMODEL_4_1, //!< Shader model 4.1 for Direct3D10.1-capable videocards and Nintendo Wii U
			SHADERMODEL_5_0, //!< Shader model 5.0 for Direct3D11-capable videocards
			UNKNOWN
		};
	};

	class ShaderModelConverter
	{
	public:
		/// Convert shader model to string
		static STL::String< Char8 > ToString( ShaderModelBit::List param );
		/// Convert string to shader model
		static ShaderModelBit::List ToShaderModel( const STL::String< Char8 >& str );
	};
}

#endif /// _NanoShaderModelConverter_h_