//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Shader preprocessor definitions like "#define"
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _NanoShaderPreprocessor_h_
#define _NanoShaderPreprocessor_h_

/// STL String
#include <Foundation/STLString.h>

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// Shader preprocessor type
	/// </summary>
	class ShaderPreprocessor
	{
	public:
		ShaderPreprocessor( void );
		ShaderPreprocessor( const Char8* name, const Char8* def );

		const Char8* GetName( void ) const;
		const Char8* GetDefinition( void ) const;

	private:
		STL::String< Char8 > myName;
		STL::String< Char8 > myDefinition;
	};

	__IME_INLINED ShaderPreprocessor::ShaderPreprocessor( void ) : myName(), myDefinition()
	{
		/// Empty
	}

	__IME_INLINED ShaderPreprocessor::ShaderPreprocessor( const Char8* name, const Char8* def ) : myName( name ), myDefinition( def )
	{
		/// Empty
	}

	__IME_INLINED const Char8* ShaderPreprocessor::GetName( void ) const
	{
		return this->myName.AsCharPtr();
	}

	__IME_INLINED const Char8* ShaderPreprocessor::GetDefinition( void ) const
	{
		return this->myDefinition.AsCharPtr();
	}
}

#endif /// _NanoShaderPreprocessor_h_