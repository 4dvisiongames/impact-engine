//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Shader resource slices definiton.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoShaderResourceSlice_h_
#define _NanoShaderResourceSlice_h_

/// Direct3D11 rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>ShaderResourceSlice</c> defines how this SRV must be represented:
	/// Arrayed, or Single ones.
	/// </summary>
	struct ShaderResourceSlice
	{
		enum List
		{
			Single, //!< This is just single
			Array, //!< Array of typed objects
		};
	};
}

#endif /// _D3D11ShaderResourceSlice_h_