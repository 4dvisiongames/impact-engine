//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Direct3D11 shader resource types definiton.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoShaderResourceType_h_
#define _NanoShaderResourceType_h_

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>ShaderResourceType</c> is a shader resource type definitions.
	/// </summary>
	struct ShaderResourceTypeEnum
	{
		enum List
		{
			Unknown, //!< Uses as default and error handling
			Buffer,
			Tex1D,
			Tex1DArray,
			Tex2D,
			Tex2DArray,
			Tex2DMS,
			Tex2DMSArray,
			Tex3D,
			Tex3DArray,
			TexCube,
			TexCubeArray
		};
	};
	typedef ShaderResourceTypeEnum::List ShaderResourceType;
}

#endif /// _NanoShaderResourceType_h_