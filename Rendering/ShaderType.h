//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Shader types.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoShaderType_h_
#define _NanoShaderType_h_

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>ShaderType</c> is a shader type enumeration definition.
	/// </summary>
	struct ShaderTypeEnum
	{
		enum List
		{
			Vertex, //!< Vertex shader
			Geometry, //!< Geometry shader
			Hull, //!< Hull shader
			Domain, //!< Domain shader
			Pixel, //!< Pixel shader
			Compute //!< Compute shader
		};
	};
	typedef ShaderTypeEnum::List ShaderType;
}

#endif /// _NanoShaderType_h_