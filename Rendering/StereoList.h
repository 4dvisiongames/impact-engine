//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Stereo type definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoStereoList_h_
#define _NanoStereoList_h_

namespace Renderer
{
	struct StereoListEnum
	{
		enum List
		{
			NoStereo,
			// half-resolution, non-square pixel views
			HalfResolutionLeftRight,
			HalfResolutionTopBottom,
			// two full resolution views side by side, as for a dual cable display
			FullResolutionSideBySize,
			FullResouutionInterlaced,
			// Quad-buffer suppport(for such API that support it)
			QuadBuffer,
			// two full resolution views stacked with a 30 pixel guard band
			// On the PC this can be configured as a custom video timing, but
			// it definitely isn't a consumer level task. The quad_buffer
			// support can handle 720P-3D with apropriate driver support.
			FullResolutionHDMI720p
		};
	};

	typedef StereoListEnum::List StereoList;
}

#endif /// _NanoStereoList_h_