//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Stream mapping utilities.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoStreamMapping_h_
#define _NanoStreamMapping_h_

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>StreamMapping</c> is a flags container for CPU-mapped stream.
	/// </summary>
	struct StreamMapping
	{
		enum List
		{
			DiscardPrevious = 0x0000001, //!< Discard all previous data
			CompareAndDiscard = 0x0000002, //!< Compare current data and rewrite what we need to change
		};
	};
}

#endif /// _NanoStreamMapping_h_