//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Stream source definitions for various vertex buffers.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoStreamSource_h_
#define _NanoStreamSource_h_

/// Vertex buffer
#include <Rendering/VertexBuffer.h>

namespace Renderer
{
	struct StreamSource
	{
		// This is implicitly constant, changes by used platform(in case if you need it)
		static const SizeT MaxStreams = 8;

		// My vertex buffer
		IntrusivePtr< VertexBuffer > myVertexBuffer;
		// Offset
		SizeT myOffset;
		// Stride
		SizeT myStride;

		/// <summary cref="StreamSource::StreamSource">
		/// Default constructor.
		/// </summary>
		StreamSource( void );

		/// <summary cref="StreamSource::StreamSource">
		/// Default constructor with parameters.
		/// </summary>
		/// <param name="name">Name of vertex declaration element.</param>
		/// <param name="index">Index of element.</param>
		/// <param name="fmt">Format type.</param>
		/// <param name="memoffset">Memory offset from last element.</param>
		/// <param name="attr">Vertex group attribute.</param>
		StreamSource( const IntrusivePtr< VertexBuffer >& rhs, SizeT offset, SizeT stride );

		/// <summary cref="StreamSource::StreamSource">
		/// Copy constructor.
		/// </summary>
		/// <param name="rhs">A reference to the vertex element.</param>
		StreamSource( const StreamSource& rhs );

		/// <summary cref="StreamSource::operator=">
		/// Assignement operator.
		/// </summary>
		void operator = ( const StreamSource& rhs );

		/// <summary cref="StreamSource::operator==">
		/// Equality operator.
		/// </summary>
		/// <param name="rhs">A reference to the other vertex elements.</param>
		/// <returns>True if equals, otherwise false.</returns>
		bool operator == ( const StreamSource& rhs );

		/// <summary cref="VertexElement::operator!=">
		/// Unequality operator.
		/// </summary>
		/// <param name="rhs">A reference to the other vertex elements.</param>
		/// <returns>True if unequals, otherwise false.</returns>
		bool operator != ( const StreamSource& rhs );
	};

	__IME_INLINED StreamSource::StreamSource( void ) : myVertexBuffer( nullptr ), myStride( 0 ), myOffset( 0 )
	{
		/// Empty
	}

	__IME_INLINED StreamSource::StreamSource( const IntrusivePtr< VertexBuffer >& rhs, SizeT offset, SizeT stride ) : myVertexBuffer( rhs ), myOffset( offset ), myStride( stride )
	{
		/// Empty
	}

	__IME_INLINED StreamSource::StreamSource( const StreamSource& rhs ) : myVertexBuffer( rhs.myVertexBuffer ), myOffset( rhs.myOffset ), myStride( rhs.myStride )
	{
		/// Empty
	}

	__IME_INLINED void StreamSource::operator = ( const StreamSource& rhs )
	{
		this->myVertexBuffer = rhs.myVertexBuffer;
		this->myOffset = rhs.myOffset;
		this->myStride = rhs.myStride;
	}

	__IME_INLINED bool StreamSource::operator == ( const StreamSource& rhs )
	{
		return (this->myVertexBuffer == rhs.myVertexBuffer) && (this->myOffset == rhs.myOffset) && (this->myStride == rhs.myStride);
	}

	__IME_INLINED bool StreamSource::operator != ( const StreamSource& rhs )
	{
		return !(*this == rhs);
	}
}

#endif /// _NanoStreamSource_h_