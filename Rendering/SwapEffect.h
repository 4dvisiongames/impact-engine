//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Swap effects definitions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoSwapEffect_h_
#define _NanoSwapEffect_h_

namespace Renderer
{
	struct SwapEffectEnum
	{
		enum List
		{
			NoSync,
			Vertical,
			Smart
		};
	};

	typedef SwapEffectEnum::List SwapEffect;
}

#endif /// _NanoSwapEffect_h_