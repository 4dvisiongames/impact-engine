//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Texture definitions used in engine.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoTexture_h_
#define _NanoTexture_h_

#if defined( __IME_PLATFORM_WIN64 )
/// Direct3D11 texture
#include <Rendering/Direct3D11/D3D11Texture.h>
#else
#error Texture is not implemented on this platfom!
#endif

#endif /// _NanoTexture_h_