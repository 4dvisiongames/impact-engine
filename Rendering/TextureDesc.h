//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Texture descriptions.
///
/// Author: Pavel Umnikov
//=====================================================================================

#ifndef _NanoTextureDesc_h_
#define _NanoTextureDesc_h_

/// Texture format
#include <Rendering/TextureFormat.h>

/// Rendering subsystem namespace
namespace Renderer
{
	struct MSAADesciption
	{
		u32 mySampleCount;
		u32 mySampleQuality;
	};

	/// <summary>
	/// <c>Texture1DDesc</c> is a 1D texture description.
	/// </summary>
	struct Texture1DDesc
	{
		u32 myWidth; //!< Width
		u16 mySlices; //!< Array size
		TextureFormat myFormat; //!< Texture format
		bool changeable; //!< Resetup current texture 
		bool rendereable;

		//! Default constructor.
		Texture1DDesc( void ) 
			: myWidth( 1 )
			, mySlices( 1 )
			, myFormat( TextureFormatEnum::Unknown )
			, changeable( false )
			, rendereable( false )
		{
		}

		//! Constructor with parameters.
		Texture1DDesc( u32 width, u16 slices, TextureFormat format, bool change, bool isRendereable ) 
			: myWidth( width )
			, mySlices( slices )
			, myFormat( format )
			, changeable( change )
			, rendereable( isRendereable )
		{
			/// Empty
		}
	};

	/// <summary>
	/// <c>Texture2DDesc</c> is a 2D texture description.
	/// </summary>
	struct Texture2DDesc
	{
		u32 myWidth; //!< Width
		u32 myHeight; //!< Height
		u16 mySlices; //! Array size
		MSAADesciption myMSAA;
		TextureFormat myFormat; //!< Texture format
		bool changeable; //!< Texture can be resetupped
		bool rendereable;

		//! Default constructor.
		Texture2DDesc( void ) 
			: myWidth( 0 )
			, myHeight( 0 )
			, mySlices( 1 )
			, myFormat( TextureFormatEnum::Unknown )
			, changeable( false )
			, rendereable( false )
		{
			this->myMSAA.mySampleCount = 1;
			this->myMSAA.mySampleQuality = 0;
		}

		//! Constructor with parameters.
		Texture2DDesc( u32 width, u32 height, u32 msaaSamples, u32 msaaQuality, u16 slices, TextureFormat format, bool change, bool isRendereable ) 
			: myWidth( width )
			, myHeight( height )
			, mySlices( slices )
			, myFormat( format )
			, changeable( change )
			, rendereable( isRendereable )
		{
			this->myMSAA.mySampleCount = msaaSamples;
			this->myMSAA.mySampleQuality = msaaQuality;
		}
	};

	/// <summary>
	/// <c>Texture3DDesc</c> is a 3D texture description.
	/// </summary>
	struct Texture3DDesc
	{
		u32 myWidth; //!< Width
		u32 myHeight; //!< Height
		u32 myDepth; //!< Depth
		u16 mySlices; //! Array size
		TextureFormat myFormat; //!< Texture format
		bool changeable; //!< Texture can be resetupped
		bool rendereable; //!< Can be uased for rendering to offscreen texture

		//! Default constructor.
		Texture3DDesc( void ) 
			: myWidth( 0 )
			, myHeight( 0 )
			, myDepth( 0 )
			, mySlices( 1 )
			, myFormat( TextureFormatEnum::Unknown )
			, changeable( false )
			, rendereable( false )
		{
			/// Empty
		}

		//! Constructor with parameters.
		Texture3DDesc( u32 width, u32 height, u32 depth, u16 slices, TextureFormat format, bool change, bool isRendereable ) 
			: myWidth( width )
			, myHeight( height )
			, myDepth( depth )
			, mySlices( slices )
			, myFormat( format )
			, changeable( change )
			, rendereable( isRendereable )
		{
			/// Empty
		}
	};

	/// <summary>
	/// <c>TextureCubeDesc</c> is a cubemap texture description.
	/// </summary>
	struct TextureCubeDesc
	{
		u32 myWidth; //!< Width
		u32 myHeight; //!< Height
		u16 mySlices; //! Array size
		TextureFormat myFormat; //!< Texture format
		bool changeable; //!< Texture can be resetupped
		bool rendereable; //!< Can be uased for rendering to offscreen texture

		//! Default constructor.
		TextureCubeDesc( void ) 
			: myWidth( 0 )
			, myHeight( 0 )
			, mySlices( 1 )
			, myFormat( TextureFormatEnum::Unknown )
			, changeable( false )
			, rendereable( false )
		{
			/// Empty
		}

		//! Constructor with parameters.
		TextureCubeDesc( u32 width, u32 height, u16 slices, TextureFormat format, bool change, bool isRendereable ) 
			: myWidth( width )
			, myHeight( height )
			, mySlices( slices )
			, myFormat( format )
			, changeable( change )
			, rendereable( isRendereable )
		{
			/// Empty
		}
	};
}

#endif /// _NanpTextureDesc_h_