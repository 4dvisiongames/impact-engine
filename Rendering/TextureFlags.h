//============== (C) Copyright 2012, PARANOIA Team. All rights reserved. ==============
/// Desc: Texture flags.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoTextureFlags_h_
#define _NanoTextureFlags_h_

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>TextureBindFlags</c> is a texture flag definitions.
	/// </summary>
	struct TextureBindFlagsEnum
	{
		enum List
		{
			RenderTargetView = (1 << 1), //!< Can be used as Render Target View
			ShaderResourceView = (1 << 2), //!< Can be used as Shader Resource View
			UnorederedAccessView = (1 << 3), //!< Can be used as Unordered Access View
			DepthStencilView = (1 << 4), //!< Can be used as Depth-Stencil View
		};
	};
	typedef TextureBindFlagsEnum::List TextureBindFlags;
}

#endif /// _NanoTextureFlags_h_