//============= (C) Copyright 2010, PARANOIA Team. All rights reserved. =============
/// Desc: Render target format descriptions and enumerations.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Render target formats
#include <Rendering/TextureFormat.h>

/// Rendering sub-system namespace
namespace Renderer
{
	STL::String< Char8 > TextureFormatConverter::ToString( const TextureFormat format )
	{
		switch( format )
		{
		case TextureFormatEnum::RGBA8: return "RGBA8";
		case TextureFormatEnum::RGB8E5: return "RGB8E5";
		case TextureFormatEnum::R32F: return "R32F";
		case TextureFormatEnum::RG16F: return "RG16F";
		case TextureFormatEnum::RGBA16F: return "RGBA16F";
		case TextureFormatEnum::Depth32: return "Depth32";
		case TextureFormatEnum::Depth24Stencil8: return "Depth24Stencil8";
		case TextureFormatEnum::DepthAccessiable: return "DepthAccessiable";
		case TextureFormatEnum::DepthStencilAccessiable: return "DepthStencilAccessiable";
		default: return "Unknown";
		};
	}

	TextureFormat TextureFormatConverter::ToFormat( const STL::String< Char8 > string )
	{
		if( string == "RGBA8" ) return TextureFormatEnum::RGBA8;
		else if( string == "RGB8E5" ) return TextureFormatEnum::RGB8E5;
		else if( string == "R32F" ) return TextureFormatEnum::R32F;
		else if( string == "RG16F" ) return TextureFormatEnum::RG16F;
		else if( string == "RGBA16F" ) return TextureFormatEnum::RGBA16F;
		else if( string == "Depth32" ) return TextureFormatEnum::Depth32;
		else if( string == "Depth24Stencil8" ) return TextureFormatEnum::Depth24Stencil8;
		else if( string == "DepthAccessiable" ) return TextureFormatEnum::DepthAccessiable;
		else if( string == "DepthStencilAccessiable" ) return TextureFormatEnum::DepthStencilAccessiable;
		else return TextureFormatEnum::Unknown;
	}

	u32 TextureFormatConverter::ToBPP( const TextureFormat format )
	{
		switch( format )
		{
		case TextureFormatEnum::Depth32:
		case TextureFormatEnum::Depth24Stencil8:
		case TextureFormatEnum::RGBA8:
		case TextureFormatEnum::RGB8E5:
		case TextureFormatEnum::R32F:
		case TextureFormatEnum::RG16F: return 32;
		case TextureFormatEnum::RGBA16F: return 64;
		default: return 0;
		};
	}
}