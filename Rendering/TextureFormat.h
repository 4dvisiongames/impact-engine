//============= (C) Copyright 2010, PARANOIA Team. All rights reserved. =============
/// Desc: Render target format descriptions and enumerations.
///
/// Author: Pavel Umnikov
//===================================================================================

#ifndef _NanoTextureFormat_h_
#define _NanoTextureFormat_h_

/// STL string
#include <Foundation/STLString.h>

/// Renderer sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>TextureFormatEnum</c> is a render target format definitions.
	/// </summary>
	struct TextureFormatEnum
	{
		enum List
		{
			/// Unknown format
			Unknown = 0x0000000,
			/// Integer formats
			RGBA8,
			RGB8E5, //!< Fixed-point HDR
			/// Floating point formats
			R32F,
			RG16F,
			RGBA16F,
			/// Depth buffer formats
			Depth32,
			Depth24Stencil8, //!< Depth + Stencil
			/// Depth buffer(readable) formats
			DepthAccessiable,
			DepthStencilAccessiable,
			/// Compressed formats
			DXT1,
			DXT3,
			DXT5,
		};
	};
	typedef TextureFormatEnum::List TextureFormat;

	class TextureFormatConverter
	{
	public:
		/// <summary cref="TextureFormatConverter::ToString">
		/// Convert render target format to string type.
		/// </summary>
		/// <param name="format">Source format.</param>
		static STL::String< Char8 > ToString( const TextureFormat format );

		/// <summary cref="TextureFormatConverter::ToFormat">
		/// Convert string type to render target format.
		/// </summary>
		/// <param name="string">Source string.</param>
		static TextureFormat ToFormat( const STL::String< Char8 > string );

		/// <summary cref="TextureFormatConverter::ToBPP">
		/// Convert format to bits per pixel value.
		/// </summary>
		/// <param name="format">Source format.</param>
		/// <returns>Bits Per Pixel of current image format.</returns>
		static u32 ToBPP( const TextureFormat format );
	};
};

#endif /// _NanoTextureFormat_h_