//============== (C) Copyright 2014, PARANOIA Team. All rights reserved. ==============
/// Desc: Texture type definition.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

namespace Renderer
{
	struct TextureTypeEnum
	{
		enum List
		{
			Tex1D = 1 << 1,
			Tex2D = 1 << 2,
			Tex3D = 1 << 3,
			TexCube = 1 << 4,

			TexRendereable = 1 << 10, //!< Additional flag used as addition for rendering to this texture

			MaxCount
		};
	};
	typedef TextureTypeEnum::List TextureType;
}