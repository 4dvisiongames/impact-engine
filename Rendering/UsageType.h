#pragma once

#ifndef _NanoUsageType_h_
#define _NanoUsageType_h_

namespace Renderer
{
	/// <summary>
	/// <c>UsageFlags</c> is a flags contatiner of buffer type.
	/// </summary>
	struct UsageFlagsEnum
	{
		enum List
		{
			Default, //!< Default usage of resource(Direct3D will do itself all)
			Static, //!< Static buffer flag
			Dynamic, //!< Dynamic buffer flag
		};
	};
	typedef UsageFlagsEnum::List UsageFlags;
}

#endif /// _NanoUsageType_h_