//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Vertex buffer definitions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoVertexBuffer_h_
#define _NanoVertexBuffer_h_

#if defined( __IME_PLATFORM_WIN64 )
/// OpenGL vertex buffer
#include <Rendering/Direct3D11/D3D11VertexBuffer.h>
#else
#error Vertex Buffer is not implemented on this platfom!
#endif

#endif /// _NanoVertexBuffer_h_