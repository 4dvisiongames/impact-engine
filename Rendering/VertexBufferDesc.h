#pragma once

#ifndef __Rendering__VertexBufferDesc_h__
#define __Rendering__VertexBufferDesc_h__

/// CPU access
#include <Rendering/CPUAccess.h>
/// Usage
#include <Rendering/UsageType.h>

namespace Renderer
{
	struct VertexBufferDesc
	{
		SizeT myCPUAccessMask;
		Renderer::UsageFlags myUsageFlags;
		SizeT myVertexCount;
		SizeT myVertexSize;

		/// These used only on first time data uploading
		void* myData; //!< Pointer to the data

		VertexBufferDesc( void );
		VertexBufferDesc( SizeT accessMask, Renderer::UsageFlags flag, SizeT vertexSize, SizeT vertices, void* data );
	};

	__IME_INLINED VertexBufferDesc::VertexBufferDesc( void )
		: myCPUAccessMask( 0 )
		, myUsageFlags( Renderer::UsageFlagsEnum::Default )
		, myVertexCount( 0 )
		, myVertexSize( 0 )
		, myData( nullptr )
	{
		/// Empty
	}

	__IME_INLINED VertexBufferDesc::VertexBufferDesc( SizeT accessMask, Renderer::UsageFlags flag, SizeT vertexSize, SizeT vertices, void* data )
		: myCPUAccessMask( accessMask )
		, myUsageFlags( flag )
		, myVertexSize( vertexSize )
		, myVertexCount( vertices )
		, myData( data )
	{
		/// Empty
	}
}

#endif /// __Rendering__VertexBufferDesc_h__