//============= (C) Copyright 2011, PARANOIA Team. All rights reserved. =============
/// Desc: Vertex element defintions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoVertexElement_h_
#define _NanoVertexElement_h_

/// STL String
#include <Foundation/STLString.h>
/// Vertex element format
#include <Rendering/VertexElementFormat.h>
/// Vertex element group
#include <Rendering/VertexElementGroup.h>

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>VertexElement</c> is a vertex element defintion.
	/// </summary>
	struct VertexElement
	{
		STL::String< Char8 > elementName; //!< Semantic name
		s32 elementIndex; //!< Element index
		Renderer::VertexElementFormat::List format; //!< Vertex element format
		u32 offset; //!< Memory offset from last element
		Renderer::VertexElementGroup::List attrib; //!< Vertex element group definition

		/// <summary cref="VertexElement::VertexElement">
		/// Default constructor.
		/// </summary>
		VertexElement( void );

		/// <summary cref="VertexElement::VertexElement">
		/// Default constructor with parameters.
		/// </summary>
		/// <param name="name">Name of vertex declaration element.</param>
		/// <param name="index">Index of element.</param>
		/// <param name="fmt">Format type.</param>
		/// <param name="memoffset">Memory offset from last element.</param>
		/// <param name="attr">Vertex group attribute.</param>
		VertexElement( const STL::String< Char8 >& name, s32 index, Renderer::VertexElementFormat::List fmt, u32 offset, 
			Renderer::VertexElementGroup::List attr );

		/// <summary cref="VertexElement::VertexElement">
		/// Copy constructor.
		/// </summary>
		/// <param name="rhs">A reference to the vertex element.</param>
		VertexElement( const VertexElement& rhs );

		/// <summary cref="VertexElement::operator=">
		/// Assignement operator.
		/// </summary>
		void operator = ( const VertexElement& rhs );

		/// <summary cref="VertexElement::operator==">
		/// Equality operator.
		/// </summary>
		/// <param name="rhs">A reference to the other vertex elements.</param>
		/// <returns>True if equals, otherwise false.</returns>
		bool operator == ( const VertexElement& rhs );

		/// <summary cref="VertexElement::operator!=">
		/// Unequality operator.
		/// </summary>
		/// <param name="rhs">A reference to the other vertex elements.</param>
		/// <returns>True if unequals, otherwise false.</returns>
		bool operator != ( const VertexElement& rhs );
	};

	__IME_INLINED VertexElement::VertexElement( void ) : elementName( "DefaultElement" ), elementIndex( 0 ), 
		format( Renderer::VertexElementFormat::Unknown ), offset( 0 ), attrib( Renderer::VertexElementGroup::PerVertex )
	{
		/// Empty
	}

	__IME_INLINED VertexElement::VertexElement( const STL::String< Char8 >& name, s32 index, Renderer::VertexElementFormat::List fmt, 
		u32 memoffset,	Renderer::VertexElementGroup::List attr ) : elementName( name ), elementIndex( index ), format( fmt ), offset( memoffset ), attrib( attr )
	{
		/// Empty
	}

	__IME_INLINED VertexElement::VertexElement( const VertexElement& rhs ) : elementName( rhs.elementName ), elementIndex( rhs.elementIndex ), 
		format( rhs.format ), offset( rhs.offset ), attrib( rhs.attrib )
	{
		/// Empty
	}

	__IME_INLINED void VertexElement::operator = ( const VertexElement& rhs )
	{
		this->attrib = rhs.attrib;
		this->elementIndex = rhs.elementIndex;
		this->elementName = rhs.elementName;
		this->format = rhs.format;
		this->offset = rhs.offset;
	}

	__IME_INLINED bool VertexElement::operator == ( const VertexElement& rhs )
	{
		return((this->attrib == rhs.attrib) && (this->elementIndex == rhs.elementIndex) && (this->elementName == rhs.elementName) &&
			   (this->format == rhs.format) && (this->offset == rhs.offset) );
	}

	__IME_INLINED bool VertexElement::operator != ( const VertexElement& rhs )
	{
		return !(*this == rhs);
	}
}

#endif /// _NanoVertexElement_h_