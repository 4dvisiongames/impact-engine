//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Vertex element format defintions.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Vertex element format
#include <Rendering/VertexElementFormat.h>

/// Rendering sub-system namespace
namespace Renderer
{
	unsigned int VertexElementFormatConverter::ToFormatSize( Renderer::VertexElementFormat::List format )
	{
		switch( format )
		{
		case VertexElementFormat::Half_Single:
			return 2; ///< R16F - 16 bits
			break;

		case VertexElementFormat::Half_XY:
		case VertexElementFormat::Float_Single:
		case VertexElementFormat::Integer_Single:
			return 4; ///< R16G16F, R32F and R32UINT - 32 bits
			break;

		case VertexElementFormat::Half_XYZW:
		case VertexElementFormat::Float_XY:
		case VertexElementFormat::Integer_XY:
			return 8; ///< R16G16B16A16F, R32G32F and R32G32UINT - 64 bits
			break;

		case VertexElementFormat::Float_XYZW:
		case VertexElementFormat::Integer_XYZW:
			return 16; ///< R32G32B32A32F and R32G32B32A32UINT - 128 bits
			break;

		default:
			return NULL; ///< ZONER: This case is error-prone
			break;
		}
	}
}