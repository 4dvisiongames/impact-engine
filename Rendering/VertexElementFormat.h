//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Vertex element format defintions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoVertexElementFormat_h_
#define _NanoVertexElementFormat_h_

/// STL string
#include <Foundation/STLString.h>

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>VertexElementFormat</c> is vertex declaration formats enumeration.
	/// </summary>
	struct VertexElementFormat
	{
		enum List
		{
			/// Floating point types( 16 bit per value )
			Half_Single,
			Half_XY,
			Half_XYZW,

			/// Double types( 32 bit per value )
			Float_Single,
			Float_XY,
			Float_XYZW,

			/// Integer types(ZONER: native on D3D10 or GL_EXT_texture_integer)
			Integer_Single,
			Integer_XY,
			Integer_XYZW,

			/// Unknown type
			Unknown,
		};
	};

	/// <summary>
	/// <c>VertexElementFormatConverter</c> is a vertex element format converting class.
	/// </summary>
	class VertexElementFormatConverter
	{
	public:
		/// <summary cref="VertexElementFormatConverter::ToFormatSize">
		/// Convert format to specified size.
		/// </summary>
		/// <param name="format">Input format.</param>
		/// <returns>Size of format.</returns>
		static unsigned int ToFormatSize( Renderer::VertexElementFormat::List format );
	};
}

#endif /// _NanoVertexElementFormat_h_