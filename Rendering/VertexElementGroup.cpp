//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Vertex element group defintions.
///
/// Author: Pavel Umnikov
//===================================================================================

/// Vertex element group
#include <Rendering/VertexElementGroup.h>

/// Rendering sub-system namespace
namespace Renderer
{
	STL::String< Char8 > VertexElementGroupConverter::ToString( const Renderer::VertexElementGroup::List type )
	{
		switch( type )
		{
		case Renderer::VertexElementGroup::PerInstance:
			return "Per-Instance";
			break;

		case Renderer::VertexElementGroup::PerVertex:
			return "Per-Vertex";

		default:
			return "Error";
			break;
		};
	};
}