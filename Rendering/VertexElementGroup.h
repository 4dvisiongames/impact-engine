//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Vertex element group defintions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoVertexElementGroup_h_
#define _NanoVertexElementGroup_h_

/// STL string
#include <Foundation/STLString.h>

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>VertexElementGroup</c> is vertex attribute grouping(how to execute vertex component)
	/// </summary>
	struct VertexElementGroup
	{
		enum List
		{
			///< ZONER: You set this only if attribute really need to per-vertex(like TBN)
			PerVertex, //!< Pre-vertex attribute
			///< ZONER: You set this only if attribute needs to be per-instance(like object id)
			PerInstance, //!< Per-instance attribute
		};
	};

	/// <summary>
	/// <c>VertexElementGroupConverter</c> is vertex element group attribute converter.
	/// </summary>
	class VertexElementGroupConverter
	{
	public:
		/// <summary cref="VertexElementGroupConverter::ToString">
		/// Convert element group attribute to string type.
		/// </summary>
		/// <param name="type">Input type.</param>
		/// <returns>Converted element group to string.</returns>
		static STL::String< Char8 > ToString( const Renderer::VertexElementGroup::List type );
	};
}

#endif /// _NanoVertexElementGroup_h_