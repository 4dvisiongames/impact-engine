//============= (C) Copyright 2012, PARANOIA Team. All rights reserved. =============
/// Desc: Basic vertex element semantic defintions.
///
/// Author: Pavel Umnikov
//===================================================================================

#pragma once

#ifndef _NanoVertexElementSemantic_h_
#define _NanoVertexElementSemantic_h_

/// STL string
#include <Foundation/STLString.h>

/// Rendering sub-system namespace
namespace Renderer
{
	/// <summary>
	/// <c>ElementSemantic</c> is a semantic definitons.
	/// </summary>
	struct ElementSemantic
	{
		enum List
		{
			Position, //!< Position vertex element semantic
			Normal, //!< Normal vertex element semantic
			Tangent, //!< Tangential vertex element semantic
			Binormal, //!< Bi-Normal vertex element semantic
			Texcoord, //!< Texture coordinate vertex element semantic
			Colour, //!< Colour vertex element semantic
			BlendWeight, //!< Blended weights vertex element semantic
		};
	};

	/// <summary>
	/// <c>ElementSemanticConverter</c> is converting system of element semantic.
	/// </summary>
	class ElementSemanticConverter
	{
	public:
		/// <summary cref="ElementSemanticConverter::ToString">
		/// Convert semantic of element to the string.
		/// </summary>
		static STL::String< Char8 > ToString( const ElementSemantic::List semantic );
	};
}

#endif /// _NanoVertexElementSemantic_h_