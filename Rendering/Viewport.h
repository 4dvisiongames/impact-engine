//============== (C) Copyright 2011, PARANOIA Team. All rights reserved. ==============
/// Desc: Window client-space handling.
///
/// Author: Pavel Umnikov
//=====================================================================================

#pragma once

#ifndef _NanoViewport_h_
#define _NanoViewport_h_

namespace Renderer
{
	struct Viewport
	{
		u32 myHeight; //!< Client-space height
		u32 myWidth; //!< Client-space width
		float myNearDepth;
		float myFarDepth;
	};
}

#endif /// _NanoViewport_h_