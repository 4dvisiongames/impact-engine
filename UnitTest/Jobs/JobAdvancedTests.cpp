/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include "stdafx.h"
#include <Foundation/Job.h>
#include <Foundation/JobArea.h>
#include <Foundation/JobDispatcher.h>
#include <Foundation/JobServer.h>

typedef float Value;

struct TreeNode
{
	//! Pointer to left subtree
	TreeNode* left;
	//! Pointer to right subtree
	TreeNode* right;
	//! Number of nodes in this subtree, including this node.
	long node_count;
	//! Value associated with the node.
	Value value;
};

class OptimizedSumTask : public Jobs::Job
{
public:
	OptimizedSumTask( TreeNode* root_, Value* sum_ )
		: root( root_ )
		, sum( sum_ )
		, is_continuation( false )
	{
	}

	Jobs::Job* Execute() 
	{
		Jobs::Job* next = nullptr;
		if ( !is_continuation )
		{
			if ( root->node_count < 1000 )
			{
				*sum = SerialSumTree( root );
			}
			else
			{
				// Create tasks before spawning any of them.
				Jobs::Job* a = nullptr;
				Jobs::Job* b = nullptr;
				if ( root->left )
				{
					a = new OptimizedSumTask( root->left, &x );
					if ( a )
						a->SetParent( this );
				}

				if ( root->right )
				{
					b = new OptimizedSumTask( root->right, &y );
					if ( b )
						b->SetParent( this );
				}

				this->RescheduleAsContinuation();

				is_continuation = true;
				this->SetRefCount< Threading::MemoryOrderEnum::Relaxed >( 1 );
				if ( a )
				{
					this->AddRef<Threading::MemoryOrderEnum::Relaxed>();
					a->Setup();
				}

				if ( b )
				{
					this->AddRef<Threading::MemoryOrderEnum::Relaxed>();
					b->Setup();
				}

				if ( a )
				{
					if ( b )
						Jobs::Dispatcher::Instance()->Spawn( b );
				}
				else
				{
					a = b;
				}

				next = a;
			}
		}
		else
		{
			*sum = root->value;
			if ( root->left )
				*sum += x;
			if ( root->right )
				*sum += y;
		}

		return next;
	}

	static Value SerialSumTree( TreeNode* root )
	{
		Value result = root->value;
		if ( root->left )
			result += SerialSumTree( root->left );
		if ( root->right )
			result += SerialSumTree( root->right );
		return result;
	}

private:
	Value* const sum;
	TreeNode* root;
	bool is_continuation;
	Value x, y;
};

class JobAdvancedParallelSumTreeTest : public UnitTestsLib::TestCase
{
	__IME_DECLARE_TEST_RTTI;
	__IME_IMPLEMENT_TEST_CONSTRUCTOR( JobAdvancedParallelSumTreeTest, CreateParallelSumTreeTest );
	__IME_IMPLEMENT_TEST_DESTRUCTOR( JobAdvancedParallelSumTreeTest, DestroyParallelSumTreeTest );

	const long numberOfNodes = 10000000;

	JobAdvancedParallelSumTreeTest( UnitTestsLib::TestOutputLogger* logger )
		: UnitTestsLib::TestCase( logger )
	{
		// Empty
	}

	virtual void Run()
	{
		IntrusivePtr< Jobs::Server > jobsServer = new Jobs::Server();
		jobsServer->Setup();

		// Configured jobs server and check if everything is valid, ideally
		// IsValid() must be always true after initialization
		Verify( jobsServer.IsValid(), true, __FILE__, __LINE__ );
		Verify( jobsServer->IsValid(), true, __FILE__, __LINE__ );
		Verify( Jobs::Dispatcher::HasInstance(), true, __FILE__, __LINE__ );

		Value sum;
		TreeNode* root = new TreeNode();
		root->node_count = numberOfNodes;
		root->value = Value( Math::Pi * numberOfNodes );
		OptimizedSumTask* parallelSum = new OptimizedSumTask( root, &sum );
		parallelSum->SetRefCount<Threading::MemoryOrderEnum::Relaxed>( 1 );
		parallelSum->Setup();
		Jobs::Dispatcher::Instance()->Spawn( parallelSum );
		Jobs::Dispatcher::Instance()->Wait( parallelSum );

		// Since we done with all, we need to ensure all threads to stop everything, first we need
		// to say them that we have something for them(e.g. wake them all), then change all states
		// of threads from server to Exit, so threads will check their new state and exit on some
		// point of execution. See Jobs::JobWorker::Execute method, where all state checking points
		// are set.
		Verify( jobsServer->IsNeedToWakeWorkers(), true, __FILE__, __LINE__ );

		jobsServer->Discard();
		jobsServer = nullptr;
	}
};

__IME_IMPLEMENT_TEST_RTTI(
	"Jobs: parallel sum tree",
	JobAdvancedParallelSumTreeTest,
	JobAdvancedParallelSumTreeTest::CreateParallelSumTreeTest,
	JobAdvancedParallelSumTreeTest::DestroyParallelSumTreeTest );

const UnitTestsLib::SimpleRtti* jobsAdvancedParallelSumTree = &JobAdvancedParallelSumTreeTest::RTTI;