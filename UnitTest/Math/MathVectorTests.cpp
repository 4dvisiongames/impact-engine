/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include "stdafx.h"
#include <Foundation/MathVector.h>

class MathVectorAssignTest : public UnitTestsLib::TestCase
{
	__IME_DECLARE_TEST_RTTI;
	__IME_IMPLEMENT_TEST_CONSTRUCTOR( MathVectorAssignTest, CreateVectorTests );
	__IME_IMPLEMENT_TEST_DESTRUCTOR( MathVectorAssignTest, DestroyVectorTests );

	MathVectorAssignTest( UnitTestsLib::TestOutputLogger* logger )
		: UnitTestsLib::TestCase( logger )
	{
		// Empty
	}

	virtual void Run()
	{
		Math::Vector vec( 1.0f, 3.0f, 5.0f );
		float x = vec.GetX();
		float y = vec.GetY();
		float z = vec.GetZ();

		this->Verify( x == 1.0f, true, __FILE__, __LINE__ );
		this->Verify( y == 3.0f, true, __FILE__, __LINE__ );
		this->Verify( z == 5.0f, true, __FILE__, __LINE__ );
	}
};

__IME_IMPLEMENT_TEST_RTTI( 
	"Math Vector: assign from float", 
	MathVectorAssignTest, 
	MathVectorAssignTest::CreateVectorTests, 
	MathVectorAssignTest::DestroyVectorTests );

class MathVectorAssignFromFloatInVecTest : public UnitTestsLib::TestCase
{
	__IME_DECLARE_TEST_RTTI;
	__IME_IMPLEMENT_TEST_CONSTRUCTOR( MathVectorAssignFromFloatInVecTest, CreateVectorTests );
	__IME_IMPLEMENT_TEST_DESTRUCTOR( MathVectorAssignFromFloatInVecTest, DestroyVectorTests );

	MathVectorAssignFromFloatInVecTest( UnitTestsLib::TestOutputLogger* logger )
		: UnitTestsLib::TestCase( logger )
	{
		// Empty
	}

	virtual void Run()
	{
		Math::FloatInVec x( 1.0f );
		Math::FloatInVec y( 3.0f );
		Math::FloatInVec z( 5.0f );
		Math::Vector vec( x, y, z );
		float fx = vec.GetX();
		float fy = vec.GetY();
		float fz = vec.GetZ();

		this->Verify( fx == 1.0f, true, __FILE__, __LINE__ );
		this->Verify( fy == 3.0f, true, __FILE__, __LINE__ );
		this->Verify( fz == 5.0f, true, __FILE__, __LINE__ );
	}
};

__IME_IMPLEMENT_TEST_RTTI( 
	"Math Vector: assign from vectorized float",
	MathVectorAssignFromFloatInVecTest,
	MathVectorAssignFromFloatInVecTest::CreateVectorTests,
	MathVectorAssignFromFloatInVecTest::DestroyVectorTests );

class MathVectorSubtractionTest : public UnitTestsLib::TestCase
{
	__IME_DECLARE_TEST_RTTI;
	__IME_IMPLEMENT_TEST_CONSTRUCTOR( MathVectorSubtractionTest, CreateVectorTests );
	__IME_IMPLEMENT_TEST_DESTRUCTOR( MathVectorSubtractionTest, DestroyVectorTests );

	MathVectorSubtractionTest( UnitTestsLib::TestOutputLogger* logger )
		: UnitTestsLib::TestCase( logger )
	{
		// Empty
	}

	virtual void Run()
	{
		Math::Vector vec1( 3.0f, 5.0f, 7.0f );
		Math::Vector vec2( 1.0f, 3.0f, 5.0f );
		Math::Vector vec = vec1 - vec2;
		float fx = vec.GetX();
		float fy = vec.GetY();
		float fz = vec.GetZ();

		this->Verify( fx == 2.0f, true, __FILE__, __LINE__ );
		this->Verify( fy == 2.0f, true, __FILE__, __LINE__ );
		this->Verify( fz == 2.0f, true, __FILE__, __LINE__ );
	}
};

__IME_IMPLEMENT_TEST_RTTI( 
	"Math Vector: subtraction := vecA - vecB",
	MathVectorSubtractionTest,
	MathVectorSubtractionTest::CreateVectorTests,
	MathVectorSubtractionTest::DestroyVectorTests );

// Math vector tests
const UnitTestsLib::SimpleRtti* vectorAssignTestRtti = &MathVectorAssignTest::RTTI;
const UnitTestsLib::SimpleRtti* vectorAssignFromFloatInVecTest = &MathVectorAssignFromFloatInVecTest::RTTI;
const UnitTestsLib::SimpleRtti* vectorSutractionTest = &MathVectorSubtractionTest::RTTI;