// UnitTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <Foundation/System.h>
#include <UnitTest/Math/MathTests.h>
#include <UnitTest/Jobs/JobTests.h>

class ImEFailureHandler : public UnitTestsLib::TestFailureHandler
{
public:
	virtual void OnFailure( const char* message )
	{
		Core::System::Error( message );
	}
};

class ImEOutputLogger : public UnitTestsLib::TestOutputLogger
{
public:
	virtual void Output( const char* message )
	{
		Debug::Messaging::Send( Debug::EMessageType::Normal, message );
	}
};

void __cdecl _destroyOnExit()
{
	/// Exit from engine
	Core::System::Exit( 0 );
}

int main(int argc, char* argv[])
{
	/// Setup engines core routines including
	/// multithreading and mathematics related functions
	Core::System::Setup( true );
	atexit( _destroyOnExit );

	/// Inner loop
	{
		ImEFailureHandler failureHandler;
		ImEOutputLogger outputLogger;

		try
		{
			UnitTestsLib::TestRunner runner;

			runner.RegisterFailureHandler( &failureHandler );
			runner.RegisterOutputLogger( &outputLogger );
		
			PrepareMathTests( runner );
			PrepareCoreJobTests( runner );
			PrepareAdvancedJobTests( runner );

			runner.Run();
		}
		catch ( UnitTestsLib::TestStartupException e )
		{
			Core::System::Error( e.what() );
		}
	}

	system( "pause" );

	/// Exit process
	return 0;
}

