/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <UnitTestsLib/Base/TestCase.h>
#include <UnitTestsLib/Base/TestFailureException.h>

/// C++ STL
#include <typeinfo>
#include <sstream>

namespace UnitTestsLib
{
	const SimpleRtti TestCase::RTTI( "TestCaseNotUsed", nullptr, nullptr );

	TestCase::TestCase( TestOutputLogger* logger )
		: myNext( nullptr )
		, myOutputLogger( logger )
		, myNumFailed( 0 )
		, myNumSucceeded( 0 )
		, myNumVerified( 0 )
	{
		/// Empty
	}

	TestCase::~TestCase()
	{
		/// Empty
	}

	void TestCase::Verify( bool statement, bool expect, const char* file, int line )
	{
		this->myNumVerified++;

		const UnitTestsLib::SimpleRtti& rtti = this->GetRtti();

		if ( statement != expect )
		{
			std::stringstream strStream;
			strStream << "Failed case[" 
				<< rtti.myTestCaseName.c_str()
				<< "]: file [" 
				<< file 
				<< "] on line [" 
				<< line 
				<< "]: expected [" 
				<< expect 
				<< "]; actual ["
				<< statement
				<< "]\r\n";

			std::string& str = strStream.str();

			this->myOutputLogger->Output( str.c_str() );
			this->myNumFailed++;

			throw TestFailureException( str.c_str() );
		}
		else
		{
			std::stringstream strStream;
			strStream << "Passed case["
				<< rtti.myTestCaseName.c_str()
				<< "]: file ["
				<< file
				<< "] on line ["
				<< line
				<< "]: expected ["
				<< expect
				<< "]; actual ["
				<< statement
				<< "]\r\n";

			std::string& str = strStream.str();
			this->myOutputLogger->Output( str.c_str() );

			this->myNumSucceeded++;
		}
	}

	void TestCase::Verify( bool statement, bool expect, const char* message, const char* file, int line )
	{
		this->myNumVerified++;

		const UnitTestsLib::SimpleRtti& rtti = this->GetRtti();

		if ( statement != expect )
		{
			std::stringstream strStream;
			strStream << "Failed at class["
				<< rtti.myTestCaseName.c_str()
				<< "]: file ["
				<< file
				<< "] on line ["
				<< line
				<< "]: expected ["
				<< expect
				<< "]; actual ["
				<< statement
				<< "]\r\nMessage:["
				<< message
				<< "]\r\n";

			std::string& str = strStream.str();
			this->myOutputLogger->Output( str.c_str() );

			this->myNumFailed++;

			throw TestFailureException( str.c_str() );
		}
		else
		{
			std::stringstream strStream;
			strStream << "Failed at class["
				<< rtti.myTestCaseName.c_str()
				<< "]: file ["
				<< file
				<< "] on line ["
				<< line
				<< "]: expected ["
				<< expect
				<< "]; actual ["
				<< statement
				<< "]\r\nMessage:["
				<< message
				<< "]\r\n";

			std::string& str = strStream.str();
			this->myOutputLogger->Output( str.c_str() );

			this->myNumSucceeded++;
		}
	}
}