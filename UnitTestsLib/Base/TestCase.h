/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#include <Foundation/RefCount.h>

#include <UnitTestsLib/Base/TestSimpleRtti.h>
#include <UnitTestsLib/Base/TestFailureHandler.h>
#include <UnitTestsLib/Base/TestOutputLogger.h>

namespace UnitTestsLib
{
	class TestCase
	{
		friend class TestRunner;

		__IME_DECLARE_TEST_RTTI;

	public:
		virtual ~TestCase();

		// Runs current instance of case
		virtual void Run() = 0;

		// Verifies selected statement
		void Verify( bool statement,
					 bool expect,
					 const char* file = __FILE__,
					 int line = __LINE__ );

		// Verifies selected statement and writes message on failure
		void Verify( bool statement, 
					 bool expect, 
					 const char* message,
					 const char* file = __FILE__, 
					 int line = __LINE__ );

		u32 GetNumSucceeded() const;

		u32 GetNumFailed() const;

		u32 GetNumVerified() const;

	protected:
		TestCase( TestOutputLogger* handler );

	private:
		TestCase* myNext;
		TestOutputLogger* myOutputLogger;

		u32 myNumSucceeded;
		u32 myNumFailed;
		u32 myNumVerified;
	};

	__IME_INLINED u32 TestCase::GetNumSucceeded() const
	{
		return this->myNumSucceeded;
	}

	__IME_INLINED u32 TestCase::GetNumFailed() const
	{
		return this->myNumFailed;
	}

	__IME_INLINED u32 TestCase::GetNumVerified() const
	{
		return this->myNumVerified;
	}
}