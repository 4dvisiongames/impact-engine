/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#pragma once

#include <string>

namespace UnitTestsLib
{
	// Forward declaration
	class TestCase;
	class TestOutputLogger;

	struct SimpleRtti
	{
		typedef TestCase* ( *CreatorFunction )( TestOutputLogger* );
		typedef void( *DestructorFunction )( TestCase* );

		SimpleRtti( const char* testCaseName, CreatorFunction testCreator, DestructorFunction testDestructor )
			: myTestCaseName( testCaseName )
			, myConstructor( testCreator )
			, myDestructor( testDestructor )
		{
			/// Empty
		}

		std::string myTestCaseName;
		CreatorFunction myConstructor;
		DestructorFunction myDestructor;
	};
}

#define __IME_DECLARE_TEST_RTTI \
	public: \
	static const UnitTestsLib::SimpleRtti RTTI; \
	virtual const UnitTestsLib::SimpleRtti& GetRtti() const \
	{ \
		return RTTI; \
	} \
	private:

#define __IME_IMPLEMENT_TEST_CONSTRUCTOR(className, ctorName) \
	public: \
	static TestCase* ctorName(UnitTestsLib::TestOutputLogger* logger) \
	{ \
		return new className(logger); \
	} \
	private:

#define __IME_IMPLEMENT_TEST_DESTRUCTOR(className, dtorName) \
	public: \
	static void dtorName( UnitTestsLib::TestCase* cls ) \
	{ \
		delete cls; \
	} \
	private:

#define __IME_IMPLEMENT_TEST_RTTI(caseName, className, constructor, destructor) \
	const UnitTestsLib::SimpleRtti className::RTTI(caseName, constructor, destructor);
