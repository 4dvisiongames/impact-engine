/*

Copyright (c) 2015, Pavel Umnikov
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice,
this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright
notice, this list of conditions and the following disclaimer in the
documentation and/or other materials provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
DAMAGE.

*/

#include <UnitTestsLib/TestRunner.h>
#include <UnitTestsLib/Base/TestOutputLogger.h>
#include <UnitTestsLib/Base/TestFailureException.h>
#include <UnitTestsLib/Base/TestStartupException.h>
#include <UnitTestsLib/Base/TestSimpleRtti.h>

/// C++ STL
#include <sstream>

namespace UnitTestsLib
{
	TestRunner::TestRunner()
		: myHead( nullptr )
		, myLast( nullptr )
		, myFailureHandler( nullptr )
		, myOutputLogger( nullptr )
	{
		/// Empty
	}

	TestRunner::~TestRunner()
	{
		this->Dispose();
	}

	void TestRunner::RegisterTestCase( const SimpleRtti& testCaseRtti )
	{
		if( testCaseRtti.myConstructor == nullptr )
			throw TestStartupException( "Failed to register RTTI object, because testCaseRtti.myConstructor == nullptr!" );

		TestCase* testCase = testCaseRtti.myConstructor( this->myOutputLogger );

		std::stringstream strStream;
		strStream << "--> TestCase registered: ["
			<< testCaseRtti.myTestCaseName.c_str()
			<< "]\r\n";

		std::string& str = strStream.str();

		this->myOutputLogger->Output( str.c_str() );

		if ( this->myHead == nullptr )
		{
			this->myHead = this->myLast = testCase;
			return;
		}

		// Always added once, when next node of head is null
		if ( this->myHead->myNext == nullptr )
		{
			this->myLast = testCase;
			this->myHead->myNext = this->myLast;
			return;
		}

		// For all others
		this->myLast->myNext = testCase;
		this->myLast = testCase;
	}

	void TestRunner::RegisterFailureHandler( TestFailureHandler* handler )
	{
		if ( handler == nullptr )
			throw TestStartupException( "Failed to register TestFailureHandler, because handler == nullptr!" );

		this->myFailureHandler = handler;
	}

	void TestRunner::RegisterOutputLogger( TestOutputLogger* logger )
	{
		if ( logger == nullptr )
			throw TestStartupException( "Failed to register TestOutputLogger, because logger == nullptr!" );

		this->myOutputLogger = logger;
	}

	void TestRunner::Run()
	{
		if ( this->myFailureHandler == nullptr )
			throw TestStartupException( "Failed to start, because myFailureHandler == nullptr!" );

		if ( this->myOutputLogger == nullptr )
			throw TestStartupException( "Failed to start, because myOutputLogger == nullptr!" );

		TestCase* testCase = this->myHead;
		while ( testCase != nullptr )
		{
			try
			{
				testCase->Run();
			}
			catch ( TestFailureException e )
			{
				this->myFailureHandler->OnFailure( e.what() );
			}

			u32 verified = testCase->GetNumVerified();
			u32 passed = testCase->GetNumSucceeded();
			u32 failed = testCase->GetNumFailed();

			std::stringstream strStream;
			strStream << "--> TestCase results: verifications["
				<< verified
				<< "] where passed["
				<< passed
				<< "] and failed ["
				<< failed
				<< "]\r\n";

			std::string& str = strStream.str();

			this->myOutputLogger->Output( str.c_str() );
			testCase = testCase->myNext;
		}
	}

	void TestRunner::Dispose()
	{
		TestCase* current = this->myHead;
		while ( current != nullptr )
		{
			TestCase* toDestroy = current;
			current = current->myNext;

			const SimpleRtti& rtti = toDestroy->GetRtti();
			if( rtti.myDestructor == nullptr )
				throw TestStartupException( "Failed to destroy RTTI object, because testCaseRtti.myDestructor == nullptr!" );

			rtti.myDestructor( toDestroy );
		}

		this->myFailureHandler = nullptr;
		this->myOutputLogger = nullptr;
	}
}