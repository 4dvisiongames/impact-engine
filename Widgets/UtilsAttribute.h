/*

Copyright (c) 2014, Pavel Umnikov 
All rights reserved. 

Redistribution and use in source and binary forms, with or without 
modification, are permitted provided that the following conditions are met: 

 * Redistributions of source code must retain the above copyright notice, 
   this list of conditions and the following disclaimer. 
 * Redistributions in binary form must reproduce the above copyright 
   notice, this list of conditions and the following disclaimer in the 
   documentation and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND ANY 
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE 
DISCLAIMED. IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE FOR ANY 
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR 
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER 
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT 
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY 
OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH 
DAMAGE. 

 */

#pragma once

#ifndef __Widgets__UtilsAttribute_h__
#define __Widgets__UtilsAttribute_h__

/// Delegate
#include <Foundation/STLDelegate.h>

namespace Widgets
{
	namespace Utils
	{
		template< typename Type >
		class Attribute
		{
		public:
			typedef Attribute< Type > ThisType;
			typedef STL::DelegateArg0< Type > TypedDelegate;

			Attribute( );

			template< typename OtherType >
			Attribute( const OtherType& rhs );

			Attribute( const TypedDelegate& delRhs );

			bool operator == ( const ThisType& rhs );
			bool operator != ( const ThisType& rhs );

			bool IsSet( ) const;
			const Type& Get( ) const;
			const Type& Get( const Type& defaultValue ) const;

		private:
			TypedDelegate myDelegate;
			mutable Type myCachedValue;
		};

		template< typename Type >
		__IME_INLINED bool Attribute< Type >::operator == ( const ThisType& rhs )
		{
			return rhs.Get() == this->Get();
		}

		template< typename Type >
		__IME_INLINED bool Attribute< Type >::operator != ( const ThisType& rhs )
		{
			return rhs.Get() != this->Get();
		}

		template< typename Type >
		__IME_INLINED bool Attribute< Type >::IsSet( ) const
		{
			return this->myDelegate != nullptr;
		}

		template< typename Type >
		__IME_INLINED const Type& Attribute< Type >::Get( ) const
		{
			if( this->IsSet() ) {
				this->myCachedValue = this->myDelegate();
			}

			return this->myCachedValue;
		}
	}
}

#endif /// __Widgets__UtilsAttribute_h__